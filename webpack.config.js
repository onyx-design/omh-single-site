const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');

// Paths
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
const paths = {
  dist: path.resolve(__dirname, './dist/js'),
  src: path.resolve(__dirname, './assets/js')
}

// Webpack Config
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
module.exports = {
    watch: true,
    context: paths.src,
    entry: {
      // 'meta-boxes': './meta-boxes.js',
      // 'mh-create-product': './mh-create-product.js',
      // 'mh-pay-page': './mh-pay-page.js',
      'admin' : './admin.js',
      'omh'   : './omh.js'
    },
    output: {
      path: paths.dist,
      filename: '[name].js'
    },
    externals: {
      jquery: 'jQuery'
    },
    devtool: 'source-map',
    module: {
      rules: [
        { 
          test: /\.js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['env']
              }
            }
          ]
        }  
      ]
    },
    plugins: [
      new CleanWebpackPlugin(['dist/js']),
      new webpack.ProgressPlugin(function (percentage, message) {
        const percent = Math.round(percentage * 100);
        process.stderr.clearLine();
        process.stderr.cursorTo(0);
        process.stderr.write(percent + '% ' + message);
      })
    ]
}

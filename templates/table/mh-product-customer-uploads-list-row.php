<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_customer_upload = $row_data;

if( $omh_customer_upload ): 

	$filename = basename( $omh_customer_upload );
	$file_path = WP_CONTENT_DIR . '/uploads/chapters/' . $filename;
	// $file_path = get_current_site_upload_dir( 'omh/' . $this->query['id'] ) . $filename;
	$file_extension = pathinfo( $file_path, PATHINFO_EXTENSION );
?>

<tr class="omh-table-row">
	<!-- Row Actions -->
	<td data-col="actions">
		<?php
			echo OMH_HTML_UI_Button::factory(
				array(
					'href'		=> $omh_customer_upload,
					'color'		=> 'secondary',
					'size'		=> 'sm',
					'label'		=> 'Download',
					'attrs'		=> array(
						'download'	=> $filename
					)
				)
			);
		?>
	</td>
	<!-- Preview -->
	<td class="td-thumbnail">
		<?php
			$allowed_extensions = array( 'jpg', 'jpeg', 'png', 'bmp' );
			if( in_array( $file_extension, $allowed_extensions ) ) {
				echo OMH_HTML_Tag::factory(
					array(
						'type'	=> 'img',
						'attrs'	=> array(
							'src'	=> $omh_customer_upload
						)
					)
				);
			} else {
			?>
				<div class="td-thumbnail-placeholder">
					<?php echo strtoupper( $file_extension ); ?>
				</div>
			<?php
			}
		?>
	</td>
	<!-- Info -->
	<td>
		<?php echo $filename; ?>
		<div class="text-muted">
			<?php echo human_readable_filesize( filesize( $file_path ) ); ?>
		</div>
	</td>
</tr>

<?php endif; ?>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_college = $row_data;

// Determine Row Actions 
$enable_row_actions = true;
$row_action_attr =  ' data-default-row-action="edit"';

if( $omh_college ) { ?>
<tr class="omh-table-row" data-id="<?php echo $omh_college->ID; ?>" <?php echo $row_action_attr; ?>>

	<!-- ID -->
	<td class="td-primary">
		<?php echo $omh_college->get_id(); ?>

		<?php if( $enable_row_actions ): ?>
			<div class="row-actions">
				<a class="row-action row-action--edit" data-action="edit" data-omh-toggle-modal="college-modal" data-omh-modal-load-id="<?php echo $omh_college->ID;?>" title="Edit College" data-toggle="tooltip">
					Edit
				</a>
			</div>
		<?php endif; // End if( $row_actions ) ?>
	</td>

	<!-- Logo -->
	<td class="td-thumbnail">
		<?php 
			if( $logo_src = wp_get_attachment_image_src( $omh_college->get_logo_image_id(), 'woocommerce_thumbnail' ) ) {

				echo '<img src="' . $logo_src[0] . '" />';
			}
		?>
	</td>

	<!-- Banner -->
	<td class="td-thumbnail">
		<?php 
			if( $banner_src = wp_get_attachment_image_src( $omh_college->get_banner_image_id(), 'woocommerce_thumbnail' ) ) {

				echo '<img src="' . $banner_src[0] . '" />';
			}
		?>
	</td>

	<!-- College Name -->
	<td>
		<?php echo $omh_college->get_college_name(); ?>
	</td>

	<!-- College Shortname -->
	<td>
		<?php echo $omh_college->get_college_shortname(); ?>
	</td>

	<!-- College Letters -->
	<td>
		<?php echo $omh_college->get_college_letters(); ?>
	</td>

	<!-- City -->
	<td>
		<?php echo $omh_college->get_city(); ?>
	</td>

	<!-- State -->
	<td>
		<?php echo $omh_college->get_state(); ?>
	</td>

	<!-- Term ID -->
	<td>
		<?php echo $omh_college->get_term_id(); ?>
	</td>

	<!-- Meta Created -->
	<td>
		<?php echo $omh_college->get_meta_created(); ?>
	</td>

	<!-- Meta Updated -->
	<td>
		<?php echo $omh_college->get_meta_updated(); ?>
	</td>
</tr>
<?php } ?>
<?php

$garment_product = OMH()->garment_product_factory->get_by_inksoft_id( $inksoft_product->ID );
$garment_styles = $garment_product ? $garment_product->get_garment_styles() : array();
?>
<table class="table omh-table omh-add-global-garment-table">
	<thead class="omh-table-section omh-table-section-headers">
		<tr>
			<th scope="col" data-col="add">Add?</th>
			<th scope="col" data-col="garment">
				<div class="omh-add-global-garment-name"><?php echo $inksoft_product->Name; ?></div>
				<div class="omh-add-global-garment-information">
					<?php echo $inksoft_product->Manufacturer; ?> - <?php echo $inksoft_product->Sku; ?>
				</div>
			</th>
		</tr>
	</thead>

	<tbody class="omh-table-section omh-table-section-results omh-add-global-garment-body">
		<?php 
			foreach( $inksoft_product->Styles as $inksoft_product_style ): 

				if( isset( $garment_styles[ $inksoft_product_style->ID ] ) ) {
					$added = true;
				} else {
					$added = false;
				}
		?>
			<tr class="omh-table-row garment-style-row omh-field-input omh-field-wrap omh-input-wrap">
				<td class="add-garment-style">
					<?php
						$garment_style_add_checkbox = OMH_HTML_UI_Input::factory(
							array(
								'input_id'		=> 'add_inksoft_product_' . $inksoft_product_style->ID,
								'input_type'	=> 'checkbox',
								'label'			=> false
							)
						);

						if( $added ) {
							$garment_style_add_checkbox->set_disabled( true );
						}

						echo $garment_style_add_checkbox;
					?>
				</td>

				<td>
					<img class="omh-add-global-garment-thumbnail" src="//stores.inksoft.com/<?php echo $inksoft_product_style->Sides[0]->ImageFilePath; ?>" />
					<div class="omh-add-global-garment-name"><?php echo $inksoft_product_style->Name; ?></div>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
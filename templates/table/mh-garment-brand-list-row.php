<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_user = wp_get_current_user();

$omh_garment_brand = $row_data;

// Determine Row Actions 
$enable_row_actions = false;
$row_action_attr 	= '';

if( $omh_user->has_role( 'administrator' ) ) {

	$row_action_attr =  ' data-default-row-action="edit"';
}
?>

<tr class="omh-table-row" <?php echo $row_action_attr; ?>>

	<!-- ID -->
	<td class="td-primary">
		<?php echo $omh_garment_brand->get_id(); ?>

		<div class="row-actions">
			<a class="row-action row-action--edit" data-action="edit" data-omh-toggle-modal="garment-brand-modal" data-omh-modal-load-id="<?php echo $omh_garment_brand->get_id();?>" title="Edit Garment Brand" data-toggle="tooltip">
				Edit
			</a>
		</div>
	</td>

	<!-- Brand Slug -->
	<td>
		<?php echo $omh_garment_brand->get_brand_slug(); ?>
	</td>

	<!-- Brand Code -->
	<td>
		<?php echo $omh_garment_brand->get_brand_code(); ?>
	</td>

	<!-- InkSoft Brand ID -->
	<td>
		<?php echo $omh_garment_brand->get_inksoft_brand_id(); ?>
	</td>

	<!-- Brand Name -->
	<td>
		<?php echo $omh_garment_brand->get_brand_name(); ?>
	</td>

	<!-- Brand Image URL -->
	<td>
		<?php echo $omh_garment_brand->get_brand_image_url(); ?>
	</td>

	<!-- Size Chart URL -->
	<td>
		<?php echo $omh_garment_brand->get_size_chart_url(); ?>
	</td>

	<!-- Meta Created -->
	<td>
		<?php echo $omh_garment_brand->get_meta_created(); ?>
	</td>

	<!-- Meta Updated -->
	<td>
		<?php echo $omh_garment_brand->get_meta_updated(); ?>
	</td>
</tr>

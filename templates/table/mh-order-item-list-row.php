<?php 
	$line_item = $row_data;
	$line_item_variable = wc_get_product( $line_item->get_product_id() );
	if( $line_item_variation = wc_get_product( $line_item->get_variation_id() ) ) {
		$line_item_garment = $line_item_variation->get_product_garment();
	}
?>

<tr>
	
	<!-- Product Image -->
	<td class="td-thumbnail"><img src="<?php echo $line_item_garment && $line_item_garment->get_image_back() ? $line_item_garment->get_image_back()['thumbnail_src'] : ''; ?>" /></td>

	<!-- Product Name  -->
	<td> <?php echo $line_item->get_name(); ?> </td>

	<!-- Product Price & Quantity -->
	<td class="text-right"> <?php echo wc_price( $line_item->get_subtotal() / $line_item->get_quantity() ); ?> x <?php echo $line_item->get_quantity(); ?></td>

	<!-- Line Item Price  -->
	<td class="text-right"> <?php echo wc_price( $line_item->get_total() ); ?> </td>


	<!-- Subtotal -->
	<?php if ( $this->show_earnings_col ): ?>
		<td class="text-right">
			<?php 
				$chapter = OMH_Session::get_chapter();

				if( !empty( OMH()->credit_factory->get_credit_by_chapter_and_order( $chapter->get_id(), $line_item->get_order_id() ) ) ) {
					echo wc_price( ( $line_item->get_total() * ( $line_item_variable->get_earnings_percentage() / 100 ) ) );
				} else {
					echo wc_price( 0 );
				}
			?>
		</td>
	<?php endif; ?>
</tr>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_organization = $row_data;

// Determine Row Actions 
$enable_row_actions = true;
$row_action_attr =  ' data-default-row-action="edit"';

if( $omh_organization ) { ?>
<tr class="omh-table-row" data-id="<?php echo $omh_organization->ID; ?>" <?php echo $row_action_attr; ?>>

	<!-- ID -->
	<td class="td-primary">
		<?php echo $omh_organization->get_id(); ?>

		<?php if( $enable_row_actions ): ?>
			<div class="row-actions">
				<a class="row-action row-action--edit" data-action="edit" data-omh-toggle-modal="organization-modal" data-omh-modal-load-id="<?php echo $omh_organization->ID;?>" title="Edit Organization" data-toggle="tooltip">
					Edit
				</a>
			</div>
		<?php endif; // End if( $row_actions ) ?>
	</td>

	<!-- Logo -->
	<td class="td-thumbnail">
		<?php 
			if( $logo_src = wp_get_attachment_image_src( $omh_organization->get_logo_image_id(), 'woocommerce_thumbnail' ) ) {

				echo '<img src="' . $logo_src[0] . '" />';
			}
		?>
	</td>

	<!-- Banner -->
	<td class="td-thumbnail">
		<?php 
			if( $banner_src = wp_get_attachment_image_src( $omh_organization->get_banner_image_id(), 'woocommerce_thumbnail' ) ) {

				echo '<img src="' . $banner_src[0] . '" />';
			}
		?>
	</td>

	<!-- Org Name -->
	<td>
		<?php echo $omh_organization->get_org_name(); ?>
	</td>

	<!-- Org Letters -->
	<td>
		<?php echo $omh_organization->get_org_letters(); ?>
	</td>

	<!-- Org Greek Letters -->
	<td>
		<?php echo $omh_organization->get_org_greek_letters(); ?>
	</td>

	<!-- Org Shortname -->
	<td>
		<?php echo $omh_organization->get_org_shortname(); ?>
	</td>

	<!-- Term ID -->
	<td>
		<?php echo $omh_organization->get_term_id(); ?>
	</td>

	<!-- Meta Created -->
	<td>
		<?php echo $omh_organization->get_meta_created(); ?>
	</td>

	<!-- Meta Updated -->
	<td>
		<?php echo $omh_organization->get_meta_updated(); ?>
	</td>
</tr>
<?php } ?>
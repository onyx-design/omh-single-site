<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

$omh_credit = $row_data;

if( $omh_credit ) {
?>

<tr class="omh-table-row" data-id="<?php echo $omh_credit->ID; ?>">

	<?php if ( $this->show_date_created_col ): ?>
	<td>
		<?php echo date( 'F j, Y', strtotime( $omh_credit->get_ledger_date() ) ); ?>
	</td>
	<?php endif; ?>

	<?php if ( $this->show_order_number_col ): ?>
	<td>
		<?php
			if( $order = $omh_credit->get_order() ) {
				echo $order->get_order_number();
			}
		?>
	</td>
	<?php endif; ?>

	<?php if ( $this->show_description_col ): ?>
	<td>
		<?php echo $omh_credit->get_description(); ?>
		<div class="text-muted">
			<?php echo $omh_credit->get_notes(); ?>
		</div>
	</td>
	<?php endif; ?>

	<?php if ( $this->show_type_col ): ?>
	<td>
		<?php echo $omh_credit->get_type_badge(); ?>
	</td>
	<?php endif; ?>

	<?php if ( $this->show_amount_col ): ?>
	<td class="currency">
		<?php echo wc_price( $omh_credit->get_amount( false ) ); ?>
	</td>
	<?php endif; ?>
	
</tr>
<?php 
 }
?>

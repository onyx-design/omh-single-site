<?php
if( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_user = wp_get_current_user();

$omh_product = $row_data;

// Determine Row Actions
$enable_row_actions = false;
$row_action_attr = '';

if( $omh_user->has_role('administrator') || $omh_product->get_product_stats( 'step' ) > 1 ) {

	$enable_row_actions = true;

	if( $omh_product->user_can_edit() ) {
		$row_action_attr = ' data-default-row-action="edit"';
	}
}
?>

<tr class="omh-table-row" data-id="<?php echo $omh_product->get_id(); ?>" <?php echo $row_action_attr; ?>>

	<td class="td-thumbnail">
		<?php // Image
			echo $omh_product->get_image();
		?>
	</td>

	<td class="td-primary">

		<?php // Name
			echo $omh_product->get_title(); 
		?>
		
		<?php if( $enable_row_actions ): ?>
			<div class="row-actions">

				<?php if( $omh_product->user_can_edit() ) : // Edit Product ?>
					<a class="row-action row-action--edit" data-action="edit" href="<?php echo OMH()->dashboard_url( 'edit-product?id=' . $omh_product->get_id() ); ?>" title="Edit product" data-toggle="tooltip">
						Edit
					</a>
				<?php endif; ?>
				
				<?php if( $omh_product->user_can_delete() ): // Delete Product ?>
					<a class="row-action row-action--delete" data-action="delete" data-omh-toggle-modal="delete-product-modal" data-omh-modal-load-id="<?php echo $omh_product->get_id(); ?>" title="Delete Product" data-toggle="tooltip">
						Delete
					</a>
				<?php endif; ?>

				<?php if( $omh_product->can_convert_to_retail() ): ?>
					<a class="row-action row-action--convert" data-action="convert-to-retail" data-omh-toggle-modal="convert-to-retail-modal" data-omh-modal-load-id="<?php echo $omh_product->get_id(); ?>" title="Convert to Retail" data-toggle="tooltip">
						<i class="fa fa-exchange"></i> Retail
					</a>
				<?php endif; ?>

				<?php if( $omh_product->can_convert_to_campaign() ): ?>
					<a class="row-action row-action--convert" data-action="convert-to-campaign" data-omh-toggle-modal="convert-to-campaign-modal" data-omh-modal-load-id="<?php echo $omh_product->get_id(); ?>" title="Convert to Campaign" data-toggle="tooltip">
						<i class="fa fa-exchange"></i> Campaign
					</a>
				<?php endif; ?>

				<?php if( $omh_product->can_convert_to_bulk() ): ?>
					<a class="row-action row-action--convert" data-action="convert-to-bulk" data-omh-toggle-modal="convert-to-bulk-modal" data-omh-modal-load-id="<?php echo $omh_product->get_id(); ?>" title="Convert to Bulk" data-toggle="tooltip">
						<i class="fa fa-exchange"></i> Bulk
					</a>
				<?php endif; ?>

				<?php if( $omh_product->get_product_status( 'step' ) > 1 && $omh_user->has_role('house_admin') ) : // View Sales ?>
					<a class="row-action row-action--product-sales" data-action="sales" href="<?php echo OMH()->dashboard_url( 'product-sales?id=' . $omh_product->get_id() ); ?>" title="View retail orders" data-delay="" data-toggle="tooltip">
						Sales
					</a>
				<?php endif; ?>

				<?php if( $omh_product->is_product_visible( 'link' ) ) : ?>
					<a class="row-action row-action--view" data-action="view" href="<?php echo $omh_product->get_permalink(); ?>" target="_blank" title="View on storefront" data-toggle="tooltip">
						View
					</a>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</td>

	<td>
		<?php // Type
			echo ucwords( $omh_product->get_sales_type() );
		?>
	</td>

	<td>
		<?php // Chapter
			$chapter = $omh_product->get_chapter();

			echo $chapter ? $chapter->get_chapter_name() : '';
		?>
	</td>

	<td>
		<?php // SKU
			echo $omh_product->get_sku();
		?>
	</td>

	<?php if ( $this->show_status_col ): ?>
		<td>
			<?php // Status
				echo $omh_product->get_product_status_badge();
			?>
		</td>
	<?php endif; ?>

</tr>
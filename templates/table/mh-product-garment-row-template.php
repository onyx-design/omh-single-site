<tr class="omh-table-row garment__template-row row--template omh-field-input omh-field-wrap omh-input-wrap" name="<%= garmentStyleId %>" data-omh-namespace="<%= garmentNamespace %>" data-omh-subfield-of="<%= parentField.namespace %>" data-omh-field-type="garment">

	<input type="hidden" value="<%= garmentStyleId %>" name="garment_style_id" data-omh-subfield-of="<%= garmentNamespace %>" data-omh-var-type="int">

	<!-- Actions -->
	<td class="garment-actions">
		<div class="garment__remove omh-field-wrap">
			<input type="checkbox" id="garment__removed[<%= garmentStyleId %>]" class="field--template omh-field-input remove-garment" data-omh-field-type="checkbox">
			<label for="garment__removed[<%= garmentStyleId %>]">
				<i class="fa fa-times-circle"></i>
			</label>
		</div>
		<div class="garment__featured omh-field-wrap">
			<input type="radio" id="featured-garment[<%= garmentStyleId %>]" name="featured" class="field--template omh-field-input" data-omh-subfield-of="<%= garmentNamespace %>" data-omh-field-type="checkbox">
			<label for="featured-garment[<%= garmentStyleId %>]">
				<i class="fa fa-star"></i>
			</label>
		</div>
		<div class="garment__details">
			<i class="fa fa-info"></i>
		</div>
	</td>
	<!-- Garment Info -->
	<!-- <td>
		<div class="garment__manufacturer"></div>
		<div class="garment__name"></div>
		<div class="garment__color"></div>
	</td> -->
	<!-- Images -->
	<td class="garment__image-front">
		<div class="front-image-file omh-field-wrap omh-field--ajax-file">
			<div class="mh-file-upload-box__file">
				<input class="mh-file-upload field--template omh-field-input" type="file" id="image_front[<%= garmentStyleId %>]" name="image_front" data-omh-field-type="ajaxFile" data-omh-subfield-of="<%= garmentNamespace %>" />
			</div>
			<div class="mh-file-upload-box__uploading">Uploading</div>
			<div class="mh-file-upload-box__success">Done</div>
			<div class="mh-file-upload-box__error">Error</div>
		</div>

		<div class="omh-garment-name"></div>
	</td>
	<td class="garment__image-back">
		<div class="back-image-file omh-field-wrap omh-field--ajax-file">
			<div class="mh-file-upload-box__file">
				<input class="mh-file-upload field--template omh-field-input" type="file" id="image_back[<%= garmentStyleId %>]" name="image_back" data-omh-field-type="ajaxFile" data-omh-subfield-of="<%= garmentNamespace %>" />
			</div>
			<div class="mh-file-upload-box__uploading">Uploading</div>
			<div class="mh-file-upload-box__success">Done</div>
			<div class="mh-file-upload-box__error">Error</div>
		</div>
	</td>
	<td class="garment__image-proof">
		<div class="proof-image-file omh-field-wrap omh-field--ajax-file">
			<div class="mh-file-upload-box__file">
				<input class="mh-file-upload field--template omh-field-input" type="file" id="image_proof[<%= garmentStyleId %>]" name="image_proof" data-omh-field-type="ajaxFile" data-omh-subfield-of="<%= garmentNamespace %>" />
			</div>
			<div class="mh-file-upload-box__uploading">Uploading</div>
			<div class="mh-file-upload-box__success">Done</div>
			<div class="mh-file-upload-box__error">Error</div>
		</div>
	</td>
	<!-- Price Tiers -->
	<td class="garment__price-tiers">
		<div class="omh-table-wrap omh-table-wrap--garment-price-tiers omh-field-wrap">
			<div class="omh-table-wrap-inner">
				<table data-omh-field-type="garmentPriceTiers" data-omh-subfield-of="<%= garmentNamespace %>" name="pricing" class="table omh-table table-equal-width table--price-quantity-tiers pricing-tiers pricing-tiers--quantity  omh-field-input omh-field-advanced omh-field-advanced--price-tiers"  >
					<tbody>
						<tr class="omh-table-row price-tiers__header-row">
							<th>
								Tier
								<span class="product-price-tier-qty">Min Qty</span>
							</th>
							
						</tr>
						<tr class="omh-table-row price-tiers__values-row">
							<th>Price</th>
							
						</tr>
					</tbody>
				</table>
				
			</div>
		</div>
	</td>
	<!-- Sizes -->
	<td class="garment__sizes">
		<div class="omh-field-wrap omh-field-input" name="sizes" data-omh-field-type="garmentSizes" data-omh-subfield-of="<%= garmentNamespace %>" data-omh-namespace="sizes[<%= garmentStyleId %>]">
		</div>
	</td>
</tr>

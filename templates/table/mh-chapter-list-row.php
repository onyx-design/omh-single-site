<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_chapter = $row_data;

// Determine Row Actions 
$enable_row_actions = true;
$row_action_attr =  ' data-default-row-action="edit"';
?>

<tr class="omh-table-row" <?php echo $row_action_attr; ?>>

	<!-- ID -->
	<td class="td-primary">
		<?php echo $omh_chapter->get_id(); ?>

		<?php if( $enable_row_actions ): ?>
			<div class="row-actions">
				<a class="row-action row-action--edit" data-action="edit" data-omh-toggle-modal="chapter-modal" data-omh-modal-load-id="<?php echo $omh_chapter->ID;?>" title="Edit Chapter" data-toggle="tooltip">
					Edit
				</a>
			</div>
		<?php endif; // End if( $row_actions ) ?>
	</td>

	<!-- Banner -->
	<td class="td-thumbnail">
		<?php 
			if( $banner_src = wp_get_attachment_image_src( $omh_chapter->get_banner_image_id(), 'woocommerce_thumbnail' ) ) {

				echo '<img src="' . $banner_src[0] . '" />';
			}
		?>
	</td>

	<!-- Org ID -->
	<td>
		<?php echo isset( $this->organizations[ $omh_chapter->get_org_id() ] ) ? $this->organizations[ $omh_chapter->get_org_id() ]->org_letters : ''; ?>
	</td>

	<!-- College ID -->
	<td>
		<?php echo isset( $this->colleges[ $omh_chapter->get_college_id() ] ) ? $this->colleges[ $omh_chapter->get_college_id() ]->college_name : ''; ?>
	</td>

	<!-- Chapter -->
	<td>
		<?php echo $omh_chapter->get_chapter_name(); ?>
	</td>

	<!-- URL Structure -->
	<td>
		<?php echo $omh_chapter->get_url_structure(); ?>
	</td>

	<!-- Earnings Percentage -->
	<td>
		<?php echo $omh_chapter->get_earnings_percentage( false ); ?>
	</td>

	<!-- Status -->
	<td>
		<?php echo $omh_chapter->get_status(); ?>
	</td>

	<!-- Term ID -->
	<td>
		<?php echo $omh_chapter->get_term_id(); ?>
	</td>

	<!-- Meta Created -->
	<td>
		<?php echo $omh_chapter->get_meta_created(); ?>
	</td>

	<!-- Meta Updated -->
	<td>
		<?php echo $omh_chapter->get_meta_updated(); ?>
	</td>
</tr>

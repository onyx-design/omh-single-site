<tr class="omh-table-row garment__template-row row--template campaign-row--template omh-field-input omh-field-wrap omh-input-wrap" name="<%= garmentStyleId %>" data-omh-namespace="<%= garmentNamespace %>" data-omh-subfield-of="<%= parentField.namespace %>" data-omh-field-type="garment">

	<input type="hidden" value="<%= garmentStyleId %>" name="garment_style_id" data-omh-subfield-of="<%= garmentNamespace %>" data-omh-var-type="int">

	<!-- Actions -->
	<td class="garment-actions">
		<?php if (is_super_admin()) : ?>
		<div class="garment__remove omh-field-wrap">
			<input type="checkbox" name="garment__removed" id="garment__removed[<%= garmentStyleId %>]" class="field--template omh-field-input remove-garment" data-omh-subfield-of="<%= garmentNamespace %>" data-omh-field-type="checkbox">
			<label for="garment__removed[<%= garmentStyleId %>]">
				<i class="fa fa-times-circle"></i>
			</label>
		</div>
		<?php endif; ?>
		<div class="garment__featured omh-field-wrap <?php if (!is_super_admin()) {echo 'disabled';} ?>">
			<input type="radio" id="featured-garment[<%= garmentStyleId %>]" name="featured" class="field--template omh-field-input <?php if (!is_super_admin()) {echo 'disabled';} ?>" <?php if (!is_super_admin()) {echo 'disabled';} ?> data-omh-subfield-of="<%= garmentNamespace %>" data-omh-field-type="checkbox">
			<label for="featured-garment[<%= garmentStyleId %>]">
				<i class="fa fa-star"></i>
			</label>
		</div>
		<div class="garment__details">
			<i class="fa fa-info"></i>
		</div>
	</td>
	<!-- Garment Info -->
	<!-- <td>
		<div class="garment__manufacturer"></div>
		<div class="garment__name"></div>
		<div class="garment__color"></div>
	</td> -->
	<!-- Images -->
	<td class="garment__image-front">
		<div class="front-image-file omh-field-wrap omh-field--ajax-file <?php if (!is_super_admin()) {echo 'disabled';} ?>">
			<div class="mh-file-upload-box__file">
				<input class="mh-file-upload field--template omh-field-input" <?php if (!is_super_admin()) {echo 'disabled';} ?> type="file" id="image_front[<%= garmentStyleId %>]" name="image_front" data-omh-field-type="ajaxFile" data-omh-subfield-of="<%= garmentNamespace %>" />
			</div>
			<div class="mh-file-upload-box__uploading">Uploading</div>
			<div class="mh-file-upload-box__success">Done</div>
			<div class="mh-file-upload-box__error">Error</div>
			<div class="mh-file-upload__icons">
				<i class="fa fa-trash-o mh-file-upload-icon"></i>
			</div>
		</div>

		<div class="omh-garment-name"></div>
		<div class="omh-garment-feedback invalid-feedback"></div>
	</td>
	<td class="garment__image-back">
		<div class="back-image-file omh-field-wrap omh-field--ajax-file <?php if (!is_super_admin()) {echo 'disabled';} ?>">
			<div class="mh-file-upload-box__file">
				<input class="mh-file-upload field--template omh-field-input" <?php if (!is_super_admin()) {echo 'disabled';} ?> type="file" id="image_back[<%= garmentStyleId %>]" name="image_back" data-omh-field-type="ajaxFile" data-omh-subfield-of="<%= garmentNamespace %>" />
			</div>
			<div class="mh-file-upload-box__uploading">Uploading</div>
			<div class="mh-file-upload-box__success">Done</div>
			<div class="mh-file-upload-box__error">Error</div>
		</div>
	</td>
	<td class="garment__image-proof">
		<div class="proof-image-file omh-field-wrap omh-field--ajax-file <?php if (!is_super_admin()) {echo 'disabled';} ?>">
			<div class="mh-file-upload-box__file">
				<input class="mh-file-upload field--template omh-field-input" <?php if (!is_super_admin()) {echo 'disabled';} ?> type="file" id="image_proof[<%= garmentStyleId %>]" name="image_proof" data-omh-field-type="ajaxFile" data-omh-subfield-of="<%= garmentNamespace %>" />
			</div>
			<div class="mh-file-upload-box__uploading">Uploading</div>
			<div class="mh-file-upload-box__success">Done</div>
			<div class="mh-file-upload-box__error">Error</div>
		</div>
	</td>
	<!-- Price Tiers -->
	<td class="garment__price-tiers">

		<div class="input-group">
			<div class="form-control omh-field-wrap" data-omh-field-type="price">
				<input class="form-control omh-field-input price-val" type="number" name="base_price" omh-var-type="price" min="0.01" step="0.01" id="base_price[<%= garmentStyleId %>]" data-omh-subfield-of="<%= garmentNamespace %>" <?php echo !is_super_admin() ? 'disabled' : '' ?> required data-calc-sum-for="garment-earnings-<%= garmentStyleId %>" data-calc-negate="true" />
				<span class="form-text  small text-muted" title="This is the amount paid to Merch House." data-toggle="tooltip">Base<i class="fa fa-info-circle"></i></span>
			</div>
			<div class="form-control omh-field-wrap" data-omh-field-type="price">
				<input class="form-control omh-field-input price-val" type="number" name="retail_price" omh-var-type="price" step="0.01" id="retail_price[<%= garmentStyleId %>]" data-omh-subfield-of="<%= garmentNamespace %>" required data-calc-sum-for="garment-earnings-<%= garmentStyleId %>" />
				<span class="form-text  small text-muted" title="This is the price of the item in your store." data-toggle="tooltip">Retail<i class="fa fa-info-circle"></i></span>
			</div>
			<div class="form-control" data-omh-field-type="price">
				<input class="form-control disabled price-val no-validation-state" data-virtual-disabled="true" type="number" min="0.00" step="0.01" omh-var-type="price" tabindex="-1" data-calc-sum="garment-earnings-<%= garmentStyleId %>" step="0.01" />
				<span class="form-text small text-muted" title="This is the amount of Store Credit you receive per sale." data-toggle="tooltip">Earnings<i class="fa fa-info-circle"></i></span>
			</div>
		</div>
	</td>
	<!-- Sizes -->
	<td class="garment__sizes">
		<div class="omh-field-wrap omh-field-input <?php if (!is_super_admin()) {echo 'disabled';} ?>" name="sizes" data-omh-field-type="garmentSizes" data-omh-subfield-of="<%= garmentNamespace %>" data-omh-namespace="sizes[<%= garmentStyleId %>]">
		</div>
	</td>
</tr>
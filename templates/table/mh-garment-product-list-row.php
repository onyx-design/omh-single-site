<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_garment_product = $row_data;

?>

<tr class="omh-table-row">

	<!-- ID -->
	<td>
		<?php echo $omh_garment_product->get_id(); ?>
	</td>

	<!-- Product Slug -->
	<td>
		<?php echo $omh_garment_product->get_product_slug(); ?>
	</td>

	<!-- InkSoft Product ID -->
	<td>
		<?php echo $omh_garment_product->get_inksoft_product_id(); ?>
	</td>

	<!-- InkSoft SKU -->
	<td>
		<?php echo $omh_garment_product->get_inksoft_sku(); ?>
	</td>

	<!-- Manufacturer ID -->
	<td>
		<?php echo $omh_garment_product->get_garment_brand_id(); ?>
	</td>

	<!-- Manufacturer SKU -->
	<td>
		<?php echo $omh_garment_product->get_manufacturer_sku(); ?>
	</td>

	<!-- Product Name -->
	<td>
		<?php echo $omh_garment_product->get_product_name(); ?>
	</td>

	<!-- Material -->
	<td>
		<?php echo $omh_garment_product->get_material(); ?>
	</td>

	<!-- Product Long Description -->
	<td>
		<?php echo $omh_garment_product->get_product_long_description(); ?>
	</td>

	<!-- Meta Created -->
	<td>
		<?php echo $omh_garment_product->get_meta_created(); ?>
	</td>

	<!-- Meta Updated -->
	<td>
		<?php echo $omh_garment_product->get_meta_updated(); ?>
	</td>
</tr>

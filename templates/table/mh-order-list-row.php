<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_user = wp_get_current_user();

$omh_order = $row_data;

// Determine Row Actions 
$enable_row_actions = false;
$row_action_attr 	= '';

if ($omh_user->has_role( 'administrator' ) ) {

	$enable_row_actions = true;

	$row_action_attr =  ' data-default-row-action="view"';
}
?>

<tr class="omh-table-row" data-id="<?php echo $omh_order->get_id(); ?>" <?php echo $row_action_attr; ?>>

	<!-- === Order === -->
	<td class="td-primary">
		#<?php echo $omh_order->get_order_number(); ?>

		<?php if( $enable_row_actions ): ?>
			<div class="row-actions">
				<a class="row-action row-action--view" data-action="view" href="<?php echo OMH()->dashboard_url( 'view-order?id=' . $omh_order->get_id() ); ?>" title="View Order Details" data-toggle="tooltip">
					View
				</a>
			</div>
		<?php endif; // End if( $row_actions ) ?>
	</td>

	<!-- === Date === -->
	<td>
		<?php echo wc_format_datetime( $omh_order->get_date_created() ); ?>
	</td>
	
	<!-- === User Info === -->
	<?php if ( $this->show_customer_col ): ?>
	<td>
		<?php echo $omh_order->get_formatted_billing_full_name(); ?>
		<div class="text-muted">
			<?php echo $omh_order->get_billing_email(); ?>
		</div>
	</td>
	<?php endif ?>

	<!-- === User Role === -->
	<?php if ( $this->show_user_role_col ): ?>
		<td>
			<?php 
				echo $omh_order->get_user_role_badge(); 
			?>
		</td>
	<?php endif ?>

	<!-- === Status === -->
	<?php if ( $this->show_status_col ): ?>
		<td>
			<?php 
				$badge_contents = $omh_order->get_order_status_badge();
				echo OMH_HTML_Tag::factory(
					array (
						'type'		=> 'OMH_HTML_UI_Badge',
						'label' 	=> $badge_contents['text'],
						'color' 	=> $badge_contents['color']
					)
				);
			?>
		</td>
	<?php endif; ?>

	<!-- === Subtotal === -->
	<?php if ( $this->show_subtotal_col ): ?>
		<td>
			<?php echo wc_price( $omh_order->get_subtotal() ); ?>
		</td>
	<?php endif; ?>

	<!-- === Total === -->
	<td>
		<?php echo $omh_order->get_formatted_order_total(); ?>
	</td>
</tr>
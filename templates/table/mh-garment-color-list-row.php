<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_user = wp_get_current_user();

$omh_garment_color = $row_data;

// Determine Row Actions 
$enable_row_actions = false;
$row_action_attr 	= '';

if( $omh_user->has_role( 'administrator' ) ) {

	$row_action_attr =  ' data-default-row-action="edit"';
}

?>

<tr class="omh-table-row" <?php echo $row_action_attr; ?>>

	<!-- ID -->
	<td class="td-primary">
		<?php echo $omh_garment_color->get_id(); ?>

		<div class="row-actions">
			<a class="row-action row-action--edit" data-action="edit" data-omh-toggle-modal="garment-color-modal" data-omh-modal-load-id="<?php echo $omh_garment_color->get_id();?>" title="Edit Garment Color" data-toggle="tooltip">
				Edit
			</a>
		</div>
	</td>

	<!-- Color Code -->
	<td>
		<?php echo $omh_garment_color->get_color_code(); ?>
	</td>

	<!-- Color Name -->
	<td>
		<?php echo $omh_garment_color->get_color_name(); ?>
	</td>

	<!-- Color Hex 1 -->
	<td>
		<?php echo $omh_garment_color->get_color_hex_1(); ?>
	</td>

	<!-- Color Hex 2 -->
	<td>
		<?php echo $omh_garment_color->get_color_hex_2(); ?>
	</td>

	<!-- Meta Created -->
	<td>
		<?php echo $omh_garment_color->get_meta_created(); ?>
	</td>

	<!-- Meta Updated -->
	<td>
		<?php echo $omh_garment_color->get_meta_updated(); ?>
	</td>
</tr>

<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$omh_user = wp_get_current_user();

$omh_product = $row_data;

// Determine Row Actions 
$enable_row_actions = false;
$row_action_attr 	= '';

if ($omh_user->has_role('administrator') || +$omh_product->get_product_status('step') > 1) {

	$enable_row_actions = true;

	if( $omh_product->is_product_visible('link') && $omh_user->has_role('house_admin', false) ) {
		$row_action_attr =  ' data-default-row-action="view"';
	} elseif ($omh_product->user_can_edit()) {
		$row_action_attr =  ' data-default-row-action="edit"';
	} else {
		$row_action_attr =  ' data-default-row-action="sales"';
	}
}
// get_earnings_percentage
$total_sales = (float) $omh_product->get_total_sales();

$garment_meta = $omh_product->get_garments_meta();
$base_price = array_column($garment_meta, 'base_price')[0];
$retail_price = array_column($garment_meta, 'retail_price')[0];
$earning_per_product = $retail_price - $base_price;

?>

<tr class="omh-table-row" data-id="<?php echo $omh_product->get_id(); ?>" <?php echo $row_action_attr; ?>>

	<td class="td-thumbnail">
		<?php // Image
		echo $omh_product->get_image(); 
		?>
	</td>
	<td class="td-primary">

		<?php // Product Name
		echo $omh_product->get_title(); 
		?>

		<?php if ($enable_row_actions) : ?>
		<div class="row-actions">

			<?php if ($omh_product->user_can_edit()) : // Edit Product ?>
			<a class="row-action row-action--edit" data-action="edit" href="<?php echo OMH()->dashboard_url('edit-product?id=' . $omh_product->get_id()); ?>" title="Edit product" data-toggle="tooltip">
				Edit
			</a>
			<?php endif; ?>

			<?php if ($omh_product->is_product_visible('link')) : ?>
			<a class="row-action <?php echo ( $omh_user->has_role('house_admin', false) ? 'btn btn-sm btn-outline-primary row-action--btn row-action--show' : '' ); ?> row-action--view" data-action="view" href="<?php echo $omh_product->get_permalink(); ?>" target="_blank" title="View on storefront" data-toggle="tooltip">
				View
			</a>
			<?php endif; ?>

			<?php if (+$omh_product->get_product_status('step') > 1 && $omh_user->has_role('house_admin') ) : // View Sales ?>
				<a class="row-action row-action--product-sales" data-action="sales" href="<?php echo OMH()->dashboard_url('product-sales?id=' . $omh_product->get_id()); ?>" title="View campaign orders" data-delay="" data-toggle="tooltip">
					Sales
				</a>
			<?php endif; ?>
		</div>
		<?php endif; // End if( $row_actions ) ?>

	</td>
	<td>
		<?php // Status (Badge)
		echo $omh_product->get_product_status_badge(['placement'=>'bottom']);
		?>
	</td>
	<td>
		<?php // SKU
		echo $omh_product->get_sku(); 
		?>
	</td>		
	<td>
		<?php // Price
		echo $product_price = $omh_product->get_price() ? wc_price($omh_product->get_price()) : wc_price('0.00');
		?>
	</td>
	<td>
		<?php // Sold
		echo $total_sales;
		?>
	</td>
	<td>
		<?php // Target Quantity
		echo $omh_product->get_campaign_target_sales();
		?>
	</td>
	<?php if ($omh_user->has_role('house_admin') ) : ?>
	<td>
		<?php // Total Earnings
		echo wc_price($earning_per_product * $total_sales);
		?>
	</td>
	<?php endif; ?>
	<td>
		<?php // Date Ending
		echo date( 'M j, Y h:i:s a', strtotime( $omh_product->get_campaign_end_date() ) );
		?>
	</td>
</tr>
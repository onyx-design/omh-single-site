<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Convert this to JSON to use in JavaScript
// $omh_product_garment = $row_data->to_array();

// get sales type from class-omh-table-product-garments
$omh_product_sales_type = $this->sales_type;

if( $product_garment ):
	omh_debug($product_garment);
	$product_garment_style_id = $product_garment->get_garment_style_id();
	$garment_sizes = $product_garment->get_sizes();
	// Determine whether to output the 'add a row'
	$on_last_garment_row = false;
	// if ( $product_garment['ID'] == $this->results[count($this->results) - 1]['ID'] ) {
	// 	$on_last_garment_row = true;
	// }
?>

<tr class="omh-table-row omh-field-input omh-field-wrap omh-input-wrap" name="<?php echo $product_garment_style_id; ?>" data-omh-namespace="product_garments[<?php echo $product_garment_style_id; ?>]" data-omh-subfield-of="product_garments" data-omh-field-type="garment" >

	<input type="hidden" value="<?php echo $product_garment_style_id; ?>" name="garment_style_id" data-omh-subfield-of="product_garments[<?php echo $product_garment_style_id; ?>]" data-omh-var-type="int">

	<!-- Row Actions -->
	<td class="garment-actions">
		<div class="garment__remove omh-field-wrap">
			<input type="checkbox" id="garment__removed[<?php echo $product_garment_style_id; ?>]" class="field--template omh-field-input remove-garment" data-omh-field-type="checkbox">
			<label for="garment__removed[<?php echo $product_garment_style_id; ?>]">
				<i class="fa fa-times-circle"></i>
			</label>
		</div>
		<div class="garment__featured omh-field-wrap">
			<input type="radio" id="featured-garment[<?php echo $product_garment_style_id; ?>]" name="featured" class="field--template omh-field-input" data-omh-subfield-of="product_garments[<?php echo $product_garment_style_id; ?>]" data-omh-field-type="checkbox">
			<label for="featured-garment[<?php echo $product_garment_style_id; ?>]">
				<i class="fa fa-star"></i>
			</label>
		</div>
		<div class="garment__details">
			<i class="fa fa-info"></i>
		</div>
	</td>

	<?php if ( $this->show_garment_col ): ?>
		<!-- <td>
			<div class="garment__manufacturer"><?php //echo $product_garment['manufacturer']; ?></div>
			<div class="garment__name"><?php //echo $product_garment['name']; ?></div>
			<div class="garment__color"><?php //echo $product_garment['color']; ?></div>
		</td> -->
	<?php endif; ?>

	<?php if ( $this->show_front_image_col ): ?>
		<!-- Front Image -->
		<td class="garment__image-front">
			<div class="front-image-file omh-field-wrap omh-field--ajax-file">
				<div class="mh-file-upload-box__file">
					<input class="mh-file-upload field--template omh-field-input" type="file" id="image_front[<?php echo $product_garment_style_id; ?>]" name="image_front" data-omh-field-type="ajaxFile" data-omh-subfield-of="product_garments[<?php echo $product_garment_style_id; ?>]" />
				</div>
				<div class="mh-file-upload-box__uploading">Uploading</div>
				<div class="mh-file-upload-box__success">Done</div>
				<div class="mh-file-upload-box__error">Error</div>
			</div>
			<div class="omh-garment-name"><?php echo $product_garment->get_garment_term_name(); ?></div>
		</td>
	<?php endif; ?>

	<?php if ( $this->show_back_image_col ): ?>
		<!-- Back Image -->
		<td class="garment__image-back">
			<div class="back-image-file omh-field-wrap omh-field--ajax-file never-focused never-input never-changed orig-value initialized">
				<div class="mh-file-upload-box__file">
					<input class="mh-file-upload field--template omh-field-input" type="file" id="image_back[<?php echo $product_garment_style_id; ?>]" name="image_back" data-omh-field-type="ajaxFile" data-omh-subfield-of="product_garments[<?php echo $product_garment_style_id; ?>]" />
				</div>
				<div class="mh-file-upload-box__uploading">Uploading</div>
				<div class="mh-file-upload-box__success">Done</div>
				<div class="mh-file-upload-box__error">Error</div>
			</div>
		</td>
	<?php endif; ?>

	<?php if ( $this->show_proof_image_col ): ?>
		<!-- Proof Image -->
		<td class="garment__image-proof">
			<div class="proof-image-file omh-field-wrap omh-field--ajax-file never-focused never-input never-changed orig-value initialized">
				<div class="mh-file-upload-box__file">
					<input class="mh-file-upload field--template omh-field-input" type="file" id="image_proof[<%= garmentStyleId %>]" name="image_proof" data-omh-field-type="ajaxFile" data-omh-subfield-of="<%= garmentNamespace %>" />
				</div>
				<div class="mh-file-upload-box__uploading">Uploading</div>
				<div class="mh-file-upload-box__success">Done</div>
				<div class="mh-file-upload-box__error">Error</div>
			</div>
		</td>
	<?php endif; ?>

	<?php if ( $this->show_price_tiers_col ): ?>
		<!-- Price Tiers -->
		<?php if ('bulk' === $omh_product_sales_type):  
			$price_tiers = $product_garment->get_pricing();
			$price_tiers_qty = $product_garment->get_product()->get_product_price_tiers();
		?>
			<td class="garment__price-tiers">
				<div class="omh-table-wrap omh-table-wrap--garment-price-tiers omh-field-wrap">
					<div class="omh-table-wrap-inner">
						<table data-omh-field-type="garmentPriceTiers" data-omh-subfield-of="product_garments[<?php echo $product_garment_style_id; ?>]" name="pricing" class="table omh-table table-equal-width table--price-quantity-tiers pricing-tiers pricing-tiers--quantity  omh-field-input omh-field-advanced omh-field-advanced--price-tiers">
							<tbody>
								<tr class="omh-table-row price-tiers__header-row">
									<th>
										Tier
										<span class="product-price-tier-qty">Min Qty</span>
									</th>
									<?php foreach( $price_tiers as $price_tier => $tier_meta ): ?>
										<th class="text-center price-tier__number" data-price-tier="<?php echo $price_tier + 1; ?>">
											<?php echo $price_tier + 1; ?>
											<span class="product-price-tier-qty"><?php echo $price_tiers_qty[$price_tier]; ?></span>
										</th>
									<?php endforeach; ?>
								</tr>
								<tr class="omh-table-row price-tiers__values-row">
									<th>Price</th>
									<?php foreach( $price_tiers as $price_tier => $tier_meta ): ?>
										<td class="text-center cell--input omh-field-wrap">
											<?php 
												$pricing_tier = $price_tier + 1;

												echo OMH_HTML_Tag::factory( 
													array(
														'type'	=> 'input',
														'class'	=> 'form-control omh-table-input price-tier__quantity omh-field-input',
														'attrs'	=> array(
															'name'	=> "price_tiers[{$pricing_tier}]",
															'data-omh-field-type' => 'priceTier',
															'data-omh-subfield-of'=> "pricing[{$product_garment_style_id}]",
															'data-price-tier' => "{$pricing_tier}",
															'type'	=> 'number',
															'min'	=> 0,
															'step'	=> 0.01,
															'value'	=> $tier_meta
														)
													)
												);
											?>
										</td>
									<?php endforeach; ?>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</td>
		<?php endif; ?>

		<?php if (in_array($omh_product_sales_type, array('retail', 'campaign'))):
			$garment_base_price = $product_garment->get_base_price();
			$garment_retail_price = $product_garment->get_retail_price();
		?>
			<td>
				<?php
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_type' => 'number',
							'label' 	=> 'Base Price',
							'required' 	=> true,
							'value' => $garment_base_price
						)
					)
				?>
				<?php
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_type' => 'number',
							'label' 	=> 'Retail Price',
							'required' 	=> true,
							'value' => $garment_retail_price
						)
					)
				?>
			</td>
		<?php endif; ?>
	<?php endif; ?>

	<?php if ( $this->show_sizes_col ): ?>
		<!-- Available Sizes -->
		<td class="garment__sizes">
			<div class="omh-field-wrap omh-field-input" name="sizes" data-omh-field-type="garmentSizes" data-omh-subfield-of="product_garments[<?php echo $product_garment_style_id; ?>]" data-omh-namespace="sizes[<?php echo $product_garment->get_garment_term_id(); ?>]">
				<?php foreach( $garment_sizes as $size => $available ):
					// $size_term = get_term_by( 'slug', $size, 'pa_size' );
					// $formatted_size = str_replace( 'size-', '', $size );
					$garment_size_id = $product_garment->get_garment_term_slug() . '_size_' . $size
				?>
					<div class="form-check omh-field-wrap initialized was-validated is-valid orig-value">
						<label for="garment-<?php echo $product_garment_style_id ?>-<?php echo $size ?>" class="form-check-label">
							<?php echo $size;
								echo OMH_HTML_UI_Input::factory(
									array(
										'input_type'	=> 'checkbox',
										'class'			=> false,
										'input_id'		=> "garment-{$product_garment_style_id}-{$size}",
										'value'			=> $size,
										'label'			=> false,
										'required'		=> false,
										'input_attrs'	=> array(
											'class' 		=> 'form-check-input field--template sizes-garment__sizes initialized was-validated is-valid orig-value',
											'checked'		=> $available,
											'name'			=> "{$size}",
											'data-omh-field-type' => 'checkbox',
											'data-omh-field-type' => 'garmentSizes',
											'data-omh-subfield-of' => "sizes[{$product_garment_style_id}]",
										),
									)
								);
							?>
						</label>
					</div>
				<?php endforeach; ?>
			</div>
		</td>
	<?php endif; ?> 

</tr>
<?php endif; ?> 
<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$omh_user = wp_get_current_user();

$omh_product = $row_data;

$active_bulk_order = $omh_product->get_active_bulk_order();
if (in_array($omh_product->get_product_status(), array('processing', 'completed')) && !$active_bulk_order) {
	log_print_r("No active bulk order found for Product ID: {$omh_product->get_id()}", 'OMH Fatal Error');
}

// Determine Row Actions 
$enable_row_actions = false;
$row_action_attr 	= '';

if ($omh_user->has_role('administrator') || +$omh_product->get_product_status('step') > 1) {

	$enable_row_actions = true;

	if ('design_in_review' == $omh_product->get_product_status()) {
		$row_action_attr =  ' data-default-row-action="edit-product"';
	} else if ($omh_product->user_can_edit_bulk_order()) {
		$row_action_attr =  ' data-default-row-action="edit-order"';
	} else if (in_array($omh_product->get_product_status(), array('processing', 'completed')) && $active_bulk_order) {
		$row_action_attr =  ' data-default-row-action="view-order"';
	}
}

// $mh_product_status = $omh_product->get_mh_product_status();

// $edit_product_text = 'List';
// $edit_popover = null;

// if ('design_finalized' == $mh_product_status) {
// 	$edit_product_disabled = false;
// } else if ('active' == $mh_product_status) {
// 	$edit_product_text = 'Edit';
// 	$edit_product_disabled = false;
// } else {
// 	$edit_product_disabled = true;
// 	$edit_popover = array(
// 		'placement'	=> 'top',
// 		'content'	=> 'Your design is still being reviewed. You will be able to list it once the design has been approved, which usually takes 1-2 business days.'
// 	);
// }
?>

<tr class="omh-table-row" data-id="<?php echo $omh_product->get_id(); ?>" <?php echo $row_action_attr; ?>>
	<!-- Image -->
	<td class="td-thumbnail">
		<?php echo $omh_product->get_image(); ?>
	</td>
	<!-- Name -->
	<td class="td-primary">
		<?php echo $omh_product->get_title(); ?>

		<?php if (in_array($omh_product->get_product_status(), array('design_in_review', 'ready_to_order', 'pending'))) : ?>
			<div class="row-actions">				
				<?php if ($omh_product->user_can_edit_bulk_order()) : // Edit Bulk Order 
				?>
					<a class="row-action btn btn-sm btn-outline-primary row-action--btn row-action--show row-action--edit" data-action="edit-order" href="<?php echo OMH()->dashboard_url('edit-bulk-order?id=' . $omh_product->get_id()); ?>" title="Select your garment / size quantities and complete your order" data-toggle="tooltip">
						Checkout
					</a>
				<?php endif ?>

				<?php if ($omh_user->has_role('administrator')) : ?>
					<a class="row-action row-action--edit" data-action="edit-product" href="<?php echo OMH()->dashboard_url('edit-product?id=' . $omh_product->get_id()); ?>" title="Edit Product">
						Edit
					</a>
				<?php endif; ?>
			</div>
		<?php elseif (in_array($omh_product->get_product_status(), array('processing', 'completed')) && $active_bulk_order) : ?>
			<div class="row-actions">
				<?php if (in_array('ordered', $this->scopes) && $active_bulk_order->get_tracking_url()) : ?>
					<a class="row-action btn btn-sm btn-outline-primary row-action--btn row-action--show " data-action="track-order" href="<?php echo $active_bulk_order->get_tracking_url(); ?>" title="Track Order">
						Track
					</a>
				<?php endif; ?>
				<?php if ($omh_user->is_house_admin(false)) : ?>
					<a class="row-action row-action--view" data-action="view-order" href="<?php echo OMH()->dashboard_url('view-order?id=' . $active_bulk_order->get_id()); ?>" title="View your order" data-toggle="tooltip">
						View
					</a>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</td>

	<?php if ($this->show_status_col) : ?>
		<td>
			<?php // Status
			echo $omh_product->get_product_status_badge();
			?>
		</td>
	<?php endif; ?>
	<td>
		<?php // SKU
		echo $omh_product->get_sku();
		?>
	</td>

	<?php if (in_array('pending', $this->scopes)) : ?>
		<td>
			<?php // Date Created
			echo $omh_product->get_date_created()->date('F d, Y');
			?>
		</td>
	<?php endif; ?>

	<td>
		<?php if (in_array('ordered', $this->scopes) && $active_bulk_order) : ?>
			<?php // Date Paid
			echo $active_bulk_order->get_date_paid()->date('F d, Y');
			?>
		<?php endif; ?>
	</td>
</tr>
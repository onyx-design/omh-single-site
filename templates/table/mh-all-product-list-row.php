<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$omh_user = wp_get_current_user();

$omh_product = $row_data;



// Determine Row Actions 
$enable_row_actions = false;
$row_action_attr 	= '';

if ($omh_user->has_role('administrator') || +$omh_product->get_product_status('step') > 1) {

	$enable_row_actions = true;

	if ($omh_product->user_can_edit()) {

		$row_action_attr =  ' data-default-row-action="edit"';
	} else if (6 == $omh_product->get_product_status('step')) {
		$row_action_attr =  ' data-default-row-action="sales"';
	}
}
?>

<tr class="omh-table-row" data-id="<?php echo $omh_product->get_id(); ?>" <?php echo $row_action_attr; ?>>

	<td class="td-thumbnail">
		<?php // Image
		echo $omh_product->get_image();
		?>
	</td>
	<td class="td-primary">

		<?php // Product Name
		echo $omh_product->get_title();
		?>

		<?php if ($enable_row_actions) : ?>
			<div class="row-actions">

				<?php if ($omh_product->user_can_edit()) : // Edit Product 
				?>
					<a class="row-action row-action--edit" data-action="edit" href="<?php echo OMH()->dashboard_url('edit-product?id=' . $omh_product->get_id()); ?>" title="<?php echo ($omh_product->get_product_status() === 'ready_to_list') ? 'List' : 'Edit'; ?> product" data-toggle="tooltip">
						<?php echo ($omh_product->get_product_status() === 'ready_to_list') ? 'List' : 'Details'; ?>
					</a>
				<?php endif; ?>

				<?php if (+$omh_product->get_product_status('step') >= 3 && $omh_user->has_role('house_admin')) : // View Sales 
				?>
					<a class="row-action row-action--product-sales" data-action="sales" href="<?php echo OMH()->dashboard_url('product-sales?id=' . $omh_product->get_id()); ?>" title="View retail orders" data-delay="" data-toggle="tooltip">
						Sales
					</a>
				<?php endif; ?>

				<?php if ($omh_product->is_product_visible('link')) : ?>
					<a class="row-action row-action--view" data-action="view" href="<?php echo $omh_product->get_permalink(); ?>" target="_blank" title="View on storefront" data-toggle="tooltip">
						View
					</a>
				<?php endif; ?>
			</div>
		<?php endif; // End if( $row_actions ) 
		?>

	</td>
	<td>
		<?php // Product Type (Bulk/Retail/Campaign)
		echo ucwords($omh_product->get_sales_type());
		?>
	</td>
	<td>
		<?php // Status (Badge)
		echo $omh_product->get_product_status_badge(['placement' => 'bottom']);
		?>
	</td>
	<td>
		<?php // SKU
		echo $omh_product->get_sku();
		?>
	</td>
	<td>
		<?php // Price
		echo $omh_product->get_price() ? wc_price($omh_product->get_price()) : wc_price('0.00');
		?>
	</td>
	<td>
		<?php // Sold
		echo $omh_product->get_total_sales();
		?>
	</td>
</tr>
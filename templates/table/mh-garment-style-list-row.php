<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_garment_style = $row_data;
?>

<tr class="omh-table-row">

	<!-- Enabled -->
	<td>
		<?php 
			echo OMH_HTML_UI_Input::factory(
				array(
					'input_id'		=> 'enable_garment_style_' . $omh_garment_style->get_id(),
					'class'			=> 'enable-garment-style-checkbox',
					'input_type'	=> 'checkbox',
					'label'			=> false,
					'input_attrs'	=> array(
						'checked'		=> $omh_garment_style->get_enabled(),
						'data-object-id'=> $omh_garment_style->get_id()
					)	
				)
			); 
		?>
	</td>

	<!-- ID -->
	<td>
		<?php echo $omh_garment_style->get_id(); ?>
	</td>

	<!-- Style Full Name -->
	<td>
		<?php echo $omh_garment_style->get_style_full_name(); ?>
	</td>

	<!-- Style Slug -->
	<td>
		<?php echo $omh_garment_style->get_style_slug(); ?>
	</td>

	<!-- InkSoft Style ID -->
	<td>
		<?php echo $omh_garment_style->get_inksoft_style_id(); ?>
	</td>

	<!-- Garment Product ID -->
	<td>
		<?php echo $omh_garment_style->get_garment_product_id(); ?>
	</td>

	<!-- Color Name -->
	<td>
		<?php echo $omh_garment_style->get_garment_color() ? $omh_garment_style->get_garment_color()->get_color_name() : ''; ?>
	</td>

	<!-- Sizes -->
	<td>
		<?php echo $omh_garment_style->get_sizes(); ?>
	</td>

	<!-- Style Base Cost -->
	<td>
		<?php echo $omh_garment_style->get_style_base_cost(); ?>
	</td>

	<!-- InkSoft Image URL -->
	<td>
		<?php echo $omh_garment_style->get_inksoft_image_url(); ?>
	</td>

	<td>
		<?php echo $omh_garment_style->get_term_id(); ?>
	</td>

	<!-- Meta Created -->
	<td>
		<?php echo $omh_garment_style->get_meta_created(); ?>
	</td>

	<!-- Meta Updated -->
	<td>
		<?php echo $omh_garment_style->get_meta_updated(); ?>
	</td>
</tr>
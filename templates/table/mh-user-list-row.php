<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$user = $row_data;
$chapter = $user->get_chapter();
$allowed_roles = $this->current_user->is_administrator() ?
	OMH_User::get_allowed_roles() : array(
		'house_admin' => 'House Admin',
		// dev:activate
		'house_member' => 'House Member'
	);

// Determine user's Merch House role
$roles = $user->roles;
$user_mh_role = false;
$user_mh_role_display = null;

foreach ($allowed_roles as $mh_role => $mh_role_display) {
	if (in_array($mh_role, $roles)) {
		$user_mh_role = $mh_role;
		$user_mh_role_display = $mh_role_display;
		break;
	}
}

$row_action_attr = ' data-default-row-action="edit"';

$enable_load_data = true;
$enable_ajax_search = true;
?>

<tr class="omh-table-row" data-id="<?php echo $user->ID; ?>" <?php echo $row_action_attr; ?>>

	<td class="td-primary">
		<?php // Email
			echo $user->user_email;
		?>
		<?php if( $this->enable_row_actions ): ?>
			<div class="row-actions">
			<?php if ($this->show_upgrade_user) : ?>
				<?php if($user->is_house_member()) : ?>
					<a class="row-action row-action--change-role" href="#" title="Make House Admin" 
					onClick="changeRole(<?php echo $user->ID; ?>, 'house_admin'); return false;" 
					data-toggle="tooltip">
						Make House Admin
					</a>
				<?php endif;?>
				<?php if($user->is_house_admin()) : ?>
						<a class="row-action row-action--change-role" href="#" title="Make House Member" onClick="changeRole(<?php echo $user->ID; ?>, 'house_member'); return false;" data-toggle="tooltip">
							Make House Member
						</a>
				<?php endif;?>
			<?php endif; ?>
			<?php if($this->show_remove_user) : ?>
				<?php if($user->is_house_member()) : ?>
						<a class="row-action row-action--delete" data-action="delete" data-omh-toggle-modal="remove-house-member-modal"  title="Remove House Member" data-omh-modal-load-id="<?php echo $user->ID; ?>"  data-toggle="tooltip">
							Remove
						</a>
						<?php endif;?>
			<?php endif;?>

				<?php if ($this->show_user_role) : ?>
					<?php if(!$user->is_activated()) : ?>
						<a class="row-action row-action--activation" href="#" title="Resend User Activation Email" onClick="resendUserActivation(<?php echo $user->ID; ?>)">
							Send Activation Email
						</a>
					<?php endif;?>
				<?php endif;?>
			</div>
		<?php endif;?>
	</td>
	<td>
		<?php echo $user->first_name . ' ' . $user->last_name; ?>
	</td>

	<!-- === Role === -->
	<?php if ($this->show_user_role) : ?>
		<td>
			<?php
				if ($this->current_user->is_administrator()) {

					echo OMH_HTML_UI_Select::factory(
						array(
							'input_id'		=> 'user_role_select_' . $user->ID,
							'label'			=> false,
							'placeholder'	=> 'Select a role',
							'input_class'	=> 'omh-user-role-select',
							'value'			=> $user_mh_role,
							'select_options' => $allowed_roles
						)
					);
				} elseif ($this->current_user->is_house_admin()) {

					if ($user->is_administrator()) {
						echo $user_mh_role_display;
					} else {
						echo $user->user_email;

						echo OMH_HTML_UI_Select::factory(
							array(
								'input_id'		=> 'user_role_select_' . $user->ID,
								'label'			=> false,
								'placeholder'	=> 'Select a role',
								'input_class'	=> 'omh-user-role-select',
								'value'			=> $user_mh_role,
								'select_options' => $allowed_roles
							)
						);
					}
				} else {
					echo $user_mh_role;
				}
				?>
		</td>
	<?php endif; ?>

	<!-- === Chapter === -->
	<?php if ($this->show_user_chapter) : ?>
		<td>
			<?php
				// Get current Chapter
				$user_chapter_id_value = null;
				$user_chapter = null;

				if ($primary_chapter_id = $user->get_primary_chapter_id()) {

					$user_chapter = OMH()->chapter_factory->read($primary_chapter_id);

					$user_chapter_id_value = array(
						$primary_chapter_id => $user_chapter->get_chapter_name()
					);
				}

				if (!$user->is_administrator() || $this->current_user->is_administrator()) {

					echo OMH_HTML_UI_Select2::factory(
						array(
							'input_id'		=> 'user_chapter_id_' . $user->ID,
							'label'			=> false,
							'preselect_value' => $user_chapter_id_value,
							'placeholder'	=> 'Select a Chapter',
							'search_model'	=> 'chapters',
							'input_class'	=> 'omh-user-chapter-id'
						)
					);
				} elseif ($primary_chapter_id && $user_chapter) {
					echo $user_chapter->get_chapter_name();
				}
				?>
		</td>
	<?php endif ?>

	<!-- === Update User Role Button === -->
	<?php if ($this->show_upgrade_user) : ?>
		<td class="userRole">
			<?php echo $user_mh_role_display; ?>
		</td>
		<?php endif ?>

		<!-- === T-Shirt Size === -->
		<?php if ($this->show_t_shirt_size) : ?>
		<td>
			<?php echo get_user_meta($user->ID, 't_shirt_size', true); ?>
		</td>
	<?php endif; ?>

	<!-- === Registration Date === -->
	<?php if ($this->show_user_registration) : ?>
		<td>
			<?php echo date('M j, Y', strtotime($user->user_registered)); ?>
		</td>
	<?php endif ?>

	<!-- === Activation URL === -->
	<?php if ($this->show_activation_url) : ?>
		<td>
			<?php if ($chapter && $user->get_user_activation_key()) {

					echo OMH_HTML_UI_Input::factory(
						array(
							'size'			=> 'small',
							'input_type'	=> 'text',
							'input_id'		=> 'activation-url-' . $user->ID,
							'value'			=> OMH()->dashboard_url('activation?key=' . $user->get_user_activation_key(), $chapter->get_id()),
							'contents'		=> array(
								'input'			=> array(
									'attrs'			=> array(
										'readonly'	=> true
									)
								)
							),
							'append'		=> array(
								OMH_HTML_UI_Button::factory(
									array(
										'size'	=> 'small',
										'color'	=> 'secondary',
										'class'	=> 'copy-to-clipboard',
										'label'	=> 'Copy',
										'attrs'		=> array(
											'data-clipboard-target'	=> '#activation-url-' . $user->ID
										)
									)
								)
							)
						)
					);
				} ?>
		</td>
	<?php endif ?>
</tr>
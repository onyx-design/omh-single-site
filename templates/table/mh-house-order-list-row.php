<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_order = $row_data;
?>

<tr class="omh-table-row" data-id="<?php echo $omh_order->get_id(); ?>">

	<!-- Actions -->
	<td data-col="actions">
		<a class="table-action table-action--view" href="<?php echo OMH()->dashboard_url( 'view-order?id=' . $omh_order->get_id() ); ?>" title="View Order Details">
			<i class="fa fa-eye"></i>
		</a>
	</td>

	<!-- Order -->
	<td>
		#<?php echo $omh_order->get_order_number(); ?>
	</td>

	<!-- Date -->
	<td>
		<?php echo wc_format_datetime( $omh_order->get_date_created() ); ?>
	</td>
	
	<!-- User Info -->
	<td>
		<?php echo $omh_order->get_formatted_billing_full_name(); ?>
		<div class="text-muted">
			<?php echo $omh_order->get_billing_email(); ?>
		</div>
	</td>

	<!-- User Role -->
	<?php if ( $this->show_user_role_col ): ?>
		<td>
			<?php 
				echo $omh_order->get_user_role_badge(); 
			?>
		</td>
	<?php endif ?>

	<!-- Status -->
	<td>
		<?php 
			$badge_contents = $omh_order->get_order_status_badge();
			echo OMH_HTML_Tag::factory(
				array (
					'type'		=> 'OMH_HTML_UI_Badge',
					'label' 	=> $badge_contents['text'],
					'color' 	=> $badge_contents['color']
				)
			);
		?>
	</td>

	<!-- Earnings -->
	<td>
		<?php echo wc_price( $omh_order->get_chapter_earnings( $this->chapter ?? OMH_Session::get_chapter() ) ); ?>
	</td>

</tr>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! $messages ) {
	return;
}

$notice_map = array(
	'error'		=> 'danger',
	'notice'	=> 'info',
	'success'	=> 'success'
);

$notice_type = str_replace( array( 'notices/', '.php' ), '', $template_name );
?>

<?php
foreach ( $messages as $message ) {

		// Remove the "Have a coupon?" notice
		// if( 0 === strpos( $message, 'Have a coupon?' ) ) {
		// 	continue;
		// }

		$dom_document = new DOMDocument();
		$dom_document->loadHTML( $message );
		$dom_selector = new DOMXPath($dom_document);

		$dom_anchor = $dom_selector->query( '//a' );

		if( $dom_anchor ) {

			$found_anchors = array();

			foreach( $dom_anchor as $dom_node ) {
				$found_anchors[] = $dom_node->textContent;
			}

			$message = trim( str_replace( $found_anchors, '', strip_tags( $message ) ) ) . ' ';

			foreach( $dom_anchor as $dom_node ) {

				if( $dom_node->textContent ) {

					$name = str_replace( '-', '_', sanitize_title( $dom_node->textContent ) );
					$classes = str_replace( array( 'button', 'btn' ), '', $dom_node->getAttribute( 'class' ) );
					// $href = $dom_node->getAttribute( 'href' ) !== '#' ? 

					switch( $notice_map[ $notice_type ] ) {
						case 'info':
							$color = 'secondary';
							break;
						case 'danger':
							$color = 'danger';
							break;
						case 'success':
						default:
							$color = 'primary';
							break;
					}

					// $message .= (string) OMH_HTML_Tag::factory(
					// 	array(
					// 		'contents'	=> array(
					// 			$name 		=> OMH_HTML_UI_Button::factory(
					// 				array(
					// 					'href'		=> $dom_node->getAttribute( 'href' ) ,
					// 					'color'		=> $color,
					// 					'label'		=> $dom_node->textContent,
					// 					'class'		=> $classes,
					// 					'size'		=> 'small'
					// 				)
					// 			)
					// 		)
					// 	)
					// );

					$message .= OMH_HTML_UI_Button::factory(
							array(
								'href'		=> $dom_node->getAttribute( 'href' ),
								'color'		=> $color,
								'label'		=> $dom_node->textContent,
								'class'		=> $classes,
								'size'		=> 'small'
							)
						);
				}
			}
		}

		echo OMH_HTML_UI_Alert::factory(
			array(
				'color'	=> $notice_map[ $notice_type ],
				'text'	=> wp_kses_post( $message )
			)
		);
} 
?>
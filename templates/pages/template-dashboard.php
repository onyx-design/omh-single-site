<?php 
/**
 * Template Name: Dashboard 
 */
get_header(); 

global $post;

$enable_ajax_search = true;

// Set user to current user if not already set
$omh_user = wp_get_current_user();
?>

<div class="mh-dashboard-content">
	<div id="mh-dashboard-save-bar" class="onyx-drawer onyx-drawer--top onyx-drawer--save-bar onyx-drawer--no-close">
		<div class="fill-parent-rel dashboard-drawer-padding">
			<div class="container">
				<div class="row site-nav-height align-items-center">
					<div class="col-sm-12 text-right">
						<span class="mh-screen-form-msgs"></span>
						<?php
							echo OMH_HTML_UI_Button::factory( 
								array(
									'label' 	=> 'Reset',
									'onclick' 	=> 'omh.Forms.reset();',
									'class'		=> 'ml-3 mh-form-reset',
									'attrs'		=> array(
										'data-reset-form'	=> 'mh-screen-form'
									)
								)
							);

							echo OMH_HTML_UI_Button::factory( 
								array(
									'label' 	=> 'Save',
									'color' 	=> 'primary',
									'form'		=> 'mh-screen-form',
									'value' 	=> 'submit',
									'class' 	=> 'ml-3 mh-forms-submit'
								)
							);
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="mh-dashboard-content" class="container-wrap container-fluid">
		<?php
			include( OMH()->dashboard_template_path( 'dashboard-drawer' ) );

			$template = 'screens/screen-' . $post->post_name;

			if( OMH()->dashboard_template_exists( $template ) ) {
				include( OMH()->dashboard_template_path( $template ) );
			} else {
				echo 'Template does not exist!';
			}
		?>
	</div>
</div>

<script type="text/javascript">
	var omh_security = '<?php echo wp_create_nonce( 'change-chapter' ); ?>';

	jQuery(document).on('omhloaded', function() {
		var $ = jQuery;
		var sessionChapterSelect = $('#session_chapter_id');

		if( sessionChapterSelect.length ) {
			sessionChapterSelect.OMHFormField();

			var $sessionChapterField = sessionChapterSelect.get(0).omhFormField;

			$sessionChapterField.$input.on('select2:select', function(event) {
				selectedChapter = event.params.data;

				omh.ajax(
					'omh_change_chapter',
					{ 
						page: '<?php echo get_query_var( 'pagename' ) ?>',
						chapterId: selectedChapter.id
					},
					null,
					omh_security
				);

				// document.location.replace( document.location.protocol + '//' + document.location.hostname + document.location.pathname + '?ch=' + selectedChapter.id );
			});
		}
	});
</script>

<?php get_footer(); ?>
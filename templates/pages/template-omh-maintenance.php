<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Merch House - Temporary Site Maintenance</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:500,700&display=swap" rel="stylesheet">
    <style>
        html,
        body {
            width: 100%;
            height: 100%;
            overflow: hidden;
            padding: 0;
            font-family: 'Poppins', sans-serif;
            color: #6c6c6c;
            text-align: center;
        }

        .content-wrap {
            width: 300px;
            display: block;
            margin: 0 auto;
            height: 100%;
            position: relative;
            overflow:auto;
        }

        .content-wrap * {
            max-width: 100%;
        }

        h1 {
            font-size: 24px;
            text-align: center;
        }

        h2 {
            font-size: 28px;
            text-align: center;
        }

        a {
            color: #58C3B7;
        }

        .content-wrap-inner {
            position: absolute;
            top: 50%;
            -webkit-transform: translateY(-50%);
            -ms-transform: translateY(-50%);
            transform: translateY(-50%);
        }
    </style>
</head>

<body>
    <div class="content-wrap">
        <div class="content-wrap-inner">
            <img class="logo" src="https://merch.house/wp-content/uploads/2019/09/Merch_House_Logo-MH_Full-Black_Color-600px.png" title="Merch House Logo" />
            <h1>Sorry, we're temporarily down for maintenance.</h1>
            <h2>New improvements coming soon!</h2>
            <p>Please text <a href="tel:+12138801378">(213) 880-1378</a> or email <a href="mailto:info@merch.house">info@merch.house</a> for help.</p>
        </div>

    </div>
</body>

</html>
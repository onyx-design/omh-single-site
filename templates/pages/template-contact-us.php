<?php 
/*template name: Contact Us */

$omh_page_title = 'Contact Us';

$page_container = true;

get_header(); ?>

<div class="mh-dashboard-content">
	<div id="mh-dashboard-save-bar" class="onyx-drawer onyx-drawer--top onyx-drawer--save-bar onyx-drawer--no-close">
		<div class="fill-parent-rel dashboard-drawer-padding">
			<div class="container">
				<div class="row site-nav-height align-items-center">
					<div class="col-sm-12 text-right">
						<span class="mh-screen-form-msgs"></span>
						<?php
							echo OMH_HTML_UI_Button::factory( 
								array(
									'label' 	=> 'Reset',
									'onclick' 	=> 'omh.Forms.reset();',
									'class'		=> 'ml-3 mh-form-reset',
									'attrs'		=> array(
										'data-reset-form'	=> 'mh-screen-form'
									)
								)
							);

							echo OMH_HTML_UI_Button::factory( 
								array(
									'label' 	=> 'Save',
									'color' 	=> 'primary',
									'form'		=> 'mh-screen-form',
									'value' 	=> 'submit',
									'class' 	=> 'ml-3 mh-forms-submit'
								)
							);
						?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="mh-dashboard-content" class="container-wrap container-fluid">
		<div id="mh-dashboard-wrap">
			<div class="container">
				<div id="mh-dashboard" class="row">
					<div class="col">

						<?php
							include( OMH()->dashboard_template_path( 'dashboard-header' ) );
						?>
						<form id="mh-screen-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="contact_us_form" data-form-nonce="<?php echo wp_create_nonce( 'contact-us-form' ); ?>">
							<div class="row">
								<div class="col-md-3">
									312 W. 5th St #M9 Los Angeles, CA 90013 <a href="mailto:info@merch.house">info@merch.house</a>
								</div>

								<div class="col-md-9">
									<div class="card shadow-sm">
										<div class="card-body">
											<div class="row">
												<div class="col-md-12">
													<?php
														echo OMH_HTML_UI_Input::factory(
															array(
																'input_id'		=> 'name',
																'label'			=> 'Name',
																'required'		=> true,
																'form_text'		=> '0 of 40 max characters'
															)
														); 
													?>
												</div>
												<div class="col-md-12">
													<?php
														echo OMH_HTML_UI_Input::factory(
															array(
																'input_id'		=> 'email',
																'label'			=> 'Email',
																'required'		=> true
															)
														); 
													?>
												</div>
												<div class="col-md-12">
													<?php
														echo OMH_HTML_UI_Input::factory(
															array(
																'input_id'		=> 'subject',
																'label'			=> 'Subject',
																'required'		=> true,
																'form_text'		=> '0 of 40 max characters'
															)
														); 
													?>
												</div>
												<div class="col-md-12">
													<?php
														echo OMH_HTML_UI_Textarea::factory(
															array(
																'input_id'		=> 'message',
																'label'			=> 'Message',
																'required'		=> true,
																'contents'		=> array(
																	'input'			=> array(
																		'attrs'			=> array(
																			'rows'		=> 10,
																			'cols'		=> 50
																		)
																	)
																)
															)
														);
													?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>

						<div class="row">
							<div class="col">
								<hr>
							</div>
						</div>

						<div class="row">
							<div class="col text-right">
								<?php  
									echo OMH_HTML_UI_Button::factory(
										array(
											'color'		=> 'primary',
											'value'		=> 'submit',
											'form'		=> 'mh-screen-form',
											'label'		=> 'Submit',
											'class' 	=> 'mh-form-submit'
										)
									);
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
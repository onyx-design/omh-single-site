<?php
/**
 * Template Name: Sandbox
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set("max_execution_time", 600 );
ini_set("log_errors", 1); 

// Easy Line breaks and text output
define( 'BR', "<br />");
$br = "<br />";
if( !function_exists('br') ) {function br($msg='') {echo ( is_scalar( $msg ) ? ( is_bool( $msg ) ? boolstr( $msg )."<br />" : "$msg<br />" ) : print_r( $msg, true ) );}}
if( !function_exists('boolstr') ) {function boolstr($b) {return $b ? 'true' : 'false';}}
?>

<?php get_header(); ?>
<code style="white-space:pre;">
<?php 
	global $wpdb;

	// $inskoft_product = OMH_Model_Garment_Product::get_from_inksoft( 1 );

	// br( $inskoft_product );

	// br( OMH_Model_Garment_Brand::get_inksoft_brands() );

	// $user = get_user_by( 'id', 1 );

	// br( $user );

	// br( OMH()->dashboard_url( 'activation?key=', 4275 ) );

	// $user = get_user_by( 'ID', 2 );

	// $user->add_role( 'house_member' );

	// br( $user );

	// $user_query = new WP_User_Query( array( 'role__in' => array( 'customer', 'house_member' ) ) );

	// br( $user_query->get_results() );

	// $chapter = OMH()->session->get_chapter();

	// br( $chapter->get_available_store_credit( false ) );

	// $product = wc_get_product( 627 );

	// foreach( $product->get_garments() as $garment ) {
	// 	br( $garment->get_garment_style() );
	// 	br( $garment->get_garment_style()->get_garment_product() );
	// }

	// $chapters = OMH()->chapter_factory->query(
	// 	array(
	// 		'posts_per_page'	=> -1,
	// 		'search'			=> 'tke'
	// 	)
	// );

	// br( array_map( function( $chapter ) {
	// 	return $chapter->get_term_id();
	// }, $chapters ) );

	// $color_array = array(
	// 	'AQUA'			=> 	'AQ',
	// 	'White'			=> 	'WT',
	// 	'Stone'			=> 	'ST',
	// 	'Grey'			=> 	'GR',
	// 	'Ash'			=> 	'ASH',
	// 	'Berry'			=> 	'BRY',
	// 	'Black'			=> 	'BLK',
	// 	'OLIVE'			=> 	'OLV',
	// 	'Red'			=> 	'RED',
	// 	'Sky'			=> 	'SKY',
	// 	'TAN'			=> 	'TAN',
	// 	'Yam'			=> 	'YAM',
	// 	'Navy'			=> 	'NVY',
	// 	'Orange'			=> 	'ORG',
	// 	'Eco Grey'			=> 	'EGR',
	// 	'PINK'			=> 	'PNK',
	// 	'Denim'			=> 	'DNM',
	// 	'Forest'			=> 	'FST',
	// 	'Maroon'			=> 	'MRN',
	// 	'ARMY'			=> 	'ARMY',
	// 	'Camo'			=> 	'CAMO',
	// 	'CARDINAL'			=> 	'CARD',
	// 	'Charcoal'			=> 	'CHAR',
	// 	'Eco Black'			=> 	'EBLK',
	// 	'Envy'			=> 	'ENVY',
	// 	'Gold'			=> 	'GOLD',
	// 	'Hemp'			=> 	'HEMP',
	// 	'Kiwi'			=> 	'KIWI',
	// 	'LEAF'			=> 	'LEAF',
	// 	'Lime'			=> 	'LIME',
	// 	'MINT'			=> 	'MINT',
	// 	'Plum'			=> 	'PLUM',
	// 	'Royal'			=> 	'ROYL',
	// 	'RUST'			=> 	'RUST',
	// 	'Sage'			=> 	'SAGE',
	// 	'Sand'			=> 	'SAND',
	// 	'Silk'			=> 	'SILK',
	// 	'TEAL'			=> 	'TEAL',
	// 	'DARK GREY'			=> 	'DKGR',
	// 	'Light Grey'			=> 	'LTGR',
	// 	'Purple'			=> 	'PURP',
	// 	'SILVER'			=> 	'SILV',
	// 	'Stone Grey'			=> 	'STGR',
	// 	'Chocolate'			=> 	'CHOC',
	// 	'AQUA TRIBLEND'			=> 	'AQTRI',
	// 	'Black Aqua'			=> 	'BLKAQ',
	// 	'BLACK WHITE'			=> 	'BLKWT',
	// 	'Brick'			=> 	'BRICK',
	// 	'BROWN'			=> 	'BROWN',
	// 	'Chili'			=> 	'CHILI',
	// 	'CORAL'			=> 	'CORAL',
	// 	'Cream'			=> 	'CREAM',
	// 	'CREME'			=> 	'CREME',
	// 	'Daisy'			=> 	'DAISY',
	// 	'Dark Green'			=> 	'DKGRN',
	// 	'Dark Heather'			=> 	'DKHTR',
	// 	'Grass'			=> 	'GRASS',
	// 	'HEATHER AQUA'			=> 	'HTRAQ',
	// 	'Heather White'			=> 	'HTRWT',
	// 	'Kelly'			=> 	'KELLY',
	// 	'Khaki'			=> 	'KHAKI',
	// 	'LAPIS'			=> 	'LAPIS',
	// 	'Light Green'			=> 	'LTGRN',
	// 	'Light Olive'			=> 	'LTOLV',
	// 	'Lilac'			=> 	'LILAC',
	// 	'Mauve'			=> 	'MAUVE',
	// 	'Melon'			=> 	'MELON',
	// 	'PEACH'			=> 	'PEACH',
	// 	'Red White'			=> 	'REDWT',
	// 	'SLATE'			=> 	'SLATE',
	// 	'STORM'			=> 	'STORM',
	// 	'Tweed'			=> 	'TWEED',
	// 	'White Black'			=> 	'WTBLK',
	// 	'WHITE MARBLE'			=> 	'WTMBL',
	// 	'White Red'			=> 	'WTRED',
	// 	'Classic Navy'			=> 	'CLNVY',
	// 	'Dark Navy'			=> 	'DKNVY',
	// 	'J. Navy'			=> 	'JNVY',
	// 	'Light Blue'			=> 	'LTBLU',
	// 	'NAVY WHITE'			=> 	'NVYWT',
	// 	'White Navy'			=> 	'WTNVY',
	// 	'Black Stone'			=> 	'BLKST',
	// 	'HEATHER STONE'			=> 	'HTRST',
	// 	'Olive Stone'			=> 	'OLVST',
	// 	'S. Orange'			=> 	'SORG',
	// 	'STONE MARBLE'			=> 	'STMBL',
	// 	'Navy Stone'			=> 	'NVYST',
	// 	'Black Grey'			=> 	'BLKGR',
	// 	'DEEP HEATHER'			=> 	'DPHTR',
	// 	'Deep Red'			=> 	'DPRED',
	// 	'Digital Grey'			=> 	'DIGGR',
	// 	'Grey Heather'			=> 	'GRHTR',
	// 	'Grey Triblend'			=> 	'GRTRI',
	// 	'HEATHER GREY'			=> 	'HTRGR',
	// 	'Ice Grey'			=> 	'ICEGR',
	// 	'Vintage Grey'			=> 	'VINGR',
	// 	'Classic Pink'			=> 	'CLPNK',
	// 	'Light Pink'			=> 	'LTPNK',
	// 	'White Pink'			=> 	'WTPNK',
	// 	'WHITE DENIM'			=> 	'WTDNM',
	// 	'Deep Forest'			=> 	'DPFST',
	// 	'Maroon White'			=> 	'MRNWT',
	// 	'WHITE MAROON'			=> 	'WTMRN',
	// 	'ATHLETIC HEATHER'			=> 	'ATHHTR',
	// 	'Azalea'			=> 	'AZALEA',
	// 	'BERRY TRIBLEND'			=> 	'BRYTRI',
	// 	'BLACK HEATHER'			=> 	'BLKHTR',
	// 	'BLACK MARBLE'			=> 	'BLKMBL',
	// 	'BLACK RED'			=> 	'BLKRED',
	// 	'Blackberry'			=> 	'BLKBRY',
	// 	'Butter'			=> 	'BUTTER',
	// 	'Citrus'			=> 	'CITRUS',
	// 	'Cobalt'			=> 	'COBALT',
	// 	'Digital Black'			=> 	'DIGBLK',
	// 	'Garnet'			=> 	'GARNET',
	// 	'Gravel'			=> 	'GRAVEL',
	// 	'GREEN TRIBLEND'			=> 	'GRNTRI',
	// 	'Heather Black'			=> 	'HTRBLK',
	// 	'HEATHER GREEN'			=> 	'HTRGRN',
	// 	'HEATHER OLIVE'			=> 	'HTROLV',
	// 	'Heather Red'			=> 	'HTRRED',
	// 	'HEATHER TAN'			=> 	'HTRTAN',
	// 	'Indigo'			=> 	'INDIGO',
	// 	'Lagoon'			=> 	'LAGOON',
	// 	'Meadow'			=> 	'MEADOW',
	// 	'OLIVE TRIBLEND'			=> 	'OLVTRI',
	// 	'Orchid'			=> 	'ORCHID',
	// 	'Oxford'			=> 	'OXFORD',
	// 	'Pepper'			=> 	'PEPPER',
	// 	'Red Marble'			=> 	'REDMBL',
	// 	'Red Triblend'			=> 	'REDTRI',
	// 	'Royal White'			=> 	'ROYLWT',
	// 	'Russet'			=> 	'RUSSET',
	// 	'Spruce'			=> 	'SPRUCE',
	// 	'SUNSET'			=> 	'SUNSET',
	// 	'True Red'			=> 	'TRURED',
	// 	'VINTAGE BLACK'			=> 	'VINBLK',
	// 	'Vintage Red'			=> 	'VINRED',
	// 	'Violet'			=> 	'VIOLET',
	// 	'White Royal'			=> 	'WTROYL',
	// 	'White Slub'			=> 	'WTSLUB',
	// 	'YELLOW'			=> 	'YELLOW',
	// 	'Blue Marble'			=> 	'BLUMBL',
	// 	'Digital Blue'			=> 	'DIGBLU',
	// 	'Flo Blue'			=> 	'FLOBLU',
	// 	'HEATHER BLUE'			=> 	'HTRBLU',
	// 	'HEATHER NAVY'			=> 	'HTRNVY',
	// 	'Ice Blue'			=> 	'ICEBLU',
	// 	'NAVY MARBLE'			=> 	'NVYMBL',
	// 	'Navy Red'			=> 	'NVYRED',
	// 	'NAVY TRIBLEND'			=> 	'NVYTRI',
	// 	'Sky Blue'			=> 	'SKYBLU',
	// 	'True Navy'			=> 	'TRUNVY',
	// 	'Vintage Navy'			=> 	'VINNVY',
	// 	'Antique Orange'			=> 	'ANTORG',
	// 	'HEATHER ORANGE'			=> 	'HTRORG',
	// 	'ORANGE TRIBLEND'			=> 	'ORGTRI',
	// 	'Sandstone'			=> 	'SANDST',
	// 	'Charcoal Grey'			=> 	'CHARGR',
	// 	'Deep Royal'			=> 	'DPROYL',
	// 	'DEEP TEAL'			=> 	'DPTEAL',
	// 	'Iron Grey'			=> 	'IRONGR',
	// 	'Purple White'			=> 	'PURPWT',
	// 	'Warm Grey'			=> 	'WARMGR',
	// 	'WHITE DARK GREY'			=> 	'WTDKGR',
	// 	'Black Dk Grey'			=> 	'BLDKGR',
	// 	'Deep Purple'			=> 	'DPPURP',
	// 	'Silver Grey'			=> 	'SILVGR',
	// 	'Dark Chocolate'			=> 	'DKCHOC',
	// 	'Heather Pink'			=> 	'HTRPNK',
	// 	'Hot Pink'			=> 	'HOTPNK',
	// 	'DENIM TRIBLEND'			=> 	'DNMTRI',
	// 	'Vintage Denim'			=> 	'VINDNM',
	// 	'Forest Green'			=> 	'FSTGRN',
	// 	'FOREST MARBLE'			=> 	'FSTMBL',
	// 	'HEATHER FOREST'			=> 	'HTRFST',
	// 	'HEATHER MAROON'			=> 	'HTRMRN',
	// 	'MAROON MARBLE'			=> 	'MRNMBL',
	// 	'MAROON TRIBLEND'			=> 	'MRNTRI',
	// 	'Military Green'			=> 	'MILGRN',
	// 	'Army Heather'			=> 	'ARMYHTR',
	// 	'ASPHALT'			=> 	'ASPHALT',
	// 	'Black Slub'			=> 	'BLKSLUB',
	// 	'Black Teal'			=> 	'BLKTEAL',
	// 	'Blossom'			=> 	'BLOSSOM',
	// 	'Cardinal Red'			=> 	'CARDRED',
	// 	'Charcoal Heather'			=> 	'CHARHTR',
	// 	'CHARCOAL MARBLE'			=> 	'CHARMBL',
	// 	'Charcoal Triblend'			=> 	'CHARTRI',
	// 	'CLAY TRIBLEND'			=> 	'CLAYTRI',
	// 	'CRANBERRY'			=> 	'CRANBRY',
	// 	'Crimson'			=> 	'CRIMSON',
	// 	'Currant'			=> 	'CURRANT',
	// 	'Eco True Black'			=> 	'ETRUBLK',
	// 	'Eco True Green'			=> 	'ETRUGRN',
	// 	'Eco True Red'			=> 	'ETRURED',
	// 	'EVERGREEN'			=> 	'EVERGRN',
	// 	'FUCHSIA'			=> 	'FUCHSIA',
	// 	'Green Camo'			=> 	'GRNCAMO',
	// 	'HEATHER CARDINAL'			=> 	'HTRCARD',
	// 	'HEATHER CLAY'			=> 	'HTRCLAY',
	// 	'Kelly White'			=> 	'KELLYWT',
	// 	'Light Steel'			=> 	'LTSTEEL',
	// 	'Mint Green'			=> 	'MINTGRN',
	// 	'MINT TRIBLEND'			=> 	'MINTTRI',
	// 	'Mustard'			=> 	'MUSTARD',
	// 	'NATURAL'			=> 	'NATURAL',
	// 	'Neon Green'			=> 	'NEONGRN',
	// 	'Old Gold'			=> 	'OLDGOLD',
	// 	'OLIVE SLUB'			=> 	'OLVSLUB',
	// 	'Peacock'			=> 	'PEACOCK',
	// 	'Raspberry'			=> 	'RASPBRY',
	// 	'Redwood'			=> 	'REDWOOD',
	// 	'Royal Heather'			=> 	'ROYLHTR',
	// 	'Safety Green'			=> 	'SAFEGRN',
	// 	'Scarlet'			=> 	'SCARLET',
	// 	'Seafoam'			=> 	'SEAFOAM',
	// 	'Tan Camo'			=> 	'TANCAMO',
	// 	'TEAL TRIBLEND'			=> 	'TEALTRI',
	// 	'TRUE ROYAL'			=> 	'TRUROYL',
	// 	'Turf Green'			=> 	'TURFGRN',
	// 	'Vintage Gold'			=> 	'VINGOLD',
	// 	'Vintage Royal'			=> 	'VINROYL',
	// 	'WHITE KELLY'			=> 	'WTKELLY',
	// 	'Wildberry'			=> 	'WILDBRY',
	// 	'BABY BLUE'			=> 	'BABYBLU',
	// 	'Blue Jean'			=> 	'BLUJEAN',
	// 	'Cool Blue'			=> 	'COOLBLU',
	// 	'Eco True Navy'			=> 	'ETRUNVY',
	// 	'Fair Blue'			=> 	'FAIRBLU',
	// 	'Maui Blue'			=> 	'MAUIBLU',
	// 	'NAVY SLUB'			=> 	'NVYSLUB',
	// 	'NEON BLUE'			=> 	'NEONBLU',
	// 	'ROYAL BLUE'			=> 	'ROYLBLU',
	// 	'Brown Stone'			=> 	'BROWNST',
	// 	'Eco True Orange'			=> 	'ETRUORG',
	// 	'Neon Orange'			=> 	'NEONORG',
	// 	'Royal Orange'			=> 	'ROYLORG',
	// 	'Safety Orange'			=> 	'SAFEORG',
	// 	'Tennessee Orange'			=> 	'TENNORG',
	// 	'Heather Galapagos Blue'			=> 	'HTRGBLU',
	// 	'Black Purple'			=> 	'BLKPURP',
	// 	'Black Silver'			=> 	'BLKSILV',
	// 	'CANVAS RED'			=> 	'CANVRED',
	// 	'Cherry Red'			=> 	'CHERRED',
	// 	'Chrome Black'			=> 	'CHRMBLK',
	// 	'Dark Grey Marble'			=> 	'DKGRMBL',
	// 	'Dk Grey Black'			=> 	'DKGRBLK',
	// 	'Heather Dark Grey'			=> 	'HTRDKGR',
	// 	'Light Grey Marble'			=> 	'LTGRMBL',
	// 	'NEW SILVER'			=> 	'NEWSILV',
	// 	'Purple Berry'			=> 	'PURPBRY',
	// 	'PURPLE TRIBLEND'			=> 	'PURPTRI',
	// 	'Sport Grey'			=> 	'SPORTGR',
	// 	'Urban Grey'			=> 	'URBANGR',
	// 	'Vintage Purple'			=> 	'VINPURP',
	// 	'Tan Chocolate'			=> 	'TANCHOC',
	// 	'Neon Pink'			=> 	'NEONPNK',
	// 	'Safety Pink'			=> 	'SAFEPNK',
	// 	'SOFT PINK'			=> 	'SOFTPNK',
	// 	'DENIM SLUB'			=> 	'DNMSLUB',
	// 	'Heliconia'			=> 	'HELICON',
	// 	'Lagoon Blue'			=> 	'LAGONBL',
	// 	'Emerald Heather'			=> 	'EMERHTR',
	// 	'EMERALD TRIBLEND'			=> 	'EMERTRI',
	// 	'Apple Green'			=> 	'APPLEGRN',
	// 	'Athletic Heather White'			=> 	'ATHHTRWT',
	// 	'BROWN TRIBLEND'			=> 	'BROWNTRI',
	// 	'Chambray'			=> 	'CHAMBRAY',
	// 	'Cornsilk'			=> 	'CORNSILK',
	// 	'DARK GRY HEATHER'			=> 	'DKGRYHTR',
	// 	'Eco True Gold'			=> 	'ETRUGOLD',
	// 	'Espresso'			=> 	'ESPRESSO',
	// 	'HEATHER BROWN'			=> 	'HTRBROWN',
	// 	'HEATHER PEACH'			=> 	'HTRPEACH',
	// 	'HEATHER SLATE'			=> 	'HTRSLATE',
	// 	'Irish Green'			=> 	'IRISHGRN',
	// 	'Jade Dome'			=> 	'JADEDOME',
	// 	'Lavender'			=> 	'LAVENDER',
	// 	'MAUVE TRIBLEND'			=> 	'MAUVETRI',
	// 	'PEACH TRIBLEND'			=> 	'PEACHTRI',
	// 	'Royal Camo'			=> 	'ROYLCAMO',
	// 	'Sapphire'			=> 	'SAPPHIRE',
	// 	'Snow Camo'			=> 	'SNOWCAMO',
	// 	'STORM TRIBLEND'			=> 	'STORMTRI',
	// 	'White Athletic Heather'			=> 	'WTATHHTR',
	// 	'WHITE TR ROYAL'			=> 	'WTTRROYL',
	// 	'Bondi Blue'			=> 	'BONDIBLU',
	// 	'Classic Navy Heather'			=> 	'CLNVYHTR',
	// 	'OCEAN BLUE'			=> 	'OCEANBLU',
	// 	'Photo Blue'			=> 	'PHOTOBLU',
	// 	'Power Blue'			=> 	'POWERBLU',
	// 	'Texas Orange'			=> 	'TEXASORG',
	// 	'Red Navy Tie-Dye'			=> 	'REDNVYTD',
	// 	'Wildberry Very Berry Tie-Dye'			=> 	'WLDBRYTD',
	// 	'Atomic Orange Cosmic Pink Tie-D'			=> 	'ATMORGTD',
	// 	'Athletic Heather Black'			=> 	'ATHHTRBL',
	// 	'Black Athletic Heather'			=> 	'BLATHHTR',
	// 	'BLK HEATHER BLK'			=> 	'BLHTRBLK',
	// 	'Coral Silk'			=> 	'CORLSILK',
	// 	'Crunchberry'			=> 	'CRUCHBRY',
	// 	'Black Deep Heather'			=> 	'BLKDPHTR',
	// 	'Deep Heather Black'			=> 	'DPHTRBLK',
	// 	'Grey Heather Triblend'			=> 	'GRHTRTRI',
	// 	'Heather Grey Red'			=> 	'HTRGRRED',
	// 	'Oxford Gray'			=> 	'OXFORDGR',
	// 	'Purple Rush'			=> 	'PURPRUSH',
	// 	'TEAM PURPLE'			=> 	'TEAMPURP',
	// 	'WHITE DEEP HTHR'			=> 	'WTDPHTHR',
	// 	'Deep Heather Navy'			=> 	'DPHTRNVY',
	// 	'GREY NAVY TRB'			=> 	'GRNVYTRB',
	// 	'Heather Grey Navy'			=> 	'HTRGRNVY',
	// 	'Cyber Pink'			=> 	'CYBERPNK',
	// 	'Dusty Plum'			=> 	'DUSTPLUM',
	// 	'HEATHER SEA GRN'			=> 	'HTRSEGRN',
	// 	'Macchiato'			=> 	'MACHIATO',
	// 	'SOFT CREAM'			=> 	'SFTCREAM',
	// 	'Tangerine'			=> 	'TANGERIN',
	// 	'TURQUOISE'			=> 	'TURQUOIS',
	// 	'Vintage Heather Red'			=> 	'VINHTRED',
	// 	'WHITE ASPHALT'			=> 	'WTASPHAL',
	// 	'White True Royal'			=> 	'WTTRUROY',
	// 	'Athletic Heather Navy'			=> 	'ATHHTRNV',
	// 	'Bluespruce'			=> 	'BLUSPRUC',
	// 	'Gym Blue Black'			=> 	'GYMBLUBL',
	// 	'HEATHER ICE BLUE'			=> 	'HTRICEBL',
	// 	'Indigo Blue'			=> 	'INDIGOBL',
	// 	'Mystic Blue'			=> 	'MYSTICBL',
	// 	'Navy Heather Triblend'			=> 	'NVYHTRTR',
	// 	'Tahiti Blue'			=> 	'TAHITIBL',
	// 	'Vintage Heather Navy'			=> 	'VINHTRNV',
	// 	'White Baby Blue'			=> 	'WTBABYBL',
	// 	'WHITE NEON BLUE'			=> 	'WTNEONBL',
	// 	'Stone Navy Stitch'			=> 	'STNVYSTI',
	// 	'BLACK DEEP HTHR'			=> 	'BLKDPHTH',
	// 	'Heather Deep Teal'			=> 	'HTRDPTEA',
	// 	'Heather Grey Royal'			=> 	'HTRGRROY',
	// 	'Heather Grey Purple'			=> 	'HTRGRPUR',
	// 	'WHITE NEON PINK'			=> 	'WTNEONPN',
	// 	'Black Glitter'			=> 	'BLKGLITT',
	// 	'Black Neon Green'			=> 	'BLKNNGRN',
	// 	'BLACK TRUE ROYL'			=> 	'BLKTRURO',
	// 	'Chalky Mint'			=> 	'CHKYMINT',
	// 	'Power Red Black'			=> 	'PWREDBLK',
	// 	'SOLID BLK BLEND'			=> 	'SLDBLKBD',
	// 	'SOLID WHT BLEND'			=> 	'SLDWHTBD',
	// 	'Charcoal Black Triblend'			=> 	'CHARBLTR',
	// 	'GRY MAROON TRB'			=> 	'GRYMRNTR',
	// 	'ICE BLUE TRIBLND'			=> 	'ICEBLUTR',
	// 	'Heather Military Green'			=> 	'HTRMILGR',
	// 	'Charcoal Heather Triblend'			=> 	'CHARHTRT',
	// 	'Heather Raspberry'			=> 	'HTRRASPB',
	// 	'Heather True Royal'			=> 	'HTRTRURO',
	// 	'Heavy Metal'			=> 	'HEAVYMET',
	// 	'Island Reef'			=> 	'ISLANDRE',
	// 	'Khaki Brown'			=> 	'KHAKIBRO',
	// 	'Natural Black'			=> 	'NATURALB',
	// 	'Neon Yellow'			=> 	'NEONYELL',
	// 	'OATMEAL TRIBLEND'			=> 	'OATMEALT',
	// 	'Periwinkle'			=> 	'PERIWINK',
	// 	'Premium Heather'			=> 	'PREMIUMH',
	// 	'Royal Caribe'			=> 	'ROYLCARI',
	// 	'Safety Yellow'			=> 	'SAFEYELL',
	// 	'SOLID BLACK SLUB'			=> 	'SLDBLKSL',
	// 	'Tour Yellow'			=> 	'TOURYELL',
	// 	'Watermelon'			=> 	'WATERMEL',
	// 	'White Fleck Triblend'			=> 	'WTFLECKT',
	// 	'WHT HTHR OLIVE'			=> 	'WHTHTHRO',
	// 	'Yellow Haze'			=> 	'YELLOWHA',
	// 	'Heather Lake Blue'			=> 	'HTRLAKEB',
	// 	'Seafoam Blue'			=> 	'SEAFOAMB',
	// 	'Baby Blue Navy'			=> 	'BABYBLUN',
	// 	'Black Neon Orange'			=> 	'BLKNEONO',
	// 	'Neon Red Orange'			=> 	'NEONREDO',
	// 	'WHT NEON ORANGE'			=> 	'WHTNEONO',
	// 	'Antique Cherry Red'			=> 	'ANTCHERR',
	// 	'Dark Grey Heather Black'			=> 	'DKGRHTRB',
	// 	'Eco Grey Eco True Black'			=> 	'EGRETRUB',
	// 	'GREY LT RED TRB'			=> 	'GRLTREDT',
	// 	'GREY TR RYL TRB'			=> 	'GRTRRYLT',
	// 	'Sport Grey Black'			=> 	'SPORTGRB',
	// 	'Sport Grey Red'			=> 	'SPORTGRR',
	// 	'Venetian Grey'			=> 	'VENETIAN',
	// 	'Eco Grey Eco True Navy'			=> 	'EGRETRUN',
	// 	'Eco True Navy Eco Grey'			=> 	'ETRUNVYE',
	// 	'Sport Grey Navy'			=> 	'SPORTGRN',
	// 	'Black Neon Pink'			=> 	'BLKNEONP',
	// 	'WHT FLCK TRBLND'			=> 	'WHTFLCKT',
	// 	'YLLW GLD TRBLND'			=> 	'YLLWGLDT',
	// 	'BLK HTHR TRIBLND'			=> 	'BLKHTHRT',
	// 	'BLK MINERAL WASH'			=> 	'BLKMINWA',
	// 	'Lime Maui Tie-Dye'			=> 	'LIMEMAUI',
	// 	'Amethyst Heather'			=> 	'AMETHYST',
	// 	'Antique Irish Green'			=> 	'ANTIRISH',
	// 	'Antique Jade Dome'			=> 	'ANTJADED',
	// 	'Antique Sapphire'			=> 	'ANTSAPPH',
	// 	'Asphalt Slub'			=> 	'ASPHALTS',
	// 	'Banana Cream'			=> 	'BANANACR',
	// 	'Brown Savana'			=> 	'BROWNSAV',
	// 	'Charcoal Black Slub'			=> 	'CHARBLKS',
	// 	'Eco True Currant'			=> 	'ETRUCURR',
	// 	'Electric Green'			=> 	'ELECTRIC',
	// 	'Gunmetal Heather'			=> 	'GUNMETAL',
	// 	'Heather Kelly Green'			=> 	'HTRKELLY',
	// 	'Heather Sapphire'			=> 	'HTRSAPPH',
	// 	'HTHR RASPBERRY'			=> 	'HTHRRASP',
	// 	'HTHR TRUE ROYAL'			=> 	'HTHRTRUR',
	// 	'Lucky Grn Blk'			=> 	'LUCKYGRN',
	// 	'MAIZE YELLOW'			=> 	'MAIZEYEL',
	// 	'PEBBLE BROWN'			=> 	'PEBBLEBR',
	// 	'SD DARK GRY TRBL'			=> 	'SDDKGRYT',
	// 	'Ultra Violet'			=> 	'ULTRAVIO',
	// 	'Carolina Blue'			=> 	'CAROLINA',
	// 	'Columbia Blue'			=> 	'COLUMBIA',
	// 	'Eco Pacific Blue'			=> 	'EPACIFIC',
	// 	'Tropical Blue'			=> 	'TROPICAL',
	// 	'Red Stone Stitch'			=> 	'REDSTSTI',
	// 	'Stonewashed Green'			=> 	'STWASHED',
	// 	'ATH GREY TRBLND'			=> 	'ATHGRTRB',
	// 	'DEEP HEATHR BLK'			=> 	'DPHEATHR',
	// 	'Eco Oatmeal Eco Grey'			=> 	'EOATMEAL',
	// 	'GREY EMERLD TRB'			=> 	'GREMERLD',
	// 	'Bright Salmon'			=> 	'BRIGHTSA',
	// 	'CHRCL BLK SLUB'			=> 	'CHRCLBLK',
	// 	'GRY CHR BLK TRB'			=> 	'GRYCHRBL',
	// 	'Heather Aubergine'			=> 	'HTRAUBER',
	// 	'HTHR GRASS GRN'			=> 	'HTHRGRAS',
	// 	'Island Yellow'			=> 	'ISLANDYE',
	// 	'SEA GREEN TRBLND'			=> 	'SEAGRNTR',
	// 	'SOLID NVY TRBLND'			=> 	'SLDNVYTR',
	// 	'SOLID RED TRIBLN'			=> 	'SLDREDTR',
	// 	'SOLID WHT TRBLND'			=> 	'SLDWHTTR',
	// 	'Vintage Turquoise'			=> 	'VINTURQU',
	// 	'WHT FLCK CHR TR'			=> 	'WHTFLCKC',
	// 	'WHT FLK NVY TRB'			=> 	'WHTFLKNV',
	// 	'WHT HTHR PEACH'			=> 	'WHTHTHRP',
	// 	'HTHR COLUM BLUE'			=> 	'HTHRCOLU',
	// 	'HEATHR DEEP TEAL'			=> 	'HEATHRDP',
	// 	'HTHR TEAM PURPLE'			=> 	'HTHRTEAM',
	// 	'BLCK MRBLE BLCK'			=> 	'BLCKMRBL',
	// 	'Eco Ivory Eco True Black'			=> 	'EIVORYET',
	// 	'Eco True Dusty Pine'			=> 	'ETRUDUST',
	// 	'Ultramarine White'			=> 	'ULTRAMAR',
	// 	'California Blue'			=> 	'CALIFORN',
	// 	'GRASS GRN TRBLND'			=> 	'GRASSGRN',
	// 	'HTHR MILTARY GRN'			=> 	'HTHRMILT',
	// 	'HTHR PRSM SUNSET'			=> 	'HTHRPRSM',
	// 	'HTHR YELLOW GOLD'			=> 	'HTHRYELL',
	// 	'MLTRY GRN TRBLND'			=> 	'MLTRYGRN',
	// 	'Eco True Pacific Blue'			=> 	'ETRUPACI',
	// 	'Blue Triblend'			=> 	'BLUTRIBD',
	// 	'Gray Camo'			=> 	'GRYCAMO',
	// 	'Grey Camo'			=> 	'GRECAMO',
	// 	'BLUE TRBLND'			=> 	'BLUTRIBLD',
	// 	'Cardinal Triblend'			=> 	'CARDTRIBD',
	// 	'Dark Grey Heather'			=> 	'DKGRYHR',
	// 	'Dark Heather Gray'			=> 	'DKHRGR',
	// 	'Dark Heather Grey'			=> 	'DKHRGRY',
	// 	'DK GREY HEATHER'			=> 	'DKGRHTR',
	// 	'CARDINAL TRBLND'			=> 	'CRDTRIBD',
	// 	'Graphite'			=> 	'GRPHITE',
	// 	'KELLY GREEN'			=> 	'KLLYGRN',
	// 	'Midnight'			=> 	'MDNIGHT',
	// 	'STEEL BLUE'			=> 	'STEELBL',
	// 	'Burnt Orange'			=> 	'BRNTORG',
	// 	'SLD BLK TRIBLEND'			=> 	'SDBLTRI',
	// 	'Solid Black Triblend'			=> 	'SDBLTRIB',
	// 	'True Royal Marble'			=> 	'TRURYLM',
	// 	'True Royal Triblend'			=> 	'TRURYLT',
	// 	'Varsity Red'			=> 	'VRSITYR',
	// 	'Burnt Orange White'			=> 	'BURNTORG',
	// 	'Charcoal-Black Triblend'			=> 	'CHRBLK',
	// 	'Graphite Heather'			=> 	'GRAPHITE',
	// 	'Kelly Green Heather'			=> 	'KELLYGRN',
	// 	'Varsity Royal'			=> 	'VARSITYR',
	// 	'Midnight Navy'			=> 	'MIDNIGHT',
	// 	'CHAR-BLACK TRIB'			=> 	'CHARBLK',
	// 	'TRUE ROYAL MRBLE'			=> 	'TRUROYLM',
	// 	'TRUE ROYAL TRBLN'			=> 	'TRUROYLT',
	// 	'STEEL BLU TRBLND'			=> 	'STEELBLU',
	// 	'HTHR PRISM MINT'			=> 	'HTRPRSMT',
	// 	'HTHR PRISM LILAC'			=> 	'HRPRSTL',
	// 	'HTHR PRISM PEACH'			=> 	'HTHRPRSP',
	// );

	// foreach( $color_array as $color_name => $color_code ) {

	// 	$garment_color = OMH()->garment_color_factory->get_by_color_name( $color_name );

	// 	if( $garment_color ) {
	// 		$garment_color->set_color_code( $color_code );
	// 		$garment_color->save();
	// 	} else {
	// 		br( 'There was a problem locating: ' . $color_name );
	// 	}
	// }

	// $populating_garment_products = OMH_Model_Garment_Product::populate_garment_products(1001221);

	// br( $populating_garment_products );

	// $garment_style = OMH()->garment_style_factory->read( 2 );

	// br( $garment_style->get_taxonomy() );

	// $user = wp_get_current_user();

	// $args = array(
	// 	'post_type'		=> 'product',
	// 	'tax_query'		=> array(
	// 		array(
	// 			'taxonomy'	=> 'chapters',
	// 			'field'		=> 'slug',
	// 			'terms'		=> 'mainebeta'
	// 		)
	// 	)
	// );

	// $query = new WP_Query( $args );

	// br( $query->get_posts() );

	// $chapter = OMH()->chapter_factory->read( 1756 );

	// br( $chapter->get_public_products() );

	// $product = wc_get_product( 84 );

	// br( $product->get_variation_garment_sizes() );
	
	// $search_query[] = array(
	// 	'key'		=> 'first_name',
	// 	'value'		=> $search_term,
	// 	'compare'	=> 'LIKE'
	// );
	// $search_query[] = array(
	// 	'key'		=> 'last_name',
	// 	'value'		=> $search_term,
	// 	'compare'	=> 'LIKE'
	// );
	// $search_query[] = array(
	// 	'key'		=> 'nickname',
	// 	'value'		=> $search_term,
	// 	'compare'	=> 'LIKE'
	// );

	// $user_query_args = array(
	// 	'meta_query'	=> array(
	// 		'relation'		=> 'AND',
	// 		array(
	// 			'key'			=> 'primary_chapter_id',
	// 			'value'			=> '1754',
	// 			'compare'		=> '='
	// 		),
	// 		array(
	// 			'relation'		=> 'OR',
	// 			array(
	// 				'key'			=> 'first_name',
	// 				'value'			=> 'Ben',
	// 				'compare'		=> 'LIKE'
	// 			),
	// 			array(
	// 				'key'			=> 'last_name',
	// 				'value'			=> 'Ben',
	// 				'compare'		=> 'LIKE'
	// 			),
	// 			array(
	// 				'key'			=> 'nickname',
	// 				'value'			=> 'Ben',
	// 				'compare'		=> 'LIKE'
	// 			)
	// 		)
	// 	)
	// );

	// $wp_user_query = new WP_User_Query( $user_query_args );

	// br( $wp_user_query->get_results() );

	// $array = array(
	// 	'id'			=> 428,
	// 	'is_temp'		=> false,
	// 	'src'			=> 'http://mh-ss.onyx/wp-content/uploads/2019/03/Phoenix_Revamped_4.png',
	// 	'thumbnail_src'	=> 'http://mh-ss.onyx/wp-content/uploads/2019/03/Phoenix_Revamped_4-150x150.png'
	// );

	// br( $array );

	// br( get_unique_filename( 'Phoenix_Revamped.png', OMH_TEMP_UPLOAD_DIR ) );

	// $inksoft_api_response = OMH_API_Inksoft::request( 'get_product', array( 
	// 	'query_params' => array( 
	// 		'ProductID' => '1000041',
	// 		// 'Email' 	=> 'admin@merch.house',
	// 		// 'ApiKey'	=> '161bf9b2-7d78-4614-ac03-9eef97dd2e37'
	// 	) 
	// ) );

	// br( $inksoft_api_response );

	// $product_variable = wc_get_product( 366 );

	// br( $product_variable->has_size( '2xl' ) );

	// br( $product_variable->get_variation_attributes() );

	// br( $product_variable->get_children() );

	// $garment_variations = $product_variable->get_garment_variations( 1 );

	// foreach( $garment_variations as $garment_variation_id ) {

		// $garment_variation = wc_get_product( $garment_variation_id );

		// wp_update_post( array( 'ID' => $garment_variation->get_id(), 'post_status' => 'public' ) );
	// }

	// foreach( $product_variable->get_children() as $child_id ) {

	// 	$variation = wc_get_product( $child_id );

	// 	br( $variation->get_size_attribute() );
	// }

	// $credit = OMH()->credit_factory->query(
	// 	array(
	// 		'return'	=> array(
	// 			'amount'
	// 		),
	// 		'where'		=> array(
	// 			array(
	// 				'name'	=> 'chapter_id',
	// 				'value'	=> '1',
	// 			)
	// 		)
	// 	)
	// );

	// br( array_sum( $credit ) );

	// $omh_order = wc_get_order( 362 );

	// br( $omh_order->get_items() );

	// br( wp_get_current_user() );

	// br( $omh_order->get_user() );

	// $garment_style = OMH()->garment_style_factory->read( 3 );

	// br( $garment_style );
	// br( $garment_style->get_term() );

	// $styles = $wpdb->get_results( "SELECT * FROM wp_omh_garment_styles" );

	// foreach( $styles as $style ) {

	// 	$style_slug = $style->style_slug;
	// 	$style_color = ucwords( str_replace( '-', '%', array_pop( explode( '_', $style_slug ) ) ) );

	// 	$color_id = $wpdb->get_var( "SELECT ID FROM wp_omh_garment_colors WHERE color_name LIKE '{$style_color}'" );

	// 	$wpdb->update( 'wp_omh_garment_styles', array( 'garment_color_id' => $color_id ), array( 'ID' => $style->ID ) );
	// }

	// $college = OMH()->college_factory->get_by_term_slug( 'merch-house' );

	// br( $college );

	// $sql = "
	// 	SELECT *
	// 	FROM `%s`
	// ";

	// $table = 'wp_omh_colleges';
	// $where = 2;

	// if( $where ) {
	// 	$sql .= " WHERE ID = %d";
	// }

	// $query = $wpdb->get_results(
	// 	$wpdb->prepare(
	// 		$sql,
	// 		$table,
	// 		$where ?: null
	// 	)
	// );

	// br( $query );

	// $existing_attributes = wp_get_post_terms( 84, 'pa_size', array( 'fields' => 'ids' ) );
	// br( $existing_attributes );
	// $test_product = wc_get_product( 14 );

	// br( $test_product->get_chapter()->get_organization()->get_term() );

	// var_dump( get_term_by( 'slug', '' ) );
	// var_dump( get_the_terms( 14, 'colleges' ) );

	// $omh_session = OMH_Session::instance();

	// br( $omh_session->set_chapter( 16 ) );

	// br( $omh_session->get_chapter()->get_term() );

	// br( 
	// 	serialize(
	// 		array(
	// 			'media_1'	=> 'http://mh-ss.onyx/wp-content/uploads/chapters/-Phoenix_Revamped.png',
	// 			'media_2'	=> 'http://mh-ss.onyx/wp-content/uploads/chapters/-phoenix.png',
	// 			'media_3'	=> 'http://mh-ss.onyx/wp-content/uploads/chapters/-awesome.jpeg',
	// 			'media_4'	=> ''
	// 		)
	// 	)
	// );
?>
</code>
<script type="text/javascript">
	// (function($) {
	// 	$(document).ready(function() {
	// 		omh.ajax(
	// 			'omh_site_search',
	// 			{ 
	// 				search: 'zbt',
	// 				type: 'products'
	// 			},
	// 			function( response ) {

	// 				console.log('search response', response);
	// 			},
	// 			null
	// 		);
	// 	});
	// })(jQuery);
</script>
<?php get_footer(); ?>
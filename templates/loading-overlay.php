<?php
defined( 'ABSPATH' ) ?? exit;

$chapter = OMH_Session::get_chapter();
?>
<div class="loading-overlay" role="dialog" style="opacity: 0; pointer-events: none;">
	<div class="loading-dialog loading-dialog-centered" role="document">
		<div class="loading-content">
			<div class="loading-header">
				<h4 class="loading-title">Submitting Design...</h4>
			</div>
			<div class="loading-body">
				<div class="loading-image">
					<svg version="1.0" xmlns="http://www.w3.org/2000/svg" class="loading-t-shirt" width="1280.000000pt" height="1280.000000pt" viewBox="0 0 1280.000000 1280.000000" preserveAspectRatio="xMidYMid meet">
						<g transform="translate(0.000000,1280.000000) scale(0.100000,-0.100000)">
							<path d="M4580 11189 c-23 -9 -1390 -300 -1568 -334 -100 -19 -166 -52 -237
								-116 -225 -204 -2596 -2420 -2620 -2448 -54 -64 -57 -69 -76 -143 -46 -179 -1
								-391 116 -543 49 -63 956 -1032 995 -1063 61 -48 147 -93 220 -116 126 -40
								182 -42 336 -12 12 3 39 13 60 24 22 11 316 278 654 594 l615 574 5 -3125 5
								-3126 3207 -3 3208 -2 2 3201 3 3200 570 -532 c314 -293 630 -588 703 -655 73
								-68 154 -135 180 -148 179 -90 441 -43 624 113 81 69 954 1007 1000 1074 79
								116 118 237 118 367 0 92 -24 212 -51 251 -27 38 -73 82 -1299 1224 -619 577
								-1189 1109 -1268 1182 -82 76 -164 144 -193 159 -49 24 -344 90 -1419 314
								-135 28 -255 55 -268 60 -16 7 -55 2 -145 -19 -294 -70 -611 -120 -982 -153
								-339 -30 -935 -30 -1270 0 -442 40 -894 119 -1186 206 -8 2 -25 0 -39 -5z"
							/>
						</g>
					</svg>
					<div class="loading-greek-letters font-weight-bold greek-letters">
						<?php echo $chapter ? $chapter->get_organization()->get_org_greek_letters() : 'MH'; ?>
					</div>
				</div>
				<div class="loading-text">
					<p class="lead font-weight-normal pb-2">This may take a minute (ish)</p>
					<p>Please do not close your browser window <br>or navigate away from this page.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="loading-backdrop" style="opacity: 0; pointer-events: none;"></div>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p>Follower email: <?php echo $follower_email; ?></p>
<p>Followed <?php echo $following_type; ?>: <?php echo $following_store_name; ?></p>


<?php

do_action( 'woocommerce_email_footer', $email );
<?php
// Exit if accessed directly
defined('ABSPATH') || exit;

do_action('woocommerce_email_header', $email_heading, $email); ?>

<p><?php _e($greeting, OMH_TEXT_DOMAIN); ?>,</p>

<p>Thank you for signing up <?php echo $chapter; ?> to Merch House.</p>
<p>A representative from Merch House will contact you soon about your chapter account.</p>

<p> Name: <?php echo $name; ?> </p>

<p> College name: <?php echo $college; ?> </p>

<p>Please email info@merch.house if you have any questions!</p>

<p><?php _e($closing, OMH_TEXT_DOMAIN); ?></p>

<?php

do_action('woocommerce_email_footer', $email);

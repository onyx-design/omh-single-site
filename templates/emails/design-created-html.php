<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

$product_garments = $product->get_garments();
$product_chapter = $product->get_chapter();
$house_admin = get_user_by( 'ID', $product->get_created_by() );
$media_files = $product->get_customer_uploads();

do_action( 'woocommerce_email_header', $email_heading, $email );
?>

<?php if( !OMH()->is_live_site() ): ?>
<i>
	This was sent from <a href="<?php echo $site_url; ?>"><?php echo $site_url; ?></a>
</i>
<?php endif; ?>
<h3>
	<?php
		printf( 
			'Go to Edit Product Page <a href="%1$s" target="_blank">%1$s</a>', 
			esc_html( $edit_design ) 
		);
	?>
</h3>

<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
	<tbody>
		<tr bgcolor="#EAF2FA">
			<td colspan="2">
				<font style="font-family:sans-serif;font-size:12px">
					<strong>Chapter Name</strong>
				</font>
			</td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td width="20">&nbsp;</td>
			<td>
				<font style="font-family:sans-serif;font-size:12px">
					<?php
						if( $product_chapter ) {
							echo $product_chapter->get_chapter_name();
						}
					?>
				</font>
			</td>
		</tr>
		<tr bgcolor="#EAF2FA">
			<td colspan="2">
				<font style="font-family:sans-serif;font-size:12px">
					<strong>Product Name</strong>
				</font>
			</td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td width="20">&nbsp;</td>
			<td>
				<font style="font-family:sans-serif;font-size:12px">
					<?php echo $product->get_name(); ?>
				</font>
			</td>
		</tr>
		<tr bgcolor="#EAF2FA">
			<td colspan="2">
				<font style="font-family:sans-serif;font-size:12px">
					<strong>Sales Type</strong>
				</font>
			</td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td width="20">&nbsp;</td>
			<td>
				<font style="font-family:sans-serif;font-size:12px">
					<?php echo $product->get_sales_type(); ?>
				</font>
			</td>
		</tr>
		<tr bgcolor="#EAF2FA">
			<td colspan="2">
				<font style="font-family:sans-serif;font-size:12px">
					<strong>Deliver Date</strong>
				</font>
			</td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td width="20">&nbsp;</td>
			<td>
				<font style="font-family:sans-serif;font-size:12px">
					<?php echo $product->get_deliver_date(); ?>
				</font>
			</td>
		</tr>
		<tr bgcolor="#EAF2FA">
			<td colspan="2">
				<font style="font-family:sans-serif;font-size:12px">
					<strong>Upload helpful ideas (screenshots, ideas, logos, etc)</strong>
				</font>
			</td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td width="20">&nbsp;</td>
			<td>
				<font style="font-family:sans-serif;font-size:12px">
					<ul>
						<?php if( isset( $media_files['media_1'] ) ): ?>
						<li>
							<a href="<?php echo esc_url( $media_files['media_1'] ); ?>" target="_blank"><?php echo $media_files['media_1']; ?></a>
						</li>
						<?php endif; ?>
						<?php if( isset( $media_files['media_2'] ) ): ?>
						<li>
							<a href="<?php echo esc_url( $media_files['media_2'] ); ?>" target="_blank"><?php echo $media_files['media_2']; ?></a>
						</li>
						<?php endif; ?>
						<?php if( isset( $media_files['media_3'] ) ): ?>
						<li>
							<a href="<?php echo esc_url( $media_files['media_3'] ); ?>" target="_blank"><?php echo $media_files['media_3']; ?></a>
						</li>
						<?php endif; ?>
						<?php if( isset( $media_files['media_4'] ) ): ?>
						<li>
							<a href="<?php echo esc_url( $media_files['media_4'] ); ?>" target="_blank"><?php echo $media_files['media_4']; ?></a>
						</li>
						<?php endif; ?>
					</ul>
				</font>
			</td>
		</tr>
		<tr bgcolor="#EAF2FA">
			<td colspan="2">
				<font style="font-family:sans-serif;font-size:12px">
					<strong>Leave comments for our talented designers. The more detailed, the closer the design will be to your vision!</strong>
				</font>
			</td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td width="20">&nbsp;</td>
			<td>
				<font style="font-family:sans-serif;font-size:12px">
					<?php echo $product->get_customer_comment(); ?>
				</font>
			</td>
		</tr>
		<tr bgcolor="#EAF2FA">
			<td colspan="2">
				<font style="font-family:sans-serif;font-size:12px">
					<strong>DesignID</strong>
				</font>
			</td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td width="20">&nbsp;</td>
			<td>
				<font style="font-family:sans-serif;font-size:12px"><?php echo $product->get_design_id(); ?></font>
			</td>
		</tr>
		<?php if( $product_garments ): ?>
			<?php foreach( $product_garments as $product_garment ): ?>
				<?php if( $product_garment_style = $product_garment->get_garment_style() ): ?>
					<?php if( $product_garment_product = $product_garment_style->get_garment_product() ): ?>
						<tr bgcolor="#EAF2FA">
							<td colspan="2">
								<font style="font-family:sans-serif;font-size:12px">
									<strong>Garment Product</strong>
								</font>
							</td>
						</tr>
						<tr bgcolor="#FFFFFF">
							<td width="20">&nbsp;</td>
							<td>
								<div style="font-weight:600;"><?php echo $product_garment_product->get_product_name(); ?></div>
								<div>Brand: <?php echo $product_garment_product->get_garment_brand()->get_brand_name(); ?></div>
								<div>InkSoft Product ID: <?php echo $product_garment_product->get_inksoft_product_id(); ?></div>
								<div>InkSoft SKU: <?php echo $product_garment_product->get_inksoft_sku(); ?></div>
								<div>Description: <?php echo $product_garment_product->get_product_long_description(); ?></div>
							</td>
						</tr>
					<?php endif; ?>
					<tr bgcolor="#EAF2FA">
						<td colspan="2">
							<font style="font-family:sans-serif;font-size:12px">
								<strong>Garment Style</strong>
							</font>
						</td>
					</tr>
					<tr bgcolor="#FFFFFF">
						<td width="20">&nbsp;</td>
						<td>
							<div style="font-weight:600;"><?php echo $product_garment_style->get_style_full_name(); ?></div>
							<div>Color: <?php echo $product_garment_style->get_garment_color()->get_color_name(); ?></div>
							<div>Sizes: <?php echo $product_garment_style->get_sizes(); ?></div>
							<div>InkSoft Style ID: <?php echo $product_garment_style->get_inksoft_style_id(); ?></div>
						</td>
					</tr>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
		<tr bgcolor="#EAF2FA">
			<td colspan="2">
				<font style="font-family:sans-serif;font-size:12px">
					<strong>Chapter Dashboard</strong>
				</font>
			</td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td width="20">&nbsp;</td>
			<td>
				<font style="font-family:sans-serif;font-size:12px">
					<?php
						if( $product_chapter ) {
							echo OMH()->dashboard_url( 'dashboard', $product_chapter->get_id() );
						}
					?>
				</font>
			</td>
		</tr>
		<tr bgcolor="#EAF2FA">
			<td colspan="2">
				<font style="font-family:sans-serif;font-size:12px">
					<strong>House Admin</strong>
				</font>
			</td>
		</tr>
		<tr bgcolor="#FFFFFF">
			<td width="20">&nbsp;</td>
			<td>
				<font style="font-family:sans-serif;font-size:12px">
					<?php
						if( $house_admin ) {
							echo "{$house_admin->get_full_name()} ({$house_admin->get_email()})";
						} else {
							echo 'There was a problem retrieving that user.';
						}
					?>
				</font>
			</td>
		</tr>
	</tbody>
</table>
<?php

do_action( 'woocommerce_email_footer', $email );
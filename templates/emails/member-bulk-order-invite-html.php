<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( $greeting, OMH_TEXT_DOMAIN ); ?>,</p>

<p><?php printf( __( '[%s] has started a new bulk order. Please %s by %s.' ), esc_html( $site_title ), '<a href="' . esc_html( $submission_url ) . '">submit your sizes</a>', $end_submission_date ); ?></p>

<p>If you are not an active House Member, you will first be prompted to register your account.</p>

<p><?php _e( $closing, OMH_TEXT_DOMAIN ); ?></p>

<?php

do_action( 'woocommerce_email_footer', $email );
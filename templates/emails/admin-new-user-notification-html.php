<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$user_data = array(
	'Email'			=> $user->user_email,
	'First Name'	=> $user->first_name,
	'Last Name'		=> $user->last_name,
	'Phone Number'	=> get_user_meta( $user->ID, 'billing_phone', true )
);

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( $greeting, OMH_TEXT_DOMAIN ); ?>,</p>

<p>
	<?php printf( __( 'The following user has completed their account chapter activation for %1$s.', OMH_TEXT_DOMAIN ), esc_html( $chapter_name ) ); ?>
</p>

<p>
	<ul>
		<?php foreach( $user_data as $user_field => $user_value ): ?>
			<li><strong><?php echo $user_field; ?>: </strong><?php echo $user_value; ?></li>
		<?php endforeach; ?>
	</ul>
</p>

<p><?php _e( $closing, OMH_TEXT_DOMAIN ); ?></p>

<?php

do_action( 'woocommerce_email_footer', $email );
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( $greeting, OMH_TEXT_DOMAIN ); ?>,</p>

<p><?php printf( __( 'Your account has been upgraded to a House Member on %s.', OMH_TEXT_DOMAIN ), esc_html( $chapter_name ) ); ?></p>

<?php if( !$user->is_activated() ): ?>
	<p><?php printf( __( 'You must first activate your account by %s.', OMH_TEXT_DOMAIN ), '<a href="' . esc_html( OMH()->dashboard_url( 'activation?key=' . $user->get_user_activation_key(), $chapter->get_id() ) ) . '">clicking here</a>' ); ?></p>
<?php else: ?>
	<p><?php printf( __( 'You can now browse and purchase member only retail products listed on %s\'s Shop. You will also receive apparel related updates from the house, including details on future campaigns such as rush t-shirts and philanthropy campaigns.', OMH_TEXT_DOMAIN ), esc_html( $chapter_name ) ); ?></p>
<?php endif; ?>

<p><?php printf( __( 'For help, contact your House Admin or email %s.', OMH_TEXT_DOMAIN ), '<a href="mailto:info@merch.house">info@merch.house</a>' ); ?></p>

<p><?php printf( __( '%s to access %s\'s Merch House store!', OMH_TEXT_DOMAIN ), '<a href="' . $chapter->get_term_link() . '">Click here</a>', esc_html( $chapter_name ) ); ?></p>

<p><?php _e( $closing, OMH_TEXT_DOMAIN ); ?></p>

<?php

do_action( 'woocommerce_email_footer', $email );
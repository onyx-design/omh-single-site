<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( $greeting, OMH_TEXT_DOMAIN ); ?>,</p>

<p><?php printf( __( 'You have been invited to %s.', OMH_TEXT_DOMAIN ), esc_html( $chapter_name ) ); ?></p>

<?php if( !$user->is_activated() ): ?>
	<p><?php printf( __( 'You must verify your account by %s.', OMH_TEXT_DOMAIN ), '<a href="' .esc_html($activation_link) . '">clicking here</a>' ); ?></p>
<?php endif; ?>

<p><?php printf( __( 'You can now browse and purchase member only retail products listed on %s\'s Shop. You will also receive apparel related updates from the house, including details on future campaigns such as rush t-shirts and philanthropy campaigns.', OMH_TEXT_DOMAIN ), esc_html( $chapter_name ) ); ?></p>

<p><?php printf( __( 'For help, contact your House Admin or email %s.', OMH_TEXT_DOMAIN ), '<a href="mailto:info@merch.house">info@merch.house</a>' ); ?></p>

<p><?php _e( $closing, OMH_TEXT_DOMAIN ); ?></p>

<?php

do_action( 'woocommerce_email_footer', $email );
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php echo $site_title; ?> has joined Merch House!</p>

<p>Merch House is an all-in-one apparel management platform for Greeks. We help fraternities and sororities run their own online store.</p>

<p><?php printf( __( 'Activate your account to gain access to your store. Click here: %s', OMH_TEXT_DOMAIN ), '<a href="' . esc_html( $activation ) . '">Activation Link</a>' ); ?></p>

<p>By activating your account, you can:</p>

<ul>
	<li>Buy your chapter’s custom designed shirts and apparel <em>individually</em></li>
	<li>Access member only products and pricing</li>
	<li>Participate in bulk orders and designs</li>
	<li>Get notified of special member only discounts and free merch</li>
	<li>Share your store link with other chapters, family and friends </li>
</ul>

<p>
	Cheers,<br/>
	<?php _e( $closing, OMH_TEXT_DOMAIN ); ?>
</p>

<?php

do_action( 'woocommerce_email_footer', $email );
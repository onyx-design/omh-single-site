<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( $greeting, OMH_TEXT_DOMAIN ); ?>,</p>

<p>
	<?php printf( __( 'The following house members have been added to the chapter %1$s.', OMH_TEXT_DOMAIN ), esc_html( $chapter_name ) ); ?>
</p>

<p>
	<ul>
		<?php foreach( $user_list as $user ): ?>
			<li>
			<?php echo $user ?>
		</li>
		<?php endforeach; ?>
	</ul>
</p>

<p><?php _e( $closing, OMH_TEXT_DOMAIN ); ?></p>

<?php

do_action( 'woocommerce_email_footer', $email );
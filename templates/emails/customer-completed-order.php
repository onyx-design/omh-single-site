<?php
	/**
	 * Customer completed order email
	 */

	// Exit if accessed directly
	defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email );?>

<p>
	<?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) );?>
</p>

<p>
	Your Merch House order has been shipped.
</p>

<?php
	do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

	do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

	do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
?>
<p>
	Thanks for shopping on Merch House - the only Greek chapter to chapter marketplace!
</p>

<?php

do_action( 'woocommerce_email_footer', $email );

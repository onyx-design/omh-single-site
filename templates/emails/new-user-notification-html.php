<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( $greeting, OMH_TEXT_DOMAIN ); ?>,</p>

<p>Welcome to Merch House, the only Greek marketplace providing fraternity and sorority chapters with their own storefronts.</p>

<p>
	<?php printf( __( '<a href="%1$s">Click here</a> to activate your account.', OMH_TEXT_DOMAIN ), esc_html( $activation_link ) ); ?>
<p>

<p><?php printf( __( 'If the above link does not work, copy and paste this url into your browser:<br>%1$s.', OMH_TEXT_DOMAIN ), esc_html( $activation_link ) ); ?></p>

<p>Please reply to this email with any questions.</p>

<p><?php _e( $closing, OMH_TEXT_DOMAIN ); ?></p>

<?php

do_action( 'woocommerce_email_footer', $email );
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( $greeting, OMH_TEXT_DOMAIN ); ?>,</p>

<p><?php printf( __( 'Your "%1$s" design has been approved.', OMH_TEXT_DOMAIN ), esc_html( $design_name ) ); ?></p>

<?php
	if ( ( 'bulk' == $product->get_product_order_type() ) && $start_bulk_order ) {
		?>
			<p><?php printf( __( '%1$s to start your bulk order.', OMH_TEXT_DOMAIN ), '<a href="' . esc_html( $start_bulk_order ) . '">Click here</a>' ); ?></p>
		<?php
	}
	else {
		?>
			<p><?php printf( __( '%1$s to review, edit, and list your product for sale in your store.', OMH_TEXT_DOMAIN ), '<a href="' . esc_html( $edit_design ) . '">Click here</a>' ); ?></p>
		<?php
	}
?>

<p><?php printf( __( 'If you would like any changes made to your design, please email %1$s.', OMH_TEXT_DOMAIN ), '<a href="mailto:art@merch.house">art@merch.house</a>' ); ?></p>

<p><?php printf( __( 'For help, contact your House Admin or email %1$s.', OMH_TEXT_DOMAIN ), '<a href="mailto:info@merch.house">info@merch.house</a>' ); ?></p>

<p><?php _e( $closing, OMH_TEXT_DOMAIN ); ?></p>

<?php

do_action( 'woocommerce_email_footer', $email );
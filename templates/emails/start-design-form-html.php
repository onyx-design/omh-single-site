<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<p><?php _e( $greeting, OMH_TEXT_DOMAIN ); ?>,</p>

<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
	<tbody>
		<?php foreach( $form_fields as $form_field ): ?>
			<tr bgcolor="#EAF2FA">
				<td colspan="2">
					<font style="font-family:sans-serif;font-size:12px">
						<strong><?php echo $form_field['label']; ?></strong>
					</font>
				</td>
			</tr>
			<tr bgcolor="#FFFFFF">
				<td width="20">&nbsp;</td>
				<td>
					<font style="font-family:sans-serif;font-size:12px">
						<?php echo $form_field['value']; ?>
					</font>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<p><?php _e( $closing, OMH_TEXT_DOMAIN ); ?></p>

<?php

do_action( 'woocommerce_email_footer', $email );
<div class="mh-account-activation-wrapper">
	<h3>Activate your House Member account for <?php echo $chapter->get_chapter_name(); ?></h3>
	<div id="mh-dashboard-notices"></div>
	<form id="mh-account-activation-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="house_member_activation" data-form-nonce="<?php echo wp_create_nonce( 'house-member-activation' ); ?>">

		<div class="col-md-6">
		<?php
			echo OMH_HTML_UI_Input::factory(
				array(
					'input_id'		=> 'first_name',
					'label' 		=> 'First Name',
					'value'			=> $user->first_name,
					'required'		=> true
				)
			);
		?>
		</div>

		<div class="col-md-6">
		<?php
			echo OMH_HTML_UI_Input::factory(
				array(
					'input_id'		=> 'last_name',
					'label'		 	=> 'Last Name',
					'value'			=> $user->last_name,
					'required'		=> true
				)
			);
		?>
		</div>

		<div class="col-md-6">
		<?php
			echo OMH_HTML_UI_Input::factory(
				array(
					'input_type'	=> 'password',
					'input_id'		=> 'new_password',
					'label'		 	=> 'Password',
					'input_attrs'	=> array(
						'pattern'					=> '(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9$@$!%*#?&]{6,}',
						'data-invalid-msg-pattern'	=> OMH_Frontend::password_hint()
					),
					'required'		=> true,
					'match_field'	=> 'confirm_new_password'
				)
			);
		?>
		</div>

		<div class="col-md-6">
		<?php
			echo OMH_HTML_UI_Input::factory(
				array(
					'input_type'	=> 'password',
					'input_id'		=> 'confirm_new_password',
					'label'		 	=> 'Confirm Password',
					'input_attrs'	=> array(
						'pattern'					=> '(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9$@$!%*#?&]{6,}',
						'data-invalid-msg-pattern'	=> OMH_Frontend::password_hint()
					),
					'required'		=> true,
					'match_field'	=> 'new_password'
				)
			);
		?>
		</div>

		<?php
			echo OMH_HTML_UI_Input::factory(
				array(
					'input_id'		=> 'user_id',
					'input_type'	=> 'hidden',
					'class'			=> 'd-none',
					'value'			=> $user->ID
				)
			);
		?>

		<?php
			// Submit
			echo OMH_HTML_UI_Button::factory(
				array(
					'color'		=> 'primary',
					'value'		=> 'submit',
					'form'		=> 'mh-account-activation-form',
					'class'		=> array(
						'account-activation-submit',
						'mh-forms-submit'
					),
					'label'		=> 'Submit',
				)
			);
		?>
	</form>
</div>
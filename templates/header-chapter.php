<?php
defined( 'ABSPATH' ) || exit;

$chapter = OMH()->chapter_factory->get_by_term_slug( $term->slug );
$college = $chapter->get_college();
$organization = $chapter->get_org();
$header_title = $chapter->get_chapter_banner_name();
$header_subtitle = 'Official Merchandise Store';
$header_location = $college ? "{$college->get_city()}, {$college->get_state()}" : '';


$header_image = $chapter->get_banner_image( 'full' ) ?: ( $college ? $college->get_banner_image( 'full' ) : $header_image );

$header_logo = $organization ? $organization->get_logo_image(true) : '';

if( $header_logo ) {

	$header_logo = '<img class="header-logo-img" src="'.$header_logo[0] .'" width="' . $header_logo[1] . '" height="' . $header_logo[2] .'" />';
}
?>



<div id="page-header-wrap" data-animate-in-effect="none" data-midnight="light" class="page-header--chapters" style="height: 350px;">
	<div class="not-loaded " data-padding-amt="normal" data-animate-in-effect="none" id="page-header-bg" data-midnight="light" data-text-effect="" data-bg-pos="top" data-alignment="center" data-alignment-v="middle" data-parallax="0" data-height="350" style="background-color: #000000;  height:350px;">
		<div class="page-header-bg-image-wrap" id="nectar-page-header-p-wrap" data-parallax-speed="medium">
			<div class="page-header-bg-image" style="background-image: url(<?php echo $header_image ; ?>);background-position: center center;"></div>
			<div class="row-bg-overlay" style="background-color:#FFFFFF;opacity: 0.6;position: absolute;top: 0;left: 0;height: 100%;width: 100%;z-index: 10;backface-visibility: hidden;"></div>
		</div>
		<div class="container">
			<div clas="row" style="height:100%;">
				<div class="col span_6">
					<div class="inner-wrap">
						<div class="omh-header-content">
							<div class="header-logo">
								<?php echo $header_logo ;?>
							</div>
							<div class="header-titles-wrap">
								<h1 class="header-title py-0"><?php echo $header_title;?></h1>
								<h3 class="header-subtitle"><?php echo $header_subtitle;?></h3>
								<h4 class="header-store-location" style="font-weight:400;">
									<?php echo $header_location ;?>
								</h4>
								<?php 
									echo OMH_HTML_UI_Button::factory(
										array(
											'color'		=> 'primary',
											'attrs'		=> array(
												'data-omh-toggle-modal'	=> 'new-follower-modal',
												'id'		=> 'new-follower-modal-toggle-btn'
											),
											'class'		=> 'mh-form-submit',
											'label'		=> 'Follow Store',
											'size'		=> 'small'
										)
									);
								?>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
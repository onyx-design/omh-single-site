<?php
defined( 'ABSPATH' ) || exit;

$organization = OMH()->organization_factory->get_by_term_slug( $term->slug );
$header_title = $organization->get_org_name();
$header_subtitle = 'Browse products from different chapters on Merch House';

$header_logo = $organization->get_logo_image(true) ?: '';

if( $header_logo ) {

	$header_logo = '<img class="header-logo-img" src="'.$header_logo[0] .'" width="' . $header_logo[1] . '" height="' . $header_logo[2] .'" />';
}
?>
<div class="row page-header-no-bg" data-alignment="left">
	<div class="container">	
<!-- Recruitment Gallery Alert -->
<div class="row">
	<div class="col">
		<?php
		if (strcmp($term->slug, 'beta') === 0){
			echo OMH_HTML_UI_Alert::factory(
				array(
					'color'	=> 'info',
					'text'	=> "New! Browse our Fall Recruitment Gallery "
					. OMH_HTML_UI_Button::factory(
						array(
							'color'		=> 'info',
							'href'		=> site_url('beta-theta-pi-design-gallery'),
							'label'		=> 'Click Here!',
							'size'		=> 'small'
						)
					),
					'dismissible'	=> false
					)
				);	
			}
		?>
							
</div>
		<div class="col span_9 section-title">
			<div class="omh-header-content">
				<div class="header-logo">
					<?php echo $header_logo ;?>
				</div>
				<div class="header-titles-wrap">
					<h1 class="header-title py-0"><?php echo $header_title;?></h1>
					<h3 class="header-subtitle"><?php echo $header_subtitle;?></h3>
					<?php 
						echo OMH_HTML_UI_Button::factory(
							array(
								'color'		=> 'primary',
								'attrs'		=> array(
									'data-omh-toggle-modal'	=> 'new-follower-modal',
									'id'		=> 'new-follower-modal-toggle-btn'
								),
								'class'		=> 'mh-form-submit',
								'label'		=> 'Follow Store',
								'size'		=> 'small'
							)
						);
					?>
				</div>
			</div>
			
									
		</div>
		<div class="col span_3">
			<div class="inner-wrap">
				
			</div>
		</div>
	</div>

</div>

<?php
defined('ABSPATH') || exit;
global $post;

wp_nonce_field('omh_ajax_search_nonce', 'omh_ajax_search_nonce');

$search_options = array();

$term = get_queried_object();
if( $term instanceof WP_Term ) {
	$allowed_taxonomies = array( 'chapters', 'organizations', 'colleges' );
	if( ( 'WP_Term' == get_class( $term ) ) && in_array( $term->taxonomy, $allowed_taxonomies ) ){
			if ($term->taxonomy == "organizations"){
				$organization = OMH()->organization_factory->get_by_term_slug( $term->slug );
				$header_title = $organization->get_org_name();
			}
		}
	}

?>

<div class="omh-sidebar">

	<?php
		if (is_shop() || is_product_category()) {
			?>
			<div class="omh-sidebar-widget omh-sidebar-search">
				<h4 class="omh-sidebar-heading mb-0">Search Orgs / Chapters</h4>
				<?php
					echo OMH_HTML_UI_Select2::factory(
						array(
							'input_id'			=> 'organization_select_id',
							'label'				=> false,
							'placeholder'		=> 'Enter text to search...',
							'search_model'		=> 'public_search',
							'class'				=> 'mb-0 marketplace-select omh-select2-minimal',
							'select2'		=> array(
								'dropdown-css-class'	=> 'omh-select2-minimal__dropdown'
							)
						)
					);
				?>
			</div>
			<?php
		}

		if(isset($organization)){
			if( 'organizations' == $term->taxonomy 
				&& $publicly_available_chapters = $organization->get_publicly_available_chapters()
			) { 
				$public_chapters = array(
					0 => 'Select a ' . $header_title . ' Chapter'
				);
				foreach( $publicly_available_chapters as $publicly_available_chapter ) {
					$public_chapters[ get_term_link( $publicly_available_chapter ) ] = $publicly_available_chapter->name;
				}
				?>
				<div class="omh-sidebar-widget omh-sidebar-search">
					<h4 class="omh-sidebar-heading mb-0">Search Chapters</h4>
					<?php	
					echo OMH_HTML_UI_Select2::factory(
						array(
							'input_id'		=> 'publicly_available_chapters',
							'label'			=> false,
							'placeholder'	=> 'Enter text to search...',
							'select_options'=> $public_chapters,
							'class'				=> 'mb-0 marketplace-select omh-select2-minimal',
							'select2'		=> array(
								'dropdown-css-class'	=> 'omh-select2-minimal__dropdown'
							)
						)
					);
					?>
				</div>
				<?php
			}
		}
		?>
	
	
		<?php if (isset($publicly_available_categories)) {
			foreach ($publicly_available_categories as $public_category_parent => $public_categories) : ?>
				<div class="widget woocommerce omh-sidebar-widget omh-sidebar-product-categories-wrapper">
					<h4 class="omh-sidebar-heading"><?php echo $public_category_parent; ?></h4>
					<ul class="omh-sidebar omh-sidebar-product-categories-list product-categories nectar_widget">
						<?php foreach ($public_categories as $public_category) : ?>
							<li class="omh-sidebar-product-categories-list-item cat-item cat-item-<?php echo $public_category->term_id; ?>">
								<a href="<?php echo shop_url($public_category->slug); ?>" class="<?php echo get_query_var('product_cat') === $public_category->slug ? 'active' : ''; ?>"><?php echo $public_category->name; ?></a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
			<?php endforeach; 
		}?>
	</div>

<script type="text/javascript">
	jQuery(document).on('omhloaded', function() {
		var $ = jQuery;
		var organizationSelect = $('#organization_select_id');
		var publicly_available_chapters = $('#publicly_available_chapters');

		if (organizationSelect.length) {
			organizationSelect.OMHFormField();

			var $organizationField = organizationSelect.get(0).omhFormField;

			$organizationField.$input.on('select2:select', function(event) {

				ga('send', {
				hitType: 'event',
				eventCategory: 'Marketplace-Search',
				eventAction: 'selected',
				eventLabel: event.params.data.name
				});

				selectedOption = event.params.data;
				url = "/" + selectedOption.taxonomy + "/" + selectedOption.slug;
				window.location.href = url;
			});
		}

		if (publicly_available_chapters.length) {
			publicly_available_chapters.on('select2:select', function(event){
				window.location.href = event.params.data.id;
			});
		}
	});


	jQuery('body')
		.on('omhFieldBeforeInit.organization_select_id', function(event, field, settings) {
			_.set(settings, 'select2.templateResult', function(result) {

				if( true === _.get( result, 'loading' ) ) {
					return result.text;
				}

				console.log( 'RESULT', result);
				var response;
				if (result.taxonomy === "chapters"){
					response = [result.text,'<span class="badge-pill badge-primary badge ">','Chapter','</span>'].join("\n");
				}
				if (result.taxonomy === "organizations"){
					response = [result.text,'<span class="badge-pill badge-secondary badge ">','Organization','</span>'].join("\n");
				}

				return response;
			});

			settings.select2.escapeMarkup = function(markup) {
				return markup;
			};
		});
</script>
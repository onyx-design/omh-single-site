<?php
defined( 'ABSPATH' ) || exit;

$college = OMH()->college_factory->get_by_term_slug( $term->slug );
$header_title = $college->get_college_name();
$header_location = "{$college->get_city()}, {$college->get_state()}";

$header_logo = $college->get_logo_image() ?: $header_logo;
$header_image = $college->get_banner_image() ?: $header_image;
?>

<div id="page-header-wrap" data-animate-in-effect="none" data-midnight="light" class="" style="height: 350px;">
	<div class="page-header page-header-colleges not-loaded " data-padding-amt="normal" data-animate-in-effect="none" id="page-header-bg" data-midnight="light" data-text-effect="" data-bg-pos="top" data-alignment="center" data-alignment-v="middle" data-parallax="0" data-height="350" style="background-color: #000000;  height:350px;">
		<div class="page-header-bg-image-wrap" id="nectar-page-header-p-wrap" data-parallax-speed="medium">
			<div class="page-header-bg-image" style="background-image: url(<?php echo $header_image; ?>);background-position: center center;"></div>
			<div class="row-bg-overlay" style="background-color:#000000;opacity: 0.8;position: absolute;top: 0;left: 0;height: 100%;width: 100%;z-index: 10;backface-visibility: hidden;"></div>
		</div>
		<div class="container">
			<div clas="row" style="height:100%;">
				<div class="header-logo">
					<img src="<?php echo $header_logo; ?>" />
				</div>
				<div class="header-title">
					<h1><?php echo $header_title; ?></h1>
				</div>
				<div class="header-subtitle">
					<?php echo $header_subtitle; ?>
				</div>
				<?php if( $header_location ): ?>
					<div class="header-store-location">
						<?php echo $header_location; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
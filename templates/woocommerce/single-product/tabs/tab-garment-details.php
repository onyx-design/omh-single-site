<?php
defined( 'ABSPATH' ) || exit;

global $product;

$garment_products = [];

foreach( $product->get_garments() as $garment ) {

	if( $garment->get_garment_style() ) {
		$garment_product_id = $garment->get_garment_style()->get_garment_product_id();	
	}
	

	if( !array_key_exists( $garment_product_id, $garment_products ) && $garment->get_garment_style() ) {

		$garment_products[ $garment_product_id ] = $garment->get_garment_style()->get_garment_product();
	}
}

?>
<div class="garment-product-details-wrap">
	<?php

		foreach( $garment_products as $garment_product_id => $garment_product ) {

			$garment_brand = $garment_product->get_garment_brand();
			?>
			<div class="garment-product-details" data-garment-product-id="<?php echo $garment_product_id; ?>">
				<h5 class="garment-product-details-title"><?php echo $garment_product->get_product_name(); ?></h5>
				<div class="garment-product-detail garment-product-detail-description">
					<p>
						<?php echo $garment_product->get_product_long_description(); ?>
					</p>
				</div>
				<div class="garment-product-detail garment-product-detail-material">
					<label class="garment-product-detail-header">Material</label>
					<p>
						<?php echo $garment_product->get_material(); ?>
					</p>
				</div>
				<div class="garment-product-detail garment-product-detail-brand">
					<label class="garment-product-detail-header">Brand</label>
					<p>
						<?php echo $garment_brand->get_brand_name(); ?>
					</p>
				</div>
			</div>
			<?php
		}
	?>
</div>


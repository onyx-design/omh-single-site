<?php
defined( 'ABSPATH' ) || exit;

global $product;

$product_price_tiers = $product->get_product_price_tiers();

$product_cart_qty = $product->get_cart_quantity();

?>
<p>
	The following bulk pricing discounts are based on the total quantity of all garments and sizes of <strong>this product</strong> in your order (not total order quantity).
</p>
<div class="omh-table-wrap--garment-price-tiers">
	<div class="omh-table-wrap-inner">
		<table id="BulkPricingTablePDP" class="table omh-table table-equal-width table--price-quantity-tiers pricing-tiers pricing-tiers--quantity" data-orig-qty="<?php echo $product_cart_qty;?>">
			<thead>
				<tr class="omh-table-row price-tiers__preview-row">
					<th>Preview<span class="current-qty">Qty in Cart</span></th>
					
					<?php

						foreach( $product_price_tiers as $index => $min_qty ) {
							$tier_index = $index + 1;
							
							echo '<th class="qty-preview" data-tier="'.$tier_index.'"><span class="preview-qty" data-tier="'.$tier_index.'"></span><span class="current-qty"></span></th>';
						}
					?>
				</tr>
				<tr class="omh-table-row price-tiers__values-row">
					<th>Min Qty</th>
					<?php

						foreach( $product_price_tiers as $index => $min_qty ) {
							$tier_index = $index + 1;
							echo '<th data-tier="'.$tier_index.'">'.$min_qty.'</th>';
						}
					?>
				</tr>
			</thead>
			<tbody>

				<?php

					foreach( $product->get_garments() as $product_garment ) {

						echo '<tr class="omh-table-row price-tiers__garment-row" data-garment-style-id="' . $product_garment->get_garment_style_id() . '">';

						// Output garment thumb col
						$image_front = $product_garment->get_image_front();
						$img_src = wp_get_attachment_image_src( $image_front['id'] );
						$garment_swatch_img = '<img src="' . $img_src[0] . '" />';

							echo '<td class="garment-image">'.$garment_swatch_img.'</td>';

							foreach( $product_garment->get_pricing() as $index => $tier_price ) {
								$tier_index = $index + 1;
								echo '<td data-tier="'.$tier_index.'" class="price-val">'.$tier_price.'</td>';
							}

						echo '</tr>';
					}
				?>
				
			</tbody>
		</table>
		
	</div>
</div>
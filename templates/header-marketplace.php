<?php
defined('ABSPATH') || exit;
global $post;
wp_nonce_field('omh_ajax_search_nonce', 'omh_ajax_search_nonce');


$header_title = "Shop Marketplace";

$header_subtitle = 'Browse products from all chapters on Merch House';

?>

<div class="row page-header-no-bg marketplace-header" data-alignment="left">
	<div class="container">
		<div class="col span_9 section-title">
			<div class="omh-header-content">
				<div class="header-titles-wrap">
					<h1 class="header-title py-0"><?php echo $header_title; ?></h1>
					<h3 class="header-subtitle"><?php echo $header_subtitle; ?></h3>
				</div>
			</div>
		</div>
		<div class="col span_3">
			<div class="inner-wrap">
			</div>
		</div>
	</div>
</div>

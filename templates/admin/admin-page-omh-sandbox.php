<?php
/**
 * Template Name: Sandbox
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
set_time_limit( 600 );
ini_set("max_execution_time", 600 );
ini_set("log_errors", 1); 

// Easy Line breaks and text output
define( 'BR', "<br />");
$br = "<br />";
if( !function_exists('br') ) {function br($msg='') {echo ( is_scalar( $msg ) ? ( is_bool( $msg ) ? boolstr( $msg )."<br />" : "$msg<br />" ) : print_r( $msg, true ) );}}
if( !function_exists('boolstr') ) {function boolstr($b) {return $b ? 'true' : 'false';}}

?>
<h1>OMH Dev Sandbox</h1>
<code style="white-space:pre;">
<?php 
	global $wpdb;

	$follower = OMH_Factory_Follower::query( 
		array(
			'where' => array(
				array(
					'name'	=> 'follower_email',
					'value'	=> 'raysarno@gmail.com'
				),
				array(
					'name'	=> 'following_id',
					'value'	=> '646'
				),
				array(
					'name'	=> 'following_type',
					'value'	=> 'chapter'
				)
			),
			'return' => 'count'
		)
	);
	br( $follower );

	/* 
	function validate_order_credit( OMH_Model_Credit $credit_row ) {
		global $wpdb;

		if( !( $credit_row instanceof OMH_Model_Credit ) 
			|| 'credit' !== $credit_row->get_type() 
			|| empty( $credit_row )
		) {
			// Invalid $credit_row
			return false;
		}

		$chapter_credits = OMH()->credit_factory->calculate_order_credit( $credit_row->get_order_id(), 'credit', false );

		$calculated_credit = $chapter_credits[ $credit_row->get_chapter_id() ] ?? 0;

		$is_correct = ( $calculated_credit == $credit_row->get_amount() );

		$valid_text = $is_correct ? 'CORRECT' : 'NOT-CRT';
		
		br( "{$valid_text} - ID {$credit_row->get_id()} - Orig {$credit_row->get_amount()} - new {$calculated_credit}" );

		$wpdb->update( 'wp_omh_credit_check2', array( 'check_amount2' => $calculated_credit ), array( 'ID' => $credit_row->get_id() ), '%d', '%d' );
		
		return $is_correct;

	}

	function fill_order_subtotal( $credit_row ) {

		$result = 'Order ID NULL';

		if( $order_id = $credit_row['order_id'] ) {

			$result = 'Order NULL';

			if( $order = wc_get_order( $order_id ) ) {

				$result = 'Subtotal empty';

				global $wpdb;

				$subtotal_cents = intval( $order->get_subtotal() * 100 );

				if( !empty( $subtotal_cents ) ) {

					$result = 'UPDATE FAILED';

					if( $wpdb->update( 'wp_omh_credit_check2', array( 'order_subtotal' => $subtotal_cents ), array( 'ID' => $credit_row['ID'] ), '%d', '%d' ) ) {
						
						$result = 'UPDATED';
					};
				}
			}
		}

		return $result;
	}

	$order_credits = OMH()->credit_factory->query( 
		array( 
			'posts_per_page'	=> -1,
			'where' => array(
				array( 
					'name'	=> 'type',
					'value'	=> 'credit'
				)
			)
		), 
		true 
	);

	// $order_credits = $wpdb->get_results( 'SELECT ID, order_id FROM wp_omh_credit_check2 WHERE type = "credit"', ARRAY_A );
	
	$total_rows = count( $order_credits );
	br( 'total credit rows: ' . OMH()->credit_factory->total_rows() );
	
	// br( $order_credits[1]->get_order_id() );
	// br( OMH()->credit_factory->calculate_order_credit( $order_credits[1]->get_order_id(), 'credit', false ) );
	$correct_rows = 0;
	$subtotal_results = array();
	foreach( $order_credits as $credit_row ) {
		if( validate_order_credit( $credit_row ) ) {
			$correct_rows++;
		}
		
		// $result = fill_order_subtotal( $credit_row );

		// if( !array_key_exists( $result, $subtotal_results ) ) {
		// 	$subtotal_results[ $result ] = 0;
		// }

		// $subtotal_results[ $result ]++;
	}



	br();
	br();
	br( 'total read rows: ' . count( $order_credits ) );
	br( 'total correct rows: ' . $correct_rows );
	// br( 'fill_order_subtotal results');
	// br( $subtotal_results );


	*/

	// $product_id = 10525;

	// $new_order = OMH_Order_Bulk::create_bulk_order( $product_id, array( 'quantities' => array( 'qty_10532' => 10 ) ) );

	// br( $new_order );

	// $order = new OMH_Order();

	// br( $order->get_id() );

	// $order->save();

	// br( $order->get_id() );

	// br( wc_get_product( 8744 ) );

	// br( OMH_Product_Manager::get_products( 'retail' ) );

	// $product = new OMH_Product_Variable_Retail( 8718 );
	// $product->save();

	// $product = wc_get_product( 8718 );
	// br( $product );

	br();
	br();
	br();
	br( "Exec time:" . ox_exec_time() );
?>
</code>
<script type="text/javascript">
</script>
<?php ?>
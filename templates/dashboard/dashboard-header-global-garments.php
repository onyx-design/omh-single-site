<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div class="omh-store-credit-total text-right">
	<?php
		// Add Modal Button to Global Garment Pages
		echo OMH_HTML_UI_Button::factory(
    		array(
    			'color'		=> 'primary',
    			'size'		=> 'small',
    			'label'		=> 'Add Global Garment',
    			'attrs'		=> array(
    				'data-toggle'	=> 'modal',
    				'data-target'	=> '#global-garment-modal'
    			)
    		)
    	)->render();
	?>
</div>

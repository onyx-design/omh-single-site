<?php
if ( ! defined( 'ABSPATH' ) ) {
exit; // Exit if accessed directly
}

global $post;
?>
	<div class="onyx-page-overlay"></div>

	<nav id="mh-dashboard-menu" class="onyx-drawer onyx-drawer--left onyx-drawer--dashboard-menu dashboard-drawer navbar navbar-dark">

		<div class="drawer-section drawer-section-nav-spacer site-nav-height">
		</div>

		<div class="drawer-section drawer-section-profile nav navbar-nav">
			<button class="btn onyx-drawer__toggle dashboard-drawer-toggle nav-link pr-0 d-inline-block d-lg-none d-xl-none pt-1" type="button" data-drawer-toggle="dashboard-menu" aria-controls="mh-dashboard-menu" aria-label="Toggle navigation">
				<i class="fa fa-chevron-left m-0"></i>
			</button>
			<ul class="nav navbar-nav flex-column">
				<li class="nav-item dashboard-chapter-display">
					<?php if( is_super_admin() ):
						$primary_chapter = OMH()->session->get_chapter();

						$session_chapter = array(
							$primary_chapter->get_id() => OMH()->chapter_factory->read( $primary_chapter->get_id() )->get_chapter_name()
						);

						echo OMH_HTML_UI_Select2::factory(
							array(
								'input_id'			=> 'session_chapter_id',
								'label'				=> false,
								'preselect_value'	=> $session_chapter,
								'placeholder'		=> 'Select a Chapter',
								'search_model'		=> 'chapters',
								'class'				=> 'mb-0 px-2 admin-chapter-select'
							)
						);
					?>
					<?php elseif( $omh_user->has_house_role() && $chapter = OMH_Session::get_chapter() ): ?>
						<a class="nav-link mt-1" href="<?php echo $chapter->get_term_link();?>" target="_blank" title="View <?php echo $chapter->get_chapter_name(); ?> storefront">
							<span class="dashboard-drawer-organization-crest" style="background-image: url( <?php echo $chapter->get_organization()->get_logo_image(); ?>">
							</span>

							<span class="dashboard-chapter-name">
							<?php
								$organization = $chapter->get_org();
								$college = $chapter->get_college();

								if( $organization && $college && ( $org_greek_letters = $organization->get_org_greek_letters() ) ) {

									?>
										<?php echo $college->get_college_shortname(); ?>
										<span class="greek-letters"><?php echo $org_greek_letters; ?></span>
									<?php
								} else {
									echo $chapter->get_chapter_name();
								}
							?>
							</span>
						</a>
					<?php endif; ?>
				</li>
				<li class="nav-item">
					<a class="nav-link dashboard-profile-link" href="<?php echo OMH()->dashboard_url( 'account-details' ); ?>">
						<i class="bg-warning dashboard-icon-profile dashboard-user-initials"><?php echo $omh_user->get_initials(); ?></i>
						<span class="dashboard-user-fullname"><?php echo $omh_user->get_full_name(); ?></span>
					</a>  
					<span class="dashboard-drawer-role-badge dashboard-user-role"><?php echo $omh_user->get_house_role_badge(); ?></span>
				</li>
			</ul>
		</div>

		<div class="onyx-drawer__content">

			<?php if( $omh_user->has_role( 'house_admin' ) && $chapter = OMH_Session::get_chapter() ): ?>
				<div class="drawer-section drawer-menu">

					<ul class="nav navbar-nav flex-column">
						<li class="nav-item">
							<a class="nav-link <?php echo $post->post_name === 'dashboard' ? 'active' : ''; ?>" href="<?php echo OMH()->dashboard_url(); ?>">
								<i class="fa fa-tachometer"></i>
								Dashboard
							</a>  
						</li>
					</ul>

					<h6 class="text-uppercase drawer-menu-heading font-weight-bold text-muted">Manage Store</h6>

					<ul class="nav navbar-nav flex-column">
						<li class="nav-item">
							<a class="nav-link <?php echo get_query_var( 'pagename' ) === 'create-product' ? 'active' : ''; ?>" href="<?php echo OMH()->dashboard_url( 'create-product' ); ?>">Submit Design</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo get_query_var( 'pagename' ) === 'retail-products' ? 'active' : ''; ?>" href="<?php echo OMH()->dashboard_url( 'retail-products' ); ?>">Manage Retail</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo get_query_var( 'pagename' ) === 'bulk-orders' ? 'active' : ''; ?>" href="<?php echo OMH()->dashboard_url( 'bulk-orders' ); ?>">Bulk Orders</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo get_query_var( 'pagename' ) === 'campaign-products' ? 'active' : ''; ?>" href="<?php echo OMH()->dashboard_url( 'campaign-products' ); ?>">Campaigns</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo get_query_var( 'pagename' ) === 'store-credit' ? 'active' : ''; ?>" href="<?php echo OMH()->dashboard_url( 'store-credit' ); ?>">Store Credit</a>
						</li>
						<!-- // dev:activate -->
						<li class="nav-item">
							<a class="nav-link <?php echo get_query_var( 'pagename' ) === 'members' ? 'active' : ''; ?>" href="<?php echo OMH()->dashboard_url( 'members' ); ?>">Members</a>
						</li>

					</ul>
				</div>
			<?php endif; ?>

			<div class="drawer-section drawer-menu">
				<h6 class="text-uppercase drawer-menu-heading font-weight-bold text-muted">My Account</h6>

				<ul class="nav navbar-nav flex-column">
					<li class="nav-item">
						<a class="nav-link <?php echo in_array( get_query_var( 'pagename' ), array( 'account-details', 'password-change', 'my-addresses' ) ) ? 'active' : ''; ?>" href="<?php echo OMH()->dashboard_url( 'account-details' ); ?>">Account Details</a>
					</li>
					<li class="nav-item">
						<a class="nav-link <?php echo get_query_var( 'pagename' ) === 'my-orders' ? 'active' : ''; ?>" href="<?php echo OMH()->dashboard_url( 'my-orders' ); ?>">My Orders</a>
					</li>
				</ul>
			</div>

			<?php if( is_super_admin() ) { ?>
				<div class="drawer-section drawer-menu">
					<h6 class="text-uppercase drawer-menu-heading font-weight-bold text-muted">Merch House Admin</h6>

					<ul class="nav navbar-nav flex-column">
						<li class="nav-item">
							<a class="nav-link <?php echo get_query_var( 'pagename' ) === 'products' ? 'active' : ''; ?>" href="<?php echo OMH()->mh_admin_url( 'products' ); ?>">Products</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo get_query_var( 'pagename' ) === 'orders' ? 'active' : ''; ?>" href="<?php echo OMH()->mh_admin_url( 'orders' ); ?>">Orders</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo get_query_var( 'pagename' ) === 'users' ? 'active' : ''; ?>" href="<?php echo OMH()->mh_admin_url( 'users' ); ?>">Users</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo in_array( get_query_var( 'pagename' ), array( 'chapters', 'organizations', 'colleges' ) ) ? 'active' : ''; ?>" href="<?php echo OMH()->mh_admin_url( 'chapters' ); ?>">Chapters</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo ( 0 === strpos( get_query_var( 'pagename' ), 'global-garments' ) ) ? 'active' : ''; ?>" href="<?php echo OMH()->mh_admin_url( 'global-garments' ); ?>">Garments</a>
						</li>
					</ul>
				</div>
			<?php } ?>

			<div class="logout-nav drawer-section drawer-menu">
				<ul class="nav navbar-nav flex-column">
					<li class="nav-item">
						<a class="nav-link" href="<?php echo wp_logout_url(); ?>">
							<i class="fa fa-sign-out"></i>
							Logout
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
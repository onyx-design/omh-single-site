<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$allowed_roles = false;

$header_tabs = array(
	'garment_products' 	=> array(
		'name'			=> 'Products',
		'active'		=> true,
		'destination'	=> OMH()->mh_admin_url( 'global-garments/products' )
	),
	'garment_styles'	=> array(
		'name'			=> 'Styles',
		'destination'	=> OMH()->mh_admin_url( 'global-garments/styles' )
	),
	'garment_brands' 	=> array(
		'name'			=> 'Brands',
		'destination'	=> OMH()->mh_admin_url( 'global-garments/brands' )
	),
	'garment_colors' 	=> array(
		'name'			=> 'Colors',
		'destination'	=> OMH()->mh_admin_url( 'global-garments/colors' )
	),
);

// Add Global Garment Button in Dashboard Header
ob_start();
include_once( OMH()->dashboard_template_path( 'dashboard-header-global-garments' ) );
$omh_header_extra_content = ob_get_clean();

// Add Global Garment Modal in Dashboard Footer
ob_start();
include_once( OMH()->dashboard_template_path( 'dashboard-footer-global-garments' ) );
$omh_footer_extra_content = ob_get_clean();
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row">
			
			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">
						<?php
							echo OMH_Table::factory(
								array(
									'type'	=> 'Garment_Products',
								)
							)->get_ui_table();
						?>
					</div>
				</div>
			</div>

			<!-- Add Global Garment Product Modal -->
			<!-- <div class="modal fade mh-modal" id="global-garment-modal" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="global-garment-modal-title">Add Global Garment Product</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form id="mh-form-add-garment-product" class="mh-form form-not-initialized needs-validation" data-ajax-action="add_garment_product" data-form-nonce="<?php echo wp_create_nonce( 'add-garment-product' ); ?>">

								<div class="row">
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'product_name',
												'label'			=> 'Product Name',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'product_slug',
												'label'			=> 'Product Slug',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-6">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_type'	=> 'number',
												'input_id'		=> 'inksoft_product_id',
												'label'			=> 'InkSoft Product ID',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-6">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'inksoft_sku',
												'label'			=> 'InkSoft SKU',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-6">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'manufacturer_sku',
												'label'			=> 'Manufacturer SKU',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-6">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'garment_brand_id',
												'label'			=> 'Brand ID',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Textarea::factory(
											array(
												'input_id'		=> 'product_long_description',
												'label'			=> 'Product Long Description',
												'value'			=> '',
												'required'		=> true,
											)
										);

									?>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-reset-form'	=> 'mh-form-add-garment-product',
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-add-garment-product',
										'label'		=> 'Save',
										'class' 	=> 'mh-forms-submit'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div> -->
			<?php
				include( OMH()->dashboard_template_path( 'dashboard-footer' ) );
			?>

		</div>
	</div>
</div>
<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
</script>
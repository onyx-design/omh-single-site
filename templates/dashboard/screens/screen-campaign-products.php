<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$allowed_roles = array( 'house_admin' );

$omh_breadcrumbs = array(
	array(
		'label'		=> OMH_Session::get_chapter()->get_chapter_name(),
		'url'		=> OMH()->dashboard_url()
	),
	array(
		'label'		=> 'Campaigns',
		'active'	=> true
	)
);

$header_tabs = array(
	'products'		=> array(
		'name'			=> 'Products',
		'active'		=> true,
		'destination'	=> OMH()->dashboard_url( 'campaign-products' )
	),
	'orders'		=> array(
		'name'			=> 'Orders',
		'active'		=> false,
		'destination'	=> OMH()->dashboard_url( 'campaign-orders' )
	)
);

$omh_page_title = 'Campaigns';

// Make sure there aren't any stray notices
$omh_notices = array();

if( isset( $_GET['created'] ) && $_GET['created'] ) {

	if(	$product = wc_get_product( $_GET['created'] ) ) {

		if( $created_by_user = get_user_by( 'ID', $product->get_created_by() ) ) {

			$omh_notices[] = array(
				'text'	=> 'New product created. The completed proofs will be sent to <a href="mailto:' . $created_by_user->get_email() . '">' . $created_by_user->get_email() . '</a>. Email <a href="mailto:art@merch.house">art@merch.house</a> with additional files or questions.',
				'color'	=> 'success'
			);
		} else {

			$omh_notices[] = array(
				'text'	=> 'New product created. There was a problem updating the email on the product. Please contact <a href="mailto:art@merch.house">art@merch.house</a> to let us know where the complete proofs should be sent to. Email <a href="mailto:art@merch.house">art@merch.house</a> with additional files or questions.',
				'color'	=> 'success'
			);
		}
	} else {
		$omh_notices[] = array(
			'text'	=> 'There seems to be an issue creating your product. Please contact <a href="mailto:admin@merch.house">admin@merch.house</a> for more information.',
			'color'	=> 'danger'
		);
	}
}

if( isset( $_GET['updated'] ) ? $_GET['updated'] : false ) {

	$omh_product = isset( $_GET['id'] ) ? wc_get_product( $_GET['id'] ) : false;

	$product_name = $omh_product ? $omh_product->get_title() : 'Campaign product';

	$omh_notices[] = array(
		'text'	=> $product_name . ' updated successfully.',
		'color'	=> 'success'
	);
}
?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row">

			<div class="col-md-12">

				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
					
					echo OMH_HTML_UI_Alert::factory(
						array(
							'color'	=> 'info',
							'text'	=> "Your Merch House Community Manager will work with you on setting up your campaign’s end date and target quantity. You can monitor your live campaign’s sales here. ",
							'dismissible'	=> false
						)
					);
				?>

				<div class="card shadow-sm">
					<div class="card-body">
						<?php
							echo OMH_Table::factory(
								array(
									'type'	=> 'Campaign_Products',
								)
							)->get_ui_table();
						?>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
	var omh_security = '<?php echo wp_create_nonce( 'set-featured-product' ); ?>';

	(function($) {
		$(document).ready(function() {

			var $ajax_action = 'omh_set_featured_product',
				$ajax_data = {};

			$(document).on('change', '.featured-product-checkbox', function(e){

				$ajax_data = {
					obj_id		: $( this ).data( 'objectId' ),
					form_data 	: {
						featured_product : $( this ).prop( 'checked' )
					}
				}

				omh.ajax(
					$ajax_action,
					$ajax_data,
					null,
					omh_security
				);

			});
		});
	})(jQuery);
</script>
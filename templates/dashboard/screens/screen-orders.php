<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$allowed_roles = array( 'administrator' );
?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row">

			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">
						<?php
							echo OMH_Table::factory(
								array(
									'type'	=> 'Orders'
								)
							)->get_ui_table();
						?>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
	var omh_security = table_security;
</script>
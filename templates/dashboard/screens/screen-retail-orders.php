<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$allowed_roles = array( 'administrator', 'house_admin' );

$chapter = OMH_Session::get_chapter();

$omh_breadcrumbs = array(
	array(
		'label'		=> $chapter->get_chapter_name(),
		'url'		=> OMH()->dashboard_url()
	),
	array(
		'label'		=> 'Manage Retail',
		'active'	=> true
	),
);

$header_tabs = array(
	'products'		=> array(
		'name'			=> 'Products',
		'active'		=> false,
		'destination'	=> OMH()->dashboard_url( 'retail-products' )
	),
	'orders'		=> array(
		'name'			=> 'Orders',
		'active'		=> true,
		'destination'	=> OMH()->dashboard_url( 'retail-orders' )
	)
);

$omh_page_title = 'Manage Retail';
?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row">

			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">
						<?php
							echo OMH_Table::factory(
								array(
									'type'		=> 'Orders',
									'scopes'	=> array( 'retail', 'chapter' )
								)
							)->get_ui_table( 
								array(
									'force_scope' => array( 'retail', 'chapter' )
								)
							);
						?>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
<script type="text/javascript">

	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
	var omh_security = table_security;
</script>
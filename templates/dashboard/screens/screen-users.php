<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

$allowed_roles = false;

$tab = $query_tab = empty( $_GET['tab'] ) ? null : $_GET['tab'];

$enable_load_data = true;
$enable_ajax_search = true;
?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row">
			
			<div class="col-md-12">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">
						<?php
						echo OMH_HTML_UI_Button::factory(
								array(
									'color'		=> 'primary',
									'size'		=> 'small',
									'label'		=> 'Add User',
									'attrs'		=> array(
										'data-toggle'	=> 'modal',
										'data-target'	=> '#user-modal'
									)
								)
							)->render();
							
							echo OMH_Table::factory(
								array(
									'type'	=> 'Users',
								)
							)->get_ui_table();
						?>
					</div>
				</div>

			</div>

			<!-- Add User Modal -->
			<div class="modal fade mh-modal" id="user-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="user-modal-title">Add User</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form id="mh-form-add-user" class="mh-form form-not-initialized needs-validation row pb-0" data-ajax-action="add_user" data-form-nonce="<?php echo wp_create_nonce( 'add-user' ); ?>">

								<div class="col-md-6">
								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_id'		=> 'first_name',
											'label'			=> 'First Name',
											'required'		=> false,
											'value'			=> '',
										)
									);
								?>
								</div>
								<div class="col-md-6">
								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_id'		=> 'last_name',
											'label'			=> 'Last Name',
											'required'		=> false,
											'value'			=> '',
										)
									);
								?>
								</div>
								<div class="col-md-12">
								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_id'		=> 'email',
											'input_type'	=> 'email',
											'label'			=> 'Email',
											'required'		=> true,
											'value'			=> '',
										)
									);
								?>
								</div>
								<div class="col-md-12">
								<?php
									$primary_chapter = OMH()->session->get_chapter();

									$session_chapter = array(
										$primary_chapter->get_id() => OMH()->chapter_factory->read( $primary_chapter->get_id() )->get_chapter_name()
									);

									echo OMH_HTML_UI_Select2::factory(
										array(
											'input_id'			=> 'chapter_id',
											'label'				=> 'Chapter',
											'required'			=> false,
											'value'				=> $session_chapter,
											'preselect_value'	=> $session_chapter,
											'placeholder'		=> 'Select a Chapter',
											'search_model'		=> 'chapters',
										)
									);
								?>
								</div>
								<div class="col-md-12">
								<?php
									echo OMH_HTML_UI_Select::factory(
										array(
											'input_id'			=> 'role',
											'label'				=> 'Role',
											'required'			=> true,
											'value'				=> 'house_admin',
											// 'disabled'			=> true,
											// 'readonly'			=> true,
											'select_options'	=> OMH_User::get_house_roles()
										)
									);
								?>
								</div>
								<div class="col-md-12">
								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_id'		=> 'send_user_activation_email',
											'input_type'	=> 'checkbox',
											'label'			=> 'Send User Activation Email',
											'required'		=> false,
											'value'			=> true,
											'input_attrs'	=> array(
												'checked'		=> true
											)
										)
									);
								?>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-add-user',
										'label'		=> 'Save',
										'class' 	=> 'mh-forms-submit'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
	var omh_security = '<?php echo wp_create_nonce( 'manage-users' ); ?>';
	var table_select2 = '<?php echo wp_create_nonce( 'manage-users' ); ?>';

	var $usersTable = jQuery( 'div[data-table-type="Users"]');
	var $tableFields;

	function omhInitTableFields() {
 
		$tableFields = $usersTable.find('select.omh-field-input');

		$tableFields.OMHFormField();

		$tableFields.on('select2:select', function(event) {
			var $eventTarget = jQuery(event.target),
				ajax_action,
				ajax_obj = {
					user_id: parseInt( jQuery( event.target ).closest('.omh-table-row').data('id') )
				};

			if( $eventTarget.hasClass( 'omh-user-role-select' ) ) {

				ajax_action = 'omh_set_user_role';
				ajax_obj.new_role = event.params.data.id;
			} else if( $eventTarget.hasClass( 'omh-user-chapter-id' ) ) {

				ajax_action = 'omh_set_user_chapter';
				ajax_obj.chapter = event.params.data;
			} else {
				return false;
			}

			omh.ajax(
				ajax_action,
				ajax_obj,
				null,
				table_select2
			);
		} );
	}

	(function($){
		$(document).ready(function() {

			omhInitTableFields();

			$usersTable.on( 'omh_table_request', function(event) {
				omhInitTableFields();
			} )
		});
	})(jQuery);
</script>
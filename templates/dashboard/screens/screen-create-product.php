<?php
// Exit if accessed directly
defined('ABSPATH') || exit;

$allowed_roles = array('house_admin');

$omh_breadcrumbs = array(
	array(
		'label'		=> OMH_Session::get_chapter()->get_chapter_name(),
		'url'		=> OMH()->dashboard_url()
	),
	array(
		'label'		=> 'Submit Design',
		'active'	=> true
	)
);

$omh_page_title = 'Submit Design';

$omh_notices = array(
	array(
		'color'	=> 'info',
		'text'	=> "Choose which type of product you’d like for your design. If you’re not sure, text <a href=\"sms:+12138801378\">213-880-1378</a> for help. We can always change it after you submit!",
		'dismissible'	=> false,
		'attrs'	=> array(
			'data-notice-key'	=> 'create_product_help'
		)
	)
);
?>

<div id="mh-dashboard-wrap">
	<div class="container">

		<div id="mh-dashboard" class="row screen--create-product">
			<div class="col">
				<?php
				include(OMH()->dashboard_template_path('dashboard-header'));
				?>

				<div class="row omh-sales-type-select" id="mh-design-type-wrap">
					<div class="col-12 col-md-4">
						<div class="card sales-type-card" data-sales-type="retail">
							<div class="card-header text-center">
								<h2>Retail</h2>
							</div>
							<div class="card-body text-center">
								<?php
								echo OMH_HTML_UI_Badge::factory(
									array(
										'label' 	=> 'Published on Store',
										'color' 	=> 'info',
										'size'		=> 'lg'
									)
								);
								?>
							</div>
							<ul class="list-group list-group-flush">
								<li class="list-group-item">No minimums, product stays on store for individual purchase</li>
								<li class="list-group-item">Earn 5% credit on every retail sale!</li>
								<li class="list-group-item">Limited blank product options</li>
								<li class="list-group-item">Print only (no embroidery)</li>
							</ul>
						</div>
					</div>
					<div class="col-12 col-md-4">
						<div class="card sales-type-card" data-sales-type="bulk">
							<div class="card-header text-center">
								<h2>Bulk</h2>
							</div>
							<div class="card-body text-center">
								<?php
								echo OMH_HTML_UI_Badge::factory(
									array(
										'label' 	=> 'Best Prices',
										'color' 	=> 'info',
										'size'		=> 'lg'
									)
								);
								?>
							</div>
							<ul class="list-group list-group-flush">
								<li class="list-group-item">12 piece minimums, the more you order the more you save</li>
								<li class="list-group-item">Fast 5-7 business day turnaround</li>
								<li class="list-group-item">Full blank product options</li>
								<li class="list-group-item">Screenprint and embroidery</li>
							</ul>
						</div>
					</div>
					<div class="col-12 col-md-4">
						<div class="card sales-type-card" data-sales-type="campaign">
							<div class="card-header text-center">
								<h2>Campaign</h2>
							</div>
							<div class="card-body text-center">
								<?php
								echo OMH_HTML_UI_Badge::factory(
									array(
										'label' 	=> 'Individual Order Link',
										'color' 	=> 'info',
										'size'		=> 'lg'
									)
								);
								?>
							</div>
							<ul class="list-group list-group-flush">
								<li class="list-group-item">Bulk prices, but you get a link to share and each person pays individually</li>
								<li class="list-group-item">Set a time period and target quantity</li>
								<li class="list-group-item">Full blank product options</li>
								<li class="list-group-item">Screenprint and embroidery</li>
							</ul>
						</div>
					</div>
					<div class="col-12 text-center">
						<?php
						echo OMH_HTML_UI_Button::factory(
							array(
								'class' 	=> 'hide-overlay my-4',
								'color'		=> 'primary',
								'label'		=> 'Start Design',
								'disabled' 	=> true
							)
						);
						?>
					</div>
				</div>
				<form id="mh-create-product-form" class="mh-form form-not-initialized needs-validation" data-submit-type="form" data-ajax-action="create_product" data-form-nonce="<?php echo wp_create_nonce('create-product'); ?>" style="display:none;">
					<div class="row">
						<?php
						echo OMH_HTML_UI_Input::factory(
							array(
								'input_id'		=> 'sales_type',
								'input_type'	=> 'hidden',
								'class'			=> 'd-none',
								'label'			=> false,
								'required' 		=> true,
							)
						);
						?>
						<div class="col">
							<div class="mh-create-product-design-copy">
								<p>
									Click Change Product to browse our extensive blank catalog. If you would like your design on more than one garment, type in the second garment in the comment box below.
								</p>
								<p>
									You can add images, art or text to the design studio OR just drop in files on the bottom left. A designer will send over a mockup by this time tomorrrow!
								</p>
							</div>
							<div class="card shadow-sm">
								<div class="card-body">
									<div class="mh-create-product-wrapper show-form">

										<div class="row">
											<div class="col">
												<div class="form-group">
													<label>Design Studio</label>
													<div align="center" id="embeddedDesigner"></div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col">
												<?php

												?>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">

												<?php
												$allowed_media_types = array(
													// JPG or JPEG
													'application/jpeg',
													'image/jpeg',
													'.jpeg',
													'application/jpg',
													'image/jpg',
													'.jpg',

													// PDF
													'application/pdf',
													'.pdf',

													// AI
													'application/illustrator',
													'application/postscript',
													'.ai',

													// PSD
													'application/photoshop',
													'image/vnd.adobe.photoshop',
													'image/x-psd',
													'.psd',

													// PNG
													'image/png',
													'.png',

													// SVG
													'image/svg+xml',
													'.svg',

													// TIFF
													'image/tiff',
													'.tiff',

													// GIF
													'image/gif',
													'.gif',
												);
												?>

												<div class="form-group form-group-file-img">
													<div class="row">
														<div class="col-md-12">
															<div class="custom-file">
																<label>Upload Design Images / Inspiration</label>
																<div class="input-group mb-2">
																	<div class="custom-file custom-file-hide-default-label">
																		<input type="file" data-omh-field-type="file" class="custom-file-input never-input never-changed orig-value omh-field-input" name="file_upload_1" accept="<?php echo implode(', ', $allowed_media_types); ?>" />
																		<label class="custom-file-label">Choose New File</label>
																	</div>
																	<div class="input-group-append">
																		<button class="btn btn-danger input-reset-btn" type="button" for="file_upload_1" style="display:none;"><i class="fa fa-close"></i></button>
																		<button class="btn btn-secondary input-focus-btn" type="button" for="file_upload_1">Browse</button>
																	</div>
																</div>

																<div class="input-group mb-2">
																	<div class="custom-file custom-file-hide-default-label">
																		<input type="file" data-omh-field-type="file" class="custom-file-input omh-field-input never-input never-changed orig-value" name="file_upload_2" accept="<?php echo implode(', ', $allowed_media_types); ?>" />
																		<label class="custom-file-label">Choose New File</label>
																	</div>
																	<div class="input-group-append">
																		<button class="btn btn-danger input-reset-btn" type="button" for="file_upload_2" style="display:none;"><i class="fa fa-close"></i></button>
																		<button class="btn btn-secondary input-focus-btn" type="button" for="file_upload_2">Browse</button>
																	</div>
																</div>

																<div class="input-group mb-2">
																	<div class="custom-file custom-file-hide-default-label">
																		<input type="file" data-omh-field-type="file" class="custom-file-input omh-field-input never-input never-changed orig-value" name="file_upload_3" accept="<?php echo implode(', ', $allowed_media_types); ?>" />
																		<label class="custom-file-label">Choose New File</label>
																	</div>
																	<div class="input-group-append">
																		<button class="btn btn-danger input-reset-btn" type="button" for="file_upload_3" style="display:none;"><i class="fa fa-close"></i></button>
																		<button class="btn btn-secondary input-focus-btn" type="button" for="file_upload_3">Browse</button>
																	</div>
																</div>

																<div class="input-group mb-2">
																	<div class="custom-file custom-file-hide-default-label">
																		<input type="file" data-omh-field-type="file" class="custom-file-input never-input never-changed orig-value omh-field-input" name="file_upload_4" accept="<?php echo implode(', ', $allowed_media_types); ?>" />
																		<label class="custom-file-label">Choose New File</label>
																	</div>
																	<div class="input-group-append">
																		<button class="btn btn-danger input-reset-btn" type="button" for="file_upload_4" style="display:none;"><i class="fa fa-close"></i></button>
																		<button class="btn btn-secondary input-focus-btn" type="button" for="file_upload_4">Browse</button>
																	</div>
																</div>
															</div>
															<small class="form-text text-muted">Acceptable File Types: .jpg .jpeg .psd .ai .pdf .png .gif .tiff</small>
														</div>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<?php

												echo OMH_HTML_UI_Input::factory(
													array(
														'input_id'			=> 'product_name',
														'label'				=> 'Product Name',
														'required' 			=> true,
														'value'				=> '',
													)
												);

												echo OMH_HTML_UI_Textarea::factory(
													array(
														'input_id' 	=> 'comments',
														'label' 	=> 'Leave comments for our talented designers. The more detailed, the closer the design will be to your vision!',
														'value' 	=> ''
													)
												);

												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'		=> 'datepicker',
														'input_id'			=> 'deliver_date',
														'label'				=> 'When do you need shirts in hand by?',
														'value'				=> '',
														'class'				=> 'omh-field-wrap--deliver_date'
													)
												);
												?>
											</div>

											<div class="col-md-12">
												<?php
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_id'		=> 'confirm_submission',
														'input_type'	=> 'checkbox',
														'label'			=> 'I have double checked the design, uploads, comments and everything is correct.',
														'required'		=> true,
													)
												);
												?>
											</div>

											<?php
											/**
											 * Hidden Fields from the InkSoft Designer
											 */
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'design_id',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'site_url',
													'value'		=> get_admin_url(),
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'created_by',
													'value'		=> $omh_user->ID,
												)
											);

											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'product_id',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'product_style_id',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'manufacturer',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'manufacturer_sku',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'sku',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'name',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'long_description',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'color',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'sizes',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'base_cost',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'image_url',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'selected_product',
													'value'		=> '',
												)
											);
											echo OMH_HTML_UI_Input::factory(
												array(
													'input_type' => 'hidden',
													'input_id'	=> 'selected_product_style',
													'value'		=> '',
												)
											);
											?>
										</div>

										<div class="text-center">
											<?php
											echo OMH_HTML_UI_Button::factory(
												array(
													'color'		=> 'primary',
													'form'		=> 'mh-create-product-form',
													'value'		=> 'submit',
													'label'		=> 'Submit',
													'class' 	=> array(
														'mh-forms-submit',
														'mh-create-product-form-btn'
													)
												)
											);
											?>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<script>
	// import _ from '../vendor/lodash.min';

	(function($) {

		var productOrderType = '',
			savingDesign = false,
			flashvars = {
				DesignerLocation: "https://images.inksoft.com/designer/html5",
				EnforceBoundaries: "1",
				Background: "",
				VectorOnly: false,
				DigitalPrint: true,
				ScreenPrint: true,
				Embroidery: false,
				MaxScreenPrintColors: "8",
				RoundPrices: false,
				StoreID: "143867", // "84643"
				PublisherID: "8526",
				SessionID: "",
				SessionToken: "",
				CartRetailItemID: "",
				UserID: "1034779",
				UserName: "Admin Merch House",
				UserEmail: "admin@merch.house",
				DesignID: "",
				DefaultProductID: "1000098", // "1000042"
				DefaultProductStyleID: "1001563", // "1001034"
				ProductID: "1000098", // "1000098"
				ProductStyleID: "1001563", // "1001561"
				ProductCategoryID: "1000034", // "1000003"
				ClipArtGalleryID: "",
				DisableAddToCart: true, //false
				DisableUploadImage: false,
				DisableClipArt: false,
				DisableUserArt: false,
				DisableProducts: false,
				DisableDesigns: true, //false
				DisableDistress: true, //false
				DisableResolutionMeter: false,
				DisableUploadVectorArt: false,
				DisableUploadRasterArt: false,
				StartPage: "",
				StartPageCategoryID: "",
				StartPageHTML: "",
				StartBanner: "",
				OrderID: "",
				CartID: "",
				ArtID: "",
				FontID: "",
				Domain: "stores.inksoft.com",
				SSLEnabled: true,
				SSLDomain: "stores.inksoft.com",
				StoreURI: "mhnational", // "merchhouse2"
				Admin: "",
				NextURL: "",
				CartURL: false, // "https://stores.inksoft.com/merchhouse2/Cart",
				OrderSummary: false, //true,
				VideoLink: false, // "http://www.youtube.com/watch?v=EfXICdRwt4E",
				Phone: "661-373-3501",
				WelcomeScreen: "",
				ContactUsLink: "https://merch.house/contact-us", //"/merchhouse2/Stores/Contact",
				WelcomeVideo: "",
				GreetBoxSetting: "",
				HelpVideoOverview: "",
				AutoZoom: true,
				EnableNameNumbers: false, //true,
				AddThisPublisherId: "xa-4fccb0966fef0ba7",
				EnableCartPricing: false,
				EnableCartCheckout: false,
				EnableCartBilling: false,
				EnableCartShipping: false, //true,
				PaymentDisabled: true, //false,
				PaymentRequired: false,
				BillingAddressRequired: false, //true,
				PasswordLength: "4",
				DefaultCountryCode: "US",
				CurrencyCode: "USD",
				CurrencySymbol: "$",
				HideProductPricing: true,
				PB: true,
				HideClipArtNames: true,
				HideDesignNames: true,
				ThemeName: "flat",
				FullScreen: false,
				Version: "3.37.0.0",
				BackgroundColor: "",
				StoreLogo: "//stores.inksoft.com/images/publishers/8526/stores/mhnational/img/logo.png",
				StoreName: "Merch House",
				StoreEmail: "admin@merch.house",
				EnableEZD: false,
				EmbedType: "div"
			};

		// once the user selects a design hide the design type container
		function hideOverlay() {
			$('#mh-design-type-wrap').hide();
			$('#mh-create-product-form').show();

			var selectedSalesType = $('input#sales_type').val();

			// Set correct Inksoft Store ID for retail 
			if ('retail' == selectedSalesType) {

				var retailVars = {
					StoreURI: "merchhousedtg",
					StoreID: "96654",
					DefaultProductID: "1000052",
					DefaultProductStyleID: "1001335",
					ProductID: "1000098",
					ProductStyleID: "1001607",
					ProductCategoryID: "1000051"
				};

				$.extend(flashvars, retailVars);
			}


			// launch the correct designer
			launchDesigner('HTML5DS', flashvars, document.getElementById("embeddedDesigner"));

			omh.Notices.removeNotice('create_product_help');

			omh.Notices.setNotice({
				content: 'Nice! You are submitting a design for a <b>' + selectedSalesType + '</b> product. <br /><br />Not what you wanted? <a class="btn btn-outline-info btn-sm" role="button" href="/dashboard/create-product">Start Over</a>',
				key: 'design_selected_notice'
			});
		}

		function selectDesign() {

			$('.sales-type-card').removeClass('selected');
			// add selected to current class
			$(this).addClass('selected');
			// get radio value
			var selected_design = $(this).attr('data-sales-type');
			// set selected value in hidden input
			$('#sales_type').val(selected_design);
			// Set modifier attr on #mh-dashboard
			$('#mh-dashboard').attr('data-selected-sales-type', selected_design);
			// set disabled to false and remove disabled as a class for styling
			$('button.hide-overlay').prop('disabled', false).removeClass('disabled').text('Start Design - ' + selected_design);


		}

		// DO NOT DELETE
		// dev:improve need to figure out how to reload the designer without a page reload
		// function revertSalesTypeSelect() { 
		// 	// from the parent get all radio classes and remove selected
		// 	$('.sales-type-card').removeClass('selected');
		// 	// unset selected value in hidden input
		// 	$('#sales_type').val('');
		// 	// Set modifier attr on #mh-dashboard
		// 	$('#mh-dashboard').attr('data-selected-sales-type', '');
		// 	// set disabled to false and remove disabled as a class for styling
		// 	$('button.hide-overlay').text('Start Design').prop('disabled', true).addClass('disabled');
		// }

		function designEmpty() {
			return state.selectedDesign.isEmpty();
		}

		function saveDesign(event, form, beforeSubmit) {
			beforeSubmit.allowSubmit = false;
			savingDesign = true;

			toggleLoadingOverlay(true);

			$('.mh-create-product-form-btn').addClass('omh-saving-design').html('Saving Design...').prop('disabled', true);

			// If the design is empty, we don't have to submit anything to InkSoft
			if (designEmpty()) {

				updateHiddenFormFields();
				// form.submit( beforeSubmit.submitAction, beforeSubmit.submitAllowValid, false);
			} else {

				// Taken from ui.controllers.SaveDesignController
				var t = {
					designId: state.selectedDesign.design_id,
					productId: state.selectedProductID,
					productStyleId: state.selectedProductStyle.product_style_id,
					sessionId: state.currentSessionID,
					userId: state.activeUserID,
					name: $('.mh-create-product-wrapper .mh-form #product_name').val(),
					notes: $('.mh-create-product-wrapper .mh-form #comments').val(),
					guest_email: "",
					success: function(t, r) {

						state.selectedDesignID = state.selectedDesign.design_id = r.designID,
							ajaxCallEnded("SaveDesignTemplate"),
							state.designSaveResult = r,
							//e.savingDesign = !1, 
							r.sessionID && (state.currentSessionID = parseInt(r.sessionID.trim(), 10)),
							r.userID && (state.activeUserID = parseInt(r.userID, 10)),
							r.sessionToken && (state.currentSessionToken = r.sessionToken.trim()),

							updateHiddenFormFields();


						window.onbeforeunload = null
					},
					error: function(t) {
						window.onbeforeunload = null,
							ajaxCallEnded("SaveDesignTemplate"), alert("There was an unexpected error saving this design.  The error returned was: " + t)

						toggleLoadingOverlay(false);
					}
				};

				state.designer.saveDesign(t);
			}
		}

		function updateHiddenFormFields() {

			var image_url = typeof(state.selectedProductStyle.selectedRegion && state.selectedProductStyle.selectedRegion.imgurl) !== "undefined" ?
				"https:" + state.selectedProductStyle.selectedRegion.imgurl :
				"https://stores.inksoft.com" + state.selectedProductStyle.image_file_path_front + "/500.png";

			// Remove unneeded data
			state.selectedProduct.product_styles = ['removed to save space'];
			state.selectedProduct.defaultStyle = ['removed to save space'];

			state.selectedProductStyle.product_regions = ['removed to save space'];

			// Sometimes the name is wrapped inside a function that must be called
			if (typeof state.selectedProduct.name.$$unwrapTrustedValue === "function") {
				state.selectedProduct.name = state.selectedProduct.name.$$unwrapTrustedValue();
			}

			$('input#product_id').val(state.selectedProduct.product_id);
			$('input#manufacturer').val(state.selectedProduct.manufacturer);
			$('input#manufacturer_sku').val(state.selectedProduct.manufacturer_sku);
			$('input#sku').val(state.selectedProduct.sku);
			$('input#name').val(state.selectedProduct.name);
			$('input#long_description').val(state.selectedProduct.long_description);
			$('input#product_style_id').val(state.selectedProductStyle.product_style_id);
			$('input#color').val(state.selectedProductStyle.color);
			$('input#sizes').val(state.selectedProductStyle.sizes);
			$('input#base_cost').val(state.selectedProductStyle.unit_price);
			$('input#image_url').val(image_url);
			$('input#selected_product').val(JSON.stringify(state.selectedProduct));
			$('input#selected_product_style').val(JSON.stringify(state.selectedProductStyle));

			$('input#design_id').val(state.selectedDesignID);

			$('#mh-create-product-form')[0].omhForm.access.submit(null, null, false);
		}

		// function initCreateProductScreen() {

		// 	// window.scrollTo(0,0);



		// 	// $('.mh-create-product-wrapper .mh-form').on('submit',function(e){

		// 	// 	e.preventDefault();
		// 	// 	e.stopPropagation();
		// 	// });

		// 	// $('.mh-create-product-wrapper .mh-create-product-form-btn').click(function(e){

		// 	// 	e.preventDefault();
		// 	// 	e.stopPropagation();

		// 	// 	if( !$('.mh-create-product-wrapper .mh-form #product_name').val() ) {

		// 	// 		$('.mh-create-product-wrapper .mh-form #product_name').focus();
		// 	// 	} else {

		// 	// 		if( !savingDesign ) {

		// 	// 			saveDesign();
		// 	// 		}
		// 	// 	}
		// 	// });


		// }

		function toggleLoadingOverlay(on) {

			$('body').toggleClass('loading-open', on);
		}

		// $(document).ready(initCreateProductScreen);

		// INITIALIZE THE SCREEN
		(function($) {

			if ($('.mh-create-product-wrapper').length) {
				// Cache lodash before Inksoft loads older version
				var lodash;
				lodashCacheInterval = setInterval(function() {
					if (window._.VERSION.substring(0, 1) === '4') {
						lodash = window._;
						clearInterval(lodashCacheInterval);
					}
				});
				// Reinstate newer lodash version
				lodashVersionInterval = setInterval(function() {
					if (window._.VERSION.substring(0, 1) !== '4') {
						window._ = lodash;
						clearInterval(lodashVersionInterval);
					}
				});
			}
		})(jQuery);

		$(document).on('omh_form_before_submit', saveDesign);

		$('.sales-type-card').on('click', selectDesign);
		$('button.hide-overlay').on('click', hideOverlay);

		return {
			designEmpty: designEmpty,
			saveDesign: saveDesign,
		};
	})(jQuery);
</script>
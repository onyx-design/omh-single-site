<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$allowed_roles = false;

$header_tabs = array(
	'garment_products' 	=> array(
		'name'			=> 'Products',
		'destination'	=> OMH()->mh_admin_url( 'global-garments/products' )
	),
	'garment_styles'	=> array(
		'name'			=> 'Styles',
		'active'		=> true,
		'destination'	=> OMH()->mh_admin_url( 'global-garments/styles' )
	),
	'garment_brands' 	=> array(
		'name'			=> 'Brands',
		'destination'	=> OMH()->mh_admin_url( 'global-garments/brands' )
	),
	'garment_colors' 	=> array(
		'name'			=> 'Colors',
		'destination'	=> OMH()->mh_admin_url( 'global-garments/colors' )
	),
);

// Add Global Garment Button in Dashboard Header
ob_start();
include_once( OMH()->dashboard_template_path( 'dashboard-header-global-garments' ) );
$omh_header_extra_content = ob_get_clean();

// Add Global Garment Modal in Dashboard Footer
ob_start();
include_once( OMH()->dashboard_template_path( 'dashboard-footer-global-garments' ) );
$omh_footer_extra_content = ob_get_clean();
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row">
			
			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">

						<?php
							echo OMH_Table::factory(
								array(
									'type'	=> 'Garment_Styles',
								)
							)->get_ui_table();
						?>

					</div>
				</div>
			</div>

			<!-- Add Global Garment Style Modal -->
			<!-- <div class="modal fade mh-modal" id="global-garment-modal" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="global-garment-modal-title">Add Global Garment Style</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form id="mh-form-add-garment-style" class="mh-form form-not-initialized needs-validation" data-ajax-action="add_garment_style" data-form-nonce="<?php echo wp_create_nonce( 'add-garment-style' ); ?>">

								<div class="row">
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'style_slug',
												'label'			=> 'Style Slug',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-6">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_type'	=> 'number',
												'input_id'		=> 'inksoft_style_id',
												'label'			=> 'InkSoft Style ID',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-6">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'style_name',
												'label'			=> 'Style Name',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-6">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'full_name',
												'label'			=> 'Full Name',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-6">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'color_name',
												'label'			=> 'Color Name',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-6">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'sizes',
												'label'			=> 'Sizes',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-6">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'style_base_cost',
												'label'			=> 'Style Base Cost',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'inksoft_image_url',
												'label'			=> 'InkSoft Image URL',
												'value'			=> '',
												'required'		=> true,
											)
										);

									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'thumbnail_url',
												'label'			=> 'Thumbnail URL',
												'value'			=> '',
												'required'		=> true,
											)
										);

									?>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-reset-form'	=> 'mh-form-add-garment-style',
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-add-garment-style',
										'label'		=> 'Save',
										'class' 	=> 'mh-forms-submit'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div> -->
			<?php
				include( OMH()->dashboard_template_path( 'dashboard-footer' ) );
			?>

		</div>
	</div>
</div>
<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>',
		enabled_garment_style_security = '<?php echo wp_create_nonce( 'enable-global-garment-style' ); ?>';

	(function($){
		$(document).ready(function() {

			var $ajax_action = 'omh_enable_global_garment_style',
				$ajax_data = {};

			$(document).on('change', '.enable-garment-style-checkbox input', function(e) {

				$ajax_data = {
					obj_id		: $( this ).data( 'objectId' ),
					form_data	: {
						enabled_style : $( this ).prop( 'checked' )
					}
				}

				omh.ajax(
					$ajax_action,
					$ajax_data,
					null,
					enabled_garment_style_security
				);
			} );
		});
	})(jQuery);
</script>
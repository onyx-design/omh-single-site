<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$allowed_roles = false;

$omh_page_title = 'Organization Manager';

$header_tabs = array(
	'chapters'	=> array(
		'name'			=> 'Chapters',
		'destination'	=> OMH()->mh_admin_url( 'chapters' ),
	),
	'organizations' 	=> array(
		'name'			=> 'Organizations',
		'destination'	=> OMH()->mh_admin_url( 'organizations' ),
		'active'		=> true,
	),
	'colleges' 	=> array(
		'name'			=> 'Colleges',
		'destination'	=> OMH()->mh_admin_url( 'colleges' ),
	),
);

$enable_load_data = true;
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row">
			
			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">

						<?php
							echo OMH_Table::factory(
								array(
									'type'	=> 'Organizations',
								)
							)->get_ui_table();
						?>

					</div>
				</div>
			</div>

			<!-- Save Organization Modal -->
			<div class="modal fade mh-modal" id="organization-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="organization-modal-title">Save Organization</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form id="mh-form-save-organization" class="mh-form form-not-initialized needs-validation" data-ajax-action="save_organization" data-form-nonce="<?php echo wp_create_nonce( 'save-organization' ); ?>" data-omh-form-model="omh_organizations">
								<div class="row">
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'org_name',
												'label'			=> 'Organization Name',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'org_letters',
												'label'			=> 'Organization Letters',
												'value'			=> '',
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'org_greek_letters',
												'label'			=> 'Organization Greek Letters',
												'value'			=> '',
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'org_shortname',
												'label'			=> 'Organization Shortname',
												'value'			=> '',
											)
										);
									?>
									</div>
									<div class="col-md-6">
										<div class="front-image-file omh-field-wrap omh-field--ajax-file">
											<label for="banner_image_id">Banner Image</label>
											<div class="mh-file-upload-box__file">
												<input class="mh-file-upload field--template omh-field-input" type="file" id="banner_image_id" name="banner_image_id" data-omh-field-type="ajaxFile" />
											</div>
											<div class="mh-file-upload-box__uploading">Uploading</div>
											<div class="mh-file-upload-box__success">Done</div>
											<div class="mh-file-upload-box__error">Error</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class=" omh-field-wrap omh-field--ajax-file">
											<label for="logo_image_id">Logo Image</label>
											<div class="mh-file-upload-box__file">
												<input class="mh-file-upload field--template omh-field-input" type="file" id="logo_image_id" name="logo_image_id" data-omh-field-type="ajaxFile" />
											</div>
											<div class="mh-file-upload-box__uploading">Uploading</div>
											<div class="mh-file-upload-box__success">Done</div>
											<div class="mh-file-upload-box__error">Error</div>
										</div>
									</div>
								</div>
								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'hidden',
											'input_id'		=> 'term_id',
											'value'			=> '',
											'class'			=> 'd-none'
										)
									);
									
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'hidden',
											'input_id'		=> 'ID',
											'value'			=> ''
										)
									);
								?>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-save-organization',
										'label'		=> 'Save',
										'class' 	=> 'mh-forms-submit'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
</script>
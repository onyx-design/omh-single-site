<?php
	// Exit if accessed directly
	defined( 'ABSPATH' ) || exit;

	// If order doesn't exist, redirect everyone
	$order_id = (int) ( empty( $_GET['id'] ) ? null : $_GET['id'] );

	if ( !isset( $order_id )
		|| !( $omh_order = wc_get_order( $order_id ) )
	) {
		wp_redirect( OMH()->dashboard_url( 'orders' ) );
		exit;
	}

	$current_user_order = '';

	// Order Table Filter
	$omh_order_filter = array(
		'order_id' => $order_id,
	);

	$current_user = wp_get_current_user();

	// Determine if customer is a user
	$customer_id = $omh_order->get_user_id();

	// Table Class for when the customer is viewing this table instead of the admin
	$current_user_order = $customer_id == $current_user->ID ? ' current-user-order' : '';

	// Order Context
	// =================================
	$context = 'none';

	if ( $current_user->ID === $customer_id ) {
		$context = 'user';
	} elseif ( $current_user->has_role( 'house_admin' ) ) {
		if ( !empty( $omh_order->get_chapter_line_items( $current_user->get_chapter() ) ) ) {
			$context = 'admin';
		}

	}

	// if ( $context === 'none' ) {
	// 	wp_redirect( OMH()->dashboard_url() );
	// 	exit;
	// }

	// Breadcrumbs
	// ================================
	if( OMH_Session::get_chapter() ){
		$omh_breadcrumbs = array(
			array(
				'label' => OMH_Session::get_chapter()->get_chapter_name(),
				'url'   => OMH()->dashboard_url(),
			),
		);
	}

	if ( is_super_admin() ) {
		$omh_breadcrumbs[] = array(
			'label' => 'Orders',
			'url'   => OMH()->dashboard_url( 'orders' ),
		);
	} elseif ( $context === 'user' ) {
		$omh_breadcrumbs[] = array(
			'label' => 'Orders',
			'url'   => OMH()->dashboard_url( 'my-orders' ),
		);
	} elseif ( $context === 'admin' ) {
		$omh_breadcrumbs[] = array(
			'label' => 'Orders',
			'url'   => OMH()->dashboard_url( 'retail-orders' ),
		);
	}

	$omh_breadcrumbs[] = array(
		'label'  => 'View Order',
		'active' => true,
	);

	// Pull Customer Details

	// =================================
	if ( $customer_id ) { // Valid User

		$customer = new OMH_User( $customer_id );

		$customer_fullname = $customer->get_full_name();
		$customer_email    = $customer->user_email;
	} else { // Guest

		$customer_fullname = $omh_order->get_billing_first_name() . $omh_order->get_billing_last_name();
		$customer_email    = $omh_order->get_billing_email();
	}

	// Customer badge
	// =================================
	$user_badge = $omh_order->get_user_role_badge();

	$omh_page_pretitle = get_the_title();
	$omh_page_title    = "Order #{$omh_order->get_order_number()}";

	// Extra Content

	// =================================

	// Order Status Badge
	$badge_contents           = $omh_order->get_order_status_badge();
	$omh_header_extra_content = OMH_HTML_UI_Badge::factory(
		array(
			'label' => $badge_contents['text'],
			'color' => $badge_contents['color'],
		)
	);

	// Order Tracking Button

	// Configure WC Shipment Tracking

	if ( class_exists( 'WC_Shipment_Tracking_Actions' ) ) {
		$wc_shipment_tracking = WC_Shipment_Tracking_Actions::get_instance();
		$tracking_items       = $wc_shipment_tracking->get_tracking_items( $order_id );

		// Build the tracking data as an omh_notice
		$order_shipped_notice = 'Your Order has been shipped!';

		if ( !empty( $tracking_items ) ) {

			foreach ( $tracking_items as $tracking_item ) {
				$formatted_tracking_item = $wc_shipment_tracking->get_formatted_tracking_item( $order_id, $tracking_item );
				$formatted_tracking_link = $formatted_tracking_item['formatted_tracking_link'];
				$order_shipped_notice .= '<div class="omh-tracking-item">';
				$order_shipped_notice .= ' <span>' . $tracking_item['tracking_provider'] . '</span>';
				$order_shipped_notice .= ' <span>' . $tracking_item['tracking_number'] . '</span>';
				$order_shipped_notice .= ' <span>Shipped: ' . date( 'm/d/Y', $tracking_item['date_shipped'] ) . '</span>';
				$order_shipped_notice .= ' <span>';
				$order_shipped_notice .= OMH_HTML_UI_Button::factory(
					array(
						'color'		=> 'primary',
						'href'		=> $formatted_tracking_link,
						'label'		=> 'Track',
						'size'		=> 'small'
						)
					);
				$order_shipped_notice .= ' </span>';
				$order_shipped_notice .= '</div>';
			}

			$omh_notices[] = array(
				'text'        => $order_shipped_notice,
				'color'       => 'success',
				'dismissible' => true,
				'icon'        => array(),
			);
		}

	}

	// Order Items
	// =================================
	$order_items_table_args = array(
		'type'    => 'Order_Items',
		'filters' => $omh_order_filter,
	);

	if ( $context === 'admin' ) {

		$order_items_table_args['scopes']                = array( 'admin' );
		$order_items_table_args['filters']['chapter_id'] = $chapter->get_id();
	}

	$order_items_table = OMH_Table::factory( $order_items_table_args )->get_ui_table();

	// Order Updates
	// =================================
	$order_updates_table = OMH_Table::factory(
		array(
			'type'    => 'Order_Updates',
			'filters' => $omh_order_filter,
		)
	)->get_ui_table();

	// Order Notes
	// =================================
	$order_notes       = $omh_order->get_customer_note();
	$order_notes_class = '';

	if ( !$order_notes ) {
		$order_notes       = 'No notes from customer.';
		$order_notes_class = 'small text-muted';
	}

?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row <?php echo $current_user_order; ?>">
			<div class="col">

				<?php
					// Dashboard Header
					include OMH()->dashboard_template_path( 'dashboard-header' );

					// Dashboard Notice
					if ( isset( $notice ) ) {echo $notice;}

				?>

				<div class="row">

					<!-- =====
						Left Column
					===== -->
					<div class="col-md-4">

						<!-- === Order Summary === -->
						<?php if ( $context === 'user' ): ?>
						<div class="card shadow-sm mb-4">
							<div class="card-body">
								<h4 class="mb-3">Summary</h4>
								<table class="w-100 mb-0" style="font-size: 16px;">
									<tbody>
										<tr>
											<th>Subtotal</th>
											<td class="text-right"><?php echo $omh_order->get_subtotal_to_display(); ?></td>
										</tr>
										<tr>
											<th>Tax</th>
											<td class="text-right"><?php echo wc_price( $omh_order->get_total_tax() ); ?></td>
										</tr>
										<tr>
											<th>Shipping</th>
											<td class="text-right"><?php echo wc_price( $omh_order->get_shipping_total() ); ?></td>
										</tr>
										<?php if ( $omh_order->get_applied_store_credit() ): ?>
										<tr class="border-bottom">
											<th>Applied Store Credit</th>
											<td class="text-right"><?php echo wc_price( $omh_order->get_applied_store_credit( true ) ); ?></td>
										</tr>
										<?php endif;?>
										<tr>
											<th>Total</th>
											<td class="text-right"><?php echo $omh_order->get_formatted_order_total(); ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<?php endif;?>

						<!-- === Notes === -->
						<div class="card shadow-sm mb-4">
							<div class="card-body">
								<h4 class="mb-3">Notes</h4>
								<span class="<?php echo $order_notes_class ?>">
									<?php echo $order_notes ?>
								</span>
							</div>
						</div>

						<!-- === Customer Details === -->
						<div class="card shadow-sm">
							<div class="card-body">
								<h4 class="float-left">Customer</h4>
								<span class="float-right mb-3">
									<?php echo $user_badge; ?>
								</span>
								<span class="d-block" style="clear: both;">
									<?php echo $customer_fullname; ?>
									<br>
									<?php echo $customer_email; ?>
								</span>
							</div>
							<div class="card-body">
								<h4 class="mb-3">Shipping Address</h4>
								<span><?php echo $omh_order->get_formatted_shipping_address() ?: '<span class="small text-muted">No shipping address set.</span>'; ?></span>
							</div>
							<?php if ( $context === 'user' ): ?>
							<div class="card-body">
								<h4>Billing Address</h4>
								<span><?php echo $omh_order->get_formatted_billing_address() ?: '<span class="small text-muted">No billing address set.</span>'; ?></span>
							</div>
							<?php endif;?>
						</div>

					</div>

					<!-- =====
						Right Column
					===== -->
					<div class="col-md-8">

						<!-- === Order Details === -->
						<div class="card shadow-sm mb-4">
							<div class="card-body">
								<h4 class="mb-3">Order Items</h4>
								<?php echo $order_items_table; ?>
							</div>
						</div>

						<!-- === Order Updates === -->
						<!-- <div class="card shadow-sm mb-4">
							<div class="card-body">
								<h4>Order Updates</h4>
								<?php // echo $order_updates_table; ?>
							</div>
						</div> -->

					</div>

				</div> <!-- END: .row -->

			</div>
		</div>
	</div>
</div>
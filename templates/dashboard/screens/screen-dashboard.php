<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$chapter = OMH_Session::get_chapter();
?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row">
			<div class="col">

				<?php
				include(OMH()->dashboard_template_path('dashboard-header'));
				?>

				<!-- Welcome Alert & Message -->
				<div class="row">
					<div class="col">
						<?php
						if ($omh_user->has_role('house_admin') && $chapter) {
							echo OMH_HTML_UI_Alert::factory(
								array(
									'color'	=> 'info',
									'text'	=> "Text <a href=\"sms:+12138801378\">(213) 880-1378</a> or email info@merch.house for immediate service and free design help!<br>"
										// 'text'	=> "You are now logged in as the House Admin for {$chapter->get_chapter_name()}. You can manage your chapter’s storefront by creating new products.<br>" 
										. OMH_HTML_UI_Button::factory(
											array(
												'color'		=> 'info',
												'href'		=> OMH()->dashboard_url('create-product'),
												'label'		=> 'Submit Design',
												'size'		=> 'small'
											)
										),
									'dismissible'	=> false
								)
							);
						}
						?>
					</div>
				</div>

				<!-- === Main Tables Row === -->
				<div class="row">

					<!-- === Left Tables Column === -->
					<div class="col-lg-12 col-xl-8">

						<!-- ===== Table: Current Campaigns ===== -->
						<?php if ($omh_user->has_role('house_admin')) : ?>
							<?php
							$table_current_campaigns = OMH_Table::factory(
								array(
									'type'		=> 'Campaign_Products',
									'title'		=> array('Current Campaigns'),
									'sections'	=> array(
										'before'		=> true,
										'search'		=> false,
										'tabs'			=> false,
										'pagination'	=> true,
										'headers'		=> true,
										'results'		=> true
									),
									'scopes'	=> array('current'),
									'tabs'		=> array(),
								)
							);

							if (count($table_current_campaigns->get_results())) {
							?>
								<div class="card shadow-sm mb-4">
									<div class="card-body">
										<?php
										echo $table_current_campaigns->get_ui_table(
											array(
												'force_scope'	=> 'current'
											)
										);
										?>
									</div>
								</div>
							<?php
							}
							?>
						<?php endif; ?>

						<!-- ===== Table: Pending Bulk Orders ===== -->
						<?php if ($omh_user->has_role('house_admin')) : ?>
							<?php
							$table_pending_bulk_orders = OMH_Table::factory(
								array(
									'type'		=> 'Bulk_Products',
									'title'		=> array('Pending Bulk Orders'),
									'sections'	=> array(
										'before'		=> true,
										'search'		=> false,
										'tabs'			=> false,
										'pagination'	=> true,
										'headers'		=> true,
										'results'		=> true
									),
									'scopes'	=> array('ready_to_order'),
									'tabs'		=> array(),
								)
							);

							if (count($table_pending_bulk_orders->get_results())) {
							?>
								<div class="card shadow-sm mb-4">
									<div class="card-body">
										<?php
										echo $table_pending_bulk_orders->get_ui_table(
											array(
												'force_scope'	=> 'ready_to_order'
											)
										);
										?>
									</div>
								</div>
							<?php
							}
							?>
						<?php endif; ?>

						<!-- ===== Table: Designs in Review ===== -->
						<?php if ($omh_user->has_role('house_admin')) : ?>
							<?php
							$table_retail_design_in_review = OMH_Table::factory(
								array(
									'type'		=> 'Products',
									'title'		=> array('Designs in Review'),
									'sections'	=> array(
										'before'		=> true,
										'search'		=> false,
										'tabs'			=> false,
										'pagination'	=> true,
										'headers'		=> true,
										'results'		=> true
									),
									'scopes'	=> array('design_in_review'),
									'tabs'		=> array(),
								)
							);

							if (count($table_retail_design_in_review->get_results())) {
							?>
								<div class="card shadow-sm mb-4">
									<div class="card-body">
										<?php
										echo $table_retail_design_in_review->get_ui_table(
											array(
												'force_scope'	=> 'design_in_review'
											)
										);
										?>
									</div>
								</div>
							<?php
							}
							?>
						<?php endif; ?>

						<!-- ===== Table: Retail Products Ready To List ===== -->
						<?php if ($omh_user->has_role('house_admin')) : ?>
							<?php
							$table_retail_ready_to_list_product = OMH_Table::factory(
								array(
									'type'		=> 'Retail_Products',
									'title'		=> array('Retail Products Ready To List'),
									'sections'	=> array(
										'before'		=> true,
										'search'		=> false,
										'tabs'			=> false,
										'pagination'	=> true,
										'headers'		=> true,
										'results'		=> true
									),
									'scopes'	=> array('ready_to_list'),
									'tabs'		=> array(),
								)
							);

							if (count($table_retail_ready_to_list_product->get_results())) {
							?>
								<div class="card shadow-sm mb-4">
									<div class="card-body">
										<?php
										echo $table_retail_ready_to_list_product->get_ui_table(
											array(
												'force_scope'	=> 'ready_to_list'
											)
										);
										?>
									</div>
								</div>
							<?php
							}
							?>
						<?php endif; ?>

						<!-- ===== Table: Completed Bulk Orders ===== -->
						<?php if ($omh_user->has_role('house_admin')) : ?>
							<?php
							$table_completed_bulk_orders = OMH_Table::factory(
								array(
									'type'		=> 'Bulk_Products',
									'scopes'	=> array('completed'),
									'sections'	=> array(
										'before'		=> true,
										'search'		=> false,
										'tabs'			=> false,
										'pagination'	=> true,
										'headers'		=> true,
										'results'		=> true
									),
									'title'		=> array('Completed Bulk Orders'),
									'tabs'		=> array(),
								)
							);

							if (count($table_completed_bulk_orders->get_results())) {
							?>
								<div class="card shadow-sm mb-4">
									<div class="card-body">
										<?php
										echo $table_completed_bulk_orders->get_ui_table(
											array(
												'force_scope'	=> 'completed'
											)
										);
										?>
									</div>
								</div>
							<?php
							}
							?>
						<?php endif; ?>

						<!-- ===== Table: Recent Retail Orders ===== -->
						<?php if ($omh_user->has_role('house_admin')) : ?>
							<?php
							$recent_retail_order_title = $chapter ? 'Recent ' . $chapter->get_chapter_name() . ' Retail Orders' : 'Recent Retail Orders';
							?>
							<div class="card shadow-sm mb-4">
								<div class="card-body">
									<?php
									echo OMH_Table::factory(
										array(
											'type'		=> 'Orders',
											'scopes'	=> array('recent', 'retail', 'chapter'),
											'sections'	=> array(
												'before'		=> true,
												'search'		=> false,
												'tabs'			=> false,
												'pagination'	=> false,
												'headers'		=> true,
												'results'		=> true
											),
											'title'		=> array($recent_retail_order_title),
											'tabs'		=> array()
										)
									)->get_ui_table();
									?>
								</div>
							</div>
						<?php endif; ?>



						<!-- ===== Table: My Recent Orders ===== -->
						<div class="card shadow-sm mb-4">
							<div class="card-body">
								<?php
								echo OMH_Table::factory(
									array(
										'type'		=> 'Orders',
										'scopes'	=> array('my', 'recent', 'retail'),
										'sections'	=> array(
											'before'		=> true,
											'search'		=> false,
											'tabs'			=> false,
											'pagination'	=> false,
											'headers'		=> true,
											'results'		=> true
										),
										'title'		=> array('My Recent Orders'),
										'tabs'		=> array()
									)
								)->get_ui_table();
								?>
							</div>
						</div>

					</div><!-- === END: Left Tables Column === -->

					<!-- === Right Tables Column === -->
					<div class="col-lg-12 col-xl-4">

						<!-- ===== Table: House Admins ===== -->
						<?php if ($omh_user->has_role('house_admin')) : ?>
							<?php
							$house_admin_list = OMH_Table::factory(
								array(
									'type'		=> 'Users',
									'scopes'	=> array('house_admin_list'),
									'sections'	=> array(
										'before'		=> true,
										'search'		=> false,
										'tabs'			=> false,
										'pagination'	=> true,
										'headers'		=> true,
										'results'		=> true
									),
									'title'		=> array('House Admins'),
									'tabs'		=> array(),
								)
							);

							if (count($house_admin_list->get_results())) {
							?>
								<div class="card shadow-sm mb-4">
									<div class="card-body">
										<?php
										echo $house_admin_list->get_ui_table(
											array(
												'force_scope'	=> 'house_admin_list'
											)
										);
										?>
									</div>
								</div>
							<?php
							}
							?>
						<?php endif; ?>
						<!-- ===== END Table: List of House Admins ===== -->

						<!-- ===== Store Credit ===== -->
						<?php if ($omh_user->has_role('house_admin') && $chapter) : ?>

							<div class="card shadow-sm mb-4">
								<div class="card-body">
									<div class="row">
										<div class="col-12">
											<h4>Store Credit</h4>
											<small class="text-muted">Current Balance</small>
										</div>
										<div class="col-6">
											<h3 class="font-weight-bold"><?php echo wc_price(OMH()->credit_factory->get_total_credit($chapter->get_id(), false)); ?></h3>
										</div>
										<div class="col-6 text-right">
											<?php
											echo OMH_HTML_UI_Button::factory(
												array(
													'color'		=> 'primary',
													'size'		=> 'small',
													'href'		=> OMH()->dashboard_url('store-credit'),
													'label'		=> 'Details',
												)
											);
											?>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>

						<!-- ===== Followers ===== -->
						<?php if ($omh_user->has_role('house_admin') && $chapter ) : ?>
							<?php $follower_count = OMH_Factory_Follower::get_object_followers_count( $chapter->get_id(), 'chapter' ); ?>
							<div class="card shadow-sm mb-4">
								<div class="card-body">
									<div class="row">
										<div class="col-12">
											<h4>Followers</h4>
											<small class="text-muted"># Subscribers</small>
										</div>
										<div class="col-6">
											<h3 class="font-weight-bold"><?php echo $follower_count; ?></h3>
										</div>
										<div class="col-6 text-right">
											<?php if( $omh_user->has_role('administrator') ) {
												echo OMH_HTML_UI_Button::factory(
													array(
														'color'		=> 'primary',
														'size'		=> 'small',
														'href'		=> OMH()->dashboard_url('?export_followers=true'),
														'label'		=> 'Export All',
														'attrs'		=> array(
															'target'	=> '_blank'
														)
													)
												);
											}
											?>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>

					</div><!-- === END: Right Tables Column === -->

				</div><!-- === END: Main Tables Row === -->

			</div>
		</div>
	</div>

	<script type="text/javascript">
		var table_security = '<?php echo wp_create_nonce('omh-mh-table-request'); ?>';
		var omh_security = table_security;
	</script>
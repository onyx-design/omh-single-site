<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$allowed_roles = false;

wp_redirect( OMH()->mh_admin_url( 'global-garments-products' ) );
exit;
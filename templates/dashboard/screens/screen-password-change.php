<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_page_title = 'My Account';

$omh_breadcrumbs = array(
	array(
		'label'		=> 'My Account',
		'url'		=> OMH()->dashboard_url( 'account-details' ),
	),
	array(
		'label'		=> 'Change Password',
		'active'	=> true
	)
);

$header_tabs = array(
	'account_details' 	=> array(
		'name'			=> 'Account Details',
		'destination'	=> OMH()->dashboard_url( 'account-details' )
	),
	'password_change'	=> array(
		'name'			=> 'Change Password',
		'active'		=> true,
		'destination'	=> OMH()->dashboard_url( 'password-change' )
	),
	'my_addresses' 		=> array(
		'name'			=> 'My Addresses',
		'destination'	=> OMH()->dashboard_url( 'my-addresses' )
	)
);
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row">
			
			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="row">
					<div class="col-md-4">
						Need to update your password but not sure what your current password is?
						<br />
						<a href="<?php echo wp_logout_url( wp_lostpassword_url() ); ?>">Recover your password</a>
					</div>
					<div class="col-md-8">
						<div class="card shadow-sm">
							<div class="card-body">
								<form id="mh-screen-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="save_user_password" data-form-nonce="<?php echo wp_create_nonce( 'save-user-password' ); ?>" data-object-id="<?php echo $omh_user->ID; ?>">

									<?php 
										echo OMH_HTML_UI_Input::factory( 
											array(
												'input_type'	=> 'password',
												'input_id' 		=> 'current_password', 
												'label' 		=> 'Current Password',
												'required' 		=> true
											) 
										); 

										echo OMH_HTML_UI_Input::factory( 
											array(
												'input_type'	=> 'password',
												'input_id' 		=> 'new_password', 
												'label' 		=> 'New Password',
												'input_attrs'	=> array(
													'pattern'					=> '(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9$@$!%*#?&]{6,}',
													'data-invalid-msg-pattern'	=> OMH_Frontend::password_hint()
												),
												'required' 		=> true,
												'match_field'	=> 'confirm_new_password'
											) 
										); 
										
										echo OMH_HTML_UI_Input::factory( 
											array(
												'input_type'	=> 'password',
												'input_id' 		=> 'confirm_new_password', 
												'label' 		=> 'Confirm New Password',
												'input_attrs'	=> array(
													'pattern'					=> '(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9$@$!%*#?&]{6,}',
													'data-invalid-msg-pattern'	=> OMH_Frontend::password_hint()
												),
												'required' 		=> true,
												'match_field'	=> 'new_password'
											) 
										); 
									?>

								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col">
						<hr>
					</div>
				</div>

				<div class="row">
					<div class="col text-right">
						<?php  
							echo OMH_HTML_Tag::factory(
								array(
									'type'		=> 'OMH_HTML_UI_Button',
									'color'		=> 'primary',
									'value'		=> 'submit',
									'form'		=> 'mh-screen-form',
									'label'		=> 'Save',
									'class' 	=> 'mh-form-submit'
								)
							);
						?>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
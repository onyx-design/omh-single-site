<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

$product_id = $_GET['id'];
if( !isset( $product_id ) ) {
	wp_redirect( OMH()->dashboard_url( 'products') );
	exit;
}

$allowed_roles = false;
$redirect_disallowed_roles = OMH()->dashboard_url( 'edit-product?id=' . $product_id );

$omh_breadcrumbs = array(
	array(
		'label'		=> OMH_Session::get_chapter()->get_chapter_name(),
		'url'		=> OMH()->dashboard_url()
	),
	array(
		'label'		=> 'Products',
		'url'		=> OMH()->dashboard_url( 'retail-products' )
	),
	array(
		'label'		=> 'Product Design',
		'active'	=> true
	)
);

if( !$omh_product = wc_get_product( $product_id ) ) {
	wp_redirect( OMH()->dashboard_url( 'retail-products' ) );
	exit;
}

$omh_page_pretitle = get_the_title();
$omh_page_title = $omh_product->get_title();

$omh_page_actions = array(
	'view-product' => array(
		'label'			=> 'View Product',
		'url'			=> $omh_product->get_permalink(),
		'new_tab'		=> true,
		'icon'			=> 'eye'
	),
);

if( is_super_admin() ) {

	$header_tabs = array(
		'edit_product'			=> array(
			'name'			=> 'Edit Product',
			'destination'	=> OMH()->dashboard_url( 'edit-product?id=' . $product_id )
		),
		// 'edit_product_design'	=> array(
		// 	'name'			=> 'Product Design',
		// 	'active'		=> true,
		// 	'destination'	=> OMH()->dashboard_url( 'edit-product-design?id=' . $product_id )
		// ),
	);

	if ('bulk' !== $omh_sales_type) {
		$headers_tabs['product_sales'] = array(
			'name'			=> 'Product Sales',
			'destination'	=> OMH()->dashboard_url( 'product-sales?id=' . $product_id )
		);
	}
}
?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row screen--edit-product-design">
			<div class="col">

				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<?php if( $product_id ): ?>
					<form id="mh-screen-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="save_dashboard_product_design_details" data-form-nonce="<?php echo wp_create_nonce( 'save-dashboard-product-design-details' ); ?>" data-object-id="<?php echo $product_id; ?>">

						<div class="row">
							<div class="col-md-6">
								<div class="card shadow-sm">
									<div class="card-body">
										<h4>Uploads and Comments</h4>
										
										<?php
											echo OMH_Table::factory(
												array(
													'type' => 'Product_Customer_Uploads',
													'query' => array(
														'id'	=> $product_id
													)
												)
											)->get_ui_table();

											echo OMH_HTML_UI_Textarea::factory(
												array(
													'input_id'	=> 'customer_comments',
													'label'		=> 'Customer Comments',
													'class'		=> 'omh-field-input',
													'value'		=> wp_unslash( $omh_product->get_customer_comment() ),
													'disabled'	=> true
												)
											);

										?>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="card shadow-sm">
									<div class="card-body">
										<h4>Designer Submission</h4>

										<div class="row">
										<?php if( 0 /* $rendered_design = $omh_product->get_rendered_design() */ ) {

											$rendered_design = json_decode( $rendered_design );

											foreach( $rendered_design->DesignSummary->Canvases as $sides ) {
											?>
												<div class="col-md-6">
											<?php
												echo $sides->SideDisplayName;

												echo OMH_HTML_Tag::factory(
													array(
														'type'	=> 'img',
														'attrs'	=> array(
															'src'	=> 'https://images.inksoft.com' . $sides->PngRelativeUrlWithProduct
														)
													)
												);
											?>
											</div>
											<?php } ?>
											<div class="col-md-12">
												<?php
													$zipped_assets = $rendered_design->RenderedFiles[ array_search( '.zip', array_column( $rendered_design->RenderedFiles, 'Extension' ) ) ];

													echo OMH_HTML_UI_Button::factory(
														array(
															'href'		=> 'https://images.inksoft.com' . $zipped_assets->RelativePath,
															'color'		=> 'secondary',
															'label'		=> 'Download Assets Zip',
															'class' 	=> 'download-assets-zip-btn',
															'attrs'		=> array(
																'target'	=> '_blank'
																// 'download'	=> basename( $zipped_assets->RelativePath )
															)
														)
													);
												?>
											</div>
										<?php } else { ?>
											<div class="col-md-12">
										<?php
											echo OMH_HTML_UI_Button::factory(
												array(
													'color'		=> 'secondary',
													'label'		=> 'Render Design',
													'class' 	=> 'render-product-design-btn',
													'disabled'	=> true,
												)
											);
										?>
											</div>
										<?php
										} 
										?>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="card shadow-sm">
									<div class="card-body">
										<h4>Selected Garment Style</h4>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="card shadow-sm">
									<div class="card-body">
										<div class="row">
											<div class="col-12">
												<h4>Initial Design Submission</h4>
											</div>
										</div>

										<div class="row">
											<div class="col-12">
												
												<div class="form-check form-group">
													<input class="form-check-input" type="checkbox" name="_design_approved_mh" id="_design_approved_mh">
													<label class="form-check-label" for="_design_approved_mh">
														Mech House Approved
													</label>
													<small class="form-text text-muted">Design approved by Merch House</small>
												</div>

												<div class="form-check form-group">
													<input class="form-check-input" type="checkbox" name="_send_design_approved_mh" id="_send_design_approved_mh">
													<label class="form-check-label" for="_send_design_approved_mh">
														Send Design Approved Email
													</label>
													<small class="form-text text-muted">Send Email to House Admin that the design has been approved</small>
												</div>

												<div class="form-check form-group">
													<input class="form-check-input" type="checkbox" name="_design_approved_ha" id="_design_approved_ha">
													<label class="form-check-label" for="_design_approved_ha">
														House Admin Approved
													</label>
													<small class="form-text text-muted">Design approved by House Admin</small>
												</div>

												<div class="form-check form-group">
													<input class="form-check-input" type="checkbox" name="_allow_free" id="_allow_free">
													<label class="form-check-label" for="_allow_free">
														Allow Free
													</label>
													<small class="form-text text-muted">This does not impact the price, only changes the status to be "Active" rather than "Design Finalized".</small>
												</div>

												<?php

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_design_id',
													// 		'label'			=> 'Inksoft Design ID',
													// 		'value'			=> $omh_product->get_meta( '_design_id', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_product_id',
													// 		'label'			=> 'Inksoft Product ID',
													// 		'value'			=> $omh_product->get_meta( '_mh_product_id', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_product_style_id',
													// 		'label'			=> 'Product Style ID',
													// 		'value'			=> $omh_product->get_meta( '_mh_product_style_id', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_base_cost',
													// 		'label'			=> 'Base Cost',
													// 		'value'			=> $omh_product->get_meta( '_mh_base_cost', true ),
													// 		'form_text'		=> 'The price paid to Merch House per unit'
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_blank_name',
													// 		'label'			=> 'Blank\'s Name',
													// 		'value'			=> $omh_product->get_meta( '_mh_blank_name', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_manufacturer',
													// 		'label'			=> 'Blank\'s Manufacturer',
													// 		'value'			=> $omh_product->get_meta( '_mh_manufacturer', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_blank_sizes',
													// 		'label'			=> 'Blank\'s Sizes',
													// 		'value'			=> $omh_product->get_meta( '_mh_blank_sizes', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_blank_sku',
													// 		'label'			=> 'Blank\'s SKU',
													// 		'value'			=> $omh_product->get_meta( '_mh_blank_sku', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_blank_style_number',
													// 		'label'			=> 'Blank\'s Style Number',
													// 		'value'			=> $omh_product->get_meta( '_mh_blank_style_number', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_blank_style_name',
													// 		'label'			=> 'Blank\'s Style Name',
													// 		'value'			=> $omh_product->get_meta( '_mh_blank_style_name', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_blank_color',
													// 		'label'			=> 'Blank\'s Color',
													// 		'value'			=> $omh_product->get_meta( '_mh_blank_color', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Textarea::factory(
													// 	array(
													// 		'input_id'		=> '_mh_blank_description',
													// 		'label'			=> 'Blank\'s Description',
													// 		'value'			=> $omh_product->get_meta( '_mh_blank_description', true )
													// 	)
													// );

													// echo OMH_HTML_UI_Input::factory(
													// 	array(
													// 		'input_id'		=> '_mh_blank_material',
													// 		'label'			=> 'Blank\'s Material',
													// 		'value'			=> $omh_product->get_meta( '_mh_blank_material', true )
													// 	)
													// );

												?>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col">
								<hr>
							</div>
						</div>

						<div class="row">
							<div class="col text-right">
								<?php  
									// echo OMH_HTML_UI_Button::factory(
									// 	array(
									// 		'color'		=> 'primary',
									// 		'value'		=> 'submit',
									// 		'form'		=> 'mh-screen-form',
									// 		'label'		=> 'Save',
									// 		'class' 	=> 'mh-form-submit'
									// 	)
									// );
								?>
							</div>
						</div>
					</form>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var omh_security = '<?php echo wp_create_nonce( 'render-product-design' ); ?>';

	(function($) {
		$(document).ready(function() {

			var $ajax_action = 'omh_render_product_design',
				$ajax_data = {};

			$(document).on( 'click', 'button.render-product-design-btn', function( e ) {

				e.preventDefault();

				$ajax_data = {
					obj_id		: $(this).closest('form').data('objectId')
				}

				omh.ajax(
					$ajax_action,
					$ajax_data,
					null,
					omh_security
				);

			} );
		});
	})(jQuery);
</script>
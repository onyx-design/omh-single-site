<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$allowed_roles = array( 'house_admin' );

$omh_breadcrumbs = array(
	array(
		'label'		=> OMH_Session::get_chapter()->get_chapter_name(),
		'url'		=> OMH()->dashboard_url()
	),
	array(
		'label'		=> 'Store Credit',
		'active'	=> true
	)
);

ob_start();
include_once( OMH()->dashboard_template_path( 'dashboard-header-store-credit' ) );
$omh_header_extra_content = ob_get_clean();
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row">

			<div class="col-md-12">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">

						<?php
							echo OMH_Table::factory(
								array(
									'type'		=> 'Credits'
								)
							)->get_ui_table();
						?>
					</div>
				</div>
			</div>

			<!-- Add Store Credit Adjustment Modal -->
			<div class="modal fade mh-modal" id="store-credit-modal" role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="store-credit-modal-title">Add Credit Adjustment</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form id="mh-form-add-credit-adjustment" class="mh-form form-not-initialized needs-validation" data-ajax-action="add_credit_adjustment" data-form-nonce="<?php echo wp_create_nonce( 'add-credit-adjustment' ); ?>">

								<?php 
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'number',
											'input_id'		=> 'adjustment_amount',
											'label'			=> 'Adjustment Amount',
											'value'			=> '',
											'required'		=> true,
											'contents'		=> array(
												'input'			=> array(
													'attrs'			=> array(
														'step'			=> '0.01',
													),
												)
											),
											'prepend'		=> array(
												'dollars'	=> OMH_HTML_UI_Element::factory(
													array(
														'type'		=> 'span',
														'class'		=> 'input-group-text',
														'contents'	=> '$'
													)
												)
											)
										)
									); 

									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'text',
											'input_id'		=> 'adjustment_notes',
											'label'			=> 'Adjustment Notes',
											'value'			=> '',
											'required'		=> true,
											'contents'		=> array(
												'input'			=> array(
													'attrs'			=> array(
														'maxlength'		=> '256'
													)
												)
											)
										)
									);

									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'datepicker',
											'input_id'		=> 'ledger_date',
											'label'			=> 'Date',
											'value'			=> '',
											'required'		=> true
										)
									);
								?>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-reset-form'	=> 'mh-form-add-credit-adjustment',
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-add-credit-adjustment',
										'label'		=> 'Save',
										'class' 	=> 'mh-forms-submit'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
	var omh_security = table_security;

	function deleteCreditAdjustment( creditID ) {

		omh.ajax(
			'omh_delete_credit_adjustment',
			{ credit_id : creditID },
			null,
			'<?php echo wp_create_nonce( 'delete-credit-adjustment' ); ?>'
		);
	}

	function recalculateCredit( creditID ) {

		omh.ajax(
			'omh_recalculate_credit_row',
			{ credit_id : creditID },
			null,
			'<?php echo wp_create_nonce( 'recalculate-credit-row' ); ?>'
		);
	}

	function updateStoreCreditBalance( creditBalance ) {
		jQuery( '.dashboard-store-credit-balance' ).html( creditBalance );
	}

	jQuery(document).on('omh_add_credit_adjustment', function(event,responseData) {
		var creditBalance 	= _.get( responseData, 'data.credit_balance.value' );

		updateStoreCreditBalance( creditBalance );
	})
	.on('omh_delete_credit_adjustment', function(event,responseData) {
		var creditBalance 	= _.get( responseData, 'data.credit_balance.value' );

		updateStoreCreditBalance( creditBalance );
	});
</script>
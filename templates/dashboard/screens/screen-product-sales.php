<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

$product_id = $_GET['id'];
if( !isset( $product_id ) ) {
	wp_redirect( OMH()->dashboard_url( 'products') );
	exit;
}

$allowed_roles = array( 'house_admin' );

$current_user = wp_get_current_user();

$product_id = (int) ( empty( $_GET['id'] ) ? null : $_GET['id'] );
if( !isset( $product_id ) 
	|| !( $omh_product = wc_get_product( $product_id ) )
) {
	wp_redirect( OMH()->dashboard_url( 'retail-products' ) );
	exit;
}

$omh_breadcrumbs = array(
	array(
		'label'		=> OMH_Session::get_chapter()->get_chapter_name(),
		'url'		=> OMH()->dashboard_url()
	),
);

if( $omh_product->get_sales_type() === 'bulk' ) {

	$omh_breadcrumbs[] = array(
		'label'		=> 'Products',
		'url'		=> OMH()->dashboard_url( 'bulk-orders' ),
	);
} elseif( $omh_product->get_sales_type() === 'retail' ) {

	$omh_breadcrumbs[] = array(
		'label'		=> 'Products',
		'url'		=> OMH()->dashboard_url( 'retail-products' ),
	);
} elseif( $omh_product->get_sales_type() === 'campaign' ) {

	$omh_breadcrumbs[] = array(
		'label'		=> 'Products',
		'url'		=> OMH()->dashboard_url( 'campaign-products' ),
	);
}

$omh_breadcrumbs[] = array(
	'label'		=> 'Product Sales',
	'active'	=> true
);
	

$product_chapter = $omh_product->get_chapter();

if( $current_user && $current_user->has_role( 'house_admin', false ) ) {

	$user_chapter = $current_user->get_chapter();

	if( $product_chapter->get_id() !== $user_chapter->get_id() ) {
		wp_redirect( OMH()->dashboard_url( 'retail-products' ) );
		exit;
	}
}

// Product Table Filter
$omh_product_filter = array(
	'product_id'	=> $product_id
);

ob_start();
include( OMH()->dashboard_template_path( 'dashboard-header-products' ) );
$omh_header_extra_content = ob_get_clean();

$omh_page_pretitle = get_the_title();
$omh_page_title = $omh_product->get_title();

if( $omh_product->is_product_visible() || is_super_admin() ) {

	$omh_page_actions = array(
		'view-product' => array(
			'label'			=> 'View Product',
			'url'			=> $omh_product->get_permalink(),
			'new_tab'		=> true,
			'icon'			=> 'eye'
		),
	);
}

if( is_super_admin() ) {

	$header_tabs = array(
		'edit_product'			=> array(
			'name'			=> 'Edit Product',
			'destination'	=> OMH()->dashboard_url( 'edit-product?id=' . $product_id )
		),
		// 'edit_product_design'	=> array( // temporarily removed
		// 	'name'			=> 'Product Design',
		// 	'destination'	=> OMH()->dashboard_url( 'edit-product-design?id=' . $product_id )
		// ),
		'product_sales'			=> array(
			'name'			=> 'Product Sales',
			'active'		=> true,
			'destination'	=> OMH()->dashboard_url( 'product-sales?id=' . $product_id )
		),
	);
}
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row screen--product-sales">

			<div class="col">

				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">
						<?php
							echo OMH_Table::factory(
								array(
									'type'		=> 'Product_Sales',
									'filters' 	=> $omh_product_filter
								)
							)->get_ui_table(
								array(
									'default_filters'	=> $omh_product_filter
								)
							);
						?>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
<script type="text/javascript">

	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
	var omh_security = table_security;

</script>

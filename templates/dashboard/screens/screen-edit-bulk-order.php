<?php
// Exit if accessed directly
if (!defined('ABSPATH')) exit;

$allowed_roles = array('house_admin');

$omh_breadcrumbs = array(
	array(
		'label'		=> OMH_Session::get_chapter()->get_chapter_name(),
		'url'		=> OMH()->dashboard_url()
	),
	array(
		'label'		=> 'Bulk Orders',
		'url'		=> OMH()->dashboard_url('bulk-orders')
	),
	array(
		'label'		=> 'Edit Bulk',
		'active'	=> true
	)
);

// If product doesn't exist, redirect everyone
$product_id = (int) (empty($_GET['id']) ? null : $_GET['id']);
if (
	!isset($product_id)
	|| !($omh_product = wc_get_product($product_id))
	|| ($omh_product->get_sales_type() !== 'bulk')
) {
	wp_redirect(OMH()->dashboard_url('bulk-orders'));
	exit;
}

if (!$omh_product->is_product_purchasable() && !is_super_admin(get_current_user_id())) {
	wp_redirect(OMH()->dashboard_url('product-sales?id=' . $omh_product->get_id()));
	exit;
}

$bulk_order = $omh_product->get_active_bulk_order();
$order_total_qty = 0;

// If design is pending and we're not a super admin, we need to redirect
// dev:improve dev:todo Product Statuses have changed and now use Product Visibility
// if( 'active' != $omh_product->get_mh_product_status() ) {
// 	$allowed_roles = false;
// }

if ($bulk_order) {

	foreach ($bulk_order->get_items('line_item') as $order_item) {
		$order_total_qty += $order_item->get_quantity();
	}
}

/**
 * @tag Product Status
 * @tag Visibility
 */
$data = array(
	'_design_approved_mh' 		=> $omh_product->get_meta('_design_approved_mh') ? true : false,
	'_visibility_mh' 			=> $omh_product->get_meta('_visibility_mh', true),
	'_mh_base_cost' 			=> $omh_product->get_meta('_mh_base_cost', true),
	'_mh_product_description' 	=> $omh_product->get_meta('_mh_product_description', true)
);

ob_start();
include(OMH()->dashboard_template_path('dashboard-header-products'));
$omh_header_extra_content = ob_get_clean();

$omh_page_pretitle = get_the_title();
$omh_page_title = $omh_product->get_title();

// $omh_page_actions = array(
// 	'view-product' => array(
// 		'label'			=> 'View Product',
// 		'url'			=> $omh_product->get_permalink(),
// 		'new_tab'		=> true,
// 		'icon'			=> 'eye'
// 	),
// );

// if( is_super_admin() ) {

// 	$header_tabs = array(
// 		'edit_product'			=> array(
// 			'name'			=> 'Edit Product',
// 			'destination'	=> OMH()->dashboard_url( 'edit-product?id=' . $product_id )
// 		),
// 		'edit_product_design'	=> array(
// 			'name'			=> 'Product Design',
// 			'destination'	=> OMH()->dashboard_url( 'edit-product-design?id=' . $product_id )
// 		),
// 		'product_sales'			=> array(
// 			'name'			=> 'Product Sales',
// 			'destination'	=> OMH()->dashboard_url( 'product-sales?id=' . $product_id )
// 		),
// 		'edit_bulk_order'			=> array(
// 			'name'			=> 'Edit Bulk Order',
// 			'active'		=> true,
// 			'destination'	=> OMH()->dashboard_url( 'edit-bulk-order?id=' . $product_id )
// 		),
// 	);
// }

// $input_disabled_design_approved = is_super_admin() ? false : $omh_user->has_role( 'house_admin' ) ? $data['_design_approved_mh'] ? false : true : true;
$input_disabled = is_super_admin() ? false : true;

$enable_load_data = true;
$enable_ajax_search = true;

// Determine product categories
$product_cats = array();
$selected_cats = $omh_product->get_category_ids();
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row screen--edit-product">

			<div class="col">

				<?php
				include(OMH()->dashboard_template_path('dashboard-header'));

				if (empty($_GET['updated']) ? false : $_GET['updated']) {

					/*
						 * Alert & Message
						 */
					echo OMH_HTML_UI_Alert::factory(
						array(
							'color'			=> 'success',
							'text'			=> 'Product updated'
						)
					);
				}
				?>

				<form id="mh-screen-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="save_dashboard_edit_bulk_details" data-form-nonce="<?php echo wp_create_nonce('save-dashboard-edit-bulk-details'); ?>" data-object-id="<?php echo $product_id; ?>">
					<!-- Total Order Amount and Qty -->
					<div class="row">
						<div class="col-12">
							<div class="card shadow-sm">
								<div class="card-body">
									<h4>Order Summary</h4>
									<div class="row">
										<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'bulk_order_id',
												'input_type'	=> 'hidden',
												'class'			=> 'd-none',
												'value'			=> $omh_product->get_active_bulk_order_id()
											)
										);
										?>
										<div class="col-8">
											<table class="table omh-table table-equal-width table--garment-price-tiers table--order-price-tiers">
												<thead>
													<tr>
														<th class="garment-tier garment-tier--below-min" data-tier-min="0" data-tier-price="0">
															Order Quantity
															<div class="tier-progress">
																<div class="progress" data-target-progress="0">
																</div>
															</div>
														</th>
														<?php
														foreach ($omh_product->get_product_price_tiers() as $index => $tier) {
															echo OMH_HTML_Tag::factory(
																array(
																	'type'	=> 'th',
																	'class'	=> 'text-center garment-tier',
																	'attrs'	=> array(
																		'data-tier'	=> $index,
																		'data-tier-min'	=> $tier
																	),
																	'contents'	=> array(
																		$tier,
																		'progress'	=> OMH_HTML_Tag::factory(
																			array(
																				'type'	=> 'div',
																				'class'	=> 'tier-progress',
																				'contents'	=> '<div class="progress" data-target-progress="0"></div>'
																			)
																		)
																	)
																)
															);
														}
														?>
													</tr>
												</thead>
											</table>
										</div>
										<div class="col-4">
											<div>

												<div class="row">
													<div class="col-8">
														<p>Order Quantity Total</p>
													</div>
													<div data-calc-sum="order_qty_total" class="col-4">

													</div>
													<div class="col-8">
														<p>Order Total</p>
													</div>
													<div data-calc-sum="order_price_total" class="col-4 price-val"></div>
												</div>

												<div class="row">
													<div class="col text-left">
														<?php
														echo OMH_HTML_UI_Button::factory(
															array(
																'input_id'		=> 'proceed-to-checkout',
																'attrs'			=> array(
																	'data-ajax-action'	=> 'omh_checkout_dashboard_edit_bulk_details',
																	'data-toggle' 		=> 'tooltip',
																	'data-original-title' => "Your order must be above the minimum order quantity to continue."
																),
																'color'			=> 'primary',
																'value'			=> 'submit',
																'form'			=> 'mh-screen-form',
																'label'			=> 'Proceed to Checkout',
																'class' 		=> 'mh-form-submit submit-allow-valid'
															)
														);
														?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Product Garment Variations -->
					<?php foreach ($omh_product->get_garments() as $product_garment_id => $product_garment) :
						// get are necessary variables from product garment class
						$product_garment_term_name = $product_garment->get_garment_term_name();
						$product_garment_pricing = $product_garment->get_pricing();
						$product_garment_sizes = $product_garment->get_enabled_sizes();
						$product_garment_variations = $product_garment->get_garment_variations();

						// create attribute values to be used for calcualtions
						$garment_qty_calc_sum_selector = 'garment_qty_' . sanitize_title($product_garment->get_garment_term_slug());
						$garment_price_calc_sum_selector = 'garment_price_' . sanitize_title($product_garment->get_garment_term_slug());

						// check if a size is present, if not don't render garment
						$render_garment = false;
						foreach ($product_garment_sizes as $key => $size_check) {
							if (isset($key)) {
								$render_garment = true;
								// once it finds a size "break" out of the foreach
								break;
							}
						}
						?>
					<?php if ($render_garment) : ?>
					<div class="row">
						<div class="col">
							<div class="card shadow-sm">
								<div class="card-body garment-wrap" data-garment="<?php echo $product_garment_term_name ?>">
									<h4><?php echo $product_garment_term_name ?></h4>
									<div class="row <?php echo $garment_qty_calc_sum_selector; ?>">
										<div class="col-8 d-flex flex-column justify-content-around">
											<table class="table omh-table table-equal-width table--garment-price-tiers">
												<thead>
													<tr>
														<th class="garment-tier garment-tier--below-min" data-tier-min="0" data-tier-price="<?php echo array_values($product_garment_pricing)[0]; ?>">
															Min. Order Quantity
															<div class="tier-progress">
																<div class="progress" data-target-progress="0">
																</div>
															</div>
														</th>
														<?php
																foreach ($product_garment_pricing as $index => $tier_price) {
																	echo OMH_HTML_Tag::factory(
																		array(
																			'type'	=> 'th',
																			'class'	=> 'text-center garment-tier',
																			'attrs'	=> array(
																				'data-tier'		=> $index,
																				'data-tier-price'	=> $tier_price
																			),
																			'contents'	=> array(
																				$omh_product->get_product_price_tiers()[$index],
																				'progress'	=> OMH_HTML_Tag::factory(
																					array(
																						'type'	=> 'div',
																						'class'	=> 'tier-progress',
																						'contents'	=> '<div class="progress" data-target-progress="0"></div>'
																					)
																				)
																			)
																		)
																	);
																}
																?>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th>Unit Price</th>
														<?php
																foreach ($product_garment_pricing as $index => $tier_price) {
																	echo '<td class="text-center price-val" data-tier="' . $index . '">' . $tier_price . '</td>';
																}
																?>
													</tr>
												</tbody>
											</table>
											<table class="table omh-table table-equal-width table--garment-variations">
												<thead>
													<tr>
														<th class="py-0">
															Size
															<!-- TODO: Ask about this <div class="small text-muted>">+ Markup</div> -->
														</th>
														<?php
														foreach ($product_garment_variations as $variation_object) {
															$variation_data = $variation_object->get_data();
															$raw_size_label = $variation_data['attributes']['pa_size'];
															$size_label_formatted = substr($raw_size_label, strpos($raw_size_label, "-")+1);
															$size_label = strtoupper($size_label_formatted);

															echo OMH_HTML_Tag::factory(
																array(
																	'type'	=> 'th',
																	'class'	=> 'text-center',
																	'contents'	=> array(
																		$size_label

																		)
																	)
																);
															}	
																?>
														<th>Total</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th>Prices</th>
														<?php
																foreach ($product_garment_variations as $variation_object) {
																	$variation_data = $variation_object->get_data();
																	$calc_product_selector = 'line_total_' . $variation_data['id'];
																	echo OMH_HTML_Tag::factory(
																		array(
																			'type'	=> 'td',
																			'class'	=> 'text-center price-val',
																			'attrs'	=> array(
																				'data-variation-id' 	=> $variation_data['id'],
																				'data-variation-field'	=> 'line_item_price',
																				'data-calc-product-for' => $calc_product_selector
																			),
																			'contents'	=> array(
																				(string) $variation_object->get_price() // should be line item price
																			)
																		)
																	);
																}
																?>
														<td></td>
													</tr>
													<tr>
														<th>Qty</th>
														<?php
																foreach ($product_garment_variations as $variation_object) {
																	$value = 0;
																	$variation_data = $variation_object->get_data();
																	$calc_product_selector = 'line_total_' . $variation_data['id'];

																	if ($bulk_order) {

																		foreach ($bulk_order->get_items() as $item_id => $item) {
																			$product_id = $item->get_variation_id() ?: $item->get_product_id();

																			if ($product_id == $variation_data['id']) {
																				$value = $item->get_quantity();
																			}
																		}
																	}

																	echo OMH_HTML_Tag::factory(
																		array(
																			'type'	=> 'td',
																			'class'	=> 'text-center cell--input',
																			'contents'	=> array(
																				OMH_HTML_Tag::factory(
																					array(
																						'type'	=> 'input',
																						'class'	=> 'form-control omh-table-input omh-field-input',
																						'attrs'	=> array(
																							'name'	=> 'qty_' . $variation_data['id'],
																							'type'	=> 'number',
																							'min'	=> '0',
																							// 'disabled'	=> !( $omh_order->is_order_editable() ),
																							'step'	=> '1',
																							'value'	=> $value,
																							'data-calc-product-for' => $calc_product_selector,
																							'data-calc-sum-for'		=> $garment_qty_calc_sum_selector
																						)
																					)
																				)
																			)
																		)
																	);
																}
																?>
														<td class="<?php echo $garment_qty_calc_sum_selector; ?>" data-calc-sum-for="order_qty_total" data-calc-sum="<?php echo $garment_qty_calc_sum_selector; ?>"></td>
													</tr>
													<tr>
														<td>Item Total</td>
														<?php
																foreach ($product_garment_variations as $variation_object) {
																	$variation_data = $variation_object->get_data();
																	$calc_product_selector = 'line_total_' . $variation_data['id'];
																	echo OMH_HTML_Tag::factory(
																		array(
																			'type'	=> 'td',
																			'class'	=> 'text-center price-val',
																			'attrs'	=> array(
																				'data-calc-product' => $calc_product_selector,
																				'data-calc-sum-for'	=> $garment_price_calc_sum_selector
																			),
																			'contents'	=> array(
																				// (string) $variation_data['line_item_total']
																			)
																		)
																	);
																}
																?>

														<td data-calc-sum-for="order_price_total" data-calc-sum="<?php echo $garment_price_calc_sum_selector; ?>" class="price-val"></td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="col-4">
											<div class="hoverzoom omh-bulk-order-image-gallery woocommerce-product-gallery__image">
												<?php
														$image_proof = $product_garment->get_image_proof();

														if ($image_proof) {

															?>
												<a href="<?php echo $image_proof['src']; ?>" class="omh-bulk-order-image-gallery magnific-popup">
													<img src="<?php echo $image_proof['src']; ?>" />
												</a>
												<span class="hoverzoom-icon"><i class="fa fa-search-plus"></i></span>
												<?php
														}
														?>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php endif ?>
					<?php endforeach ?>

					<div class="row">
						<div class="col">
							<hr>
						</div>
					</div>

					<div class="row">
						<div class="col text-right">
							<?php
							echo OMH_HTML_UI_Button::factory(
								array(
									'color'		=> 'primary',
									'value'		=> 'submit',
									'form'		=> 'mh-screen-form',
									'label'		=> 'Save',
									'class' 	=> 'mh-form-submit'
								)
							);
							?>
						</div>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	// Here we are adding highlights to each tier pricing and dynamically change the prices when tiers are being met
	var tiers = <?php echo json_encode($omh_product->get_product_price_tiers()); ?>;

	var $orderQty = jQuery('div[data-calc-sum="order_qty_total"]').eq(0),
		$highlightTiers = jQuery('[data-tier]'),
		activeTier = null;

	function getCurrentTier() {

		var orderQty = parseInt($orderQty.text()),
			currentTier = -1;

		jQuery.each(tiers, function(index, tierMinOrderQty) {

			if (orderQty >= tierMinOrderQty) {
				currentTier = index;
			}
			else {
				return false;
			}


		});

		return currentTier;
	}

	function highlightCurrentTier() {
		var currentTier = getCurrentTier();
		var checkoutButtonRef = jQuery('.mh-form-submit[data-ajax-action="omh_checkout_dashboard_edit_bulk_details"]');
		if (currentTier !== activeTier) {
			$highlightTiers.removeClass('active-tier');

			if (currentTier > -1) {
				// checkoutButtonRef.tooltip("destroy");
				checkoutButtonRef.tooltip('dispose');
				
				$highlightTiers.filter('[data-tier="' + currentTier + '"]').addClass('active-tier');
				checkoutButtonRef.removeAttr('disabled').prop('disabled', false).removeClass('disabled');
			} else {
				checkoutButtonRef.attr('disabled', 'disabled').prop('disabled', true).addClass('disabled');
				checkoutButtonRef.tooltip();
			}

			// Update current garment price(s) based on tier
			jQuery('.garment-wrap .table--garment-price-tiers tbody tr').each(function() {
				var $activeTier = jQuery(this).children('td.active-tier').eq(0);

				if (0 == $activeTier.length) {
					$activeTier = jQuery(this).children('td').eq(0);
				}

				var price = $activeTier.text();

				// Set garment prices
				jQuery(this).closest('.garment-wrap').find('.table--garment-variations tbody td.price-val').text(price).trigger('change');

				
			});

			activeTier = currentTier;
		}


	}

	jQuery('[data-calc-sum="order_qty_total"]').change(highlightCurrentTier);
	highlightCurrentTier();

	// console.log(tiers)
</script>
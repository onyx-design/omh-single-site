<?php
// Exit if accessed directly
if (!defined('ABSPATH')) exit;

$allowed_roles = array('house_admin');

$chapter = OMH_Session::get_chapter();

$enable_load_data = true;
$enable_ajax_search = true;
?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row">

			<div class="col-md-12">
				<?php
				include(OMH()->dashboard_template_path('dashboard-header'));
				?>
				<?php
				echo OMH_HTML_UI_Alert::factory(
					array(
						'color'	=> 'info',
						'text'	=> "Add active members from your chapter so they can be notified when new campaigns are launched and retail products are published to your store. To make a member a House Admin, hover over the member's email and click Make House Admin.<br>",
						'dismissible'	=> false
					)
				);
				?>

				<div class="card shadow-sm">
					<div class="card-body">

						<?php
						echo OMH_HTML_UI_Button::factory(
							array(
								'color'		=> 'primary',
								'size'		=> 'small',
								'label'		=> 'Add House Members',
								'attrs'		=> array(
									'data-toggle'		=> 'modal',
									'data-target'		=> '#invite-users-modal',
								),
							)
						);

						echo OMH_HTML_UI_Icon::factory(
							array(
								'class' => 'mh-question-icon-tooltip',
								'icon'  =>  'question-circle',
								'attrs'	=> array(
									'data-toggle' 		=> 'tooltip',
									'data-original-title' => "After adding a member, hover over their email for more options."
								),
							)
						);

						echo OMH_Table::factory(
							array(
								'type'			=> 'Users',
								'scopes'	=> array('members'),
							)
						)->get_ui_table(array('force_scope' => 'members'));
						?>
					</div>
				</div>

			</div>

			<!-- Invite User Modal -->
			<div class="modal fade mh-modal" id="invite-users-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="invite-users-modal-title">Add House Members</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form id="mh-form-invite-members" class="mh-form form-not-initialized needs-validation row pb-0" data-ajax-action="invite_house_members" data-form-nonce="<?php echo wp_create_nonce('invite-house-members'); ?>" data-object-id="<?php echo get_current_user_id(); ?>">

								<div class="col-12">
									Enter email addresses of active members. You can copy/paste a column of email addresses - one entry per line. After they are added as House Members, you can hover over their name to change their role to House Admin.
								</div>

								<div class="col-12">
									<?php
									echo OMH_HTML_UI_Textarea::factory(
										array(
											'input_id'		=> 'email_list',
											'label'			=> false,
											'required'		=> true,
											'form_text'		=> 'One entry per line',
											'contents'		=> array(
												'input'			=> array(
													'attrs'			=> array(
														'rows'		=> 5
													)
												)
											)
										)
									);

									echo OMH_HTML_UI_Input::factory(
										array(
											'input_id'		=> 'chapter_id',
											'input_type'	=> 'hidden',
											'class'			=> 'd-none',
											'value'			=> $chapter ? $chapter->get_id() : 0
										)
									);
									?>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<?php
							echo OMH_HTML_UI_Button::factory(
								array(
									'color'		=> 'secondary',
									'label'		=> 'Cancel',
									'attrs'		=> array(
										'data-dismiss'		=> 'modal',
									)
								)
							);

							echo OMH_HTML_UI_Button::factory(
								array(
									'color'		=> 'primary',
									'value'		=> 'submit',
									'form'		=> 'mh-form-invite-members',
									'label'		=> 'Submit',
									'class' 	=> 'mh-forms-submit'
								)
							);
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Invite User Modal -->

			<!-- Remove House Member Modal -->
			<div class="modal fade mh-modal" id="remove-house-member-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="remove-house-member-modal-title">Remove House Member</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form id="mh-form-remove-house-member" class="mh-form form-not-initialized allow-save-unchanged row pb-0" data-ajax-action="remove_house_user" data-form-nonce="<?php echo wp_create_nonce('remove-house-user'); ?>" data-omh-form-model="omh_user">
								<div class="col-12">
									Are you sure you want to remove this user from your chapter?
								</div>
								<?php
								echo OMH_HTML_UI_Input::factory(
									array(
										'input_id'		=> 'user_id',
										'input_type'	=> 'hidden',
										'value'			=> ''
									)
								);
								?>
								<div class="col-md-12 text-center">
									<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_id'		=> 'email',
											'label'			=> 'Email',
											'required'		=> false,
											'value'			=> '',
											'disabled'		=> true,
										)
									);
									?>
								</div>
								<div class="col-md-12 text-center">
									<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_id'		=> 'name',
											'label'			=> 'Name',
											'required'		=> false,
											'value'			=> '',
											'disabled'		=> true,
										)
									);
									?>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<?php
							echo OMH_HTML_UI_Button::factory(
								array(
									'color'		=> 'secondary',
									'label'		=> 'Cancel',
									'attrs'		=> array(
										'data-dismiss'		=> 'modal',
									)
								)
							);

							echo OMH_HTML_UI_Button::factory(
								array(
									// 'color'		=> 'danger',
									'value'		=> 'submit',
									'form'		=> 'mh-form-remove-house-member',
									'label'		=> 'Remove',
									'class' 	=> 'mh-forms-submit'
								)
							);
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Remove House Member Modal -->

		</div>
	</div>
</div>
<script type="text/javascript">
	var tooltipDefaults = {
		container: 'body',
		placement: 'right',
		trigger: 'hover',
		boundary: 'viewport',
		delay: {
			show: 400,
			hide: 100
		}
	};
	var $ = jQuery;

	var table_security = '<?php echo wp_create_nonce('omh-mh-table-request'); ?>';
	var omh_security = '<?php echo wp_create_nonce('manage-users'); ?>';
	var change_role_security = '<?php echo wp_create_nonce('manage-users'); ?>';

	function changeRole(userID, newRole) {
		ajax_obj = {
			user_id: parseInt(userID),
			new_role: newRole
		}
		omh.ajax("omh_set_user_role", ajax_obj, null, change_role_security);
	}

	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip(tooltipDefaults);
	})
</script>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_page_title = 'My Account';

$omh_breadcrumbs = array(
	array(
		'label'		=> 'My Account',
		'url'		=> OMH()->dashboard_url( 'account-details' ),
	),
	array(
		'label'		=> 'My Addresses',
		'active'	=> true
	)
);

$header_tabs = array(
	'account_details' 	=> array(
		'name'			=> 'Account Details',
		'destination'	=> OMH()->dashboard_url( 'account-details' )
	),
	'password_change'	=> array(
		'name'			=> 'Change Password',
		'destination'	=> OMH()->dashboard_url( 'password-change' )
	),
	'my_addresses' 		=> array(
		'name'			=> 'My Addresses',
		'active'		=> true,
		'destination'	=> OMH()->dashboard_url( 'my-addresses' )
	)
);
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row">
			
			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>
				<form id="mh-screen-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="save_user_addresses" data-form-nonce="<?php echo wp_create_nonce( 'save-user-addresses' ); ?>" data-object-id="<?php echo $omh_user->ID; ?>">
					<div class="row">
						<div class="col-md-4">
							<h3>Billing Address</h3>
						</div>
						<div class="col-md-8">
							<div class="card shadow-sm">
								<div class="card-body">
									<div class="row">
										<div class="col-md-6">
											<?php 
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'	=> 'text',
														'input_id'		=> 'billing_first_name',
														'label'			=> 'First Name',
														'value'			=> $omh_user->get_meta( 'billing_first_name', true ),
														'required'		=> true
													)
												); 
											?>
										</div>
										<div class="col-md-6">
											<?php 
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'	=> 'text',
														'input_id'		=> 'billing_last_name',
														'label'			=> 'Last Name',
														'value'			=> $omh_user->get_meta( 'billing_last_name', true ),
														'required'		=> true
													)
												); 
											?>
										</div>
										<div class="col-md-12">
											<?php 
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'	=> 'text',
														'input_id'		=> 'billing_country',
														'label'			=> 'Country',
														'value'			=> $omh_user->get_meta( 'billing_country', true ),
														'required'		=> true
													)
												); 
											?>
										</div>
										<div class="col-md-12">
											<?php 
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'	=> 'text',
														'input_id'		=> 'billing_address_1',
														'label'			=> 'Street Address',
														'value'			=> $omh_user->get_meta( 'billing_address_1', true ),
														'required'		=> true
													)
												); 
											?>
											<?php 
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'	=> 'text',
														'input_id'		=> 'billing_address_2',
														'label'			=> false,
														'placeholder'	=> 'Apartment, suite, unit etc. (optional)',
														'value'			=> $omh_user->get_meta( 'billing_address_2', true ),
														'required'		=> false
													)
												); 
											?>
										</div>
										<div class="col-md-12">
											<?php 
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'	=> 'text',
														'input_id'		=> 'billing_city',
														'label'			=> 'Town / City',
														'value'			=> $omh_user->get_meta( 'billing_city', true ),
														'required'		=> true
													)
												); 
											?>
										</div>
										<div class="col-md-12">
											<?php 
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'	=> 'text',
														'input_id'		=> 'billing_state',
														'label'			=> 'State',
														'value'			=> $omh_user->get_meta( 'billing_state', true ),
														'required'		=> true
													)
												); 
											?>
										</div>
										<div class="col-md-12">
											<?php 
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'	=> 'text',
														'input_id'		=> 'billing_postcode',
														'label'			=> 'Zip',
														'value'			=> $omh_user->get_meta( 'billing_postcode', true ),
														'required'		=> true
													)
												); 
											?>
										</div>
										<div class="col-md-6">
											<?php 
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'	=> 'tel',
														'input_id'		=> 'billing_phone',
														'label'			=> 'Phone',
														'placeholder'	=> '(000) 000-0000',
														'value'			=> $omh_user->get_meta( 'billing_phone', true ),
														'required'		=> true
													)
												); 
											?>
										</div>
										<div class="col-md-6">
											<?php 
												echo OMH_HTML_UI_Input::factory(
													array(
														'input_type'	=> 'email',
														'input_id'		=> 'billing_email',
														'label'			=> 'Email Address',
														'value'			=> $omh_user->get_meta( 'billing_email', true ),
														'required'		=> true
													)
												); 
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col">
							<hr>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<h3>Shipping Address</h3>
						</div>
						<div class="col-md-8">
							<div class="card shadow-sm">
								<div class="card-body">
									<div class="row pb-0">
										<div class="col">
											<div class="form-check">
												<input class="form-check-input" checked type="checkbox" name="ship_to_same_address" id="ship_to_same_address" data-toggle="collapse" data-target="#shippingAddress">
												<label class="form-check-label" for="ship_to_same_address">
													Use Billing Address as Shipping Address
												</label>
											</div>
										</div>
									</div>
									<div class="collapse" id="shippingAddress">
										<div class="row">
											<div class="col-md-6">
												<?php 
													echo OMH_HTML_UI_Input::factory(
														array(
															'input_type'	=> 'text',
															'input_id'		=> 'shipping_first_name',
															'label'			=> 'First Name',
															'value'			=> $omh_user->get_meta( 'shipping_first_name', true ),
															'required'		=> true
														)
													); 
												?>
											</div>
											<div class="col-md-6">
												<?php 
													echo OMH_HTML_UI_Input::factory(
														array(
															'input_type'	=> 'text',
															'input_id'		=> 'shipping_last_name',
															'label'			=> 'Last Name',
															'value'			=> $omh_user->get_meta( 'shipping_last_name', true ),
															'required'		=> true
														)
													); 
												?>
											</div>
											<div class="col-md-12">
												<?php 
													echo OMH_HTML_UI_Input::factory(
														array(
															'input_type'	=> 'text',
															'input_id'		=> 'shipping_country',
															'label'			=> 'Country',
															'value'			=> $omh_user->get_meta( 'shipping_country', true ),
															'required'		=> true
														)
													); 
												?>
											</div>
											<div class="col-md-12">
												<?php 
													echo OMH_HTML_UI_Input::factory(
														array(
															'input_type'	=> 'text',
															'input_id'		=> 'shipping_address_1',
															'label'			=> 'Street Address',
															'value'			=> $omh_user->get_meta( 'shipping_address_1', true ),
															'required'		=> true
														)
													); 
												?>
												<?php 
													echo OMH_HTML_UI_Input::factory(
														array(
															'input_type'	=> 'text',
															'input_id'		=> 'shipping_address_2',
															'label'			=> false,
															'placeholder'	=> 'Apartment, suite, unit etc. (optional)',
															'value'			=> $omh_user->get_meta( 'shipping_address_2', true ),
															'required'		=> false
														)
													); 
												?>
											</div>
											<div class="col-md-12">
												<?php 
													echo OMH_HTML_UI_Input::factory(
														array(
															'input_type'	=> 'text',
															'input_id'		=> 'shipping_city',
															'label'			=> 'Town / City',
															'value'			=> $omh_user->get_meta( 'shipping_city', true ),
															'required'		=> true
														)
													); 
												?>
											</div>
											<div class="col-md-12">
												<?php 
													echo OMH_HTML_UI_Input::factory(
														array(
															'input_type'	=> 'text',
															'input_id'		=> 'shipping_state',
															'label'			=> 'State',
															'value'			=> $omh_user->get_meta( 'shipping_state', true ),
															'required'		=> true
														)
													); 
												?>
											</div>
											<div class="col-md-12">
												<?php 
													echo OMH_HTML_UI_Input::factory(
														array(
															'input_type'	=> 'text',
															'input_id'		=> 'shipping_postcode',
															'label'			=> 'Zip',
															'value'			=> $omh_user->get_meta( 'shipping_postcode', true ),
															'required'		=> true
														)
													); 
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>

				<div class="row">
					<div class="col">
						<hr>
					</div>
				</div>

				<div class="row">
					<div class="col text-right">
						<?php  
							echo OMH_HTML_UI_Button::factory(
								array(
									'color'		=> 'primary',
									'value'		=> 'submit',
									'form'		=> 'mh-screen-form',
									'label'		=> 'Save',
									'class' 	=> 'mh-forms-submit'
								)
							);
						?>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_breadcrumbs = array(
	array(
		'label'		=> 'My Account',
		'url'		=> OMH()->dashboard_url( 'account-details' ),
	),
	array(
		'label'		=> 'My Orders',
		'active'	=> true
	)
);
?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row">

			<div class="col-md-12">

				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">
						<?php
							echo OMH_Table::factory(
								array(
									'type'		=> 'Orders',
									'scopes'	=> array( 'my' )
								)
							)->get_ui_table();
						?>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
	var omh_security = table_security;
</script>
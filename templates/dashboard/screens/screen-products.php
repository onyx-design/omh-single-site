<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$allowed_roles = false;

$enable_load_data = true;
$enable_ajax_search = true;
?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row">

			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">
						<?php
							echo OMH_Table::factory(
								array(
									'type'	=> 'Products'
								)
							)->get_ui_table();
						?>
					</div>
				</div>

			</div>

			<!-- Add Convert to Retail Modal -->
			<div class="modal fade mh-modal" id="convert-to-retail-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="convert-to-retail-modal-title">Convert to Retail</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form 
								id="mh-form-convert-to-retail"
								class="mh-form form-not-initialized needs-validation row pb-0"
								data-ajax-action="product_convert_to_retail"
								data-form-nonce="<?php echo wp_create_nonce( 'product-convert-to-retail' ); ?>"
								data-omh-form-model="wp_products"
							>

								<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Select::factory(
											array(
												'input_id'			=> 'product_status',
												'label'				=> 'Status',
												// 'required'			=> true,
												'value'				=> '',
												'select_options'	=> array_map( function( $field ) {
													return $field['label'];
												}, OMH_Product_Variable_Retail::$product_statuses )
											)
										);
									?>
								</div>

								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'hidden',
											'input_id'		=> 'ID',
											'value'			=> '',
											'class'			=> 'd-none'
										)
									);
								?>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-convert-to-retail',
										'label'		=> 'Save',
										'class' 	=> 'mh-forms-submit submit-allow-valid'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Convert to Retail Modal -->

			<!-- Add Convert to Campaign Modal -->
			<div class="modal fade mh-modal" id="convert-to-campaign-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="convert-to-campaign-modal-title">Convert to Retail</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form 
								id="mh-form-convert-to-campaign"
								class="mh-form form-not-initialized needs-validation row pb-0"
								data-ajax-action="product_convert_to_campaign"
								data-form-nonce="<?php echo wp_create_nonce( 'product-convert-to-campaign' ); ?>"
								data-omh-form-model="wp_products"
							>

								<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Select::factory(
											array(
												'input_id'			=> 'product_status',
												'label'				=> 'Status',
												// 'required'			=> true,
												'value'				=> '',
												'select_options'	=> array_map( function( $field ) {
													return $field['label'];
												}, OMH_Product_Variable_Campaign::get_product_statuses() )
											)
										);
										
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_type'		=> 'datepicker',
												'input_id'			=> 'campaign_end_date',
												'label'				=> 'End Date',
												'required'			=> true,
												'value'				=> '',
												'input_attrs'		=> array(
													'data-flatpickr-options'	=> base64_encode( json_encode( array(
														'minDate'	=> null
													)))
												)
											)
										);

										echo OMH_HTML_UI_Input::factory(
											array(
												'input_type'		=> 'number',
												'input_id'			=> 'campaign_target_sales',
												'label'				=> 'Target Sales',
												'required'			=> true,
												'input_attrs'		=> array(
													'step'				=> 1
												),
												'value'				=> ''
											)
										);
									?>
								</div>

								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'hidden',
											'input_id'		=> 'ID',
											'value'			=> '',
											'class'			=> 'd-none'
										)
									);
								?>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-convert-to-campaign',
										'label'		=> 'Save',
										'class' 	=> 'mh-forms-submit submit-allow-valid'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Convert to Campaign Modal -->

			<!-- Add Convert to Bulk Modal -->
			<div class="modal fade mh-modal" id="convert-to-bulk-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="convert-to-bulk-modal-title">Convert to Bulk</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form 
								id="mh-form-convert-to-bulk"
								class="mh-form form-not-initialized needs-validation row pb-0"
								data-ajax-action="product_convert_to_bulk"
								data-form-nonce="<?php echo wp_create_nonce( 'product-convert-to-bulk' ); ?>"
								data-omh-form-model="wp_products"
							>
								<div class="col-md-12">
									<?php
										// dev:todo How do we return this from OMH AJAX?
										echo OMH_HTML_UI_Select::factory(
											array(
												'input_id'			=> 'product_status',
												'label'				=> 'Status',
												// 'required'			=> true,
												'value'				=> '',
												'select_options'	=> array_map( function( $field ) {
													return $field['label'];
												}, OMH_Product_Variable_Bulk::get_product_statuses() )
											)
										);
									?>
								</div>

								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'hidden',
											'input_id'		=> 'ID',
											'value'			=> '',
											'class'			=> 'd-none'
										)
									);
								?>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-convert-to-bulk',
										'label'		=> 'Save',
										'class' 	=> 'mh-forms-submit submit-allow-valid'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Convert to Bulk Modal -->

			<!-- Add Delete Product Modal -->
			<div class="modal fade mh-modal" id="delete-product-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="delete-product-modal-title">Delete Product</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form 
								id="mh-form-delete-product"
								class="mh-form form-not-initialized allow-save-unchanged row pb-0"
								data-ajax-action="delete_product"
								data-form-nonce="<?php echo wp_create_nonce( 'delete-product' ); ?>"
								data-omh-form-model="wp_products"
							>
								<div class="col-md-12 text-center">
								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'number',
											'input_id'		=> 'ID',
											'label'			=> 'Product ID',
											'value'			=> '',
											'disabled'		=> true,
											'required'		=> true,
											'input_class'	=> 'text-center'
										)
									);

									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'text',
											'input_id'		=> 'product_name',
											'label'			=> 'Name',
											'value'			=> '',
											'disabled'		=> true,
											'required'		=> true,
											'input_class'	=> 'text-center'
										)
									);

									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'text',
											'input_id'		=> 'product_chapter',
											'label'			=> 'Chapter',
											'value'			=> '',
											'disabled'		=> true,
											'required'		=> true,
											'input_class'	=> 'text-center'
										)
									);
								?>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-delete-product',
										'label'		=> 'Delete',
										'class' 	=> 'mh-forms-submit'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Delete Product Modal -->

		</div>
	</div>
</div>

<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
	var omh_security = '<?php echo wp_create_nonce( 'set-featured-product' ); ?>';

	(function($) {
		$(document).ready(function() {

			var $ajax_action = 'omh_set_featured_product',
				$ajax_data = {};

			$(document).on('change', '.featured-product-checkbox', function(e){

				$ajax_data = {
					obj_id		: $( this ).data( 'objectId' ),
					form_data 	: {
						featured_product : $( this ).prop( 'checked' )
					}
				}

				omh.ajax(
					$ajax_action,
					$ajax_data,
					null,
					omh_security
				);

			});
		});
	})(jQuery);
</script>
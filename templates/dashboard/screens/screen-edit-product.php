<?php
// Exit if accessed directly
if (!defined('ABSPATH')) exit;

$allowed_roles = array('house_admin');

// If product doesn't exist, redirect everyone
$product_id = (int) (empty($_GET['id']) ? null : $_GET['id']);
if (
	!isset($product_id)
	|| !($omh_product = wc_get_product($product_id))
) {
	wp_redirect(OMH()->dashboard_url('retail-products'));
	exit;
}

// If design is pending and we're not a super admin, we need to redirect
// dev:improve dev:todo Product Statuses have changed and now use Product Visibility
// if( 'active' != $omh_product->get_mh_product_status() ) {
// 	$allowed_roles = false;
// }

/**
 * @tag Product Status
 * @tag Visibility
 */

ob_start();
include(OMH()->dashboard_template_path('dashboard-header-products'));
$omh_header_extra_content = ob_get_clean();
$omh_page_pretitle = get_the_title();
$omh_page_title = $omh_product->get_title();
$omh_sales_type = $omh_product->get_sales_type();

$products_url = OMH()->dashboard_url('retail-products');
if ('bulk' === $omh_sales_type) {
	$products_url = OMH()->dashboard_url('bulk-orders');
} elseif ('campaign' === $omh_sales_type) {
	$products_url = OMH()->dashboard_url('campaign-products');
}

$omh_breadcrumbs = array(
	array(
		'label'		=> OMH_Session::get_chapter()->get_chapter_name(),
		'url'		=> OMH()->dashboard_url()
	),
	array(
		'label'		=> 'Products',
		'url'		=> $products_url
	),
	array(
		'label'		=> 'Edit Product',
		'active'	=> true
	)
);

$omh_page_actions = array();

// Maybe add action buttons for house admins
$omh_user = wp_get_current_user();
if ($omh_user->has_role('house_admin')) {

	if (in_array($omh_product->get_product_status(), array('unpublished', 'ready_to_list'))) {

		$omh_page_actions['publish-product'] = array(
			'label'	=> 'Publish',
			'class' => 'btn btn-primary btn-sm',
			'url' => '#publish-product'
		);
	} else if (in_array($omh_product->get_product_status(), array('published', 'private'))) {
		$omh_page_actions['unpublish-product'] = array(
			'label'	=> 'Unpublish',
			'icon' => 'archive',
			'url' => '#unpublish-product'
		);
	}
}

// Matbe add View Product page action
if (
	in_array($omh_product->get_product_status(), array('published', 'private'))
	|| ($omh_user->has_role('administrator') && ($omh_product->get_product_status('step') > 1) && ($omh_product->get_sales_type() !== 'bulk'))
) {
	$omh_page_actions['view-product'] = array(
		'label'			=> 'View Product',
		'url'			=> $omh_product->get_permalink(),
		'new_tab'		=> true,
		'icon'			=> 'eye'
	);
}

// $omh_page_actions = array(
// 	'view-product' => array(
// 		'label'			=> 'View Product',
// 		'url'			=> $omh_product->get_permalink(),
// 		'new_tab'		=> true,
// 		'icon'			=> 'eye'
// 	),
// );


if (is_super_admin()) {

	$header_tabs = array(
		'edit_product'			=> array(
			'name'			=> 'Edit Product',
			'active'		=> true,
			'destination'	=> OMH()->dashboard_url('edit-product?id=' . $product_id)
		),
		// 'edit_product_design'	=> array( // Removing this temporarily
		// 	'name'			=> 'Product Design',
		// 	'destination'	=> OMH()->dashboard_url('edit-product-design?id=' . $product_id)
		// ),
	);

	if ('bulk' === $omh_sales_type) {
		$headers_tabs['edit_bulk_order'] = array(
			'name'			=> 'Edit Bulk Order',
			'destination'	=> OMH()->dashboard_url('edit-bulk-order?id=' . $product_id)
		);
	} else {
		$headers_tabs['product_sales'] = array(
			'name'			=> 'Product Sales',
			'destination'	=> OMH()->dashboard_url('product-sales?id=' . $product_id)
		);
	}
}


$enable_load_data = true;
$enable_ajax_search = true;

// Determine product categories
$product_cats = array();
$selected_cats = $omh_product->get_category_ids();

foreach (get_terms('product_cat', array('hide_empty' => false)) as $product_cat) {

	$product_cats[$product_cat->term_id] = array(
		'term_id'	=> $product_cat->term_id,
		'name'		=> $product_cat->name,
		'count'		=> $product_cat->count,
		'parent_id'	=> $product_cat->parent,
		'selected'	=> in_array($product_cat->term_id, $selected_cats)
	);
}


// Product Status logic
$product_status_options = $omh_product::get_product_statuses();

if (!is_super_admin()) {
	// filter options for house admin
	$product_status_options = array_filter($product_status_options, function ($var) {
		return $var['house_admin_can_set'] ?? false;
	});
}

$status_selections = array();

foreach ($product_status_options as $status_slug => $status_details) {
	$status_selections[$status_slug] = $status_details['label'];
}
// get_earnings_percentage
$total_sales = (float) $omh_product->get_total_sales();
$garment_meta = $omh_product->get_garments_meta();
$base_price = array_column($garment_meta, 'base_price')[0];
$retail_price = array_column($garment_meta, 'retail_price')[0];
$earnings_per_product = (float) $retail_price - $base_price;
$total_earnings = $total_sales * $earnings_per_product;
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row screen--edit-product">
			<!-- Page Header -->
			<div class="col">
				<?php
				include(OMH()->dashboard_template_path('dashboard-header'));
				if (empty($_GET['updated']) ? false : $_GET['updated']) {
					/*
						 * Alert & Message
						 */
					echo OMH_HTML_UI_Alert::factory(
						array(
							'color'			=> 'success',
							'text'			=> 'Product updated'
						)
					);
				}
				?>
				<!-- Edit Garment Form -->
				<form id="mh-screen-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="save_dashboard_product_details" data-form-nonce="<?php echo wp_create_nonce('save-dashboard-product-details'); ?>" data-object-id="<?php echo $product_id; ?>">
					<!-- If product is retail or campaign no need to show price tiers -->
					<?php if ('bulk' === $omh_sales_type) : ?>
						<div class="row">
							<div class="col-12">
								<div class="card shadow-sm">
									<div class="card-body">
										<h4>Price Tiers</h4>
										<div class="omh-table-wrap omh-table-wrap--product-price-tiers omh-field-wrap">
											<div class="omh-table-wrap-inner">
												<table data-omh-field-type="productPriceTiers" name="product_price_tiers" data-omh-form="mh-screen-form" class="table omh-table table-equal-width table--price-quantity-tiers pricing-tiers pricing-tiers--quantity  omh-field-input omh-field-advanced omh-field-advanced--price-tiers">
													<tbody>
														<tr class="omh-table-row price-tiers__header-row">
															<th>Tier</th>
														</tr>
														<tr class="omh-table-row price-tiers__values-row">
															<th>Min Qty</th>
														</tr>
													</tbody>
												</table>

											</div>

											<div class="price-tier__add-btn add-content-btn add-content-btn--right">
												<div class="add-content-btn__inner">
													<div class="add-content-btn__content">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endif ?>
					<div class="row">
						<div class="col">
							<div class="card shadow-sm">
								<div class="card-body">
									<h4>Variations</h4>
									<div class="product-garments-table-wrap omh-field-wrap">
										<?php
										echo OMH_Table::factory(
											array(
												'type' => 'Product_Garments',
												'query' => array(
													'product_id' => $product_id,
												)
											)
										)->get_ui_table(
											array(
												'class'	=> array(
													'omh-field-input',
													'omh-field-advanced',
													'omh-field-advanced--price-tiers',
													'table--static',
													'omh-field-input--load-first'
												),
												'load_table'	=> true,
												'attrs'	=> array(
													'data-omh-field-type'	=> 'garmentsTable',
													'name'	=> 'product_garments',
													'data-omh-form' => 'mh-screen-form'
												)
											)
										);
										?>

										<?php if (is_super_admin()) : ?>
											<div class="add-content-btn add-content-btn--bottom garment__add-btn" data-toggle="modal" data-target="#add_garment-modal">
												<div class="add-content-btn__inner">
													<div class="add-content-btn__content">
														Add Garment
													</div>
												</div>
											</div>
										<?php endif; ?>


									</div>
								</div>
							</div>
						</div>
					</div>
					<?php if ($omh_user->has_role('administrator')) : ?>
						<div class="row">
							<div class="col-md-8">
								<div class="card shadow-sm">
									<div class="card-body">
										<h4>Details</h4>
										<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id' 	=> 'product_name',
												'label' 	=> 'Product Name',
												'required' 	=> true,
												// 'disabled'	=> true,
												'value'		=> $omh_product->get_title()
											)
										);
										?>

										<label for="product_description">
											Description
											<small class="form-text text-muted">Enter any information here you would like your customers to know about this product.</small>
										</label>
										<?php
										echo OMH_HTML_UI_Textarea::factory(
											array(
												'input_id'		=> 'product_description',
												'label'			=> false,
												'value'			=> $omh_product->get_description(),
												'contents'		=> array(
													'input'			=> array(
														'attrs'			=> array(
															'rows'		=> 5,
															'cols'		=> 50
														)
													)
												)
											)
										);
										?>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="row">
									<div class="col">
										<div class="card shadow-sm">
											<div class="card-body">
												<h4>Product Visibility</h4>
												<?php
												echo OMH_HTML_UI_Select::factory(
													array(
														'input_id'			=> 'product_status',
														'label'				=> 'Product Status',
														'value'				=> $omh_product->get_product_status(),
														'select_options'	=> $status_selections
													)
												);
												echo OMH_HTML_UI_Select2::factory(
													array(
														'input_id'		=> 'product_categories',
														'label'			=> 'Categories',
														'input_attrs'	=> array(
															'multiple'		=> 'multiple'
														)
													)
												);
												?>
											</div>
										</div>
									</div>
								</div>

								<?php if ('campaign' === $omh_sales_type) : ?>
									<div class="row">
										<div class="col">
											<div class="card shadow-sm">
												<div class="card-body">
													<h4>Campaign Settings</h4>
													<?php
													echo OMH_HTML_UI_Input::factory(
														array(
															'input_type'		=> 'datepicker',
															'input_id'			=> 'campaign_end_date',
															'label'				=> 'End Date',
															'required'			=> true,
															'contents'			=> array(
																'input'				=> array(
																	'attrs'				=> array(
																		'data-allow-past'	=> 'true'
																	)
																)
															),
															'value'				=> $omh_product->get_campaign_end_date(),
														)
													);

													echo OMH_HTML_UI_Input::factory(
														array(
															'input_type'		=> 'number',
															'input_id'			=> 'campaign_target_sales',
															'label'				=> 'Target Sales',
															'required'		=> true,
															'contents'		=> array(
																'input'			=> array(
																	'attrs'			=> array(
																		'step'			=> '1',
																	),
																)
															),
															'value'				=> $omh_product->get_campaign_target_sales()
														)
													);
													?>
												</div>
											</div>
											<div class="card shadow-sm">
												<div class="card-body">
														<div class="input-group">

															<div class="form-control" data-omh-field-type="price">
																<span class="form-text  small text-muted" >Total # Sold</span>
																<input class="form-control disabled price-val no-validation-state" data-virtual-disabled="true" type="number" min="0.00" step="0.01" omh-var-type="price" tabindex="-1" value=<?php echo $total_sales ?> />
															</div>
															<div class="form-control" data-omh-field-type="earnings">
																<span class="form-text small text-muted" >Earnings P/S</span>
																<input class="form-control disabled price-val no-validation-state" data-virtual-disabled="true" type="number" min="0.00" step="0.01" omh-var-type="price" tabindex="-1" value=<?php echo number_format($earnings_per_product,2); ?> />
															</div>
															<div class="form-control" data-omh-field-type="price">
																<span class="form-text  small text-muted" >Total Earnings</span>
																<input class="form-control disabled price-val no-validation-state" data-virtual-disabled="true" type="number" min="0.00" step="0.01" omh-var-type="price" tabindex="-1" value=<?php echo number_format($total_earnings,2); ?>>
															</div>
														</div>
												</div>
											</div>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php endif; ?>

					<?php if ($omh_user->has_role('administrator')) : ?>
						<div class="row">
							<div class="col">
								<hr>
							</div>
						</div>
						<div class="row">
							<div class="col text-right">
								<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-screen-form',
										'label'		=> 'Save Product',
										'class' 	=> 'mh-form-submit'
									)
								);
								?>
							</div>
						</div>
					<?php endif; ?>
				</form>
				<!-- Add Garment Modal -->
				<div class="modal fade mh-modal" id="add_garment-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="add_garment-modal-title">Add Garment</h4>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form id="mh-form-add-garment" class="mh-form form-not-initialized needs-validation row pd-0">
									<div class="col-md-12">
										<?php
										echo OMH_HTML_UI_Select2::factory(
											array(
												'input_id'		=> 'add_garment_id',
												'label'			=> 'Garment',
												'value'			=> '',
												'placeholder'	=> 'Select a Garment',
												'search_model'	=> 'garment_styles'
											)
										);
										?>
									</div>
							</div>
							</form>
							<div class="modal-footer">
								<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-dismiss'		=> 'modal'
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'form'		=> false,
										'color'		=> 'primary',
										'value'		=> 'submit',
										'label'		=> 'Add Garment',
										'class' 	=> 'garment__add-confirm',
										'attrs'		=> array(
											'disabled'		=> true
										)
									)
								);
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var omh_security = '<?php echo wp_create_nonce('get-product-garment'); ?>',
		productStatusSecurity = '<?php echo wp_create_nonce('product-status-action'); ?>';

	var omhProductData = {
		priceTiers: <?php echo json_encode($omh_product->get_product_price_tiers()); ?>,
		garmentData: <?php echo json_encode($omh_product->get_frontend_product_garments()); ?>,
		productCats: <?php echo json_encode($product_cats); ?>,
		enabledCats: <?php echo json_encode($omh_product->get_category_ids()); ?>
	};

	var selectedNewGarment;

	var productCatsField;

	jQuery(document).on('omhloaded', function() {
		var $ = jQuery;

		var $addGarmentConfirm = $('.garment__add-confirm'),
			addGarmentIdField = $('#add_garment_id').get(0).omhFormField,
			garmentsTableField = $('[data-omh-field-type="garmentsTable"]').get(0).omhFormField;

		addGarmentIdField.$input.on('select2:select', function(event) {
			selectedNewGarment = event.params.data;
			$addGarmentConfirm.attr('disabled', false);

		});

		$addGarmentConfirm.click(function() {

			garmentsTableField.addNewGarment(selectedNewGarment);
			$('#add_garment-modal').modal('hide');
		})

		$('#add_garment-modal').on('hidden.bs.modal', function(event) {
			$addGarmentConfirm.attr('disabled', true);
			addGarmentIdField.$input.val(null).trigger('change');
			// console.log('modal hidden', event);
		});


		// find any input with an id with base_price and track event changes
		$(document).on('change', '.campaign-row--template input[id^="base_price"]', function(event) {
			var basePriceField = _.get(event.target, 'omhFormField');
			var retailPriceField = _.get(basePriceField, 'parent.fields.retail_price');

			if (!basePriceField || !retailPriceField) {
				return;
			}

			var baseVal = basePriceField.getValue();
			var retailVal = retailPriceField.getValue()

			if ((retailVal || 0) < event.target.valueAsNumber) {
				retailPriceField.setValue(baseVal);
				retailPriceField.$input.trigger('change');
			}

			$(retailPriceField).prop('min', baseVal).attr('min', baseVal)
		});

		// find any input with an id with retail_price and track event changes
		$(document).on('change', '.campaign-row--template input[id^="retail_price"]', function(event) {
			var retailPriceField = _.get(event.target, 'omhFormField');
			var basePriceField = _.get(retailPriceField, 'parent.fields.base_price');
			var minVal = basePriceField.getValue();

			// don't allow the user to set the retail price lower than the base price
			if (minVal > event.target.valueAsNumber) {
				retailPriceField.setValue(minVal)
			}
		});

		$(document).on('click', 'a[href$="#publish-product"]', function(e) {
			e.preventDefault();
			e.stopPropagation();

			var $ajax_data = {
				product_id: <?php echo $product_id; ?>
			}

			omh.ajax(
				'omh_publish_product',
				$ajax_data,
				function(response) {
					if (_.defaultTo(response.data.reload, false)) {
						location.reload();
					}
				},
				productStatusSecurity
			);
		});

		$(document).on('click', 'a[href$="#unpublish-product"]', function(e) {
			e.preventDefault();

			var $ajax_data = {
				product_id: <?php echo $product_id; ?>
			}

			omh.ajax(
				'omh_unpublish_product',
				$ajax_data,
				function(response) {
					if (_.defaultTo(response.data.reload, false)) {
						location.reload();
					}
				},
				productStatusSecurity
			);
		});
	});


	jQuery('body')
		.on('omhFieldBeforeInit.add_garment_id', function(event, field, settings) {

			_.set(settings, 'select2.templateResult', function(result) {
				return [
					'<div class="select2-result select2-result--garment-style clearfix">',
					'<img class="select2--garment-thumb" src="' + result.thumb + '" />',
					result.text,
					'</div>'
				].join("\n");
			});



			settings.select2.escapeMarkup = function(markup) {
				return markup;
			};

		})
		.on('omhFieldBeforeInit.product_categories', function(event, field, settings) {

			function setCatChildrenDepths(parentCat) {

				if (_.has(parentCat, 'children')) {
					_.forEach(parentCat.children, function(childCat) {
						childCat.depth = parentCat.depth + 1;
						setCatChildrenDepths(childCat);
					});
				}



			}

			var productCats = omhProductData.productCats,
				productCatTree = [],
				enabledCats = [];

			_.forEach(productCats, function(cat, termId) {

				// Initialize additional category properties
				// cat.parent = null;
				// cat.children = {};
				if (cat.selected) {
					enabledCats.push(_.toString(cat.term_id));
				}


				if (cat.parent_id == 0) {
					cat.depth = 0;
					cat.children = _.defaultTo(cat.children, {});
					setCatChildrenDepths(cat);
					productCatTree.push(cat);
				} else if (_.has(productCats, cat.parent_id)) {

					cat.parent = productCats[cat.parent_id];

					cat.parent.children = _.defaultTo(cat.parent.children, {});

					cat.parent.children[cat.term_id] = cat;

					// Try to set the depth if the parent's has been set, otherwise it will be set when the parent gets processed
					if (_.has(cat.parent, 'depth')) {
						cat.depth = cat.parent.depth + 1;
					}


				}
			});

			field.settings.productCats = productCats;
			field.settings.productCatTree = productCatTree;

			field.flattenCats = function(catsTree) {
				catsTree = _.defaultTo(catsTree, this.settings.productCatTree);
				return _.flatMapDeep(catsTree, function(cat) {
					return _.concat([cat], _.values(_.defaultTo(cat.children, [])))
				});
			}

			field.getSelect2Data = function(flatCats) {
				flatCats = _.defaultTo(flatCats, this.flattenCats(this.settings.productCatTree));
				return _.map(flatCats, (cat) => ({
					id: _.toString(cat.term_id),
					text: cat.name,
					count: cat.count,
					selected: cat.selected
				}));
			}

			productCatsField = field;

			field.settings = _.defaultsDeep({
				presetValue: enabledCats,
				select2: {
					multiple: true,
					ajax: null,
					data: field.getSelect2Data(),
					minimumInputLength: 0,
					templateResult: function(result) {
						return [
							'<div class="select2-result select2-result--product-cat" data-cat-depth="' + result.depth + '">',
							result.text,
							'</div>'
						].join("\n");
					},
					escapeMarkup: function(markup) {
						return markup;
					}
				}
			}, field.settings);
		});
</script>
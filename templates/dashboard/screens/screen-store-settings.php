<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( !user_is_house_admin() ) {

	wp_redirect( omh_dashboard_url() );
	exit;
}

$current_site_salient_settings = get_blog_option( get_current_blog_id(), 'salient_redux' );
$store_settings = get_blog_option( get_current_blog_id(), 'store_settings' );

$shop_page_id = wc_get_page_id( 'shop' );
// $store_banner_image = get_post_meta( $shop_page_id, '_nectar_header_bg', true );
$store_description = get_post_meta( $shop_page_id, '_nectar_header_subtitle', true );
$store_location = $store_settings['store_location'] ?? '';
$store_facebook_url = isset( $current_site_salient_settings['facebook-url'] ) ? str_replace( 'https://facebook.com/', '', $current_site_salient_settings['facebook-url'] ) : '';
$store_instagram_url = isset( $current_site_salient_settings['instagram-url'] ) ? str_replace( 'https://instagram.com/', '', $current_site_salient_settings['instagram-url'] ) : '';
$store_twitter_url = isset( $current_site_salient_settings['twitter-url'] ) ? str_replace( 'https://twitter.com/', '', $current_site_salient_settings['twitter-url'] ) : '';
?>

<div id="mh-dashboard-wrap">
	<div class="container">
		<div id="mh-dashboard" class="row">
			
			<div class="col">
				<?php
					include( mh_get_dashboard_template_path( 'mh-dashboard-header' ) );
				?>

				<div class="row">
					<div class="col-md-4">
						<p>These settings control customization options for your storefront.</p>
						<p>Make your store your own with a banner image, description, and links to your social profiles!</p>
					</div>
					<div class="col-md-8">
						<div class="card shadow-sm">
							<div class="card-body">
								<form id="mh-screen-form" class="mh-form form-not-initialized needs-validation" data-submit-type="form" data-ajax-action="save_store_settings" data-form-nonce="<?php echo wp_create_nonce( 'save-store-settings' ); ?>" data-object-id="<?php echo get_current_blog_id(); ?>">

									<?php
										$banner_img_url = 
											!empty( $store_settings['store_banner_image'] ) ?
											wp_get_attachment_image_url( $store_settings['store_banner_image'], 'medium' ) :
											'';	
										// $banner_img_url = $store_banner_image;

										$banner_thumb = '<div class="no-image"></div>';


										$file_input_wrap_class = $banner_img_url ? 'file-has-image' : 'file-no-image';

										if( $banner_img_url ) {
											$banner_thumb = OMH_HTML_Tag::factory(
												array(
													'type'	=> 'img',
													'class'	=> 'mh-file-input-thumb mb-1',
													'attrs'	=> array(
														'src'	=> $banner_img_url
													)
												)
											);
										}
									?>

									<div class="form-group form-group-file-img <?php echo $file_input_wrap_class; ?>" data-omh-field-state="store_banner_image">
										<div class="row">
											<label class="col-12" for="store_banner_image">Store Banner Image</label>
											<div class="col-md-4 col-lg-2 pr-lg-0">
												<div class="form-file-img-wrap" data-omh-field-img-thumb="store_banner_image">
													<?php
														if( $banner_img_url ) {
															echo $banner_thumb;
														}
													?>
												</div>
											</div>
											<div class="col-md-8 col-lg-10">
												<div class="form-check form-file-state-wrap form-row pl-0 mx-0 omh-field-wrap">
													<input type="checkbox" class="form-check-input form-file-img-remove omh-field-input" name="store_banner_image_remove" id="store_banner_image_remove" />
													<label for="store_banner_image_remove">
														<span></span>
														<span></span>
													</label>
													<span class="small form-file-state-msg">
													</span>
												</div>
												<div>
													<div class="custom-file omh-field-wrap">
														<div class="input-group">
															<div class="custom-file custom-file-hide-default-label">
																<input type="file" class="custom-file-input never-input never-changed orig-value omh-field-input" name="store_banner_image" accept="image/jpeg, image/jpg" />
																<label class="custom-file-label">Choose New File</label>
															</div>
															<div class="input-group-append">
																<button class="btn btn-danger input-reset-btn" type="button" for="store_banner_image" style="display:none;"><i class="fa fa-close"></i></button>
																<button class="btn btn-secondary input-focus-btn" type="button" for="store_banner_image">Browse</button>
															</div>	

														</div>
														<small class="form-text text-muted">Please upload a .jpg or .jpeg file. It should be 1600 x 500 pixels.</small>
													</div>
												</div>
											</div>
										</div>
									</div>
									<?php 

										echo OMH_HTML_UI_Textarea::factory( 
											array(
												'input_id' 	=> 'store_description',
												'label' 	=> 'Store Description', 
												// 'value' 	=> wp_unslash(  ($store_settings['store_description'] ?? '') ),
												'value'		=> wp_unslash( $store_description ?: '' ),
											)
										);

										// echo OMH_HTML_UI_Input::factory(
										// 	array(
										// 		'input_id'	=> 'store_location',
										// 		'label'		=> 'Store Location',
										// 		// 'value'		=> $store_settings['store_location'] ?? '',
										// 		'value'		=> wp_unslash( $store_location ?: '' ),
										// 		'placeholder'=> 'City, State Code',
										// 		'prepend'	=> array(
										// 			'<i class="fa fa-map-marker"></i>'
										// 		)
										// 	) 
										// );
									?>

									<?php 
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'	=> 'social_facebook',
												'label'		=> 'Social Profiles',
												'class'		=> 'form-group input-social-profile',
												// 'value'		=> $store_settings['social_facebook'] ?? '',
												'value'		=> wp_unslash( $store_facebook_url ?: '' ),
												'prepend'	=> array(
													'<i class="fa fa-facebook"></i>',
													'facebook.com/'
												)
											) 
										);

										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'	=> 'social_instagram',
												'label'		=> false,
												'class'		=> 'form-group input-social-profile',
												// 'value'		=> $store_settings['social_instagram'] ?? '',
												'value'		=> wp_unslash( $store_instagram_url ?: '' ),
												'prepend'	=> array(
													'<i class="fa fa-instagram"></i>',
													'instagram.com/'
												)
											) 
										);

										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'	=> 'social_twitter',
												'label'		=> false,
												'class'		=> 'form-group input-social-profile',
												// 'value'		=> $store_settings['social_twitter'] ?? '',
												'value'		=> wp_unslash( $store_twitter_url ?: '' ),
												'prepend'	=> array(
													'<i class="fa fa-twitter"></i>',
													'twitter.com/'
												)
											) 
										);

									?>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col">
						<hr>
					</div>
				</div>

				<div class="row">
					<div class="col text-right">
						<?php  
							echo OMH_HTML_UI_Button::factory(
								array(
									'color'		=> 'primary',
									'value'		=> 'submit',
									'form'		=> 'mh-screen-form',
									'label'		=> 'Save',
									'class' 	=> 'mh-forms-submit'
								)
							);
						?>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
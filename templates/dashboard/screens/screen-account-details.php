<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_page_title = 'My Account';

$omh_breadcrumbs = array(
	array(
		'label'		=> 'My Account',
		'active'	=> true
	)
);

$header_tabs = array(
	'account_details' 	=> array(
		'name'			=> 'Account Details',
		'active'		=> true,
		'destination'	=> OMH()->dashboard_url( 'account-details' )
	),
	'password_change'	=> array(
		'name'			=> 'Change Password',
		'destination'	=> OMH()->dashboard_url( 'password-change' )
	),
	'my_addresses' 		=> array(
		'name'			=> 'My Addresses',
		'destination'	=> OMH()->dashboard_url( 'my-addresses' )
	),
);

$omh_user_shirt_size = $omh_user->get_meta( 't_shirt_size', true );
$omh_user_instagram_handle = $omh_user->get_meta( 'instagram_handle', true );

$omh_user_notifications_general = $omh_user->get_meta('_omh_notifications_general', true);
if (empty($omh_user_notifications_general)){
	$omh_user_notifications_general = true;
}

$has_house = $omh_user->has_house_role();


if($has_house) {
	$chapter = $omh_user->get_chapter(true);
	$chapter_name = $chapter->get_chapter_name();
	$omh_user_notifications_house_products = $omh_user->get_meta('_omh_notifications_house_products', true);
	if (empty($omh_user_notifications_house_products)){
		$omh_user_notifications_house_products = true;
	}
}

$is_admin = $omh_user->is_house_admin();
if ($is_admin){
	$omh_user_notifications_house_admin = $omh_user->get_meta('_omh_notifications_house_admin', true);
	if (empty($omh_user_notifications_house_admin)){
		$omh_user_notifications_house_admin = true;
	}
}
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row">
			
			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>
	<form id="mh-screen-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="save_user_account_details" data-form-nonce="<?php echo wp_create_nonce( 'save-user-account-details' ); ?>" data-object-id="<?php echo $omh_user->ID; ?>">
									
				<div class="row">
					<div class="col-md-4">
						<h3>Account Details</h3>
					</div>
					<div class="col-md-8">
						<div class="card shadow-sm">
							<div class="card-body">
							
									<?php 

										echo OMH_HTML_UI_Input::factory( 
											array(
												'input_id' 	=> 'first_name', 
												'label' 	=> 'First Name',
												'required' 	=> true,
												'value'		=> $omh_user->first_name
											) 
										); 

										echo OMH_HTML_UI_Input::factory( 
											array(
												'input_id' 	=> 'last_name', 
												'label' 	=> 'Last Name',
												'required' 	=> true,
												'value'		=> $omh_user->last_name
											) 
										); 

										echo OMH_HTML_UI_Input::factory( 
											array(
												'input_type'=> 'email',
												'input_id' 	=> 'user_email', 
												'label' 	=> 'Email Address',
												'required' 	=> true,
												'value'		=> $omh_user->user_email
											) 
										); 
									?>

									<?php 
										$shirt_size_options = array(
											'small'	=> array(
												'type'		=> 'option',
												'contents'	=> 'S',
												'attrs'		=> array(
													'value'		=> 'S',
													'selected' 	=> $omh_user_shirt_size == 'S' ? true : null
												)
											),
											'medium'	=> array(
												'type'		=> 'option',
												'contents'	=> 'M',
												'attrs'		=> array(
													'value'		=> 'M',
													'selected' 	=> $omh_user_shirt_size == 'M' ? true : null
												)
											),
											'large'	=> array(
												'type'		=> 'option',
												'contents'	=> 'L',
												'attrs'		=> array(
													'value'		=> 'L',
													'selected' 	=> $omh_user_shirt_size == 'L' ? true : null
												)
											),
											'x_large'	=> array(
												'type'		=> 'option',
												'contents'	=> 'XL',
												'attrs'		=> array(
													'value'		=> 'XL',
													'selected' 	=> $omh_user_shirt_size == 'XL' ? true : null
												)
											),
											'2x_large'	=> array(
												'type'		=> 'option',
												'contents'	=> '2XL',
												'attrs'		=> array(
													'value'		=> '2XL',
													'selected' 	=> $omh_user_shirt_size == '2XL' ? true : null
												)
											),
											'3x_large'	=> array(
												'type'		=> 'option',
												'contents'	=> '3XL',
												'attrs'		=> array(
													'value'		=> '3XL',
													'selected' 	=> $omh_user_shirt_size == '3XL' ? true : null
												)
											)
										);

										if( !$omh_user_shirt_size ) {
											array_unshift( $shirt_size_options, 
												array(
													'type'		=> 'option',
													'contents'	=> 'Select your size',
													'attrs'		=> array(
														'value'	=> '',
														'selected'	=> true,
														'disabled'	=> true
													)
												)
											);
										}
									?>

							</div>
						</div>
					</div>
				</div>



				<div class="row">
					<div class="col-md-4">
						<h3>Notifications</h3>
					</div>
					<div class="col-md-8">
						<div class="card shadow-sm">
							<div class="card-body">
									
								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_id'		=> '_omh_notifications_general',
											'input_type'	=> 'checkbox',
											'label'			=> 'General News & Updates',
											'form_text'		=> 'News, updates, new features, and promotions - we promise not to bother you often!',
											'required'		=> false,
											'value'			=> $omh_user_notifications_general,
										)
									);
								if ($has_house){
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_id'		=> '_omh_notifications_house_products',
											'input_type'	=> 'checkbox',
											'label'			=> 'Product / Campaign Launches for ' . $chapter_name,
											'form_text'		=> 'Get notified when your House launches a new retail product or campaign!',
											'required'		=> false,
											'value'			=> $omh_user_notifications_house_products
										)
									);
								}
								if ($is_admin){
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_id'		=> '_omh_notifications_house_admin',
											'input_type'	=> 'checkbox',
											'label'			=> 'House Admin Announcements',
											'form_text'		=> 'Critical updates and new features specifically for House Admins',
											'required'		=> false,
											'value'			=> $omh_user_notifications_house_admin
										)
									);
								}

								?>

								</form>
							</div>
						</div>
					</div>
				</div>

				</form>
				<div class="row">
					<div class="col">
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col text-right">
						<?php  
							echo OMH_HTML_UI_Button::factory(
								array(
									'color'		=> 'primary',
									'value'		=> 'submit',
									'form'		=> 'mh-screen-form',
									'label'		=> 'Save',
									'class' 	=> 'mh-form-submit'
								)
							);
						?>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
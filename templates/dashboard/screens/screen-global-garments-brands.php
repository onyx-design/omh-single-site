<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$allowed_roles = false;

$header_tabs = array(
	'garment_products' 	=> array(
		'name'			=> 'Products',
		'destination'	=> OMH()->mh_admin_url( 'global-garments/products' )
	),
	'garment_styles'	=> array(
		'name'			=> 'Styles',
		'destination'	=> OMH()->mh_admin_url( 'global-garments/styles' )
	),
	'garment_brands' 	=> array(
		'name'			=> 'Brands',
		'active'		=> true,
		'destination'	=> OMH()->mh_admin_url( 'global-garments/brands' )
	),
	'garment_colors' 	=> array(
		'name'			=> 'Colors',
		'destination'	=> OMH()->mh_admin_url( 'global-garments/colors' )
	),
);

// Add Global Garment Button in Dashboard Header
ob_start();
include_once( OMH()->dashboard_template_path( 'dashboard-header-global-garments' ) );
$omh_header_extra_content = ob_get_clean();

// Add Global Garment Modal in Dashboard Footer
ob_start();
include_once( OMH()->dashboard_template_path( 'dashboard-footer-global-garments' ) );
$omh_footer_extra_content = ob_get_clean();

$enable_load_data = true;
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row">
			
			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">

						<?php
    						// Add Modal Button to Global Garment Pages
							// echo OMH_HTML_UI_Button::factory(
					  //   		array(
					  //   			'color'		=> 'primary',
					  //   			'size'		=> 'small',
					  //   			'label'		=> 'Add Garment Brand',
					  //   			'attrs'		=> array(
					  //   				'data-toggle'	=> 'modal',
					  //   				'data-target'	=> '#global-garment-modal'
					  //   			)
					  //   		)
					  //   	)->render();

							echo OMH_Table::factory(
								array(
									'type'	=> 'Garment_Brands',
								)
							)->get_ui_table();
						?>

					</div>
				</div>
			</div>

			<!-- Save Global Garment Brand Modal -->
			<div class="modal fade mh-modal" id="garment-brand-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="garment-brand-modal-title">Save Garment Brand</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form id="mh-form-save-garment-brand" class="mh-form form-not-initialized needs-validation" data-ajax-action="save_garment_brand" data-form-nonce="<?php echo wp_create_nonce( 'save-garment-brand' ); ?>" data-omh-form-model="omh_garment_brands">

								<div class="row">
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'brand_slug',
												'label'			=> 'Brand Slug',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'brand_code',
												'label'			=> 'Brand Code',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
								</div>
								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'hidden',
											'input_id'		=> 'ID',
											'value'			=> ''
										)
									);
								?>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-save-garment-brand',
										'label'		=> 'Save',
										'class' 	=> 'mh-forms-submit'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>
			<?php
				include( OMH()->dashboard_template_path( 'dashboard-footer' ) );
			?>

		</div>
	</div>
</div>
<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
</script>
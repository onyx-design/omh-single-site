<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

$allowed_roles = false;

$omh_page_title = 'Chapter Manager';

$header_tabs = array(
	'chapters'	=> array(
		'name'			=> 'Chapters',
		'destination'	=> OMH()->mh_admin_url( 'chapters' ),
		'active'		=> true,
	),
	'organizations' 	=> array(
		'name'			=> 'Organizations',
		'destination'	=> OMH()->mh_admin_url( 'organizations' ),
	),
	'colleges' 	=> array(
		'name'			=> 'Colleges',
		'destination'	=> OMH()->mh_admin_url( 'colleges' ),
	),
);

$enable_load_data = true;
$enable_ajax_search = true;
?>

<div id="mh-dashboard-wrap" class="mh-dashboard-screen-form">
	<div class="container">
		<div id="mh-dashboard" class="row">
			
			<div class="col">
				<?php
					include( OMH()->dashboard_template_path( 'dashboard-header' ) );
				?>

				<div class="card shadow-sm">
					<div class="card-body">

						<?php
							echo OMH_Table::factory(
								array(
									'type'	=> 'Chapters',
								)
							)->get_ui_table();
						?>

					</div>
				</div>
			</div>

			<!-- Add Chapter Modal -->
			<div class="modal fade mh-modal" id="chapter-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="chapter-modal-title">Save Chapter</h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<form id="mh-form-save-chapter" class="mh-form form-not-initialized needs-validation row pb-0" data-ajax-action="save_chapter" data-form-nonce="<?php echo wp_create_nonce( 'save-chapter' ); ?>" data-omh-form-model="omh_chapters">

									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Select2::factory(
											array(
												'input_id'		=> 'college_id',
												'label'			=> 'College',
												'value'			=> '',
												'placeholder'	=> 'Select a College',
												'search_model'	=> 'colleges'
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Select2::factory(
											array(
												'input_id'		=> 'org_id',
												'label'			=> 'Organization',
												'value'			=> '',
												'placeholder'	=> 'Select an Organization',
												'search_model'	=> 'organizations'
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'chapter_name',
												'label'			=> 'Chapter Name',
												'value'			=> '',
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'url_structure',
												'label'			=> 'URL Structure',
												'value'			=> '',
												'required'		=> true,
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Input::factory(
											array(
												'input_id'		=> 'earnings_percentage',
												'label'			=> 'Earnings Percentage',
												'value'			=> null,
												'append'		=> '<i class="fa fa-percent"></i>',
												'required'		=> false
											)
										);
									?>
									</div>
									<div class="col-md-12">
									<?php
										echo OMH_HTML_UI_Select::factory(
											array(
												'input_id'			=> 'status',
												'label'				=> 'Status',
												'value'				=> 'Leads',
												'select_options'	=> array(
													'Leads'				=> 'Leads',
													'Warm'				=> 'Warm',
													'Suspended'			=> 'Suspended',
													'Signed'			=> 'Signed',
													'Passed'			=> 'Passed'
												)
											)
										);
									?>
									</div>
									<div class="col-md-6">
										<div class="front-image-file omh-field-wrap omh-field--ajax-file">
											<label for="banner_image_id">Banner Image</label>
											<div class="mh-file-upload-box__file">
												<input class="mh-file-upload field--template omh-field-input" type="file" id="banner_image_id" name="banner_image_id" data-omh-field-type="ajaxFile" />
											</div>
											<div class="mh-file-upload-box__uploading">Uploading</div>
											<div class="mh-file-upload-box__success">Done</div>
											<div class="mh-file-upload-box__error">Error</div>
										</div>
									</div>
								<?php
									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'hidden',
											'input_id'		=> 'term_id',
											'value'			=> '',
											'class'			=> 'd-none'
										)
									);

									echo OMH_HTML_UI_Input::factory(
										array(
											'input_type'	=> 'hidden',
											'input_id'		=> 'ID',
											'value'			=> '',
											'class'			=> 'd-none'
										)
									);
								?>
							</form>
						</div>
						<div class="modal-footer">
							<?php
								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'secondary',
										'label'		=> 'Cancel',
										'attrs'		=> array(
											'data-dismiss'		=> 'modal',
										)
									)
								);

								echo OMH_HTML_UI_Button::factory(
									array(
										'color'		=> 'primary',
										'value'		=> 'submit',
										'form'		=> 'mh-form-save-chapter',
										'label'		=> 'Save',
										'class' 	=> 'mh-forms-submit'
									)
								);
							?>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
	var table_security = '<?php echo wp_create_nonce( 'omh-mh-table-request' ); ?>';
</script>
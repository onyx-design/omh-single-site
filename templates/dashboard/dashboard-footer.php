<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$omh_footer_extra_content = $omh_footer_extra_content ?? false;
?>

<div id="mh-dashboard-footer" class="row">
		<?php if ( $omh_footer_extra_content ): ?>
			<div class="mh-footer-extra-content">
				<?php echo $omh_footer_extra_content; ?>
			</div>
		<?php endif; ?>
</div>
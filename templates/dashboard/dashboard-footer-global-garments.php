<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<!-- Add Global Garment Product Modal -->
<div class="modal fade mh-modal" id="global-garment-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="global-garment-modal-title">Add Global Garment Product</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<?php
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'inksoft_product_search_id',
							'label'			=> 'InkSoft Product ID',
							'value'			=> '',
							'required'		=> true,
							'append'		=> array(
								OMH_HTML_UI_Button::factory(
									array(
										'size'	=> 'small',
										'color'	=> 'secondary',
										'class'	=> 'inksoft-product-search-btn',
										'label'	=> 'Search'
									)
								)
							)
						)
					);
				?>
				<form id="mh-form-add-garment-product" class="mh-form form-not-initialized needs-validation" data-ajax-action="add_global_garment_from_inksoft" data-form-nonce="<?php echo wp_create_nonce( 'add-global-garment-from-inksoft' ); ?>">

					<div class="row">
						<div class="col">
							<?php
								echo OMH_HTML_UI_Input::factory(
									array(
										'input_type'	=> 'hidden',
										'input_id'		=> 'inksoft_product_id',
										'value'			=> '',
										'required'		=> true,
										'class'			=> 'd-none'
									)
								);
							?>

							<div class="inksoft-product-search-results-wrap omh-field-wrap"></div>
						</div>
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<?php
					echo OMH_HTML_UI_Button::factory(
						array(
							'color'		=> 'secondary',
							'label'		=> 'Cancel',
							'attrs'		=> array(
								'data-reset-form'	=> 'mh-form-add-garment-product',
								'data-dismiss'		=> 'modal',
							)
						)
					);

					echo OMH_HTML_UI_Button::factory(
						array(
							'color'		=> 'primary',
							'value'		=> 'submit',
							'form'		=> 'mh-form-add-garment-product',
							'label'		=> 'Save',
							'class' 	=> 'mh-forms-submit'
						)
					);
				?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var inksoft_product_security = '<?php echo wp_create_nonce( 'add-global-garment-from-inksoft' ); ?>';

	var omhInksoftProductData = {
			inksoftProductId 	: '',
			inksoftResults 		: {}
		};

	jQuery(document).on('omh_forms_initialized', function() {

		var $ = jQuery;

		var $omhInksoftModalForm = $('#mh-form-add-garment-product').get(0).omhForm,
			$omhInksoftSearchWrap = $('.inksoft-product-search-results-wrap'),
			$ajax_action = 'omh_get_global_garment_from_inksoft',
			$ajax_data = {};

			console.log($('#mh-form-add-garment-product'));

		window.oimf = $omhInksoftModalForm;

		$(document).on( 'click', 'button.inksoft-product-search-btn', function(e) {

			e.preventDefault();

			$ajax_data = {
				inksoft_product_id : $('#inksoft_product_search_id').val()
			}

			omh.ajax(
				$ajax_action,
				$ajax_data,
				garmentProductTemplate,
				inksoft_product_security
			);
		} )
		.on('hide.bs.modal',function(event) {
			$omhInksoftSearchWrap.html('');
		});

		function garmentProductTemplate(response) {

			if( response.status === 'success' ) {
				$('#inksoft_product_id').val(response.data.ID);

				var inksoftProductResultsTemplate = _.template([
							'<div class="omh-field-input omh-field-advanced omh-field-advanced--inksoft-product table--static inksoft-product-search-results-inner-wrap" name="inksoft_style_additions" data-omh-namespace="inksoft_style_additions" data-omh-form="mh-form-add-garment-product" data-omh-field-type="inksoftProductResults">',
								'<div class="omh-table-wrap-inner">',
									'<table class="table omh-table">',
										'<thead class="omh-table-section omh-table-section-headers">',
											'<tr class="inksoft-product-table__template row--template"></tr>',
										'</thead>',
										'<tbody class="omh-table-section omh-table-section-results">',
											'<tr class="inksoft-product-style-table__template row--template"></tr>',
										'</tbody>',
									'</table>',
								'</div>',
							'</div>',
						].join("\n")
					),
					inksoftProductResultsOptions = {
						inksoftProduct : response.data
					}

				$omhInksoftSearchWrap.html(inksoftProductResultsTemplate());

				$('[data-omh-field-type="inksoftProductResults"]').OMHFormField( $omhInksoftModalForm, inksoftProductResultsOptions );
			}
		}
	});
</script>
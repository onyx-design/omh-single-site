<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

$allowed_roles = $allowed_roles ?? true;
$redirect_disallowed_roles = $redirect_disallowed_roles ?? OMH()->dashboard_url( 'account-details' );

if( !is_super_admin()
	&& ( true !== $allowed_roles )
	&& ( 
		( false === $allowed_roles ) || empty( $allowed_roles ) || ( !$omh_user->has_role( $allowed_roles ) ) 
	)
) {
	wp_redirect( $redirect_disallowed_roles );
	exit;
}

$omh_breadcrumbs = $omh_breadcrumbs ?? array();

$omh_page_pretitle = $omh_page_pretitle ?? '';
$omh_page_title = apply_filters( 'omh_page_title', !empty( $omh_page_title ) ? $omh_page_title : get_the_title() );
$omh_page_subtitle = $omh_page_subtitle ?? '';

$omh_page_actions = $omh_page_actions ?? array();

$omh_header_extra_content = $omh_header_extra_content ?? false;

$omh_header_page_description = $omh_header_page_description ?? false;

$header_tabs = $header_tabs ?? false;
$active_tab = $active_tab ?? null;

// $omh_notices = $omh_notices ?? array();
$omh_notices = apply_filters( 'omh_notices', $omh_notices ?? array() );

$omh_back_link = $omh_back_link ?? null;

$enable_load_data = $enable_load_data ?? false;
$enable_ajax_search = $enable_ajax_search ?? false;
?>

<div id="mh-dashboard-header" class="row">
	<div class="col">

		<?php if( $omh_breadcrumbs ): ?>
			<div class="dashboard-breadcrumbs">
				<?php
					$breadcrumbs = OMH_HTML_UI_Breadcrumbs::factory();

					foreach( $omh_breadcrumbs as $omh_breadcrumb ) {

						$breadcrumbs->add_crumb( $omh_breadcrumb );
					}

					echo $breadcrumbs;
				?>
			</div>
		<?php endif; ?>

		<div class="mh-header-title">

			<div class="container">
				<div class="row">
					<div class="col-12">
						<button class="btn onyx-drawer__toggle dashboard-drawer-toggle d-inline-block d-lg-none d-xl-none position-absolute" type="button" data-drawer-toggle="dashboard-menu" aria-controls="mh-dashboard-menu" aria-label="Toggle navigation">
						    <i class="fa fa-bars"></i>
						</button>

						<h1 class="omh-page-title"><?php echo $omh_page_title; ?></h1>
					</div>
				</div>
			</div>
			
		</div>

		<div class="mh-header-page-actions">
			<?php foreach( $omh_page_actions as $omh_page_action => $omh_page_action_args ) {

					$page_action_defaults = array(
						'label'		=> '',
						'url'		=> '',
						'new_tab'	=> false,
						'icon'		=> false,
						'class'		=> '',
					);

					$omh_page_action_args = wp_parse_args( $omh_page_action_args, $page_action_defaults );
			?>
				<a href="<?php echo $omh_page_action_args['url']; ?>" <?php echo $omh_page_action_args['new_tab'] ? 'target="_blank"' : ''; ?> class="mh-header-page-action mh-header-page-action--<?php echo str_replace( array( ' ', '-' ), '_', $omh_page_action ); ?> <?php echo $omh_page_action_args['class']; ?>">
					<?php if( $page_action_icon = $omh_page_action_args['icon'] ): ?>
						<i class="fa fa-<?php echo $page_action_icon; ?>"></i>
					<?php endif; ?>
					<?php echo $omh_page_action_args['label']; ?>
				</a>
			<?php } ?>
		</div>

		<?php 
			if ( $omh_header_extra_content ) {
				echo '<div class="mh-header-extra-content">';
				echo $omh_header_extra_content;
				echo '</div>';
			}
		?>

		<?php
			if( $omh_header_page_description ) {
				echo '<div class="mh-header-page-description">';
				echo $omh_header_page_description;
				echo '</div>';
			}
		?>

		<?php 
			// Add Load Data Nonce
			if( $enable_load_data ) {
				wp_nonce_field( 'omh_load_data_nonce', 'omh_load_data_nonce' );
			}

			// Add Load Data Nonce
			if( $enable_ajax_search ) {
				wp_nonce_field( 'omh_ajax_search_nonce', 'omh_ajax_search_nonce' );
			}

			// dev:improve dev:todo Build in permissions
			if( $header_tabs ) {
				echo OMH_HTML_UI_Nav_Tabs::factory(
					array(
						'tabs'			=> $header_tabs,
						'active_tab'	=> $active_tab,
						'class'			=> 'nav nav-tabs mt-3'
					)
				);
			}
			else {
				echo '<hr>';
			}
		?>

		<?php 
			if ( $omh_back_link && isset( $omh_back_link['href'] ) && isset( $omh_back_link['label'] ) ) {
				echo '<div class="mh-header__back-link-wrap mb-3">';
				echo OMH_HTML_Tag::factory(
					array(
						'type' => 'a',
						'class' => 'mh-header__back-link',
						'attrs' => array(
							'href' => $omh_back_link['href']
						),
						'contents' => array(
							'label' => $omh_back_link['label']
						)
					)
				);
				echo '</div>';
			}
		?>

		<div id="mh-dashboard-notices">
			<?php
				foreach( $omh_notices as $omh_notice ) {

					echo OMH_HTML_UI_Alert::factory( $omh_notice );
				}

				wc_print_notices();
			?>
		</div>
	</div>
</div>
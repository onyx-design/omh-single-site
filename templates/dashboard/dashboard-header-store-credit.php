<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$chapter = OMH_Session::get_chapter();
?>

<div class="omh-store-credit-total text-right">
	<small class="text-muted">
		Your Store Credit Balance
	</small>
	<h2 class="text-primary font-weight-bold dashboard-store-credit-balance">
		<?php echo wc_price( OMH()->credit_factory->get_total_credit( $chapter ? $chapter->get_id() : 0, false ) ); ?>
	</h2>
</div>

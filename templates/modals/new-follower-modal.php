<?php 

// $omh_notices = $omh_notices ?? array();
$omh_notices = apply_filters( 'omh_notices', $omh_notices ?? array() );


$term = get_queried_object();
$following_type = false;
$following_object = null;
$modal_title = '';

if( 'chapters' === $term->taxonomy ) {
	$following_type = 'chapter';
	$following_object = OMH()->chapter_factory->get_by_term_slug( $term->slug );
	$modal_title = $following_object->get_chapter_banner_name();
}
else if( 'organizations' === $term->taxonomy ) {
	$following_type = 'organization';
	$following_object = OMH()->organization_factory->get_by_term_slug( $term->slug );
	$modal_title = $following_object->get_org_name();
}
?>

<div class="modal fade mh-modal" id="new-follower-modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="user-modal-title">Follow <?php echo $modal_title; ?></h4>
				
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			<p>Enter your email to stay up to date on new designs, campaigns, discounts and more from <?php echo $modal_title; ?>!</p>
				<form id="mh-new-follower-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="new_omh_follower" data-form-nonce="<?php echo wp_create_nonce('new-omh-follower'); ?>">
					<?php
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_type'	=> 'email',
							'input_id'		=> 'follower_email',
							'label'			=> false,
							'placeholder' 	=> 'Email Address',
							// 'class'			=> array(
							// 	'signup-form-group',
							// ),
							'required'		=> true
						)
					);

					echo OMH_HTML_UI_Input::factory(
						array(
							'input_type'	=> 'hidden',
							'input_id'		=> 'following_id',
							'label'			=> false,
							'value'			=> $following_object->get_id(),
							'attrs'			=> array(
								'readonly'		=> true
							),
							// 'class'			=> array(
							// 	'signup-form-group',
							// ),
							'required'		=> true
						)
					);
				
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_type'	=> 'hidden',
							'input_id'		=> 'following_type',
							'label'			=> false,
							'value'			=> $following_type,
							'attrs'			=> array(
								'readonly'		=> true
							),
							// 'class'			=> array(
							// 	'signup-form-group',
							// ),
							'required'		=> true
						)
					);

					?>
				</form>
			</div>
			<div id="new-follower-modal-notices">
				<?php
				foreach ($omh_notices as $omh_notice) {

					echo OMH_HTML_UI_Alert::factory($omh_notice);
				}

				wc_print_notices();
				?>
			</div>
			<div class="modal-footer">
				<?php

				echo OMH_HTML_UI_Button::factory(
					array(
						'color'		=> 'primary',
						'value'		=> 'submit',
						'form'		=> 'mh-new-follower-form',
						'class'		=> array(
							'new-follower-submit',
							'mh-forms-submit'
						),
						'label'		=> 'Submit',
					)
				);
				?>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
jQuery('#new-follower-modal-toggle-btn').click(function() {
	if( !$(this).is(':disabled') && !$(this).hasClass('disabled') ) {
		$(this).addClass('cta-loading');
	}
})

jQuery('form#mh-new-follower-form').on('omh_form_before_submit', function() {
	var $followBtn = jQuery('#new-follower-modal-toggle-btn');

	if( !$followBtn.is(':disabled') && !$followBtn.hasClass('disabled') ) {
		$followBtn.addClass('cta-loading');
	}
})
jQuery(document).on('omh_new_omh_follower', function(event, responseData) {

	var $followBtn = jQuery('#new-follower-modal-toggle-btn');

	$followBtn.removeClass('cta-loading');

	if( 'success' == responseData.status ) {
		$followBtn.html('<i class="fa fa-check"></i> Following');
	}
	else {
		$followBtn.addClass('btn-danger').removeClass('btn-primary').html('Error, please refresh and retry.');
	}

	$followBtn.prop('disabled', true).addClass('disabled').removeClass('cta-loading');
});
</script>
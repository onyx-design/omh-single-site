<?php 

// $omh_notices = $omh_notices ?? array();
$omh_notices = apply_filters( 'omh_notices', $omh_notices ?? array() );
?>

<div class="modal fade mh-modal" id="signup-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true" data-preserve-on-form-submit="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="user-modal-title">Signup</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="mh-signup-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="signup_form" data-form-nonce="<?php echo wp_create_nonce('signup-form'); ?>">
					<?php
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'signup_name',
							'label'			=> false,
							'placeholder' 	=> 'Name',
							'class'			=> array(
								'signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_type'	=> 'email',
							'input_id'		=> 'signup_email',
							'label'			=> false,
							'placeholder' 	=> 'Email Address',
							'class'			=> array(
								'signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'signup_phone_number',
							'label'			=> false,
							'placeholder' 	=> 'Phone Number (optional)',
							'class'			=> array(
								'signup-form-group',
							),
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'signup_college',
							'label'			=> false,
							'placeholder' 	=> 'College',
							'class'			=> array(
								'signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'signup_organization',
							'label'			=> false,
							'placeholder' 	=> 'Fraternity/Sorority',
							'class'			=> array(
								'signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'signup_officer_position',
							'label'			=> false,
							'placeholder' 	=> 'Officer Position (optional)',
							'class'			=> array(
								'signup-form-group',
							),
						)
					);
					?>
				</form>
			</div>
			<div id="signup-modal-notices">
				<?php
				foreach ($omh_notices as $omh_notice) {

					echo OMH_HTML_UI_Alert::factory($omh_notice);
				}

				wc_print_notices();
				?>
			</div>
			<div class="modal-footer">
				<?php
				echo OMH_HTML_UI_Button::factory(
					array(
						'color'		=> 'secondary',
						'label'		=> 'Cancel',
						'attrs'		=> array(
							'data-dismiss'		=> 'modal',
						)
					)
				);

				echo OMH_HTML_UI_Button::factory(
					array(
						'color'		=> 'primary',
						'value'		=> 'submit',
						'form'		=> 'mh-signup-form',
						'class'		=> array(
							'signup-submit',
							'mh-forms-submit'
						),
						'label'		=> 'Submit',
					)
				);
				?>
			</div>

		</div>
	</div>
</div>
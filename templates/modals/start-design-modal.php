<?php

// $omh_notices = $omh_notices ?? array();
$omh_notices = apply_filters('omh_notices', $omh_notices ?? array());
?>

<div class="modal fade mh-modal" id="start-design-modal" role="dialog" aria-hidden="true" data-on-close-clear-form="true" data-preserve-on-form-submit="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="user-modal-title">Start Design</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="mh-start-design-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="start_design_form" data-form-nonce="<?php echo wp_create_nonce('start-design-form'); ?>" data-submit-type="form">
					<?php
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'name',
							'label'			=> false,
							'placeholder' 	=> 'Name',
							'class'			=> array(
								'start-design-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_type'	=> 'email',
							'input_id'		=> 'email',
							'label'			=> false,
							'placeholder' 	=> 'Email Address',
							'class'			=> array(
								'start-design-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'phone_number',
							'label'			=> false,
							'placeholder' 	=> 'Phone Number (optional)',
							'class'			=> array(
								'start-design-form-group',
							),
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'chapter',
							'label'			=> false,
							'placeholder' 	=> 'Chapter Name',
							'class'			=> array(
								'start-design-form-group',
							),
							'required'		=> true
						)
					);
					?>
					<label class="omh-form-label">Leave comments for our talented designers. The more detailed, the closer the design will be to your vision!</label>
					<?php
					echo OMH_HTML_UI_Textarea::factory(
						array(
							'input_id'		=> 'description',
							'label'			=> false,
							'contents'		=> array(
								'input'			=> array(
									'attrs'			=> array(
										'rows'		=> 5,
										'cols'		=> 50
									)
								)
							)
						)
					);
					?>
					<div class="col-md-6">

						<?php
						$allowed_media_types = array(
							// JPG or JPEG
							'application/jpeg',
							'image/jpeg',
							'.jpeg',
							'application/jpg',
							'image/jpg',
							'.jpg',

							// PDF
							'application/pdf',
							'.pdf',

							// AI
							'application/illustrator',
							'application/postscript',
							'.ai',

							// PSD
							'application/photoshop',
							'image/vnd.adobe.photoshop',
							'image/x-psd',
							'.psd',

							// PNG
							'image/png',
							'.png',

							// SVG
							'image/svg+xml',
							'.svg',

							// TIFF
							'image/tiff',
							'.tiff',

							// GIF
							'image/gif',
							'.gif',
						);
						?>

						<div class="form-group form-group-file-img">
							<div class="row">
								<div class="col-md-12">
									<div class="custom-file">
										<label class="omh-form-label">Upload Design Images / Inspiration</label>
										<div class="input-group mb-2">
											<div class="custom-file custom-file-hide-default-label">
												<input type="file" data-omh-field-type="file" class="custom-file-input never-input never-changed orig-value omh-field-input" name="file_upload_1" accept="<?php echo implode(', ', $allowed_media_types); ?>" />
												<label class="custom-file-label">Choose New File</label>
											</div>
											<div class="input-group-append">
												<button class="btn btn-danger input-reset-btn" type="button" for="file_upload_1" style="display:none;"><i class="fa fa-close"></i></button>
												<button class="btn btn-secondary input-focus-btn" type="button" for="file_upload_1">Browse</button>
											</div>
										</div>

										<div class="input-group mb-2">
											<div class="custom-file custom-file-hide-default-label">
												<input type="file" data-omh-field-type="file" class="custom-file-input omh-field-input never-input never-changed orig-value" name="file_upload_2" accept="<?php echo implode(', ', $allowed_media_types); ?>" />
												<label class="custom-file-label">Choose New File</label>
											</div>
											<div class="input-group-append">
												<button class="btn btn-danger input-reset-btn" type="button" for="file_upload_2" style="display:none;"><i class="fa fa-close"></i></button>
												<button class="btn btn-secondary input-focus-btn" type="button" for="file_upload_2">Browse</button>
											</div>
										</div>
									</div>
									<small class="form-text text-muted">You can upload .jpg .jpeg .psd .ai .pdf .png .gif or .tiff files.</small>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div id="start-design-notice">
				<?php
				foreach ($omh_notices as $omh_notice) {

					echo OMH_HTML_UI_Alert::factory($omh_notice);
				}

				wc_print_notices();
				?>
			</div>
			<div class="modal-footer">
				<?php
				echo OMH_HTML_UI_Button::factory(
					array(
						'color'		=> 'secondary',
						'label'		=> 'Cancel',
						'attrs'		=> array(
							'data-dismiss'		=> 'modal',
						)
					)
				);

				echo OMH_HTML_UI_Button::factory(
					array(
						'color'		=> 'primary',
						'value'		=> 'submit',
						'form'		=> 'mh-start-design-form',
						'class'		=> array(
							'start-design-submit',
							'mh-forms-submit'
						),
						'label'		=> 'Submit',
					)
				);
				?>
			</div>
		</div>
	</div>
</div>
</div>
<?php 
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Install {

	public static function init() {
		add_action( 'init', array( __CLASS__, 'check_version' ), 10 );
		add_action( 'omh_updated', array( __CLASS__, 'on_update' ) );
	}

	public static function check_version() {
		if( version_compare( get_option( 'omh_version' ), OMH()->version, '<' ) ) {
			self::install();
			do_action( 'omh_updated' );
		}
	}

	/**
	 * Functions to run on update
	 */
	public static function on_update() {

		flush_rewrite_rules();
	}

	public static function install() {

		if( 'yes' === get_transient( 'omh_installing' ) ) {
			return;
		}

		set_transient( 'omh_installing', 'yes', MINUTE_IN_SECONDS * 10 );
		OMH()->maybe_define_constant( 'OMH_INSTALLING', true );

		self::create_tables();
		self::create_roles();
		self::setup_environment();
		self::create_terms();
		self::create_attributes();
		self::maybe_migrate();
		self::update_omh_version();

		delete_transient( 'omh_installing' );

		do_action( 'omh_installed' );
	}

	public static function create_tables() {
		global $wpdb;

		$wpdb->hide_errors();

		require_once ABSPATH . 'wp-admin/includes/upgrade.php';

		foreach( self::get_schema() as $table_schema ) {
			$wpdb->query( $table_schema );
		}
	}

	public static function get_schema() {
		global $wpdb;

		$collate = $wpdb->has_cap( 'collation' ) ? $wpdb->get_charset_collate() : '';

		$tables = array(
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_credit (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					type VARCHAR(64) NOT NULL DEFAULT '',
					amount INT SIGNED NOT NULL DEFAULT 0,
					order_number INT DEFAULT NULL,
					order_id BIGINT UNSIGNED DEFAULT NULL,
					line_item_id BIGINT UNSIGNED DEFAULT NULL,
					variant_id BIGINT UNSIGNED DEFAULT NULL,
					product_id BIGINT UNSIGNED DEFAULT NULL,
					notes VARCHAR(256),
					ledger_date DATETIME DEFAULT NULL,
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			",
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_chapters (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					org_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
					college_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
					term_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
					chapter VARCHAR(256) NOT NULL DEFAULT '',
					url_structure VARCHAR(200) NOT NULL DEFAULT '',
					earnings_percentage INT UNSIGNED DEFAULT NULL,
					status VARCHAR(32) NOT NULL DEFAULT '',
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			",
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_colleges (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					term_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
					college_name VARCHAR(256) NOT NULL DEFAULT '',
					college_shortname VARCHAR(128) NOT NULL DEFAULT '',
					college_letters VARCHAR(256) NOT NULL DEFAULT '',
					city VARCHAR(256) NOT NULL DEFAULT '',
					state VARCHAR(64) NOT NULL DEFAULT '',
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			",
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_organizations (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					term_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
					org_name VARCHAR(256) NOT NULL DEFAULT '',
					org_shortname VARCHAR(128) NOT NULL DEFAULT '',
					org_letters VARCHAR(128) NOT NULL DEFAULT '',
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			",
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_temp_uploads (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					file_name VARCHAR(256) NOT NULL DEFAULT '',
					file_path VARCHAR(2056) NOT NULL DEFAULT '',
					file_url VARCHAR(2056) NOT NULL DEFAULT '',
					thumbnail_path VARCHAR(2056) NOT NULL DEFAULT '',
					thumbnail_url VARCHAR(2056) NOT NULL DEFAULT '',
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			",
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_garment_blanks (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					product_id INT UNSIGNED NOT NULL DEFAULT 0,
					product_style_id INT UNSIGNED NOT NULL DEFAULT 0,
					name VARCHAR(256) NOT NULL DEFAULT '',
					slug VARCHAR(28) NOT NULL UNIQUE DEFAULT '',
					inksoft_sku VARCHAR(64) NOT NULL DEFAULT '',
					manufacturer_sku VARCHAR(64) NOT NULL DEFAULT '',
					manufacturer VARCHAR(64) NOT NULL DEFAULT '',
					long_description TEXT,
					color VARCHAR(64) NOT NULL DEFAULT '',
					sizes VARCHAR(64) NOT NULL DEFAULT '',
					base_cost INT NOT NULL DEFAULT 0,
					inksoft_image_url VARCHAR(2056) NOT NULL DEFAULT '',
					image_url VARCHAR(2056) NOT NULL DEFAULT '',
					thumbnail_url VARCHAR(2056) NOT NULL DEFAULT '',
					selected_product LONGTEXT,
					selected_product_style LONGTEXT,
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			",
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_garment_brands (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					brand_slug VARCHAR(2048) NOT NULL DEFAULT '',
					brand_code VARCHAR(2048) NOT NULL DEFAULT '',
					inksoft_brand_id INT UNSIGNED NOT NULL DEFAULT 0,
					brand_name VARCHAR(256) NOT NULL DEFAULT '',
					brand_image_url VARCHAR(2048) NOT NULL DEFAULT '',
					size_chart_url VARCHAR(2048) NOT NULL DEFAULT '',
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			",
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_garment_products (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					product_slug VARCHAR(2048) NOT NULL DEFAULT '',
					inksoft_product_id INT UNSIGNED NOT NULL DEFAULT 0,
					inksoft_sku VARCHAR(256) NOT NULL DEFAULT '',
					garment_brand_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
					manufacturer_sku VARCHAR(256) NOT NULL DEFAULT '',
					product_name VARCHAR(256) NOT NULL DEFAULT '',
					product_long_description TEXT,
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			",
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_garment_styles (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					style_slug VARCHAR(2048) NOT NULL DEFAULT '',
					inksoft_style_id INT UNSIGNED NOT NULL DEFAULT 0,
					garment_product_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
					garment_color_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
					sizes VARCHAR(256) NOT NULL DEFAULT '',
					style_base_cost VARCHAR(256) NOT NULL DEFAULT '',
					inksoft_image_url VARCHAR(2048) NOT NULL DEFAULT '',
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			",
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_garment_colors (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					color_code VARCHAR(8) UNIQUE DEFAULT '',
					color_name VARCHAR(128) UNIQUE DEFAULT '',
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			",
			"
				CREATE TABLE IF NOT EXISTS {$wpdb->prefix}omh_followers (
					ID BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					follower_email VARCHAR(256) NOT NULL DEFAULT '',
					following_id BIGINT UNSIGNED NOT NULL DEFAULT 0,
					following_type VARCHAR(64) NOT NULL DEFAULT '',
					meta_created DATETIME DEFAULT CURRENT_TIMESTAMP,
					meta_updated DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
					PRIMARY KEY  (ID)
				) $collate;
			"
		);

		return $tables;
	}

	public static function create_roles() {
		global $wp_roles;

		if( !class_exists( 'WP_Roles' ) ) {
			return;
		}

		if( !isset( $wp_roles ) ) {
			$wp_roles = new WP_Roles();
		}

		$house_admin_role = add_role(
			'house_admin',
			'House Admin',
			array(
				'read'			=> true,
				'admin_house'	=> true
			)
		);

		// dev:activate
		// $house_member_role = add_role(
		// 	'house_member',
		// 	'House Member',
		// 	array(
		// 		'read'			=> true
		// 	)
		// );

		$capabilities = self::get_core_capabilities();

		foreach( $capabilities as $capability_group ) {
			foreach( $capability_group as $capability ) {
				$wp_roles->add_cap( 'administrator', $capability );
			}
		}
	}

	private static function get_core_capabilities() {

		$capabilities = array();

		$capability_types = array(
			'chapters',
			'colleges',
			'organizations',
		);

		foreach( $capability_types as $capability_type ) {

			$capabilities[ $capability_type ] = array(
				// Post type.
				"edit_{$capability_type}",
				"read_{$capability_type}",
				"delete_{$capability_type}",
				"edit_{$capability_type}s",
				"edit_others_{$capability_type}s",
				"publish_{$capability_type}s",
				"read_private_{$capability_type}s",
				"delete_{$capability_type}s",
				"delete_private_{$capability_type}s",
				"delete_published_{$capability_type}s",
				"delete_others_{$capability_type}s",
				"edit_private_{$capability_type}s",
				"edit_published_{$capability_type}s",

				// Terms.
				"manage_{$capability_type}_terms",
				"edit_{$capability_type}_terms",
				"delete_{$capability_type}_terms",
				"assign_{$capability_type}_terms",
			);
		}

		return $capabilities;
	}

	private static function setup_environment() {
		OMH_Taxonomies::register_taxonomies();

		flush_rewrite_rules();
	}

	public static function create_terms() {
		
		$taxonomies = array(
			'sales_type'		=> array(
				'retail',
				'bulk',
				'campaign',
			),
			/**
			 * @tag Product Status
			 */
			'product_status'	=> array(
				'design_in_review',

				// Retail
				'ready_to_list',
				'published',
				'private',
				'unpublished',
				'archived',
				
				// Bulk
				'ready_to_order',
				'order_in_progress',
				'paid',
				'complete'
			)
		);

		foreach( $taxonomies as $taxonomy => $terms ) {

			foreach( $terms as $term ) {

				if( !get_term_by( 'name', $term, $taxonomy ) ) {
					wp_insert_term( $term, $taxonomy );
				}
			}
		}
	}

	public static function create_attributes() {

		// This can only run if WooCommerce is active
		if( !class_exists( 'WooCommerce' ) ) {
			return;
		}

		// These are the attributes we want to add/update
		$attributes = array( 
			'pa_size' 		=> array(
				'args'			=> array(
					'name' 			=> 'Size',
					'slug' 			=> 'size',
					'type'			=> 'select',
					'order_by' 		=> 'menu_order',
					'has_archives' 	=> 0
				),
				'children'		=> array(
					'size-xs'		=> array(
						'name'			=> 'XS',
						'slug'			=> 'size-xs',
						'order'			=> 1
					),
					'size-s'		=> array(
						'name'			=> 'S',
						'slug'			=> 'size-s',
						'order'			=> 2
					),
					'size-m'		=> array(
						'name'			=> 'M',
						'slug'			=> 'size-m',
						'order'			=> 3
					),
					'size-l'		=> array(
						'name'			=> 'L',
						'slug'			=> 'size-l',
						'order'			=> 4
					),
					'size-xl'		=> array(
						'name'			=> 'XL',
						'slug'			=> 'size-xl',
						'order'			=> 5
					),
					'size-2xl'		=> array(
						'name'			=> '2XL',
						'slug'			=> 'size-2xl',
						'order'			=> 6
					),
					'size-3xl'		=> array(
						'name'			=> '3XL',
						'slug'			=> 'size-3XL',
						'order'			=> 7
					),
					'size-4xl'		=> array(
						'name'			=> '4XL',
						'slug'			=> 'size-4XL',
						'order' 		=> 8
					),
					'size-5xl'		=> array(
						'name'			=> '5XL',
						'slug'			=> 'size-5XL',
						'order'			=> 9
					)
				)
			),
			'pa_garment'	=> array(
				'args'			=> array(
					'name'			=> 'Garment',
					'slug'			=> 'garment',
					'type'			=> 'select',
					'order_by'		=> 'menu_order',
					'has_archives'	=> 0
				)
			)
		);

		// Get the existing attributes on the site
		$current_attributes = array_column( wc_get_attribute_taxonomies(), 'attribute_name' );

		foreach( $attributes as $attribute_name => $attribute_options ) {

			$attribute_args = $attribute_options['args'];

			// If the attribute doesn't exist, we need to create it
			if( !in_array( $attribute_args['slug'], $current_attributes ) && !wc_attribute_taxonomy_id_by_name( $attribute_args['slug'] ) ) {
				$attribute = wc_create_attribute( $attribute_args );
			// Otherwise we just want to get the attribute
			} else {
				$attribute = wc_attribute_taxonomy_id_by_name( $attribute_args['slug'] );
			}

			// If there wasn't an error during the create/read attribute process, and there are children to add, let's do that
			if( !is_wp_error( $attribute ) 
				&& ( isset( $attribute ) && $attribute ) 
				&& isset( $attribute_options['children'] ) 
			) {

				foreach( $attribute_options['children'] as $term_name => $term_args ) {

					// Check to see if the child exists. If it doesn't, we need to add it
					$attribute_term = omh_get_or_create_wc_attribute_term( $attribute_name, $term_args['name'], $term_args['slug'] );

					if( $attribute_term
						&& isset( $term_args['order'] )
						&& $term_args['order'] 
					) {
						update_term_meta( $attribute_term->term_id, 'order_' . $attribute_name, $term_args['order'] );
					}

					// If there wasn't any error during the create/read child process, set the order of the children as this is used for the front and back ends
					if( !is_wp_error( $attribute_term ) ) {
						if( isset( $term_args['order'] ) && $term_args['order'] ) {
							update_term_meta( $attribute_term->term_id, 'order_' . $attribute_name, $term_args['order'] );
						}
					}
				}
			}
		}
	}

	private static function maybe_migrate() {

		$local_omh_version = get_option( 'omh_version', false );
		$new_omh_version = OMH()->version;

		// Add term_id to garments
		if( version_compare( $local_omh_version, '1.0.7', '<' ) ) {
			self::add_col( "{$wpdb->prefix}omh_garment_styles", 'term_id', "BIGINT UNSIGNED UNIQUE", 'ID' );
		}

		if( version_compare( $local_omh_version, '1.0.8', '<' ) ) {
			self::rename_col( "{$wpdb->prefix}omh_chapters", 'chapter', 'chapter_name', 'VARCHAR(256)' );
		}

		// Remove trailing decache
		if( version_compare( $local_omh_version, '1.0.9', '<' ) ) {
			global $wpdb;

			$sql = "
				UPDATE `{$wpdb->prefix}omh_garment_styles`
				SET `inksoft_image_url` = REPLACE( `inksoft_image_url`, '?decache=', '' )
				WHERE `inksoft_image_url` LIKE '%?decache=';
			";

			$wpdb->query( $sql );
		}

		// Rebuild Style Colors
		if( version_compare( $local_omh_version, '1.0.10', '<' ) ) {
			global $wpdb;

			$styles = $wpdb->get_results( "SELECT * FROM `{$wpdb->prefix}omh_garment_styles`" );

			foreach( $styles as $style ) {

				$style_slug = $style->style_slug;
				$style_color = ucwords( str_replace( '-', '%', array_pop( explode( '_', $style_slug ) ) ) );

				$color_id = $wpdb->get_var( "SELECT `ID` FROM `{$wpdb->prefix}omh_garment_colors` WHERE `color_name` LIKE '{$style_color}'" );

				$wpdb->update( "{$wpdb->prefix}omh_garment_styles", array( 'garment_color_id' => $color_id ), array( 'ID' => $style->ID ) );
			}
		}

		if( version_compare( $local_omh_version, '1.0.11', '<' ) ) {
			global $wpdb;

			self::add_col( "{$wpdb->prefix}omh_garment_styles", 'style_full_name', "VARCHAR(512) NOT NULL DEFAULT ''", 'term_id' );

			$sql = "
				UPDATE `{$wpdb->prefix}omh_garment_styles` style
				LEFT JOIN `{$wpdb->prefix}omh_garment_products` product
					ON style.`garment_product_id` = product.`ID`
				LEFT JOIN `{$wpdb->prefix}omh_garment_brands` brand
					ON product.`garment_brand_id` = brand.`ID`
				LEFT JOIN `{$wpdb->prefix}omh_garment_colors` color
					ON style.`garment_color_id` = color.`ID`
				SET style.`style_full_name` = CONCAT( brand.`brand_name`, ' - ', product.`product_name`, ' - ', color.`color_name` );
			";

			$wpdb->query( $sql );
		}

		if( version_compare( $local_omh_version, '1.0.12', '<' ) ) {
			global $wpdb;

			$taxonomy_change = array(
				'chapter'		=> 'chapters',
				'college'		=> 'colleges',
				'organization'	=> 'organizations'
			);

			foreach( $taxonomy_change as $old_taxonomy => $new_taxonomy ) {
				
				$sql = "
					UPDATE `{$wpdb->prefix}term_taxonomy`
					SET `taxonomy` = '{$new_taxonomy}'
					WHERE `taxonomy` = '{$old_taxonomy}'
				";

				$wpdb->query( $sql );
			}
		}

		if( version_compare( $local_omh_version, '1.0.13', '<' ) ) {
			self::add_col( "{$wpdb->prefix}omh_credit", 'chapter_id', "BIGINT UNSIGNED", 'order_id' );
		}

		if( version_compare( $local_omh_version, '1.0.14', '<' ) ) {
			self::drop_col( "{$wpdb->prefx}omh_credit", 'order_number' );
			self::drop_col( "{$wpdb->prefx}omh_credit", 'line_item_id' );
			self::drop_col( "{$wpdb->prefx}omh_credit", 'variant_id' );
			self::drop_col( "{$wpdb->prefx}omh_credit", 'product_id' );
		}

		if( version_compare( $local_omh_version, '1.0.15', '<' ) ) {
			OMH_Model_Garment_Style::build_style_full_names();
		}

		if( version_compare( $local_omh_version, '1.0.16', '<' ) ) {
			// Made changes to the permalinks for both Products and Chapters
			flush_rewrite_rules();
		}

		if( version_compare( $local_omh_version, '1.0.18', '<' ) ) {
			global $wpdb;

			// Start Sequential Order Numbers at 1000
			$sql = "
				INSERT INTO {$wpdb->postmeta} (post_id, meta_key, meta_value)
				VALUES ( 1, '_order_number', 1000 );
			";

			$wpdb->query( $sql );
		}

		if( version_compare( $local_omh_version, '1.0.21', '<' ) ) {
			global $wpdb;

			$sql = "
				ALTER TABLE `{$wpdb->prefix}omh_garment_colors` 
					ADD `color_hex_1` VARCHAR(20) NOT NULL DEFAULT '' AFTER `color_name`,
					ADD `color_hex_2` VARCHAR(20) NOT NULL DEFAULT '' AFTER `color_hex_1`
			";

			$wpdb->query( $sql );
		}

		if( version_compare( $local_omh_version, '1.0.22', '<' ) ) {
			OMH_Model_Garment_Style::build_style_full_names();
		}

		if( version_compare( $local_omh_version, '1.0.24', '<' ) ) {
			global $wpdb;

			$sql = "
				ALTER TABLE `{$wpdb->prefix}omh_chapters` 
					ADD `banner_image_id` INT UNSIGNED NOT NULL DEFAULT 0 AFTER `term_id`
			";
			$wpdb->query( $sql );

			$sql = "
				ALTER TABLE `{$wpdb->prefix}omh_colleges` 
					ADD `banner_image_id` INT UNSIGNED NOT NULL DEFAULT 0 AFTER `term_id`,
					ADD `logo_image_id` INT UNSIGNED NOT NULL DEFAULT 0 AFTER `banner_image_id`
			";
			$wpdb->query( $sql );

			$sql = "
				ALTER TABLE `{$wpdb->prefix}omh_organizations` 
					ADD `banner_image_id` INT UNSIGNED NOT NULL DEFAULT 0 AFTER `term_id`,
					ADD `logo_image_id` INT UNSIGNED NOT NULL DEFAULT 0 AFTER `banner_image_id`
			";
			$wpdb->query( $sql );
		}

		// There was an issue with our old migration that created many many indices that were causing issues when saving rows, remove them all
		if( version_compare( $local_omh_version, '1.0.25', '<' ) ) {
			global $wpdb;

			$sql = "
				ALTER TABLE wp_omh_organizations 
					DROP INDEX term_id,
					DROP INDEX term_id_2,
					DROP INDEX term_id_3,
					DROP INDEX term_id_4,
					DROP INDEX term_id_5,
					DROP INDEX term_id_6,
					DROP INDEX term_id_7,
					DROP INDEX term_id_8,
					DROP INDEX term_id_9,
					DROP INDEX term_id_10,
					DROP INDEX term_id_11,
					DROP INDEX term_id_12,
					DROP INDEX term_id_13,
					DROP INDEX term_id_14,
					DROP INDEX term_id_15,
					DROP INDEX term_id_16,
					DROP INDEX term_id_17,
					DROP INDEX term_id_18,
					DROP INDEX term_id_19,
					DROP INDEX term_id_20,
					DROP INDEX term_id_21,
					DROP INDEX term_id_22,
					DROP INDEX term_id_23,
					DROP INDEX term_id_24,
					DROP INDEX term_id_25,
					DROP INDEX term_id_26,
					DROP INDEX term_id_27,
					DROP INDEX term_id_28
			";
			$wpdb->query( $sql );

			$sql = "
				ALTER TABLE wp_omh_colleges 
					DROP INDEX term_id,
					DROP INDEX term_id_2,
					DROP INDEX term_id_3,
					DROP INDEX term_id_4,
					DROP INDEX term_id_5,
					DROP INDEX term_id_6,
					DROP INDEX term_id_7,
					DROP INDEX term_id_8,
					DROP INDEX term_id_9,
					DROP INDEX term_id_10,
					DROP INDEX term_id_11,
					DROP INDEX term_id_12,
					DROP INDEX term_id_13,
					DROP INDEX term_id_14,
					DROP INDEX term_id_15,
					DROP INDEX term_id_16,
					DROP INDEX term_id_17,
					DROP INDEX term_id_18,
					DROP INDEX term_id_19,
					DROP INDEX term_id_20,
					DROP INDEX term_id_21,
					DROP INDEX term_id_22,
					DROP INDEX term_id_23,
					DROP INDEX term_id_24,
					DROP INDEX term_id_25,
					DROP INDEX term_id_26,
					DROP INDEX term_id_27,
					DROP INDEX term_id_28
			";
			$wpdb->query( $sql );

			$sql = "
				ALTER TABLE wp_omh_chapters 
					DROP INDEX term_id,
					DROP INDEX term_id_2,
					DROP INDEX term_id_3,
					DROP INDEX term_id_4,
					DROP INDEX term_id_5,
					DROP INDEX term_id_6,
					DROP INDEX term_id_7,
					DROP INDEX term_id_8,
					DROP INDEX term_id_9,
					DROP INDEX term_id_10,
					DROP INDEX term_id_11,
					DROP INDEX term_id_12,
					DROP INDEX term_id_13,
					DROP INDEX term_id_14,
					DROP INDEX term_id_15,
					DROP INDEX term_id_16,
					DROP INDEX term_id_17,
					DROP INDEX term_id_18,
					DROP INDEX term_id_19,
					DROP INDEX term_id_20,
					DROP INDEX term_id_21,
					DROP INDEX term_id_22,
					DROP INDEX term_id_23,
					DROP INDEX term_id_24,
					DROP INDEX term_id_25,
					DROP INDEX term_id_26,
					DROP INDEX term_id_27,
					DROP INDEX term_id_28
			";
			$wpdb->query( $sql );
		}

		if( version_compare( $local_omh_version, '1.0.26', '<' ) ) {
			global $wpdb;

			$sql = "
				UPDATE `{$wpdb->prefix}omh_garment_styles`
				SET `inksoft_image_url` = REPLACE( `inksoft_image_url`, '?decache=', '' )
			";

			$wpdb->query( $sql );
		}
		
		if( version_compare( $local_omh_version, '1.0.28', '<' ) ) {
			global $wpdb;

			$sql = "
				ALTER TABLE `{$wpdb->prefix}omh_garment_products` 
					ADD `material` VARCHAR(256) NOT NULL DEFAULT '' AFTER `product_long_description`
			";
			$wpdb->query( $sql );

			$material_import = array(
				array(
					'id'	=> 115,
					'material'	=> '100% Polyester'
				),
				array(
					'id'	=> 114,
					'material'	=> '100% Polyester'
				),
				array(
					'id'	=> 113,
					'material'	=> '100% Dri-FIT Polyester Twill'
				),
				array(
					'id'	=> 112,
					'material'	=> '100% Bio-Washed Chino Twill Cotton'
				),
				array(
					'id'	=> 111,
					'material'	=> '80/20 Acrylic/Wool'
				),
				array(
					'id'	=> 110,
					'material'	=> '100% Cotton Twill'
				),
				array(
					'id'	=> 109,
					'material'	=> '100% Bio-Washed Chino Twill Cotton'
				),
				array(
					'id'	=> 108,
					'material'	=> '100% Bio-Washed Chino Twill Cotton'
				),
				array(
					'id'	=> 107,
					'material'	=> '100% Polyester'
				),
				array(
					'id'	=> 106,
					'material'	=> '100% Combed Ringspun Cotton'
				),
				array(
					'id'	=> 105,
					'material'	=> '50/46/4 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 104,
					'material'	=> '50/38/12 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 103,
					'material'	=> '100% Polyester Interlock'
				),
				array(
					'id'	=> 102,
					'material'	=> '100% Polyester'
				),
				array(
					'id'	=> 101,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 100,
					'material'	=> '50/25/25 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 99,
					'material'	=> '50/25/25 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 98,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 97,
					'material'	=> '65/35 Polyester/Viscose'
				),
				array(
					'id'	=> 96,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 95,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 94,
					'material'	=> '50/37.5/12.5 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 93,
					'material'	=> '65/35 Polyester/Viscose'
				),
				array(
					'id'	=> 92,
					'material'	=> '50/25/25 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 91,
					'material'	=> '100% Cotton'
				),
				array(
					'id'	=> 90,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 89,
					'material'	=> '60/40 Ringspun Cotton/Polyester'
				),
				array(
					'id'	=> 88,
					'material'	=> '60/40 Ringspun Cotton/Polyester'
				),
				array(
					'id'	=> 87,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 86,
					'material'	=> '65/35 Viscose Polyester/Cotton'
				),
				array(
					'id'	=> 85,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 84,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 83,
					'material'	=> '65/35 Polyester/Viscose'
				),
				array(
					'id'	=> 82,
					'material'	=> '65/35 Polyester/Viscose'
				),
				array(
					'id'	=> 81,
					'material'	=> '65/35 Polyester/Viscose'
				),
				array(
					'id'	=> 80,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 79,
					'material'	=> '50/37.5/12.5 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 78,
					'material'	=> '50/50 Polyester/Combed Cotton'
				),
				array(
					'id'	=> 77,
					'material'	=> '80/20 Cotton/Polyester '
				),
				array(
					'id'	=> 76,
					'material'	=> '70/30 Cotton/Polyester '
				),
				array(
					'id'	=> 75,
					'material'	=> '80/20 Cotton/Polyester'
				),
				array(
					'id'	=> 74,
					'material'	=> '80/20 Cotton/Polyester'
				),
				array(
					'id'	=> 73,
					'material'	=> '50/38/12 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 72,
					'material'	=> '100% Polyester'
				),
				array(
					'id'	=> 71,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 70,
					'material'	=> '80/20 Ringspun Cotton/Polyester'
				),
				array(
					'id'	=> 69,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 68,
					'material'	=> '50/46/4 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 67,
					'material'	=> '90/10 Cotton/Polyester'
				),
				array(
					'id'	=> 66,
					'material'	=> '52/48 Cotton/Polyester'
				),
				array(
					'id'	=> 65,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 64,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 63,
					'material'	=> '80/20 Ringspun Cotton/Polyester'
				),
				array(
					'id'	=> 62,
					'material'	=> '52/48 Cotton/Polyester'
				),
				array(
					'id'	=> 61,
					'material'	=> '50/46/4 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 60,
					'material'	=> '50/46/4 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 59,
					'material'	=> '50/50 Cotton/Polyester Jersey'
				),
				array(
					'id'	=> 58,
					'material'	=> '35/65 Ringspun Cotton/Polyester'
				),
				array(
					'id'	=> 57,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 56,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 55,
					'material'	=> '50/50 Ringspun Cotton/Polyester'
				),
				array(
					'id'	=> 54,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 53,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 52,
					'material'	=> '50/25/25 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 51,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 50,
					'material'	=> '60/40 Cotton/Polyester'
				),
				array(
					'id'	=> 49,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 48,
					'material'	=> '60/40 Cotton/Polyester '
				),
				array(
					'id'	=> 47,
					'material'	=> '100% Polyester'
				),
				array(
					'id'	=> 46,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 45,
					'material'	=> '100% Cotton'
				),
				array(
					'id'	=> 44,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 43,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 42,
					'material'	=> '50/25/25 Polyester/Ringspun Cotton/Rayon'
				),
				array(
					'id'	=> 41,
					'material'	=> '50/25/25 Polyester/Ringspun Cotton/Rayon'
				),
				array(
					'id'	=> 40,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 39,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 38,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 37,
					'material'	=> '50/25/25 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 36,
					'material'	=> '100% Polyester Mesh'
				),
				array(
					'id'	=> 35,
					'material'	=> '100% Polyester Wicking Knit'
				),
				array(
					'id'	=> 34,
					'material'	=> '100% Polyester Wicking Knit'
				),
				array(
					'id'	=> 33,
					'material'	=> '50/50 Ringspun Cotton/Polyester'
				),
				array(
					'id'	=> 32,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 31,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 30,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 29,
					'material'	=> '50/38/12 Polyester/Cotton/Rayon'
				),
				array(
					'id'	=> 28,
					'material'	=> '52/48 Ringspun Cotton/Polyester'
				),
				array(
					'id'	=> 27,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 26,
					'material'	=> '65/35 Polyester/Viscose'
				),
				array(
					'id'	=> 25,
					'material'	=> '65/35 Polyester/Viscose'
				),
				array(
					'id'	=> 24,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 23,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 22,
					'material'	=> '35/65 Ringspun Cotton / Polyester'
				),
				array(
					'id'	=> 21,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 20,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 19,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 18,
					'material'	=> '100% Cotton'
				),
				array(
					'id'	=> 17,
					'material'	=> '52/48 Ringspun Cotton/Polyester'
				),
				array(
					'id'	=> 16,
					'material'	=> '52/48 Ringspun Cotton/Polyester'
				),
				array(
					'id'	=> 15,
					'material'	=> '100% Cotton'
				),
				array(
					'id'	=> 14,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 13,
					'material'	=> '100% Cotton'
				),
				array(
					'id'	=> 12,
					'material'	=> '100% Cotton'
				),
				array(
					'id'	=> 11,
					'material'	=> '100% Cotton'
				),
				array(
					'id'	=> 10,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 9,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 8,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 7,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 6,
					'material'	=> '100% Ringspun Cotton'
				),
				array(
					'id'	=> 5,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 4,
					'material'	=> '100% Cotton'
				),
				array(
					'id'	=> 3,
					'material'	=> '100% Cotton'
				),
				array(
					'id'	=> 2,
					'material'	=> '50/50 Cotton/Polyester'
				),
				array(
					'id'	=> 1,
					'material'	=> '100% Cotton'
				),
			);

			foreach( $material_import as $material_data ) {

				if( isset( $material_data['id'] ) ) {
					
					if( $garment_product = OMH()->garment_product_factory->read( $material_data['id'] ) ) {

						$garment_product->set_material( $material_data['material'] );
						$garment_product->save();
					}
				}
			}
		}

		if( version_compare( $local_omh_version, '1.1.5', '<' ) ) {
			global $wpdb;

			self::add_col( "{$wpdb->prefix}omh_garment_styles", 'enabled', "TINYINT UNSIGNED NOT NULL DEFAULT 0", 'inksoft_image_url' );

			$update_sql = "
				UPDATE `{$wpdb->prefix}omh_garment_styles`
					SET `enabled` = 1
			";
			$wpdb->query( $update_sql );
		}

		if( version_compare( $local_omh_version, '1.1.6', '<' ) ) {
			global $wpdb;

			self::add_col( "{$wpdb->prefix}omh_organizations", 'org_greek_letters', "VARCHAR(6) NOT NULL DEFAULT ''", 'org_letters' );
		}
	}

	/**
	 * Return if a $col_name exists for $table_name in the WP Database
	 * 
	 * @param 	String	$table_name
	 * @param 	String	$col_name
	 * 
	 * @return 	int|null
	 */
	public static function col_exists( $table_name, $col_name ) {

		$col_exists = null;

		if( $table_name ) {
			global $wpdb;

			$table_name = ( 0 !== strpos( $table_name, $wpdb->prefix ) ) ? $wpdb->prefix . $table_name : $table_name;

			$sql = "
				SELECT COUNT(*) 
				FROM INFORMATION_SCHEMA.COLUMNS
				WHERE `table_name` = '{$table_name}'
				AND `table_schema` = '" . DB_NAME . "'
				AND `column_name` = '{$col_name}';
			";

			$col_exists = $wpdb->get_var( $sql );
		}

		return $col_exists;
	}

	public static function add_col( $table_name, $col_name, $col_definition, $after_col = null ) {

		if( $table_name && !self::col_exists( $table_name, $col_name ) ) {
			global $wpdb;

			$table_name = ( 0 !== strpos( $table_name, $wpdb->prefix ) ) ? $wpdb->prefix . $table_name : $table_name;

			$sql = "
				ALTER TABLE `{$table_name}`
				ADD `{$col_name}` {$col_definition}
			";

			if( !empty( $after_col ) ) {
				$sql .= " AFTER `{$after_col}`";
			}

			$sql .= ";";

			return $wpdb->query( $sql );
		}
	}

	public static function update_col( $table_name, $col_name, $col_definition, $after_col = null ) {

		if( $table_name && self::col_exists( $table_name, $col_name ) ) {
			global $wpdb;

			$table_name = ( 0 !== strpos( $table_name, $wpdb->prefix ) ) ? $wpdb->prefix . $table_name : $table_name;

			$sql = "
				ALTER TABLE `{$table_name}`
				MODIFY `{$col_name}` {$col_definition}
			";

			if( !empty( $after_col ) ) {
				$sql .= " AFTER `{$after_col}`";
			}

			$sql .= ";";

			return $wpdb->query( $sql );
		}
	}

	public static function rename_col( $table_name, $old_col_name, $new_col_name, $data_type ) {

		if( $table_name && self::col_exists( $table_name, $old_col_name ) ) {
			global $wpdb;

			$table_name = ( 0 !== strpos( $table_name, $wpdb->prefix ) ) ? $wpdb->prefix . $table_name : $table_name;

			$sql = "
				ALTER TABLE `{$table_name}`
				CHANGE `{$old_col_name}` `{$new_col_name}` {$data_type}
			";

			return $wpdb->query( $sql );
		}
	}

	public static function drop_col( $table_name, $col_name ) {

		if( $table_name && self::col_exists( $table_name, $col_name ) ) {
			global $wpdb;

			$table_name = ( 0 !== strpos( $table_name, $wpdb->prefix ) ) ? $wpdb->prefix . $table_name : $table_name;

			$sql = "
				ALTER TABLE `{$table_name}`
				DROP COLUMN `{$col_name}`
			";

			return $wpdb->query( $sql );
		}
	}

	public static function add_table_if_not_exists( $table_name ) {

		if( $table_name ) {
			global $wpdb;

			$table_name = ( 0 !== strpos( $table_name, $wpdb->prefix ) ) ? $wpdb->prefix . $table_name : $table_name;

			$charset_collate = $wpdb->get_charset_collate();

			$sql = "
				CREATE TABLE IF NOT EXISTS IF NOT EXISTS `{$table_name}` (
					`ID` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
					PRIMARY KEY  (`ID`)
				) $charset_collate;
			";

			return $wpdb->query( $sql );
		}
	}

	private static function update_omh_version() {
		update_option( 'omh_version', OMH()->version );
	}
}
OMH_Install::init();
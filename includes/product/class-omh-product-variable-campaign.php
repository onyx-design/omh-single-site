<?php
defined( 'ABSPATH' ) || exit;

class OMH_Product_Variable_Campaign extends OMH_Product_Variable {

	/**
	 * @tag Product Status
	 */
	protected static $default_product_status = array(
		'label'					=> 'Status Unset',
		'color'					=> 'secondary',
		'tooltip'				=> false,
		'step'					=> 0,
		'house_admin_can_set'	=> false,
		'house_admin_can_edit'	=> false,
		'visibility' 			=> false,
		'purchasability'		=> false,
		'house_admin_can_set'	=> false
	);

	/**
	 * @tag Product Status
	 */
	public static $product_statuses = array(
		'design_in_review'	=> array(
			'label'					=> 'Design in Review',
			'color' 				=> 'warning',
			'step'					=> 1,
			'admin_can_set'			=> true,
			'tooltip'				=> 'Your design is still being reviewed. You will be able to list it once the design has been approved, which usually takes 1-2 business days.'
		),
		'live'		=> array(
			'label'					=> 'Live',
			'color'					=> 'primary',
			'step'					=> 2,
			'tooltip'				=> 'Your product is publicly available on your storefront.',
			'house_admin_can_set'	=> true,
			'admin_can_set'			=> true,
			'visibility' 			=> array(
				'link'		=> true,
				'search'	=> true,
				'chapter'	=> true,
				'national'	=> false
			),
			'purchasability'		=> true
		),
		'live_private'			=> array(
			'label'					=> 'Live (Private)',
			'color'					=> 'info',
			'step'					=> 3,
			'tooltip'				=> 'Your product is not visible on your storefront, but it can still be purchased via a directly link.',
			'house_admin_can_set'	=> true,
			'admin_can_set'			=> true,
			'visibility' 			=> array(
				'link'		=> true,
				'search'	=> false,
				'chapter'	=> false,
				'national'	=> false
			),
			'purchasability'		=> true
		),
		'ended'			=> array(
			'label'					=> 'Ended',
			'color'					=> 'info',
			'step'					=> 4,
			'tooltip'				=> 'Your product is not visible on your storefront, but it can still be purchased via a directly link.',
			'house_admin_can_set'	=> false,
			'admin_can_set'			=> true,
			'visibility'			=> array(
				'link'		=> true,
				'search'	=> false,
				'chapter'	=> false,
				'national'	=> false
			),
			'purchasability'		=> false
		),
		'archived' 			=> array(
			'label'					=> 'Archived',
			'step'					=> 5,
			'admin_can_set'			=> true,
			'tooltip'				=> 'This product is no longer available.',
		),
	);

	protected $sales_type = 'campaign';
	
	/**
	 * Sales Type
	 * 
	 * @return 	string
	 */
	public function get_sales_type() {
		return $this->sales_type;
	}

	public function get_campaign_end_date() {

		return $this->get_meta( '_campaign_end_date', true );
	}

	public function set_campaign_end_date( $campaign_end_date ) {

		$this->update_meta_data( '_campaign_end_date', $campaign_end_date );
		$this->save();

		return $this->get_campaign_end_date();
	}

	public function get_campaign_target_sales() {

		return $this->get_meta( '_campaign_target_sales', true );
	}

	public function set_campaign_target_sales( $campaign_target_sales ) {

		$this->update_meta_data( '_campaign_target_sales', $campaign_target_sales );
		$this->save();

		return $this->get_campaign_target_sales();
	}
}
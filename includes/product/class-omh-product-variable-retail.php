<?php
defined( 'ABSPATH' ) || exit;

class OMH_Product_Variable_Retail extends OMH_Product_Variable {
	
	protected $sales_type = 'retail';

	/**
	 * Sales Type
	 * 
	 * @return 	string
	 */
	public function get_sales_type() {
		return $this->sales_type;
	}

	/********************************************
	 * 			  PRICING FUNCTIONS 			*
	 ********************************************/

	public function get_retail_price_html() {

		$featured_garment = $this->get_featured_garment();
		$featured_garment_price = $featured_garment->get_retail_price();

		return wc_price( $featured_garment_price, array( 'decimals' => 2 ) );
	}

	public function get_retail_price() {

		$featured_garment = $this->get_featured_garment();
		$featured_garment_price = $featured_garment->get_retail_price();

		return $featured_garment_price;
	}

	public function get_base_price_html() {

		$featured_garment = $this->get_featured_garment();
		$featured_garment_price = $featured_garment->get_base_price();

		return wc_price( $featured_garment_price, array( 'decimals' => 2 ) );
	}

	public function get_base_price() {

		$featured_garment = $this->get_featured_garment();
		$featured_garment_price = $featured_garment->get_base_price();

		return $featured_garment_price;
	}

	public function set_base_price( $price ) {

		$featured_garment = $this->get_featured_garment();
		$featured_garment->set_retail_price( $price );

		// return wc_price( $featured_garment_price, array( 'decimals' => 2 ) );
	}

	public function set_retail_price( $price ) {

		$featured_garment = $this->get_featured_garment();
		$featured_garment->set_base_price( $price );

		// return wc_price( $featured_garment_price, array( 'decimals' => 2 ) );
	}
}
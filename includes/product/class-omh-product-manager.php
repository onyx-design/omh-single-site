<?php
defined( 'ABSPATH' ) || exit;

/**
 * Describes the actions/hooks/ and general functions
 * around dealing with products
 */
class OMH_Product_Manager {

	public static function init() {

		/**
		 * Override WC Product Variable Class
		 * 
		 * @see WC_Product_Factory::get_product_classname()
		 */
		add_filter( 'woocommerce_product_class', array( __CLASS__, 'product_class' ), 10, 4 );

		/**
		 * @see WC_Product::save()
		 */
		add_action( 'woocommerce_before_product_object_save', array( __CLASS__, 'update_sales_type' ), 10, 2 );
		add_action( 'woocommerce_before_product_object_save', array( __CLASS__, 'generate_search_string' ), 10, 2 );

		// Update bulk product status based on associated bulk order status
		add_action( 'woocommerce_order_status_changed', array( __CLASS__, 'sync_bulk_product_order_status' ), 10, 4 );
	}

	/**
	 * @tag Product Status
	 */
	public static function sync_bulk_product_order_status( $order_id, $from_status, $to_status, $order ) {
		// readable name if needed
		$new_status = wc_get_order_status_name($to_status);

		// if it is a bulk order proceed to set product status accordingly
		if (null !== $order->get_bulk_product_id()) {
			$bulk_product_id = $order->get_bulk_product_id();
			$product = wc_get_product( $bulk_product_id );

			if( $product && $product->get_sales_type() === 'bulk' ) {
				$product->set_product_status( $to_status );
			}
		}
	}

	public static function product_class( $classname, $product_type, $post_type, $product_id = null ) {

		$sales_type = OMH_Product_Manager::get_product_sales_type( $product_id );

		switch( $sales_type ) {
			case 'retail':
			case 'bulk':
			case 'campaign':
				$class_suffix = '_' . ucfirst( str_replace( '-', '_', $sales_type ) );
				break;
			default:
				$class_suffix = '';
				break;
		}

		if( 'variable' === $product_type ) {
			return "OMH_Product_Variable{$class_suffix}";
		}
		else if( 'variation' === $product_type ) {
			return "OMH_Product_Variation{$class_suffix}";
		}

		return $classname;
	}

	public static function update_sales_type( $product, $data_store ) {

		if( method_exists( $product, 'get_sales_type') ) {
			$old_type = static::get_product_sales_type( $product->get_id() );
		
			$new_type = $product->get_sales_type();

			wp_set_object_terms( $product->get_id(), $new_type, 'sales_type' );

			if( $old_type !== $new_type ) {
				$data_store->updated_props[] = 'sales_type';
				do_action( 'omh_sales_type_changed', $product, $old_type, $new_type );
			}
		}
	}

	/**
	 * Before a product is saved, generate the search string
	 */
	public static function generate_search_string( $product, $data_store ) {
		
		if( method_exists( $product, 'generate_search_string') ) {

			$search_string = $product->generate_search_string();

			$product->set_search_string( $search_string );
		}
	}

	public static function get_product_sales_type( $product_id ) {

		$post_type = get_post_type( $product_id );
		$terms = get_the_terms( $product_id, 'sales_type' );

		if( in_array( $post_type, array( 'product', 'product_variation' ) ) ) {
			return !empty( $terms ) ? sanitize_title( current( $terms )->name ) : false;
		} else {
			return false;
		}
	}

	public static function get_classname_from_sales_type( $sales_type, $product_type = 'variable' ) {

		$sales_types = omh_get_sales_types();

		if( !empty( $sales_types[ $sales_type ] ) ) {
			return $product_type ? 'OMH_Product_' . ucfirst( str_replace( '-', '_', $product_type ) ) . '_' . ucfirst( str_replace( '-', '_', $sales_type ) ) : false;
		}

		return $product_type ? "OMH_Product_{$product_type}" : false;
	}

	public static function get_products( $sales_type = null ) {

		$sales_types = omh_get_sales_types();
		$args = array();

		if( $sales_type && !empty( $sales_types[ $sales_type ] ) ) {
			$args['tax_query'][] = array(
				'taxonomy'	=> 'sales_type',
				'field'		=> 'slug',
				'terms'		=> $sales_type
			);
		}

		return wc_get_products( $args );
	}
}
OMH_Product_Manager::init();
<?php
defined( 'ABSPATH' ) || exit;

class OMH_Product_Variation_Campaign extends OMH_Product_Variation {

	public function get_sales_type() {
		return 'campaign';
	}
}
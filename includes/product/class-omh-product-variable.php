<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Product_Variable extends WC_Product_Variable {

	public $default_price_tiers = array( 
		1,
		12,
		24,
		48,
		73,
		145
	);

	/**
	 * @tag Product Status
	 */
	protected static $default_product_status = array(
		'label'					=> 'Status Unset',
		'color'					=> 'secondary',
		'tooltip'				=> false,
		'step'					=> 0,
		'house_admin_can_set'	=> false,
		'house_admin_can_edit'	=> false,
		'visibility' 			=> false,
		'purchasability'		=> false,
		'house_admin_can_set'	=> false,
		'admin_can_set'			=> true,
	);

	/**
	 * @tag Product Status
	 */
	public static $product_statuses = array(
		'design_in_review'	=> array(
			'label'					=> 'Design in Review',
			'color' 				=> 'warning',
			'step'					=> 1,
			'admin_can_set'			=> true,
			'tooltip'				=> 'Your design is still being reviewed. You will be able to list it once the design has been approved, which usually takes 1-2 business days.'
		),
		'ready_to_list'		=> array(
			'label'					=> 'Ready to List',
			'color'					=> 'success',
			'step'					=> 2,
			'tooltip'				=> 'Your design has been approved! You can now publish the product to your store.',
			'house_admin_can_set'	=> true,
			'house_admin_can_edit'	=> true,
			'admin_can_set'			=> true,
			'visibility' 			=> array(
				'link'		=> false,
				'search'	=> false,
				'chapter'	=> false,
				'national'	=> false
			),
			'purchasability'		=> true
		),
		'published'			=> array(
			'label'					=> 'Published',
			'color'					=> 'primary',
			'step'					=> 3,
			'tooltip'				=> 'Your product is publicly available on your storefront.',
			'house_admin_can_set'	=> true,
			'house_admin_can_edit'	=> true,
			'admin_can_set'			=> true,
			'visibility' 			=> array(
				'link'		=> true,
				'search'	=> true,
				'chapter'	=> true,
				'national'	=> true
			),
			'purchasability'		=> true
		),
		'private'			=> array(
			'label'					=> 'Private',
			'color'					=> 'info',
			'step'					=> 4,
			'tooltip'				=> 'Your product is not visible on your storefront, but it can still be purchased via a directly link.',
			'house_admin_can_set'	=> true,
			'house_admin_can_edit'	=> true,
			'admin_can_set'			=> true,
			'visibility'			=> array(
				'link'		=> true,
				'search'	=> false,
				'chapter'	=> false,
				'national'	=> false
			),
			'purchasability'		=> true
		),
		'unpublished' 		=> array(
			'label'					=> 'Unpublished',
			'house_admin_can_set'	=> true,
			'step'					=> 5,
			'tooltip'				=> 'Your product is currently hidden and unavailable, but you can republish it anytime!',
			'house_admin_can_edit'	=> true,
			'admin_can_set'			=> true,
			'visibility'			=> array(
				'link'		=> true,
				'search'	=> false,
				'chapter'	=> false,
				'national'	=> false
			),
			'purchasability'		=> true
		),
		'archived' 			=> array(
			'label'					=> 'Archived',
			'step'					=> 6,
			'admin_can_set'			=> true,
			'tooltip'				=> 'This product is no longer available.',
		),
	);

	protected $sales_type = false;

	protected $chapter = null;

	protected $organization = null;

	protected $college = null;

	protected $garments = null;

	protected $variations = null;

	/**
	 * Constructor
	 * 
	 * Required to extend WooCommerce
	 * 
	 * @param 	mixed 	$product
	 * 
	 * @return 	bool|void
	 */
	public function __construct( $product = 0 ) {

		try {
			parent::__construct( $product );
		} catch( Exception $e ) {
			return false;
		}
	}

	/********************************************
	 * 			 TAXONOMY FUNCTIONS 			*
	 *											*
	 * 	 These functions apply to our custom 	*
	 * 	  taxonomies - sales type, chapter, 	*
	 * 		 organization, and college 			*
	 ********************************************/

	/**
	 * @tag Sales Type
	 * 
	 * @return 	string
	 */
	public function get_sales_type() {
		return $this->sales_type;
	}

	/**
	 * @tag Sales Type
	 */
	public function set_sales_type( $sales_type ) {

		$sales_types = omh_get_sales_types();

		if( !empty( $sales_types[ $sales_type ] ) ) {
			
			$this->sales_type = $sales_type;

			return true;
		}

		return false;
	}

	/**
	 * BEGIN PRODUCT STATUS SECTION
	 */

	/**
	 * @tag Product Status
	 */
	public function get_product_status( $format = 'slug' ) {

		$terms = get_the_terms( $this->get_id(), 'product_status' );

		$status_slug = !empty( $terms ) ? sanitize_title( current( $terms )->name ) : false;

		if( 'slug' == $format ) {
			return $status_slug;
		}
		
		// Merge with default product status
		$product_status = array_merge( static::$default_product_status, ( static::$product_statuses[ $status_slug ] ?? array() ) );

		// Maybe add status slug to full status array
		if( 'full' == $format ) {
			$product_status['slug'] = isset( static::$product_statuses[ $status_slug ] ) ? $status_slug : false;
		}
		
		return $product_status[ $format ] ?? $product_status;
	}

	/**
	 * @tag Product Status
	 */
	public function set_product_status( $product_status ) {

		$product_statuses = static::$product_statuses;
		
		if( isset( $product_statuses[$product_status] ) ) {
			wp_set_object_terms( $this->get_id(), $product_status, 'product_status' );
		}

		return $this->get_product_status();
	}

	/**
	 * @tag Product Status
	 * @tag Visibiliy
	 */
	public function get_visibility( $context = 'chapter' ) {

		$product_status = $this->get_product_status( 'full' );
		
		if( !empty( $product_status['visibility'] ) 
			&& ( !empty( $product_status['visibility'][ $context ] )
				|| ( 'link' == $context && 'ready_to_list' == $product_status['slug'] && is_super_admin() )
			)
		) {
			return true;
		}

		return false;
	}

	public static function get_product_statuses() {

		$product_statuses = array();

		foreach(static::$product_statuses as $product_status => $status_schema) {
			if(!empty($status_schema['admin_can_set'])) {
				$product_statuses[$product_status] = $status_schema;
			}
		}

		return $product_statuses;
	}

	/**
	 * @tag Product Status
	 * @tag Visibility
	 */
	public static function get_visible_product_statuses( $context ) {
		
		$visible_statuses = array();

		foreach( OMH_Product_Variable::$product_statuses as $product_status => $status_schema ) {
			if( !empty( $status_schema['visibility'][ $context ] ) ) {
				$visible_statuses[] = $product_status;
			}
		}

		return $visible_statuses;
	}

	/**
	 * @tag Product Status
	 * @tag Purchasability
	 */
	public function get_purchasability() {

		$product_status = $this->get_product_status( 'full' );
		
		if( !empty( $product_status['purchasability'] )
		) {
			return true;
		}

		return false;
	}

	public function is_product_visible( $context = 'chapter' ) {
		return $this->get_visibility( $context );
	}

	public function is_product_purchasable() {
		return $this->get_purchasability();
	}

	public function is_product_visible_legacy() {

		$visible = false;

		if( $this->is_design_approved_legacy() ) {
			//@tag visibility
			switch( $this->get_store_visibility() ) {
				case 'national':
				case 'public':
					$visible = true;
					break;
				case 'house_admin':
					$user = wp_get_current_user();
					$user_chapter = $user instanceof OMH_User ? $user->get_chapter() : false;
					$product_chapter = $this->get_chapter();
					if( $user->has_role( 'house_admin' ) 
						&& $user_chapter
						&& $product_chapter
						&& $user_chapter->get_id() == $product_chapter->get_id()
					) {
						$visible = true;
						break;
					}
				case 'hidden':
				default:
					break;
			}
			// $visible = $this->get_visibility();
		}

		return $visible;
	}

	// @tag visibility
	public function get_store_visibility() {

		return $this->get_meta( '_visibility_mh', true ) ?: 'hidden';
	}

	// @tag visibility
	public function set_store_visibility( $store_visibility ) {

		$this->update_meta_data( '_visibility_mh', $store_visibility );
		$this->save();

		return $this->get_store_visibility();
	}

	public function is_product_purchasable_legacy() {

		$purchasable = false;

		// if( $this->is_design_approved() ) {
			// @tag purchasability
			switch( $this->get_store_purchasability() ) {
				case 'public':
					$purchasable = true;
					break;
				case 'house_admin':
					$user = wp_get_current_user();
					$user_chapter = $user instanceof OMH_User ? $user->get_chapter() : false;
					$product_chapter = $this->get_chapter();
					if( $user->has_role( 'house_admin' ) 
						&& $user_chapter
						&& $product_chapter
						&& $user_chapter->get_id() == $product_chapter->get_id()
					) {
						$purchasable = true;
						break;
					}
				case 'unavailable':
				default:
					$purchasable = false;
					break;
			}
		// }

		return $purchasable;
	}

	// @tag purchasability
	public function get_store_purchasability() {

		return $this->get_meta( '_purchasability_mh', true );
	}

	// @tag purchasability
	public function set_store_purchasability( $store_purchasability ) {

		$this->update_meta_data( '_purchasability_mh', $store_purchasability );
		$this->save();

		return $this->get_store_purchasability();
	}

	public function get_product_status_badge( $tooltip = true ) {
		$status_options = $this->get_product_status('full');

		// Parse / Determine tooltip
		if( true === $tooltip ) {
			// Set $tooltip to be the default tooltip text 
			// (the UI_Badge accepts String or Array for tooltip)
			$tooltip = $status_options['tooltip'] ?? false;
		}
		else if( 
			is_array( $tooltip ) 
			&& !isset( $tooltip['title'], $tooltip['tooltip'] ) 
		) {
			// Set the default tooltip text if tooltip params were sent without title
			$tooltip['title']	= $status_options['tooltip'] ?? false;
		}

		return OMH_HTML_UI_Badge::factory(
			array(
				'label' 	=> $status_options['label'],
				'color' 	=> $status_options['color'],
				'tooltip'	=> $tooltip
			)
		);
	}

	/**
	 * Get the Product status to be displayed on the frontend
	 * 
	 * @return 	string
	 */
	public function get_product_status_label( $return_slug = false ) {

		$status_map = array(
			'hidden'			=> 'Hidden',
			'design_in_review'	=> 'Design In Review',
			'house_admin'		=> 'Private',
			'public'			=> 'Public',
			'national'			=> 'National'
		);
		// @tag visibility
		$status = $this->get_store_visibility();

		if( $return_slug ) {

			return $status;
		}

		return $status_map[ $status ];
	}

	/**
	 * Return the HTML for Product Status display
	 * 
	 * @return 	string
	 */
	function get_product_status_badge_legacy() {

		$status_colors = array(
			'Hidden' 			=> 'secondary',
			'Design In Review' 	=> 'warning',
			'Private' 			=> 'info',
			'Public' 			=> 'success',
			'National' 			=> 'primary',
		);

		$product_status = $this->get_product_status_label();

		return array(
			'text' => $product_status,
			'color' => $status_colors[$product_status]
		);
	}

	/**
	 * Returns whether the design has been approved by both Merch Houe and the House Admin
	 * 
	 * @return 	bool
	 */
	public function is_design_approved() {
		// @tag visibility
		if( 'design_in_review' !== $this->get_product_status() ) {
			return true;
		}

		if( 'on' == $this->get_meta( '_design_approved_mh', true ) 
			&& 'on' == $this->get_meta( '_design_approved_ha', true ) 
		) {
			return true;
		}

		return false;
	}

	/**
	 * END PRODUCT STATUS SECTION
	 */

	/**
	 * If user can edit this product
	 */
	public function user_can_edit( $user_id = 0 ) {

		$user = empty( $user_id ) ? wp_get_current_user() : get_user_by( 'ID', $user_id );

		return (
			$user->has_role( 'administrator' )
			|| ( $user->has_role( 'house_admin' ) && $this->get_product_status( 'house_admin_can_edit' ) ) 
		);
	}

	/**
	 * If user can delete this product
	 */
	public function user_can_delete( $user_id = 0 ) {

		$user = empty( $user_id ) ? wp_get_current_user() : get_user_by( 'ID', $user_id );

		return( $user->has_role( 'administrator' ) );
	}

	/**
	 * If this product is a retail or campaign product, 
	 * or does not have a sales type, 
	 * it can be converted to a bulk product
	 * 
	 * @return 	bool
	 */
	public function can_convert_to_bulk() {

		$sales_type = $this->get_sales_type();

		return ( $sales_type === false ) || ( $sales_type === 'retail' ) || ( $sales_type === 'campaign' );
	}

	/**
	 * If this product is a bulk or campaign product, 
	 * or does not have a sales type, 
	 * it can be converted to a retail product
	 * 
	 * @return 	bool
	 */
	public function can_convert_to_retail() {

		$sales_type = $this->get_sales_type();

		return ( $sales_type === false ) || ( $sales_type === 'bulk' ) || ( $sales_type === 'campaign' );
	}

	/**
	 * If this product is a bulk or retail product, 
	 * or does not have a sales type, 
	 * it can be converted to a campaign product
	 * 
	 * @return 	bool
	 */
	public function can_convert_to_campaign() {

		$sales_type = $this->get_sales_type();

		return ( $sales_type === false ) || ( $sales_type === 'bulk' ) || ( $sales_type === 'retail' );
	}

	/**
	 * this proceeds to convert the garment product to a bulk sales type
	 * given the condition
	 * 
	 * @return 	bool
	 */
	public function convert_to_bulk() {

		$sales_type = $this->get_sales_type();

		if( $this->can_convert_to_bulk() ) {

			$garments_meta = $this->get_garments_meta();

			if ($sales_type === 'retail' || $sales_type === 'campaign') {

				$product_price = $this->get_price();
				$product_price_tiers = $this->get_product_price_tiers();
				
				$garments_meta = array_map( function( $garment_meta ) use ( $product_price, $product_price_tiers ) {

					$garment_meta['pricing'] = array_fill( 0, count( $product_price_tiers ), $product_price );

					unset( $garment_meta['base_price'] );
					unset( $garment_meta['retail_price'] );
					
					return $garment_meta;
				}, $garments_meta );
			}
			
			$this->set_sales_type( 'bulk' );
			$this->set_garments_meta( $garments_meta );
		}

		$convert_success = true;

		// use try catch to preform save, if there is an issue catch it and set variable to false
		try {
			$this->save();

		} catch (Exception $e) {
			$convert_success = false;
		}

		return $convert_success;
	}

	public function convert_to_retail() {

		$sales_type = $this->get_sales_type();

		if( $this->can_convert_to_retail() ) {

			$garments_meta = $this->get_garments_meta();

			if ( $sales_type === 'bulk' || $sales_type === 'campaign' || $sales_type === false ) {
				
				// When converting from Bulk to Retail, we need to save the prices from the tiers
				// dev:improve Maybe move this to a separate function
				$minimum_order_quantity = $this->minimum_order_quantity_price();

				$garments_meta = array_map( function( $garment_meta ) use ( $minimum_order_quantity ) {

					$garment_meta['base_price'] = $minimum_order_quantity;
					$garment_meta['retail_price'] = $minimum_order_quantity;

					unset( $garment_meta['pricing'] );
					
					return $garment_meta;
				}, $garments_meta );
				
				$this->set_product_price_tiers();
			}
			
			$this->set_sales_type( 'retail' );
			$this->set_garments_meta( $garments_meta );
		}

		$convert_success = true;

		// use try catch to preform save, if there is an issue catch it and set variable to false
		try {
			$this->save();

		} catch (Exception $e) {
			$convert_success = false;
		}

		return $convert_success;
	}

	public function convert_to_campaign() {

		$sales_type = $this->get_sales_type();

		if( $this->can_convert_to_campaign() ) {

			$garments_meta = $this->get_garments_meta();

			if ( $sales_type === 'bulk' || $sales_type === 'retail' || $sales_type === false ) {
				
				// When converting from Bulk to Campaign, we need to save the prices from the tiers
				// dev:improve Maybe move this to a separate function
				$minimum_order_quantity = $this->minimum_order_quantity_price();

				$garments_meta = array_map( function( $garment_meta ) use ( $minimum_order_quantity ) {

					$garment_meta['base_price'] = $minimum_order_quantity;
					$garment_meta['retail_price'] = $minimum_order_quantity;

					unset( $garment_meta['pricing'] );
					
					return $garment_meta;
				}, $garments_meta );
				
				$this->set_product_price_tiers();
			}
			
			$this->set_sales_type( 'campaign' );
			$this->set_garments_meta( $garments_meta );
		}

		$convert_success = true;

		// use try catch to preform save, if there is an issue catch it and set variable to false
		try {
			$this->save();

		} catch (Exception $e) {
			$convert_success = false;
		}

		return $convert_success;
	}

	/**
	 * Get the chapter attached to this product
	 * 
	 * @return OMH_Chapter|false
	 */
	public function get_chapter( $force_refresh = false ) {

		// If $this->chapter isn't set or were force refreshing, update $this->chapter
		if( is_null( $this->chapter ) || $force_refresh ) {

			$chapter_term = get_the_terms( $this->get_id(), 'chapters' );

			if( $chapter_term && !is_wp_error( $chapter_term ) ) {

				$chapter_term = $chapter_term[0];

				$this->chapter = OMH()->chapter_factory->get_by_term_slug( $chapter_term->slug );
			} else {

				$this->chapter = false;
			}
		}

		return $this->chapter;
	}

	/**
	 * Set the chapter attached to this product
	 * This will also set the organization and college terms
	 * 
	 * @param OMH_Model_Chapter|string $chapter 
	 * @return type
	 */
	public function set_chapter( $chapter = '' ) {

		$chapter_term = false;

		if( is_string( $chapter ) && !is_numeric( $chapter ) ) {
			$chapter_term = get_term_by( 'slug', $chapter, 'chapters', OBJECT );
		} elseif( is_numeric( $chapter ) ) {

			if( $chapter = OMH()->chapter_factory->read( $chapter ) ) {

				$chapter_term = $chapter->get_term();
			} else {
				$chapter_term = false;
			}
		} else {
			$chapter_term = $chapter;
		}
		if( $this->set_chapter_term( $chapter_term ) ) {

			// Force refresh the chapter when changing it
			$this->chapter = OMH()->chapter_factory->get_by_term_slug( $chapter_term->slug );

			if( $organization = $this->get_organization( true ) ) {
				$this->set_organization_term( $organization->get_term() );
			}

			if( $college = $this->get_college( true ) ) {
				$this->set_college_term( $college->get_term() );
			}

			return true;
		}

		return false;
	}

	/**
	 * Set the chapter term
	 * 
	 * @param WP_Term|null $term 
	 * 
	 * @return bool
	 */
	public function set_chapter_term( $term = null ) {

		if( empty( $term ) || is_wp_error( $term ) ) {
			return false;
		}

		return !is_wp_error( wp_set_object_terms( $this->get_id(), $term->term_id, 'chapters', false ) );
	}

	/**
	 * Get the organization attached to this product
	 * 
	 * @return OMH_Model_Organization|false
	 */
	public function get_organization( $force_refresh = false ) {

		if( is_null( $this->organization ) || $force_refresh ) {

			$chapter = $this->get_chapter();

			if( $chapter !== false ) {
				$this->organization = $chapter->get_organization();
			} else {

				$this->organization = false;
			}
		}

		return $this->organization;
	}

	/**
	 * Set the organization term
	 * 
	 * @param WP_Term|null $term 
	 * 
	 * @return bool
	 */
	public function set_organization_term( $term = null ) {

		if( empty( $term ) || is_wp_error( $term ) ) {
			return false;
		}

		return !is_wp_error( wp_set_object_terms( $this->get_id(), $term->term_id, 'organizations', false ) );
	}

	/**
	 * Get the college attached to this product
	 * 
	 * @return OMH_Model_College|false
	 */
	public function get_college( $force_refresh  = false ) {

		if( is_null( $this->college ) || $force_refresh ) {

			$chapter = $this->get_chapter();

			if( $chapter !== false ) {
				$this->college = $chapter->get_college();
			} else {

				$this->college = false;
			}
		}

		return $this->college;
	}

	/**
	 * Set the college term
	 * 
	 * @param WP_Term|null $term 
	 * 
	 * @return bool
	 */
	public function set_college_term( $term = null ) {

		if( empty( $term ) || is_wp_error( $term ) ) {
			return false;
		}

		return !is_wp_error( wp_set_object_terms( $this->get_id(), $term->term_id, 'colleges', false ) );
	}

	/********************************************
	 * 			 ATTRIBUTE FUNCTIONS 			*
	 *											*
	 * These functions apply to our custom att- *
	 * ributes - size and garment 				*
	 ********************************************/

	/**
	 * Remove any garment attributes that are currently not being used on the product
	 * 
	 * @return type
	 */
	// public function clean_garment_attributes() {

	// 	return $this->clean_attribute( 'pa_garment' );
	// }

	// /**
	//  * Remove any size attributes that are currently not being used on the producut
	//  * 
	//  * @return type
	//  */
	// public function clean_size_attributes() {

	// 	return $this->clean_attribute( 'pa_size' );
	// }

	// public function clean_attribute( $attribute ) {

	// 	if( 0 !== strpos( $attribute, 'pa_' ) ) {
	// 		$attribute = 'pa_' . $attribute;
	// 	}

	// 	$product = wc_get_product( $this );

	// 	$product_attributes = $product->get_attributes();
	// 	$variation_attributes = $product->get_variation_attributes();

	// 	if( !isset( $variation_attributes[ $attribute ] ) || !isset( $product_attributes[ $attribute ] ) ) {
	// 		return false;
	// 	}

	// 	foreach( $product_attributes[ $attribute ]->get_options() as $attribute_term_id ) {

	// 		$attribute_term = get_term( $attribute_term_id, $attribute );

	// 		if( !in_array( $attribute_term->slug, $variation_attributes[ $attribute ] ) ) {
	// 			$removed_term = wp_remove_object_terms( $product->get_id(), $attribute_term->term_id, $attribute );
	// 		}
	// 	}

	// 	$product->set_attributes( $product_attributes );
		
	// 	return $product->save();
	// }

	/**
	 * Garments
	 **/
	
	/**
	 * Sets the Garment Meta
	 * 
	 * @param 	array 	$garments_meta 
	 * @param 	bool 	$sync_garments
	 * 
	 * @return 	array
	 */
	public function set_garments_meta( $garments_meta = array(), $sync_garments = true ) {

		/**
		 * Validate Garments Meta
		 */
		$featured_garment_style = null;
		foreach( $garments_meta as $garment_style_id => $garment_meta ) {

			$product_garment_classname = $this->get_product_garment_classname();

			$product_garment = new $product_garment_classname( $this, $garment_meta );

			// Make sure this garment's taxonomy term has been created
			$product_garment->get_garment_term_id();
			$product_garment->save_images();

			// Make sure that there is exactly one featured garment
			if( $product_garment->get_featured() ) {

				// Set to false if we already found one
				if( $featured_garment_style ) {
					$product_garment->set_featured( false );
				} else {
					$featured_garment_style = $product_garment->get_garment_style_id();
				}
			}

			$garments_meta[ $garment_style_id ] = $product_garment->to_array();
		}

		// If we didn't find a featured garment, set the first one to featured
		if( !$featured_garment_style ) {

			reset( $garments_meta );
			$first_key = key( $garments_meta );
			$garments_meta[ $first_key ]['featured'] = true;
			$featured_garment_style = $first_key;
		}

		$this->update_meta_data( '_mh_garments',  $garments_meta );
		$this->save();
		$this->garments = null;

		if( $sync_garments ) {

			$this->sync_garments();
		}

		return $this->get_garments_meta();

	}

	public function get_garments_meta( $garment_id = null ) {

		$garment_meta = $this->get_meta( '_mh_garments', true );

		if( $garment_id ) {

			$garment_id = strval( $garment_id );

			return isset( $garment_meta[ $garment_id ] ) ? $garment_meta[ $garment_id ] : false;
		}

		return $garment_meta;
	}

	public function sync_garments() {

		// Determine Garment Attributes
		$attributes = $default_attributes = array(
			'pa_garment'	=> array(),
			'pa_size'		=> array()
		);

		$garment_term_slugs = array();
		$gallery_image_ids	= array();

		foreach( $this->get_garments( true ) as $garment ) {

			/**
			 * Sync the garment
			 * 
			 * This will create garment/size terms and variations
			 * It won't delete variations or remove product terms, this is handled below
			 */
			$garment->sync();

			$attributes['pa_garment'][] 	= $garment->get_garment_term_id();
			$attributes['pa_size'] 	 		= array_merge( $attributes['pa_size'], $garment->get_enabled_sizes() );

			$garment_term_slugs[] = $garment->get_garment_term_slug(); 

			if( $garment->get_featured() ) {

				$enabled_sizes = $garment->get_enabled_sizes( true );

				$this->set_image_id( $garment->get_image_front( true ) );

				// Put featured garment's images at the front of the gallery
				array_unshift( $gallery_image_ids, $garment->get_image_proof( true ) );
				array_unshift( $gallery_image_ids, $garment->get_image_back( true ) );

				// Set default attributes
				$default_attributes['pa_garment'] = $garment->get_garment_term_slug();
				$default_attributes['pa_size'] = reset( $enabled_sizes );
			} else {

				$gallery_image_ids[] = $garment->get_image_front( true );
				$gallery_image_ids[] = $garment->get_image_back( true );
				$gallery_image_ids[] = $garment->get_image_proof( true );
			}

		}

		$this->set_gallery_image_ids( array_filter( $gallery_image_ids ) );

		$attributes['pa_size'] = array_unique( $attributes['pa_size'] );

		// Sync attributes
		$this->omh_set_attributes( $attributes );
		$this->set_default_attributes( $default_attributes );

		// delete variations
		foreach( $this->get_variations( true )  as $variation_id => $variation ) {

			$variation_attributes = $variation->get_attributes();

			if( isset( $variation_attributes['pa_garment'] ) && !in_array( $variation_attributes['pa_garment'], $garment_term_slugs  ) ) {

				$variation->delete();
			}
		}

		self::sync( $this );

		$this->save();
	}

	public function get_variations( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->variations ) ) {

			foreach( $this->get_children() as $child_id ) {

				$this->variations[ $child_id ] = wc_get_product( $child_id );
			}
		}

		return (array) $this->variations;
	}


	public function get_featured_garment( $force_refresh = false ) {

		foreach( $this->get_garments( $force_refresh ) as $garment ) {

			if( $garment->get_featured() ) {
				return $garment;
			}
		}
	}

	public function get_variation_by_attributes( $attributes, $force_refresh = false ) {

		foreach( $this->get_variations( $force_refresh )  as $variation_id => $variation ) {

			$variation_attributes = $variation->get_attributes();

			if( $attributes == $variation_attributes ) {

				return $variation;
			}
		}

		return null;
	}

	/**
	 * Description
	 * @param type|array $garments 
	 * @return type
	 */
	// public function set_garments_old( $garments = array() ) {

	// 	$product_garments = array();

	// 	if( $garments ) {

	// 		foreach( $garments as $garment_style_id => $garment ) {

	// 			$garment_style_id = intval( trim( $garment_style_id, "'" ) );

	// 			$garment['garment_style_id'] = $garment_style_id;

	// 			// if( $this->has_garment_variation( $garment_style_id ) ) {
	// 				$garment['product_id'] = $this->get_id();
	// 			// }

	// 			$product_garment = new OMH_Product_Garment( $garment );

	// 			$exists = $product_garment->exists();
	// 			$create = isset( $garment['create'] ) ? ( $garment['create'] === 'true' ? true : false ) : false;
	// 			$delete = isset( $garment['delete'] ) ? ( $garment['delete'] === 'true' ? true : false ) : false;

	// 			if( empty( array_filter( array_map( 'maybe_str_to_bool', $garment['sizes'] ) ) ) ) {

	// 				$product_garment = $product_garment->delete( $this, $product_garment );
	// 				continue;
	// 			}

	// 			// dev:todo Clean this up, this is just to keep track of the truth table!
	// 			if( $exists ) {

	// 				if( $create ) {

	// 					if( $delete ) {
	// 						$product_garment = $product_garment->delete( $this, $product_garment );
	// 					} else {
	// 						$product_garment = $product_garment->update( $this, $product_garment );
	// 					}
	// 				} else {

	// 					if( $delete ) {
	// 						$product_garment = $product_garment->delete( $this, $product_garment );
	// 					} else {
	// 						$product_garment = $product_garment->update( $this, $product_garment );
	// 					}
	// 				}
	// 			} else {

	// 				if( $create ) {

	// 					if( $delete ) {
	// 						// Force deletion of meta if variation doesn't exist
	// 						$product_garment = $product_garment->delete( $this, $product_garment );
	// 					} else {
	// 						$product_garment = $product_garment->create( $this, $product_garment );
	// 					}
	// 				} else {

	// 					if( $delete ) {
	// 						// Force deletion of meta if variation doesn't exist
	// 						$product_garment = $product_garment->delete( $this, $product_garment );
	// 					} else {
	// 						$product_garment = $product_garment->update( $this, $product_garment );
	// 					}
	// 				}
	// 			}

	// 			if( $frontend_garment_data = $product_garment->get_frontend_garment_data() ) {
	// 				$product_garments[ $garment_style_id ] = $frontend_garment_data;
	// 			}
	// 		}
	// 	}

	// 	// Set garment meta here
	// 	$this->set_garment_meta( $product_garments );
	// 	$this->save();

	// 	OMH()->clear_page_cache( $this->get_id() );

	// 	$this->clean_garment_attributes();
	// 	$this->clean_size_attributes();
	// }

	/**
	 * Get Garments structed for the frontend
	 * 
	 * Array (
	 * 		garment_style_id,
	 * 		image_front,
	 * 		image_back,
	 * 		image_proof.
	 * 		featured,
	 * 		pricing,
	 * 		sizes
	 * )
	 * 
	 * @return type
	 */
	public function get_frontend_product_garments( $include_garment_style = true ) {

		$product_garments = array();

		foreach( $this->get_garments() as $garment_style_id => $garment ) {

			$product_garments[ $garment_style_id ] = $garment->to_array( $include_garment_style );
		}
		
		return $product_garments;
	}

	public function get_product_garment_classname() {

		$sales_type = $this->get_sales_type();

		switch( $sales_type ) {
			case 'retail':
			case 'bulk':
			case 'campaign':
				return 'OMH_Product_Garment_' . ucfirst( str_replace( '-', '_', $sales_type ) );
		}

		return 'OMH_Product_Garment';
	}

	public function get_garments( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->garments ) ) {

			$garment_classname = $this->get_product_garment_classname();

			$garments = array();
			
			foreach( $this->get_garments_meta() as $garment_style_id => $garment_meta ) {

				$garments[ $garment_style_id ] = new $garment_classname( $this, $garment_meta );
			}
			
			$this->garments = $garments;
		}

		return $this->garments;
	}

	public function get_garment_by_term_name( $garment_name = null ) {

		if( $garment_name ) {
			foreach( $this->get_garments() as $garment ) {

				if( $garment_name == $garment->get_garment_term_name() ) {
					return $garment;
				}
			}
		}

		return null;
	}

	public function get_garment_by_term_slug( $garment_slug = null ) {

		if( $garment_slug ) {
			foreach( $this->get_garments() as $garment ) {

				if( $garment_slug == $garment->get_garment_term_slug() ) {
					return $garment;
				}
			}
		}
		
		return null;
	}

	// /** //dev:remove //dev:garments
	//  * Set the Garment meta for this product
	//  * 
	//  * @param 	array 	$garment_meta
	//  * 
	//  * @return 	array
	//  */
	// public function set_garment_meta( $garment_meta ) {

	// 	$this->update_meta_data( '_mh_garments', $garment_meta );

	// 	return $this->get_garments_meta();
	// }

	/**
	 * Get the sizes of the garments attached to this Product Variable
	 * 
	 * @return 	array
	 */
	public function get_variation_garment_sizes() {

		$variation_garment_sizes = array();

		foreach( $this->get_children() as $child_id ) {

			// Get the variation
			$variation = wc_get_product( $child_id );
			// Get the attributes attached to the variation
			$variation_attributes = $variation->get_variation_attributes();

			// If the variation has a garment AND a size, add it to the array(garment) => size
			if( isset( $variation_attributes['attribute_pa_garment'], $variation_attributes['attribute_pa_size'] ) ) {
				$variation_garment_sizes[ $variation_attributes['attribute_pa_garment'] ][] = $variation_attributes['attribute_pa_size'];
			}
		}

		return $variation_garment_sizes;
	}

	/**
	 * Get all variations with this garment style
	 * 
	 * @param type $garment_style_id 
	 * @return type
	 */
	public function get_garment_variations( $garment_style_id ) {

		$garment_style = OMH()->garment_style_factory->read( $garment_style_id );
		$garment_variations = array();

		foreach( $this->get_children() as $child_id ) {

			$variation = wc_get_product( $child_id );

			if( $variation->has_attribute( $garment_style->get_style_slug() ) ) {
				$garment_variations[] = $child_id;
			}
		}

		return $garment_variations;
	}

	public function has_garment_variation( $garment_style_id ) {

		return $this->get_garments_meta( $garment_style_id ) ? true : false;
	}

	public function has_garment( $garment_name, $garment_slug = null ) {

		return $this->has_attribute_term( 'pa_garment', $garment_name, $garment_slug );
	}

	/**
	 * Add garment to a product
	 * 
	 * dev:todo When adding a garment, we need to get the garment from our OMH Table 
	 * and make sure to add any sizes that exist on the garment, but not on the product
	 * 
	 * Do we need to remove sizes?
	 * 
	 * @param 	string 		$garment_name 
	 * @param 	string|null $garment_slug
	 * 
	 * @return 	bool
	 */
	public function add_garment( $garment_name, $garment_slug = null ) {

		if( !$this->has_garment( $garment_name, $garment_slug ) ) {

			return $this->add_attribute_term( 'pa_garment', $garment_name, $garment_slug );
		}

		return true;
	}

	/**
	 * Sizes
	 **/

	public function has_size( $size_name, $size_slug = null ) {

		return $this->has_attribute_term( 'pa_size', $size_name, $size_slug );
	}

	/**
	 * Add size attribute to this product if it doesn't exist
	 * 
	 * @param type $size 
	 * @return type
	 */
	public function add_size( $size_name, $size_slug = null ) {

		if( !$this->has_size( $size_name, $size_slug ) ) {

			return $this->add_attribute_term( 'pa_size', $size_name, $size_slug );
		}

		return true;
	}

	/**
	 * Attributes
	 **/

	/**
	 * Get the Attribute, or false if it isn't on this product
	 * 
	 * @param type $attribute_slug 
	 * @return type
	 */

	protected function omh_set_attributes( $set_attributes ) {

		$wc_attributes = array();
		
		foreach( $set_attributes as $attribute_slug => $attribute_options ) {

			if( 0 !== strpos( $attribute_slug, 'pa_' ) ) {
				$attribute_slug = 'pa_' . $attribute_slug;
			}

			$attribute_position = 'pa_garment' == $attribute_slug ? 0 : 1;

			$attribute = new WC_Product_Attribute();
			$attribute->set_id( wc_attribute_taxonomy_id_by_name( $attribute_slug ) );
			$attribute->set_name( $attribute_slug );
			$attribute->set_options( $attribute_options );
			$attribute->set_position( $attribute_position );
			$attribute->set_visible( true );
			$attribute->set_variation( true );

			$attributes[ $attribute_slug ] = $attribute;
		}

		$this->set_attributes( $attributes );
		static::sync( $this );
		$this->save();

		return $attributes;
	}
	public function get_attached_attribute( $attribute_slug ) {

		$product_attributes = $this->get_attributes();

		return isset( $product_attributes[ $attribute_slug ] ) ? $product_attributes[ $attribute_slug ] : false;
	}

	public function add_attached_attribute( $attribute_slug, $term_name, $term_slug ) {

		if( 0 !== strpos( $attribute_slug, 'pa_' ) ) {
			$attribute_slug = 'pa_' . $attribute_slug;
		}

		if( $attribute = $this->get_attached_attribute( $attribute_slug ) ) {
			return $attribute;
		}

		$attribute_options = array();
		if( $attribute_term = omh_get_or_create_wc_attribute_term( $attribute_slug, $term_name, $term_slug ) ) {
			$attribute_options[] = $attribute_term->term_id;
		}

		$attribute = new WC_Product_Attribute();
		$attribute->set_id( wc_attribute_taxonomy_id_by_name( $attribute_slug ) );
		$attribute->set_name( $attribute_slug );
		$attribute->set_options( $attribute_options );
		$attribute->set_position( 0 );
		$attribute->set_visible( true );
		$attribute->set_variation( true );

		$attributes = $this->get_attributes();
		$attributes[ $attribute_slug ] = $attribute;

		$this->set_attributes( $attributes );
		$this->save();

		return $attribute;
	}

	public function has_attribute_term( $attribute_slug, $term_name, $term_slug = null ) {

		if( $attribute = $this->add_attached_attribute( $attribute_slug, $term_name, $term_slug ) ) {

			$term = omh_get_or_create_wc_attribute_term( $attribute->get_taxonomy(), $term_name, $term_slug );
			$existing_attributes = wp_get_post_terms( $this->get_id(), $attribute_slug, array( 'fields' => 'ids' ) );

			if( $term 
				&& in_array( $term->term_id, $attribute->get_options() ) 
				&& in_array( $term->term_id, $existing_attributes )
			) {
				return true;
			}
		}

		return false;
	}

	public function add_attribute_term( $attribute_slug, $term_name, $term_slug = null ) {

		$term = omh_get_or_create_wc_attribute_term( $attribute_slug, $term_name, $term_slug );

		if( $term && isset( $term->term_id ) ) {

			$term_taxonomy_ids = wp_set_object_terms( $this->get_id(), $term->slug, $attribute_slug, true );

			if( !is_wp_error( $term_taxonomy_ids ) ) {
				return true;
			}
		}

		return false;
	}

	/********************************************
	 * 			  PRICING FUNCTIONS 			*
	 ********************************************/

	/**
	 * Get the quantity of this product (and children) in the cart
	 * 
	 * @return 	int
	 */
	public function get_cart_quantity() {

		$quantity = 0;
		foreach( WC()->cart->get_cart_contents() as $cart_item ) {

			if( $this->get_id() == $cart_item['product_id'] ) {

				$quantity += $cart_item['quantity'];
			}
		}

		return $quantity;
	}

	/**
	 * Get the price tiers for this product, or use the default tiers
	 * 
	 * @return 	array
	 */
	public function get_product_price_tiers( $raw = false ) {

		$product_price_tiers = $this->get_meta( '_mh_price_tiers', true ) ?: $this->default_price_tiers;

		if( $raw ) {

			return implode( ',', (array) $product_price_tiers );
		} else {
			$product_price_tiers = is_string( $product_price_tiers ) ? explode( ',', $product_price_tiers ) : array_values( $product_price_tiers );

			return array_map( 'intval', $product_price_tiers );
		}
	}

	/**
	 * Set the Product Price Tier array
	 * 
	 * @param 	array 	$product_price_tiers
	 * 
	 * @return 	array|string
	 */
	public function set_product_price_tiers( $product_price_tiers = array() ) {

		$product_price_tiers = implode( ',', (array) $product_price_tiers );

		$this->update_meta_data( '_mh_price_tiers', $product_price_tiers );
		$this->save();

		return $this->get_product_price_tiers();
	}

	/**
	 * Get the price tier based on quantity
	 * 
	 * @param 	int 	$quantity
	 * 
	 * @return 	int
	 */
	public function get_product_price_tier( $quantity ) {

		$quantity = absint( $quantity );
		$product_price_tiers = $this->get_product_price_tiers();

		// If there is no quantity (or is negative), return the lowest price tier
		if( !$quantity ) {
			return reset( $product_price_tiers );
		}

		$price_tier = false;
		foreach( $product_price_tiers as $product_price_tier => $product_price_tier_qty ) {

			if( $quantity >= $product_price_tier_qty ) {

				$price_tier = $product_price_tier;
			}
			else if( $quantity < $product_price_tier_qty ) {
				break;
			}
		}

		return $price_tier;
	}

	/**
	 * Return the first product price tier price
	 * 
	 * @return 	int
	 */
	public function minimum_order_quantity_price() {

		$garments_meta = $this->get_garments_meta();

		return !empty( array_column( $garments_meta, 'pricing' ) ) ? min( array_column( array_column( $garments_meta, 'pricing' ), 0 ) ) : false;
	}

	/**
	 * Return the first product price tier quantity
	 * 
	 * @return 	int
	 */
	public function minimum_order_quantity() {

		$product_tiers = $this->get_product_price_tiers();
		// Return first tier qty
		return reset( $product_tiers );
	}

	/**
	 * Returns if we haven't hit the minimum order quantity or not
	 * 
	 * @return 	bool
	 */
	public function requires_minimum_order_quantity( $quantity = 0 ) {
		return !( $quantity >= $this->minimum_order_quantity() );
	}

	/**
	 * 
	 * @see OMH_Model_Chapter->get_earnings_percentage() for default value
	 * 
	 * @return 	string|int
	 */
	public function get_earnings_percentage( $allow_default = true ) {

		$chapter = $this->get_chapter();

		$earnings_percentage = null;

		if( $allow_default ) {

			// If Chapter is set and chapter has meta, override default with Chapter's Earnings Percentage
			if( $chapter ) {

				$chapter_earnings_percent = $chapter->get_earnings_percentage( $allow_default );
				if( !is_null( $chapter_earnings_percent ) && ( '' != $chapter_earnings_percent ) ) {
					$earnings_percentage = $chapter_earnings_percent;
				}
			}
		}

		// If Product has Earnings Percentage, override all Earnings Percentages
		// $meta_earnings_percent = $this->get_meta( '_mh_earnings_percentage', true );
		// if( !is_null( $meta_earnings_percent ) && ( '' != $meta_earnings_percent ) ) {
		// 	$earnings_percentage = $meta_earnings_percent;
		// }

		return $earnings_percentage;
	}

	/**
	 * @param 	string|int 	$earnings_percentage
	 * 
	 * @return 	string|int
	 */
	public function set_earnings_percentage( $earnings_percentage ) {

		if( empty( $earnings_percentage ) && ( 0 !== $earnings_percentage ) && ( '0' !== $earnings_percentage ) ) {
			$earnings_percentage = null;
		}

		$this->update_meta_data( '_mh_earnings_percentage', $earnings_percentage );
		$this->save();

		return $this->get_earnings_percentage( false );
	}

	// public function get_price_html( $price = '' ) {

	// 	$featured_garment = $this->get_featured_garment();
	// 	$featured_garment_pricing = $featured_garment->get_pricing();

	// 	$featured_garment_price = reset( $featured_garment_pricing );

	// 	return wc_price( $featured_garment_price, array( 'decimals' => 2 ) );
	// }

	/********************************************
	 * 			  INKSOFT FUNCTIONS				*
	 ********************************************/

	public function get_rendered_design() {

		return $this->get_meta( '_rendered_design', true ) ?: false;
	}

	public function set_rendered_design( $rendered_design ) {

		$this->update_meta_data( '_rendered_design', $rendered_design );
		$this->save();

		return $this->get_rendered_design();
	}

	/********************************************
	 * 			AVAILABILITY FUNCTIONS			*
	 ********************************************/


	/********************************************
	 * 			  META FUNCTIONS				*
	 ********************************************/

	public function generate_search_string() {

		// Get title
		$search_string = $this->get_title();

		if( $chapter = $this->get_chapter() ) {
		
			// Get chapter name
			$search_string .= ' ' . $chapter->get_chapter_name();

			if( $org = $chapter->get_organization() ) {
				
				// Get org letters
				$search_string .= ' ' . $org->get_org_letters();
			}

			if( $college = $chapter->get_college() ) {

				// Get college letters
				$search_string .= ' ' . $college->get_college_letters();
			}
		}

		return $search_string;
	}

	public function get_search_string() {

		return $this->get_meta( '_search_string', true ) ?: false;
	}

	public function set_search_string( $search_string ) {

		$this->update_meta_data( '_search_string', $search_string );

		return $this->get_search_string();
	}

	public function get_design_id() {

		return $this->get_meta( '_design_id', true ) ?: false;
	}

	public function set_design_id( $design_id ) {

		$this->update_meta_data( '_design_id', $design_id );

		return $this->get_design_id();
	}

	public function get_customer_uploads() {

		return array_filter( $this->get_meta( '_mh_customer_uploads', true ) ?: array() );
	}

	public function set_customer_uploads( $customer_uploads ) {

		$this->update_meta_data( '_mh_customer_uploads', array_filter( $customer_uploads ) );

		return $this->get_customer_uploads();
	}

	public function get_customer_comment() {

		return $this->get_meta( '_mh_customer_comments', true ) ?: '';
	}

	public function set_customer_comment( $customer_comment ) {

		$this->update_meta_data( '_mh_customer_comments', $customer_comment );

		return $this->get_customer_comment();
	}

	public function get_created_by() {

		return $this->get_meta( '_mh_created_by', true ) ?: 0;
	}

	public function set_created_by( $created_by ) {

		$this->update_meta_data( '_mh_created_by', $created_by );

		return $this->get_created_by();
	}

	public function get_product_type() {

		return $this->get_meta( '_mh_product_type', true );
	}

	public function set_product_type( $product_type ) {

		$this->update_meta_data( '_mh_product_type', $product_type );

		return $this->get_product_type();
	}

	public function get_deliver_date() {

		return $this->get_meta( '_mh_deliver_date', true );
	}

	public function set_deliver_date( $deliver_date ) {

		$this->update_meta_data( '_mh_deliver_date', $deliver_date );

		return $this->get_deliver_date();
	}

	/********************************************
	 * 			  HELPER FUNCTIONS				*
	 ********************************************/

	/**
	 * Returns whether this product allows free or a $0.00 price
	 * 
	 * @return 	bool
	 */
	public function is_allow_free() {

		if( 'on' == $this->get_meta( '_allow_free', true ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Returns the status of the product
	 * 
	 * @return 	string
	 */
	public function get_mh_product_status() {

		$status = 'design_in_review';

		if( $this->is_design_approved() ) {

			$status = 'design_finalized';

			if( $this->get_price() 
				|| $this->is_allow_free() 
			) {
				
				$status = 'active';
			}
		}

		return $status;
	}
}
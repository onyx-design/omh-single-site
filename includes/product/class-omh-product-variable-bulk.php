<?php
defined( 'ABSPATH' ) || exit;

class OMH_Product_Variable_Bulk extends OMH_Product_Variable {

	protected $sales_type = 'bulk';
	
	/**
	 * Order Status => Product Status
	 * 
	 * @tag Product Status
	 */
	public static $product_statuses = array(
		'design_in_review' => array (
			'color' 			=> 'warning',
			'label' 			=> 'Design in Review',
			'step'				=> 1,
			'admin_can_set'		=> true,
		),
		'ready_to_order' => array (
			'label'				=> 'Ready to Order',
			'color'				=> 'success',
			'purchasability'	=> true,
			'step'				=> 2,
			'admin_can_set'		=> true,
		),
		'pending' => array (
			'label' 			=> 'Ready to Order',
			'color'				=> 'success',
			'purchasability'	=> true,
			'step'				=> 3,
			'admin_can_set'		=> false,
		),
		'processing' => array (
			'color' 			=> 'info',
			'label' 			=> 'Paid',
			'step'				=> 4,
			'admin_can_set'		=> true,
		),
		'completed' => array (
			'color' 			=> 'primary',
			'label' 			=> 'Shipped',
			'step'				=> 5,
			'admin_can_set'		=> true,
		)
	);
	
	/**
	 * Sales Type
	 * 
	 * @return 	string
	 */
	public function get_sales_type() {
		return $this->sales_type;
	}

	/**
	 * Get all bulk order ids
	 * 
	 * @tag Bulk Order
	 */
	public function get_bulk_order_ids() {

		return $this->get_meta( 'bulk_order_ids' );
	}

	/**
	 * @tag Bulk Order
	 */
	public function set_bulk_order_ids( $bulk_order_ids ) {

		$this->update_meta_data( 'bulk_order_ids', $bulk_order_ids );

		return $bulk_order_ids;
	}

	/**
	 * @tag Bulk Order
	 */
	public function add_bulk_order_id( $bulk_order_id ) {

		$bulk_order_ids = (array) $this->get_bulk_order_ids();

		$bulk_order_ids[] = $bulk_order_id;

		return $this->set_bulk_order_ids( $bulk_order_ids );
	}

	/**
	 * @tag Bulk Order
	 * 
	 * @return int
	 */
	public function get_active_bulk_order_id() {

		// Get bulk order ID, or return 0 if there isn't one yet
		// This will wrap into the new product status typing that will be coming,
		// if a product status is completed, this will probably return 0
		return $this->get_meta( 'active_bulk_order_id' ) ?: 0;
	}

	/**
	 * @tag Bulk Order
	 * 
	 * @return int
	 */
	public function set_active_bulk_order_id( $active_bulk_order_id ) {

		$this->update_meta_data( 'active_bulk_order_id', $active_bulk_order_id );

		return $this->get_active_bulk_order_id();
	}

	/**
	 * @tag Bulk Order
	 */
	public function get_active_bulk_order() {
		return wc_get_order( $this->get_active_bulk_order_id() );
	}

	/**
	 * Can user edit this bulk order
	 */
	public function user_can_edit_bulk_order( $user_id = 0 ) {
		$user = empty($user_id) ? wp_get_current_user() : get_user_by('ID', $user_id);

		return ($user->has_role('house_admin') && $this->get_product_status('purchasability'));
		// return ( ( $user->has_role('administrator') && $this->get_product_status('step') == 1 )
		// 	|| ( $user->has_role('house_admin') && $this->get_product_status('purchasability') )
		// );
	}
}
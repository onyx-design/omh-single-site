<?php
defined( 'ABSPATH' ) || exit;

class OMH_Product_Garment_Bulk extends OMH_Product_Garment {
	
	protected $pricing = array();

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_pricing() {

		if( $this->product ) {

			// $product = wc_get_product( $this->get_product_id() );

			// If it's empty and we are attached to a product, we need to fill it with defaults
			if( ( !$this->pricing || empty( $this->pricing ) ) ) {

				$this->pricing = array_fill( 0, count( $this->product->get_product_price_tiers() ), 20 );
			} else {

				// Make sure pricing array is the same length as the product price tiers
				$this->pricing = array_pad( $this->pricing, count( $this->product->get_product_price_tiers() ), 20 );
			}
		}

		return array_map( 'floatval', $this->pricing );
	}

	public function set_pricing( $pricing ) {

		$this->pricing = array_map( 'floatval', $pricing );

		return $this->get_pricing();
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	/**
	 * Returns this model in Array format
	 * 
	 * @return 	array
	 */
	public function to_array( $add_garment_style = false ) {

		$return_map = array(
			'garment_style_id',
			'image_front',
			'image_back',
			'image_proof',
			'featured',
			'pricing',
			'sizes'
		);

		$object_vars = array();
		foreach( get_object_vars( $this ) as $key => $value ) {

			if( in_array( $key, $return_map ) && method_exists( $this, "get_$key" ) ) {

				$object_vars[ $key ] = $this->{ "get_$key" }();
			}
		}

		if( $add_garment_style ) {

			$garment_style = $this->get_garment_style();

			if( $garment_style ) {
				$object_vars['garment'] = $garment_style->to_array();
			}
			else {
				$object_vars['garment'] = null;
			}
		}

		return $object_vars;
	}

	protected function update_variation( $variation ) {

		$garment_price_tiers = $this->get_pricing();

		// Set Price to the minimum Price Tier price
		$variation->set_price( reset( $garment_price_tiers ) );
		$variation->set_regular_price( reset( $garment_price_tiers ) );

		// Set Stock
		$variation->set_manage_stock( false );
		$variation->set_stock_status( 'instock' );

		$variation->set_image_id( $this->get_image_front( true ) );

		$variation->save();
	}
}
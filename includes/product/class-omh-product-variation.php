<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Product_Variation extends WC_Product_Variation {

	private $cart_parent_product_quantity = false;

	protected $parent_product = null;

	/**
	 * Constructor
	 * 
	 * @param 	mixed 	$product
	 * 
	 * @return 	bool|void
	 */
	public function __construct( $product = 0 ) {

		try {
			parent::__construct( $product );
		} catch( Exception $e ) {
			return false;
		}
	}

	public function get_product( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->parent_product ) ) {

			$this->parent_product = wc_get_product( $this->get_parent_id() );
		}

		return $this->parent_product;
	}

	/********************************************
	 * 			 ATTRIBUTE FUNCTIONS 			*
	 ********************************************/

	/**** Garments ****/

	public function get_product_garment() {

		if( $this->get_product() ) {

			return $this->get_product()->get_garment_by_term_name( $this->get_garment_attribute() );
		}
	}

	public function has_attribute( $attribute_slug ) {

		return in_array( $attribute_slug, $this->get_variation_attributes() );
	}

	public function get_garment_attribute() {

		$garment = $this->get_attribute( 'garment' );

		return $garment;
	}

	public function set_garment_attribute( $garment_slug ) {

		if( !$garment_slug ) {
			return false;
		}

		$this->update_meta_data( 'attribute_pa_garment', $garment_slug );

		return $this->get_product_garment();
	}

	/**** Sizes ****/

	public function get_size_attribute() {

		$size = $this->get_attribute( 'size' );

		return $size;
	}

	public function set_size_attribute( $size_slug ) {

		if( !$size_slug ) {
			return false;
		}

		$this->update_meta_data( 'attribute_pa_size', $size_slug );

		return $this->get_size_attribute();
	}

	/********************************************
	 * 			  PRICING FUNCTIONS 			*
	 ********************************************/

	public function get_cart_parent_quantity() {

		$parent_quantity = 0;
		foreach( WC()->cart->get_cart_contents() as $cart_item ) {

			if( $this->get_parent_id() == $cart_item['product_id'] ) {

				$parent_quantity += $cart_item['quantity'];
			}
		}

		return $parent_quantity;
	}

	/**
	 * Get the Garment price Tiers
	 * 
	 * @return 	array
	 */
	public function get_garment_price_tiers() {

		if( $this->get_product_garment() ) {

			return $this->get_product_garment()->get_pricing();
		}

	}

	/**
	 * Returns the price of this variation based on the parent quantity within an order
	 * 
	 * Context: Order
	 * 
	 * @param 	int|null 	$parent_total_qty
	 * 
	 * @return 	int
	 */
	public function get_tier_price( $parent_total_qty = null ) {

		$garment_price_tiers = $this->get_garment_price_tiers();

		if( !$parent_total_qty ) {
			// Return minimum garment price tier
			return $garment_price_tiers[0];
		}

		$parent_product = $this->get_product();
		$parent_product_price_tier = $parent_product->get_product_price_tier( $parent_total_qty );

		// If parent product price tier isn't false and exists in the tier array, get that tier's price,
		// otherwise get the last tier
		return ( ( false !== $parent_product_price_tier ) && isset( $garment_price_tiers[ $parent_product_price_tier ] ) ) ? $garment_price_tiers[ $parent_product_price_tier ] : array_pop( $garment_price_tiers );
	}

	/********************************************
	 * 			 	META FUNCTIONS 				*
	 ********************************************/
}
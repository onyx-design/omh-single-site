<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * This Resource Model is the model attached to the Product
 * which pulls data from the Global Garment table
 * 
 * dev:todo This is the same thing as the Chapter, Organization, and College models - find a place where these go
 */
class OMH_Product_Garment_Old {

	/**
	 * Global Garment ID
	 * 
	 * @var int
	 */
	private $garment_style_id = 0;

	/**
	 * Image Front
	 * 
	 * array(
	 * 	id 				=> '',
	 *  is_temp 		=> false,
	 * 	src 			=> ''
	 *  thumbnail_src 	=> ''
	 * )
	 * 
	 * @var array|null
	 */
	private $image_front = array();

	/**
	 * Image Back
	 * 
	 * array(
	 * 	id 				=> '',
	 *  is_temp 		=> false,
	 * 	src 			=> ''
	 *  thumbnail_src 	=> ''
	 * )
	 * 
	 * @var array|null
	 */
	private $image_back = array();

	/**
	 * Image Proof
	 * 
	 * array(
	 * 	id 				=> '',
	 *  is_temp 		=> false,
	 * 	src 			=> ''
	 *  thumbnail_src 	=> ''
	 * )
	 * 
	 * @var array|null
	 */
	private $image_proof = array();

	private $featured = false;

	/**
	 * @var type
	 */
	private $pricing = array();

	/**
	 * @var type
	 */
	private $sizes = array();

	private $product = null;

	private $garment_style = null;

	private $deleted = false;

	/********************************
	 *			Functions			*
	 ********************************/

	public static function make( $args ) {
		return new OMH_Product_Garment( $args );
	}

	public function __construct( $product_garment ) {

		foreach( $product_garment as $key => $value ) {

			if( method_exists( $this, "set_{$key}" ) ) {

				$this->{ "set_{$key}" }( $value );
			}
		}
	}

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_garment_style_id() {

		return $this->garment_style_id;
	}

	public function set_garment_style_id( $garment_style_id ) {

		$this->garment_style_id = $garment_style_id;

		return $this->get_garment_style_id();
	}

	public function get_image_front() {

		return array_merge( $this->get_image_defaults(), (array) $this->image_front );
	}

	public function set_image_front( $image_front ) {

		$this->image_front = array_merge( $this->get_image_defaults(), (array) $image_front );

		return $this->get_image_front();
	}

	public function get_image_back() {

		return array_merge( $this->get_image_defaults(), (array) $this->image_back );
	}

	public function set_image_back( $image_back ) {

		$this->image_back = array_merge( $this->get_image_defaults(), (array) $image_back );

		return $this->get_image_back();
	}

	public function get_image_proof() {

		return array_merge( $this->get_image_defaults(), (array) $this->image_proof );
	}

	public function set_image_proof( $image_proof ) {

		$this->image_proof = array_merge( $this->get_image_defaults(), (array) $image_proof );

		return $this->get_image_proof();
	}

	public function get_featured() {

		return maybe_str_to_bool( $this->featured );
	}

	public function set_featured( $featured ) {

		$this->featured = maybe_str_to_bool( $featured );

		return $this->get_featured();
	}

	public function get_pricing() {

		if( $this->get_product_id() ) {

			$product = wc_get_product( $this->get_product_id() );

			// If it's empty and we are attached to a product, we need to fill it with defaults
			if( ( !$this->pricing || empty( $this->pricing ) ) ) {

				$this->pricing = array_fill( 0, count( $product->get_product_price_tiers() ), 20 );
			} else {

				// Make sure pricing array is the same length as the product price tiers
				$this->pricing = array_pad( $this->pricing, count( $product->get_product_price_tiers() ), 20 );
			}
		}

		return array_map( 'floatval', $this->pricing );
	}

	public function set_pricing( $pricing ) {

		$this->pricing = array_map( 'floatval', $pricing );

		return $this->get_pricing();
	}

	public function get_sizes() {

		if( $product_garment_style = $this->get_garment_style() ) {

			$garment_style_sizes = explode( ',', $product_garment_style->get_sizes() );

			$default_sizes = array_fill_keys( $garment_style_sizes, false );

			if( ( !$this->sizes || empty( $this->sizes ) ) ) {

				$this->sizes = $default_sizes;
			} else {

				$this->sizes = array_merge( $default_sizes, $this->sizes );
			}
		}

		return array_map( 'maybe_str_to_bool', $this->sizes );
	}

	public function set_sizes( $sizes ) {

		$this->sizes = array_map( 'maybe_str_to_bool', $sizes );

		return $this->get_sizes();
	}

	public function get_product_id() {

		return $this->product_id;
	}

	public function set_product_id( $product_id ) {

		$this->product_id = $product_id;

		$this->product = wc_get_product( $product_id );

		return $this->get_product_id();
	}

	public function is_deleted() {

		return $this->deleted;
	}

	public function set_deleted() {

		$this->deleted = true;

		return true;
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	public function get_product() {

		if( is_null( $this->product ) && $this->get_product_id() ) {

			$this->product = wc_get_product( $this->get_product_id() );
		}

		return $this->product;
	}

	/**
	 * Get the Garment Objects from our custom tables using the $garment_style_id
	 * 
	 * @return 	OMH_Model_Garment_Product
	 */
	public function get_garment_style() {

		if( !$this->get_garment_style_id() ) {
			return false;
		}

		if( is_null( $this->garment_style ) && $this->get_garment_style_id() ) {

			$this->garment_style = OMH()->garment_style_factory->read( $this->get_garment_style_id() );
		}

		return $this->garment_style;
	}

	public function get_frontend_garment_data() {

		if( !$this->is_deleted() ) {

			return array(
				'garment_style_id'	=> $this->get_garment_style_id(),
				'image_front'		=> $this->get_image_front(),
				'image_back'		=> $this->get_image_back(),
				'image_proof'		=> $this->get_image_proof(),
				'featured'			=> $this->get_featured(),
				'pricing'			=> $this->get_pricing(),
				'sizes'				=> $this->get_sizes()
			);
		}
	}

	public function get_image_defaults() {

		return array(
			'id'			=> 0,
			'is_temp'		=> false,
			'src'			=> '',
			'thumbnail_src'	=> ''
		);
	}


	/**
	 * Return whether this Product Garment exists or not
	 * 
	 * @return 	bool
	 */
	public function exists() {

		if( $this->get_product_id() ) {
			return true;
		}

		return false;
	}

	/**
	 * Returns this model in Array format
	 * 
	 * @return 	array
	 */
	public function to_array() {

		$return_map = array(
			'garment_style_id',
			'image_front',
			'image_back',
			'image_proof',
			'featured',
			'pricing',
			'sizes'
		);

		$object_vars = array();
		foreach( get_object_vars( $this ) as $key => $value ) {

			if( in_array( $key, $return_map ) && method_exists( $this, "get_$key" ) ) {

				$object_vars[ $key ] = $this->{ "get_$key" }();
			}
		}

		return $object_vars;
	}

	/**
	 * Add size variation by size name
	 * 
	 * @param type $garment_size 
	 * @return type
	 */
	public function add_size_variation( $garment_size ) {

		$product_variable = $this->get_product();
		$variable_post = get_post( $product_variable->get_id() );

		$garment_style = $this->get_garment_style();

		// Chapter Data
		$chapter = $product_variable->get_chapter();
		$college = $chapter->get_college();
		$organization = $chapter->get_org();

		$size_term = omh_get_or_create_wc_attribute_term( 'size', $garment_size );
		$variation_size = strtolower( $garment_size );

		$garment_style_slug = $garment_style->get_style_slug();
		$garment_product = $garment_style->get_garment_product();
		$garment_color = $garment_style->get_garment_color();
		$garment_brand = $garment_product->get_garment_brand();

		$garment_price_tiers = $this->get_pricing();

		$variation_post = array(
			'post_title'	=> $product_variable->get_title(),
			'post_name'		=> "{$variable_post->post_name}-{$garment_style_slug}-{$variation_size}",
			'post_status'	=> 'publish',
			'post_parent'	=> $product_variable->get_id(),
			'post_type'		=> 'product_variation',
			'guid'			=> $product_variable->get_permalink()
		);
		$variation_id = wp_insert_post( $variation_post );

		if( $variation_id && !is_wp_error( $variation_id ) ) {

			$garment_variation = new OMH_Product_Variation( $variation_id );

			// Set Garment Attribute on Variation
			$garment_variation->set_garment_attribute( $garment_style_slug );

			// Allow size to be used for variations
			$product_variable->add_size( $garment_size );

			$garment_variation->set_size_attribute( $size_term->slug );

			// Set SKU
			// Old: [Store code]-[post id]-[garment brand code][garment product manufacturer sku]-[color code]-[size code]
			// New: [college code]-[org code]-[post id]-[garment brand code]-[garment product manufacturer sku]-[color code]-[size code]
			$variation_sku = "{$college->get_college_letters()}-{$organization->get_org_letters()}-{$product_variable->get_id()}-{$garment_brand->get_brand_code()}-{$garment_product->get_manufacturer_sku()}-{$garment_color->get_color_code()}-{$variation_size}";
			$garment_variation->set_sku( strtoupper( $variation_sku ) );

			// Set Price to the minimum Price Tier price
			$garment_variation->set_price( reset( $garment_price_tiers ) );
			$garment_variation->set_regular_price( reset( $garment_price_tiers ) );

			// Set Stock
			$garment_variation->set_manage_stock( false );
			$garment_variation->set_stock_status( 'instock' );

			$garment_variation->set_garment_style_id( $this->get_garment_style_id() );
			$garment_variation->set_frontend_garment_data( $this->get_frontend_garment_data() );

			if( $image_front = $this->get_image_front() ) {
				$garment_variation->set_image_id( $image_front['id'] );
			}

			return $garment_variation->save();
		}

		return false;
	}

	public function update_size_variation( $garment_size ) {

	}

	/**
	 * Convert the images from Temporary Files to WP Uploads
	 * 
	 * @return type
	 */
	public function save_images() {

		$images = array(
			'image_front'	=> $this->get_image_front(),
			'image_back'	=> $this->get_image_back(),
			'image_proof'	=> $this->get_image_proof()
		);

		foreach( $images as $image_type => $image ) {

			if( maybe_str_to_bool( $image['is_temp'] ) && $image['id'] ) {

				if( $inserted_image = OMH_Tool_Sideload_Media::insert_media_file_from_temp( $image['id'] ) ) {

					$this->{ "set_{$image_type}" }( $inserted_image );
				} else {

					return false;
				}
			}
		}

		return true;
	}

	/********************************
	 *			  CRUD				*
	 ********************************/

	/**
	 * Static create Product Garment Variation
	 * 
	 * @param 	OMH_Product_Variable 		$product_variable 
	 * @param 	OMH_Product_Garment|null 	$product_garment 
	 */
	public static function create( $product_variable, $product_garment = null ) {

		// Product Data
		$product_variable = wc_get_product( $product_variable );
		$product_garment->set_product_id( $product_variable->get_id() );

		// Garment Data
		$garment_style = $product_garment->get_garment_style();
		$garment_style_slug = $garment_style->get_style_slug();
		$garment_style_name = $garment_style->get_style_name();

		// Garment Size Data
		$garment_available_sizes = array_keys( array_filter( $product_garment->get_sizes(), 'maybe_str_to_bool' ) );

		// Allow garment to use be used for variations
		$product_variable->add_garment( $garment_style_name, $garment_style_slug );

		// Save images
		$product_garment->save_images();

		/*
		 * Create Variation for each size/garment combination
		 */
		foreach( $garment_available_sizes as $garment_size ) {

			$product_garment->add_size_variation( $garment_size );
		}

		// Set Featured Image
		if( $product_garment->get_featured() ) {

			$product_variable->set_image_id( $product_garment->get_image_front()['id'] );
			$product_variable->save();
		}

		return $product_garment;
	}

	/**
	 * dev:todo Finish this?
	 * 
	 * @param 	OMH_Product_Variable 		$product_variable 
	 * @param 	OMH_Product_Garment|null 	$product_garment 
	 */
	public static function update( $product_variable, $product_garment = null ) {

		// Product Data
		$product_variable = wc_get_product( $product_variable );
		$product_garment->set_product_id( $product_variable->get_id() );

		// Chapter Data
		$chapter = $product_variable->get_chapter();
		$college = $chapter->get_college();
		$organization = $chapter->get_org();

		// Garment Data
		$garment_style = $product_garment->get_garment_style();
		$garment_product = $garment_style->get_garment_product();
		$garment_color = $garment_style->get_garment_color();
		$garment_brand = $garment_product->get_garment_brand();
		$garment_price_tiers = $product_garment->get_pricing();

		// Garment Size Data
		$garment_available_sizes = array_keys( array_filter( $product_garment->get_sizes(), 'maybe_str_to_bool' ) );
		$garment_existing_sizes = array();

		// Get variations with this garment
		$garment_variations = array_map( 'wc_get_product', $product_variable->get_garment_variations( $garment_style->get_id() ) );

		// Save images
		$product_garment->save_images();

		/*
		 * Update Current Variations
		 */
		foreach( $garment_variations as $garment_variation ) {

			// Compare selected sizes
			if( !in_array( $garment_variation->get_size_attribute(), $garment_available_sizes ) ) {

				$garment_variation->delete();
				continue;
			}

			$garment_size = $garment_variation->get_size_attribute();
			$variation_size = strtolower( $garment_size );
			$garment_existing_sizes[] = $garment_size;

			// Set SKU
			// Old: [Store code]-[post id]-[garment brand code][garment product manufacturer sku]-[color code]-[size code]
			// New: [college code]-[org code]-[post id]-[garment brand code]-[garment product manufacturer sku]-[color code]-[size code]
			$variation_sku = "{$college->get_college_letters()}-{$organization->get_org_letters()}-{$product_variable->get_id()}-{$garment_brand->get_brand_code()}-{$garment_product->get_manufacturer_sku()}-{$garment_color->get_color_code()}-{$variation_size}";
			$garment_variation->set_sku( strtoupper( $variation_sku ) );

			// Make sure prices are up to date
			$garment_variation->set_price( reset( $garment_price_tiers ) );
			$garment_variation->set_regular_price( reset( $garment_price_tiers ) );

			$garment_variation->set_frontend_garment_data( $product_garment );

			if( $image_front = $product_garment->get_image_front() ) {
				$garment_variation->set_image_id( $image_front['id'] );
			}

			$garment_variation->save();

			OMH()->clear_page_cache( $garment_variation->get_id() );
		}

		/*
		 * Add New Variations
		 */
		foreach( array_diff( $garment_available_sizes, $garment_existing_sizes ) as $new_size ) {

			$product_garment->add_size_variation( $new_size );
		}

		// Set Featured Image
		if( $product_garment->get_featured() ) {

			$product_variable->set_image_id( $product_garment->get_image_front()['id'] );
			$product_variable->save();
		}

		return $product_garment;
	}

	public static function delete( $product_variable, $product_garment = null ) {

		$product_variable = wc_get_product( $product_variable );
		$product_garment->set_product_id( 0 );

		$garment_style = $product_garment->get_garment_style();

		// Get variations with this garment
		$garment_variations = $product_variable->get_garment_variations( $garment_style->get_id() );

		foreach( $garment_variations as $garment_variation_id ) {

			$garment_variation = wc_get_product( $garment_variation_id );

			$garment_variation->delete();
		}

		// Dev:todo Check if all variations were deleted? If not we should show a message
		$product_garment->set_deleted();

		return $product_garment;
	}
}
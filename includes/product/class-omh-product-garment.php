<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * This Resource Model is the model attached to the Product
 * which pulls data from the Global Garment table
 */
class OMH_Product_Garment {

	protected $product = null;

	protected $product_attribute = null;

	protected $garment_style = null;

	protected $garment_meta = array();

	protected $garment_term = null;

	protected $garment_term_id = null;

	protected $size_term = null;

	protected $size_term_id = null;

	protected $variations = null;

	protected $garment_style_id = 0;
	
	protected $pricing = array();

	protected $image_front = array();
	protected $image_back = array();
	protected $image_proof = array();

	protected $sizes = array();

	const image_defaults = array(
		'id'			=> 0,
		'is_temp'		=> false,
		'src'			=> '',
		'thumbnail_src'	=> ''
	);

	/**
	 * { function_description }
	 *
	 * @param      <type>  $product       The product
	 * @param      ...     $garment_args  The garment arguments
	 * 						
	 */
	public function __construct( $product, $garment_meta = null ) {

		$this->product = $product;

		// omh_debug(get_class($this));
		// omh_debug(method_exists($this, 'set_base_price'));

		foreach( $garment_meta as $key => $value ) {
			// omh_debug($key);
			// omh_debug($value);

			if( method_exists( $this, "set_{$key}" ) ) {

				$this->{ "set_{$key}" }( $value );
			}
		}

		$this->garment_meta = $garment_meta;

		// $this->garment_style_id = (int) $garment_meta['garment_style_id'] ?? 0;

		// foreach( $garment_args as $key => $arg ) {

		// 	swithc( $key ) {
		// 		case 'meta':

		// 			$this->garment
		// 			break;
		// 	}
		// }
		
		// foreach( $garment_args as $arg ) {

		// 	if( is_array( $arg ) ) {

		// 		$this->garment_data = $arg;
		// 	}
		// 	else if( $arg instanceof WC_Product_Attribute  ) {

		// 		// $this->product_attribu
		// 	}
		// }
	}

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_garment_style_id() {

		return (int) $this->garment_style_id;
	}

	public function set_garment_style_id( $garment_style_id ) {

		$this->garment_style_id = (int) $garment_style_id;

		return $this->get_garment_style_id();
	}

	public function get_pricing() {

		if( $this->product ) {

			// $product = wc_get_product( $this->get_product_id() );

			// If it's empty and we are attached to a product, we need to fill it with defaults
			if( ( !$this->pricing || empty( $this->pricing ) ) ) {

				$this->pricing = array_fill( 0, count( $this->product->get_product_price_tiers() ), 20 );
			} else {

				// Make sure pricing array is the same length as the product price tiers
				$this->pricing = array_pad( $this->pricing, count( $this->product->get_product_price_tiers() ), 20 );
			}
		}

		return array_map( 'floatval', $this->pricing );
	}

	public function set_pricing( $pricing ) {

		$this->pricing = array_map( 'floatval', $pricing );

		return $this->get_pricing();
	}

	public function get_image_front( $return_id = false ) {

		$image_front = $this->sanitize_image_data( $this->image_front );

		return $return_id ? $image_front['id'] ?? 0 : $image_front;
	}

	public function set_image_front( $image_front ) {

		$this->image_front = $this->sanitize_image_data( $image_front );

		return $this->get_image_front();
	}

	public function get_image_back( $return_id = false ) {

		$image_back = $this->sanitize_image_data( $this->image_back );

		return $return_id ? $image_back['id'] ?? 0 : $image_back;
	}

	public function set_image_back( $image_back ) {

		$this->image_back = $this->sanitize_image_data( $image_back );

		return $this->get_image_back();
	}

	public function get_image_proof( $return_id = false ) {

		$image_proof = $this->sanitize_image_data( $this->image_proof );

		return $return_id ? $image_proof['id'] ?? 0 : $image_proof;
	}

	public function set_image_proof( $image_proof ) {

		$this->image_proof = $this->sanitize_image_data( $image_proof );

		return $this->get_image_proof();
	}

	public function get_featured() {

		return maybe_str_to_bool( $this->featured );
	}

	public function set_featured( $featured ) {

		$this->featured = maybe_str_to_bool( $featured );

		return $this->get_featured();
	}

	public function get_sizes() {

		if( $product_garment_style = $this->get_garment_style() ) {

			$garment_style_sizes = explode( ',', $product_garment_style->get_sizes() );

			$default_sizes = array_fill_keys( $garment_style_sizes, false );

			if( ( !$this->sizes || empty( $this->sizes ) ) ) {

				$this->sizes = $default_sizes;
			} else {

				$this->sizes = array_merge( $default_sizes, $this->sizes );
			}
		}

		return array_map( 'maybe_str_to_bool', $this->sizes );
	}

	public function set_sizes( $sizes ) {

		$this->sizes = array_map( 'maybe_str_to_bool', $sizes );

		return $this->get_sizes();
	}

	/**
	 * Virtual / non-local getters/setters
	 */
	public function get_product() {
		return $this->product;
	}

	public function get_garment_style( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->garment_style ) ) {

			if( $this->get_garment_style_id() ) {

				$this->garment_style = OMH()->garment_style_factory->read( $this->get_garment_style_id() );
			}

		}

		return $this->garment_style;
	}

	public function get_garment_term( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->garment_term ) ) {

			if( $this->get_garment_style( $force_refresh ) ) {

				$this->garment_term = $this->get_garment_style()->get_term();
			}
		}

		return $this->garment_term;
	}

	public function get_garment_term_id( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->garment_term_id ) ) {

			if( $this->get_garment_term( $force_refresh ) ) {

				$this->garment_term_id = $this->garment_term->term_id;
			}
		}

		return $this->garment_term_id;
	}

	public function get_garment_term_name() {

		if( $this->get_garment_term() ) {

			return $this->get_garment_term()->name;
		}
	}

	public function get_garment_term_slug() {

		if( $this->get_garment_term() ) {

			return $this->get_garment_term()->slug;
		}
	}

	public function get_enabled_sizes( $return_term_slugs = false ) {

		$enabled_sizes = array_keys( array_filter( $this->get_sizes() ) );

		if( $return_term_slugs ) {
			foreach( $enabled_sizes as $key => $size ) {
				$enabled_sizes[ $key ] = 'size-' . strtolower( $size );
			}
		}

		return $enabled_sizes;
	}

	public function get_garment_variations( $force_refresh = false ) {
		
		if( $force_refresh || !isset( $this->variations ) ) {
			$size_terms = get_terms( 'pa_size' );
			$this->variations = array();			
			foreach( $this->product->get_variations( $force_refresh ) as $variation ) {
				$variation_attributes = $variation->get_attributes();
				if( isset( $variation_attributes['pa_garment'] ) && $variation_attributes['pa_garment'] == $this->get_garment_term_slug() ) {
					// find index from size_terms and use as key for each variation
					$pa_size = $variation->get_data()['attributes']['pa_size'];
					$index = array_search($pa_size, array_column($size_terms, 'slug'));
					$this->variations[$index] = $variation;
				}
			}
			// sort variations by key
			ksort($this->variations);
		}

		return $this->variations;
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	/**
	 * Returns this model in Array format
	 * 
	 * @return 	array
	 */
	public function to_array( $add_garment_style = false ) {

		$return_map = array(
			'garment_style_id',
			'image_front',
			'image_back',
			'image_proof',
			'featured',
			'pricing',
			'sizes'
		);

		$object_vars = array();
		foreach( get_object_vars( $this ) as $key => $value ) {

			if( in_array( $key, $return_map ) && method_exists( $this, "get_$key" ) ) {

				$object_vars[ $key ] = $this->{ "get_$key" }();
			}
		}

		if( $add_garment_style ) {

			$garment_style = $this->get_garment_style();

			if( $garment_style ) {
				$object_vars['garment'] = $garment_style->to_array();
			}
			else {
				$object_vars['garment'] = null;
			}
		}

		return $object_vars;
	}

	public function sanitize_image_data( $image_data ) {

		$sanitized_image_data = array_merge( static::image_defaults, (array) $image_data );

		$sanitized_image_data['id'] 		= (int) $sanitized_image_data['id'];
		$sanitized_image_data['is_temp'] 	= maybe_str_to_bool( ( $sanitized_image_data['is_temp'] ?? false ) );

		return $image_data;
	}

	/**
	 * Convert the images from Temporary Files to WP Uploads
	 * 
	 * @return type
	 */
	public function save_images() {

		$images = array(
			'image_front'	=> $this->get_image_front(),
			'image_back'	=> $this->get_image_back(),
			'image_proof'	=> $this->get_image_proof()
		);

		foreach( $images as $image_type => $image ) {

			if( maybe_str_to_bool( $image['is_temp'] ?? false ) && $image['id'] ) {

				if( $inserted_image = OMH_Tool_Sideload_Media::insert_media_file_from_temp( $image['id'] ) ) {

					$this->{ "set_{$image_type}" }( $inserted_image );
				} else {

					return false;
				}
			}
		}

		return true;
	}

	
	/********************************
	 *			 Variations			*
	 ********************************/

	public function sync() {

		$test_attributes = array(
			'pa_garment'	=> $this->get_garment_term_slug(),
			'pa_size'		=> ''
		);

		$enabled_sizes = $this->get_enabled_sizes();

		// Add/update any sizes that are enabled
		foreach( $enabled_sizes as $size ) {

			$test_attributes['pa_size'] = 'size-' . strtolower( $size );

			$variation = $this->product->get_variation_by_attributes( $test_attributes );

			if( !$variation ) {

				$variation = $this->add_variation( $size );
			}

			$this->update_variation( $variation );

		}

		foreach( $this->get_garment_variations( true ) as $variation_id => $variation ) {

			$size = strtoupper( str_replace( 'size-', '', $variation->get_size_attribute() ) );

			if( !in_array( $size, $enabled_sizes ) ) {

				$variation->delete();
			}

		}
	}
	/**
	 * Add size variation by size name
	 * 
	 * @param type $garment_size 
	 * @return type
	 */
	protected function add_variation( $garment_size ) {

		$product 			= $this->get_product();
		$product_post 		= get_post( $product->get_id() );

		$garment_style 		= $this->get_garment_style();
		$garment_product 	= $garment_style->get_garment_product();
		$garment_color 		= $garment_style->get_garment_color();
		$garment_brand 		= $garment_product->get_garment_brand();


		// Chapter Data
		$chapter 			= $product->get_chapter();
		$college 			= $chapter->get_college();
		$organization 		= $chapter->get_org();

		$size_term 			= omh_get_or_create_wc_attribute_term( 'size', $garment_size );
		$variation_size 	= strtolower( $garment_size );

		$garment_style_slug = $garment_style->get_style_slug();
		$garment_product 	= $garment_style->get_garment_product();

		$variation_post = array(
			'post_title'	=> $product->get_title(),
			'post_name'		=> "{$product_post->post_name}-{$garment_style_slug}-{$variation_size}",
			'post_status'	=> 'publish',
			'post_parent'	=> $product->get_id(),
			'post_type'		=> 'product_variation',
			'guid'			=> $product->get_permalink()
		);
		$variation_id = wp_insert_post( $variation_post );

		if( $variation_id && !is_wp_error( $variation_id ) ) {

			$variation = new OMH_Product_Variation( $variation_id );

			// Set Garment Attribute on Variation
			$variation->set_garment_attribute( $garment_style_slug );

			// Allow size to be used for variations
			// $product->add_size( $garment_size );

			$variation->set_size_attribute( $size_term->slug );

			// Set SKU
			// Old: [Store code]-[post id]-[garment brand code][garment product manufacturer sku]-[color code]-[size code]
			// New: [college code]-[org code]-[post id]-[garment brand code]-[garment product manufacturer sku]-[color code]-[size code]
			$variation_sku = implode( '-', 
				array(
					$college->get_college_letters(),
					$organization->get_org_letters(),
					$product->get_id(),
					$garment_brand->get_brand_code(),
					$garment_product->get_manufacturer_sku(),
					$garment_color->get_color_code(),
					$variation_size
				)
			);

			$variation->set_sku( strtoupper( $variation_sku ) );

			return $variation;
		}

		return false;
	}

	protected function update_variation( $variation ) {

		$garment_price_tiers = $this->get_pricing();

		// Set Price to the minimum Price Tier price
		$variation->set_price( reset( $garment_price_tiers ) );
		$variation->set_regular_price( reset( $garment_price_tiers ) );

		// Set Stock
		$variation->set_manage_stock( false );
		$variation->set_stock_status( 'instock' );

		$variation->set_image_id( $this->get_image_front( true ) );

		$variation->save();
	}
}

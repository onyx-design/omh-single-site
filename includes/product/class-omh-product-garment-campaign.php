<?php
defined( 'ABSPATH' ) || exit;

class OMH_Product_Garment_Campaign extends OMH_Product_Garment {

	protected $base_price = 0;

	protected $retail_price = 0;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_base_price() {
		return (string) $this->base_price;
	}

	public function set_base_price( $base_price ) {

		$this->base_price = (float) $base_price;

		return $this->get_base_price();
	}

	public function get_retail_price() {
		return (string) $this->retail_price;
	}

	public function set_retail_price( $retail_price ) {

		$this->retail_price = (float) $retail_price;

		return $this->get_retail_price();
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	/**
	 * Returns this model in Array format
	 * 
	 * @return 	array
	 */
	public function to_array( $add_garment_style = false ) {

		$return_map = array(
			'garment_style_id',
			'image_front',
			'image_back',
			'image_proof',
			'featured',
			'base_price',
			'retail_price',
			'sizes'
		);

		$object_vars = array();
		foreach( get_object_vars( $this ) as $key => $value ) {

			if( in_array( $key, $return_map ) && method_exists( $this, "get_$key" ) ) {

				$object_vars[ $key ] = $this->{ "get_$key" }();
			}
		}

		if( $add_garment_style ) {

			$garment_style = $this->get_garment_style();

			if( $garment_style ) {
				$object_vars['garment'] = $garment_style->to_array();
			}
			else {
				$object_vars['garment'] = null;
			}
		}

		return $object_vars;
	}

	protected function update_variation( $variation ) {

		// Set Price to the minimum Price Tier price
		$variation->set_price( $this->get_retail_price() );
		$variation->set_regular_price( $this->get_retail_price() );

		// Set Stock
		$variation->set_manage_stock( false );
		$variation->set_stock_status( 'instock' );

		$variation->set_image_id( $this->get_image_front( true ) );

		$variation->save();
	}
}
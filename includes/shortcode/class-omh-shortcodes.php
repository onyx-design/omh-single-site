<?php
defined( 'ABSPATH' ) ?? exit;

class OMH_Shortcodes {

	public function __construct() {

		add_action( 'init', array( $this, 'register_shortcodes' ) );
	}

	public function register_shortcodes() {

		$shortcodes = array(
			'landing-page-form'				=> __CLASS__ . '::landing_page_form',
			'signup-form'					=> __CLASS__ . '::signup_form',
			'house-admin-activation-form'	=> __CLASS__ . '::house_admin_activation_form',
			'house-user-activation-form'	=> __CLASS__ . '::house_user_activation_form',
			'organization-start-design-form'	=> __CLASS__ . '::organization_start_design_form',

		);

		foreach( $shortcodes as $shortcode => $function ) {
			add_shortcode( 'merchhouse-' . $shortcode, $function );
		}
	}

	public static function landing_page_form( $atts ) {
		?>
		<div class="mh-landing-page-signup-wrapper container">
			<div class="mh-landing-page-signup-message">You will hear from us soon!</div>
			<form id="mh-landing-page-signup-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="landing_page_signup_form" data-form-nonce="<?php echo wp_create_nonce( 'landing-page-signup-form' ); ?>">
				<?php
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'email',
							'label'			=> false,
							'placeholder' 	=> 'Email Address',
							'class'			=> array(
								'landing-page-signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'college',
							'label'			=> false,
							'placeholder' 	=> 'What college do you attend?',
							'class'			=> array(
								'landing-page-signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'organization',
							'label'			=> false,
							'placeholder' 	=> 'What fraternity/sorority are you in?',
							'class'			=> array(
								'landing-page-signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Button::factory(
						array(
							'color'		=> 'primary',
							'value'		=> 'submit',
							'form'		=> 'mh-landing-page-signup-form',
							'class'		=> array(
								'landing-page-signup-submit',
								'mh-forms-submit'
							),
							'label'		=> 'Submit',
						)
					);
				?>
			</form>
		</div>
		<script>
			jQuery(document).bind('omh_landing_page_signup_form', function(e, json, response) {
				jQuery('#mh-landing-page-signup-form').addClass('landing-page-submitted');
				jQuery('.mh-landing-page-signup-message').addClass('landing-page-submitted');
			});
		</script>
		<?php
	}

	public static function signup_form( $atts ) {
		?>
		<div class="mh-signup-wrapper">
			<form id="mh-signup-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="signup_form" data-form-nonce="<?php echo wp_create_nonce( 'signup-form' ); ?>">
				<?php
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'name',
							'label'			=> false,
							'placeholder' 	=> 'Name',
							'class'			=> array(
								'signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_type'	=> 'email',
							'input_id'		=> 'email',
							'label'			=> false,
							'placeholder' 	=> 'Email Address',
							'class'			=> array(
								'signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'phone_number',
							'label'			=> false,
							'placeholder' 	=> 'Phone Number (optional)',
							'class'			=> array(
								'signup-form-group',
							),
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'college',
							'label'			=> false,
							'placeholder' 	=> 'College',
							'class'			=> array(
								'signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'organization',
							'label'			=> false,
							'placeholder' 	=> 'Fraternity/Sorority',
							'class'			=> array(
								'signup-form-group',
							),
							'required'		=> true
						)
					);
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'officer_position',
							'label'			=> false,
							'placeholder' 	=> 'Officer Position (optional)',
							'class'			=> array(
								'signup-form-group',
							),
						)
					);
					// echo OMH_HTML_UI_Input::factory(
					// 	array(
					// 		'input_id'		=> 'chapter_instagram',
					// 		'label'			=> false,
					// 		'placeholder' 	=> 'Chapter Instagram (optional)',
					// 		'class'			=> array(
					// 			'signup-form-group',
					// 		),
					// 	)
					// );

					// Submit
					echo OMH_HTML_UI_Button::factory(
						array(
							'color'		=> 'primary',
							'value'		=> 'submit',
							'form'		=> 'mh-signup-form',
							'class'		=> array(
								'signup-submit',
								'mh-forms-submit'
							),
							'label'		=> 'Submit',
						)
					);
				?>
			</form>
			<div id="mh-dashboard-notices"></div>
		</div>
		<?php
	}

	public static function house_user_activation_form( $atts ) {

		$activation_key = $_GET['key'] ?? false;
		$chapter_slug = get_query_var( 'chapters' );
		$throw_error = false;

		ob_start();

		if( !$activation_key || !$chapter_slug ) {

			if( !$activation_key ) {

				echo OMH_HTML_UI_Alert::factory(
					array(
						'color'			=> 'warning',
						'text'			=> 'Activation Key is required. Please contact an administrator if you continue to see this error.',
						'dismissible'	=> false
					)
				);
			}

			if( !$chapter_slug ) {

				echo OMH_HTML_UI_Alert::factory(
					array(
						'color'			=> 'warning',
						'text'			=> 'Chapter not found. Please contact an administrator if you continue to see this error.',
						'dismissible'	=> false
					)
				);
			}

			$throw_error = true;
		} else {

			if( !( $user = OMH_User::get_by_activation_key( $activation_key, 'house_admin' ) ) ) {
				$user = OMH_User::get_by_activation_key( $activation_key, 'house_member' );
			}

			if( $user ) {
			
				$chapter = OMH()->chapter_factory->get_by_url_structure( $chapter_slug );

				if( $user->has_role( 'house_admin' ) ) {
					require( OMH_TEMPLATE_PATH . 'house-admin-activation-form.php' );
				} elseif( $user->has_role( 'house_member' ) ) {
					require( OMH_TEMPLATE_PATH . 'house-member-activation-form.php' );
				}
			} else {

				echo OMH_HTML_UI_Alert::factory(
					array(
						'color'			=> 'warning',
						'text'			=> 'Invalid activation key.',
						'dismissible'	=> false
					)
				);

				$throw_error = true;
			}
		}

		if( $throw_error ) {

			echo OMH_HTML_UI_Button::factory(
				array(
					'color'			=> 'primary',
					'label'			=> 'Go back to Merch.House',
					'href'			=> home_url()
				)
			);
		}

		return ob_get_clean();
	}

	public static function house_admin_activation_form( $atts ) {

		$activation_key = $_GET['key'] ?? false;
		$chapter_slug = get_query_var( 'chapters' );

		if( $activation_key && $chapter_slug ):

			$users = get_users( 
				array(
					'role__in'		=> array( 'house_admin' ),
					'meta_key'		=> 'house_admin_activation_key',
					'meta_value'	=> $activation_key
				)
			);

			$chapter = OMH()->chapter_factory->get_by_url_structure( $chapter_slug );

			if( ( $user = !empty( $users ) ? current( $users ) : false ) ):

				$omh_user = get_user_by( 'id', $user->ID );
				?>
				<div class="mh-account-activation-wrapper">
					<h3>Activate your House Admin account for <?php echo $chapter->get_chapter_name(); ?></h3>
					<div id="mh-dashboard-notices"></div>
					<form id="mh-account-activation-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="account_activation" data-form-nonce="<?php echo wp_create_nonce( 'account-activation' ); ?>">

						<div class="col-md-6">
						<?php
							echo OMH_HTML_UI_Input::factory(
								array(
									'input_id'		=> 'first_name',
									'label' 		=> 'First Name',
									'value'			=> $omh_user->first_name,
									'required'		=> true
								)
							);
						?>
						</div>

						<div class="col-md-6">
						<?php
							echo OMH_HTML_UI_Input::factory(
								array(
									'input_id'		=> 'last_name',
									'label'		 	=> 'Last Name',
									'value'			=> $omh_user->last_name,
									'required'		=> true
								)
							);
						?>
						</div>

						<div class="col-md-6">
						<?php
							echo OMH_HTML_UI_Input::factory(
								array(
									'input_type'	=> 'password',
									'input_id'		=> 'new_password',
									'label'		 	=> 'Password',
									'input_attrs'	=> array(
										'pattern'					=> '(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9$@$!%*#?&]{6,}',
										'data-invalid-msg-pattern'	=> OMH_Frontend::password_hint()
									),
									'required'		=> true,
									'match_field'	=> 'confirm_new_password'
								)
							);
						?>
						</div>

						<div class="col-md-6">
						<?php
							echo OMH_HTML_UI_Input::factory(
								array(
									'input_type'	=> 'password',
									'input_id'		=> 'confirm_new_password',
									'label'		 	=> 'Confirm Password',
									'input_attrs'	=> array(
										'pattern'					=> '(?=.*[A-Za-z])(?=.*[0-9])[A-Za-z0-9$@$!%*#?&]{6,}',
										'data-invalid-msg-pattern'	=> OMH_Frontend::password_hint()
									),
									'required'		=> true,
									'match_field'	=> 'new_password'
								)
							);
						?>
						</div>

						<div class="col-md-6">
						<?php
							echo OMH_HTML_UI_Input::factory(
								array(
									'input_id'		=> 'phone_number',
									'label'		 	=> 'Phone Number',
									'required'		=> false
								)
							);
						?>
						</div>
						
						<div class="col-md-12">
						<?php
							echo OMH_HTML_UI_Input::factory(
								array(
									'input_id'		=> 'agree_to_terms',
									'input_type'	=> 'checkbox',
									'label'			=> 'I agree to the Merch House <a target="_blank" href="https://merch.house/terms-and-conditions/#chapter-agreement">Chapter Agreement</a>',
									'required'		=> true,
								)
							);
						?>
						</div>

						<?php
							echo OMH_HTML_UI_Input::factory(
								array(
									'input_id'		=> 'user_id',
									'input_type'	=> 'hidden',
									'class'			=> 'd-none',
									'value'			=> $omh_user->ID
								)
							);
						?>

						<?php
							// Submit
							echo OMH_HTML_UI_Button::factory(
								array(
									'color'		=> 'primary',
									'value'		=> 'submit',
									'form'		=> 'mh-account-activation-form',
									'class'		=> array(
										'account-activation-submit',
										'mh-forms-submit'
									),
									'label'		=> 'Submit',
								)
							);
						?>
					</form>
				</div>
				<?php
			else:
				echo OMH_HTML_UI_Alert::factory(
					array(
						'color'			=> 'warning',
						'text'			=> 'Invalid activation key.',
						'dismissible'	=> false
					)
				);

				echo OMH_HTML_UI_Button::factory(
					array(
						'color'			=> 'primary',
						'label'			=> 'Go back to Merch.House',
						'href'			=> home_url()
					)
				);
			endif;
		else:
			echo OMH_HTML_UI_Alert::factory(
				array(
					'color'			=> 'warning',
					'text'			=> 'Invalid activation key.',
					'dismissible'	=> false
				)
			);

			echo OMH_HTML_UI_Button::factory(
				array(
					'color'			=> 'primary',
					'label'			=> 'Go back to Merch.House',
					'href'			=> home_url()
				)
			);
		?>

		<?php
		endif;
	}

	public static function organization_start_design_form($atts) {
	?>
	<div class="mh-organization-start-design-wrapper">
		<form id="mh-organization-start-design-form" class="mh-form form-not-initialized needs-validation" data-ajax-action="organization_start_design_form" data-form-nonce="<?php echo wp_create_nonce( 'organization-start-design-form' ); ?>" data-submit-type="form">
			<?php
				echo OMH_HTML_UI_Input::factory(
					array(
						'input_id'		=> 'osd_name',
						'label'			=> false,
						'placeholder' 	=> 'Name',
						'class'			=> array(
							'organization-start-design-form-group',
						),
						'required'		=> true
					)
				);
				echo OMH_HTML_UI_Input::factory(
					array(
						'input_type'	=> 'email',
						'input_id'		=> 'osd_email',
						'label'			=> false,
						'placeholder' 	=> 'Email Address',
						'class'			=> array(
							'organization-start-design-form-group',
						),
						'required'		=> true
					)
				);
				echo OMH_HTML_UI_Input::factory(
					array(
						'input_id'		=> 'osd_phone_number',
						'label'			=> false,
						'placeholder' 	=> 'Phone Number (optional)',
						'class'			=> array(
							'organization-start-design-form-group',
						),
					)
				);
				echo OMH_HTML_UI_Input::factory(
					array(
						'input_id'		=> 'osd_chapter',
						'label'			=> false,
						'placeholder' 	=> 'Chapter Name',
						'class'			=> array(
							'organization-start-design-form-group',
						),
						'required'		=> true
					)
				);

				?>
				<label class="omh-form-label">Leave comments for our talented designers. The more detailed, the closer the design will be to your vision!</label>
				<?php
				echo OMH_HTML_UI_Textarea::factory(
					array(
						'input_id'		=> 'osd_description',
						'label'			=> false,
						'contents'		=> array(
							'input'			=> array(
								'attrs'			=> array(
									'rows'		=> 5,
									'cols'		=> 50
								)
							)
						)
					)
				);
				?>
				
	
				<?php
				$allowed_media_types = array(
					// JPG or JPEG
					'application/jpeg',
					'image/jpeg',
					'.jpeg',
					'application/jpg',
					'image/jpg',
					'.jpg',

					// PDF
					'application/pdf',
					'.pdf',

					// AI
					'application/illustrator',
					'application/postscript',
					'.ai',

					// PSD
					'application/photoshop',
					'image/vnd.adobe.photoshop',
					'image/x-psd',
					'.psd',

					// PNG
					'image/png',
					'.png',

					// SVG
					'image/svg+xml',
					'.svg',

					// TIFF
					'image/tiff',
					'.tiff',

					// GIF
					'image/gif',
					'.gif',
				);
				?>
	
				<div class="form-group form-group-file-img" style="display:none;">
					<div class="row">
						<div class="col-md-12">
							<div class="custom-file">
								<label class="omh-form-label">Optional: Design Images / Inspiration</label>
								<div class="input-group mb-2">
									<div class="custom-file custom-file-hide-default-label">
										<input type="file" data-omh-field-type="file" class="custom-file-input never-input never-changed orig-value omh-field-input" name="osd_file_upload_1" id="osd_file_upload_1" accept="<?php echo implode(', ', $allowed_media_types); ?>" />
										<label class="custom-file-label">Choose New File</label>
									</div>
									<div class="input-group-append">
										<button class="btn btn-danger input-reset-btn" type="button" for="osd_file_upload_1" style="display:none;"><i class="fa fa-close"></i></button>
										<button class="btn btn-secondary input-focus-btn" type="button" for="osd_file_upload_1">Browse</button>
									</div>
								</div>

								<div class="input-group mb-2">
									<div class="custom-file custom-file-hide-default-label">
										<input type="file" data-omh-field-type="file" class="custom-file-input omh-field-input never-input never-changed orig-value" name="osd_file_upload_2" id="osd_file_upload_2" accept="<?php echo implode(', ', $allowed_media_types); ?>" />
										<label class="custom-file-label">Choose New File</label>
									</div>
									<div class="input-group-append">
										<button class="btn btn-danger input-reset-btn" type="button" for="osd_file_upload_2" style="display:none;"><i class="fa fa-close"></i></button>
										<button class="btn btn-secondary input-focus-btn" type="button" for="osd_file_upload_2">Browse</button>
									</div>
								</div>
							</div>
							<small class="form-text text-muted">You can upload .jpg .jpeg .psd .ai .pdf .png .gif or .tiff files.</small>
						</div>
					</div>
				</div>
				
				<?php 
					echo OMH_HTML_UI_Input::factory(
						array(
							'input_id'		=> 'osd_selection_id',
							'label'			=> false,
							'input_attrs'	=> array(
								'style'	=> array(
									'display' => 'none'
								) 
							),
							'placeholder' 	=> 'Selection',
							'class'			=> array(
								'organization-start-design-form-group',
								),
							'required'		=> true,
						)
					);
				?>
				
				<?php 
					echo OMH_HTML_UI_Button::factory(
						array(
							'color'		=> 'primary',
							'value'		=> 'submit',
							'form'		=> 'mh-organization-start-design-form',
							'class'		=> array(
								'organization-start-design-submit',
								'mh-forms-submit'
							),
							'label'		=> 'Submit',
						)
					);
				?>
		</form>
		<div id="osd-notices"></div>
		<script type="text/javascript">
			jQuery( document ).ready(function($) {
				$('.mh-design-gallery-item input[type="radio"]').change(function(e) {

					var $selectedDesign = $('.mh-design-gallery-item input[type="radio"]:checked'); 
					var selectedDesign = $selectedDesign.first().val();

					$('input#osd_selection_id').val( selectedDesign );

					// Update Selected Design area
					var designDetails = {
						img: '',
						thumbs: '',
						title: '',
						descrip: ''
					};
					
					if( 'CUSTOM' == selectedDesign ) {

						designDetails.img = '';
						designDetails.title = 'Create Your Own';
						designDetails.descrip = 'Want a custom design created from scratch? Just fill out the form below and upload any images that help describe your vision!';
					}
					else {

						designDetails.img = $selectedDesign.siblings('label').children('img').clone();
						designDetails.title = selectedDesign;
						designDetails.descrip = 'We can customize this design for your chapter! Just let us know what you would like changed by filling out the form below. ';

						if( 'CHAPTER HOUSE' == selectedDesign ) {

							designDetails.descrip = 'We can create a custom sketched graphic of your chapter house! Just submit high quality images of your chapter house in the form below.';
						}
						else if( 'AMERICA' == selectedDesign ) {
							
							designDetails.descrip = 'We can customize this design for your chapter! Just let us know what you would like changed by filling out the form below. <br><br>Canadian chapters - we can change the flag to the Canada flag!';
						}
					}

					$('#mh-organization-start-design-form .form-group-file-img').toggle(('CUSTOM' == selectedDesign || 'CHAPTER HOUSE' == selectedDesign ));

					

					// Prepare thumbnails
					$selectedDesign.siblings('.mh-design-gallery-item__thumbnails').children('img').each(function(i, elem) {

						var $thumb = [
							'<div class="mh-design-gallery-item mh-design-gallery-item--thumb">',
								'<input type="radio" name="designGalleryThumb" id="designGalleryThumb' + i + '" value="thumb' + i + '" style="display: none;">',
							 	'<label class="mh-design-gallery-item__label" for="designGalleryThumb' + i + '">',
							 		'<img src="' + $(this).attr('src') +'">',
								'</label>',
							'</div>'].join('');

						designDetails.thumbs += $thumb;
					});


					// Set values
					$('.mh-design-gallery-selected-design__img').html(designDetails.img);
					$('.mh-design-gallery-selected-design__thumbnails').html(designDetails.thumbs);
					$('.mh-design-gallery-selected-design__title').html(designDetails.title);
					$('.mh-design-gallery-selected-design__descrip').html(designDetails.descrip);


					// Maybe initialize thumbnails
					if( designDetails.thumbs ) {

						var $thumbRadios = $('.mh-design-gallery-selected-design__thumbnails').find('input[type="radio"]');

						// Select first thumb
						$thumbRadios.first().prop('checked', true);

						$thumbRadios.change(function() {
							
							var $thumbImg = $thumbRadios.filter(':checked').first().siblings('label').children('img');

							$('.mh-design-gallery-selected-design__img img').attr('src', $thumbImg.attr('src') );
						})
					}



					// Scroll to selected design / submit form row
					nectar_scrollToY( jQuery('#mh-design-gallery-form-row').offset().top - $('#header-outer').outerHeight() - $('.woocommerce-store-notice').outerHeight() );

					// var changeID = e.target.id;
					// $('input[name=selection_id]').val(changeID);
					// var test = $("label[for='" + changeID + "']");
				});

				/*helper function to scroll the page in an animated manner*/
				function nectar_scrollToY(scrollTargetY, speed, easing) {
					
					var scrollY = window.scrollY || document.documentElement.scrollTop,
					scrollTargetY = scrollTargetY || 0,
					speed = speed || 2000,
					easing = easing || 'easeInOutQuint',
					currentTime = 0;
					
					var time = Math.max(.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, .8));
					
					
					var easingEquations = {
					easeInOutQuint: function (pos) {
						if ((pos /= 0.5) < 1) {
						return 0.5 * Math.pow(pos, 5);
						}
						return 0.5 * (Math.pow((pos - 2), 5) + 2);
					}
					};
					
					
					function tick() {
					currentTime += 1 / 60;
					
					var p = currentTime / time;
					var t = easingEquations[easing](p);
					
					if (p < 1) {
						requestAnimationFrame(tick);
						
						window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
					} else {
						window.scrollTo(0, scrollTargetY);
					}
					}
					
					tick();
				}


				$(document).on('omh_form_after_submit', function(event, form, response) {
					if( 'success' == response.status ) {
						$('#mh-organization-start-design-form').hide();
						$('.mh-design-gallery-item input[type="radio"]').prop('disabled', true);
					}
				});
			});
		</script>
	</div>
	
	

<?php
	}
}
new OMH_Shortcodes;
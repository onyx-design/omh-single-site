<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Define the taxonomies used as well as any permalink settings
 */
class OMH_Taxonomies {

	/**
	 * Hooks
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'register_taxonomies' ), 0 );

		add_action( 'save_post_product', array( __CLASS__, 'save_chapter_meta_box' ) );

		// Permalinks
		// add_action( 'init', array( __CLASS__, 'rewrite_tags' ), 20 );
		add_filter( 'available_permalink_structure_tags', array( __CLASS__, 'permalink_structure_tags' ), 10, 1 );
		add_filter( 'post_type_link', array( __CLASS__, 'adjust_post_link' ), 10, 2 );
	}

	public static function register_taxonomies() {

		if( taxonomy_exists( 'chapters' ) ) {
			return;
		}

		$show_in_menu = true;

		do_action( 'omh_register_taxonomy' );

		/**
		 * Sales Type Taxonomy
		 */
		register_taxonomy(
			'sales_type',
			array( 
				'product',
				'order',
			),
			array(
				'label'				=> 'Sales Types',
				'hierarchical'		=> false,
				'show_ui'			=> $show_in_menu,
				'show_in_nav_menus'	=> $show_in_menu,
				'show_in_menu'		=> $show_in_menu,
				'query_var'			=> is_admin(),
				'rewrite'			=> false,
				'public'			=> false,
			)
		);

		/**
		 * Product Status Taxonomy
		 */
		register_taxonomy(
			'product_status',
			array(
				'product',
			),
			array(
				'label'				=> 'Product Statuses',
				'hierarchical'		=> false,
				'show_ui'			=> $show_in_menu,
				'show_in_nav_menus'	=> $show_in_menu,
				'show_in_menu'		=> $show_in_menu,
				'query_var'			=> is_admin(),
				'rewrite'			=> false,
				'public'			=> false,
			)
			);

		/**
		 * Chapters Taxonomy
		 */
		register_taxonomy(
			'chapters',
			array(
				'product',
			),
			array(
				'label'				=> 'Chapters',
				'labels'			=> array(
					'name'				=> 'Chapter',
					'singular_name'		=> 'Chapter',
					'menu_name'			=> 'Chapters',
					'search_items'		=> 'Search chapters',
					'all_items'			=> 'All chapters',
					'parent_item'		=> 'Parent chapter',
					'parent_item_colon'	=> 'Parent chapter:',
					'edit_item'			=> 'Edit chapter',
					'update_item'		=> 'Update chapter',
					'add_new_item'		=> 'Add new chapter',
					'new_item_name'		=> 'New chapter name',
					'not_found'			=> 'No chapters found',
				),
				'show_ui'			=> true,
				'show_in_menu'		=> $show_in_menu,
				'show_in_nav_menus'	=> true,
				'query_var'			=> true,
				'capabilities'		=> array(
					'manage_terms'		=> 'manage_chapter_terms',
					'edit_terms'		=> 'edit_chapter_terms',
					'delete_terms'		=> 'delete_chapter_terms',
					'assign_terms'		=> 'assign_chapter_terms',
				),
				// 'rewrite'               => array(
				// 	'slug'         => '/',
				// 	'with_front'   => false,
				// ),
				'show_admin_column'	=> true,
				'meta_box_cb'		=> array( __CLASS__, 'chapter_meta_box' ),
			)
		);

		/**
		 * Colleges Taxonomy
		 */
		register_taxonomy(
			'colleges',
			array(
				'product',
			),
			array(
				'hierarchical'		=> true,
				'label'				=> 'College',
				'labels'			=> array(
					'name'				=> 'College',
					'singular_name'		=> 'College',
					'menu_name'			=> 'Colleges',
					'search_items'		=> 'Search colleges',
					'all_items'			=> 'All colleges',
					'parent_item'		=> 'Parent college',
					'parent_item_colon'	=> 'Parent college:',
					'edit_item'			=> 'Edit college',
					'update_item'		=> 'Update college',
					'add_new_item'		=> 'Add new college',
					'new_item_name'		=> 'New college name',
					'not_found'			=> 'No colleges found',
				),
				'show_ui'			=> true,
				'show_in_menu'		=> $show_in_menu,
				'show_in_nav_menus'	=> true,
				'query_var'			=> true,
				'capabilities'		=> array(
					'manage_terms'		=> 'manage_college_terms',
					'edit_terms'		=> 'edit_college_terms',
					'delete_terms'		=> 'delete_college_terms',
					'assign_terms'		=> 'assign_college_terms',
				),
				'show_admin_column'	=> true,
				'meta_box_cb'		=> array( __CLASS__, 'college_meta_box' )
			)
		);

		/**
		 * Organizations Taxonomy
		 */
		register_taxonomy(
			'organizations',
			array(
				'product',
			),
			array(
				'hierarchical'		=> true,
				'label'				=> 'Organizations',
				'labels'			=> array(
					'name'				=> 'Organization',
					'singular_name'		=> 'Organization',
					'menu_name'			=> 'Organizations',
					'search_items'		=> 'Search organizations',
					'all_items'			=> 'All organizations',
					'parent_item'		=> 'Parent organization',
					'parent_item_colon'	=> 'Parent organization:',
					'edit_item'			=> 'Edit organization',
					'update_item'		=> 'Update organization',
					'add_new_item'		=> 'Add new organization',
					'new_item_name'		=> 'New organization name',
					'not_found'			=> 'No organizations found',
				),
				'show_ui'			=> true,
				'show_in_menu'		=> $show_in_menu,
				'show_in_nav_menus'	=> true,
				'query_var'			=> true,
				'capabilities'		=> array(
					'manage_terms'		=> 'manage_organization_terms',
					'edit_terms'		=> 'edit_organization_terms',
					'delete_terms'		=> 'delete_organization_terms',
					'assign_terms'		=> 'assign_organization_terms',
				),
				'show_admin_column'	=> true,
				'meta_box_cb'		=> array( __CLASS__, 'organization_meta_box' )
			)
		);

		do_action( 'omh_after_register_taxonomy' );
	}

	public static function chapter_meta_box( $post ) {

		$product = wc_get_product( $post );

		if( $product instanceof OMH_Product_Variable ) {
			?>
			<select class="select2-admin" disabled>
			<?php if( $product && $chapter = $product->get_chapter() ): ?>
				<option><?php echo $chapter->get_term()->name; ?></option>
			<?php else: ?>
				<option>No Organization set</option>
			<?php endif; ?>
				</select>
			<?php
		} else {
			?>
			<div class="omh-metabox-description">
				Chapter can only be setup on a Variable product.
			</div>
			<?php
		}
	}

	public static function organization_meta_box( $post ) {

		?>
			<div class="omh-metabox-description">
				Organization cannot be changed as it is pulled from the chapter
			</div>
		<?php

		$product = wc_get_product( $post );

		if( $product instanceof OMH_Product_Variable ): ?>
			<select class="select2-admin" disabled>
			<?php if( $product && $organization = $product->get_organization() ): ?>
				<option><?php echo $organization->get_term()->name; ?></option>
			<?php else: ?>
				<option>No Organization set</option>
			<?php endif; ?>
				</select>
			<?php
		endif;
	}

	public static function college_meta_box( $post ) {

		?>
			<div class="omh-metabox-description">
				College cannot be changed as it is pulled from the chapter
			</div>
		<?php

		$product = wc_get_product( $post );

		if( $product instanceof OMH_Product_Variable ): ?>
			<select class="select2-admin" disabled>
			<?php if( $product && $college = $product->get_college() ): ?>
				<option><?php echo $college->get_term()->name; ?></option>
			<?php else: ?>
				<option>No College set</option>
			<?php endif; ?>
				</select>
			<?php
		endif;
	}

	public static function save_chapter_meta_box( $post_id ) {

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if( !isset( $_POST['chapters'] ) ) {
			return;
		}

		$chapter = sanitize_text_field( $_POST['chapters'] );

		if( !empty( $chapter ) ) {

			if( $product = wc_get_product( $post_id ) ) {

				$product->set_chapter( $chapter );
			}
		}
	}

	/**
	 * Permalinks
	 **/

	/**
	 * Add our chapter tag to the permalink settings page
	 * 
	 * This allows the chapter tag to be used across WordPress from the
	 * permalinks settings page by adding "%chapter%" to a permalink
	 * 
	 * @tag Permalinks
	 * 
	 * @see 	OMH_Taxonomies::adjust_post_link()
	 * 
	 * @param 	array 	$tags
	 * 
	 * @return 	array 	$tags
	 */
	public static function permalink_structure_tags( $tags ) {

		$tags['chapter'] = 'chapter';

		return $tags;
	}

	/**
	 * Replace %chapter% in the URL with the Product's Chapter
	 * 
	 * This function replaces the "%chapter%" tag when it's being
	 * used on the permalinks settings page with the current products's
	 * Chapter
	 * 
	 * @tag Permalinks
	 * 
	 * @see 	OMH_Taxonomies::permalink_strcture_tags()
	 * 
	 * @param 	string 	$permalink 
	 * @param 	WP_Post $post
	 * 
	 * @return 	string	$permalink
	 */
	public static function adjust_post_link( $permalink, $post ) {

		if ( 'product' !== $post->post_type ) {
			return $permalink;
		}

		if( false === strpos( $permalink, '%chapter%' ) ) {
			return $permalink;
		}

		$product = wc_get_product( $post );

		if( ( $product instanceof OMH_Product_Variable ) && ( $chapter = $product->get_chapter() ) ) {
			$permalink = str_replace( '%chapter%', $chapter->get_url_structure(), $permalink );
		}

		return $permalink;
	}
}

OMH_Taxonomies::init();
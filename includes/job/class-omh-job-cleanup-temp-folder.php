<?php
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_Job_Cleanup_Temp_Folder extends OMH_Job {

	public static $job_type = 'omh_job_cleanup_temp_folder';

	public static function run() {
		global $wpdb;
		$delete_time_diff = strtotime("-1 month");

		$delete_query = "DELETE FROM {$wpdb->prefix}omh_temp_uploads WHERE meta_created < '" . gmdate("Y-m-d", $delete_time_diff) . "'";
		$wpdb->query($delete_query);

		$temp_dir = OMH_TEMP_UPLOAD_DIR;
		$ignored = array('.', '..', '.svn', '.htaccess');
		$files_deleted = 0;

		foreach (scandir($temp_dir) as $i => $file) {
			if (in_array($file, $ignored)) continue;
			// Check if almost at timeout
			if($i % 10 == 0) {
				if(50 <= ox_exec_time()) {
					omh_dev_log( "JOB ".static::$job_type." exited to avoid timeout. {$i} files checked, {$files_deleted} files deleted" );
					return;
				}
			}
			$file_timestamp = filemtime($temp_dir . '/' . $file);
			$file_name = $file;
			// Deletes file if older than set time
			if ($file_timestamp < $delete_time_diff){
				$files_deleted++;
				wp_delete_file($temp_dir . '/' . $file_name );
			}
		}

		omh_dev_log( "JOB ".static::$job_type." finished in ".ox_exec_time()."s. {$i} files checked, {$files_deleted} files deleted" );
		return;
	}
}
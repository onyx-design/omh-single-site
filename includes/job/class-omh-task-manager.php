<?php
defined( 'ABSPATH' ) || exit;

class OMH_Task_Manager {

	/********************************
	 *			 Schema				*
	 ********************************/
	protected $job_list = array(
		'omh_job_cleanup_temp_folder'	=> array(
			'start'				=> 'midnight tonight',
			// 'schedule'			=> '0 0 * * 3',
			'interval'			=> '604800',
			'class'				=> 'OMH_Job_Cleanup_Temp_Folder',
		)
	);

	/********************************
	 *			 Functions			*
	 ********************************/
	public function __construct(){ 
		foreach( $this->job_list as $job_name => $job_meta ) {
			if(false === as_next_scheduled_action($job_name)) {
				add_action($job_name, $job_meta['class']::run(), 10);
				WC()->queue()->schedule_recurring(strtotime( $job_meta['start'] ), $job_meta['interval'], $job_name );
			}
		}
	}

	// Wrapper function to unschedule a cron job
	public function unschedule($action) {
		as_unschedule_action($action);
	}

	// Wrapper Function to get next
	public function get_next_task_time($action) {
		return as_next_scheduled_action($action);
	}
}
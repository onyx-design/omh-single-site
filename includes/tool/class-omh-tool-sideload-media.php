<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

class OMH_Tool_Sideload_Media {

	static $prepared = false;

	const image_defaults = array(
		'id'			=> 0,
		'is_temp'		=> false,
		'src'			=> '',
		'thumbnail_src'	=> ''
	);

	public static function prepare_sideload() {

		if( !static::$prepared ) {

			// Gives us access to the download_url() and wp_handle_sideload() functions.
			require_once( ABSPATH . 'wp-admin/includes/file.php' );

			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );

			static::$prepared = true;
		}
	}

	public static function sanitize_url( $url, $secure = true ) {

		$protocol = $secure ? 'https://' : 'http://';

		if( ( 0 !== strpos( $url, $protocol ) ) ) {

			$url = $protocol . str_replace( array( '://', '//' ), '', $url );
		}

		return $url;
	}

	public static function validate_url( $url ) {

		if( !$url 
			|| !filter_var( $url, FILTER_SANITIZE_URL )
		) {
			return false;
		}

		return true;
	}

	public static function insert_media_file_from_url( $file_url ) {

		if( !isset( $file_url ) || !$file_url ) {
			return false;
		}

		$file_url = static::sanitize_url( $file_url, is_ssl() );

		if( !static::validate_url( $file_url ) ) {

			return false;
		}

		$file_type_data = wp_check_filetype( $file_url, null );
		$file_type = $file_type_data['type'];

		$insert_data = array(
			'file_url'	=> $file_url,
			'file_type'	=> $file_type
		);

		if( $inserted_attachment_id = static::insert_media_file_into_site( $insert_data ) ) {

			return $inserted_attachment_id;
		}

		return false;
	}

	public static function insert_media_file_from_temp( $temp_id ) {

		if( empty( $temp_id )
			|| !$temporary_file = OMH()->temp_upload_factory->read( $temp_id ) 
		) {
			return false;
		}

		$file_url = static::sanitize_url( $temporary_file->get_file_url(), is_ssl() );
		if( !static::validate_url( $file_url ) ) {
			return false;
		}

		$file_type_data = wp_check_filetype( $temporary_file->get_file_name(), null );
		$file_type = $file_type_data['type'];

		$insert_data = array(
			'file_url'	=> $file_url,
			'file_type'	=> $file_type
		);

		if( $inserted_attachment_id = static::insert_media_file_into_site( $insert_data ) ) {

			return array(
				'id'			=> $inserted_attachment_id,
				'is_temp'		=> false,
				'src'			=> wp_get_attachment_url( $inserted_attachment_id ),
				'thumbnail_src'	=> wp_get_attachment_thumb_url( $inserted_attachment_id )
			);
		}

		return false;
	}

	public static function get_media_image_object( $media_id ) {

		if( $media_id && wp_attachment_is_image( $media_id ) ) {

			return array(
				'id'			=> $media_id,
				'is_temp'		=> false,
				'src'			=> wp_get_attachment_url( $media_id ),
				'thumbnail_src'	=> wp_get_attachment_thumb_url( $media_id )
			);
		}
		else {
			return static::image_defaults;
		}
	}

	public static function insert_media_file_into_site( $media_file_data = array() ) {

		if( !isset( $media_file_data['file_url'], $media_file_data['file_type'] ) 
			|| !static::validate_url( $media_file_data['file_url'] )
		) {
			return false;
		}

		static::prepare_sideload();

		$media_file_url = $media_file_data['file_url'];
		$media_file_type = $media_file_data['file_type'];

		$sideload_result = static::sideload_media_file( $media_file_url, $media_file_type );

		if( !$sideload_result || !empty( $sideload_result['error'] ) ) {
			return false;
		}

		$new_file_path = $sideload_result['file'];
		$new_file_type = $sideload_result['type'];

		$inserted_attachment_id = static::insert_media_file( $new_file_path, $new_file_type );

		return $inserted_attachment_id;
	}

	/**
	 * Sideload Media File into the Uploads Directory
	 * 
	 * @param 	string 	$file_url 
	 * @param 	string 	$file_type 
	 * @param 	int 	$timeout_seconds
	 * 
	 * @return 	
	 */
	public static function sideload_media_file( $file_url = '', $file_type = '', $timeout_seconds = 5 ) {

		if( !$file_url || !$file_type ) {
			return false;
		}

		static::prepare_sideload();

		$temp_file = download_url( $file_url, $timeout_seconds );

		if( is_wp_error( $temp_file ) ) {
			return false;
		}

		// Array based on $_FILE 
		// @see: https://codex.wordpress.org/Function_Reference/wp_handle_sideload
		$file = array(
			'name'		=> basename( $file_url ),
			'type'		=> $file_type,
			'tmp_name'	=> $temp_file,
			'error'		=> 0,
			'size'		=> filesize( $temp_file ),
		);

		$overrides = array(
			// Tells WordPress to not look for the POST form
			// fields that would normally be present, default is true,
			// we downloaded the file from a remote server, so there
			// will be no form fields.
			'test_form'		=> false,

			// Setting this to false lets WordPress allow empty files – not recommended.
			'test_size'		=> true,

			// A properly uploaded file will pass this test.
			// There should be no reason to override this one.
			'test_upload'	=> true
		);

		$sideloaded_file = wp_handle_sideload( $file, $overrides );

		return $sideloaded_file;
	}

	public static function insert_media_file( $file_path = '', $file_type = '' ) {

		if( !$file_path || !$file_type ) {
			return false;
		}

		static::prepare_sideload();

		$wp_upload_dir = wp_upload_dir();

		$attachment_data = array(
			'guid'				=> $wp_upload_dir['url'] . '/' . basename( $file_path ),
			'post_mime_type'	=> $file_type,
			'post_title'		=> preg_replace( '/\.[^.]+$/', '', basename( $file_path ) ),
			'post_content'		=> '',
			'post_status'		=> 'inherit',
		);

		$inserted_attachment_id = wp_insert_attachment( $attachment_data, $file_path );
		$inserted_attachment_path = get_attached_file( $inserted_attachment_id );

		static::update_inserted_attachment_metadata( $inserted_attachment_id, $inserted_attachment_path );

		return $inserted_attachment_id;
	}

	private static function update_inserted_attachment_metadata( $inserted_attachment_id, $file_path ) {

		static::prepare_sideload();

		// Generate metadata for the attachment and update the database record.
		$attach_data = wp_generate_attachment_metadata( $inserted_attachment_id, $file_path );
		wp_update_attachment_metadata( $inserted_attachment_id, $attach_data );
	}
}
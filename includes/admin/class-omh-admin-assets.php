<?php
defined( 'ABSPATH' ) || exit;

class OMH_Admin_Assets {

	public function __construct() {
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
	}

	public function admin_styles() {

		wp_enqueue_style( 'omh-admin', OMH()->plugin_url() . '/dist/css/admin.css' );
	}

	public function admin_scripts() {

		wp_enqueue_script( 'omh-admin', OMH()->plugin_url() . '/dist/js/admin.js', array( 'jquery' ) );
	}
}
return new OMH_Admin_Assets();
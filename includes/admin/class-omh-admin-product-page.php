<?php
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_Admin_Product_Page {

	public function __construct() {

		add_action('admin_print_scripts', array( $this, 'admin_scripts' ) );
		add_action('admin_print_styles', array( $this,'admin_styles' ) );

		// Add Download Sales CSV Metabox
		// add_action( 'add_meta_boxes', array( $this, 'metabox_download_product_sales' ), 10, 2 );
	}

	function admin_scripts() {
		wp_enqueue_script( 'thickbox' );
	}

	function admin_styles() {
		wp_enqueue_style( 'thickbox' );
	}

	public function metabox_download_product_sales( $post_type, $post ) {

		if( 'product' == $post_type ) {

			add_meta_box(
				'download_product_sales_metabox',
				'Download Sales Data',
				array( $this, 'download_product_sales_metabox_html' ),
				'product',
				'side',
				'high'
			);
		}
	}

	public function download_product_sales_metabox_html() {
		global $post;
		?>
		<span id="omh-download-product-sales-btn" class="button button-primary" style="text-align:center">Download CSV</span>

		<script type="text/javascript">
			jQuery( function( $ ) {
				$(document).ready(function() {

					$('#omh-download-product-sales-btn').click(function(){

						var ajax_data = {
							'action'		: 'omh_generate_product_sales_csv',
							'security'		: '<?php echo wp_create_nonce( 'generate-product-sales-csv' ); ?>',
							'product_id'	: <?php echo $post->ID; ?>
						};

						var ajax_args = {
							url 	: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
							type 	: "POST",
							data 	: ajax_data,
							cache 	: false,
							dataType: "json",
							async 	: false,
							complete: function( response, status ) {

								// Download CSV
								var downloadWindow = window.open( response.responseJSON.data );
								downloadWindow.location;
								// document.location.href = response.responseJSON.data;
							}
						};

						jQuery.ajax( ajax_args );
					});
				});
			});
		</script>
		<?php
	}
}
return new OMH_Admin_Product_Page();
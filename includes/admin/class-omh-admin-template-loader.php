<?php
defined( 'ABSPATH' ) || exit;

class OMH_Admin_Template_Loader {

	public function __construct() {
		add_filter( 'theme_page_templates', array( $this, 'add_custom_page_templates_to_admin' ), 10, 4 );
		add_filter( 'woocommerce_locate_template', array( __CLASS__, 'wc_email_template_loader' ), 10, 3 );
	}

	public function add_custom_page_templates_to_admin( $post_templates, $wp_theme, $post, $post_type ) {

		foreach( OMH()->page_templates() as $template ) {

			$post_templates[ $template ] = __( $this->get_template_name_from_template( $template ) );
		}

		return $post_templates;
	}

	private function get_template_name_from_template( $template ) {

		return ucwords( trim( str_replace( array( 'template', '-', '.php' ), ' ', strtolower( $template ) ) ) );
	}

	public static function wc_email_template_loader( $template, $template_name, $template_path ) {

		// Check for email templates in our plugin
		if( 0 === strpos( $template_name, 'emails/' ) ) {

			$basename = basename( $template );

			if( file_exists( OMH_TEMPLATE_PATH . "emails/{$basename}" ) ) {
				$template = OMH_TEMPLATE_PATH . "emails/{$basename}";
			}
		}

		return $template;
	}
}
return new OMH_Admin_Template_Loader();
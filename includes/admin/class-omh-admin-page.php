<?php
defined('ABSPATH') || exit;

class OMH_Admin_Page
{

	public function __construct()
	{

		add_action('admin_menu', array($this, 'add_omh_admin_pages'));
		add_action('admin_post_export_users', array($this, 'export_users'));
	}

	public function add_omh_admin_pages()
	{

		add_menu_page('Merch House', 'Merch House', 'manage_options', 'omh-admin-page', array($this, 'omh_admin_page_content'), 'dashicons-admin-home');

		if (3 == get_current_user_id()) {
			add_menu_page('MH Sandbox', 'MH Sandbox', 'manage_options', 'omh-admin-sandbox', array($this, 'omh_admin_page_sandbox'), 'dashicons-admin-tools');
		}
	}

	public function omh_admin_page_content()
	{

		$allow_submit = false;
		if ($last_user_meta = isset($_GET['last_user_meta']) ? $_GET['last_user_meta'] : false) {
			$allow_submit = true;
		}

		if (isset($_POST['submit']) || $allow_submit) {

			$action = $_POST['submit'];

			if (!wp_verify_nonce($_POST['omh_admin_page_nonce'], 'omh-admin-page-save') && !$allow_submit) {
				$this->add_admin_notice('Security check failed.', true);
			} else {

				if ('Import Master Greek Data from Table' === $action) {
					global $wpdb;

					$master_greek_data_table = 'mh_master_greek_data';
					$master_greek_data_query = "
						SELECT *
						FROM $master_greek_data_table
						-- WHERE ID > 1455
							-- AND ID < 1465
					";
					$master_greek_data = $wpdb->get_results(
						$master_greek_data_query
					);

					foreach ($master_greek_data as $greek_chapter) {

						$college = null;
						$organization = null;
						$chapter = null;

						$college = OMH()->college_factory->query(
							array(
								'posts_per_page'	=> 1,
								'where'				=> array(
									array(
										'name'		=> 'college_name',
										'value'		=> $greek_chapter->full_college_name
									)
								)
							)
						);

						$college_map = (object) array(
							'college_name'		=> $greek_chapter->full_college_name,
							'college_shortname'	=> $greek_chapter->college_short_name,
							'college_letters'	=> $greek_chapter->college_abbreviation,
							'city'				=> $greek_chapter->city,
							'state'				=> $greek_chapter->state,
						);

						if (!$college) {

							$college = new OMH_Model_College($college_map);
						}

						if ($college->save()) {

							$organization = OMH()->organization_factory->query(
								array(
									'posts_per_page'	=> 1,
									'where'				=> array(
										array(
											'name'		=> 'org_name',
											'value'		=> $greek_chapter->greek_org_full_name
										)
									)
								)
							);

							$org_map = (object) array(
								'org_name'		=> $greek_chapter->greek_org_full_name,
								'org_shortname'	=> $greek_chapter->greek_org_short_name,
								'org_letters'	=> $greek_chapter->greek_org_letters,
							);

							if (!$organization) {

								$organization = new OMH_Model_Organization($org_map);
							}

							if ($organization->save()) {

								$chapter = OMH()->chapter_factory->query(
									array(
										'posts_per_page'	=> 1,
										'where'				=> array(
											array(
												'name'		=> 'org_id',
												'value'		=> $organization->get_id()
											),
											array(
												'name'		=> 'college_id',
												'value'		=> $college->get_id()
											)
										)
									)
								);

								$chapter_map = (object) array(
									'org_id'		=> $organization->get_id(),
									'college_id'	=> $college->get_id(),
									'chapter_name'	=> $college->get_college_shortname() . ' ' . $organization->get_org_shortname(),
									'url_structure'	=> $greek_chapter->url_structure ?: strtolower(str_replace(' ', '', $college->get_college_shortname() . $organization->get_org_shortname())),
									'status'		=> $greek_chapter->status,
								);

								if (!$chapter) {

									$chapter = new OMH_Model_Chapter($chapter_map);
								}

								if ($chapter->save()) {
									// Success!
								} else {
									$this->add_admin_notice('There was a problem creating or updating chapter: ' . $greek_chapter->college_short_name . ' ' . $greek_chapter->greek_org_short_name, true);
								}
							} else {
								$this->add_admin_notice('There was a problem creating or updating organization: ' . $greek_chapter->greek_org_full_name, true);
							}
						} else {
							$this->add_admin_notice('There was a problem creating or updating college: ' . $greek_chapter->full_college_name, true);
						}

						// break;
					}
				}

				// This is for building the new User meta from Single Site to Multisite
				if ('Generate User Meta' === $action) {
					global $wpdb;

					$user_roles = array();
					$user_meta = array();

					$chapter_data = $wpdb->get_results(
						"
							SELECT wp_site_id, subdomain
							FROM ss_sub_sites
						",
						OBJECT_K
					);

					// Loop over user meta and get all meta for wp_%_capabilities
					$user_capabilities = $wpdb->get_results(
						"
							SELECT user_id, meta_key, meta_value
							FROM ss_usermeta
							WHERE meta_key LIKE 'wp_%_capabilities'
						"
					);

					foreach ($user_capabilities as $user_capability) {

						$site_id = str_replace(array('wp_', '_capabilities'), '', $user_capability->meta_key);
						$chapter_subdomain = isset($chapter_data[$site_id]) ? $chapter_data[$site_id]->subdomain : false;

						if ($chapter_subdomain) {
							$user_roles[$user_capability->user_id][$chapter_subdomain] = array_keys(unserialize($user_capability->meta_value));
						}
					}

					foreach ($user_roles as $user_id => $user_chapters) {

						$chapter_url_structure = key($user_chapters);
						$role = reset(reset($user_chapters));

						if ($role === 'house_member') {
							$role = 'customer,house_member';
						}

						$user_meta[$user_id] = array(
							'chapter_url_structure'	=> $chapter_url_structure,
							'role'					=> $role
						);
					}

					$user_role_csv = fopen(OMH_TEMPLATE_PATH . 'admin/user_chapter_roles.csv', 'w');

					fputcsv($user_role_csv, array('user_id', 'chapter_structure', 'roles'));

					foreach ($user_meta as $user_id => $meta) {

						fputcsv($user_role_csv, array($user_id, $meta['chapter_url_structure'], $meta['role']));
					}

					fclose($user_role_csv);
				}

				if ('Import Generated User Meta' === $action || $last_user_meta) {

					if (file_exists(OMH_TEMPLATE_PATH . 'admin/user_chapter_roles.csv')) {

						$user_role_csv = fopen(OMH_TEMPLATE_PATH . 'admin/user_chapter_roles.csv', 'r');

						$headers = true;
						while (($user_meta_row = fgetcsv($user_role_csv)) !== false) {

							if ($headers) {
								$headers = false;
								continue;
							}

							$user_id = $user_meta_row[0];

							if ($last_user_meta && ($last_user_meta > $user_id)) {
								continue;
							}

							$chapter_url_structure = $user_meta_row[1];
							$roles = explode(',', $user_meta_row[2]);

							if ($user = get_user_by('ID', $user_id)) {

								$primary_chapter_id = $user->get_primary_chapter_id(true);

								// Ignore already set and admins
								// @note If the user's Chapter ID is set to 1 (Main) we want to overwrite that
								if (
									$user->has_role('administrator')
									|| ($primary_chapter_id && (1 != $primary_chapter_id))
								) {
									continue;
								}

								foreach ($roles as $role) {
									$user->add_role($role);
								}

								// Get the chapter we're setting
								// If the chapter doesn't exist, set their role to customer
								if ($chapter = OMH()->chapter_factory->get_by_url_structure($chapter_url_structure)) {

									$user->set_primary_chapter_id($chapter->get_id());
								}

								if (1 == $user->get_primary_chapter_id(true)) {
									global $wpdb;

									$signup = $wpdb->get_row(
										"
											SELECT *
											FROM ss_signups
											WHERE user_email = '{$user->get_email()}'
										"
									);

									if ($signup) {
										$signup_meta = unserialize($signup->meta);

										if (isset($signup_meta['site_id'])) {

											$old_sub_site = $wpdb->get_row(
												"
													SELECT *
													FROM ss_sub_sites
													WHERE wp_site_id = {$signup_meta['site_id']}
												"
											);

											if ($chapter = OMH()->chapter_factory->get_by_url_structure($old_sub_site->subdomain)) {

												$user->set_primary_chapter_id($chapter->get_id());
											}
										}
									}
								}

								if (1 == $user->get_primary_chapter_id(true)) {
									$user->set_role('customer');
								}

								// Save the roles to custom meta just in case
								$user->update_meta('_mh_user_roles', $roles);

								if (45 <= ox_exec_time()) {
									$this->add_admin_notice('Stopped at User ID: ' . $user_id . '. <a href="' . admin_url('admin.php?page=omh-admin-page&last_user_meta=' . $user_id) . '">Continue?</a>');
									break;
								}
							}
						}

						fclose($user_role_csv);

						// unlink( OMH_TEMPLATE_PATH . 'admin/user_chapter_roles.csv' );
					} else {

						$this->add_admin_notice('Generated user meta file does not exist', true);
					}
				}

				// dev:todo Move this to an operation
				if ('Re-generate Product Search Strings' === $action) {

					$products = wc_get_products(array(
						'limit'		=> -1
					));

					foreach ($products as $product) {

						$search_string = $product->generate_search_string();

						update_post_meta($product->get_id(), '_search_string', $search_string);
					}
				}

				// dev:todo Move this to an operation
				if ('Re-generate Order Related Chapters' === $action) {

					$orders = wc_get_orders(array(
						'type'		=> 'shop_order',
						'limit'		=> -1
					));

					foreach ($orders as $order) {

						$related_chapters = $order->generate_related_chapters();

						update_post_meta($order->get_id(), '_related_chapters', $related_chapters);
					}
				}

				if ('Re-generate Order Related Products' === $action) {

					$orders = wc_get_orders(array(
						'type'		=> 'shop_order',
						'limit'		=> -1
					));

					foreach ($orders as $order) {

						$related_retail_products = $order->generate_related_retail_products();
						$related_campaign_products = $order->generate_related_campaign_products();

						if (!empty($related_retail_products)) {
							update_post_meta($order->get_id(), '_related_retail_products', $related_retail_products);
						}

						if (!empty($related_campaign_products)) {
							update_post_meta($order->get_id(), '_related_campaign_products', $related_campaign_products);
						}
					}
				}

				if ('Save Maintenance Mode Setting' === $action) {

					error_log(print_r($_POST, true));

					$enable_omh_maintenance_mode = ( (isset( $_POST['omh_maintenance_mode'] ) && 'omh_maintenance_mode' === $_POST['omh_maintenance_mode'] ) ? 1 : 0 );

					update_option('omh_maintenance_mode_enabled', $enable_omh_maintenance_mode, true);

					$option_status_check = get_option('omh_maintenance_mode_enabled');

					if ($option_status_check == $enable_omh_maintenance_mode) {

						$option_status_check = empty($option_status_check) ? 'Disabled' : 'Enabled';

						$this->add_admin_notice("OMH Maintenance Mode {$option_status_check}", false);
					} else {
						$this->add_admin_notice("Update OMH Maintenance Mode FAILED", true);
					}
				}
			}
		}

		?>
		<div class="omh-admin-wrapper">

			<?php
					if (isset($_POST['messages'])) {

						$messages = $_POST['messages'];

						foreach ($messages as $message) {

							$notice = $message['notice'];

							?>
					<div class="notice <?php echo $notice; ?> is-dismissible">
						<p><?php echo $message['message']; ?></p>
					</div>
			<?php
						}
					}
					?>

			<form autocomplete="off" method="POST" action="">
				<?php if (is_super_admin()) : ?>
					<h1>Super Admin Tools</h1>

					<h2>Activate Maintenance Page</h2>
					<span class="omh-option-description">
						Show a maintenance page to all non super-admin users. <br />You can still log in as a super admin by going to the WP login screen at /admin
					</span>
					<label class="omh-field-label" for="omh_maintenance_mode">
						<input type="checkbox" name="omh_maintenance_mode" id="omh_maintenance_mode" value="omh_maintenance_mode" <?php echo empty(get_option('omh_maintenance_mode_enabled')) ? '' : 'checked'; ?> />
						Enable OMH Maintenance Mode
					</label>
					<?php submit_button('Save Maintenance Mode Setting', 'secondary'); ?>

					<h2>User Export</h2>
					<table class="form-table omh-metabox-table">
						<tbody>
							<tr>
								<label class="omh-field-label" for="role-export-admin">
									<input type="checkbox" name="role-export[]" id="role-export-admin" value="administrator" />
									Admin
								</label>
								<br />
								<label class="omh-field-label" for="role-export-house-admin">
									<input type="checkbox" name="role-export[]" id="role-export-house-admin" value="house_admin" />
									House Admin
								</label>
								<br />
								<label class="omh-field-label" for="role-export-house-member">
									<input type="checkbox" name="role-export[]" id="role-export-house-member" value="house_member" />
									House Member
								</label>
								<br />
								<label class="omh-field-label" for="role-export-customer">
									<input type="checkbox" name="role-export[]" id="role-export-customer" value="customer" />
									Customer
								</label>
							</tr>
						</tbody>
					</table>

					<input type="submit" value="Export Users" name="submit" id="submit" class="button" formaction="<?php echo admin_url('admin-post.php?action=export_users'); ?>" formtarget="_blank" />

					<?php wp_nonce_field('omh-admin-page-save', 'omh_admin_page_nonce'); ?>

					<?php if (is_onyx_user()) : ?>
						<h2>Import Master Greek Data</h2>
						<span class="omh-option-description">
							Import the Master Greek Data from the "mh_master_greek_data" table.
						</span>
						<?php submit_button('Import Master Greek Data from Table', 'secondary'); ?>
					<?php endif; ?>

					<?php if (is_onyx_user()) : ?>
						<h2>User Meta Multisite to Single Site Generator</h2>
						<span class="omh-option-description">
							Generate user meta to associate multisite user with the new single site setup.
						</span>
						<?php submit_button('Generate User Meta', 'secondary'); ?>
					<?php endif; ?>

					<?php if (is_onyx_user() && file_exists(OMH_TEMPLATE_PATH . 'admin/user_chapter_roles.csv')) : ?>
						<h2>Import User Chapter Meta</h2>
						<span class="omh-option-description">
							Import the generated user meta from a csv located in the admin templates if it exists
						</span>
						<?php submit_button('Import Generated User Meta', 'Secondary'); ?>
					<?php endif; ?>

					<?php if (is_onyx_user()) : ?>
						<h2>Re-generate Product Search Strings</h2>
						<span class="omh-option-description">
							Re-generate meta on each product that is used for searching products
						</span>
						<?php submit_button('Re-generate Product Search Strings', 'secondary'); ?>
					<?php endif; ?>

					<?php if (is_onyx_user()) : ?>
						<h2>Re-generate Order Related Chapters</h2>
						<span class="omh-option-description">
							Re-generate meta on each order that is used for caching order's chapters
						</span>
						<?php submit_button('Re-generate Order Related Chapters', 'secondary'); ?>
					<?php endif; ?>

					<?php if (is_onyx_user()) : ?>
						<h2>Re-generate Order Related Products</h2>
						<span class="omh-option-description">
							Re-generate meta on each order that is used for caching order's products
						</span>
						<?php submit_button('Re-generate Order Related Products', 'secondary'); ?>
					<?php endif; ?>

				<?php endif; ?>
			</form>

		</div>
<?php
	}

	public function export_users()
	{

		// Only run this if the action is 'Export Users'
		if ('Export Users' === $_POST['submit']) {

			$user_roles = $_POST['role-export'] ?? array();

			$errors = false;

			if (!empty($user_roles)) {

				$csv_headers = array(
					'Name',
					'Email',
					'Roles',
					'Chapter'
				);

				$user_query_args = array(
					'role__in' 		=> $user_roles,
					'orderby'		=> 'ID',
					'order'			=> 'ASC',
				);

				$user_query = new WP_User_Query($user_query_args);

				if (!$user_query->get_total()) {

					$message = 'No results found for User query.';
					$errors = true;
				} else {

					$message = 'Successfully exported data for ' . $user_query->get_total() . _n('user', 'users', $user_query->get_total()) . '.';
				}

				$filename = 'mh-user-export-' . date('Y-m-d-His') . '.csv';

				// Header Stuff
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment;filename=' . $filename);
				header('Pragma: no-cache');

				$output = @fopen('php://output', 'w');

				ob_start();

				fputcsv($output, $csv_headers);

				foreach ($user_query->get_results() as $user) {

					$omh_user = new OMH_User($user);
					$chapter = $omh_user->get_chapter(true);

					$csv_data = array(
						$omh_user->get_full_name(),
						$omh_user->get_email(),
						implode(',', $omh_user->roles),
						$chapter ? $chapter->get_chapter_name() : ''
					);

					fputcsv($output, $csv_data);
				}

				$csv_output = ob_get_clean();

				fclose($output);

				exit($csv_output);
			} else {

				$message = 'No roles selected to export.';
				$errors = true;
			}
		}
	}

	public function omh_admin_page_sandbox()
	{
		include OMH_TEMPLATE_PATH . '/admin/admin-page-omh-sandbox.php';
	}

	public function add_admin_notice($message = '', $errors = false)
	{

		$_POST['messages'][] = array(
			'message'	=> $message,
			'errors'	=> $errors,
			'notice'	=> $errors ? 'notice-error' : 'notice-success'
		);
	}
}
return new OMH_Admin_Page();

<?php
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_Admin {

	public function __construct() {

		// Register Nav Menus
		add_action( 'init', array( $this, 'register_nav_menus' ) );

		// Add new fields to the User Admin Table
		add_filter( 'manage_users_columns', array( $this, 'add_users_columns' ), 10 );
		add_filter( 'manage_users_custom_column', array( $this, 'add_users_custom_columns' ), 10, 3 );

		// Add new fields to the User Admin Profile
		add_action( 'show_user_profile', array( $this, 'add_users_profile_fields' ), 10, 1 );
		add_action( 'edit_user_profile', array( $this, 'add_users_profile_fields' ), 10, 1 );

		// Saving new fields in User Admin Profile
		add_action( 'personal_options_update', array( $this, 'save_users_profile_fields' ), 10, 1 );
		add_action( 'edit_user_profile_update', array( $this, 'save_users_profile_fields' ), 10, 1 );
	}

	public function register_nav_menus() {

		$register_nav_menu_locations = array(
			'mh_footer_menu'			=> 'Merch House Footer Menu',
		);

		register_nav_menus( $register_nav_menu_locations );
	}

	/**
	 * Add custom columns to the User's Admin table
	 * 
	 * @param 	array 	$columns
	 * 
	 * @return 	array
	 */
	public function add_users_columns( $columns ) {

		$columns['t_shirt_size'] = 'T-Shirt Size';
		$columns['chapter'] = 'Chapter';

		return $columns;
	}

	/**
	 * Add custom column data to the User's Admin table
	 * 
	 * @param 	string|int 	$output
	 * @param 	string 		$column_name
	 * @param 	int 		$user_id
	 * 
	 * @return string|int
	 */
	public function add_users_custom_columns( $output, $column_name, $user_id ) {

		switch( $column_name ) {

			case 'chapter':

				$chapter_id = get_user_meta( $user_id, 'primary_chapter_id', true );
				if( $chapter = OMH()->chapter_factory->read( $chapter_id ) ) {
					return $chapter->get_chapter_name();
				}
				break;

			case 't_shirt_size':

				return get_user_meta( $user_id, 't_shirt_size', true );
				break;
		}

		return $output;
	}

	/**
	 * Add custom fields to the User Admin profile
	 * 
	 * @param 	WP_User 	$user
	 * 
	 * @return 	void
	 */
	public function add_users_profile_fields( $user ) {

		$t_shirt_size = get_user_meta( $user->ID, 't_shirt_size', true );
		$instagram_handle = get_user_meta( $user->ID, 'instagram_handle', true );
		?>

		<h3>Merch House User Information</h3>

		<table class="form-table">
			<tr>
				<th>
					<label for="t_shirt_size">T-Shirt Size</label>
				</th>
				<td>
					<select name="t_shirt_size" id="t_shirt_size">
						<option disabled <?php echo ( $t_shirt_size ? '' : 'selected' ); ?>>Select your size</option> 
						<option value="S" <?php echo( $t_shirt_size == 'S' ? 'selected' : '' ); ?>>S</option>
						<option value="M" <?php echo( $t_shirt_size == 'M' ? 'selected' : '' ); ?>>M</option>
						<option value="L" <?php echo( $t_shirt_size == 'L' ? 'selected' : '' ); ?>>L</option>
						<option value="XL" <?php echo( $t_shirt_size == 'XL' ? 'selected' : '' ); ?>>XL</option>
						<option value="2XL" <?php echo( $t_shirt_size == '2XL' ? 'selected' : '' ); ?>>2XL</option>
						<option value="3XL" <?php echo( $t_shirt_size == '3XL' ? 'selected' : '' ); ?>>3XL</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>
					<label for="instagram_handle">Instagram Handle</label>
				</th>
				<td>
					<input type="text" name="instagram_handle" id="instagram_handle" value="<?php echo $instagram_handle ? esc_attr( $instagram_handle ) : ''; ?>" class="regular-text" />
				</td>
			</tr>
		</table>
		<?php
	}

	/**
	 * Save the custom fields on the User Admin profile
	 * 
	 * @param 	int 	$user_id
	 * 
	 * @return 	void
	 */
	public function save_users_profile_fields( $user_id ) {

		if( !current_user_can( 'edit_user', $user_id ) ) {
			return false;
		}

		if( !empty( $_POST['t_shirt_size'] ) 
			&& in_array( $_POST['t_shirt_size'], array( 'S', 'M', 'L', 'XL', '2XL', '3XL' ) )
		) {

			$t_shirt_size = sanitize_text_field( $_POST['t_shirt_size'] );

			update_user_meta( $user_id, 't_shirt_size', $t_shirt_size );
		}

		if( !empty( $_POST['instagram_handle'] ) ) {

			$instagram_handle = sanitize_text_field( $_POST['instagram_handle'] );

			update_user_meta( $user_id, 'instagram_handle', $instagram_handle );
		}
	}
}
return new OMH_Admin();
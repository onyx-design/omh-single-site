<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OMH_Debug {

	use OMH_Singleton_Class_Abstract;

	protected $debug_active = false;

	public $stats = array();

	public $data = array();


	protected $shutdown_registered = false;

	protected function __construct() {
		
		//ini_set( "error_reporting", E_ALL );
		error_reporting(E_ALL);
		ini_set("log_errors", 1);
	}

	public function __invoke( $arg = null ) {

		$this->log( $arg );
	}

	public function is_active() {
		return $this->debug_active;
	}

	public function set_active( $set = true ) {
		$this->debug_active = (bool) $set;

		if( $this->debug_active && !$this->shutdown_registered ) {
			$this->shutdown_registered = true;
			add_action( 'shutdown', array( $this, 'debug_shutdown' ) );
		}
		else if( !$this->debug_active ) {
			remove_action( 'shutdown', array( $this, 'debug_shutdown' ) );
		}

		return $this;
	}

	public function log( $msg, $force = false ) {
		if( $force || $this->debug_active ) {
			error_log( ( is_scalar( $msg ) ? $msg : print_r( $msg, true ) ) );	
		}
	}

	public function stat( $name, $value = null, $log_stats = true ) {

		if( $log_stats ) {
			$this->set_active();
			$this->shutdown_registered = true;
			add_action( 'shutdown', array( $this, 'debug_shutdown' ) );
			//register_shutdown_function( array( $this, 'debug_shutdown' ) );
		}

		if( !is_string( $name ) ) {
			return false;
		}
		else {

			// Init the stat if it doesn't exist
			if( !isset( $this->stats[ $name ] ) ) {

				$this->stats[ $name ] = 0;
			}

			// If a $value is supplied, store it
			if( isset( $value ) ) {

				$this->stats[ $name ] = $value;
			}
			// If no $value is supplied and stat is an integer, increment it
			else if( is_int( $this->stats[ $name ] ) ) {

				$this->stats[ $name ]++;
			}

			return $this->stats[ $name ];
		}
	}

	public function log_debug_enqueue( ) {
		global $wp_scripts;
		$this->log( $wp_scripts );
	}

	public function debug_enqueue() {
		add_action( 'wp_enqueue_scripts', array( $this, 'log_debug_enqueue' ), -99999 );
	}

	public function debug_shutdown() {
		global $omh_load_time, $wp;

		$this->log( 
			"\r\n\r\n".
			"OMH DEBUG POST-SHUTDOWN REPORT"."\r\n".
			str_pad( "OMH load time: ", 20 ) . $omh_load_time . "\r\n" .
			str_pad( "wpdb queries: ", 20 ) . get_num_queries() . "\r\n" .
			str_pad( "Memory Usage: ", 20 ) . memory_get_usage() . "\r\n" .
			str_pad( "Memory Peak Usage: ", 20 ) . memory_get_peak_usage()."\r\n".
			str_pad( "Request URI", 20 ) . $_SERVER['REQUEST_URI'] ."\r\n".
			str_pad( "pagenow", 20) . $GLOBALS['pagenow'] . "\r\n\r\n"
		);

		if( $this->stats ) {
			
			$this->log( "\r\nOMH DEBUG STATS\r\n" . print_r( $this->stats, true ) );
		}
	}
}
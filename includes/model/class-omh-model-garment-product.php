<?php
defined( 'ABSPATH' ) || exit;

class OMH_Model_Garment_Product extends OMH_Model {

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_garment_products';

	protected static $factory = 'garment_product_factory';

	public static $defaults = array(
		'product_slug'				=> '',
		'inksoft_product_id'		=> 0,
		'inksoft_sku'				=> '',
		'garment_brand_id'			=> 0,
		'manufacturer_sku'			=> '',
		'product_name'				=> '',
		'product_long_description'	=> '',
		'material'					=> '',
	);

	/********************************
	 *			 Columns			*
	 ********************************/

	protected $product_slug;

	protected $inksoft_product_id;

	protected $inksoft_sku;

	protected $garment_brand_id;

	protected $manufacturer_sku;

	protected $product_name;

	protected $product_long_description;

	protected $material;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_product_slug() {

		return $this->product_slug;
	}

	public function set_product_slug( $product_slug ) {

		$this->product_slug = $product_slug;

		return $this->get_product_slug();
	}

	public function get_inksoft_product_id() {

		return $this->inksoft_product_id;
	}

	public function set_inksoft_product_id( $inksoft_product_id ) {

		$this->inksoft_product_id = $inksoft_product_id;

		return $this->get_inksoft_product_id();
	}

	public function get_inksoft_sku() {

		return $this->inksoft_sku;
	}

	public function set_inksoft_sku( $inksoft_sku ) {

		$this->inksoft_sku = $inksoft_sku;

		return $this->get_inksoft_sku();
	}

	public function get_garment_brand_id() {

		return $this->garment_brand_id;
	}

	public function set_garment_brand_id( $garment_brand_id ) {

		$this->garment_brand_id = $garment_brand_id;

		return $this->get_garment_brand_id();
	}

	public function get_manufacturer_sku() {

		return $this->manufacturer_sku;
	}

	public function set_manufacturer_sku( $manufacturer_sku ) {

		$this->manufacturer_sku = $manufacturer_sku;

		return $this->get_manufacturer_sku();
	}

	public function get_product_name() {

		return $this->product_name;
	}

	public function set_product_name( $product_name ) {

		$this->product_name = $product_name;

		return $this->get_product_name();
	}

	public function get_product_long_description() {

		return $this->product_long_description;
	}

	public function set_product_long_description( $product_long_description ) {

		$this->product_long_description = $product_long_description;

		return $this->get_product_long_description();
	}

	public function get_material() {

		return $this->material;
	}

	public function set_material( $material ) {

		$this->material = $material;

		return $this->get_material();
	}

	/********************************
	 *			Functions			*
	 ********************************/

	public static function get_search_args( $search_term = '' ) {

		$search_term = str_replace( ' ', '%', $search_term );

		return array(
			array(
				'name'		=> 'product_name',
				'value'		=> "%$search_term%",
				'compare'	=> 'LIKE'
			),
			array(
				'name'		=> 'product_slug',
				'value'		=> "%$search_term%",
				'compare'	=> 'LIKE'
			),
			'compare'	=> 'OR'
		);
	}


	/********************************
	 *			 Helpers			*
	 ********************************/

	public function get_garment_brand() {

		if( !$this->get_garment_brand_id() ) {
			return null;
		}

		if( !isset( $this->garment_brand ) || ( null === $this->garment_brand ) ) {
			$this->garment_brand = OMH()->garment_brand_factory->read( $this->get_garment_brand_id() );
		}

		return $this->garment_brand;
	}

	public function get_garment_styles() {

		$garment_styles = OMH()->garment_style_factory->get_by_garment_product_id( $this->get_id() );

		return $garment_styles ? array_column( $garment_styles, null, 'inksoft_style_id' ): array();
	}

	public function get_joined_label() {
		return $this->get_product_name();
	}

	public function populate_style_data() {
		global $wpdb;

		$response = array(
			'success'	=> true
		);

		$api_response = OMH_API_Inksoft::request( 'get_product', array(
			'query_params'	=> array(
				'ProductId' => $this->inksoft_product_id
			)
		) );

		foreach( $api_response->data()->Styles as $style ) {

			$style->ProductId = $this->inksoft_product_id;

			$garment_style = OMH_Model_Garment_Style::get_or_create_from_api_response( $style );

			if( !$garment_style ) {
				$response['errors'][] = array(
					'inksoft_id'	=> $style->ID,
					'message'		=> 'There was a problem populating InkSoft Style ID: ' . $style->ID
				);
				$response['success'] = false;
			}
		}

		return $response;
	}

	public static function get_from_inksoft( $inksoft_product_id = 0 ) {

		if( $inksoft_product_id ) {

			$api_response = OMH_API_Inksoft::request( 'get_product', array( 'query_params' => array( 'ProductId' => $inksoft_product_id ) ) );

			if( $api_response->data() ) {

				$garment_product = static::get_or_create_from_api_response( $inksoft_product );

				if( false !== $garment_product ) {

					return $garment_product;
				}
			}
		}

		return false;
	}

	public static function populate_garment_products( $last_inksoft_product_id = 0 ) {
		ini_set("max_execution_time", 600 );

		$response = array(
			'success'	=> true
		);

		$api_response = OMH_API_Inksoft::request( 'get_products', array() );

		foreach( $api_response->data() as $inksoft_product ) {

			if( $last_inksoft_product_id > $inksoft_product->ID ) {
				continue;
			}

			if( ox_exec_time() > 25 ) {

				$response['errors'][] = array(
					'inksoft_id'	=> $last_inksoft_product_id,
					'message'		=> 'PHP Timeout Hit, stopped on InkSoft Product ID: ' . $last_inksoft_product_id
				);
				$response['success'] = false;
				return $response;
			}

			$garment_product = static::get_or_create_from_api_response( $inksoft_product );

			if( false === $garment_product ) {

				$response['errors'][] = array(
					'inksoft_id'	=> $inksoft_product->ID,
					'message'		=> 'There was a problem inserting InkSoft Product ID: ' . $inksoft_product->ID
				);
				$response['success'] = false;
			} else {

				$response['products'][] = $garment_product->populate_style_data();
			}

			$last_inksoft_product_id = $inksoft_product->ID;
		}

		return $response;
	}

	public static function get_or_create_from_api_response( $api_response ) {

		if( !isset( $api_response->ID ) ) {
			return false;
		}

		$garment_product_factory = self::get_factory();
		$garment_product = $garment_product_factory->get_by_inksoft_id( $api_response->ID );

		$garment_brand = OMH_Model_Garment_Brand::get_or_create_from_api_response( $api_response );
		
		// $garment_brand_factory = OMH_Model_Garment_Brand::get_factory();
		// $garment_brand = $garment_brand_factory->get_by_inksoft_id( $api_response->ManufacturerId );

		$product_name = isset( $api_response->Name ) ? $api_response->Name : '';
		$inksoft_sku = isset( $api_response->Sku ) ? $api_response->Sku : '';
		$manufacturer_sku = isset( $api_response->ManufacturerSku ) ? $api_response->ManufacturerSku : '';
		$product_long_description = isset( $api_response->LongDescription ) ? $api_response->LongDescription : '';
		$product_slug = $garment_brand && $product_name ? "{$garment_brand->get_brand_slug()}_" . sanitize_title( $product_name ) : '';

		// Material is our own field, if the product doesn't exist yet, just input empty string
		$material = $garment_product ? $garment_product->get_material() : '';

		$product_data = array(
			'product_slug'				=> $product_slug,
			'inksoft_product_id'		=> $api_response->ID,
			'inksoft_sku'				=> $inksoft_sku,
			'garment_brand_id'			=> $garment_brand ? $garment_brand->get_id() : 0,
			'manufacturer_sku'			=> $manufacturer_sku,
			'product_name'				=> $product_name,
			'product_long_description'	=> $product_long_description,
			'material'					=> $material
		);

		if( !$garment_product ) {
			$garment_product = $garment_product_factory->create( $product_data );
		} else {
			$garment_product = $garment_product_factory->update( $garment_product->get_id(), $product_data );
		}

		return $garment_product;
	}
}
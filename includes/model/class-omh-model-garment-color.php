<?php
defined( 'ABSPATH' ) || exit;

class OMH_Model_Garment_Color extends OMH_Model {

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_garment_colors';

	protected static $factory = 'garment_color_factory';

	public static $defaults = array(
		'color_code'		=> null,
		'color_name'		=> null,
	);

	/********************************
	 *			 Columns			*
	 ********************************/

	public $color_code;

	public $color_name;

	public $color_hex_1;

	public $color_hex_2;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_color_code() {

		return $this->color_code;
	}

	public function set_color_code( $color_code ) {

		$this->color_code = $color_code;

		return $this->get_color_code();
	}

	public function get_color_name() {

		return $this->color_name;
	}

	public function set_color_name( $color_name ) {

		$this->color_name = $color_name;

		return $this->get_color_name();
	}

	public function get_color_hex_1() {

		return $this->color_hex_1;
	}

	public function set_color_hex_1( $color_hex_1 ) {

		$this->color_hex_1 = $color_hex_1;

		return $this->get_color_hex_1();
	}

	public function get_color_hex_2() {

		return $this->color_hex_2;
	}

	public function set_color_hex_2( $color_hex_2 ) {

		$this->color_hex_2 = $color_hex_2;

		return $this->get_color_hex_2();
	}

	/********************************
	 *			Functions			*
	 ********************************/

	public function is_valid() {

		if( $this->get_color_code() ) {
			return true;
		}

		return false;
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	/**
	 * Get or Create the OMH Garment Color from the Style response
	 * 
	 * dev:todo Make sure we're using the Garment Style here to create the color
	 * 
	 * Currently not used for creating colours
	 * 
	 * @param 	OMH_API_Response 	$api_response 
	 * 
	 * @return 	OMH_Model_Garment_Color
	 */
	public static function get_or_create_from_api_response( $api_response ) {

		// If the API response doesn't have ID and Name, return false as we need that information
		if( !isset( $api_response->ID, $api_response->Name ) ) {
			return false;
		}

		$garment_color_factory = self::get_factory();
		$garment_color = $garment_color_factory->get_by_color_name( $api_response->Name );

		if( !$garment_color ) {

			$color_data = array(
				'color_code'	=> null,
				'color_name'	=> $api_response->Name,
				'color_hex_1'	=> $api_response->HtmlColor1 ?? '',
				'color_hex_2'	=> $api_response->HtmlColor2 ?? ''
			);

			$garment_color = $garment_color_factory->create( $color_data );
		} else {

			$updated_data = array(
				'color_hex_1'	=> $api_response->HtmlColor1,
				'color_hex_2'	=> $api_response->HtmlColor2
			);

			$garment_color = $garment_color_factory->update( $garment_color->get_id(), $updated_data );
		}

		return $garment_color;
	}
}
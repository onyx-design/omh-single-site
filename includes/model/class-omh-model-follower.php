<?php
defined( 'ABSPATH' ) || exit;

class OMH_Model_Follower extends OMH_Model {

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_followers';

	protected static $factory = 'follower_factory';

	public static $defaults = array(
		'follower_email'				=> '',
		'following_id'		=> 0,
		'following_type'	=> ''
	);

	public static $required = array(
		'follower_email',
		'following_id',
		'following_type',
	);

	public static $fields = array(
		'follower_email'				=> array(
			'label'		=> 'Email Address',
		),
		'following_id' 		=> array(
			'label'		=> 'Following ID'
		),
		'following_type' 	=> array(
			'label'		=> 'Following Type'
		)
	);

	/********************************
	 *			 Columns			*
	 ********************************/

	protected $follower_email;

	protected $following_id;

	protected $following_type;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_follower_email() {

		return $this->follower_email;
	}

	public function set_follower_email( $follower_email ) {

		$this->follower_email = $follower_email;

		return $this->get_follower_email();
	}

	public function get_following_id() {

		return $this->following_id;
	}

	public function set_following_id( $following_id ) {

		$this->following_id = $following_id;

		return $this->get_following_id();
	}

	public function get_following_type() {

		return $this->following_type;
	}

	public function set_following_type( $following_type ) {

		$this->following_type = $following_type;

		return $this->get_following_type();
	}

	/********************************
	 *	  Custom Getters/Setters	*
	 ********************************/

	public function get_following_store() {

		$following_store = null;

		if( 'chapter' === $this->get_following_type() ) {

			$following_store = OMH()->chapter_factory->read( $this->get_following_id() );
		}
		else if( 'organization' === $this->get_following_type() ) {

			$following_store = OMH()->organization_factory->read( $this->get_following_id() );
		}

		return $following_store;
	}

	 public function get_following_store_name() {

		$following_store = $this->get_following_store();
		$following_store_name = '';

		if( $following_store ) {

			if( 'chapter' === $this->get_following_type() ) {

				$following_store_name = $following_store->get_chapter_name();
			}
			else if( 'organization' === $this->get_following_type() ) {

				$following_store_name = $following_store->get_org_name();
			}
		}

		return $following_store_name;
	 }
}
<?php
defined( 'ABSPATH' ) || exit;

class OMH_Model_College extends OMH_Model {
	use OMH_Model_Trait_Term;

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_colleges';

	protected static $factory = 'college_factory';

	protected static $taxonomy = 'colleges';

	public static $defaults = array(
		'term_id'			=> null,
		'logo_image_id'		=> 0,
		'banner_image_id'	=> 0,
	);

	public static $unique = array(
		'college_name',
		'college_shortname',
		'college_letters',
	);

	public static $required = array(
		'college_name',
		'college_shortname',
		'college_letters',
	);

	public static $fields = array(
		'term_id'			=> array(
			'label'		=> 'Term ID'
		),
		'logo_image_id'		=> array(
			'label'		=> 'Logo'
		),
		'banner_image_id'	=> array(
			'label'		=> 'Banner'
		),
		'college_name'		=> array(
			'label'		=> 'College Name'
		),
		'college_shortname'		=> array(
			'label'		=> 'College Name'
		),
		'college_letters'	=> array(
			'label'		=> 'College Letters'
		),
		'city'				=> array(
			'label'		=> 'City'
		),
		'state'				=> array(
			'label'		=> 'State'
		),
	);

	/********************************
	 *			 Columns			*
	 ********************************/

	protected $logo_image_id;

	protected $banner_image_id;

	protected $college_name;

	protected $college_shortname;

	protected $college_letters;

	protected $city;

	protected $state;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_logo_image_id() {

		return $this->logo_image_id;
	}

	public function set_logo_image_id( $logo_image_id ) {

		if( is_array( $logo_image_id ) ) {

			if( isset( $logo_image_id['id'] ) ) {

				if( !empty( maybe_str_to_bool( $logo_image_id['is_temp'] ) ) ) {

					$new_logo_image_id = OMH_Tool_Sideload_Media::insert_media_file_from_temp( $logo_image_id['id'] );

					if( $new_logo_image_id ) {

						$logo_image_id = $new_logo_image_id['id'];
					}
				}
				else {

					$logo_image_id = $logo_image_id['id'];
				}
			}
		}

		$this->logo_image_id = $logo_image_id;
	}

	public function get_banner_image_id() {

		return $this->banner_image_id;
	}

	public function set_banner_image_id( $banner_image_id ) {

		if( is_array( $banner_image_id ) ) {

			if( isset( $banner_image_id['id'] ) ) {

				if( !empty( maybe_str_to_bool( $banner_image_id['is_temp'] ) ) ) {

					$new_banner_image_id = OMH_Tool_Sideload_Media::insert_media_file_from_temp( $banner_image_id['id'] );

					if( $new_banner_image_id ) {

						$banner_image_id = $new_banner_image_id['id'];
					}
				} else {

					$banner_image_id = $banner_image_id['id'];
				}
			}
		}

		$this->banner_image_id = $banner_image_id;
	}

	public function get_college_name() {
		
		return $this->college_name;
	}

	public function set_college_name( $college_name ) {

		$this->college_name = $college_name;

		return $this->get_college_name();
	}

	public function get_college_shortname() {

		return $this->college_shortname;
	}

	public function set_college_shortname( $college_shortname ) {

		$this->college_shortname = $college_shortname;

		return $this->get_college_shortname();
	}

	public function get_college_letters() {

		return $this->college_letters;
	}

	public function set_college_letters( $college_letters ) {

		$this->college_letters = $college_letters;

		return $this->get_college_letters();
	}

	public function get_city() {

		return $this->city;
	}

	public function set_city( $city ) {

		$this->city = $city;

		return $this->get_city();
	}

	public function get_state() {

		return $this->state;
	}

	public function set_state( $state ) {

		$this->state = $state;

		return $this->get_state();
	}

	/********************************
	 *			Functions			*
	 ********************************/

	public function get_banner_image( $image_size = 'woocommerce_thumbnail' ) {

		if( $banner_src = wp_get_attachment_image_src( $this->get_banner_image_id(), $image_size ) ) {
			return $banner_src[0];
		}

		return false;
	}

	public function get_logo_image() {

		if( $logo_src = wp_get_attachment_image_src( $this->get_logo_image_id(), 'woocommerce_thumbnail' ) ) {
			return $logo_src[0];
		}

		return false;
	}

	public static function get_search_args( $search_term = '' ) {

		$search_term = str_replace( ' ', '%', $search_term );

		return array(
			array(
				'name'		=> 'college_name',
				'value'		=> "%$search_term%",
				'compare'	=> 'LIKE'
			),
			array(
				'name'		=> 'college_letters',
				'value'		=> "%$search_term%",
				'compare'	=> 'LIKE'
			),
			'compare'	=> 'OR'
		);
	}

	/********************************
	 *		 Event Listeners		*
	 ********************************/

	public function before_save() {

		// If term isn't set, create it
		// if( !$this->get_term_id() ) {
		// 	$this->set_term_id( $this->create_term() );
		// } else {
		// 	$this->set_term_id( $this->update_term() );
		// }
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	public function get_term_slug() {
		return $this->get_college_shortname();
	}

	public function get_term_label() {
		return $this->get_college_name();
	}

	public function get_joined_label() {
		return $this->get_college_name();
	}
}
<?php
defined( 'ABSPATH' ) || exit;

class OMH_Model_Chapter extends OMH_Model {
	use OMH_Model_Trait_Term;

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_chapters';

	protected static $factory = 'chapter_factory';

	protected static $taxonomy = 'chapters';

	public static $defaults = array(
		'org_id'				=> 0,
		'college_id'			=> 0,
		'term_id'				=> 0,
		'banner_image_id'		=> 0,
		'chapter_name'			=> '',
		'earnings_percentage'	=> null,
		'status'				=> 'Leads'
	);

	public static $joined = array(
		'org_id',
		'college_id',
	);

	public static $unique = array(
		'url_structure',
		array(
			'org_id',
			'chapter_name'
		)
	);

	public static $required = array(
		'url_structure',
		// dev:improve dev:todo Temporarily disabled for select boxes as the default value isn't working on the front end
		// 'status',
	);

	public static $fields = array(
		'org_id'				=> array(
			'label'	=> 'Organization'
		),
		'college_id'			=> array(
			'label'	=> 'College',
		),
		'term_id'				=> array(
			'label'	=> 'Term ID',
		),
		'banner_image_id'		=> array(
			'label'	=> 'Banner Image'
		),
		'chapter_name'				=> array(
			'label'	=> 'Chapter Name',
		),
		'url_structure'			=> array(
			'label'	=> 'URL Structure'
		),
		'earnings_percentage'	=> array(
			'label'	=> 'Earnings Percentage'
		),
		'status'				=> array(
			'label'	=> 'Status'
		),
	);

	/********************************
	 *			 Columns			*
	 ********************************/

	protected $org_id;

	protected $college_id;

	protected $chapter_name;

	protected $banner_image_id;

	protected $url_structure;

	protected $status;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_org_id() {

		return $this->org_id;
	}

	public function set_org_id( $org_id ) {

		$this->org_id = $org_id;

		return $this->get_org_id();
	}

	public function get_college_id() {

		return $this->college_id;
	}

	public function set_college_id( $college_id ) {

		$this->college_id = $college_id;

		return $this->get_college_id();
	}

	public function get_banner_image_id() {

		return $this->banner_image_id;
	}

	public function set_banner_image_id( $banner_image_id ) {

		// if( empty( $banner_image_id ) ) {
		// 	return;
		// }

		if( is_array( $banner_image_id ) ) {

			if( isset( $banner_image_id['id'] ) ) {

				if( !empty( maybe_str_to_bool( $banner_image_id['is_temp'] ) ) ) {

					$new_banner_image_id = OMH_Tool_Sideload_Media::insert_media_file_from_temp( $banner_image_id['id'] );

					if( $new_banner_image_id ) {

						$banner_image_id = $new_banner_image_id['id'];
					}
				}
				else {

					$banner_image_id = $banner_image_id['id'];
				}
			}
		}

		$this->banner_image_id = $banner_image_id;
	}

	public function get_chapter_name() {

		return wp_specialchars_decode( $this->chapter_name );
	}

	public function set_chapter_name( $chapter_name ) {

		$this->chapter_name = esc_html( $chapter_name );

		return $this->get_chapter_name();
	}

	public function get_url_structure() {

		return $this->url_structure;
	}

	public function set_url_structure( $url_structure ) {

		$this->url_structure = $url_structure;

		return $this->get_url_structure();
	}

	public function get_earnings_percentage( $allow_default = true ) {

		$earnings_percentage = $this->earnings_percentage;

		if( !$earnings_percentage && $allow_default ) {
			$earnings_percentage = OMH_DEFAULT_CHAPTER_EARNINGS_PERCENT;
		}

		return $earnings_percentage;
	}

	public function set_earnings_percentage( $earnings_percentage ) {

		// dev:improve
		if( empty( $earnings_percentage ) && $earnings_percentage !== "0" ) {
			$earnings_percentage = null;
		}

		$this->earnings_percentage = $earnings_percentage;

		return $this->earnings_percentage;
	}

	public function get_status() {

		return $this->status;
	}

	public function set_status( $status ) {

		$this->status = $status;

		return $this->get_status();
	}

	/********************************
	 *			Functions			*
	 ********************************/

	public function get_chapter_display_name() {

		$organization = $this->get_org();
		$college = $this->get_college();

		return ( $organization && $college && $organization->get_org_greek_letters() ) ? "{$college->get_college_shortname()} {$organization->get_org_greek_letters()}" : $this->get_chapter_name();
	}

	public function get_chapter_banner_name() {

		$college = $this->get_college();
		$org = $this->get_org();

		$college_letters = $college ? $college->get_college_shortname() : '';
		$org_name = $org ? $org->get_org_name() : '';

		return "{$college_letters} {$org_name}";
	}

	public function get_org() {
		return $this->get_organization();
	}

	public function get_organization() {

		if( !$this->get_org_id() ) {
			return false;
		}

		return OMH()->organization_factory->read( $this->get_org_id() );
	}

	public function get_college() {

		if( !$this->get_college_id() ) {
			return false;
		}

		return OMH()->college_factory->read( $this->get_college_id() );
	}

	public function get_banner_image( $image_size = 'woocommerce_thumbnail' ) {

		if( $banner_src = wp_get_attachment_image_src( $this->get_banner_image_id(), $image_size ) ) {
			return $banner_src[0];
		}

		return false;
	}

	public function get_available_store_credit( $raw = true ) {

		$credit = OMH()->credit_factory->query(
			array(
				'return'	=> array(
					'amount'
				),
				'where'		=> array(
					array(
						'name'	=> 'chapter_id',
						'value'	=> $this->get_id(),
					)
				)
			)
		);

		$available_amount = array_sum( $credit );

		if( $raw ) {
			return $available_amount;
		}

		// Return total store credit in dollars
		return $available_amount / 100;
	}

	public static function get_search_args( $search_term = '' ) {

		// $search_args = array();
		// $search_term = explode( ' ', $search_term );

		// foreach( $search_term as $keyword ) {

		// 	$search_args[] = array(
		// 		'name'		=> 'chapter_name',
		// 		'value'		=> "%$keyword%",
		// 		'compare'	=> 'LIKE'
		// 	);
		// }

		// $search_args['compare'] = 'OR';

		// return $search_args;

		$search_term = str_replace( ' ', '%', $search_term );
		$res = array(
			array(
				'name'		=> 'chapter_name',
				'value'		=> "%$search_term%",
				'compare'	=> 'LIKE'
			),
			array(
				'name'		=> 'url_structure',
				'value'		=> "%$search_term%",
				'compare'	=> 'LIKE'
			),
			'compare'	=> 'OR'
		);
		return $res;
	}

	public function get_ajax_search_select2_option() {
		return array(
			'id'	=> $this->get_id(),
			'text'	=> $this->get_joined_label(),
			'url' => $this->url_structure
		);
	}

	/********************************
	 *		 Event Listeners		*
	 ********************************/

	public function before_save() {

		// If the term is already set, but the Chapter data has changed, we need to update the term
		if( $this->get_term_id() 
			&& ( $term = $this->get_term() ) 
		) {

			if( ( wp_specialchars_decode( $term->name ) !== $this->get_term_label() )
				|| ( $term->slug !== $this->get_term_slug() )
			) {
				$this->update_term();

				// Make sure to flush rewrite rules
				flush_rewrite_rules();
			}
		}
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	public function get_term_slug() {
		return $this->get_url_structure();
	}

	public function get_term_label() {

		return $this->get_chapter_name();
	}

	public function get_joined_label() {
		return $this->get_chapter_name();
	}

	public function get_publicly_available_sibling_chapters() {

		$sibling_chapters = array();

		$sibling_chapter_ids = OMH()->chapter_factory->query( 
			array(  
				'posts_per_page'	=> -1,
				'where'				=> array(
					array(
						'name'		=> 'org_id',
						'value'		=> $this->get_org_id()
					),
					array(
						'name'		=> 'ID',
						'value'		=> $this->get_id(),
						'compare'	=> '!='
					)
				),
				'return'			=> array( 'term_id' )
			) 
		);

		if( $sibling_chapter_ids && count( $sibling_chapter_ids ) > 0 ) {
			$public_products = $this->get_public_products(
				array(
					'taxonomy'	=> 'chapters',
					'field'		=> 'term_id',
					'terms'		=> $sibling_chapter_ids,
					'operator'	=> 'IN'
				), true
			);

			foreach( $public_products as $public_product ) {
				$sibling_chapters = array_merge( $sibling_chapters, get_terms( array( 'object_ids' => $public_product->get_id(), 'taxonomy' => 'chapters' ) ) );
			}

		}

		return array_filter( array_unique( $sibling_chapters, SORT_REGULAR ) );
	}
}
<?php
defined( 'ABSPATH' ) || exit;

class OMH_Model {

	public static $table_map = array(
		/* Table 					=> Model */
		'omh_chapters'				=> 'OMH_Model_Chapter',
		'omh_organizations'			=> 'OMH_Model_Organization',
		'omh_colleges'				=> 'OMH_Model_College',
		'omh_garment_products'		=> 'OMH_Model_Garment_Product',
		'omh_garment_styles'		=> 'OMH_Model_Garment_Style',
		'omh_garment_colors'		=> 'OMH_Model_Garment_Color',
		'omh_garment_brands'		=> 'OMH_Model_Garment_Brand',
	);

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = null;

	protected static $factory = null;

	protected static $taxonomy = null;

	public static $defaults = array();

	/** The following variables can/should be reduced to a single array **/

	public static $joined = array();

	public static $unique = array();

	public static $required = array();

	public static $fields = array();

	/********************************
	 *			 Columns			*
	 ********************************/

	/**
	 * Internal ID
	 * 
	 * @var int
	 */
	public $ID;

	/**
	 * Internal Meta Created
	 * 
	 * @var string
	 */
	// public $meta_created = '0000-00-00 00:00:00';

	/**
	 * Internal Meta Updated
	 * 
	 * @var string
	 */
	// public $meta_updated = '0000-00-00 00:00:00';

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	/**
	 * Return the ID
	 * 
	 * @return 	int
	 */
	public function get_id() {

		return $this->ID;
	}

	/**
	 * Set the ID
	 * 
	 * @param 	int 	$ID
	 * 
	 * @return 	int
	 */
	public function set_id( $ID ) {

		$this->ID = $ID;

		return $this->get_id();
	}

	/**
	 * Return the Meta Created
	 * 
	 * @return 	string
	 */
	public function get_meta_created() {

		return $this->meta_created;
	}

	/**
	 * Set the Meta Created
	 * 
	 * @param 	string	$meta_created
	 * 
	 * @return 	string
	 */
	public function set_meta_created( $meta_created ) {

		$this->meta_created = $meta_created;

		return $this->get_meta_created();
	}

	/**
	 * Return the Meta Updated
	 * 
	 * @return 	string
	 */
	public function get_meta_updated() {

		return $this->meta_updated;
	}

	/**
	 * Set the Meta Updated
	 * 
	 * @param 	string	$meta_updated
	 * 
	 * @return 	string
	 */
	public function set_meta_updated( $meta_updated ) {

		$this->meta_updated = $meta_updated;

		return $this->meta_updated;
	}

	/********************************
	 *			Functions			*
	 ********************************/

	/**
	 * Constructor, what else is there to say?
	 * 
	 * @param 	get_class() 	$omh_model
	 */
	public function __construct( $omh_model ) {

		foreach( get_object_vars( $omh_model ) as $key => $value ) {

			if( method_exists( $this, "set_{$key}" ) ) {

				$this->{"set_{$key}"}( $value );
			}
		}
	}

	/**
	 * Get the OMH Model instance
	 * 
	 * @param 	int 	$ID
	 * 
	 * @return 	get_class()|bool
	 */
	public static function get_instance( $ID ) {

		if( !$model_factory = static::get_factory() ) {
			return false;
		}

		return $model_factory->read( $ID );
	}

	public static function get_instance_by_table( $ID, $table = '' ) {

		// If we aren't given a table, or the table isn't in our map, return false
		if( !$table || !isset( static::$table_map[ $table ] ) ) {
			return false;
		}

		$model = static::$table_map[ $table ];

		return $model::get_instance( $ID );
	}

	public static function get_model_by_table( $table = '' ) {

		if( !$table ) {
			return false;
		}

		if( !isset( static::$table_map[ $table ] ) ) {

			$table = 'omh_' . $table;

			if( !isset( static::$table_map[ $table ] ) ) {
				return false;
			}
		}

		return static::$table_map[ $table ];
	}

	/**
	 * Get the Factory for this OMH Model
	 * 
	 * @return 	mixed|OMH_Factory
	 */
	public static function get_factory() {

		if( !( $factory_name = static::$factory ) ) {
			return false;
		}

		return OMH()->{ $factory_name };
	}

	public static function get_taxonomy() {

		return static::$taxonomy;
	}

	public static function get_joined() {

		return static::$joined;
	}

	public static function ajax_search( $search_term = '' ) {

		if( !( $model_factory = static::get_factory() ) ) {
			return false;
		}

		$search_results = $model_factory->query(
			array(
				'posts_per_page'	=> -1,
				'search'			=> $search_term
			)
		);

		$results = array();
		foreach( $search_results as $search_result ) {
			$results[] = $search_result->get_ajax_search_select2_option();
		}

		return array( 'results' => $results );
	}

	public function get_ajax_search_select2_option() {
		return array(
			'id'	=> $this->get_id(),
			'text'	=> $this->get_joined_label()
		);
	}

	public function get_ajax_search_html() {

		return '<div class="ajax-search-result" data-result-value="' . $this->get_id() . '" data-result-label="' . $this->get_joined_label() . '">' . $this->get_joined_label() . '</div>';
	}

	public static function get_search_args( $search_term = '' ) {

		return array();
	}

	/**
	 * Save the OMH Model
	 * 
	 * @return 	mixed|OMH_Model
	 */
	public function save() {

		if( !( $model_factory = $this->get_factory() ) ) {
			return false;
		}

		if( !empty( $this->validate() ) ) {
			return false;
		}

		if( method_exists( $this, 'before_save' ) ) {
			$this->before_save();
		}

		if( $this->exists() ) {

			$model = $model_factory->update( $this->get_id(), $this->to_array() );
		} else {

			$model = $model_factory->create( $this->to_array(), static::$defaults );
		}

		$this->set_id( $model->ID );

		return $model;
	}

	public function delete() {

		if( !( $model_factory = $this->get_factory() ) ) {
			return false;
		}

		if( $this->exists() ) {

			return $model_factory->delete( $this->get_id() );
		}

		return false;
	}

	public function exists() {

		if( $this->get_id() && $this->get_id() > 0 ) {
			return true;
		}

		return false;
	}

	public function validate() {

		$validation = array();
		$required = $this->validate_required();
		$unique = $this->validate_unique();

		if( true !== $required ) {
			$validation['required'] = $required;
		}

		if( true !== $unique ) {
			$validation['unique'] = $unique;
		}

		return $validation;
	}

	public function validate_required() {

		if( !( $model_factory = $this->get_factory() ) ) {
			return false;
		}

		$valid = true;
		$invalid = array();

		if( $required = static::$required ) {

			foreach( $required as $required_key ) {

				if( !$this->$required_key ) {

					$invalid[] = $required_key;
				}
			}

			if( $invalid ) {
				return $this->get_field_labels( $invalid );
			}
		}

		return $valid;
	}

	/**
	 * Validate Uniqueness
	 * 
	 * dev:todo Allow uniqueness based on full set or subset of values
	 * 
	 * @return 	bool
	 */
	public function validate_unique() {

		if( !( $model_factory = $this->get_factory() ) ) {
			return false;
		}

		$valid = true;

		if( $unique = static::$unique ) {

			$inter_where = array();
			$inner_where = array();
			foreach( $unique as $unique_key ) {

				if( is_array( $unique_key ) ) {

					foreach( $unique_key as $inter_key ) {

						if( empty( $this->$inter_key ) ) {

							$inter_where[] = array(
								'name'		=> $inter_key,
								'value'		=> $this->$inter_key,
								'compare' 	=> '!='
							);
						} else {

							$inter_where[] = array(
								'name'	=> $inter_key,
								'value'	=> $this->$inter_key
							);
						}
					}

					$inter_where['compare'] = 'AND';

					$inner_where[] = $inter_where;
				} else {

					$inner_where[] = array(
						'name'	=> $unique_key,
						'value'	=> $this->$unique_key
					);
				}
			}

			$inner_where['compare'] = 'OR';

			if( $this->exists() ) {

				$where = array(
					$inner_where,
					array(
						'name'		=> 'ID',
						'value'		=> $this->get_id(),
						'compare'	=> '!='
					),
					'compare'	=> 'AND'
				);
			} else {
				$where = array(
					$inner_where,
					'compare'	=> 'AND'
				);
			}

			// Display which columns need to be unqiue
			// array_uintersect_assoc( $query[0]->to_array(), $query[1]->to_array(), $query[2]->to_array(), function( $a, $b ) {
			// 	return strcasecmp( $a, $b );
			// } )
			$query = $model_factory->query( 
				array(
					'posts_per_page' 	=> -1,
					'where'				=> $where,
				)
			);

			if( count( $query ) > 0 ) {

				$valid = array();

				foreach( $query as $query_item ) {
					$valid = array_keys( array_uintersect_assoc( $this->to_array(), $query_item->to_array(), function( $a, $b ) {
						return strcasecmp( $a, $b );
					} ) );
				}

				return $this->get_field_labels( $valid );
			}
		}

		return $valid;
	}

	

	/**
	 * Return this object as an array
	 * 
	 * @return 	array
	 */
	public function to_array( $with_joins = false ) {

		$joined = static::get_joined();

		$omh_model = array();
		foreach( get_object_vars( $this ) as $object_key => $object_val ) {
			$var_getter = "get_{$object_key}";

			if( method_exists( $this, $var_getter ) ) {
				$omh_model[ $object_key ] = $this->$var_getter();
			}
		}

		if( isset( static::$schema ) ) {
			$omh_model = array_intersect_key( $omh_model, array_flip( static::$schema ) );
		}

		if( $omh_model && $with_joins ) {
			array_walk( $omh_model, 
				function( &$var, $key ) use ( $joined ) {

					if( in_array( $key, $joined ) ) {
		
						$get_joined_method = str_replace( '_id', '', "get_{$key}" );

						if( method_exists( $this, $get_joined_method ) 
							&& $joined_object = $this->$get_joined_method()
						) {

							// $var = array(
							// 	'value' 		=> $var,
							// 	'value_label'	=> $joined_object->get_joined_label()
							// );
							$var = array(
								'value' 		=> array(
									'id' => $var,
									'text'	=> $joined_object->get_joined_label()
								)
							);
							
						}
					}
				} 
			);
		}

		return $omh_model;
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	public function get_joined_label() {
		return 'Label';
	}

	public function get_field_labels( $fields = array() ) {

		return array_map( function( $field ) {

			return isset( static::$fields[ $field ]['label'] ) ? static::$fields[ $field ]['label'] : $field;
		}, $fields );
	}

	/**
	 * @tag Product Status
	 * @tag Visibility
	 */
	public function get_public_products( $custom_tax_query = null, $return = false, $force_refresh = false ) {

		if( $return ) {
			$force_refresh = true;
		}

		if( !isset( $this->public_products ) || !$this->public_products || $force_refresh ) {
			$user = wp_get_current_user();

			if( get_query_var( 'organizations', false ) || get_query_var( 'colleges', false ) ) {
				// $visibility_values = array( 'national' );
				$visible_statuses = OMH_Product_Variable::get_visible_product_statuses( 'national' );
			} else {

				// $visibility_values = array( 'public', 'national' );

				// if( $user->has_role( 'house_admin' ) ) {
				// 	$visibility_values[] = 'house_admin';
				// }
				$visible_statuses = OMH_Product_Variable::get_visible_product_statuses( 'chapter' );
			}
			
			// $meta_query = array(
			// 	array(
			// 		'key'		=> '_visibility_mh',
			// 		'value'		=> $visibility_values,
			// 		'compare'	=> 'IN'
			// 	)
			// );
			$tax_query = array(
				'relation'	=> 'AND',
				array(
					'taxonomy'	=> 'product_status',
					'field'		=> 'slug',
					'terms'		=> $visible_statuses,
					'operator'	=> 'IN'
				)
			);

			if( $custom_tax_query ) {
				$tax_query[] = $custom_tax_query;
			} else {
				$tax_query[] = array(
					'taxonomy'	=> $this->get_taxonomy(),
					'field'		=> 'slug',
					'terms'		=> $this->get_term()->slug
				);
			}
			
			$args = array(
				'post_type'		=> 'product',
				'posts_per_page'=> -1,
				'tax_query'		=> $tax_query,
				// 'meta_query'	=> $meta_query
			);

			$query = new WP_Query( $args );

			$public_products = array_map( 'wc_get_product', $query->get_posts() );

			if( $return ) {
				return $public_products;
			}

			$this->public_products = $public_products;
		}

		return $this->public_products;
	}

	// dev:improve Change the name of these functions, I don't like them
	public function get_publicly_available_chapters() {

		$chapter_terms = array();
		foreach( $this->get_public_products() as $product ) {
			$chapter_terms = array_merge( $chapter_terms, 
				get_terms( 
					array( 
						'object_ids' 	=> $product->get_id(), 
						'taxonomy' 		=> 'chapters',
					) 
				)
			);
		}

		return array_filter( array_unique( $chapter_terms, SORT_REGULAR ) );
	}

	/**
	 * Get publicly available categories split into two parent (Apparel Styles & Themes and Events)
	 * 
	 * @return 	array
	 */
	public function get_publicly_available_categories() {

		// Categories to ignore
		// If category doesn't exist, the array_column below will ignore it
		$ignore_cats = array(
			'homepage_featured'	=> get_term_by( 'name', 'Homepage Featured', 'product_cat' ),
			'uncategorized'		=> get_term_by( 'name', 'Uncategorized', 'product_cat' ),
		);

		$apparel_styles_cat = get_term_by( 'name', 'Apparel Styles', 'product_cat' );
		$themes_and_events_cat = get_term_by( 'name', 'Themes and Events', 'product_cat' );

		$publicly_available_categories = array();
		foreach( $this->get_public_products() as $product ) {

			foreach( $product->get_category_ids() as $category_id ) {

				$category = get_term( $category_id, 'product_cat' );

				if( $category->parent ) {

					if( $apparel_styles_cat 
						&& ( $category->parent == $apparel_styles_cat->term_id )
					) {

						$publicly_available_categories[ $apparel_styles_cat->name ][ $category->term_id ] = $category;
					}elseif( $themes_and_events_cat
						&& ( $category->parent == $themes_and_events_cat->term_id )
					) {

						$publicly_available_categories[ $themes_and_events_cat->name ][ $category->term_id ] = $category;
					}
				}
			}
		}

		// Make sure parent categories are sorted alphabetically
		ksort( $publicly_available_categories );

		foreach( $publicly_available_categories as &$public_parent_categories ) {

			// Sort children alphabetically as well
			usort( $public_parent_categories, function( $a, $b ) { return strcmp( $a->name, $b->name ); } );
		}
		return $publicly_available_categories;
	}

	public static function get_from_public_products($type = null, $search_term = null){
		global $wpdb;

		$select_query = "SELECT DISTINCT wp_terms.name, wp_terms.term_id, wp_terms.slug, wp_term_taxonomy.parent, wp_term_taxonomy.taxonomy, wp_term_taxonomy.count, wp_term_taxonomy.term_taxonomy_id".
		" FROM wp_term_relationships A, wp_term_relationships B";
		
		$join_query = " INNER JOIN wp_terms".
		" ON B.term_taxonomy_id = wp_terms.term_id".
		" INNER JOIN wp_term_taxonomy".
		" ON wp_terms.term_id = wp_term_taxonomy.term_taxonomy_id";

		$where_query = " WHERE A.term_taxonomy_id = 5337 AND A.object_id = B.object_id";

		if ($type == "organizations") {
			$join_query = $join_query . " INNER JOIN wp_omh_organizations ON wp_omh_organizations.term_id = wp_term_taxonomy.term_id";
		}
		
		if ($type == "chapters") {
			$join_query = $join_query . " INNER JOIN wp_omh_chapters ON wp_omh_chapters.term_id = wp_term_taxonomy.term_id";
		}

		if ($type == "orgs_chapter"){
			$join_query = $join_query . " INNER JOIN wp_omh_chapters ON wp_omh_chapters.term_id = wp_term_taxonomy.term_id";
			$join_query = $join_query . " INNER JOIN wp_omh_organizations ON wp_omh_organizations.term_id = wp_term_taxonomy.term_id";
		}
		
		if ($type == "categories") {
			$apparel_styles_cat = get_term_by( 'name', 'Apparel Styles', 'product_cat' );
			$themes_and_events_cat = get_term_by( 'name', 'Themes and Events', 'product_cat' );

			$where_query = $where_query . " AND (parent = {$apparel_styles_cat->term_taxonomy_id}".
			" OR parent = {$themes_and_events_cat->term_taxonomy_id})";
		}
		
		if (isset($search_term)) {
			$where_query = $where_query . " AND " . "wp_terms.name" . " LIKE '%" . $search_term . "%'";
			// $where_query = $where_query . " OR " . "wp_terms.slug" . " = '" . $search_term . "'";
		}

		$order_query = " ORDER BY wp_term_taxonomy.parent ASC, wp_term_taxonomy.count DESC";

		$query = $select_query . $join_query . $where_query. $order_query;
		$res = $wpdb->get_results($query);
		return $res;
	}

	public static function get_all_public_categories()
	{
		$res = OMH_Model::get_from_public_products("categories");

		$publicly_available_categories = array();
		$apparel_styles_cat = get_term_by( 'name', 'Apparel Styles', 'product_cat' );
		$themes_and_events_cat = get_term_by( 'name', 'Themes and Events', 'product_cat' );

		$public_apparel_styles = array();
		$public_themes_and_events = array();
		foreach($res as $key => $obj){
			// Add to Apparel Styles
			if($obj->parent == $apparel_styles_cat->term_taxonomy_id){
				$publicly_available_categories[$apparel_styles_cat->name][$obj->term_taxonomy_id] = $obj;
			}
			else if ($obj->parent == $themes_and_events_cat->term_taxonomy_id){ 
				$publicly_available_categories[$themes_and_events_cat->name][$obj->term_taxonomy_id] = $obj;
			}
		}
		return $publicly_available_categories;
	}

	public static function get_all_public_organizations(){
			$res = OMH_Model::get_from_public_products("organizations");
			return $res;
	}

	public static function search_public_organizations($search_term){
			$res = OMH_Model::get_from_public_products("organizations", $search_term);
			$public_org = array();
			foreach($res as $value){
				$org = array();
				$org['text'] = $value->name;
				$org['slug'] = $value->slug;
				$org['id'] = $value->term_id;
				$org['type'] = "Organization";
				array_push($public_org, $org);
			}
			return $public_org;
	}

	public static function get_all_public_chapters(){
		$res = OMH_Model::get_from_public_products("chapters");
		return $res;
	}

	public static function search_public_chapters($search_term){
		$res = OMH_Model::get_from_public_products("chapters", $search_term);
		$public_chapter = array();
			foreach($res as $value){
				$chapter = array();
				$chapter['text'] = $value->name;
				$chapter['slug'] = $value->slug;
				$chapter['id']   = $value->term_id;
				$chapter['type'] = "Chapter";
				array_push($public_chapter, $chapter);
			}
			return $public_chapter;
	}

	public static function search_public_orgs_chapters($search_term){
		$res = OMH_Model::get_from_public_products("orgs_chapters", $search_term);
		$public_org_chapter = array();
			foreach($res as $value){
				$org_chapter = array();
				$org_chapter['text'] = $value->name;
				$org_chapter['slug'] = $value->slug;
				$org_chapter['id']   = $value->term_id;
				$org_chapter['taxonomy'] = $value->taxonomy;
				array_push($public_org_chapter, $org_chapter);
			}
			$results = array("results" => $public_org_chapter);
			return $results;
	}

	// Get publicly available tags
	public function get_publicly_available_tags() {

		$tag_ids = array();
		foreach( $this->get_public_products() as $product ) {
			$tag_ids = array_merge( $tag_ids, $product->get_tag_ids() );
		}

		return array_filter( array_unique( $tag_ids ) );
	}
}

trait OMH_Model_Trait_Term {

	/********************************
	 *			  Props				*
	 ********************************/

	protected $term_id = null;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	function get_term_id() {

		return $this->term_id;
	}

	function set_term_id( $term_id ) {

		$this->term_id = $term_id;

		return $this->get_term_id();
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	/**
	 * Make sure we have all of the data to get and create terms
	 * 
	 * @return 	bool
	 */
	function validate_term() {

		// If this model doesn't have a taxonomy, return false
		if( !$this->get_taxonomy()
			|| !$this->get_term_label()
			|| !$this->get_term_slug()
		) {

			return false;
		}

		return true;
	}

	/**
	 * Get the term and create it if it does not exist @see create_term()
	 * 
	 * @return 	bool|WP_Term
	 */
	function get_term() {

		if( !$this->validate_term() ) {
			return false;
		}

		/*
		 * If get_term_id() is null or 0, 
		 * let's try to create it
		 */
		if( !$this->get_term_id() ) {

			$term = get_term_by( 'slug', $this->get_term_slug(), $this->get_taxonomy() );

			if( $term ) {
				$this->term_id = $term->term_id;
				$this->save();
			} else {

				if( $term = $this->create_term() ) {

					$term = get_term_by( 'id', $term->term_id, $this->get_taxonomy() );
					$this->term_id = $term->term_id;
					$this->save();
				} else {
					return false;
				}
			}
		}

		$term = get_term_by( 'id', $this->get_term_id(), $this->get_taxonomy() );

		return $term;
	}

	/**
	 * Create the term
	 * 
	 * @return 	bool|WP_Term|null|WP_Error
	 */
	function create_term() {

		if( !$this->validate_term() ) {
			return false;
		}

		$term = wp_insert_term( $this->get_term_label(), $this->get_taxonomy(), array( 'slug' => $this->get_term_slug() ) );

		if( is_wp_error( $term ) ) {
			return false;
		}

		return get_term( $term['term_id'], $this->get_taxonomy() );
	}

	/**
	 * Update the term
	 * 
	 * @return 	bool|int|WP_Error
	 */
	function update_term() {

		if( !$this->validate_term() ) {
			return false;
		}

		$updated = wp_update_term(
			$this->get_term_id(),
			$this->get_taxonomy(),
			array(
				'name'	=> $this->get_term_label(),
				'slug'	=> $this->get_term_slug()
			)
		);

		if( !is_wp_error( $updated ) ) {
			return $updated['term_id'];
		} else {
			return false;
		}
	}

	public function get_term_link() {

		return get_term_link( $this->get_term() );
	}

	/**
	 * Return the string that will be used as the slug when creating/updating terms
	 * 
	 * @return 	bool
	 */
	public function get_term_slug() {
		return false;
	}

	/**
	 * Return the string that will be used as the label when creating/updating terms
	 * 
	 * @return 	bool
	 */
	public function get_term_label() {
		return false;
	}
}
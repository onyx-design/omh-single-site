<?php
defined( 'ABSPATH' ) || exit;

class OMH_Model_Garment_Style extends OMH_Model {
	use OMH_Model_Trait_Term;

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_garment_styles';

	protected static $factory = 'garment_style_factory';

	protected static $taxonomy = 'pa_garment';

	public static $defaults = array(
		'style_full_name'	=> '',
		'style_slug'		=> '',
		'inksoft_style_id'	=> 0,
		'full_name'			=> '',
		'garment_color_id'	=> '',
		'sizes'				=> '',
		'style_base_cost'	=> '',
		'inksoft_image_url'	=> '',
		'enabled'			=> 0,
		'thumbnail_url'		=> '',
	);

	public static $schema = array(
		'style_full_name',
		'style_slug',
		'inksoft_style_id',
		'garment_product_id',
		'garment_color_id',
		'sizes',
		'style_base_cost',
		'inksoft_image_url',
		'enabled',
	);

	/********************************
	 *			 Columns			*
	 ********************************/

	protected $style_full_name;

	/**
	 * @var string
	 */
	protected $style_slug;

	/**
	 * @var int
	 */
	public $inksoft_style_id;

	/**
	 * @var int
	 */
	protected $garment_product_id;

	/**
	 * @var int
	 */
	protected $garment_color_id;

	/**
	 * @var string
	 */
	protected $sizes;

	/**
	 * Base Cost/InkSoft Size Unit Price
	 * 
	 * If there are multiple unit prices across the style's sizes, use the lowest price.
	 * 
	 * @var string
	 */
	protected $style_base_cost;

	/**
	 * @var string
	 */
	protected $inksoft_image_url;

	/**
	 * @var int
	 */
	protected $enabled;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_style_full_name() {

		return $this->style_full_name;
	}

	public function set_style_full_name( $style_full_name ) {

		$this->style_full_name = $style_full_name;

		return $this->get_style_full_name();
	}

	/**
	 * Get the Style Slug
	 * 
	 * @return 	string
	 */
	public function get_style_slug() {

		return $this->style_slug;
	}

	/**
	 * Set the Style Slug
	 * 
	 * @param 	string	$style_slug
	 * 
	 * @return 	string
	 */
	public function set_style_slug( $style_slug ) {

		$this->style_slug = $style_slug;

		return $this->get_style_slug();
	}

	/**
	 * Get the InkSoft Style ID
	 * 
	 * @return 	int
	 */
	public function get_inksoft_style_id() {

		return $this->inksoft_style_id;
	}

	/**
	 * Set the InkSoft Style ID
	 * 
	 * @param 	int 	$inksoft_style_id
	 * 
	 * @return 	int
	 */
	public function set_inksoft_style_id( $inksoft_style_id ) {

		$this->inksoft_style_id = $inksoft_style_id;

		return $this->get_inksoft_style_id();
	}

	/**
	 * Get the Garment Product ID
	 * 
	 * @return 	int
	 */
	public function get_garment_product_id() {

		return $this->garment_product_id;
	}

	/**
	 * Set the Garment Product ID
	 * 
	 * @param 	int 	$garment_product_id
	 * 
	 * @return 	int
	 */
	public function set_garment_product_id( $garment_product_id ) {

		$this->garment_product_id = $garment_product_id;

		return $this->get_garment_product_id();
	}

	/**
	 * Get the Garment Color ID
	 * 
	 * @return 	int
	 */
	public function get_garment_color_id() {

		return $this->garment_color_id;
	}

	/**
	 * Set the Garment Color ID
	 * 
	 * @param 	int 	$garment_color_id
	 * 
	 * @return 	int
	 */
	public function set_garment_color_id( $garment_color_id ) {

		$this->garment_color_id = $garment_color_id;

		return $this->get_garment_color_id();
	}

	/**
	 * Get Sizes
	 * 
	 * @return 	string
	 */
	public function get_sizes() {

		return $this->sizes;
	}

	/**
	 * Set Sizes
	 * 
	 * @param 	string	$sizes
	 * 
	 * @return 	string
	 */
	public function set_sizes( $sizes ) {

		$this->sizes = $sizes;

		return $this->get_sizes();
	}

	/**
	 * Get Style Base Cost
	 * 
	 * @return 	string
	 */
	public function get_style_base_cost() {

		return $this->style_base_cost;
	}

	/**
	 * Set Style Base Cost
	 * 
	 * @param 	string	$style_base_cost
	 * 
	 * @return 	string
	 */
	public function set_style_base_cost( $style_base_cost ) {

		$this->style_base_cost = $style_base_cost;

		return $this->get_style_base_cost();
	}

	/**
	 * Get InkSoft Image URL
	 * 
	 * @return 	string
	 */
	public function get_inksoft_image_url() {

		return $this->inksoft_image_url;
	}

	/**
	 * Set InkSoft Image URL
	 * 
	 * @param 	string	$inksoft_image_url
	 * 
	 * @return 	string
	 */
	public function set_inksoft_image_url( $inksoft_image_url ) {

		$this->inksoft_image_url = $inksoft_image_url;

		return $this->get_inksoft_image_url();
	}

	/**
	 * Get enabled property
	 * 
	 * @return 	int
	 */
	public function get_enabled() {

		return $this->enabled;
	}

	/**
	 * Set enabled property
	 * 
	 * @param 	int 	$enabled
	 * 
	 * @return 	int
	 */
	public function set_enabled( $enabled ) {

		$this->enabled = $enabled;

		return $this->get_enabled();
	}

	/********************************
	 *			Functions			*
	 ********************************/

	public function __sleep() {

		if( $garment_color = $this->get_garment_color() ) {

			$this->color_code = $garment_color->color_code;
			$this->color_name = $garment_color->color_name;
		}

		return array_keys( $this->to_array() );
	}

	public static function get_search_args( $search_terms = '' ) {

		$search_args = array( 'compare' => 'OR' );

		foreach( explode( ' ', $search_terms ) as $search_term ) {

			$search_args[] = array(
				'name'		=> 'style_full_name',
				'value'		=> "%{$search_term}%",
				'compare'	=> 'LIKE'
			);
		}

		$full_search_args = array(
			'compare'	=> 'AND',
			array(
				'name'	=> 'enabled',
				'value'	=> 1
			),
			$search_args
		);

		return $full_search_args;
	}

	/********************************
	 *		 Event Listeners		*
	 ********************************/


	/********************************
	 *			 Helpers			*
	 ********************************/

	/**
	 * Get the Garment product
	 * 
	 * @return 	OMH_Model_Garment_Product|null
	 */
	public function get_garment_product() {

		if( !$this->get_garment_product_id() ) {
			return null;
		}

		if( !isset( $this->garment_product ) || ( null === $this->garment_product ) ) {
			$this->garment_product = OMH()->garment_product_factory->read( $this->get_garment_product_id() );
		}

		return $this->garment_product;
	}

	/**
	 * Return the Garment Color joined by the $garment_color_id
	 * 
	 * @return 	OMH_Model_Garment_Color|null
	 */
	public function get_garment_color() {

		if( !$this->get_garment_color_id() ) {
			return null;
		}

		if( !isset( $this->garment_color ) || ( null === $this->garment_color ) ) {
			$this->garment_color = OMH()->garment_color_factory->read( $this->get_garment_color_id() );
		}

		return $this->garment_color;
	}

	public function get_style_name() {

		$style_name = '';

		if( $this->get_garment_product()->get_garment_brand() ) {

			$garment_brand_name = $this->get_garment_product()->get_garment_brand()->get_brand_name();

			$style_name .= $garment_brand_name;
		}

		if( $this->get_garment_product() ) {

			$garment_product_name = $this->get_garment_product()->get_product_name();

			$style_name .= " - {$garment_product_name}";
		}

		if( $this->get_garment_color() ) {

			$garment_color_name = $this->get_garment_color()->get_color_name();

			$style_name .= " - {$garment_color_name}";
		}

		return $style_name;
	}

	public function get_ajax_search_select2_option() {
		//dev:improve //dev
		return array(
			'id'		=> $this->get_id(),
			'text'		=> $this->get_joined_label(),
			'thumb'		=> $this->get_inksoft_image_src(),
			'garment'	=> $this->to_array()
		);
	}

	public function get_term_slug() {
		return $this->get_style_slug();
	}

	public function get_term_label() {

		return $this->get_style_full_name();
	}

	public function get_joined_label() {

		return $this->get_style_full_name();
	}

	public function get_inksoft_image_src( $thumb = true ) {

		$image_src = '//stores.inksoft.com' . $this->get_inksoft_image_url();
		
		if( $thumb ) {
			$image_src = str_replace( '500.png', '150.png', $image_src );
		}

		return $image_src;
	}

	public static function build_style_full_names() {
		global $wpdb;

		$sql = "
			UPDATE `{$wpdb->prefix}omh_garment_styles` style
			LEFT JOIN `{$wpdb->prefix}omh_garment_products` product
				ON style.`garment_product_id` = product.`ID`
			LEFT JOIN `{$wpdb->prefix}omh_garment_brands` brand
				ON product.`garment_brand_id` = brand.`ID`
			LEFT JOIN `{$wpdb->prefix}omh_garment_colors` color
				ON style.`garment_color_id` = color.`ID`
			SET style.`style_full_name` = CONCAT( brand.`brand_name`, ' - ', product.`manufacturer_sku`, ' - ', product.`product_name`, ' - ', color.`color_name` );
		";

		$wpdb->query( $sql );
	}

	/**
	 * Get or Create the OMH Garment Style from the Style response
	 * 
	 * @param 	OMH_API_Response 	$api_response 
	 * 
	 * @return 	OMH_Model_Garment_Color
	 */
	public static function get_or_create_from_api_response( $api_response ) {

		if( !isset( $api_response->ID ) ) {
			return false;
		}

		$garment_style_factory = self::get_factory();

		$garment_style = $garment_style_factory->get_by_inksoft_id( $api_response->ID );

		$garment_product = OMH()->garment_product_factory->get_by_inksoft_id( $api_response->ProductId );
		$garment_color = OMH_Model_Garment_Color::get_or_create_from_api_response( $api_response );
		$color_name = $garment_color->get_color_name();

		// Create a comma delimited string of sizes from the size array
		$sizes = isset( $api_response->Sizes ) ? implode( ',', array_column( $api_response->Sizes, 'Name' ) ) 	: '';
		$style_base_cost = isset( $api_response->Sizes ) ? min( array_column( $api_response->Sizes, 'UnitPrice' ) ) : '';
		$inksoft_image_url = isset( $api_response->ImageFilePath_Front ) ? $api_response->ImageFilePath_Front : '';
		$style_slug = $garment_product && $color_name ? "{$garment_product->get_product_slug()}_" . sanitize_title( $color_name ) : '';

		$style_data = array(
			'style_slug'		=> $style_slug,
			'inksoft_style_id'	=> $api_response->ID,
			'garment_product_id'=> $garment_product ? $garment_product->get_id() : 0,
			'garment_color_id'	=> $garment_color ? $garment_color->get_id() : '',
			'sizes'				=> $sizes,
			'style_base_cost'	=> $style_base_cost,
			'inksoft_image_url'	=> str_replace( '?decache=', '', $inksoft_image_url ),
		);

		if( !$garment_style ) {
			$garment_style = $garment_style_factory->create( $style_data );
		} else {
			$garment_style = $garment_style_factory->update( $garment_style->get_id(), $style_data );
		}

		// Make sure sizes exist
		foreach( $api_response->Sizes as $size ) {
			omh_get_or_create_wc_attribute_term( 'pa_size', $size->Name, 'size-' . $size->Name );
		}

		return $garment_style;
	}
}
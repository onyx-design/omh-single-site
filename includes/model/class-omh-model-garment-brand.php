<?php
defined( 'ABSPATH' ) || exit;

class OMH_Model_Garment_Brand extends OMH_Model {

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_garment_brands';

	protected static $factory = 'garment_brand_factory';

	public static $defaults = array(
		'brand_slug'		=> '',
		'brand_code'		=> '',
		'inksoft_brand_id'	=> 0,
		'brand_name'		=> '',
		'brand_image_url'	=> '',
		'size_chart_url'	=> '',
	);

	/********************************
	 *			 Columns			*
	 ********************************/

	public $brand_slug;

	public $brand_code;

	public $inksoft_brand_id;

	public $brand_name;

	public $brand_image_url;

	public $size_chart_url;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_brand_slug() {

		return $this->brand_slug;
	}

	public function set_brand_slug( $brand_slug ) {

		$this->brand_slug = $brand_slug;

		return $this->get_brand_slug();
	}

	public function get_brand_code() {

		return $this->brand_code;
	}

	public function set_brand_code( $brand_code ) {

		$this->brand_code = $brand_code;

		return $this->get_brand_code();
	}

	public function get_inksoft_brand_id() {

		return $this->inksoft_brand_id;
	}

	public function set_inksoft_brand_id( $inksoft_brand_id ) {

		$this->inksoft_brand_id = $inksoft_brand_id;

		return $this->get_inksoft_brand_id();
	}

	public function get_brand_name() {

		return $this->brand_name;
	}

	public function set_brand_name( $brand_name ) {

		$this->brand_name = $brand_name;

		return $this->get_brand_name();
	}

	public function get_brand_image_url() {

		return $this->brand_image_url;
	}

	public function set_brand_image_url( $brand_image_url ) {

		$this->brand_image_url = $brand_image_url;

		return $this->get_brand_image_url();
	}

	public function get_size_chart_url() {

		return $this->size_chart_url;
	}

	public function set_size_chart_url( $size_chart_url ) {

		$this->size_chart_url = $size_chart_url;

		return $this->get_size_chart_url();
	}

	/********************************
	 *			Functions			*
	 ********************************/

	public function is_valid() {

		if( $this->get_brand_slug() && $this->get_brand_code() ) {
			return true;
		}

		return false;
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	public static function get_inksoft_brands( $inksoft_brand_id = null ) {

		$api_response = OMH_API_Inksoft::request( 'get_manufacturers', array() );

		if( $api_response->data() ) {

			$inksoft_brands = array_column( $api_response->data(), null, 'ID' );

			if( $inksoft_brand_id ) {

				return isset( $inksoft_brands[ $inksoft_brand_id ] ) ? $inksoft_brands[ $inksoft_brand_id ] : false;
			}
		}

		return array();
	}

	/**
	 * Populate the Garment Brands table
	 * 
	 * Returns true when every brand is successfully inserted,
	 * or an array of errors if they exist
	 * 
	 * @return 	bool|array
	 */
	public static function populate_garment_brands() {
		global $wpdb;

		$errors = array();

		$brands_json = '{"OK":true,"Pagination":null,"Messages":[],"Data":[{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":2,"ID":1000022,"BrandImageUrl":"/images/publishers/8526/brands/adidas/200w.gif","SizeChartUrl":null,"Name":"adidas"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":2,"ID":1000024,"BrandImageUrl":"/images/publishers/8526/brands/alo/200w.gif","SizeChartUrl":null,"Name":"alo"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":1,"ID":1000025,"BrandImageUrl":"/images/publishers/8526/brands/AloSport/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/AloSport.png","Name":"Alo Sport"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":8,"ID":1000015,"BrandImageUrl":"/images/publishers/8526/brands/Alternative/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/Alternative.png","Name":"Alternative"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":9,"ID":1000016,"BrandImageUrl":"/images/publishers/8526/brands/AmericanApparel/200w.gif","SizeChartUrl":null,"Name":"American Apparel"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":5,"ID":1000004,"BrandImageUrl":"/images/publishers/8526/brands/Anvil/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/Anvil.png","Name":"Anvil"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":2,"ID":1000017,"BrandImageUrl":"/images/publishers/8526/brands/AugustaSportswear/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/AugustaSportswear.png","Name":"Augusta Sportswear"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":2,"ID":1000018,"BrandImageUrl":"/images/publishers/8526/brands/Badger/200w.gif","SizeChartUrl":null,"Name":"Badger"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":15,"ID":1000000,"BrandImageUrl":"/images/publishers/8526/brands/Bella/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/Bella.png","Name":"Bella"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":33,"ID":1000007,"BrandImageUrl":"/images/publishers/8526/brands/Bella_Canvas/200w.gif","SizeChartUrl":null,"Name":"Bella + Canvas"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":3,"ID":1000014,"BrandImageUrl":"/images/publishers/8526/brands/Canvas/200w.gif","SizeChartUrl":null,"Name":"Canvas"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":1,"ID":1000021,"BrandImageUrl":"/images/publishers/8526/brands/Champion/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/Champion.png","Name":"Champion"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":19,"ID":1000012,"BrandImageUrl":"/images/publishers/8526/brands/ComfortColors/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/ComfortColors.png","Name":"Comfort Colors"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":1,"ID":1000013,"BrandImageUrl":"/images/publishers/8526/brands/FruitoftheLoom/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/FruitoftheLoom.png","Name":"Fruit of the Loom"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":88,"ID":1000001,"BrandImageUrl":"/images/publishers/8526/brands/Gildan/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/Gildan.png","Name":"Gildan"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":6,"ID":1000002,"BrandImageUrl":"/images/publishers/8526/brands/Hanes/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/Hanes.png","Name":"Hanes"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":1,"ID":1000020,"BrandImageUrl":"/images/publishers/8526/brands/HanesStedman/200w.gif","SizeChartUrl":null,"Name":"Hanes Stedman"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":7,"ID":1000023,"BrandImageUrl":"/images/publishers/8526/brands/IndependentTradingCo/200w.gif","SizeChartUrl":null,"Name":"Independent Trading Co."},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":4,"ID":1000009,"BrandImageUrl":"/images/publishers/8526/brands/J.America/200w.gif","SizeChartUrl":null,"Name":"J. America"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":4,"ID":1000003,"BrandImageUrl":"/images/publishers/8526/brands/Jerzees/200w.gif","SizeChartUrl":"https://images.inksoft.com/Content/images/SizeCharts/Jerzees.png","Name":"Jerzees"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":9,"ID":1000019,"BrandImageUrl":"/images/publishers/8526/brands/NextLevel/200w.gif","SizeChartUrl":null,"Name":"Next Level"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":3,"ID":1001026,"BrandImageUrl":"/images/publishers/8526/brands/NikeGolf/200w.gif","SizeChartUrl":null,"Name":"Nike Golf"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":10,"ID":1000008,"BrandImageUrl":"/images/publishers/8526/brands/PortAuthority/200w.gif","SizeChartUrl":"https://images.inksoft.com/images/publishers/8526/brands/PortAuthority/SizeChart.jpg","Name":"Port Authority"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":2,"ID":1000010,"BrandImageUrl":"/images/publishers/8526/brands/Sport-Tek/200w.gif","SizeChartUrl":"https://images.inksoft.com/images/publishers/8526/brands/Sport-Tek/SizeChart.jpg","Name":"Sport-Tek"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":1,"ID":1001029,"BrandImageUrl":"/images/publishers/8526/brands/Sportsman/200w.gif","SizeChartUrl":null,"Name":"Sportsman"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":1,"ID":1000026,"BrandImageUrl":"/images/publishers/8526/brands/ThreadfastApparel/200w.gif","SizeChartUrl":null,"Name":"Threadfast Apparel"},{"HasSizeChart":false,"HasBrandImage":false,"ProductCount":10,"ID":1000011,"BrandImageUrl":"/images/publishers/8526/brands/UltraPress/200w.gif","SizeChartUrl":null,"Name":"UltraPress"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":2,"ID":1001027,"BrandImageUrl":"/images/publishers/8526/brands/Valucap/200w.gif","SizeChartUrl":null,"Name":"Valucap"},{"HasSizeChart":true,"HasBrandImage":true,"ProductCount":3,"ID":1001028,"BrandImageUrl":"/images/publishers/8526/brands/YuPoong/200w.gif","SizeChartUrl":null,"Name":"YuPoong"}],"StatusCode":"OK"}';

		$brands = json_decode( $brands_json );

		foreach( $brands->Data as $brand ) {

			$data = array(
				'brand_slug'	=> '',
				'brand_code'	=> '',
				'inksoft_brand_id'	=> $brand->ID,
				'brand_name'			=> $brand->Name ?? '',
				'brand_image_url'	=> $brand->BrandImageUrl ?? '',
				'size_chart_url'	=> $brand->SizeChartUrl ?? '',
			);

			$inserted = $wpdb->insert( $wpdb->prefix . self::$table, $data );

			if( false === $inserted ) {

				$errors[] = array(
					'inksoft_brand_id'	=> $brand->ID,
					'message'			=> 'There was a problem inserting InkSoft Brand ID: ' . $brand->ID
				);
			}
		}

		return empty( $errors ) ? true : $errors;
	}

	/**
	 * Accepts Inksoft GetProduct API Response to check for Brand
	 * 
	 * WARNING: This requires a full GetProduct API Response since InkSoft does not have a single GetManufacturer function!!!
	 * 
	 * @param 	stdClass 	$api_response
	 * 
	 * @return 	OMH_Model_Garment_Brand
	 */
	public static function get_or_create_from_api_response( $api_response ) {

		if( !isset( $api_response->ID ) ) {
			return false;
		}

		$garment_brand_factory = self::get_factory();
		$garment_brand = $garment_brand_factory->get_by_inksoft_id( $api_response->ManufacturerId );

		$inksoft_brand_id = isset( $api_response->ManufacturerId ) ? $api_response->ManufacturerId : 0;

		if( $inksoft_brand = static::get_inksoft_brands( $inksoft_brand_id ) ) {
			
			$brand_name = $inksoft_brand->Name;
			$brand_image_url = $inksoft_brand->BrandImageUrl;
			$size_chart_url = $inksoft_brand->SizeChartUrl;
		} else {
			
			$brand_name = isset( $api_response->Manufacturer ) ? $api_response->Manufacturer : '';
			$brand_image_url = '';
			$size_chart_url = '';
		}

		$brand_data = array(
			'inksoft_brand_id'	=> $inksoft_brand_id,
			'brand_name'		=> $brand_name,
			'brand_image_url'	=> $brand_image_url,
			'size_chart_url'	=> $size_chart_url,
		);

		if( !$garment_brand ) {
			$garment_brand = $garment_brand_factory->create( $brand_data );
		} else {
			$garment_brand = $garment_brand_factory->update( $garment_brand->get_id(), $brand_data );
		}

		return $garment_brand;
	}
}
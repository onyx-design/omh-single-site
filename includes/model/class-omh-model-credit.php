<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Model_Credit extends OMH_Model {

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_credit';

	protected static $factory = 'credit_factory';

	public static $defaults = array(
		'type'			=> 'credit',
		'amount'		=> 0,
		'order_id'		=> 0,
		'chapter_id'	=> 0,
		'notes'			=> '',
		'ledger_date'	=> '0000-00-00 00:00:00'
	);

	public static $required = array(
		'type',
		'amount',
		'order_id',
		'chapter_id',
	);

	public static $fields = array(
		'type'			=> array(
			'label'	=> 'Type',
		),
		'amount'		=> array(
			'label'	=> 'Amount'
		),
		'order_id'		=> array(
			'label'	=> 'Order ID',
		),
		'chapter_id'	=> array(
			'label'	=> 'Chapter ID',
		),
		'notes'			=> array(
			'label'	=> 'Notes',
		),
		'ledger_date'	=> array(
			'label'	=> 'Leger Date',
		)
	);

	/********************************
	 *			 Columns			*
	 ********************************/

	/**
	 * @var string
	 */
	protected $type;

	/**
	 * @var string
	 */
	protected $amount;

	/**
	 * @var int
	 */
	protected $order_id;

	/**
	 * @var int
	 */
	protected $chapter_id;

	/**
	 * @var string
	 */
	protected $notes;

	/**
	 * @var string
	 */
	protected $ledger_date;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	/**
	 * Get the OMH Credit type
	 * 
	 * @param 	bool 	$raw 
	 * @return 	string
	 */
	public function get_type( $raw = true ) {

		if( $raw ) {
			return $this->type;
		} else {
			return ucfirst( $this->type );
		}
	}

	public function set_type( $type ) {

		$this->type = $type;

		return $this->get_type();
	}

	/**
	 * Get the amount in dollars
	 * 
	 * @return 	string
	 */
	public function get_amount( $raw = true ) {

		if( $raw ) {
			return $this->amount;
		} else {
			return number_format( ( $this->amount / 100 ), 2, '.', '' );
		}
	}

	public function set_amount( $amount ) {

		$this->amount = $amount;

		return $this->get_amount();
	}

	public function get_order_id() {

		return $this->order_id;
	}

	public function set_order_id( $order_id ) {

		$this->order_id = $order_id;

		return $this->get_order_id();
	}

	public function get_chapter_id() {

		return $this->chapter_id;
	}

	public function set_chapter_id( $chapter_id ) {

		$this->chapter_id = $chapter_id;

		return $this->get_chapter_id();
	}

	public function get_notes() {

		return $this->notes;
	}

	public function set_notes( $notes ) {

		$this->notes = $notes;

		return $this->get_notes();
	}

	/**
	 * Get the Ledger Date
	 * 
	 * @return 	string
	 */
	public function get_ledger_date() {

		return $this->ledger_date;
	}

	public function set_ledger_date( $ledger_date ) {

		$this->ledger_date = $ledger_date;

		return $this->get_ledger_date();
	}

	/********************************
	 *			Functions			*
	 ********************************/

	/**
	 * Get the array of OMH Credit types
	 * 
	 * @return 	array
	 */
	public static function get_types() {

		return array( 'credit', 'debit', 'adjustment' );
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	/**
	 * Get the Credit Type badge
	 * 
	 * @return 	string|null
	 */
	public function get_type_badge() {

		$type_badges = array(
			'credit'	=> array(
				'label'	=> 'Credit',
				'color' => 'success'
			),
			'debit'	=> array(
				'label'	=> 'Debit',
				'color' => 'warning'
			), 
			'adjustment'=> array(
				'label'	=> 'Adjustment',
				'color'	=> 'secondary'
			)
		);

		$type = $this->get_type();

		if( isset( $type_badges[ $type ] ) ) {

			return OMH_HTML_UI_Badge::factory(
				$type_badges[ $type ]
			);
		}

		return null;
	}

	/**
	 * Return if Credit has a Order
	 * 
	 * @return 	bool
	 */
	public function has_order() {

		if( $this->order_id ) {
			return true;
		}

		return false;
	}

	/**
	 * Get the Order if it exists
	 * 
	 * @return 	WP_Post|null
	 */
	public function get_order() {

		// dev:improve Return the object that it is
		if( $this->has_order() ) {
			return wc_get_order( $this->order_id );
		}

		return null;
	}

	/**
	 * Get the OMH Credit description
	 * 
	 * @return 	string;
	 */
	public function get_description() {

		$credit_type = $this->get_type( false );
		$source = "from Merch House";

		if( $this->has_order() ) {

			if( $order = $this->get_order() ) {

				if( $this->get_amount() < 0 ) {
					$credit_type = 'Credit applied to';
				} else {
					$credit_type = 'Credit from';
				}

				$source = "Order #{$order->get_order_number()}";
			}
		}

		return "$credit_type $source";
	}
}
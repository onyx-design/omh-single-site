<?php
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_Model_Temporary_File_Upload extends OMH_Model {

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_temp_uploads';

	protected static $factory = 'temp_upload_factory';

	public static $defaults = array(
		'file_name'			=> '',
		'file_path'			=> '',
		'file_url'			=> '',
		'thumbnail_path'	=> '',
		'thumbnail_url'		=> '',
	);

	public static $unique = array(
		// 'file_name',
		// 'file_path',
		// 'file_url',
		// 'thumbnail_path',
		// 'thumbnail_url'
	);

	public static $required = array(
		'file_name',
		'file_path',
		'file_url'
	);

	public static $fields = array(
		'file_name'			=> array(
			'label'		=> 'File Name'
		),
		'file_path'			=> array(
			'label'		=> 'File Name'
		),
		'file_url'			=> array(
			'label'		=> 'File Name'
		),
		'thumbnail_path'	=> array(
			'label'		=> 'File Name'
		),
		'thumbnail_url'		=> array(
			'label'		=> 'File Name'
		),
	);

	/********************************
	 *			 Columns			*
	 ********************************/

	protected $file_name;

	protected $file_path;

	protected $file_url;

	protected $thumbnail_path;

	protected $thumbnail_url;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_file_name() {

		return $this->file_name;
	}

	public function set_file_name( $file_name ) {

		$this->file_name = $file_name;

		return $this->get_file_name();
	}

	public function get_file_path() {

		return $this->file_path;
	}

	public function set_file_path( $file_path ) {

		$this->file_path = $file_path;

		return $this->get_file_path();
	}

	public function get_file_url() {

		return $this->file_url;
	}

	public function set_file_url( $file_url ) {

		$this->file_url = $file_url;

		return $this->get_file_url();
	}

	public function get_thumbnail_path() {

		return $this->thumbnail_path;
	}

	public function set_thumbnail_path( $thumbnail_path ) {

		$this->thumbnail_path = $thumbnail_path;

		return $this->get_thumbnail_path();
	}

	public function get_thumbnail_url() {

		return $this->thumbnail_url;
	}

	public function set_thumbnail_url( $thumbnail_url ) {

		$this->thumbnail_url = $thumbnail_url;

		return $this->get_thumbnail_url();
	}

	/********************************
	 *			Functions			*
	 ********************************/

	public static function get_search_args( $search_term = '' ) {

		$search_term = str_replace( ' ', '%', $search_term );

		return array(
			array(
				'name'		=> 'file_name',
				'value'		=> "%$search_term%",
				'compare'	=> 'LIKE'
			),
			'compare'	=> 'OR'
		);
	}

	/********************************
	 *		 Event Listeners		*
	 ********************************/

	public function before_save() {
		
	}

	/********************************
	 *			 Helpers			*
	 ********************************/
}
<?php
defined( 'ABSPATH' ) || exit;

class OMH_Model_Organization extends OMH_Model {
	use OMH_Model_Trait_Term;

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_organizations';

	protected static $factory = 'organization_factory';

	protected static $taxonomy = 'organizations';

	public static $defaults = array(
		'term_id'			=> 0,
		'logo_image_id'		=> 0,
		'banner_image_id'	=> 0,
		'org_greek_letters'	=> ''
	);

	public static $unique = array(
		'org_name',
		'org_shortname',
		'org_letters',
		'org_greek_letters',
	);

	public static $required = array(
		'org_name',
		'org_shortname',
		'org_letters',
	);

	public static $fields = array(
		'term_id'			=> array(
			'label'		=> 'Term ID',
		),
		'logo_image_id' 	=> array(
			'label'		=> 'Logo'
		),
		'banner_image_id' 	=> array(
			'label'		=> 'Banner'
		),
		'org_name'			=> array(
			'label'		=> 'Organization Name'
		),
		'org_shortname'		=> array(
			'label'		=> 'Organization Shortname'
		),
		'org_letters'		=> array(
			'label'		=> 'Organization Letters'
		),
		'org_greek_letters' => array(
			'label'		=> 'Greek Letters'
		)
	);

	/********************************
	 *			 Columns			*
	 ********************************/

	protected $org_name;

	protected $logo_image_id;

	protected $banner_image_id;

	protected $org_letters;

	protected $org_shortname;

	protected $org_greek_letters;

	/********************************
	 *		 Getters/Setters		*
	 ********************************/

	public function get_org_name() {

		return $this->org_name;
	}

	public function set_org_name( $org_name ) {

		$this->org_name = $org_name;

		return $this->get_org_name();
	}

	public function get_logo_image_id() {

		return $this->logo_image_id;
	}

	public function set_logo_image_id( $logo_image_id ) {

		if( is_array( $logo_image_id ) ) {

			if( isset( $logo_image_id['id'] ) ) {

				if( !empty( maybe_str_to_bool( $logo_image_id['is_temp'] ) ) ) {

					$new_logo_image_id = OMH_Tool_Sideload_Media::insert_media_file_from_temp( $logo_image_id['id'] );

					if( $new_logo_image_id ) {

						$logo_image_id = $new_logo_image_id['id'];
					}
				}
				else {

					$logo_image_id = $logo_image_id['id'];
				}
			}
		}

		$this->logo_image_id = $logo_image_id;
	}

	public function get_banner_image_id() {

		return $this->banner_image_id;
	}

	public function set_banner_image_id( $banner_image_id ) {

		if( is_array( $banner_image_id ) ) {

			if( isset( $banner_image_id['id'] ) ) {

				if( !empty( maybe_str_to_bool( $banner_image_id['is_temp'] ) ) ) {

					$new_banner_image_id = OMH_Tool_Sideload_Media::insert_media_file_from_temp( $banner_image_id['id'] );

					if( $new_banner_image_id ) {

						$banner_image_id = $new_banner_image_id['id'];
					}
				} else {

					$banner_image_id = $banner_image_id['id'];
				}
			}
		}

		$this->banner_image_id = $banner_image_id;
	}

	public function get_org_shortname() {

		return $this->org_shortname;
	}

	public function set_org_shortname( $org_shortname ) {

		$this->org_shortname = $org_shortname;

		return $this->get_org_shortname();
	}

	public function get_org_letters() {

		return $this->org_letters;
	}

	public function set_org_letters( $org_letters ) {

		$this->org_letters = $org_letters;

		return $this->get_org_letters();
	}

	public function get_org_greek_letters() {

		return $this->org_greek_letters;
	}

	public function set_org_greek_letters( $org_greek_letters ) {

		$this->org_greek_letters = $org_greek_letters;

		return $this->get_org_greek_letters();
	}

	/********************************
	 *			Functions			*
	 ********************************/

	public function get_banner_image( $image_size = 'woocommerce_thumbnail' ) {

		if( $banner_src = wp_get_attachment_image_src( $this->get_banner_image_id(), $image_size ) ) {
			return $banner_src[0];
		}

		return false;
	}

	public function get_logo_image( $return_full_src = false ) {

		if( $logo_src = wp_get_attachment_image_src( $this->get_logo_image_id(), 'woocommerce_thumbnail' ) ) {

			return $return_full_src ? $logo_src : $logo_src[0];
		}

		return false;
	}

	public static function get_search_args( $search_term = '' ) {

		$search_term = str_replace( ' ', '%', $search_term );

		return array(
			array(
				'name'		=> 'org_name',
				'value'		=> "%$search_term%",
				'compare'	=> 'LIKE'
			),
			array(
				'name'		=> 'org_letters',
				'value'		=> "%$search_term%",
				'compare'	=> 'LIKE'
			),
			array(
				'name'		=> 'org_shortname',
				'value'		=> "%$search_term%",
				'compare'	=> 'LIKE'
			),
			'compare'	=> 'OR'
		);
	}



	/********************************
	 *		 Event Listeners		*
	 ********************************/

	public function before_save() {

		// If term isn't set, create it
		// if( !$this->get_term_id() ) {
		// 	$this->set_term_id( $this->create_term() );
		// } else {
		// 	$this->set_term_id( $this->update_term() );
		// }
	}

	/********************************
	 *			 Helpers			*
	 ********************************/

	public function get_term_slug() {
		return $this->get_org_shortname();
	}

	public function get_term_label() {
		return $this->get_org_name();
	}

	public function get_joined_label() {
		return $this->get_org_name();
	}
}
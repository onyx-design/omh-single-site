<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OMH_Table {

	protected $query = array();

	protected $row_template = '';

	protected $options = array(
		'sections'		=> array(
			'before'		=> false,
			'search'		=> false,
			'tabs'			=> false,
			'headers'		=> true,
			'results'		=> true,
			'no_results'	=> false,
			'pagination'	=> false,
			'after'			=> false
		),
		'show_headers'	=> true,
	);

	protected $response = array(
		'sections'	=> array(),
		'query'		=> array()
	);

	protected $defaults = array(
		'sections'		=> array(
			'before'		=> false,
			'search'		=> false,
			'tabs'			=> false,
			'headers'		=> true,
			'no_results'	=> false,
			'results'		=> true,
			'pagination'	=> false,
			'after'			=> false
		),
		'query'			=> array(
			'orderby'		=> 'ID',
			'order'			=> 'DESC',
			'posts_per_page'=> -1
		),
		'pagination'	=> array(
			'posts_per_page'	=> 20,
			'paged'				=> 1
		),
		'title'			=> array(),
		'filters'		=> array(),
		'tabs'			=> array()
	);

	protected $filters = array();

	protected $scopes = false;

	protected $title = false;

	/**
	 * Child classes can set this as an associative array to render tabs in the table
	 */
	protected $tabs = false;

	protected $headers = array();

	protected $pagination = array();

	protected $results = null;

	protected $show_dropdown = true;

	protected $results_count = null;

	protected $chapter_scope = true;

	protected $wild_card = '*';

	/**
	 * Pagination
	 */
	protected $total_results = 0;

	protected $total_pages = 0;

	public $no_results_msg = 'No results to display.';

	public $ui_table = null;

	public static function factory( $args ) {

		if( is_array( $args )
			&& isset( $args['type'] )
		) {

			$classname = class_exists( 'OMH_Table_' . $args['type'] ) ? 'OMH_Table_' . $args['type'] : static::class;

			return new $classname( $args ); 
		}

		return null;
	}

	protected  function __construct( $args ) {

		foreach( $args as $arg => $value ) {

			if( property_exists( $this, $arg ) ) {

				$this->$arg = array_replace_recursive( $this->defaults[ $arg ], $value );
			}
		}
	}

	public function set_no_results() {

		$this->sections['headers'] = false;
		$this->sections['results'] = false;
		$this->sections['pagination'] = false;
		$this->sections['no_results'] = true;
	}

	public function get_total_pages() {

		return $this->total_pages;
	}

	public function set_total_pages( $total_pages ) {

		$this->total_pages = $total_pages;

		return $this;
	}

	public function get_response() {

		$response = array(
			'sections'		=> $this->get_html(),
			'query'			=> $this->query,
		);

		if( $this->sections['pagination'] ) {
			$response['pagination'] = $this->get_pagination();
		}

		return $response;
	}

	/**
	 * Return a new version of the query with parsed values for WP_Query without modifying $this->query
	 * 
	 * @param 	array|null 	$query
	 * 
	 * @return 	array
	 */
	protected function parse_query( $query = null ) {

		if( isset( $this->query['search'] ) && $this->query['search'] ) {
			$this->parse_search();
		} else {
			$this->remove_search();
		}

		$parsed_query = $query ?? array_merge( $this->query, $this->get_pagination() );

		if( isset( $parsed_query['orderby'] ) ) {

			if( 0 === strpos( $parsed_query['orderby'], 'metaval:') ) {

				$parsed_query['meta_key'] = str_replace( 'metaval:', '', $parsed_query['orderby'] );
				$parsed_query['orderby'] = 'meta_value';
			}
		}

		if( isset( $parsed_query['search'] ) ) {
			$parsed_query['search'] = "{$this->wild_card}{$parsed_query['search']}{$this->wild_card}";
		}

		return $parsed_query;
	}

	protected function parse_search() {

	}

	protected function remove_search() {

	}

	protected function get_pagination( $arg = false ) {

		$pagination = $this->pagination;

		if( $arg 
			&& isset( $pagination[ $arg ] )
		) {
			return $pagination[ $arg ];
		}

		return $pagination;
	}

	public function get_results( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->results ) ) {

			// Get results
		}

		return $this->results;
	}

	protected function add_meta_query( $meta_query, $meta_key = null, $overwrite = false ) {

		$default_meta_query = array(
			'relation'	=> 'AND',
		);

		if( $overwrite ) {
			$this->query['meta_query'] = $default_meta_query;
		}

		$this->query['meta_query'][ $meta_key ] = $meta_query;

		$this->query['meta_query'] = array_merge( $default_meta_query, $this->query['meta_query'] );
	}

	protected function add_tax_query( $tax_query, $tax_key = null, $overwrite = false ) {

		$default_tax_query = array(
			'relation'	=> 'AND',
		);

		if( $overwrite ) {
			$this->query['tax_query'] = $default_tax_query;
		}

		$this->query['tax_query'][ $tax_key ] = $tax_query;

		$this->query['tax_query'] = array_merge( $default_tax_query, $this->query['tax_query'] );
	}

	/**
	 * classes that extend from OMH_Table and have a need for scopes will require
	 * to prefix those methods with `scope_` this function will call on those scopes when triggered
	 */
	protected function do_query_scopes() {

		if( !empty( $this->scopes ) ) {
			foreach( (array) $this->scopes as $scope ) {

				if( method_exists( $this, "scope_{$scope}" ) ) {

					$this->{"scope_{$scope}"}();
				}
			}
		}

		if( $this->chapter_scope ) {

			$this->scope_session_chapter();
		}
	}

	protected function scope_session_chapter() {

		$chapter = OMH()->session->get_chapter();

		if( $chapter ) {
			$this->query['tax_query']['chapters'] = array(
				'taxonomy'	=> 'chapters',
				'field'		=> 'slug',
				'terms'		=> $chapter->get_term()->slug,
			);
		}
	}

	public function force_scope( $scopes ) {

		$this->scopes = (array) $scopes;
	}

	public function get_table_type() {

		return $this->table_type ?? str_replace( 'OMH_Table_', '', get_called_class() );
	}

	/**
	 * Allow for dynamic tabs
	 * 
	 * @return 	array
	 */
	public function get_tabs() {

		return $this->tabs ?? array();
	}

	public function get_headers() {

		return $this->headers ?? array();
	}

	public function get_ui_table( $ui_table_args = array(), $force_refresh = false ) {

		if( $force_refresh || !isset( $this->ui_table ) ) {

			$ui_table_args['table']	 = $this;
			$ui_table_args['table_type'] = $this->get_table_type();

			$this->ui_table = OMH_HTML_UI_Table::factory( $ui_table_args );
		}

		return $this->ui_table;
	}

	public function get_html() {

		$this->get_results();

		$sections = array();

		foreach( $this->sections as $section => $include_section ) {

			if( $include_section && method_exists( $this, "get_html_{$section}" ) ) {

				$sections[ $section ] = call_user_func( array( $this, "get_html_{$section}" ) );
			}
		} 

		return $sections;
	}

	protected function get_row_html( $row_data ) {

		ob_start();

		if( $this->row_template ) {
			include OMH_TEMPLATE_PATH . $this->row_template;
		}

		return ob_get_clean();
	}

	protected function get_html_before() {

		if( $this->title ) {
			return '<h4>' . implode( ' ', $this->title ) . '</h4>';
		}
	}

	protected function get_html_search() {

		return OMH_HTML_UI_Input::factory(
			array(
				'input_type'	=> 'text',
				'class'			=> array(
					'omh-field-wrap--table-search',
				),
				'input_class'	=> array(
					'omh-table-search-input'
				),
				'value'			=> isset( $this->query['search'] ) ? $this->query['search'] : '',
				'placeholder'	=> 'Filter results',
				'append'		=> array(
					OMH_HTML_UI_Button::factory(
						array(
							'color'		=> 'secondary',
							'label'		=> 'Search',
							'class'		=> 'omh-table-search-btn'
						)
					)
				)
			)
		)->render();
	}

	protected function get_html_tabs() {

		if( $tabs = $this->get_tabs() ) {

			$nav_tabs = OMH_HTML_UI_Nav_Tabs::factory();

			foreach( $tabs as $tab_name => $tab_args ) {

				if( isset( $tab_args['scope'] ) ) {

					$tab_scope = $tab_args['scope'];

					unset( $tab_args['scope'] );

					$tab_args['attrs'] = $tab_args['attrs'] ?? array();

					$tab_args['attrs']['data-set-scope'] = $tab_scope;

					if( in_array( $tab_scope, (array)$this->scopes ) ) {

						$tab_args['active'] = true;
					}
				}

				if( !empty( $tab_args['clear_scopes'] ) ) {

					unset( $tab_args['clear_scopes'] );

					$tab_args['attrs'] = $tab_args['attrs'] ?? array();

					$tab_args['attrs']['data-clear-scopes'] = true;
				}

				$tab_args['class'] = 'nav-item nav-item-table-tab';

				$nav_tabs->add_tab( $tab_args );
			}

			return $nav_tabs->render();
		}

		return '';
	}

	/**
	 * dev:todo clean this up, logic can be condensed - in a rush to complete
	 * @return type
	 */
	protected function get_html_pagination() {

		$current_page = $this->get_pagination( 'paged' );

		$posts_per_page = $this->get_pagination( 'posts_per_page' );

		$first_num =  min( ( ( $current_page - 1 ) * $posts_per_page ) + 1, $this->total_results );  //( $current_page == 1 ) ? '1' : ( $current_page * $posts_per_page ) + 1;
		$last_num = min( $current_page * $posts_per_page, $this->total_results );

		$total_html_results = OMH_HTML_Tag::factory(
			array(
				'type'		=> 'span',
				'class'		=> 'center',
				'contents'	=> array(
					$first_num . ' - ' . $last_num . ' of ' . $this->total_results . ' ' . _n( 'Result', 'Results', $this->total_results )
				)
			)
		);

		// $total_html_results->render();

		$pagination = OMH_HTML_UI_Pagination::factory();

		if( 1 < $current_page ) {

			$pagination->add_page(
				array(
					'label'		=> '«',
					'page_num'	=> '1'
				), 'first'
			);

			$pagination->add_page(
				array(
					'label'		=> '‹',
					'page_num'	=> $current_page - 1
				), 'prev'
			);

			if( 3 <= $current_page ) {

				$pagination->add_page(
					array(
						'label'		=> $current_page - 2,
						'page_num'	=> $current_page - 2
					)
				);
			}

			if( 2 <= $current_page ) {

				$pagination->add_page(
					array(
						'label'		=> $current_page - 1,
						'page_num'	=> $current_page - 1
					)
				);
			}
		}

		$pagination->add_page(
			array(
				'label'		=> $current_page,
				'page_num'	=> $current_page,
				'active'	=> true
			), $current_page
		);

		if( $current_page < $this->get_total_pages() ) {

			if( $current_page + 1 <= $this->get_total_pages() ) {

				$pagination->add_page(
					array(
						'label'		=> $current_page + 1,
						'page_num'	=> $current_page + 1
					)
				);

				if( $current_page + 2 <= $this->get_total_pages() ) {

					$pagination->add_page(
						array(
							'label'		=> $current_page + 2,
							'page_num'	=> $current_page + 2
						)
					);
				}

				$pagination->add_page(
					array(
						'label'		=> '›',
						'page_num'	=> $current_page + 1
					), 'next'
				);
			}

			$pagination->add_page(
				array(
					'label'		=> '»',
					'page_num'	=> $this->get_total_pages()
				), 'last'
			);
		}

		return $total_html_results->render() . $pagination->render();
	}

	protected function get_html_table() {

		return $this->get_html_headers() . $this->get_html_results();
	}

	public function get_html_headers() {

		if( empty( $this->get_headers() ) ) {

			return '';
		}

		$headers_tr = OMH_HTML_Tag::factory(
			array(
				'type'		=> 'tr'
			)
		);

		foreach( $this->get_headers() as $field => $args ) {

			$args['label'] = $args['label'] ?? ucwords( $field );

			$query_col = $args['query_col'] ?? $field;

			// Determine sorting
			if( ( isset( $this->query['orderby'] ) && $this->query['orderby'] == $query_col && $this->query['order'] ) ) {

				$args['sorted'] = $this->query['order'];
			} elseif( isset( $this->query['meta_key'] )
				&& ( str_replace( 'metaval:', '', $query_col ) === $this->query['meta_key'] )
			) {

				$args['sorted'] = $this->query['order'];
			}

			$args['attrs'] = array(
				'data-col'	=> $field,
				'data-query-col' => $query_col
			);

			$header_tag = OMH_HTML_UI_Table_Header::factory( $args );

			$headers_tr->add_content( $header_tag );
		}

		return $headers_tr->render();
	}

	protected function get_html_results() {

		$this->get_results();

		$results_html = '';

		foreach( $this->results as $row_data ) {

			$results_html .= $this->get_row_html( $row_data );
		}

		return $results_html;
	}

	protected function get_html_no_results() {

		return '<p class="text-muted">' . $this->no_results_msg .'</p>';
	}

	protected function get_html_after() {

	}

	protected function get_fields() {

	}

	protected function get_table_html() {

	}

	protected function get_table_headers_html( ) {

	}

	protected function get_table_row_html( $row_data ) {

	}
}
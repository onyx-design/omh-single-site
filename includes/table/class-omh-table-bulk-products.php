<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class OMH_Table_Bulk_Products extends OMH_Table_Products
{

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> true,
			'search'		=> false,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'		=> array(
			'post_type'		=> 'product',
			'post_status'	=> array(
				'publish'
			),
			'orderby'		=> 'ID',
			'order'			=> 'DESC',
			'fields'		=> 'ids',
			'posts_per_page' => -1,
			'search'		=> null,
			'tax_query'		=> array(
				array(
					'taxonomy'	=> 'product_type',
					'field'		=> 'name',
					'terms'		=> 'variable',
				),
				array(
					'taxonomy'	=> 'sales_type',
					'field'		=> 'slug',
					'terms'		=> 'bulk'
				),
				'relation'	=> 'AND'
			)
		),
		'pagination'	=> array(
			'posts_per_page' => 20,
			'paged'			=> 1,
		),
		'scopes' 	=> array(),
		'headers' 	=> array(
			'image'				=> array(
				'label'		=> 'Preview',
			),
			'name'				=> array(
				'label'		=> 'Name',
				'sortable'	=> true,
				'query_col'	=> 'title'
			),
			/**
			 * dev:todo This needs to be reworked
			 * 
			 * @tag Visibility
			 */
			'visibility'		=> array(
				'label'		=> 'Status',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_visibility_mh'
			),
			/**
			 * dev:todo This needs to be reworked
			 * 
			 * @tag Visibility
			 */
			'sku'		=> array(
				'label'		=> 'Status',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_visibility_mh'
			),
			'date_created'	=> array(
				'label'		=> 'Date Created',
			)
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $sections = array(
		'before'		=> false,
		'search'		=> false,
		'tabs'			=> true,
		'pagination'	=> true,
		'headers'		=> true,
		'results'		=> true
	);

	protected $query = array(
		'post_type'		=> 'product',
		'post_status'	=> array(
			'publish'
		),
		'posts_per_page' => -1,
		'search'		=> null,
		'orderby'		=> 'ID',
		'order'			=> 'DESC',
		'fields'		=> 'ids',
		'tax_query'		=> array(
			'relation'	=> 'AND',
			array(
				'taxonomy'	=> 'product_type',
				'field'		=> 'name',
				'terms'		=> 'variable',
			),
			array(
				'taxonomy'	=> 'sales_type',
				'field'		=> 'slug',
				'terms'		=> 'bulk'
			),
		)
	);

	protected $pagination = array(
		'posts_per_page' => 20,
		'paged'			=> 1,
	);

	protected $headers = array(
		'image'				=> array(
			'label'		=> 'Preview',
		),
		'name'				=> array(
			'label'		=> 'Name',
			'sortable'	=> true,
			'query_col'	=> 'title'
		),
		'visibility'		=> array(
			'label'		=> 'Status',
			'sortable'	=> true,
			'query_col'	=> 'metaval:_visibility_mh'
		),
		/**
		 * dev:todo This needs to be reworked
		 * 
		 * @tag Visibility
		 */
		'sku'		=> array(
			'label'		=> 'SKU',
			'sortable'	=> true,
			'query_col'	=> 'metaval:_visibility_mh'
		),
		'date_created'	=> array(
			'label'		=> 'Date Created',
		)
	);

	protected $row_template = 'table/mh-bulk-product-list-row.php';

	protected $scopes = array(
		'pending',
	);

	protected $tabs = array(
		'pending'		=> array(
			'name'			=> 'Pending',
			'scope'			=> 'pending',
			'clear_scopes'	=> true,
		),
		'ordered'		=> array(
			'name'			=> 'Ordered',
			'scope'			=> 'ordered',
			'clear_scopes'	=> true,
		)
	);

	protected $chapter_scope = true;

	protected function scope_pending()
	{

		$this->headers = array(
			'image'				=> array(
				'label'		=> 'Preview',
			),
			'name'				=> array(
				'label'		=> 'Name',
				'sortable'	=> true,
				'query_col'	=> 'title'
			),
			'visibility'		=> array(
				'label'		=> 'Status',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_visibility_mh'
			),
			/**
			 * dev:todo This needs to be reworked
			 * 
			 * @tag Visibility
			 */
			'sku'		=> array(
				'label'		=> 'SKU',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_visibility_mh'
			),
			'date_created'	=> array(
				'label'		=> 'Date Created',
			)
		);

		$this->query['tax_query']['status'] = array(
			array(
				'taxonomy'	=> 'product_status',
				'field'		=> 'slug',
				'terms'		=> array(
					'design_in_review',
					'ready_to_order',
					'pending',
				),
				'operator'	=> 'IN'
			)
		);
	}
	protected function scope_ready_to_order()
	{

		$this->headers = array(
			'image'				=> array(
				'label'		=> 'Preview',
			),
			'name'				=> array(
				'label'		=> 'Name',
				'sortable'	=> true,
				'query_col'	=> 'title'
			),
			'visibility'		=> array(
				'label'		=> 'Status',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_visibility_mh'
			),
			/**
			 * dev:todo This needs to be reworked
			 * 
			 * @tag Visibility
			 */
			'sku'		=> array(
				'label'		=> 'SKU',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_visibility_mh'
			),
			'date_created'	=> array(
				'label'		=> 'Date Created',
			)
		);

		$this->query['tax_query']['status'] = array(
			array(
				'taxonomy'	=> 'product_status',
				'field'		=> 'slug',
				'terms'		=> array(
					'ready_to_order',
					'pending',
				),
				'operator'	=> 'IN'
			)
		);
	}

	protected function scope_ordered()
	{

		$this->headers = array(
			'image'				=> array(
				'label'		=> 'Preview',
			),
			'name'				=> array(
				'label'		=> 'Name',
				'sortable'	=> true,
				'query_col'	=> 'title'
			),
			'visibility'		=> array(
				'label'		=> 'Status',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_visibility_mh'
			),
			/**
			 * dev:todo This needs to be reworked
			 * 
			 * @tag Visibility
			 */
			'sku'		=> array(
				'label'		=> 'SKU',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_visibility_mh'
			),
			'date_paid'	=> array(
				'label'		=> 'Date Paid',
			)
		);

		$this->query['tax_query']['status'] = array(
			array(
				'taxonomy'	=> 'product_status',
				'field'		=> 'slug',
				'terms'		=> array(
					'processing',
					'completed',
				),
				'operator'	=> 'IN'
			)
		);
	}

	protected function scope_completed()
	{

		$this->headers = array(
			'image'				=> array(
				'label'		=> 'Preview',
			),
			'name'				=> array(
				'label'		=> 'Name',
				'sortable'	=> true,
				'query_col'	=> 'title'
			),
			'visibility'		=> array(
				'label'		=> 'Status',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_visibility_mh'
			),
			/**
			 * dev:todo This needs to be reworked
			 * 
			 * @tag Visibility
			 */
			'sku'		=> array(
				'label'		=> 'SKU',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_visibility_mh'
			),
			'date_paid'	=> array(
				'label'		=> 'Date Paid',
			),
			'tracking'	=> array(
				'label'		=> 'Tracking',
			)
		);

		$this->query['tax_query']['status'] = array(
			array(
				'taxonomy'	=> 'product_status',
				'field'		=> 'slug',
				'terms'		=> array(
					'completed',
				),
				'operator'	=> 'IN'
			)
		);
	}
}

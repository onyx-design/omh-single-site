<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OMH_Table_Credits extends OMH_Table {

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> true,
			'search'		=> false,
			'tabs'			=> false,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'		=> array(
			'posts_per_page'=> 20,
			'orderby'		=> 'ledger_date',
			'order'			=> 'DESC'
		),
		'scopes' 	=> array(),
		'pagination'	=> array(
			'posts_per_page'=> 20,
			'paged'			=> 1,
		),
		'headers' 	=> array(
			'ledger_date'	=> array(
				'label'		=> 'Date',
				'sortable'	=> true
			),
			'order_number'	=> array(
				'label'		=> 'Order Number',
				'sortable'	=> true
			),
			'description'	=> array(
			),
			'type'			=> array(
			),
			'amount'		=> array(
				'sortable'	=> true
			),
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $row_template = 'table/mh-credit-list-row.php';

	protected $sections = array(
		'before'		=> true,
		'search'		=> false,
		'tabs'			=> false,
		'pagination'	=> true,
		'headers'		=> true,
		'results'		=> true
	);

	protected $query = array(
		'posts_per_page'=> 20,
		'orderby'		=> 'ledger_date',
		'order'			=> 'DESC'
	);

	protected $pagination = array(
		'posts_per_page'=> 20,
		'paged'			=> 1,
	);

	protected $headers = array(
		'ledger_date'	=> array(
			'label'		=> 'Date',
			'sortable'	=> true
		),
		'order_number'	=> array(
			'label'		=> 'Order Number',
			'sortable'	=> true
		),
		'description'	=> array(
		),
		'type'			=> array(
		),
		'amount'		=> array(
			'sortable'	=> true
		),
	);

	protected $scopes = array();

	protected $tabs = array();

	/**
	 * CUSTOM ARGS
	 */

	protected $wild_card = '';
	
	protected $show_order_number_col = true;

	protected $show_date_created_col = true;

	protected $show_description_col = true;

	protected $show_type_col = true;

	protected $show_amount_col = true;

	public function get_results( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->results ) ) {

			$this->do_query_scopes(); 
			$parsed_query = $this->parse_query();
			
			$this->results = OMH()->credit_factory->query( $parsed_query, true );

			$this->total_results = OMH()->credit_factory::$total_rows;
			$this->set_total_pages(ceil ($this->total_results / $this->pagination['posts_per_page']));
		}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

		return $this->results;
	}

	protected function scope_session_chapter() {

		if( $chapter = OMH()->session->get_chapter() ) {

			$this->query['where'][] = array(
				'name'	=> 'chapter_id',
				'value'	=> $chapter->get_id()
			);
		}
	}

	// protected function get_row_html( $row_data ) {

	// 	ob_start();

	// 	include OMH_TEMPLATE_PATH . 'table/mh-credit-list-row.php';

	// 	return ob_get_clean();
	// }

	protected function get_html_before() {

		if( is_super_admin() ) {
			$html_before = '';

			// Show Add Adjustment button for Super Admins
			if( is_super_admin() ) {
				$html_before .= OMH_HTML_UI_Button::factory(
					array(
						'color'		=> 'primary',
						'size'		=> 'small',
						'label'		=> 'Add Adjustment',
						'attrs'		=> array(
							'data-toggle'	=> 'modal',
							'data-target'	=> '#store-credit-modal'
						)
					)
				)->render();
			}

			return $html_before;
		}
	}
}
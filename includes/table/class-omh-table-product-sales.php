<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OMH_Table_Product_Sales extends OMH_Table {

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> false,
			'search'		=> false,
			'tabs'			=> false,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'		=> array(),
		'pagination'	=> array(
			'posts_per_page'=> 20,
			'paged'			=> 1,
		),
		'filters'	=> array(),
		'scopes' 	=> array(),
		'headers' 	=> array(
			'actions'		=> array(
				'label'			=> ''
			),
			'order'			=> array(
				'label'			=> 'Order',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_order_number'
			),
			'date'			=> array(
				'label'			=> 'Date',
				'sortable'		=> true,
				'query_col'		=> 'post_date'
			),
			'customer' 		=> array(
				'label'			=> 'Customer',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_billing_first_name'
			),
			'status'		=> array(
				'label'			=> 'Status',
				'sortable'	=> true
			),
			'earnings'			=> array(
				'label'			=> 'Earnings',
				'sortable'		=> true,
			),
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $sections = array(
		'before'		=> false,
		'search'		=> false,
		'tabs'			=> false,
		'pagination'	=> true,
		'headers'		=> true,
		'results'		=> true
	);

	protected $query = array(
	);

	protected $pagination = array(
		'posts_per_page'=> 20,
		'paged'			=> 1,
	);

	protected $headers = array(
		'actions'		=> array(
			'label'			=> ''
		),
		'order'			=> array(
			'label'			=> 'Order',
			'sortable'		=> true,
			'query_col'		=> 'metaval:_order_number'
		),
		'date'			=> array(
			'label'			=> 'Date',
			'sortable'		=> true,
			'query_col'		=> 'post_date'
		),
		'customer' 		=> array(
			'label'			=> 'Customer',
			'sortable'		=> true,
			'query_col'		=> 'metaval:_billing_first_name'
		),
		'status'		=> array(
			'label'			=> 'Status',
			'sortable'	=> true
		),
		'earnings'			=> array(
			'label'			=> 'Earnings',
			'sortable'		=> true,
		),
	);

	protected $row_template = 'table/mh-house-order-list-row.php';

	protected $scopes = array();

	protected $tabs = array();

	protected $filters = array();

	protected $show_user_role_col = false;

	/**
	 * CUSTOM ARGS
	 */

	public function get_results( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->results ) ) {

			$this->product = wc_get_product( $this->filters['product_id'] );

			$this->do_query_scopes();

			$this->total_results = omh_get_order_ids_by_product_id( 
				$this->product->get_id(), 
				'count', 
				-1,
				( $this->pagination['posts_per_page'] * ( $this->pagination['paged'] - 1 ) ) 
			);
			
			$this->set_total_pages( ceil( $this->total_results / $this->pagination['posts_per_page'] ) );

			$order_ids = omh_get_order_ids_by_product_id( $this->product->get_id(), 'ids', $this->pagination['posts_per_page'], ( $this->pagination['posts_per_page'] * ( $this->pagination['paged'] - 1 ) ) );
			$results = array();

			foreach( $order_ids as $order_id ) {

				$order = wc_get_order( $order_id );

				$results[] = $order;
			}

			$this->results = $results;
		}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

		return $this->results;
	}
}
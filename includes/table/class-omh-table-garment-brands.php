<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OMH_Table_Garment_Brands extends OMH_Table {

	protected $defaults = array(
		'sections'		=> array(
			'before'		=> true,
			'tabs'			=> false,
			'search'		=> false,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'			=> array(
			'orderby'		=> 'ID',
			'order'			=> 'DESC'
		),
		'pagination'	=> array(
			'posts_per_page'	=> 20,
			'paged'				=> 1
		),
		'scopes'		=> array(),
		'headers'		=> array(
	    	'id'				=> array(
	    		'label'			=> 'ID',
				'sortable'		=> true
	    	),
			'brand_slug'		=> array(
	    		'label'			=> 'Brand Code',
			),
			'brand_code'		=> array(
	    		'label'			=> 'InkSoft Prefix',
			),
			'inksoft_brand_id'	=> array(
				'label'			=> 'InkSoft Brand ID',
				'sortable'		=> true
			),
			'brand_name'		=> array(
	    		'label'			=> 'Brand Name',
				'sortable'		=> true
			),
			'brand_image_url'	=> array(
				'label'			=> 'Brand Image URL'
			),
			'size_chart_url'	=> array(
				'label'			=> 'Size Chart URL'
			),
			'meta_created'		=> array(
				'label'			=> 'Meta Created',
				'sortable'		=> true
			),
			'meta_updated'		=> array(
				'label'			=> 'Meta Updated',
				'sortable'		=> true
			)
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

    protected $sections = array(
		'before'		=> true,
        'tabs'          => false,
        'search'		=> false,
        'pagination'	=> true,
        'headers'		=> true,
        'results'		=> true
    );
    
    protected $headers = array(
    	'id'				=> array(
    		'label'			=> 'ID',
			'sortable'		=> true
    	),
		'brand_slug'		=> array(
    		'label'			=> 'Brand Code',
		),
		'brand_code'		=> array(
    		'label'			=> 'InkSoft Prefix',
		),
		'inksoft_brand_id'	=> array(
			'label'			=> 'InkSoft Brand ID',
			'sortable'		=> true
		),
		'brand_name'		=> array(
    		'label'			=> 'Brand Name',
			'sortable'		=> true
		),
		'brand_image_url'	=> array(
			'label'			=> 'Brand Image URL'
		),
		'size_chart_url'	=> array(
			'label'			=> 'Size Chart URL'
		),
		'meta_created'		=> array(
			'label'			=> 'Meta Created',
			'sortable'		=> true
		),
		'meta_updated'		=> array(
			'label'			=> 'Meta Updated',
			'sortable'		=> true
		)
    );

	protected $pagination = array(
		'posts_per_page'=> 20,
		'paged'			=> 1,
	);
    
    protected $scopes = array(
    	'original'
    );

    protected $query = array(
		'orderby'		=> 'ID',
		'order'			=> 'DESC'
    );

    protected $tabs = array();

    protected $row_template = 'table/mh-garment-brand-list-row.php';

	protected $chapter_scope = false;

	protected $wild_card = '';

    public function get_results( $force_refresh = false ) {

    	if( $force_refresh || !isset( $this->results ) ) {

    		$parsed_query = $this->parse_query();

    		if( $this->sections['pagination'] ) {

    			foreach( $this->get_pagination() as $pagination_arg => $pagination_value ) {

    				$parsed_query[ $pagination_arg ] = $pagination_value;
    			}
    		}

    		$this->results = OMH()->garment_brand_factory->query( $parsed_query );

    		$this->total_results = OMH()->garment_brand_factory->total_rows();

    		$this->set_total_pages( ceil( $this->total_results / $this->pagination['posts_per_page'] ) );
    	}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

    	return $this->results;
    }
}
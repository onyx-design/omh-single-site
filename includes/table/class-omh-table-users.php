<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class OMH_Table_Users extends OMH_Table
{

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> true,
			'search'		=> true,
			'headers'		=> true,
			'results'		=> true,
			'pagination'	=> true,
		),
		'query'		=> array(
			'role'			=> '',
			'role__in' 		=> array(),
			// 'search'		=> null,
			'orderby'		=> 'ID',
			'order'			=> 'DESC',
			'count_total'	=> true,
			'posts_per_page' => -1
		),
		'pagination'	=> array(
			'posts_per_page' => 20,
			'paged'			=> 1,
		),
		'scopes' 	=> array(
			'all',
		),
		'headers' 	=> array(
			'actions'		=> array(
				'label'		=> '',
			),
			'user' 				=> array(
				'sortable'			=> true,
				'query_col'			=> 'metaval:first_name',
			),
			'role'				=> array(),
			'chapter' 			=> array(),
			't_shirt_size'		=> array(
				'label'				=> 'T-Shirt Size',
				'sortable'			=> true
			),
			'user_registered'		=> array(
				'sortable'			=> true
			),
			'activation_link'	=> array(
				'label'				=> 'Activation Link'
			),
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $row_template = 'table/mh-user-list-row.php';

	protected $sections = array(
		'before'		=> true,
		'search'		=> true,
		'tabs'			=> true,
		'headers'		=> true,
		'results'		=> true,
		'pagination'	=> true,
	);

	protected $query = array(
		'role'			=> '',
		'role__in' 		=> array(),
		// 'search'		=> null,
		'search_columns' => array(
			'user_login',
			'user_nicename',
			'user_email'
		),
		'orderby'		=> 'user_registered',
		'order'			=> 'DESC',
		'count_total'	=> true,
		'posts_per_page' => -1
	);

	protected $pagination = array(
		'posts_per_page' => 20,
		'paged'			=> 1,
	);

	protected $headers = array(
		'actions'		=> array(
			'label'		=> '',
		),
		'user' 				=> array(
			'sortable'			=> true,
			'query_col'			=> 'metaval:first_name'
		),
		'role'				=> array(),
		'chapter' 			=> array(),
		't_shirt_size'		=> array(
			'label'				=> 'T-Shirt Size',
			'sortable'			=> true,
			'query_col'			=> 'metaval:t_shirt_size'
		),
		'user_registered'		=> array(
			'sortable'			=> true
		),
		'activation_link'	=> array(
			'label'				=> 'Activation Link'
		),
	);

	protected $scopes = array(
		'all',
	);

	protected $tabs = array(
		'all'			=> array(
			'name'			=> 'All',
			'scope'			=> 'all',
			'clear_scopes'	=> true
		),
	);

	protected $current_user = null;

	protected $chapter_scope = false;

	protected $show_user_info = true;

	protected $show_user_role = true;

	protected $show_remove_user = false;

	protected $enable_row_actions = true;

	protected $show_user_chapter = true;

	protected $show_t_shirt_size = true;

	protected $show_user_registration = true;

	protected $show_activation_url = true;

	protected $show_upgrade_user = false;

	protected $active_tab = 'all';

	public function get_tabs()
	{

		$tabs = $this->tabs;

		$tabs['chapter'] = array(
			'name'			=> OMH()->session->get_chapter()->get_chapter_name(),
			'scope'			=> 'members',
			'clear_scopes'	=> true
		);

		return $tabs;
	}

	public function get_results($force_refresh = false)
	{

		if ($force_refresh || !isset($this->results)) {

			$this->do_query_scopes();

			$parsed_query = $this->parse_query();

			if ($this->sections['pagination']) {

				foreach ($this->get_pagination() as $pagination_arg => $pagination_value) {

					$parsed_query[$pagination_arg] = $pagination_value;
				}
			}

			$user_query = new WP_User_Query($parsed_query);

			$this->total_results = $user_query->get_total();

			$this->set_total_pages(ceil($this->total_results / $this->pagination['posts_per_page']));

			$wp_users = $user_query->get_results();
			$results = array();

			foreach ($wp_users as $wp_user) {
				$results[] = new OMH_User($wp_user);
			}

			$this->results = $results;
		}

		if (count($this->results) < 1) {
			$this->set_no_results();
		}

		return $this->results;
	}

	protected function parse_search()
	{

		$search_terms = $this->parsed_query['search'];

		$search_query = array(
			'relation'	=> 'OR'
		);

		foreach (explode(' ', $search_terms) as $search_term) {

			$search_query[] = array(
				'key'		=> 'first_name',
				'value'		=> $search_term,
				'compare'	=> 'LIKE'
			);
			$search_query[] = array(
				'key'		=> 'last_name',
				'value'		=> $search_term,
				'compare'	=> 'LIKE'
			);
			$search_query[] = array(
				'key'		=> 'nickname',
				'value'		=> $search_term,
				'compare'	=> 'LIKE'
			);
		}

		$this->add_meta_query($search_query);
	}

	protected function get_html_before()
	{

		$html_before = '';

		$this->current_user = $omh_user = wp_get_current_user();

		if ($this->title) {
			$html_before .= '<h4>' . implode(' ', $this->title) . '</h4>';
		}

		if (in_array('house_admin_list', $this->scopes)) {
			$html_before .= '<div class="table-description">To add a new House Admin, <a href=' . OMH()->dashboard_url("members") . '>Click here.</a></div>';
		}

		return $html_before;
	}

	/**
	 * Change the pagination keys to reflect the difference between WP_Query and WP_User_Query
	 * 
	 * @param 	string|bool 	$arg 
	 * @return 	string|array
	 */
	protected function get_pagination($arg = false)
	{

		/*
		 * Map WP_Query keys to WP_User_Query keys
		 */
		$pagination_map = array(
			'posts_per_page'	=> 'number',
			'paged'				=> 'paged'
		);

		if (
			$arg
			&& isset($this->pagination[$arg])
		) {
			return $this->pagination[$arg];
		}

		$pagination = array_combine(array_merge($this->pagination, $pagination_map), $this->pagination);

		if (
			$arg
			&& isset($pagination[$arg])
		) {
			return $pagination[$arg];
		}

		return $pagination;
	}

	protected function scope_session_chapter()
	{

		$chapter_query = array(
			'relation'		=> 'AND',
			array(
				'key'		=> 'primary_chapter_id',
				'value'		=> OMH()->session->get_chapter()->get_id(),
			)
		);

		$this->query['meta_query']['chapter'] = $chapter_query;

		return $this;
	}

	protected function scope_all()
	{

		unset($this->query['role']);
		unset($this->query['meta_query']);

		$this->query['role__in'] = OMH_User::get_allowed_roles(true);

		$this->active_tab = 'all';

		return $this;
	}

	protected function scope_members()
	{

		unset($this->query['role']);

		$this->sections['tabs'] = false;

		$this->show_user_chapter = false;
		$this->scope_session_chapter();


		$this->headers = array(
			'email' 				=> array(),
			'user' 					=> array(
				'sortable'			=> true,
				'query_col'			=> 'metaval:first_name',
				'label'				=> "Name"
			),
			'role'					=> array(),
			'user_registered'			=> array(
				'sortable'			=> true,
				'label' 			=> "Date Added"
			),
		);
		$this->enable_row_actions = true;
		$this->show_user_role = false;
		$this->show_remove_user = true;
		$this->show_upgrade_user = true;
		$this->show_t_shirt_size = false;
		$this->show_activation_url = false;

		$this->query['role__in'] = array(
			'house_admin',
			'house_member'
		);

		$chapter_query = array(
			'relation'		=> 'AND',
			array(
				'key'		=> 'primary_chapter_id',
				'value'		=> OMH()->session->get_chapter()->get_id(),
			)
		);
		$this->query['meta_query']['chapter'] = $chapter_query;
		return $this;
	}

	protected function scope_house_admin()
	{

		unset($this->query['role__in']);

		$this->query['role'] = 'house_admin';

		$this->active_tab = 'house_admin';

		return $this;
	}

	// Used on the main dashboard page to show all the house admins for a store.
	protected function scope_house_admin_list()
	{

		// Only show house admins
		unset($this->query['role']);
		$this->query['role__in'] = array('house_admin');

		// Show before content instead of tabs & headers
		$this->sections['before'] 		= true;
		$this->sections['tabs'] 		= false;
		$this->sections['header'] 		= false;
		$this->sections['pagination']	= false;
		$this->headers 					= false;

		// Only show name & email
		$this->enable_row_actions 		= false;
		$this->show_user_role 			= false;
		$this->show_user_status 		= false;
		$this->show_user_registration 	= false;
		$this->show_user_chapter 		= false;
		$this->show_t_shirt_size 		= false;
		$this->show_activation_url 		= false;

		$this->chapter_scope = true;

		return $this;
	}

	protected function scope_customer()
	{

		unset($this->query['role__in']);

		$this->query['role'] = 'customer';

		$this->active_tab = 'customer';

		return $this;
	}

	protected function scope_chapter()
	{

		unset($this->query['role']);

		$this->query['role__in'] = array(
			'house_admin',
			'house_member'
		);

		$this->active_tab = 'chapter';

		$chapter_query = array(
			'relation'		=> 'AND',
			array(
				'key'		=> 'primary_chapter_id',
				'value'		=> OMH()->session->get_chapter()->get_id(),
			)
		);

		$this->query['meta_query']['chapter'] = $chapter_query;

		return $this;
	}
}

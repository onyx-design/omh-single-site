<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OMH_Table_Retail_Products extends OMH_Table_Products {

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> true,
			'search'		=> false,
			'tabs'			=> true,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'		=> array(
			'post_type'		=> 'product',
			'post_status'	=> array(
				'publish'
			),
			'orderby'		=> 'ID',
			'order'			=> 'DESC',
			'fields'		=> 'ids',
			'posts_per_page'=> -1,
			'search'		=> null,
			'tax_query'		=> array(
				array(
					'taxonomy'	=> 'product_type',
					'field'		=> 'name',
					'terms'		=> 'variable',
				),
				array(
					'taxonomy'	=> 'sales_type',
					'field'		=> 'slug',
					'terms'		=> 'retail'
				),
				'relation'	=> 'AND'
			)
		),
		'pagination'	=> array(
			'posts_per_page'=> 20,
			'paged'			=> 1,
		),
		'scopes' 	=> array(
			'products',
		),
		'headers' 	=> array(
			'image'				=> array(
				'label'		=> 'Preview',
			),
			'name'				=> array(
				'label'		=> 'Product Name',
				'sortable'	=> true,
				'query_col'	=> 'title'
			),
			'status'			=> array(
				'label'		=> 'Status',
			),
			'price'				=> array(
				'label'		=> 'Price',
			),
			'total_sold'		=> array(
				'label'		=> '# Sold'
			)
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $sections = array(
		'before'		=> false,
		'search'		=> false,
		'tabs'			=> true,
		'pagination'	=> true,
		'headers'		=> true,
		'results'		=> true
	);

	protected $query = array(
		'post_type'		=> 'product',
		'post_status'	=> array(
			'publish'
		),
		'posts_per_page'=> -1,
		'search'		=> null,
		'orderby'		=> 'ID',
		'order'			=> 'DESC',
		'fields'		=> 'ids',
		'tax_query'		=> array(
			array(
				'taxonomy'	=> 'product_type',
				'field'		=> 'name',
				'terms'		=> 'variable',
			),
			array(
				'taxonomy'	=> 'sales_type',
				'field'		=> 'slug',
				'terms'		=> 'retail'
			),
			'relation'	=> 'AND'
		)
	);

	protected $pagination = array(
		'posts_per_page'=> 20,
		'paged'			=> 1,
	);

	protected $headers = array(
		'image'				=> array(
			'label'		=> 'Preview',
		),
		'name'				=> array(
			'label'		=> 'Name',
			'sortable'	=> true,
			'query_col'	=> 'title'
		),
		'status'			=> array(
			'label'		=> 'Status',
		),
		'sku'			=> array(
			'label'		=> 'SKU',
			'sortable'	=> true,
			'query_col'		=> 'metaval:_sku'
		),
		'price'				=> array(
			'label'		=> 'Price',
		),
		'total_sold'		=> array(
			'label'		=> '# Sold'
		)
	);

	protected $row_template = 'table/mh-retail-product-list-row.php';

	protected $scopes = array(
		'products'
	);

	protected $tabs = array(
		'products'		=> array(
			'name'			=> 'Active',
			'scope'			=> 'products',
			'clear_scopes'	=> true
		),
		'archived'		=> array(
			'name'			=> 'Archived',
			'scope'			=> 'archived',
			'clear_scopes'	=> true
		),
	);
	
	protected $active_tab = 'products';
	
	protected $chapter_scope = true;

	protected function scope_products() {

		$this->query['tax_query']['status'] = array(
			array(
				'taxonomy'	=> 'product_status',
				'field'		=> 'slug',
				'terms'		=> array(
					'design_in_review',
					'ready_to_list',
					'published',
					'private',
					'unpublished',
				),
				'operator'	=> 'IN'
			)
		);

		$this->active_tab = 'products';
	}

	protected function scope_ready_to_list() {

		$this->query['tax_query']['status'] = array(
			array(
				'taxonomy'	=> 'product_status',
				'field'		=> 'slug',
				'terms'		=> array(
					'ready_to_list',
				),
				'operator'	=> 'IN'
			)
		);
	}

	protected function scope_archived() {

		$this->query['tax_query']['status'] = array(
			array(
				'taxonomy'	=> 'product_status',
				'field'		=> 'slug',
				'terms'		=> array(
					'archived',
				),
				'operator'	=> 'IN'
			)
		);

		$this->active_tab = 'archived';
	}

	protected function scope_design_in_review() {

		$this->query['tax_query']['status'] = array(
			array(
				'taxonomy'	=> 'product_status',
				'field'		=> 'slug',
				'terms'		=> array(
					'design_in_review',
				),
				'operator'	=> 'IN'
			)
		);
	}
}
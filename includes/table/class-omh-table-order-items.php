<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Table_Order_Items extends OMH_Table {

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> false,
			'search'		=> false,
			'pagination'	=> false,
			'headers'		=> true,
			'results'		=> true
		),
		'query'		=> array(),
		'filters' 	=> array(),
		'scopes' 	=> array(),
		'headers'	=> array(
			'image'		=> array(
				'label'		=> ''
			),
			'name'		=> array(
				'label'		=> 'Product Name'
			),
			'price_and_quantity'	=> array(
				'label'		=> 'Price & Quantity'
			),
			'line_item_total'		=> array(
				'label'		=> 'Line Item Total'
			)
		),
		'title'			=> array(),
		'tabs'		=> array(
		)
	);

	protected $sections = array(
		'before'		=> false,
		'search'		=> false,
		'tabs'			=> false,
		'pagination'	=> false,
		'headers'		=> true,
		'results'		=> true
	);

	protected $query = array(
	);

	protected $scopes = array(
	);

	protected $headers = array(
		'image'		=> array(
			'label'		=> ''
		),
		'name'		=> array(
			'label'		=> 'Product Name'
		),
		'price_and_quantity'	=> array(
			'label'		=> 'Price & Quantity'
		),
		'line_item_total'		=> array(
			'label'		=> 'Line Item Total'
		)
	);

	protected $row_template = 'table/mh-order-item-list-row.php';

	/**
	 * CUSTOM ARGS
	 */
	protected $order_items = array();

	protected $show_earnings_col = false;

	public function get_results( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->results ) ) {

			$this->order = wc_get_order( $this->filters['order_id'] );

			$this->do_query_scopes();

			if( empty( $this->scopes ) && !$this->order_items && $this->order ) {
				$this->order_items = $this->order->get_items( 'line_item' );
			}

			$this->total_results = count( $this->order_items );

			$this->results = $this->order_items;
		}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

		return $this->results;
	}

	/**
	 * Change what displays in the Order Items table
	 * if the user viewing it is an administrative role
	 */
	protected function scope_admin() {

		$this->show_earnings_col = true;

		$this->headers['earnings']	= array(
			'label'		=> 'Earnings'
		);

		foreach( $this->order->get_items( 'line_item' ) as $order_item ) {

			$product = wc_get_product( $order_item->get_product_id() );
			$product_chapter = $product->get_chapter();

			if( $product_chapter 
				&& ( $product_chapter->get_id() === $this->filters['chapter_id'] )
			) {
				$this->order_items[] = $order_item;
			}
		}
	}
}
<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class OMH_Table_Campaign_Products extends OMH_Table_Products
{

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> true,
			'search'		=> false,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'		=> array(
			'post_type'		=> 'product',
			'post_status'	=> array(
				'publish'
			),
			'orderby'		=> 'ID',
			'order'			=> 'DESC',
			'fields'		=> 'ids',
			'posts_per_page' => -1,
			'search'		=> null,
			'tax_query'		=> array(
				array(
					'taxonomy'	=> 'product_type',
					'field'		=> 'name',
					'terms'		=> 'variable',
				),
				array(
					'taxonomy'	=> 'sales_type',
					'field'		=> 'slug',
					'terms'		=> 'campaign'
				),
				'relation'	=> 'AND'
			)
		),
		'pagination'	=> array(
			'posts_per_page' => 20,
			'paged'			=> 1,
		),
		'scopes' 	=> array(),
		'headers' 	=> array(
			'image'				=> array(
				'label'		=> 'Preview',
			),
			'name'				=> array(
				'label'		=> 'Name',
				'sortable'	=> true,
				'query_col'	=> 'title'
			),
			'status'			=> array(
				'label'		=> 'Status',
			),
			'sku'			=> array(
				'label'		=> 'SKU',
			),
			'price'				=> array(
				'label'		=> 'Price',
			),
			'total_sold'		=> array(
				'label'		=> '# Sold'
			),
			'target_quantity'	=> array(
				'label'		=> 'Target Quantity',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_campaign_target_sales'
			),
			'date_ending'		=> array(
				'label'		=> 'Date Ending',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_campaign_end_date'
			),
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $sections = array(
		'before'		=> false,
		'search'		=> false,
		'tabs'			=> true,
		'pagination'	=> true,
		'headers'		=> true,
		'results'		=> true
	);

	protected $query = array(
		'post_type'		=> 'product',
		'post_status'	=> array(
			'publish'
		),
		'posts_per_page' => -1,
		'search'		=> null,
		'orderby'		=> 'ID',
		'order'			=> 'DESC',
		'fields'		=> 'ids',
		'tax_query'		=> array(
			array(
				'taxonomy'	=> 'product_type',
				'field'		=> 'name',
				'terms'		=> 'variable',
			),
			array(
				'taxonomy'	=> 'sales_type',
				'field'		=> 'slug',
				'terms'		=> 'campaign'
			),
			'relation'	=> 'AND'
		)
	);

	protected $pagination = array(
		'posts_per_page' => 20,
		'paged'			=> 1,
	);

	protected $headers = array(
		'image'				=> array(
			'label'		=> 'Preview',
		),
		'name'				=> array(
			'label'		=> 'Name',
			'sortable'	=> true,
			'query_col'	=> 'title'
		),
		'status'			=> array(
			'label'		=> 'Status',
		),
		'sku'			=> array(
			'label'		=> 'SKU',
		),
		'price'				=> array(
			'label'		=> 'Price',
		),
		'total_sold'		=> array(
			'label'		=> '# Sold'
		),
		'target_quantity'	=> array(
			'label'		=> 'Target Quantity',
			'sortable'	=> true,
			'query_col'	=> 'metaval:_campaign_target_sales'
		),
		'total_earnings'	=> array(
			'label'		=> 'Total Earnings',
		),
		'date_ending'		=> array(
			'label'		=> 'Date Ending',
			'sortable'	=> true,
			'query_col'	=> 'metaval:_campaign_end_date'
		),
	);

	protected $row_template = 'table/mh-campaign-product-list-row.php';

	protected $scopes = array();

	protected $tabs = array();

	protected $chapter_scope = true;

	protected function scope_current()
	{

		$this->query['tax_query']['status'] = array(
			array(
				'taxonomy'	=> 'product_status',
				'field'		=> 'slug',
				'terms'		=> array(
					// 'design_in_review',
					'live',
					'live_private'
				),
				'operator'	=> 'IN'
			)
		);
	}
}

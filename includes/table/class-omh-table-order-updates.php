<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Table_Order_Updates extends OMH_Table {

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> false,
			'search'		=> false,
			'pagination'	=> false,
			'headers'		=> false,
			'results'		=> true
		),
		'query'		=> array(

		),
		'filters' 	=> array(
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $sections = array(
		'before'		=> false,
		'search'		=> false,
		'tabs'			=> false,
		'pagination'	=> false,
		'headers'		=> false,
		'results'		=> true
	);

	protected $query = array(

	);

	protected $headers = array(

	);

	/**
	 * CUSTOM ARGS
	 */
	//None

	public function get_results( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->results ) ) {

			$omh_order = wc_get_order( $this->filters['order_id'] );

			$order_updates = $omh_order ? wc_get_order_notes( array( 'order_id' => $this->filters['order_id'] ) ) : array();

			$this->total_results = count( $order_updates );

			$this->results = $order_updates;
		}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

		return $this->results;
	}
}
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OMH_Table_Chapters extends OMH_Table {

	protected $defaults = array(
		'sections'		=> array(
			'before'		=> true,
			'search'		=> true,
			'tabs'			=> true,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'			=> array(
			'orderby'		=> 'ID',
			'order'			=> 'DESC'
		),
		'pagination'	=> array(
			'posts_per_page'	=> 20,
			'paged'				=> 1
		),
		'scopes' 	=> array(
			'all',
		),
		'headers' 	=> array(
			'id'					=> array(
				'label'			=> 'ID',
				'sortable'		=> true
			),
			'banner_image_id'	=> array(
				'label'			=> 'Banner'
			),
			'org_id'				=> array(
				'label'			=> 'Organization',
				'sortable'		=> true,
			),
			'college_id'			=> array(
				'label'			=> 'College',
				'sortable'		=> true,
			),
			'chapter_name'			=> array(
				'label'			=> 'Chapter Name',
				'sortable'		=> true,
			),
			'url_structure'			=> array(
				'label'			=> 'URL Structure',
				'sortable'		=> true,
			),
			'earnings_percentage'	=> array(
				'label'			=> 'Earnings Percentage',
				'sortable'		=> true,
			),
			'status'				=> array(
				'label'			=> 'Status',
				'sortable'		=> true,
			),
			'term_id'				=> array(
				'label'			=> 'Term ID',
				'sortable'		=> true,
			),
			'meta_created'			=> array(
				'label'			=> 'Meta Created',
				'sortable'		=> true
			),
			'meta_updated'			=> array(
				'label'			=> 'Meta Updated',
				'sortable'		=> true
			)
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $sections = array(
		'before'		=> true,
        'search'		=> true,
        'tabs'          => true,
        'pagination'	=> true,
        'headers'		=> true,
        'results'		=> true
	);

	protected $headers = array(
		'id'					=> array(
			'label'			=> 'ID',
			'sortable'		=> true
		),
		'banner_image_id'	=> array(
			'label'			=> 'Banner'
		),
		'org_id'				=> array(
			'label'			=> 'Organization',
			'sortable'		=> true,
		),
		'college_id'			=> array(
			'label'			=> 'College',
			'sortable'		=> true,
		),
		'chapter_name'			=> array(
			'label'			=> 'Chapter Name',
			'sortable'		=> true,
		),
		'url_structure'			=> array(
			'label'			=> 'URL Structure',
			'sortable'		=> true,
		),
		'earnings_percentage'	=> array(
			'label'			=> 'Earnings Percentage',
			'sortable'		=> true,
		),
		'status'				=> array(
			'label'			=> 'Status',
			'sortable'		=> true,
		),
		'term_id'				=> array(
			'label'			=> 'Term ID',
			'sortable'		=> true,
		),
		'meta_created'			=> array(
			'label'			=> 'Meta Created',
			'sortable'		=> true
		),
		'meta_updated'			=> array(
			'label'			=> 'Meta Updated',
			'sortable'		=> true
		)
	);

	protected $pagination = array(
		'posts_per_page'=> 20,
		'paged'			=> 1,
	);

	protected $scopes = array(
		'all',
	);

	protected $query = array(
		'orderby'		=> 'ID',
		'order'			=> 'DESC'
	);

	protected $tabs = array(
		'all'				=> array(
			'name'			=> 'All',
			'scope'			=> 'all',
			'clear_scopes'	=> true
		),
		'without_org'		=> array(
			'name'			=> 'Without Organization',
			'scope'			=> 'without_org',
			'clear_scopes'	=> true
		),
		'without_college'	=> array(
			'name'			=> 'Without College',
			'scope'			=> 'without_college',
			'clear_scopes'	=> true
		)
	);

	protected $active_tab = 'all';

	protected $row_template = 'table/mh-chapter-list-row.php';

	protected $chapter_scope = false;

	protected $wild_card = '';

	// Cached results for use by rows //dev:improve
	protected $organizations;
	protected $colleges;

	public function get_results( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->results ) ) {

			$this->do_query_scopes();

    		$parsed_query = $this->parse_query();

    		if( $this->sections['pagination'] ) {

    			foreach( $this->get_pagination() as $pagination_arg => $pagination_value ) {

    				$parsed_query[ $pagination_arg ] = $pagination_value;
    			}
    		}

    		$this->results = OMH()->chapter_factory->query( $parsed_query );

    		$this->total_results = OMH()->chapter_factory->total_rows( $parsed_query );

    		$this->set_total_pages( ceil( $this->total_results / $this->pagination['posts_per_page'] ) );
		}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

		return $this->results;
	}

	protected function get_html_results() {

		$this->get_results();

		// Cache Organizations & Colleges data so we can display names rather than IDs //dev:improve
		global $wpdb;
		$this->organizations = $wpdb->get_results( 'SELECT ID, org_letters FROM wp_omh_organizations ORDER BY ID ASC', OBJECT_K );
		$this->colleges = $wpdb->get_results( 'SELECT ID, college_name FROM wp_omh_colleges ORDER BY ID ASC', OBJECT_K );

		$results_html = '';

		foreach( $this->results as $row_data ) {

			$results_html .= $this->get_row_html( $row_data );
		}

		return $results_html;
	}

	protected function get_html_before() {
		
		$html_before = '';

		if( is_super_admin() ) {

			$html_before .= OMH_HTML_UI_Button::factory(
	    		array(
	    			'color'		=> 'primary',
	    			'size'		=> 'small',
	    			'label'		=> 'Add Chapter',
	    			'attrs'		=> array(
	    				'data-toggle'	=> 'modal',
	    				'data-target'	=> '#chapter-modal'
	    			)
	    		)
	    	)->render();
		}

		return $html_before;
	}

	protected function scope_all() {

		$this->query['where'] = array();

		$this->active_tab = 'all';
	}

	protected function scope_without_org() {

		$this->query['where'] = array(
			array(
				'name'		=> 'org_id',
				'value'		=> '0',
			)
		);

		$this->active_tab = 'without_org';
	}

	protected function scope_without_college() {

		$this->query['where'] = array(
			array(
				'name'		=> 'college_id',
				'value'		=> '0',
			)
		);

		$this->active_tab = 'without_college';
	}
}
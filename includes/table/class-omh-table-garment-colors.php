<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OMH_Table_Garment_Colors extends OMH_Table {

	protected $defaults = array(
		'sections'		=> array(
			'before'		=> true,
			'tabs'			=> false,
			'search'		=> false,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'			=> array(
			'orderby'		=> 'ID',
			'order'			=> 'DESC'
		),
		'pagination'	=> array(
			'posts_per_page'	=> 20,
			'paged'				=> 1
		),
		'scopes'		=> array(),
		'headers'		=> array(
	    	'id'				=> array(
	    		'label'			=> 'ID',
				'sortable'		=> true
	    	),
			'color_code'		=> array(
	    		'label'			=> 'Color Code',
			),
			'color_name'		=> array(
	    		'label'			=> 'Color Name',
			),
			'color_hex_1'		=> array(
				'label'			=> 'Color Hex 1',
			),
			'color_hex_2'		=> array(
				'label'			=> 'Color Hex 2',
			),
			'meta_created'		=> array(
				'label'			=> 'Meta Created',
				'sortable'		=> true
			),
			'meta_updated'		=> array(
				'label'			=> 'Meta Updated',
				'sortable'		=> true
			)
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

    protected $sections = array(
		'before'		=> true,
        'tabs'          => false,
        'search'		=> false,
        'pagination'	=> true,
        'headers'		=> true,
        'results'		=> true
    );
    
    protected $headers = array(
    	'id'				=> array(
    		'label'			=> 'ID',
			'sortable'		=> true
    	),
		'color_code'		=> array(
    		'label'			=> 'Color Code',
		),
		'color_name'		=> array(
    		'label'			=> 'Color Name',
		),
		'color_hex_1'		=> array(
			'label'			=> 'Color Hex 1',
		),
		'color_hex_2'		=> array(
			'label'			=> 'Color Hex 2',
		),
		'meta_created'		=> array(
			'label'			=> 'Meta Created',
			'sortable'		=> true
		),
		'meta_updated'		=> array(
			'label'			=> 'Meta Updated',
			'sortable'		=> true
		)
    );

	protected $pagination = array(
		'posts_per_page'=> 20,
		'paged'			=> 1,
	);
    
    protected $scopes = array(
    	'original'
    );

    protected $query = array(
		'orderby'		=> 'ID',
		'order'			=> 'DESC'
    );

    protected $tabs = array();

    protected $row_template = 'table/mh-garment-color-list-row.php';

	protected $chapter_scope = false;

	protected $wild_card = '';

    public function get_results( $force_refresh = false ) {

    	if( $force_refresh || !isset( $this->results ) ) {

    		$parsed_query = $this->parse_query();

    		if( $this->sections['pagination'] ) {

    			foreach( $this->get_pagination() as $pagination_arg => $pagination_value ) {

    				$parsed_query[ $pagination_arg ] = $pagination_value;
    			}
    		}

    		$this->results = OMH()->garment_color_factory->query( $parsed_query );

    		$this->total_results = OMH()->garment_color_factory->total_rows();

    		$this->set_total_pages( ceil( $this->total_results / $this->pagination['posts_per_page'] ) );
    	}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

    	return $this->results;
    }
}
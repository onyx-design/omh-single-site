<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Table_Orders extends OMH_Table {

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> false,
			'search'		=> false,
			'tabs'			=> true,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'		=> array(
			'post_type'		=> 'shop_order',
			'post_status'	=> array(
				'any'
			),
			'posts_per_page'=> 20,
			'search'		=> null,
			'orderby'		=> 'ID',
			'order'			=> 'DESC',
			'fields'		=> 'ids'
		),
		'scopes' 	=> array(),
		'pagination'	=> array(
			'posts_per_page'=> 20,
			'paged'			=> 1,
		),
		'headers' 	=> array(
			'order'			=> array(
				'label'			=> 'Order',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_order_number'
			),
			'date'			=> array(
				'label'			=> 'Date',
				'sortable'		=> true,
				'query_col'		=> 'post_date'
			),
			'customer' 		=> array(
				'label'			=> 'Customer',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_billing_first_name'
			),
			'role'			=> array(
				'label'			=> 'Role'
			),
			'status'		=> array(
				'label'			=> 'Status',
				'sortable'	=> true
			),
			'subtotal'		=> array(
				'label'			=> 'Subtotal',
			),
			'total'			=> array(
				'label'			=> 'Total',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_order_total'
			),
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $sections = array(
		'before'		=> false,
		'search'		=> false,
		'tabs'			=> true,
		'pagination'	=> true,
		'headers'		=> true,
		'results'		=> true
	);

	protected $query = array(
		'post_type'		=> 'shop_order',
		'post_status'	=> array(
			'any'
		),
		'posts_per_page'=> 20,
		'search'		=> null,
		'orderby'		=> 'ID',
		'order'			=> 'DESC',
		'fields'		=> 'ids'
	);

	protected $pagination = array(
		'posts_per_page'=> 20,
		'paged'			=> 1,
	);

	protected $headers = array(
		'order'			=> array(
			'label'			=> 'Order',
			'sortable'		=> true,
			'query_col'		=> 'metaval:_order_number'
		),
		'date'			=> array(
			'label'			=> 'Date',
			'sortable'		=> true,
			'query_col'		=> 'post_date'
		),
		'customer' 		=> array(
			'label'			=> 'Customer',
			'sortable'		=> true,
			'query_col'		=> 'metaval:_billing_first_name'
		),
		'role'			=> array(
			'label'			=> 'Role'
		),
		'status'		=> array(
			'label'			=> 'Status',
			'sortable'	=> true
		),
		'subtotal'		=> array(
			'label'			=> 'Subtotal',
		),
		'total'			=> array(
			'label'			=> 'Total',
			'sortable'		=> true,
			'query_col'		=> 'metaval:_order_total'
		),
	);

	protected $scopes = array();

	protected $tabs = array();

	protected $row_template = 'table/mh-order-list-row.php';

	/**
	 * CUSTOM ARGS
	 */
	protected $show_customer_col = true;
	
	protected $show_user_role_col = true;

	protected $show_status_col = true;

	protected $show_subtotal_col = true;

	protected $show_price_col = true;

	protected $show_earnings_col = true;

	public function get_results( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->results ) ) {

			$this->do_query_scopes();
			
			$wp_query = new WP_Query( $this->parse_query() );

			$this->total_results = $wp_query->found_posts;
			$this->set_total_pages( $wp_query->max_num_pages );

			$order_ids = $wp_query->get_posts();
			$results = array();

			foreach( $order_ids as $order_id ) {

				$omh_order = wc_get_order( $order_id );

				$results[] = $omh_order;
			}

			$this->results = $results;
		}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

		return $this->results;
	}

	protected function scope_session_chapter() {

	}

	protected function scope_my() {

		$this->show_customer_col = false;
		$this->show_user_role_col = false;

		$this->headers = array(
			'order'			=> array(
				'label'			=> 'Order',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_order_number'
			),
			'date'			=> array(
				'label'			=> 'Date',
				'sortable'		=> true,
				'query_col'		=> 'post_date'
			),
			'status'		=> array(
				'label'			=> 'Status',
				'sortable'		=> true
			),
			'subtotal'		=> array(
				'label'			=> 'Subtotal',
			),
			'total'			=> array(
				'label'			=> 'Total',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_order_total'
			),
		);

		$this->query['meta_query']['customer'] = array(
			'key'			=> '_customer_user',
			'value'			=> get_current_user_id(),
			'compare'		=> '='
		);
	}

	protected function scope_recent() {

		$this->query['posts_per_page'] = 10;
		$this->pagination = array(
			'posts_per_page'	=> 10,
			'paged'				=> 1
		);

		$this->no_results_msg = 'No recent orders';

		$this->query['date_query']['last_thirty_days']	= array(
			'after'		=> '-30 days',
			'column'	=> 'post_date'
		);
	}

	protected function scope_retail() {

		$this->query['meta_query']['has_retail_products']	= array(
			'key'			=> '_related_retail_products',
			'compare'		=> 'EXISTS'
		);
	}

	protected function scope_campaign() {

		$this->query['meta_query']['has_campaign_products'] = array(
			'key'			=> '_related_campaign_products',
			'compare'		=> 'EXISTS'
		);
	}

	protected function scope_bulk() {

		$this->query['meta_query']['has_bulk_products'] = array(
			'key'			=> 'bulk_product_id',
			'compare'		=> 'EXISTS'
		);
	}

	protected function scope_meta_or() {
		
		$this->query['meta_query']['relation'] = 'OR';
	}

	protected function scope_chapter() {

		// Remove User Role Column
		$this->show_user_role_col = false;
		$this->headers = array(
			'order'			=> array(
				'label'			=> 'Order',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_order_number'
			),
			'date'			=> array(
				'label'			=> 'Date',
				'sortable'		=> true,
				'query_col'		=> 'post_date'
			),
			'customer' 		=> array(
				'label'			=> 'Customer',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_billing_first_name'
			),
			'status'		=> array(
				'label'			=> 'Status',
				'sortable'	=> true
			),
			'subtotal'		=> array(
				'label'			=> 'Subtotal',
			),
			'total'			=> array(
				'label'			=> 'Total',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_order_total'
			),
		);
		
		$current_chapter = OMH()->session->get_chapter();

		if( $current_chapter ) {
		
			$this->query['meta_query']['chapter'] = array(
				'key'		=> '_related_chapters',
				'value'		=> serialize( strval( $current_chapter->get_id() ) ),
				'compare'	=> 'LIKE'
			);
		}
	}
}
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OMH_Table_Colleges extends OMH_Table {

	protected $defaults = array(
		'sections'		=> array(
			'before'		=> true,
			'search'		=> true,
			'tabs'			=> false,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'			=> array(
			'orderby'		=> 'ID',
			'order'			=> 'DESC'
		),
		'pagination'	=> array(
			'posts_per_page'	=> 20,
			'paged'				=> 1
		),
		'scopes' 	=> array(),
		'headers' 	=> array(
			'id'				=> array(
				'label'			=> 'ID',
				'sortable'		=> true
			),
			'logo_image_id'		=> array(
				'label'			=> 'Logo'
			),
			'banner_image_id'	=> array(
				'label'			=> 'Banner'
			),
			'college_name'		=> array(
				'label'			=> 'College Name',
				'sortable'		=> true
			),
			'college_shortname'	=> array(
				'label'			=> 'College Shortname',
				'sortable'		=> true,
			),
			'college_letters'	=> array(
				'label'			=> 'College Letters',
				'sortable'		=> true
			),
			'city'	=> array(
				'sortable'		=> true
			),
			'state'	=> array(
				'sortable'		=> true
			),
			'term_id'			=> array(
				'label'			=> 'Term ID',
				'sortable'		=> true,
			),
			'meta_created'		=> array(
				'label'			=> 'Meta Created',
				'sortable'		=> true
			),
			'meta_updated'		=> array(
				'label'			=> 'Meta Updated',
				'sortable'		=> true
			)
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $sections = array(
		'before'		=> true,
		'search'		=> true,
        'tabs'          => false,
        'pagination'	=> true,
        'headers'		=> true,
        'results'		=> true
	);

	protected $headers = array(
    	'id'				=> array(
    		'label'			=> 'ID',
			'sortable'		=> true
    	),
		'logo_image_id'		=> array(
			'label'			=> 'Logo'
		),
		'banner_image_id'	=> array(
			'label'			=> 'Banner'
		),
		'college_name'		=> array(
			'label'			=> 'College Name',
			'sortable'		=> true
		),
		'college_shortname'	=> array(
			'label'			=> 'College Shortname',
			'sortable'		=> true,
		),
		'college_letters'	=> array(
			'label'			=> 'College Letters',
			'sortable'		=> true
		),
		'city'	=> array(
			'sortable'		=> true
		),
		'state'	=> array(
			'sortable'		=> true
		),
		'term_id'			=> array(
			'label'			=> 'Term ID',
			'sortable'		=> true,
		),
		'meta_created'		=> array(
			'label'			=> 'Meta Created',
			'sortable'		=> true
		),
		'meta_updated'		=> array(
			'label'			=> 'Meta Updated',
			'sortable'		=> true
		)
	);

	protected $pagination = array(
		'posts_per_page'=> 20,
		'paged'			=> 1,
	);

	protected $scopes = array();

	protected $query = array(
		'orderby'		=> 'ID',
		'order'			=> 'DESC'
	);

	protected $tabs = array();

	protected $row_template = 'table/mh-college-list-row.php';

	protected $chapter_scope = false;

	protected $wild_card = '';

	public function get_results( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->results ) ) {

    		$parsed_query = $this->parse_query();

    		if( $this->sections['pagination'] ) {

    			foreach( $this->get_pagination() as $pagination_arg => $pagination_value ) {

    				$parsed_query[ $pagination_arg ] = $pagination_value;
    			}
    		}

    		$this->results = OMH()->college_factory->query( $parsed_query );

    		$this->total_results = OMH()->college_factory->total_rows();

    		$this->set_total_pages( ceil( $this->total_results / $this->pagination['posts_per_page'] ) );
		}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

		return $this->results;
	}

	protected function get_html_before() {
		
		$html_before = '';

		if( is_super_admin() ) {

			$html_before .= OMH_HTML_UI_Button::factory(
	    		array(
	    			'color'		=> 'primary',
	    			'size'		=> 'small',
	    			'label'		=> 'Add College',
	    			'attrs'		=> array(
	    				'data-toggle'	=> 'modal',
	    				'data-target'	=> '#college-modal'
	    			)
	    		)
	    	)->render();
		}

		return $html_before;
	}
}
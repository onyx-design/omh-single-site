<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

//dev:REMOVE

class OMH_Table_Product_Garments extends OMH_Table {

	protected $defaults = array(
		'sections'		=> array(
			'before'		=> false,
			'tabs'          => false,
			'search'		=> false,
			'pagination'	=> false,
			'headers'		=> true,
			'results'		=> true
		),        
		'query'		=> array(
			'product_id'    => 0,
		),
		'scopes' 	=> array(),
		'headers' 	=> array(
			'actions'		=> array(
				'label'		=> '',
			),
			// 'garment'       => array(
			//     'label'     => 'Garment'
			// ),
			'front_image'	=> array(
				'label'		=> 'Front'
			),
			'back_image'	=> array(
				'label'		=> 'Back'
			),
			'proof_image'	=> array(
				'label'     => 'Proof'
			),
			'base_price'    => array(
				'label'     => 'Pricing'
			),
			'sizes'		    => array(
				'label'     => 'Enabled Sizes'
			)
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $sections = array(
		'before'		=> false,
		'tabs'          => false,
		'search'		=> false,
		'pagination'	=> false,
		'headers'		=> true,
		'results'		=> true
	);
	
	protected $headers = array(
		'actions'		=> array(
			'label'		=> '',
		),
		// 'garment'       => array(
		//     'label'     => 'Garment'
		// ),
		'front_image'	=> array(
			'label'		=> 'Front'
		),
		'back_image'	=> array(
			'label'		=> 'Back'
		),
		'proof_image'	=> array(
			'label'     => 'Proof'
		),
		'base_price'    => array(
			'label'     => 'Pricing'
		),
		'sizes'		    => array(
			'label'     => 'Enabled Sizes'
		)
	);
	
	protected $scopes = array();

	protected $tabs = array();

	protected $query = array(
		'product_id'    => 0
	);

	protected $row_template = 'table/mh-product-garment-row-template.php';

	protected $sales_type = '';
	// Column Visibility

	protected $show_garment_col = false;

	protected $show_front_image_col = true;

	protected $show_back_image_col = true;

	protected $show_proof_image_col = true;

	protected $show_price_tiers_col = true;
	
	protected $show_sizes_col = true;

	public function get_results( $force_refresh = false ) {
		$product = wc_get_product( $this->query['product_id'] );

		// $this->price_tiers = $this->query['price_tiers_meta'];

		$this->sales_type = $product->get_sales_type();
		$this->results = $product->get_garments();

		if($this->sales_type === 'retail') {
			$this->row_template = '/table/mh-product-garment-retail-row-template.php';
		} elseif($this->sales_type === 'campaign') {
			$this->row_template = '/table/mh-product-garment-campaign-row-template.php';
		}

		return $this->results;
	}

	/**
	 * dev:todo Remove this as we're no longer doing any html from PHP files unless it's a template
	 * 
	 * @return 	string
	 */
	protected function get_html_results() {

		ob_start();
		// TODO: REview with Ray Do Not Use
		// foreach( $this->get_results() as $product_garment ) {
		// 	include OMH_TEMPLATE_PATH . '/table/mh-product-garments-row.php';
		// }
		
		if( $this->row_template ) {
			include OMH_TEMPLATE_PATH . $this->row_template;
		}

		return ob_get_clean();

	}

}
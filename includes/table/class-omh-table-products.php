<?php
if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class OMH_Table_Products extends OMH_Table
{

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> true,
			'search'		=> true,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'query'		=> array(
			'post_type'		=> 'product',
			'post_status'	=> array(
				'publish'
			),
			'orderby'		=> 'ID',
			'order'			=> 'DESC',
			'fields'		=> 'ids',
			'posts_per_page' => -1,
			'tax_query'		=> array(
				array(
					'taxonomy'	=> 'product_type',
					'field'		=> 'name',
					'terms'		=> 'variable',
				)
			)
		),
		'pagination'	=> array(
			'posts_per_page' => 20,
			'paged'			=> 1,
		),
		'scopes' 	=> array(),
		'headers' 	=> array(
			'image'				=> array(
				'label'		=> 'Preview',
			),
			'name'				=> array(
				'label'		=> 'Product Name',
				'sortable'	=> true,
				'query_col'	=> 'title'
			),
			'type'				=> array(
				'label'		=> 'Sales Type',
			),
			'chapter'			=> array(
				'label'		=> 'Chapter',
			),
			'sku'				=> array(
				'label'		=> 'SKU',
				'sortable'	=> true,
				'query_col'	=> 'metaval:_sku'
			),
			'status'			=> array(
				'label'		=> 'Status',
			)
		),
		'title'			=> array(),
		'tabs'			=> array()
	);

	protected $sections = array(
		'before'		=> false,
		'search'		=> true,
		'tabs'			=> true,
		'pagination'	=> true,
		'headers'		=> true,
		'results'		=> true
	);

	protected $query = array(
		'post_type'		=> 'product',
		'post_status'	=> array(
			'publish'
		),
		'posts_per_page' => -1,
		'orderby'		=> 'ID',
		'order'			=> 'DESC',
		'fields'		=> 'ids',
		'tax_query'		=> array(
			array(
				'taxonomy'	=> 'product_type',
				'field'		=> 'name',
				'terms'		=> 'variable',
			)
		)
	);

	protected $pagination = array(
		'posts_per_page' => 20,
		'paged'			=> 1,
	);

	protected $headers = array(
		'image'				=> array(
			'label'		=> 'Preview',
		),
		'name'				=> array(
			'label'		=> 'Product Name',
			'sortable'	=> true,
			'query_col'	=> 'title'
		),
		'type'				=> array(
			'label'		=> 'Sales Type',
		),
		'chapter'			=> array(
			'label'		=> 'Chapter',
		),
		'sku'				=> array(
			'label'		=> 'SKU',
			'sortable'	=> true,
			'query_col'		=> 'metaval:_sku'
		),
		'status'			=> array(
			'label'		=> 'Status',
		)
	);

	protected $row_template = 'table/mh-product-list-row.php';

	// the scopes listed must match the function with the prefixes of scope
	protected $scopes = array(
		'all'
	);

	protected $tabs = array(
		'all'			=> array(
			'name'			=> 'All',
			'scope'			=> 'all',
			'clear_scopes'	=> true
		),
		'sales_type_unassigned'	=> array(
			'name'			=> 'Unassigned Sales Type',
			'scope'			=> 'sales_type_unassigned',
			'clear_scopes'	=> true
		)
	);

	/**
	 * CUSTOM ARGS
	 */

	protected $show_status_col = true;

	protected $show_sales_col = true;

	protected $show_earnings_col = true;

	protected $show_price_col = true;

	protected $show_featured_col = true;

	protected $chapter_scope = false;

	protected $active_tab = 'all';

	public function get_results($force_refresh = false)
	{

		if ($force_refresh || !isset($this->results)) {

			$this->do_query_scopes();

			$wp_query = new WP_Query($this->parse_query());

			$this->total_results = $wp_query->found_posts;
			$this->set_total_pages($wp_query->max_num_pages);

			$product_ids = $wp_query->get_posts();
			$results = array();

			foreach ($product_ids as $product_id) {

				if ($product = wc_get_product($product_id)) {
					$results[] = $product;
				} else {
					log_print_r("There is a problem retrieving Product ID: {$product_id}", 'OMH Fatal Error');
				}
			}

			$this->results = $results;
		}

		if (count($this->results) < 1) {
			$this->set_no_results();
		}

		return $this->results;
	}

	protected function parse_search()
	{

		$search_terms = $this->query['search'];

		$search_query = array(
			'relation'	=> 'OR'
		);

		$search_query[] = array(
			'key'		=> '_search_string',
			'value'		=> $search_terms,
			'compare'	=> 'LIKE'
		);

		// foreach( explode( ' ', $search_terms ) as $search_term ) {

		// 	$search_query[] = array(
		// 		'key'		=> '_search_string',
		// 		'value'		=> $search_term,
		// 		'compare'	=> 'LIKE'
		// 	);
		// }

		$this->add_meta_query($search_query, 'search');
	}

	protected function remove_search()
	{

		$this->query['search'] = $this->query['s'] = null;

		unset($this->query['meta_query']['search']);
	}

	// protected function get_html_before() {

	// 	$html_before = '';

	// 	if( in_array( 'design_approved', $this->scopes ) ) {

	// 		$html_before .= '<h4>Approved Designs ready to list</h4>';
	// 		$html_before .= '<p class="small text-muted">These retail designs have been approved. You can now list them.</p>';
	// 	}

	// 	if( in_array( 'design_in_review', $this->scopes ) ) {

	// 		$html_before .= '<h4>Designs currently in review</h4>';
	// 		$html_before .= '<p class="small text-muted">The Merch House art team is currently working on the designs below. Email <a href="mailto:art@merch.house">art@merch.house</a> for any updates.</p>';
	// 	}

	// 	return $html_before;
	// }

	protected function scope_all()
	{
		$this->active_tab = 'all';

		$this->query['tax_query'] = array(
			array(
				'taxonomy'	=> 'product_type',
				'field'		=> 'name',
				'terms'		=> 'variable',
			)
		);
	}

	protected function scope_sales_type_unassigned()
	{
		$this->active_tab = 'sales_type_unassigned';

		$this->query['tax_query']['relation'] = 'AND';

		$this->query['tax_query']['sales_type'] = array(
			'taxonomy' => 'sales_type',
			'field' => 'name',
			'terms' => array('bulk', 'retail', 'campaign'),
			'operator' => 'NOT EXISTS'
		);
	}

	protected function scope_dashboard_all()
	{

		$this->scopes = array('all');
		$this->sections['tabs'] = false;
	}

	protected function scope_design_in_review()
	{

		$this->row_template = 'table/mh-all-product-list-row.php';

		$this->chapter_scope = true;

		$this->headers = array(
			'image'				=> array(
				'label'		=> 'Preview',
			),
			'name'				=> array(
				'label'		=> 'Name',
				'sortable'	=> true,
				'query_col'	=> 'title'
			),
			'type'				=> array(
				'label'		=> 'Type'
			),
			'status'			=> array(
				'label'		=> 'Status',
			),
			'sku'			=> array(
				'label'		=> 'SKU',
				'sortable'	=> true,
				'query_col'		=> 'metaval:_sku'
			),
			'price'				=> array(
				'label'		=> 'Price',
			),
			'total_sold'		=> array(
				'label'		=> '# Sold'
			)
		);

		// $this->query['tax_query']['sales_type'] = array(
		// 	'taxonomy' 	=> 'sales_type',
		// 	'field' 	=> 'name',
		// 	'terms' 	=> array( 'bulk', 'retail', 'campaign' ),
		// 	'operator' 	=> 'IN'
		// );

		$this->query['tax_query']['status'] = array(
			'taxonomy'	=> 'product_status',
			'field'		=> 'slug',
			'terms'		=> array(
				'design_in_review',
			),
			'operator'	=> 'IN'
		);
	}
}

<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Table_House_Orders extends OMH_Table {

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> false,
			'search'		=> false,
			'tabs'			=> false,
			'pagination'	=> true,
			'headers'		=> true,
			'results'		=> true
		),
		'filters'		=> array(),
		'pagination'	=> array(
			'posts_per_page'=> 20,
			'paged'			=> 1,
		),
		'headers' 	=> array(
			'actions'		=> array(
				'label'			=> ''
			),
			'order'			=> array(
				'label'			=> 'Order',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_order_number'
			),
			'date'			=> array(
				'label'			=> 'Date',
				'sortable'		=> true,
				'query_col'		=> 'post_date'
			),
			'customer' 		=> array(
				'label'			=> 'Customer',
				'sortable'		=> true,
				'query_col'		=> 'metaval:_billing_first_name'
			),
			'status'		=> array(
				'label'			=> 'Status',
				'sortable'	=> true
			),
			'earnings'			=> array(
				'label'			=> 'Earnings',
				'sortable'		=> true,
			),
		),
		'title'			=> array(),
	);

	protected $sections = array(
		'before'		=> false,
		'search'		=> false,
		'tabs'			=> false,
		'pagination'	=> true,
		'headers'		=> true,
		'results'		=> true
	);

	protected $pagination = array(
		'posts_per_page'=> 20,
		'paged'			=> 1,
	);

	protected $headers = array(
		'actions'		=> array(
			'label'			=> ''
		),
		'order'			=> array(
			'label'			=> 'Order',
			'sortable'		=> true,
			'query_col'		=> 'metaval:_order_number'
		),
		'date'			=> array(
			'label'			=> 'Date',
			'sortable'		=> true,
			'query_col'		=> 'post_date'
		),
		'customer' 		=> array(
			'label'			=> 'Customer',
			'sortable'		=> true,
			'query_col'		=> 'metaval:_billing_first_name'
		),
		'status'		=> array(
			'label'			=> 'Status',
			'sortable'	=> true
		),
		'earnings'			=> array(
			'label'			=> 'Earnings',
			'sortable'		=> true,
		),
	);

	protected $scopes = array(
		// 'chapter'
	);

	protected $row_template = 'table/mh-house-order-list-row.php';

	protected $order_ids = array();

	/**
	 * CUSTOM ARGS
	 */
	protected $show_customer_col = true;
	
	protected $show_user_role_col = false;

	protected $show_status_col = true;

	protected $show_earnings_col = true;

	public function get_results( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->results ) ) {

			$this->chapter = OMH()->chapter_factory->read( $this->filters['chapter_id'] );

			$this->do_query_scopes();

			$this->order_ids = $this->order_ids ?: OMH()->chapter_factory->get_chapter_orders(
				$this->chapter,
				'ids',
				$this->pagination['posts_per_page'],
				( $this->pagination['posts_per_page'] * ( $this->pagination['paged'] - 1 ) )
			);

			$this->total_results = $this->total_results ?: OMH()->chapter_factory->get_chapter_orders( 
				$this->chapter, 
				'count', 
				-1, 
				( $this->pagination['posts_per_page'] * ( $this->pagination['paged'] - 1 ) ) 
			);

			$this->set_total_pages( ceil( $this->total_results / $this->pagination['posts_per_page'] ) );

			$results = array();

			foreach( $this->order_ids as $order_id ) {

				$results[] = wc_get_order( $order_id );
			}

			$this->results = $results;
		}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

		return $this->results;
	}
}
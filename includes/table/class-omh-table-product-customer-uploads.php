<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class OMH_Table_Product_Customer_Uploads extends OMH_Table {

	protected $defaults = array(
		'sections'	=> array(
			'before'		=> false,
			'tabs'			=> false,
			'search'		=> false,
			'pagination'	=> false,
			'headers'		=> false,
			'results'		=> true
		),
		'query'		=> array(
			'id'	=> 0
		),
		'headers' 	=> array(
			'actions' 			=> array(
				'label'		=> ''
			),
			'preview'	=> array(
			),
			'info'		=> array(
			),
		),
		'scopes' 	=> array(),
		'title'			=> array(),
		'tabs' 	=> array(),
	);

	protected $row_template = 'table/mh-product-customer-uploads-list-row.php';

	protected $sections = array(
		'before'		=> false,
		'tabs'			=> false,
		'search'		=> false,
		'pagination'	=> false,
		'headers'		=> false,
		'results'		=> true
	);

	protected $query = array(
		'id'	=> 0
	);

	protected $headers = array(
		'actions' 			=> array(
			'label'		=> ''
		),
		'preview'	=> array(
		),
		'info'		=> array(
		),
	);

	protected $scopes = array();

	protected $tabs = array();

	public $no_results_msg = 'No assets uploaded.';

	public function get_results( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->results ) ) {

			$this->do_query_scopes();

			$product = wc_get_product( $this->query['id'] );

			if( $product && ( $customer_uploads = $product->get_customer_uploads() ) ) {

				$this->total_results = count( $customer_uploads );

				$results = array();

				foreach( $customer_uploads as $customer_upload ) {

					$results[] = $customer_upload;
				}

				$this->results = $results;
			} else {

				$this->results = array();
			}
		}

		if( count( $this->results ) < 1 ) {
			$this->set_no_results();
		}

		return $this->results;
	}
}
<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Session {

	/**
	 * @var OMH_Session Singleton
	 */
	protected static $_instance = null;

	protected static $user = null;

	protected static $chapter = null;

	/**
	 * Main OMH_Session Instance
	 * @return type
	 */
	public static function instance() {

		if( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function __construct() {

		if( is_user_logged_in() ) {

			self::set_user( get_current_user_id() );

			self::get_chapter();
		}

		if( self::$user && self::$user->has_role( 'administrator' ) ) {

			if( isset( $_GET['ch'] ) && $_GET['ch'] ) {

				self::set_chapter( $_GET['ch'] );
			}
		}

		// add_action( 'wp_logout', 'OMH_Session::clear_session' );
	}

	/**
	 * dev:todo Used for changing the chapter that the super admin is currently viewing
	 * 
	 * @param type $session 
	 * @return type
	 */
	public static function change_session( $session ) {

		if( !is_super_admin() ) {
			return false;
		}
	}

	/**
	 * Get the Session User
	 * 
	 * @return 	OMH_User|null
	 */
	public static function get_user() {

		return self::$user;
	}

	/**
	 * Set the Session User
	 * 
	 * @param 	int 	$user_id
	 * 
	 * @return 	OMH_User|null
	 */
	public static function set_user( $user_id ) {

		self::$user = OMH_User::get_user( $user_id );

		return self::get_user();
	}

	/**
	 * Get the Session Chapter ID
	 * 
	 * @return 	int
	 */
	public static function get_chapter() {

		if( !self::$user ) {
			return null;
		}

		if( !self::$chapter ) {

			// Written out for readability rather than using ternary operators
			if( isset( $_SESSION['chapter_id'] ) && $_SESSION['chapter_id'] ) {

				$chapter_id = $_SESSION['chapter_id'];
			} else {

				$chapter_id = self::$user->get_primary_chapter_id();
			}

			if( $chapter_id ) {
				self::set_chapter( $chapter_id );
			}
		}

		return self::$chapter;
	}

	/**
	 * Sets the Chapter session
	 * 
	 * @param 	int 	$chapter
	 * 
	 * @return 	int
	 */
	public static function set_chapter( $chapter_id ) {

		if( !self::$user ) {
			return null;
		}

		if( $chapter_id && ( $chapter = OMH()->chapter_factory->read( $chapter_id ) )  ){

			$_SESSION['chapter_id'] = $chapter_id;

			if( self::$user->has_role( 'administrator' ) ) {
				self::$user->set_primary_chapter_id( $chapter_id );
			}

			self::$chapter = $chapter;
		}

		return self::get_chapter();
	}

	public static function clear_session() {

		unset( $_SESSION['chapter_id'] );
	}
}
<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_AJAX {

	/**
	 * @var array
	 */
	public static $messages = array();

	protected static $response_data;

	protected static $allow_send_json = true;

	protected static $response_status = 'success';

	protected static $refresh_tables = false;

	protected static $response_redirect = null;

	/**
	 * Init function for OMH_Ajax
	 * 
	 * @return 	void
	 */
	public static function init() {

		add_action( 'init', array( __CLASS__, 'define_ajax' ), 0 );

		self::add_ajax_events();
	}

	/**
	 * Set WC AJAX constant and header.
	 * 
	 * @return 	void
	 */
	public static function define_ajax() {

		if ( ! empty( $_POST['omh_ajax'] ) ) {

			wc_maybe_define_constant( 'DOING_AJAX', true );
			wc_maybe_define_constant( 'OMH_DOING_AJAX', true );
			if ( ! WP_DEBUG || ( WP_DEBUG && ! WP_DEBUG_DISPLAY ) ) {

				@ini_set( 'display_errors', 0 ); // Turn off display_errors during AJAX events to prevent malformed JSON
			}

			$GLOBALS['wpdb']->hide_errors();
		}
	}

	/**
	 * Hook in methods - uses WordPress AJAX handlers (admin-ajax).
	 * 
	 * @return 	void
	 */
	public static function add_ajax_events() {

		// woocommerce_EVENT => nopriv
		$ajax_events = array(
			// Universal
			'change_chapter'						=> false,
			'table_request'							=> false,
			'load_data'								=> false,
			'ajax_search'							=> true,
			'site_search'							=> true,

			// Global garments
			'get_global_garment_from_inksoft'		=> false,
			'add_global_garment_from_inksoft'		=> false,
			'enable_global_garment_style'			=> false,
			'save_garment_color'					=> false,
			'save_garment_brand'					=> false,

			// User Functions
			'add_user'								=> false,
			'invite_house_members'					=> false,
			'house_admin_activation'				=> true,
			'house_member_activation'				=> true,
			'account_activation'					=> true,
			'resend_user_activation'				=> false,
			'set_user_role'							=> false,
			'save_user_account_details'				=> false,
			'save_user_password'					=> false,
			'save_user_addresses'					=> false,
			'set_user_chapter'						=> false,
			'remove_house_user'						=> false,

			// Product Functions
			'create_product'						=> false,
			'delete_product'						=> false,
			'save_dashboard_product_details'		=> false,
			'save_dashboard_edit_bulk_details'		=> false,
			'checkout_dashboard_edit_bulk_details'	=> false,
			'save_dashboard_product_design_details'	=> false,
			'convert_product'						=> false,
			'product_convert_to_bulk'				=> false,
			'product_convert_to_retail'				=> false,
			'product_convert_to_campaign'			=> false,
			'generate_product_sales_csv'			=> false,
			'publish_product'						=> false,
			'unpublish_product'						=> false,

			// Chapters, Organizations, and Colleges
			'save_chapter'							=> false,
			'save_organization'						=> false,
			'save_college'							=> false,

			// Store Credit Functions
			'add_credit_adjustment'					=> false,

			// General and Misc.
			'contact_us_form'						=> true,
			'temporary_file_upload'					=> false,
			'signup_form'							=> true,
			'start_design_form'						=> true,
			'organization_start_design_form'		=> true,
			'new_omh_follower'						=> true
		);

		foreach ( $ajax_events as $ajax_event => $nopriv ) {
			add_action( 'wp_ajax_omh_' . $ajax_event, array( __CLASS__, $ajax_event ) );

			if ( $nopriv ) {
				add_action( 'wp_ajax_nopriv_omh_' . $ajax_event, array( __CLASS__, $ajax_event ) );

				// WC AJAX can be used for frontend ajax requests.
				add_action( 'wc_ajax_' . $ajax_event, array( __CLASS__, $ajax_event ) );
			}
		}
	}

	/**
	 * Generalized AJAX nonce check
	 * 
	 * @param 	string $nonce
	 * @param 	string $query_arg
	 * 
	 * @return  boolean/void
	 */
	protected static function check_nonce( $nonce, $query_arg = 'security' ) {

		if( !check_ajax_referer( $nonce, $query_arg, false ) ) {

			self::add_message( 'There was an error authorizing your request. Please reload the page and try again.', 'danger', 'omh_ajax_nonce_invalid' );
			self::$response_status = 'fail';
			self::send_ajax_response();
			return false;
		}
		
		return true;
	}

	/**
	 * Add message to the message array
	 * 
	 * @param 	string 	$content
	 * @param 	string 	$type 			'success'
	 * 									'fail'
	 * 
	 * @return 	array
	 */
	public static function add_message( $content, $type = 'success', $key = null, $target = null ) {

		array_unshift( self::$messages, array( 'content' => $content, 'type' => $type, 'key' => $key, 'target' => $target ) );	

		return self::$messages;
	}

	/**
	 * Send the AJAX response and die
	 * 
	 * @param 	mixed 	$response_data
	 * @param 	string 	$message
	 * @param 	string 	$status 		'success'
	 * 									'fail'
	 * 
	 * @return type
	 */
	protected static function send_ajax_response( $message = null ) {
		
		if( self::$allow_send_json ) {

			if( $message ) {
				self::add_message( $message, self::$response_status );
			}

			wp_send_json( array(
				'data'		=> self::$response_data,
				'status'	=> self::$response_status,
				'request'	=> array(
					'action'	=> $_POST['action'],
					'omh_ajax' 	=> ( isset( $_POST['omh_ajax'] ) ? $_POST['omh_ajax'] : null )
				),
				'messages'	=> self::$messages,
				'redirect'	=> self::$response_redirect,
				'refresh_tables' => self::$refresh_tables
			) );
		}
	}

	/********************************
	 *			 Universal			*
	 ********************************/

	public static function change_chapter() {

		self::check_nonce( 'change-chapter' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		if( is_super_admin() ) {

			self::$response_redirect = OMH()->dashboard_url( $omh_ajax['page'], $omh_ajax['chapterId'] );
		}

		self::send_ajax_response();
	}

	/**
	 * dev:improve This currently does nothing
	 * 
	 * @return 	void
	 */
	public static function table_request() {

		self::check_nonce( 'omh-mh-table-request' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$omh_table = OMH_Table::factory( $omh_ajax );

		self::$response_data = $omh_table->get_response();

		self::send_ajax_response();
	}

	public static function load_data() {

		self::check_nonce( 'omh_load_data_nonce' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$response_obj = array();

		if( isset( $omh_ajax['model'], $omh_ajax['id'] ) 
			&& $omh_ajax['model']
			&& $omh_ajax['id']
		) {

			// If wp_ do different

			// else do omh_

			$omh_table = $omh_ajax['model'];
			$omh_table_id = $omh_ajax['id'];

			$model = OMH_Model::get_instance_by_table( $omh_table_id, $omh_table );

			if( $model ) {

				$response_obj = array_map( function( $field ) {

					if( is_array( $field ) ) {

						return array(
							'value'			=> $field['value'] ?? '',
							'value_label'	=> $field['value_label'] ?? ''
						);
					} else {

						return array(
							'value'	=> $field
						);
					}

					return array(
					);
				}, $model->to_array( true ) );

				// Image Object Handling //dev:improve
				foreach( $response_obj as $field => &$value ) {

					if( strpos( $field, 'image_id' ) !== false ) {

						$value['value'] = OMH_Tool_Sideload_Media::get_media_image_object( $value['value'] );
					}
				}

				omh_debug( $response_obj );

				
			} elseif( in_array( $omh_table, array( 'wp_products' ) ) ) {
				
				if( $product = wc_get_product( $omh_table_id ) ) {

					$response_obj = array(
						'ID'				=> array(
							'value' => $omh_table_id
						),
						'product_status'	=> array(
							'value'		=> 'design_in_review',
							'options'	=> array_map( function( $field ) {
								return $field['label'];
							}, OMH_Product_Variable::$product_statuses )
						),
						'product_name'		=> array(
							'value'			=> $product->get_name()
						),
						'product_chapter'	=> array(
							'value'			=> $product->get_chapter()->get_chapter_name()
						)
					);
				} else {

					self::$response_status = 'fail';

					self::add_message( "Product ID: {$omh_table_id} not found.", 'danger', 'omh_load_data' );
				}
			} elseif(in_array( $omh_table, array( 'omh_user' ) ) ){

				$omh_user = get_user_by( 'id', $omh_table_id );

				$response_obj = array(
					'user_id'				=> array(
						'value' 		=> $omh_table_id
					),
					'email'				=> array(
						'value'			=> $omh_user->get_email()
					),
					'name'				=> array(
						'value'			=> $omh_user->get_full_name()
					)
				);
			}
			else {

				self::$response_status = 'fail';

				self::add_message( "Could not load data for ID: {$omh_table_id} in {$omh_table}.", 'danger', 'omh_load_data' );
			}
		} else {

			self::$response_status = 'fail';

			self::add_message( "Missing model or ID.", 'danger', 'omh_load_data' );
		}

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	public static function ajax_search() {

		self::check_nonce( 'omh_ajax_search_nonce' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$response_obj = array();

		if( isset( $omh_ajax['omh_model'], $omh_ajax['search_term'] ) 
			&& $omh_ajax['omh_model']
			&& $omh_ajax['search_term']
		) {

			$omh_table = $omh_ajax['omh_model'];
			$search_term = $omh_ajax['search_term'];

			if ($omh_table == "public_search"){
				$response_obj = OMH_Model::search_public_orgs_chapters($search_term);
				self::$response_data = $response_obj;
				self::send_ajax_response();
			} else {
				
				$model = OMH_Model::get_model_by_table( $omh_table );
				
				if( $model ) {
				
					$search_results = $model::ajax_search( $search_term );
				
					if( $search_results ) {
						omh_debug($search_results);
						$response_obj = $search_results;
					} 
					
					else {
						self::$response_status = 'fail';
						self::add_message( "Search AJAX not setup for {$omh_table}.", 'danger', 'omh_search_model' );
					}
				} 
				
				else {

					self::$response_status = 'fail';
				
					self::add_message( "Could not search {$omh_table}.", 'danger', 'omh_search_model' );
				}
			}
		} else {

			self::$response_status = 'fail';

			self::add_message( "Missing model.", 'danger', 'omh_search_model' );
		}

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	public static function site_search() {

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$response_obj = array(
			'products'		=> false,
			'chapters'		=> false,
			'organizations'	=> false
		);

		if( isset( $omh_ajax['search'], $omh_ajax['type'] ) 
			&& $omh_ajax['search']
			&& $omh_ajax['type']
		) {
			$search_term = $omh_ajax['search'];
			$search_type = $omh_ajax['type'];

			$chapters = OMH()->chapter_factory->query(
				array(
					'posts_per_page'	=> -1,
					'search'			=> $search_term
				)
			);

			$organizations = OMH()->organization_factory->query(
				array(
					'posts_per_page'	=> -1,
					'search'			=> $search_term
				)
			);

			if( in_array( $search_type, array( 'all', 'products' ) ) ) {

				$visible_statuses = OMH_Product_Variable::get_visible_product_statuses( 'national' );

				$like_term = str_replace( ' ', '%', $search_term );

				$product_query = new WP_Query(
					array(
						'post_type'		=> 'product',
						's'				=> $search_term,
						'tax_query'		=> array(
							array(
								array(
									'taxonomy'	=> 'chapters',
									'field'		=> 'term_id',
									'terms'		=> array_map( function( $chapter ) {
										return $chapter->get_term_id();
									}, $chapters )
								),
								array(
									'taxonomy'	=> 'organizations',
									'field'		=> 'term_id',
									'terms'		=> array_map( function( $org ) {
										return $org->get_term_id();
									}, $organizations )
								),
								'relation'	=> 'OR'
							),
							array(
								'taxonomy'	=> 'product_status',
								'field'		=> 'slug',
								'terms'		=> $visible_statuses,
								'operator'	=> 'IN'
							)
						),
						'meta_query'	=> array(
							// array(
							// 	'key'		=> '_visibility_mh',
							// 	'value'		=> 'public',
							// ),
							array(
								'key'		=> '_sku',
								'value'		=> $like_term,
								'compare'	=> 'LIKE'
							),
							// 'relation'	=> 'OR'
						)
					)
				);

				$response_obj['products'] = $product_query->get_posts();
			}

			if( in_array( $search_type, array( 'all', 'chapters' ) ) ) {

				$response_obj['chapters'] = array_map( function( $chapter ) {

					return $chapter->to_array();
				}, $chapters );
			}

			if( in_array( $search_type, array( 'all', 'organizations' ) ) ) {

				$response_obj['organizations'] = array_map( function( $organization ) {

					return $organization->to_array();
				}, $organizations );
			}
		}

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	public static function get_global_garment_from_inksoft() {

		self::check_nonce( 'add-global-garment-from-inksoft' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$response_obj = array();
		self::$response_status = 'fail';

		if( isset( $omh_ajax['inksoft_product_id'] ) ) {

			$inksoft_api_response = OMH_API_Inksoft::request( 'get_product', array( 'query_params' => array( 'ProductId' => $omh_ajax['inksoft_product_id'] ) ) );

			// If-ception

			// Check that this product exists on InkSoft
			if( $inksoft_product = $inksoft_api_response->data() ) {

				// If the product exists on InkSoft AND we already have it in our system, we need to loop over the styles so we don't double-add garment styles
				if( $garment_product = OMH()->garment_product_factory->get_by_inksoft_id( $inksoft_product->ID ) ) {

					// Get the styles attached to our Global Garment Product
					if( $garment_styles = $garment_product ? $garment_product->get_garment_styles() : array() ) {

						// Loop over the InkSoft styles and if they exist in our system, add a key to track that we have the style added already
						foreach( $inksoft_product->Styles as $inksoft_product_style_key => $inksoft_product_style ) {

							if( isset( $garment_styles[ $inksoft_product_style->ID ] ) ) {
								$inksoft_product->Styles[ $inksoft_product_style_key ]->Added = true;
							}
						}
					}
				}

				$response_obj = $inksoft_product;

				self::$response_status = 'success';

				// dev:todo Return json
				// ob_start();
				// include OMH_TEMPLATE_PATH . 'table/mh-inksoft-garment-product-template.php';
				// $response_obj = ob_get_clean();	
			} else {
				self::add_message( 'No product found on InkSoft with ID: ' . $omh_ajax['inksoft_product_id'], 'danger', 'omh_get_global_garment_from_inksoft' );
			}
		} else {
			self::add_message( 'No product ID found in search term.', 'danger', 'omh_get_global_garment_from_inksoft' );
		}

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	public static function add_global_garment_from_inksoft() {

		self::check_nonce( 'add-global-garment-from-inksoft' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		self::$response_status = 'fail';

		if( isset( $form_data['inksoft_product_id'] ) ) {

			$inksoft_product = OMH_API_Inksoft::request( 'get_product', array( 'query_params' => array( 'ProductId' => $form_data['inksoft_product_id'] ) ) );

			if( $inksoft_product && isset( $form_data['inksoft_style_additions'] ) ) {

				$inksoft_styles = array_column( $inksoft_product->Styles, null, 'ID' );

				if( $garment_product = OMH_Model_Garment_Product::get_or_create_from_api_response( $inksoft_product->data() ) ) {

					foreach( $form_data['inksoft_style_additions'] as $inksoft_style_id => $inksoft_style_added ) {

						if( $inksoft_style_added && isset( $inksoft_styles[ $inksoft_style_id ] ) ) {

							$inksoft_style = $inksoft_styles[ $inksoft_style_id ];
							$inksoft_style->ProductId = $form_data['inksoft_product_id'];

							if( $garment_product_style = OMH_Model_Garment_Style::get_or_create_from_api_response( $inksoft_styles[ $inksoft_style_id ] ) ) {

							} else {
								self::add_message( 'There was a problem adding InkSoft Style ID: ' . $inksoft_style_id . '.', 'danger', 'add_global_garment_from_inksoft--' . $inksoft_style_id );
							}
						} else {

							self::add_message( 'InkSoft Style ID: ' . $inksoft_style_id . ' not found in response.', 'danger', 'add_global_garment_from_inksoft--' . $inksoft_style_id );
						}
					}

					OMH_Model_Garment_Style::build_style_full_names();

					self::add_message( 'Successfully added InkSoft Product ID: ' . $form_data['inksoft_product_id'] . '.', 'success', 'add_global_garment_from_inksoft' );

					self::$refresh_tables = true;
					self::$response_status = 'success';
				} else {

					self::add_message( 'There was a problem adding InkSoft Product ID: ' . $form_data['inksoft_product_id'] . '.', 'danger', 'add_global_garment_from_inksoft' );
				}
			} else {

				self::add_message( 'Error retrieving InkSoft Product ID: ' . $form_data['inksoft_product_id'] . ' from InkSoft.', 'danger', 'add_global_garment_from_inksoft' );
			}
		}

		self::send_ajax_response();
	}

	public static function enable_global_garment_style() {

		self::check_nonce( 'enable-global-garment-style' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		self::$response_status = 'fail';

		if( isset( $omh_ajax['obj_id'] ) && $garment_style = OMH()->garment_style_factory->read( $omh_ajax['obj_id'] ) ) {

			$valid = true;
			$invalid_msg = '';
			$enabled = isset( $form_data['enabled_style'] ) ? maybe_str_to_bool( $form_data['enabled_style'] ) : false;

			if( $enabled ) {

				// Check that this style can be enabled by making sure the associated color and brand values are valid
				$garment_color = $garment_style->get_garment_color();
				$garment_brand = $garment_style->get_garment_product() ? $garment_style->get_garment_product()->get_garment_brand() : false;

				if( !$garment_color ) {

					$valid = false;
					$invalid_msg = 'No associated color found.';
				} elseif( $garment_color && !$garment_color->is_valid() ) {

					$valid = false;
					$invalid_msg = 'Color is invalid - missing requried fields.';
				}

				if( !$garment_brand ) {

					$valid = false;
					$invalid_msg .= ' No associated brand found.';
				} elseif( $garment_brand && !$garment_brand->is_valid() ) {

					$valid = false;
					$invalid_msg .= ' Brand is invalid - missing required fields.';
				}
			}

			if( $valid ) {
				$garment_style->set_enabled( ( $enabled ? 1 : 0 ) );
			}

			if( $valid && $garment_style->save() ) {

				self::add_message( 'Successfully ' . ( $enabled ? 'enabled' : 'disabled' ) . ' garment style.', 'success', 'omh_enable_global_garment_style' );

				self::$response_status = 'success';
			} elseif( !$valid ) {
				self::add_message( 'There were problems ' . ( $enabled ? 'enabling' : 'disabling' ) . ' the garment style - ' . $invalid_msg, 'danger', 'omh_enable_global_garment_style' );
			} else {
				self::add_message( 'There was a problem updating that garment style.', 'danger', 'omh_enable_global_garment_style' );
			}
		} else {
			self::add_message( 'There was a problem ' . ( $enabled ? 'enabling' : 'disabling' ) . ' the garment style - object does not exist.', 'danger', 'omh_enable_global_garment_style' );
		}

		self::$refresh_tables = true;
		self::send_ajax_response();
	}

	public static function save_garment_color() {

		self::check_nonce( 'save-garment-color' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		self::$response_status = 'fail';

		if( isset( $form_data['ID'] ) && $garment_color = OMH()->garment_color_factory->read( $form_data['ID'] ) ) {

			$garment_color->set_color_code( $form_data['color_code'] );

			$validation = $garment_color->validate();
			if( empty( $validation ) ) {

				if( $garment_color->save() ) {

					self::$refresh_tables = true;
					self::$response_status = 'success';
					self::add_message( 'Successfully saved garment color.', 'success', 'omh_save_garment_color' );
				} else {

					self::$response_status = 'fail';
					self::add_message( 'There was a problem saving the garment color.', 'danger', 'omh_save_garment_color' );
				}
			} else {

				self::$response_status = 'fail';

				if( isset( $validation['required'] ) ) {
					self::add_message( 'The following fields are required: ' . implode( ', ', $validation['required'] ), 'danger', 'omh_save_garment_color__required' );
				}

				if( isset( $validation['unique'] ) ) {
					self::add_message( 'The following fields must be unique: ' . implode( ', ', $validation['unqiue'] ), 'danger', 'omh_save_garment_color__unique' );
				}
			}
		}

		self::send_ajax_response();
	}

	public static function save_garment_brand() {

		self::check_nonce( 'save-garment-brand' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		self::$response_status = 'fail';

		if( isset( $form_data['ID'] ) && $garment_brand = OMH()->garment_brand_factory->read( $form_data['ID'] ) ) {

			$garment_brand->set_brand_code( $form_data['brand_code'] );
			$garment_brand->set_brand_slug( $form_data['brand_slug'] );

			$validation = $garment_brand->validate();
			if( empty( $validation ) ) {

				if( $garment_brand->save() ) {

					self::$refresh_tables = true;
					self::$response_status = 'success';
					self::add_message( 'Successfully saved garment brand.', 'success', 'omh_save_garment_brand' );
				} else {

					self::$response_status = 'fail';
					self::add_message( 'There was a problem saving the garment brand.', 'danger', 'omh_save_garment_brand' );
				}
			} else {

				self::$response_status = 'fail';

				if( isset( $validation['required'] ) ) {
					self::add_message( 'The following fields are required: ' . implode( ', ', $validation['required'] ), 'danger', 'omh_save_garment_brand__required' );
				}

				if( isset( $validation['unique'] ) ) {
					self::add_message( 'The following fields must be unique: ' . implode( ', ', $validation['unqiue'] ), 'danger', 'omh_save_garment_brand__unique' );
				}
			}
		}

		self::send_ajax_response();
	}

	/********************************
	 *			 Users				*
	 ********************************/

	public static function add_user() {

		self::check_nonce( 'add-user' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		self::$response_status = 'fail';

		if( isset( $form_data['email'] ) && ( $user_email = $form_data['email'] ) ) {

			$first_name = $form_data['first_name'] ?? '';
			$last_name = $form_data['last_name'] ?? '';
			$role = $form_data['role'] ?? 'house_member';

			$user_data = array(
				'user_login'	=> $user_email,
				'user_email'	=> $user_email,
				'user_pass'		=> NULL,
				// DEV:house_member
				'role'			=> $role,
			);

			if( $first_name ) {
				$user_data['first_name'] = $first_name;
			}

			if( $last_name ) {
				$user_data['last_name'] = $last_name;
			}

			if( $first_name && $last_name ) {
				$user_data['display_name'] = "{$first_name} {$last_name}";
			}

			$user_id = wp_insert_user( $user_data );

			// Success!
			if( $user_id && !is_wp_error( $user_id ) ) {

				$user = get_user_by( 'id', $user_id );

				$user_activation_key = $user->generate_user_activation_key();

				if( isset( $form_data['chapter_id'] ) && ( $chapter_id = $form_data['chapter_id'] ) ) {
					$user->set_primary_chapter_id( $chapter_id );
				}

				// If send user activation is checked, do some stuff
				if( isset( $form_data['send_user_activation_email'] ) 
					&& maybe_str_to_bool( $form_data['send_user_activation_email'] ) 
				) {
					$user->send_activation_email();
				}
				
				self::add_message( "{$first_name} {$last_name} - {$user_email} successfully added.", 'success', 'omh_add_user' );

				self::$refresh_tables = true;
				self::$response_status = 'success';
			} else {
				self::add_message( 'There was an error creating the user: ' . $user_id->get_error_message(), 'danger', 'omh_add_user' );
			}
		} else {
			self::add_message( 'Email is required when creating a user', 'danger', 'omh_add_user' );
		}

		self::send_ajax_response();
	}

	public static function invite_house_members() {

		self::check_nonce( 'invite-house-members' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		self::$response_status = 'fail';

		$invited_by = $omh_ajax['obj_id'] ?? 0;
		$chapter_id = $form_data['chapter_id'] ?? 0;
		$email_list = $form_data['email_list'] ?? '';

		if( !$chapter_id ) {
			self::add_message( 'Chapter not found. Please contact an administrator.', 'danger', 'omh_invite_house_members' );
			self::send_ajax_response();
		}

		$email_array_list = array_filter( 
			array_unique( 
				array_map( 'trim', explode( PHP_EOL, trim( $email_list ) ) ) 
			) 
		);

		if( empty( $email_array_list ) ) {
			self::add_message( 'The Email list cannot be blank.', 'danger', 'omh_invite_house_members' );
			self::send_ajax_response();
		}

		$response_obj = array(
			'email_list'	=> array(
				'value'	=> $email_list
			),
			'chapter_id'	=> array(
				'value'	=> $chapter_id
			)
		);

		$results	= array(
			'Invited' 			=> array(),
			'Already Exists' 	=> array(),
			'Upgraded' 			=> array(),
			'Invalid' 			=> array(),
			'Error'				=> array()
		);

		foreach( $email_array_list as $email ) {

			$is_new_user = true;

			// Make sure the email is valid
			if( !is_email( $email ) ) {

				array_push( $results['Invalid'], $email );
				continue;
			}

			$user_data = array(
				'user_login'	=> $email,
				'user_email'	=> $email,
				'user_pass'		=> NULL,
				'role'			=> 'house_member',
			);

			// Check if the email already exists on the network
			if( $user_id = email_exists( $email ) ) {
				$is_new_user = false;
			// Or make a new user
			} elseif( !( $user_id = wp_insert_user( $user_data ) ) ) {

				// There was an error checking the account or making a new account
				array_push( $results['Error'], $email );
				continue;
			}

			$user = get_user_by( 'id', $user_id );
			// $user = new OMH_User($user);
			// $user = new OMH_User_House_Member( $user_id );

			if( $invited_by ) {
				$user->set_invited_by( $invited_by );
			}

		// 	// Check if the user is already a member of the current site
			if( $user->is_member_of_house( $chapter_id ) ) {
				// If the user isn't activated, send the activation email
				// if ($is_new_user){
				// 	if( !$user->is_activated() ) {
				// 		array_push( $results['Invited'], $email );
				// 		// $user->send_activation_email( $invited_by );
				// 		$user->send_activation_email();
				// 		continue;
				// 	}
				// }
				// Check if the user is already a House Member
				if( $user->has_role( 'house_member', true ) ) {
					array_push( $results['Already Exists'], $email );
					continue;
				// Upgrade their role to House Member
				} else {
					array_push( $results['Upgraded'], $email );
				}
			} elseif( $user->get_chapter_id() ) {
				array_push( $results['Invalid'], $email );
				self::add_message( "{$email} is a member of another house. Contact an administrator if you believe this is an error.", 'danger', 'omh_invite_house_members' );
				continue;
			// Invite the user to the site
			} else {
				array_push( $results['Invited'], $email );
			}

			// Don't do user activation if user is already registered
			if (!$is_new_user){
				$user->set_account_activated(true);
			}

			$user->make_house_member( $chapter_id, $is_new_user );
		}

		/*
		 * Setup Notices
		 */
		// Invited
		if( !empty( $results['Invited'] ) ) {

			$invited_count = count( $results['Invited'] );

			self::add_message( $invited_count . ' User' . ( $invited_count == 1 ? '': 's' ) . ' invited.', 'success', 'omh_invite_house_members_invited' );
		}
		// Already Exists
		if( !empty( $results['Already Exists'] ) ) {

			$already_exists_count = count( $results['Already Exists'] );

			self::add_message( $already_exists_count . ' User' . ( $already_exists_count == 1 ? '': 's' ) . ' already invited.', 'warning', 'omh_invite_house_members_aleady_exists' );
		}
		// Upgraded
		if( !empty( $results['Upgraded'] ) ) {

			$upgraded_count = count( $results['Upgraded'] );

			self::add_message( $upgraded_count . ' User' . ( $upgraded_count == 1 ? '': 's' ) . ' upgraded to House Member' . ( $upgraded_count == 1 ? '': 's' ) . ' :<ul><li>' . implode( '</li><li>', $results['Upgraded'] ) . '</li></ul>', 'success', 'omh_invite_house_members_upgraded' );
		}
		// Invalid
		if( !empty( $results['Invalid'] ) ) {

			$invalid_count = count( $results['Invalid'] );

			self::add_message( $invalid_count . ' Invalid email' . ( $invalid_count == 1 ? '': 's' ) . ':<ul><li>' . implode( '</li><li>', $results['Invalid'] ) . '</li></ul>', 'danger', 'omh_invite_house_members_invalid' );
		}
		// Error
		if( !empty( $results['Error'] ) ) {

			$error_count = count( $results['Error'] );

			self::add_message( $error_count . ' Error' . ( $error_count == 1 ? '': 's' ) . ' inviting users:<ul><li>' . implode( '</li><li>', $results['Error'] ) . '</li></ul>', 'danger', 'omh_invite_house_members_error' );
		}

		if( $invited_count > 0 ) {
			self::$refresh_tables = true;
		}

		/*
		 * Send email to Admin
		 */
		
		OMH_Emails::send_admin_house_member_list($results, $chapter_id);

		$response_obj['results'] = $results;

		self::$response_data = $response_obj;

		self::$response_status = 'success';

		self::send_ajax_response();
	}

	public static function house_admin_activation() {

		self::check_nonce( 'house-admin-activation' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		self::$response_status = 'fail';

		if( !isset( $form_data['agree_to_terms'] ) && $form_data['agree_to_terms'] ) {
			self::add_message( 'You must agree to the Chapter Agreement terms to continue.', 'danger', 'omh_house_admin_activation' );
		} else {

			$user_id = $form_data['user_id'] ?? 0;

			if( $form_data['user_id'] && $omh_user = get_user_by( 'id', $user_id ) ) {

				$new_password = $form_data['new_password'] ?? '';
				$confirm_password = $form_data['confirm_new_password'] ?? '';

				if( OMH()->is_strong_password( $new_password ) && ( 0 === strcmp( $new_password, $confirm_password ) ) ) {

					$omh_user->first_name = $form_data['first_name'] ?? '';
					$omh_user->last_name = $form_data['last_name'] ?? '';
					$omh_user->user_pass = $new_password;

					$updated = wp_update_user( $omh_user );

					if( $updated && !is_wp_error( $updated ) ) {

						$redirect = OMH()->dashboard_url();

						if( isset( $form_data['phone_number'] ) && $form_data['phone_number'] ) {
							$omh_user->update_meta( 'billing_phone', $form_data['phone_number'] );
						}

						$omh_user->activate_user();

						wp_destroy_current_session();
						wp_clear_auth_cookie();

						$credentials = array(
							'user_login'	=> $omh_user->user_login,
							'user_password'	=> $new_password,
							'remember'		=> true
						);

						// Log the user in!
						$omh_user->login( $credentials );

						wp_set_current_user( $omh_user->ID, $omh_user->user_login );
						wp_set_auth_cookie( $omh_user->ID, true );

						self::add_message( 'User activation successful.', 'success', 'omh_house_admin_activation' );

						self::$response_status = 'success';
						self::$response_redirect = $redirect;
					} else {
						self::add_message( $updated->get_error_message(), 'danger', 'omh_house_admin_activation' );
					}
				} else {
					self::add_message( OMH_Frontend::password_hint(), 'danger', 'omh_house_admin_activation' );
				}
			} else {
				self::add_message( 'User not found! Please contact <a href="mailto:admin@merch.house">admin@merch.house</a> for more information', 'danger', 'omh_house_admin_activation' );
			}
		}

		self::send_ajax_response();
	}

	public static function house_member_activation() {

		self::check_nonce( 'house-member-activation' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		self::$response_status = 'fail';

		$user_id = $form_data['user_id'] ?? 0;

		if( $form_data['user_id'] && $omh_user = get_user_by( 'id', $user_id ) ) {

			$new_password = $form_data['new_password'] ?? '';
			$confirm_password = $form_data['confirm_new_password'] ?? '';

			if( OMH()->is_strong_password( $new_password ) && ( 0 === strcmp( $new_password, $confirm_password ) ) ) {
				
				$omh_user->first_name = $form_data['first_name'] ?? '';
				$omh_user->last_name = $form_data['last_name'] ?? '';
				$omh_user->user_pass = $new_password;

				$updated = wp_update_user( $omh_user );

				if( $updated && !is_wp_error( $updated ) ) {

					$redirect = OMH()->dashboard_url( 'account-details' );

					$omh_user->activate_user();

					wp_destroy_current_session();
					wp_clear_auth_cookie();

					// Log the user in!
					$omh_user->login( $credentials );

					wp_set_current_user( $omh_user->ID, $omh_user->user_login );
					wp_set_auth_cookie( $omh_user->ID, true );

					self::add_message( 'User activation successful.', 'success', 'omh_house_member_activation' );

					self::$response_status = 'success';
					self::$response_redirect = $redirect;
				} else {
					self::add_message( $updated->get_error_message(), 'danger', 'omh_house_member_activation' );
				}
			} else {
				self::add_message( OMH_Frontend::password_hint(), 'danger', 'omh_house_member_activation' );
			}
		} else {
			self::add_message( 'User not found! Please contact <a href="mailto:admin@merch.house">admin@merch.house</a> for more information', 'danger', 'omh_house_member_activation' );
		}

		self::send_ajax_response();
	}

	public static function account_activation() {

		self::check_nonce( 'account-activation' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		self::$response_status = 'fail';

		if( !isset( $form_data['agree_to_terms'] ) && $form_data['agree_to_terms'] ) {
			self::add_message( 'You must agree to the Chapter Agreement terms to continue.', 'danger', 'omh_account_activation' );
		} else {

			$user_id = $form_data['user_id'] ?? 0;

			if( $form_data['user_id'] && $omh_user = get_user_by( 'id', $user_id ) ) {

				$new_password = $form_data['new_password'] ?? '';
				$confirm_password = $form_data['confirm_new_password'] ?? '';

				if( OMH()->is_strong_password( $new_password ) && ( 0 === strcmp( $new_password, $confirm_password ) ) ) {

					$omh_user->first_name = $form_data['first_name'] ?? '';
					$omh_user->last_name = $form_data['last_name'] ?? '';
					$omh_user->user_pass = $new_password;

					$updated = wp_update_user( $omh_user );

					if( $updated && !is_wp_error( $updated ) ) {

						$redirect = OMH()->dashboard_url();

						if( isset( $form_data['phone_number'] ) && $form_data['phone_number'] ) {
							$omh_user->update_meta( 'billing_phone', $form_data['phone_number'] );
						}

						$omh_user->delete_meta( 'house_admin_activation_key' );

						do_action( 'omh_admin_new_user_notification', $user_id );

						wp_destroy_current_session();
						wp_clear_auth_cookie();

						// Log the user in!
						$omh_user->login( $credentials );

						// wp_logout();
						wp_set_current_user( $omh_user->ID, $omh_user->user_login );
						wp_set_auth_cookie( $omh_user->ID, true );
						// do_action( 'wp_login', $omh_user->user_login, $omh_user );

						self::add_message( 'User activation successful.', 'success', 'omh_account_activation' );

						self::$response_status = 'success';
						self::$response_redirect = $redirect;
					} else {
						self::add_message( $updated->get_error_message(), 'danger', 'omh_account_activation' );
					}
				} else {
					self::add_message( OMH_Frontend::password_hint(), 'danger', 'omh_account_activation' );
				}
			} else {

				self::add_message( 'User not found! Please contact <a href="mailto:admin@merch.house">admin@merch.house</a> for more information', 'danger', 'omh_account_activation' );
			}
		}

		self::send_ajax_response();
	}

	public static function resend_user_activation() {

		self::check_nonce( 'resend-user-activation' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();
				
		self::$response_status = 'fail';

		if( $omh_ajax 
			&& isset( $omh_ajax['user_id'] )
		) {

			$user = new OMH_User( $omh_ajax['user_id'] );

			if( !$user->is_activated() ) {

				$user->send_activation_email();

				self::$response_status = 'success';
				self::add_message( "Resent activation email to {$user->get_email()}.", 'success', 'omh_resend_user_activation' );
			} else {

				self::add_message( "{$user->get_email()} is already activated.", 'danger', 'omh_resend_user_activation' );
			}
		} else {
			self::add_message( 'User not found, please contact an administrator for more information', 'danger', 'omh_resend_user_activation' );
		}

		self::send_ajax_response();
	}

	/**
	 * Set the user role of the received User
	 * 
	 * @return 	void
	 */
	public static function set_user_role() {

		self::check_nonce( 'manage-users' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$user_id = $omh_ajax['user_id'] ?? 0;
		$new_role = $omh_ajax['new_role'] ?? 'customer';

		if( !$user_id ) {
			self::add_message( 'User not found.', 'danger', 'omh_set_user_role' );
			self::send_ajax_response();
		}

		global $wp_roles;

		$user_data = array( 
			'ID' => $user_id, 
			'role' => $new_role 
		);

		$user = new OMH_User( $user_id );

		if( $user_id == wp_update_user( $user_data ) ) {

			// If the user is being upgraded to a house member, send the upgrade email
			// if( $user->has_role( 'customer', true, true ) && ( $new_role == 'house_member' ) ) {

			// }

			// If user is being upgraded to house admin and is not activated, send activation email.
			if( !$user->is_activated() && ($new_role == 'house_admin')){
				$user->send_activation_email();
			}

			$user_data['message'] = 'User updated successfully.';

			self::add_message( ( $user->get_full_name() ?: $user->user_email ) . '\'s role has been updated to ' . $wp_roles->roles[ $user_data['role'] ]['name'], 'success', 'omh_set_user_role' );

			self::$response_data = $user_data;
			self::$refresh_tables = true;
		}
		else {

			$user_data['message'] = 'User update failed.';

			self::add_message( 'There was an error updating user ID: ' . $user_data['ID'], 'danger', 'omh_set_user_role' );

			self::$response_status = 'fail';
			self::$response_data = $user_data;
		}

		self::send_ajax_response();
	}

	/**
	 * Save User Account Details
	 * * First Name
	 * * Last Name
	 * * Email Address
	 * 
	 * dev:improve Escape, validate, and sanitize
	 * 
	 * @return type
	 */
	public static function save_user_account_details() {

		self::check_nonce( 'save-user-account-details' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$user = get_user_by( 'ID', $omh_ajax['obj_id'] );

		$form_data = $omh_ajax['form_data'];

		if( !$user ) {

			self::$response_status = 'fail';
			self::$response_data = $omh_ajax;
			
			self::send_ajax_response( 
				'User does not exist.'		// Messages
			);
		}

		// $validation_fields = array(
		// 	'first_name'			=> 'type:string|required',
		// 	'last_name'				=> 'type:string|required',
		// 	'user_email'			=> 'type:email|required',
		// 	'current_password'		=> 'type:password|required',
		// 	'new_password'			=> 'type:password|required',
		// 	'confirm_new_password'	=> 'type:password|required'
		// );

		$response_obj = array(
			'first_name'		=> array(
				'value'		=> isset( $form_data['first_name'] ) ? $form_data['first_name'] : ''
			),
			'last_name'			=> array(
				'value'		=> isset( $form_data['last_name'] ) ? $form_data['last_name'] : ''
			),
			'full_name'			=> array(
				'value'		=> isset( $form_data['first_name'], $form_data['first_name'] ) ? $form_data['first_name'] . ' ' . $form_data['last_name'] : ''
			),
			'user_email'		=> array(
				'value'		=> isset( $form_data['user_email'] ) ? $form_data['user_email'] : ''
			),
			'user_initials'		=> array(
				'value'		=> isset( $form_data['first_name'], $form_data['first_name'] ) ? strtoupper( substr( $form_data['first_name'], 0, 1 ) . substr( $form_data['last_name'], 0, 1 ) ) : ''
			),
			'_omh_notifications_general' => array (
				'value'		=> maybe_str_to_bool(isset( $form_data['_omh_notifications_general'] ) ? $form_data['_omh_notifications_general'] : false),
			),
			'_omh_notifications_house_products' => array (
				'value'		=> maybe_str_to_bool(isset( $form_data['_omh_notifications_house_products'] ) ? $form_data['_omh_notifications_house_products'] : false),
			),
			'_omh_notifications_house_admin' => array (
				'value'		=> maybe_str_to_bool(isset( $form_data['_omh_notifications_house_admin'] ) ? $form_data['_omh_notifications_house_admin'] : false),
			),
		);

		$valid_form = true;

		if( !$form_data['first_name'] ) {
			$valid_form = false;
			$response_obj['first_name']['invalid_msg'] = 'First Name is a required field.';
		}

		if( !$form_data['last_name'] ) {

			$valid_form = false;
			$response_obj['last_name']['invalid_msg'] = 'Last Name is a required field.';
		}

		// dev:improve Save user information here
		if( $valid_form ) {
			// Update User Credentials
			$user_array = array(
				'ID'				=> $user->ID,
				'first_name'		=> $form_data['first_name'],
				'last_name'			=> $form_data['last_name'],
				'user_email'		=> $form_data['user_email']
			);

			// Update User Metadata
			$user_meta_array = array(
				'_omh_notifications_general' 		=> $form_data['_omh_notifications_general'],
				'_omh_notifications_house_products' => $form_data['_omh_notifications_house_products'],
				'_omh_notifications_house_admin' 	=> $form_data['_omh_notifications_house_admin'],
			);

			$updated = wp_update_user( $user_array );

			if (!is_wp_error($updated)) {
				foreach ($user_meta_array as $meta_key => $meta_val) {
					update_user_meta($user->ID, $meta_key, $meta_val);
				}
				self::add_message( 'Successfully updated user account details', 'success', 'omh_save_user_account_details' );
			} 
			else {
				self::$response_status = 'fail';
				self::add_message($updated->get_error_message(), 'danger', 'omh_save_user_account_details');	
			}
			
			self::$response_data = $response_obj;
			self::send_ajax_response();
	}
}


	/**
	 * Save User Password
	 * 
	 * @return type
	 */
	public static function save_user_password() {

		self::check_nonce( 'save-user-password' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		$user = get_user_by( 'ID', $omh_ajax['obj_id'] );
		if( !$user ) {

			self::$response_status = 'fail';
			self::$response_data = $omh_ajax;
			
			self::send_ajax_response( 
				'User does not exist.'		// Messages
			);
		}

		$response_obj = array(
			'current_password'		=> array(
				'value'		=> ''
			),
			'new_password'			=> array(
				'value'		=> ''
			),
			'confirm_new_password'	=> array(
				'value'		=> ''
			)
		);
		$valid_form = true;

		if( !$form_data['current_password'] || !wp_check_password( $form_data['current_password'], $user->data->user_pass, $user->ID ) ) {

			$valid_form = false;
			$response_obj['current_password']['invalid_msg'] = 'Password incorrect.';
		}

		if( !$form_data['new_password'] ) {

			$valid_form = false;
			$response_obj['new_password']['invalid_msg'] = 'You must provide a new password.';
		} elseif( !OMH()->is_strong_password( $form_data['new_password'] ) ) {

			$valid_form = false;
			$response_obj['new_password']['invalid_msg'] = OMH_Frontend::password_hint();
		}

		if( !$form_data['confirm_new_password'] || ( 0 !== strcmp( $form_data['new_password'], $form_data['confirm_new_password'] ) ) ) {

			$valid_form = false;
			$response_obj['confirm_new_password']['invalid_msg'] = 'New passwords do not match.';
		}

		// dev:improve Save user password here
		if( $valid_form ) {

			$user->user_pass = $form_data['new_password'];

			$updated = wp_update_user( $user );

			if( !is_wp_error( $updated ) ) {
				
				wp_set_current_user( $user->ID, $user->user_login );
				wp_set_auth_cookie( $user->ID, true );
				do_action( 'wp_login', $user->user_login, $user );

				$response_obj['form_nonce'] = wp_create_nonce( 'save-user-password' );

				//dev:improve //dev:error_handling
				self::add_message( 'Successfully updated your password', 'success', 'omh_save_user_account_details' );
			} else {

				self::$response_status = 'fail';

				//dev:improve //dev:error_handling
				self::add_message( $updated->get_error_message(), 'danger', 'omh_save_user_account_details' );
			}
		}
		else {

			self::$response_status = 'fail';

			self::add_message( 'Failed to save your password. Please fix form errors and try again.', 'danger', 'omh_save_user_account_details' );
		}

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	/**
	 * Save User Addresses
	 * 
	 * dev:improve Escape and sanitize
	 * 
	 * @return type
	 */
	public static function save_user_addresses() {

		self::check_nonce( 'save-user-addresses' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$form_data = $omh_ajax['form_data'];

		$user = get_user_by( 'ID', $omh_ajax['obj_id'] );
		if( !$user ) {

			self::$response_status = 'fail';
			self::$response_data = $omh_ajax;
			
			self::send_ajax_response( 
				'User does not exist.'		// Messages
			);
		}

		$valid_form = true;

		$response_obj = array(
			'billing_first_name'	=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['billing_first_name'] ) ? $form_data['billing_first_name'] : ''
			),
			'billing_last_name'		=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['billing_last_name'] ) ? $form_data['billing_last_name'] : ''
			),
			'billing_country'		=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['billing_country'] ) ? $form_data['billing_country'] : ''
			),
			'billing_address_1'		=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['billing_address_1'] ) ? $form_data['billing_address_1'] : ''
			),
			'billing_address_2'		=> array(
				'value'		=> isset( $form_data['billing_address_2'] ) ? $form_data['billing_address_2'] : ''
			),
			'billing_city'			=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['billing_city'] ) ? $form_data['billing_city'] : ''
			),
			'billing_state'			=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['billing_state'] ) ? $form_data['billing_state'] : ''
			),
			'billing_postcode'		=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['billing_postcode'] ) ? $form_data['billing_postcode'] : ''
			),
			'billing_phone'			=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['billing_phone'] ) ? $form_data['billing_phone'] : ''
			),
			'billing_email'			=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['billing_email'] ) ? $form_data['billing_email'] : ''
			),
			'ship_to_same_address'	=> array(
				'value'		=> isset( $form_data['ship_to_same_address'] ) ? $form_data['ship_to_same_address'] : false
			),
			'shipping_first_name'	=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['shipping_first_name'] ) ? $form_data['shipping_first_name'] : ''
			),
			'shipping_last_name'	=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['shipping_last_name'] ) ? $form_data['shipping_last_name'] : ''
			),
			'shipping_country'		=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['shipping_country'] ) ? $form_data['shipping_country'] : ''
			),
			'shipping_address_1'	=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['shipping_address_1'] ) ? $form_data['shipping_address_1'] : ''
			),
			'shipping_address_2'	=> array(
				'value'		=> isset( $form_data['shipping_address_2'] ) ? $form_data['shipping_address_2'] : ''
			),
			'shipping_city'			=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['shipping_city'] ) ? $form_data['shipping_city'] : ''
			),
			'shipping_state'		=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['shipping_state'] ) ? $form_data['shipping_state'] : ''
			),
			'shipping_postcode'		=> array(
				'required'	=> true,
				'value'		=> isset( $form_data['shipping_postcode'] ) ? $form_data['shipping_postcode'] : ''
			),
		);

		// Fix for this jumbled mess, unfinished
		foreach( $response_obj as $form_field => $form_field_value ) {

			$field_label = ucwords( str_replace( '_', ' ', $form_field ) );

			$required = ( isset( $form_field_value['required'] ) && $form_field_value['required'] ) ? true : false;

			if( $required && !$form_data[ $form_field ] ) {

				if( 0 === strpos( $form_field, 'billing' ) ) {

						$valid_form = false;
						$response_obj[ $form_field ]['invalid_msg'] = "{$field_label} is a required field.";
				} else if( 
					0 === strpos( $form_field, 'shipping' ) 
					&& ( true !== filter_var( $form_data['ship_to_same_address'], FILTER_VALIDATE_BOOLEAN ) )
				) {

					$valid_form = false;
					$response_obj[ $form_field ]['invalid_msg'] = "{$field_label} is a required field.";
				}
			}
		}

		if( $valid_form ) {

			foreach( $response_obj as $form_field => $form_field_value ) {

				if( 
					( 0 === strpos( $form_field, 'shipping' ) )
					&& ( true === filter_var( $form_data['ship_to_same_address'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE ) )
				) {

					update_user_meta( $user->ID, $form_field, $response_obj[ 'billing' . str_replace( 'shipping', '', $form_field ) ]['value'] );
				} else {
				
					update_user_meta( $user->ID, $form_field, $form_field_value['value'] );
				}
			}
			
			self::add_message( 'Successfully updated your addresses', 'success', 'omh_save_user_addresses' );
		}

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	public static function set_user_chapter() {

		self::check_nonce( 'manage-users' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$user = new OMH_User( $omh_ajax['user_id'] );

		if( isset( $omh_ajax['chapter'], $omh_ajax['chapter']['id'] ) ) {
			$user->set_primary_chapter_id( $omh_ajax['chapter']['id'] );

			self::add_message( ( $user->get_full_name() ?: $user->user_email ) . '\'s chapter has been updated to ' . $omh_ajax['chapter']['text'], 'success', 'omh_set_user_chapter' );
		} else {

			self::add_message( 'Missing chapter information', 'danger', 'omh_set_user_chapter' );

			self::$response_status = 'fail';
		}

		// if( $omh_ajax['user_id'] == wp_update_user( $user_data ) ) {

		// 	$user_data['message'] = 'User updated successfully.';

		// 	self::add_message( ( $user->get_full_name() ?: $user->user_email ) . '\'s role has been updated to ' . $wp_roles->roles[ $user_data['role'] ]['name'], 'success', 'omh_set_user_role' );

		// 	self::$response_data = $user_data;
		// 	self::send_ajax_response();
		// }
		// else {

		// 	$user_data['message'] = 'User update failed.';

		// 	self::add_message( 'There was an error updating user ID: ' . $user_data['ID'], 'danger', 'omh_set_user_role' );

		// 	self::$response_status = 'fail';
		// 	self::$response_data = $user_data;
		// 	self::send_ajax_response();
		// }

		self::send_ajax_response();
	}

	public static function remove_house_user() {

		self::check_nonce( 'remove-house-user' );
		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();
		$form_data = $omh_ajax['form_data'];

		$user_id = $form_data['user_id'] ?? 0;
		$new_role = 'customer';

		if( !$user_id ) {
			self::add_message( 'User not found.', 'danger', 'omh_set_user_role' );
			self::send_ajax_response();
		}

		$user = get_user_by( 'ID', $user_id );
		if( !$user ) {
			self::$response_status = 'fail';
			self::$response_data = $omh_ajax;
			
			self::send_ajax_response('User does not exist.');
		}

		// If user is not activated then delete
		if (!$user->is_activated()){
			wp_delete_user($user_id);
			$user_data['message'] = 'User updated successfully.';
			self::add_message( ( $user->get_full_name() ?: $user->user_email ) . ' has been removed from house.', 'success', 'omh_set_user_role' );
		}
		else {
			$user_data = array( 
				'ID' => $user_id, 
				'role' => $new_role,
			);
	
			$user = new OMH_User( $user_id );
	
			if( $user_id == wp_update_user( $user_data ) ) {
	
				// Reset User Chapter ID and remove activation keys
				$user->set_chapter_id(0);
				$user_data['message'] = 'User updated successfully.';
				self::add_message( ( $user->get_full_name() ?: $user->user_email ) . ' has been removed from house.', 'success', 'omh_set_user_role' );
				self::$response_data = $user_data;
				self::$refresh_tables = true;
			}
			else {
				$user_data['message'] = 'User update failed.';
				self::add_message( 'There was an error updating user ID: ' . $user_data['ID'], 'danger', 'omh_set_user_role' );
				self::$response_status = 'fail';
				self::$response_data = $user_data;
			}
		}
		self::send_ajax_response();
	}

	/********************************
	 *			 Products			*
	 ********************************/

	/**
	 * Create Product
	 * dev:improve Split this out into functions that can be called, this function is way too big!!!
	 * 
	 * @return 
	 */
	public static function create_product() {

		self::check_nonce( 'create-product' );

		$form_data = $_POST;
		$form_files = $_FILES ?? array();

		$response_obj = array(
			'product_name' 				=> array(
				'value'	=> $form_data['product_name'] ?? ''
			),
			'file_upload_1' 			=> array(
				'value'	=> ''
			),
			'file_upload_2' 			=> array(
				'value'	=> ''
			),
			'file_upload_3' 			=> array(
				'value'	=> ''
			),
			'file_upload_4' 			=> array(
				'value'	=> ''
			),
			'sales_type'				=> array(
				'value'	=> $form_data['sales_type'] ?? ''
			),
			// 'product_type'				=> array(
			// 	'value'	=> $form_data['product_type'] ?? ''
			// ),
			'deliver_date'				=> array(
				'value'	=> $form_data['deliver_date'] ?? ''
			),
			'comments' 					=> array(
				'value'	=> $form_data['comments'] ?? ''
			),

			/*
			 * Hidden Fields
			 */
			'design_id' 				=> array(
				'value'	=> $form_data['design_id'] ?? ''
			),
			'site_name' 				=> array(
				'value'	=> $form_data['site_name'] ?? ''
			),
			'site_url' 					=> array(
				'value'	=> $form_data['site_url'] ?? ''
			),
			'created_by' 				=> array(
				'value'	=> $form_data['created_by'] ?? ''
			),
			'product_id'				=> array(
				'value'	=> $form_data['product_id'] ?? ''
			),
			'manufacturer'				=> array(
				'value'	=> $form_data['manufacturer'] ?? ''
			),
			'manufacturer_sku'			=> array(
				'value'	=> $form_data['manufacturer_sku'] ?? ''
			),
			'sku'						=> array(
				'value'	=> $form_data['sku'] ?? ''
			),
			'name'						=> array(
				'value'	=> $form_data['name'] ?? ''
			),
			'slug'						=> array(
				'value'	=> $form_data['slug'] ?? ''
			),
			'long_description'			=> array(
				'value'	=> $form_data['long_description'] ?? ''
			),
			'product_style_id'			=> array(
				'value'	=> $form_data['product_style_id'] ?? ''
			),
			'color'						=> array(
				'value'	=> $form_data['color'] ?? ''
			),
			'sizes'						=> array(
				'value'	=> $form_data['sizes'] ?? ''
			),
			'base_cost'					=> array(
				'value'	=> $form_data['base_cost'] ?? ''
			),
			'image_url'					=> array(
				'value'	=> $form_data['image_url'] ?? ''
			),
			'selected_product'			=> array(
				'value'	=> isset( $form_data['selected_product'] ) ? wp_unslash( $form_data['selected_product'] ) : ''
			),
			'selected_product_style'	=> array(
				'value'	=> isset( $form_data['selected_product_style'] ) ? wp_unslash( $form_data['selected_product_style'] ) : ''
			),
		);

		$design_id 				= $response_obj['design_id']['value'];
		$product_style_data 	= json_decode( wp_unslash( $response_obj['selected_product_style']['value'] ), true );
		$product_sizes 			= isset( $product_style_data['sizes'] ) ? $product_style_data['sizes'] : '';
		$mockup_image_url 		= $response_obj['image_url']['value'];

		/*
		 * Create the garment
		 */

		$product_args = array(
			'product_name'		=> $response_obj['product_name']['value'],
			'post_author'		=> get_current_user_id(),
			'product_image'		=> $mockup_image_url,
			'attributes'		=> array(
				'sizes'				=> $product_sizes,
				// Array of Array of Product Style ID and Product ID
				'garments'		=> array(
					array(
						'product_style_id'		=> $response_obj['product_style_id'],
						'product_id'			=> $response_obj['product_id']
					)
				)	
			),
			'meta'				=> array(
				'_design_id'						=> $design_id,
				'_mh_inksoft_product_data'			=> $response_obj['selected_product']['value'],
				'_mh_inksoft_product_style_data'	=> $response_obj['selected_product_style']['value'],
				'_mh_sales_type'					=> $response_obj['sales_type']['value'],
				// '_mh_product_type'					=> $response_obj['product_type']['value'],
				'_mh_deliver_date'					=> $response_obj['deliver_date']['value'],
				'_mh_customer_comments'				=> $response_obj['comments']['value'],
				'_mh_created_by'					=> $response_obj['created_by']['value'],
			)
		);
		$product = create_omh_product( $product_args );

		// Handle file upload
		if( !empty( $form_files ) ) {

			foreach( $form_files as $file_index => $file_data ) {

				if( !$file_data['size'] ) {
					continue;
				}

				$file_name = $product->get_id() . '-' . sanitize_file_name( $file_data['name'] );

				// Is this file even part of our form?
				if( 
					!in_array( $file_index, array_keys( $response_obj ) ) 
					|| !isset( $file_data['name'], $file_data['tmp_name'] )
					|| empty( $file_data['name'] ) 
					|| empty( $file_data['tmp_name'] )
				) {

					$response_obj[ $file_index ]['invalid_msg'] = 'This image does not exist in the form!';

					continue;
				}

				$field_label = ucwords( str_replace( '_', ' ', $file_index ) );

				$exif_allowed_types = array( IMAGETYPE_JPEG, IMAGETYPE_PSD, IMAGETYPE_GIF, IMAGETYPE_PNG, IMAGETYPE_TIFF_II, IMAGETYPE_TIFF_MM );
				$detected_exif_type = exif_imagetype( $file_data['tmp_name'] );

				$mime_allowed_types = array( 
					// JPG or JPEG
					'application/jpg', 
					'image/jpg', 
					'application/jpeg', 
					'image/jpeg', 

					// PDF
					'application/pdf', 

					// AI
					'application/illustrator', 
					'application/postscript', 

					// PSD
					'application/photoshop',
					'image/vnd.adobe.photoshop', 
					'image/x-psd',

					// PNG
					'image/png',

					// SVG
					'image/svg+xml',

					// TIFF
					'image/tiff',

					// GIF
					'image/gif',
				);
				$detected_mime_type = mime_content_type( $file_data['tmp_name'] );

				// Make sure it's a type we allow ( or a PDF @see is_pdf_file() )
				if( 
					in_array( $detected_exif_type, $exif_allowed_types ) 
					|| in_array( $detected_mime_type, $mime_allowed_types )
					|| is_pdf_file( $file_data['tmp_name'] )
				) {

					// Upload the file to wp-content/uploads/sites/{site_id}/omh/{design_id}/
					$file_dir = WP_CONTENT_DIR . '/uploads/chapters/';
					move_uploaded_file( $file_data['tmp_name'], $file_dir . $file_name );

					// dev:todo
					$file_url = content_url() . '/uploads/chapters/' . $file_name;

					$response_obj[ $file_index ]['value'] = $file_url;
				} else {

					$response_obj[ $file_index ]['invalid_msg'] = "{$field_label} must be of type: .jpg .jpeg .psd .ai .pdf .png .gif .tiff";
				}
			}
		}

		// Save uploads to the Product
		$product_media_uploads = array(
			'media_1'	=> $response_obj['file_upload_1']['value'],
			'media_2'	=> $response_obj['file_upload_2']['value'],
			'media_3'	=> $response_obj['file_upload_3']['value'],
			'media_4'	=> $response_obj['file_upload_4']['value']
		);
		update_post_meta( $product->get_id(), '_mh_customer_uploads', $product_media_uploads );

		switch( $response_obj['sales_type']['value'] ) {
			case 'retail':
				self::add_message( 'Retail Product Created Successfully', 'success', 'omh_create_product' );
				$redirect = OMH()->dashboard_url( 'retail-products' ) . '?created=' . $product->get_id();
				break;
			case 'bulk':
				self::add_message( 'Bulk Design Created Successfully', 'success', 'omh_create_product' );
				$redirect = OMH()->dashboard_url( 'bulk-orders' ) . '?created=' . $product->get_id();
				break;
			case 'campaign':
				self::add_message( 'Campaign Product Created Successfully', 'success', 'omh_create_product' );
				$redirect = OMH()->dashboard_url( 'campaign-products' ) . '?created=' . $product->get_id();
				break;
		}

		self::$response_redirect = $redirect;

		do_action( 'omh_send_design_created', $product->get_id() );

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	/**
	 * Description
	 * @return type
	 */
	public static function delete_product() {

		self::check_nonce( 'delete-product' );

		$omh_ajax = $_POST['omh_ajax'] ?? array();
		$form_data = $omh_ajax['form_data'] ?? array();
		$product_id = $form_data['ID'] ?? 0;

		$response_data = array(
			'product_id'	=> $product_id,
			'success'		=> true
		);

		if( $product = wc_get_product( $product_id ) ) {

			$deleted = $product->delete();

			if( $deleted ) {

				self::$refresh_tables = true;
				self::add_message( "Successfully deleted Product ID: {$product_id} - {$product->get_title()}", 'success', 'omh_delete_product' );
			} else {

				$response_data['success'] = false;
				self::add_message( "There was a problem deleting Product ID: {$product_id} - {$product->get_title()}", 'danger', 'omh_delete_product' );
			}
		} else {

			$response_data['success'] = false;
			self::add_message( "Product ID: {$product_id} does not exist.", 'danger', 'omh_delete_product' );
		}

		self::$response_data = $response_data;

		self::send_ajax_response();
	}

	public static function convert_product() {

		self::check_nonce( 'convert-product' );

		$omh_ajax = $_POST['omh_ajax'] ?? array();
		$form_data = $omh_ajax['form_data'] ?? array();
		$product_id = $form_data['ID'];
		$sales_type = $form_data['sales_type'];

		if( $product = wc_get_product( $product_id ) ) {

			switch( $sales_type ) {
				case 'retail':
					$converted = $product->convert_to_retail();
					break;
				case 'bulk':
					$converted = $product->convert_to_bulk();
					break;
				default:
					$converted = false;
					break;
			}

			if( $converted ) {

				$product->save();

				self::$refresh_tables = true;
				self::add_message( "Product successfully converted to {$sales_type}.", 'success', 'omh_convert_product' );
			} else {

				self::$response_status = 'fail';
				self::add_message( "Product ID: {$product_id} cannot be converted to {$sales_type}", 'danger', 'omh_convert_product' );
			 }
		} else {

			self::$response_status = 'fail';
			self::add_message( "Product ID: {$product_id} does not exist.", 'danger', 'omh_convert_product' );
		}

		self::send_ajax_response();
	}

	public static function product_convert_to_bulk() {

		self::check_nonce( 'product-convert-to-bulk' );

		$omh_ajax = $_POST['omh_ajax'] ?? array();
		$form_data = $omh_ajax['form_data'] ?? array();
		$product_id = $form_data['ID'];
		$product_status = $form_data['product_status'] ?? 'design_in_review';

		$response_data = array(
			'ID'	=> array(
				'value'	=> $product_id,
			),
			'product_status'	=> array(
				'value'	=> $product_status
			)
		);

		if( $product = wc_get_product( $product_id ) ) {

			self::$refresh_tables = true;

			$convert_success = $product->convert_to_bulk();
			
			if ( $convert_success ) {

				$product = wc_get_product( $product );

				$product->set_product_status( $product_status );
				
				// if convert is sucessfull prepare a message for the user
				self::add_message( "Successfully converted Product ID: {$product_id} - {$product->get_title()} to Bulk", 'success', 'omh_convert_to_bulk' );
				self::$refresh_tables = $convert_success;
			} else {
				// else prepare an error message for the user
				self::add_message( "There was a problem converting Product ID: {$product_id} - {$product->get_title()} to Bulk", 'danger', 'omh_convert_to_bulk' );
				self::$response_status = 'fail';
			}
		} else {
			self::add_message( "Product ID: {$product_id} does not exist.", 'danger', 'omh_convert_to_bulk' );
			self::$response_status = 'fail';
		}

		self::$response_data = $response_data;
		self::send_ajax_response();
	}

	public static function product_convert_to_retail() {

		self::check_nonce( 'product-convert-to-retail' );

		$omh_ajax = $_POST['omh_ajax'] ?? array();
		$form_data = $omh_ajax['form_data'] ?? array();
		$product_id = $form_data['ID'];
		$product_status = $form_data['product_status'] ?? 'design_in_review';
		
		$response_data = array(
			'ID'	=> array(
				'value'	=> $product_id,
			),
			'product_status'	=> array(
				'value'	=> $product_status
			)
		);
		
		if( $product = wc_get_product( $product_id ) ) {

			self::$refresh_tables = true;

			$convert_success = $product->convert_to_retail();

			if ( $convert_success ) {

				$product = wc_get_product( $product );

				$product->set_product_status( $product_status );

				// if convert is sucessfull prepare a message for the user
				self::add_message( "Successfully converted Product ID: {$product_id} - {$product->get_title()} to Retail", 'success', 'omh_convert_to_retail' );
				self::$refresh_tables = $convert_success;
			} else {
				// else prepare an error message for the user
				self::add_message( "There was a problem converting Product ID: {$product_id} - {$product->get_title()} to Retail", 'danger', 'omh_convert_to_retail' );
				self::$response_status = 'fail';
			}
		} else {
			self::add_message( "Product ID: {$product_id} does not exist.", 'danger', 'omh_convert_to_bulk' );
			self::$response_status = 'fail';
		}

		self::$response_data = $response_data;
		self::send_ajax_response();
	}

	public static function product_convert_to_campaign() {

		self::check_nonce( 'product-convert-to-campaign' );

		$omh_ajax = $_POST['omh_ajax'] ?? array();
		$form_data = $omh_ajax['form_data'] ?? array();
		$product_id = $form_data['ID'];
		$product_status = $form_data['product_status'] ?? 'design_in_review';
		$campaign_end_date = $form_data['campaign_end_date'] ?? '';
		$campaign_target_sales = $form_data['campaign_target_sales'] ?? '';
		
		$response_data = array(
			'ID'	=> array(
				'value'	=> $product_id,
			),
			'product_status'	=> array(
				'value'	=> $product_status
			),
			'campaign_end_date'	=> array(
				'value'	=> $product_status
			),
			'campaign_target_sales'	=> array(
				'value'	=> $product_status
			),
		);
		
		if( $product = wc_get_product( $product_id ) ) {

			self::$refresh_tables = true;

			$convert_success = $product->convert_to_campaign();

			if ( $convert_success ) {

				$product = wc_get_product( $product );

				$product->set_product_status( $product_status );

				$product->set_campaign_end_date( $campaign_end_date );
				$product->set_campaign_target_sales( $campaign_target_sales );
				$product->save();

				// if convert is sucessfull prepare a message for the user
				self::add_message( "Successfully converted Product ID: {$product_id} - {$product->get_title()} to Retail", 'success', 'omh_convert_to_retail' );
				self::$refresh_tables = $convert_success;
			} else {
				// else prepare an error message for the user
				self::add_message( "There was a problem converting Product ID: {$product_id} - {$product->get_title()} to Retail", 'danger', 'omh_convert_to_retail' );
				self::$response_status = 'fail';
			}
		} else {
			self::add_message( "Product ID: {$product_id} does not exist.", 'danger', 'omh_convert_to_bulk' );
			self::$response_status = 'fail';
		}

		self::$response_data = $response_data;
		self::send_ajax_response();
	}

	public static function save_dashboard_edit_bulk_details() {
		// wordpress token authentication
		self::check_nonce( 'save-dashboard-edit-bulk-details' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();
		$form_data = $omh_ajax['form_data'];

		$response_obj = array();
		$bulk_order = null;

		foreach( $form_data as $form_key => $form_value ) {

			$response_obj[ $form_key ] = array(
				'value'		=> $form_value
			);
		}

		// If there's no product id and no order id, fail out
		if( ( empty( $omh_ajax['obj_id'] ) && empty( $form_data['bulk_order_id'] ) ) 
			|| ( !$omh_ajax['obj_id'] && !$form_data['bulk_order_id'] )
		) {
			self::$response_status = 'fail';
			self::add_message( 'Something went wrong in saving', 'danger', 'omh_save_dashboard_edit_bulk_details' );
		} else {

			$bulk_product = wc_get_product( $omh_ajax['obj_id'] );
			$bulk_order = $bulk_product->get_active_bulk_order_id() ? wc_get_order( $bulk_product->get_active_bulk_order_id() ) : null;
			
			// Check if bulk order already exists
			if( $bulk_order 
				|| ( !empty( $form_data['bulk_order_id'] ) 
				&& $bulk_order = wc_get_order( $form_data['bulk_order_id'] ) )
			) {
				
				$bulk_order_quantities = array();

				foreach( $form_data as $form_data_key => $form_data_value ) {

					if( strpos( $form_data_key, 'qty_' ) === 0 ) {
						$bulk_order_quantities[ $form_data_key ] = $form_data_value;
					}
				}

				if( !empty( $bulk_order_quantities ) ) {
					$bulk_order->set_bulk_order_quantities( $bulk_order_quantities );
				}
			} else {
				$bulk_order = wc_get_order( OMH_Order::create_bulk_order( $omh_ajax['obj_id'], $form_data ) );

				$response_obj['bulk_order_id'] = array(
					'value'		=> $bulk_order->get_id()
				);
			}
			
			if ( $bulk_order ) {
				self::add_message( 'Bulk Order have been saved successfully', 'success', 'omh_save_dashboard_edit_bulk_details' );
			} else {
				self::add_message( 'Something went wrong in saving', 'danger', 'omh_save_dashboard_edit_bulk_details' );
			}
		}
		
		self::$response_data = $response_obj;
		self::send_ajax_response();
	}

	public static function checkout_dashboard_edit_bulk_details() {
		
		self::check_nonce( 'save-dashboard-edit-bulk-details' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();
		$form_data = $omh_ajax['form_data'];

		$response_obj = array();
		$bulk_order = null;

		foreach( $form_data as $form_key => $form_value ) {

			$response_obj[ $form_key ] = array(
				'value'		=> $form_value
			);
		}

		// If there's no product id and no order id, fail out
		if( ( empty( $omh_ajax['obj_id'] ) && empty( $form_data['bulk_order_id'] ) ) 
			|| ( !$omh_ajax['obj_id'] && !$form_data['bulk_order_id'] )
		) {
			self::$response_status = 'fail';
			self::add_message( 'Something went wrong in saving', 'danger', 'omh_checkout_dashboard_edit_bulk_details' );
		} else {

			$user = OMH_User::get_user( get_current_user_id() );
			$bulk_product = wc_get_product( $omh_ajax['obj_id'] );
			$bulk_order = $bulk_product->get_active_bulk_order_id() ? wc_get_order( $bulk_product->get_active_bulk_order_id() ) : null;
			
			// Check if bulk order already exists
			if( $bulk_order 
				|| ( !empty( $form_data['bulk_order_id'] ) 
				&& $bulk_order = wc_get_order( $form_data['bulk_order_id'] ) )
			) {

				$product_chapter = $bulk_product->get_chapter();
				
				$bulk_order_quantities = array();

				foreach( $form_data as $form_data_key => $form_data_value ) {

					if( strpos( $form_data_key, 'qty_' ) === 0 ) {
						$bulk_order_quantities[ $form_data_key ] = $form_data_value;
					}
				}

				if( !empty( $bulk_order_quantities ) ) {
					$bulk_order->set_bulk_order_quantities( $bulk_order_quantities );
				}

				if( $user->is_house_admin( false ) && $user->is_member_of_house( $product_chapter->get_id() ) ) {
					$bulk_order->set_customer_id( $user->ID );
					$bulk_order->save();
				}
			} else {
				$bulk_order = wc_get_order( OMH_Order::create_bulk_order( $omh_ajax['obj_id'], $form_data ) );

				$response_obj['bulk_order_id'] = array(
					'value'		=> $bulk_order->get_id()
				);
			}
			
			if ( $bulk_order ) {
				self::add_message( 'Bulk Order have been saved successfully', 'success', 'omh_checkout_dashboard_edit_bulk_details' );
				self::$response_redirect = $bulk_order->get_checkout_payment_url();
			} else {
				self::add_message( 'Something went wrong in saving', 'danger', 'omh_checkout_dashboard_edit_bulk_details' );
			}
		}
		
		self::$response_data = $response_obj;
		self::send_ajax_response();
	}

	/**
	 * dev:improve If a key isn't set, should we remove/nullify data for that key?
	 * 
	 * @return type
	 */
	public static function save_dashboard_product_details() {

		self::check_nonce( 'save-dashboard-product-details' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();
		$form_data = $omh_ajax['form_data'];

		$response_obj = array();

		if( !isset( $omh_ajax['obj_id'] ) 
			|| !$omh_ajax['obj_id']
			|| $omh_product = wc_get_product( $omh_ajax['obj_id'] ) 
		) {

			$response_obj = array(
				'product'	=> array(
					'value'	=> $omh_product->get_id()
				),
			);

			/*
			 * Product Name
			 */
			if( isset( $form_data['product_name'] ) ) {

				$product_name = $form_data['product_name'];

				$response_obj['product_name'] = array(
					'value'	=> $product_name
				);

				$omh_product->set_name( $product_name );
			}

			/*
			 * Product Description
			 */
			if( isset( $form_data['product_description'] ) ) {

				$product_description = $form_data['product_description'];

				$response_obj['product_description'] = array(
					'value'	=> $product_description
				);

				$omh_product->set_description( $product_description );
				$omh_product->set_short_description( $product_description );
			}

			/**
			 * Product Status
			 * New feature to replace product visibility and availability
			 */

			 if (isset( $form_data['product_status'] )) {

				$product_status = $form_data['product_status'];
				$response_obj['product_status'] = array(
					'value' => $product_status
				);

				$omh_product->set_product_status( $product_status );
			 }

			/*
			 * Product Availability
			 * TODO: Decouple this feature
			 */

			// @tag visibility
			if( isset( $form_data['product_store_visiblity'] ) ) {

				$product_store_visiblity = $form_data['product_store_visiblity'];

				$response_obj['product_store_visiblity'] = array(
					'value'	=> $product_store_visiblity
				);

				$omh_product->set_store_visibility( $product_store_visiblity );
			}

			// @tag purchasability
			if( isset( $form_data['product_store_purchasability'] ) ) {

				$product_store_purchasability = $form_data['product_store_purchasability'];

				$response_obj['product_store_purchasability'] = array(
					'value'	=> $product_store_purchasability
				);

				$omh_product->set_store_purchasability( $product_store_purchasability );
			}
			
			/**
			 * Product Categories
			 * 
			 * dev:todo The frontend needs to be able to handle the array for multiple select
			 */
			if( isset( $form_data['product_categories'] ) ) {

				$omh_product->set_category_ids( $form_data['product_categories'] );

				$response_obj['product_categories'] = array(
					'value'	=> $omh_product->get_category_ids()
				);
			}

			/**
			 * Campaign End Date
			 */
			if( isset( $form_data['campaign_end_date'] ) ) {

				$omh_product->set_campaign_end_date( $form_data['campaign_end_date'] );

				$response_obj['campaign_end_date'] = array(
					'value'	=> $omh_product->get_campaign_end_date()
				);
			}

			/**
			 * Campaign Target Sales
			 */
			if( isset( $form_data['campaign_target_sales'] ) ) {

				$omh_product->set_campaign_target_sales( $form_data['campaign_target_sales'] );

				$response_obj['campaign_target_sales'] = array(
					'value'	=> $omh_product->get_campaign_target_sales()
				);
			}

			/*
			 * Product Price Tiers
			 */
			if( isset( $form_data['product_price_tiers'] ) ) {

				$product_price_tiers = $form_data['product_price_tiers'];

				$response_obj['product_price_tiers'] = array(
					'value'	=> $product_price_tiers
				);

				$omh_product->set_product_price_tiers( $product_price_tiers );
			}

			/*
			 * Earnings
			 */
			if( isset( $form_data['earnings_percentage'] ) ) {

				$earnings_percentage = $form_data['earnings_percentage'];

				$response_obj['earnings_percentage'] = array(
					'value'	=> $earnings_percentage
				);

				$omh_product->set_earnings_percentage( $earnings_percentage );
			}

			/*
			 * Variations
			 */
			// dev:todo Variations
			if( isset( $form_data['product_garments'] ) ) {

				$product_garments = $form_data['product_garments'];

				$updated_product_garments = $omh_product->set_garments_meta( $product_garments );

				$response_obj['product_garments'] = array(
					'value'	=> $omh_product->get_frontend_product_garments()
				);
			}

			OMH_Product_Variable::sync( $omh_product );
			$omh_product->save();
			OMH()->clear_page_cache( $omh_product->get_id() );

			self::add_message( 'Product saved successfully', 'success', 'omh_save_dashboard_product_details' );
		} else {

			self::add_message( 'Product doesn\'t exist.', 'danger', 'omh_save_dashboard_product_details' );
		}

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	public static function generate_product_sales_csv() {

		self::check_nonce( 'generate-product-sales-csv' );

		$product_id = $_POST['product_id'] ?? null;

		if( $product_id ) {

			$csv_headers = array(
				'Line Item ID',
				'Order ID',
				'Order Paid Date',
				'Variation ID',
				'Variation SKU',
				'Variation Size',
				'Customer ID',
				'Customer/Guest email',
				'Customer/Guest name',
				'Line Item Qty',
				'Line Item Price',
				'Line Item Total',
			);

			$csv_file_name = get_current_blog_id() . '-' . $product_id . '-sales-data.csv';

			header( 'Content-Disposition: attachment;' );

			$output = @fopen( OMH_CONTENT_DIR . $csv_file_name, 'w' );

			ob_start();

			fputcsv( $output, $csv_headers );

			foreach( omh_get_order_items_by_product_id( $product_id ) as $product_order_item ) {

				$product_order_item_order = $product_order_item->get_order();
				$customer = get_user_by( 'ID', $product_order_item_order->get_customer_id() );
				$customer_email = $customer ? $customer->user_email : $product_order_item_order->get_billing_email();
				$customer_name = $customer ? $customer->first_name . ' ' . $customer->last_name : $product_order_item_order->get_formatted_billing_full_name();
				$line_item_price = $product_order_item->get_subtotal() && $product_order_item->get_quantity() ? $product_order_item->get_subtotal() / $product_order_item->get_quantity() : $product_order_item->get_subtotal();

				$product_order_item_variation = wc_get_product( $product_order_item->get_variation_id() );

				$csv_data = array(
					$product_order_item->get_id(),
					$product_order_item_order->get_id(),
					$product_order_item_order->get_date_paid(),
					$product_order_item->get_variation_id(),
					$product_order_item_variation->get_sku(),
					$product_order_item_variation->get_attribute( 'pa_size' ),
					$customer->ID ?? '',
					$customer_email,
					$customer_name,
					$product_order_item->get_quantity(),
					$line_item_price,
					$product_order_item->get_total()
				);

				fputcsv( $output, $csv_data );
			}

			$csv_output = ob_get_clean();

			fclose( $output );

			self::$response_data = OMH_CONTENT_URL . $csv_file_name;
		}

		self::send_ajax_response();
	}

	public static function publish_product() {

		self::check_nonce('product-status-action');

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$user = OMH_User::get_user( get_current_user_id() );
		$product_id = $omh_ajax['product_id'] ?? 0;

		$response_data = array(
			'reload' => false,
		);

		if(!$user->is_house_admin(false)) {
			self::add_message( 'Only House Admins can publish products.', 'danger', 'omh_publish_product' );
		} else {

			if( $product_id && $product = wc_get_product( $product_id ) ) {

				$product->set_product_status( 'published' );

				$response_data['reload'] = true;
			} else {

				self::add_message( 'There was a problem publishing the product, please try again later.', 'danger', 'omh_publish_product' );
			}
		}

		self::$response_data = $response_data;

		self::send_ajax_response();
	}

	public static function unpublish_product() {

		self::check_nonce('product-status-action');

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();

		$user = OMH_User::get_user( get_current_user_id() );
		$product_id = $omh_ajax['product_id'] ?? 0;

		$response_data = array(
			'reload' => false,
		);

		if(!$user->is_house_admin(false)) {
			self::add_message( 'Only House Admins can unpublish products.', 'danger', 'omh_unpublish_product' );
		} else {

			if( $product_id && $product = wc_get_product( $product_id ) ) {

				$product->set_product_status( 'unpublished' );

				$response_data['reload'] = true;
			} else {

				self::add_message( 'There was a problem unpublishing the product, please try again later.', 'danger', 'omh_unpublish_product' );
			}
		}

		self::$response_data = $response_data;

		self::send_ajax_response();
	}
	
	/********************************
	 * Chapters, Orgs, and Colleges *
	 ********************************/

	/*
	 * dev:improve These save functions can be combined into a single save function
	 * because the models can have their own save functions
	 */

	public static function save_chapter() {

		self::check_nonce( 'save-chapter' );

		if( !is_super_admin() ) {

			self::$response_status = 'fail';
			self::add_message( 'Only Super Admins can save a chapter!', 'danger', 'omh_save_chapter' );
			self::send_ajax_response();
		}

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();
		$form_data = $omh_ajax['form_data'];

		$response_obj = array();

		$chapter = new OMH_Model_Chapter( (object) $form_data );

		$validation = $chapter->validate();
		if( empty( $validation ) ) {

			if( $chapter->save() ) {

				self::$refresh_tables = true;
				self::$response_status = 'success';
				self::add_message( 'Successfully saved chapter.', 'success', 'omh_save_chapter' );
			} else {

				self::$response_status = 'fail';
				self::add_message( 'There was a problem saving the chapter.', 'danger', 'omh_save_chapter' );
			}
		} else {
			self::$response_status = 'fail';

			if( isset( $validation['required'] ) ) {
				self::add_message( 'The following fields are required: ' . implode( ', ', $validation['required'] ), 'danger', 'omh_save_chapter__required' );
			}

			if( isset( $validation['unique'] ) ) {
				self::add_message( 'The following fields must be unique: ' . implode( ', ', $validation['unique'] ), 'danger', 'omh_save_chapter__unique' );
			}
		}

		self::$response_data = $response_obj;
		self::send_ajax_response();
	}

	public static function save_organization() {

		self::check_nonce( 'save-organization' );

		if( !is_super_admin() ) {

			self::$response_status = 'fail';
			self::add_message( 'Only Super Admins can save a organization!', 'danger', 'omh_save_organization' );
			self::send_ajax_response();
		}

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();
		$form_data = $omh_ajax['form_data'];

		$response_obj = array();

		$organization = new OMH_Model_Organization( (object) $form_data );

		$validation = $organization->validate();
		if( empty( $validation ) ) {

			if( $organization->save() ) {

				self::$refresh_tables = true;
				self::$response_status = 'success';
				self::add_message( 'Successfully saved organization.', 'success', 'omh_save_organization' );
			} else {

				self::$response_status = 'fail';
				self::add_message( 'There was a problem saving the organization.', 'danger', 'omh_save_organization' );
			}
		} else {

			self::$response_status = 'fail';

			if( isset( $validation['required'] ) ) {
				self::add_message( 'The following fields are required: ' . implode( ', ', $validation['required'] ), 'danger', 'omh_save_organization__required' );
			}

			if( isset( $validation['unique'] ) ) {
				self::add_message( 'The following fields must be unique: ' . implode( ', ', $validation['unique'] ), 'danger', 'omh_save_organization__unique' );
			}
		}

		self::$response_data = $response_obj;
		self::send_ajax_response();
	}

	public static function save_college() {

		self::check_nonce( 'save-college' );

		if( !is_super_admin() ) {

			self::$response_status = 'fail';
			self::add_message( 'Only Super Admins can save a college!', 'danger', 'omh_save_college' );
			self::send_ajax_response();
		}

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();
		$form_data = $omh_ajax['form_data'];

		$response_obj = array();

		$college = new OMH_Model_College( (object) $form_data );

		$validation = $college->validate();
		if( empty( $validation ) ) {

			if( $college->save() ) {

				self::$refresh_tables = true;
				self::$response_status = 'success';
				self::add_message( 'Successfully saved the college.', 'success', 'omh_save_college' );
			} else {

				self::$response_status = 'fail';
				self::add_message( 'There was a problem saving the college.', 'danger', 'omh_save_college' );
			}
		} else {

			self::$response_status = 'fail';

			if( isset( $validation['required'] ) ) {
				self::add_message( 'The following fields are required: ' . implode( ', ', $validation['required'] ), 'danger', 'omh_save_college__required' );
			}

			if( isset( $validation['unique'] ) ) {
				self::add_message( 'The following fields must be unique: ' . implode( ', ', $validation['unique'] ), 'danger', 'omh_save_college__unique' );
			}
		}

		self::$response_data = $response_obj;
		self::send_ajax_response();
	}

	public static function add_credit_adjustment() {

		self::check_nonce( 'add-credit-adjustment' );

		if( !is_super_admin() ) {

			self::$response_status = 'fail';
			self::add_message( 'Only Super Admins can add credit adjustments!', 'danger', 'omh_add_credit_adjustment' );
			self::send_ajax_response();
		}

		$omh_ajax = $_POST['omh_ajax'] ?? array();
		$form_data = $omh_ajax['form_data'];

		$response_obj = array();

		// Convert to cents
		$adjustment_amount = isset( $form_data['adjustment_amount'] ) ? ( $form_data['adjustment_amount'] * 100 ) : 0;

		// Ledger Date
		$ledger_date = isset( $form_data['ledger_date'] ) ? new DateTime( $form_data['ledger_date'] , new DateTimeZone( 'US/Pacific' ) ) : new DateTime();
		$ledger_date->setTimezone( new DateTimeZone( 'UTC' ) );

		$chapter = OMH_Session::get_chapter();

		$adjustment_data = array(
			'type'			=> 'adjustment',
			'amount'		=> $adjustment_amount,
			'chapter_id'	=> $chapter ? $chapter->get_id() : 0,
			'notes'			=> isset( $form_data['adjustment_notes'] ) ? $form_data['adjustment_notes'] : '',
			'ledger_date'	=> $ledger_date->format( 'Y-m-d H:i:s' )
		);
		// $inserted_adjustment = insert_omh_credit( $adjustment_data );
		$inserted_adjustment = OMH()->credit_factory->create( $adjustment_data );

		if( $inserted_adjustment && !is_wp_error( $inserted_adjustment ) ) {

			$response_obj['credit_balance'] = array(
				'value'	=> wc_price( OMH()->credit_factory->get_total_credit( $chapter ? $chapter->get_id() : 0, false ) )
			);

			self::$refresh_tables = true;
			self::$response_status = 'success';
			self::add_message( 'Successfully added credit adjustment.', 'success', 'omh_add_credit_adjustment' );
		} else {

			self::$response_status = 'fail';
			self::add_message( 'There was a problem adding the credit adjustment.', 'danger', 'omh_add_credit_adjustment' );
		}

		self::$response_data = $response_obj;
		self::send_ajax_response();
	}

	public static function contact_us_form() {

		self::check_nonce( 'contact-us-form' );

		$omh_ajax = $_POST['omh_ajax'] ?? array();

		$form_data = $omh_ajax['form_data'];

		$response_obj = array(
			'name'		=> array(
				'value'		=> ''
			),
			'email'			=> array(
				'value'		=> ''
			),
			'subject'		=> array(
				'value'		=> ''
			),
			'message'		=> array(
				'value'		=> ''
			)
		);
		$valid_form = true;

		if( !$form_data['name'] ) {
			
			$valid_form = false;
			$response_obj['name']['invalid_msg'] = 'Name is a required field.';
		}

		if( !$form_data['email'] ) {
			
			$valid_form = false;
			$response_obj['email']['invalid_msg'] = 'Email is a required field.';
		}

		if( !$form_data['subject'] ) {
			
			$valid_form = false;
			$response_obj['subject']['invalid_msg'] = 'Subject is a required field.';
		}

		if( !$form_data['message'] ) {
			
			$valid_form = false;
			$response_obj['message']['invalid_msg'] = 'Message is a required field.';
		}

		if( $valid_form ) {

			// Send contact form
			$admin_email = 'info@merch.house';
			$from_name = $form_data['name'];
			$from_email = $form_data['email'];
			$subject = $form_data['subject'];
			$message = "
			<br />
			Name: {$from_name}
			<br />
			Email: {$from_email}
			<br />
			Subject: {$subject}
			<br />
			Messsage: {$form_data['message']}
			";

			$email_sent = wp_mail(
				$admin_email,
				"[Contact Form] {$subject}",
				$message,
				array(
					"From: {$from_name} <{$admin_email}>"
				)
			);

			if( !$email_sent ) {

				self::$response_status = 'fail';

				self::add_message( 'Failed to send contact form. Please try again later.', 'danger', 'omh_contact_us_form' );
			} else {

				self::add_message( 'Thanks for contacting us! We will get in touch with you shortly.', 'success', 'omh_contact_us_form' );
			}
		} else {

			self::$response_status = 'fail';

			self::add_message( 'Required fields missing. Please fix form errors and try again.', 'danger', 'omh_contact_us_form' );
		}

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	public static function temporary_file_upload() {

		// self::check_nonce( 'temporary-file-upload' );

		$form_data = $_POST ?? array();
		$form_files = $_FILES ?? array();

		$response_obj = array();

		// Handle file upload
		if( !empty( $form_files ) ) {

			foreach( $form_files as $file_index => $file_data ) {

				$field_label = ucwords( str_replace( '_', ' ', $file_index ) );

				if( OMH()->verify_omh_dir( OMH_TEMP_UPLOAD_DIR ) ) {

					$temporary_file_name = get_unique_filename( sanitize_file_name( $file_data['name'] ), OMH_TEMP_UPLOAD_DIR );

					$file_location = OMH_TEMP_UPLOAD_DIR . '/' . $temporary_file_name;
					$file_url = OMH_TEMP_UPLOAD_URL . '/' . $temporary_file_name;

					if( move_uploaded_file( $file_data['tmp_name'], $file_location ) ) {

						// Create Thumbnail
						$thumbnail_name = 'thumbnail_200_' . $temporary_file_name;
						$thumbnail_location = OMH_TEMP_UPLOAD_DIR . '/' . $thumbnail_name;
						$thumbnail_url = OMH_TEMP_UPLOAD_URL . '/' . $thumbnail_name;

						remove_filter( 'wp_image_editors', 'ewww_image_optimizer_load_editor', 60 );
						$image_thumbnail = wp_get_image_editor( $file_location );
						// 200 width
						$image_thumbnail->resize( 200, null );
						$image_thumbnail->save( $thumbnail_location );

						$temp_upload_args = array(
							'file_name' 		=> $temporary_file_name,
							'file_path' 		=> $file_location,
							'file_url' 			=> $file_url,
							'thumbnail_path' 	=> $thumbnail_location,
							'thumbnail_url' 	=> $thumbnail_url
						);

						// Create row in Temp Upload table
						if( $temp_upload_id = OMH()->temp_upload_factory->create( $temp_upload_args ) ) {

							self::add_message( 'File uploaded successfully', 'success', 'omh_temporary_file_upload' );

							$response_obj[ $file_index ] = array(
								'id'			=> $temp_upload_id->get_id(),
								'is_temp'		=> true,
								'src'			=> $file_url,
								'thumbnail_src'	=> $thumbnail_url
							);
						} else {

							self::$response_status = 'fail';
							self::add_message( 'There was a problem saving the file.', 'danger', 'omh_temporary_file_upload' );
						}
					} else {

						self::$response_status = 'fail';
						self::add_message( 'There was a problem saving the file.', 'danger', 'omh_temporary_file_upload' );
					}
				} else {

					self::$response_status = 'fail';
					self::add_message( 'There was a problem creating the directory.', 'danger', 'omh_temporary_file_upload' );
				}
			}
		}

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	public static function signup_form() {

		self::check_nonce( 'signup-form' );

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();
		$form_data = $omh_ajax['form_data'];

		$response_obj = array();
		$email_fields = array();

		foreach( $form_data as $form_field => $form_value ) {

			$response_obj[ $form_field ] = array(
				'value'			=> $form_value
			);

			$email_fields[] = array(
				'label'			=> ucwords( str_replace( '_', ' ', $form_field ) ),
				'value'			=> $form_value
			);
		}

		do_action( 'omh_send_signup_form', $email_fields );
		do_action( 'omh_send_chapter_signup', $email_fields );

		self::add_message( 'Thanks for signing up! You will be contacted by a Merch House team member shortly.', 'success', 'omh_signup_form_success', 'signup-modal-notices' );

		self::$response_data = $response_obj;

		self::send_ajax_response();
	}

	public static function start_design_form() {

		self::check_nonce( 'start-design-form' );

		$form_data = $_POST;
		$form_files = $_FILES ?? array();

		$response_obj = array(
			'name' => array (
				'value' => $form_data['name']
			),
			'email' => array (
				'value' => $form_data['email']
			),
			'phone_number' => array (
				'value' => $form_data['phone_number']
			),
			'chapter' => array (
				'value' => $form_data['chapter']
			),
			'description' => array (
				'value' => $form_data['description']
			),
			'file_upload_1' 			=> array(
				'value'	=> ''
			),
			'file_upload_2' 			=> array(
				'value'	=> ''
			),
		);


		// Handle File Uploads
		if (!empty($form_files)) {
			foreach( $form_files as $file_index => $file_data) {
				if( !$file_data['size'] ) {
					continue;
				}
				$timestamp = time();
				$file_name = $timestamp . '-' . sanitize_file_name( $file_data['name'] );
				omh_debug("File Data: ");
				omh_debug($file_data);

				// Is this file even part of our form?
				if( 
					!in_array( $file_index, array_keys( $response_obj ) ) 
					|| !isset( $file_data['name'], $file_data['tmp_name'] )
					|| empty( $file_data['name'] ) 
					|| empty( $file_data['tmp_name'] )
				) {

					$response_obj[ $file_index ]['invalid_msg'] = 'This image does not exist in the form!';

					continue;
				}

				$field_label = ucwords( str_replace( '_', ' ', $file_index ) );

				$exif_allowed_types = array( IMAGETYPE_JPEG, IMAGETYPE_PSD, IMAGETYPE_GIF, IMAGETYPE_PNG, IMAGETYPE_TIFF_II, IMAGETYPE_TIFF_MM );
				$detected_exif_type = exif_imagetype( $file_data['tmp_name'] );

				$mime_allowed_types = array( 
					// JPG or JPEG
					'application/jpg', 
					'image/jpg', 
					'application/jpeg', 
					'image/jpeg', 

					// PDF
					'application/pdf', 

					// AI
					'application/illustrator', 
					'application/postscript', 

					// PSD
					'application/photoshop',
					'image/vnd.adobe.photoshop', 
					'image/x-psd',

					// PNG
					'image/png',

					// SVG
					'image/svg+xml',

					// TIFF
					'image/tiff',

					// GIF
					'image/gif',
				);
				$detected_mime_type = mime_content_type( $file_data['tmp_name'] );

				// Make sure it's a type we allow ( or a PDF @see is_pdf_file() )
				if( 
					in_array( $detected_exif_type, $exif_allowed_types ) 
					|| in_array( $detected_mime_type, $mime_allowed_types )
					// || is_pdf_file( $file_data['tmp_name'] )
				) {
					
					// Upload the file to wp-content/uploads/sites/{site_id}/omh/{design_id}/
					$file_dir = WP_CONTENT_DIR . '/uploads/start-design';

					// Make sure DIR exists!
					if( !file_exists( $file_dir ) ) {
						mkdir( $file_dir, 0755 );
					}
					
					move_uploaded_file( $file_data['tmp_name'], $file_dir . '/' . $file_name );

					// dev:todo
					$file_url = content_url() . '/uploads/start-design/' . $file_name;

					$response_obj[ $file_index ]['value'] = $file_url;
				} else {

					$response_obj[ $file_index ]['invalid_msg'] = "{$field_label} must be of type: .jpg .jpeg .psd .ai .pdf .png .gif .tiff";
				}
			}
		}

		// // Save uploads to response
		$product_media_uploads = array(
			'media_1'	=> $response_obj['file_upload_1']['value'],
			'media_2'	=> $response_obj['file_upload_2']['value']
		);
		foreach( $response_obj as $form_field => $form_value ) {
			$email_fields[] = array(
				'label'			=> ucwords( str_replace( '_', ' ', $form_field ) ),
				'value'			=> $form_value['value']
			);
		}
		
		do_action('omh_send_start_design_form', $email_fields);

		self::add_message( 'Thanks, we\'ll contact you soon!', 'success', 'start-design-notice', 'start-design-notice' );
		self::$response_data = $response_obj;
		self::send_ajax_response();
	}

	public static function organization_start_design_form() {

		self::check_nonce( 'organization-start-design-form' );

		$form_data = $_POST;
		$form_files = $_FILES ?? array();

		$response_obj = array(
			'osd_name' => array (
				'value' => $form_data['osd_name']
			),
			'osd_email' => array (
				'value' => $form_data['osd_email']
			),
			'osd_phone_number' => array (
				'value' => $form_data['osd_phone_number']
			),
			'osd_chapter' => array (
				'value' => $form_data['osd_chapter']
			),
			'osd_description' => array (
				'value' => $form_data['osd_description']
			),
			'osd_selection_id' => array (
				'value' => $form_data['osd_selection_id']
			),
			'osd_file_upload_1' 			=> array(
				'value'	=> ''
			),
			'osd_file_upload_2' 			=> array(
				'value'	=> ''
			),
		);


		// Handle File Uploads
		if (!empty($form_files)) {
			foreach( $form_files as $file_index => $file_data) {
				if( !$file_data['size'] ) {
					continue;
				}
				$timestamp = time();
				$file_name = $timestamp . '-' . sanitize_file_name( $file_data['name'] );
				omh_debug("File Data: ");
				omh_debug($file_data);

				// Is this file even part of our form?
				if( 
					!in_array( $file_index, array_keys( $response_obj ) ) 
					|| !isset( $file_data['name'], $file_data['tmp_name'] )
					|| empty( $file_data['name'] ) 
					|| empty( $file_data['tmp_name'] )
				) {

					$response_obj[ $file_index ]['invalid_msg'] = 'This image does not exist in the form!';

					continue;
				}

				$field_label = ucwords( str_replace( '_', ' ', $file_index ) );

				$exif_allowed_types = array( IMAGETYPE_JPEG, IMAGETYPE_PSD, IMAGETYPE_GIF, IMAGETYPE_PNG, IMAGETYPE_TIFF_II, IMAGETYPE_TIFF_MM );
				$detected_exif_type = exif_imagetype( $file_data['tmp_name'] );

				$mime_allowed_types = array( 
					// JPG or JPEG
					'application/jpg', 
					'image/jpg', 
					'application/jpeg', 
					'image/jpeg', 

					// PDF
					'application/pdf', 

					// AI
					'application/illustrator', 
					'application/postscript', 

					// PSD
					'application/photoshop',
					'image/vnd.adobe.photoshop', 
					'image/x-psd',

					// PNG
					'image/png',

					// SVG
					'image/svg+xml',

					// TIFF
					'image/tiff',

					// GIF
					'image/gif',
				);
				$detected_mime_type = mime_content_type( $file_data['tmp_name'] );

				// Make sure it's a type we allow ( or a PDF @see is_pdf_file() )
				if( 
					in_array( $detected_exif_type, $exif_allowed_types ) 
					|| in_array( $detected_mime_type, $mime_allowed_types )
					// || is_pdf_file( $file_data['tmp_name'] )
				) {
					
					// Upload the file to wp-content/uploads/sites/{site_id}/omh/{design_id}/
					$file_dir = WP_CONTENT_DIR . '/uploads/start-design';

					// Make sure DIR exists!
					if( !file_exists( $file_dir ) ) {
						mkdir( $file_dir, 0755 );
					}
					
					move_uploaded_file( $file_data['tmp_name'], $file_dir . '/' . $file_name );

					// dev:todo
					$file_url = content_url() . '/uploads/start-design/' . $file_name;

					$response_obj[ $file_index ]['value'] = $file_url;
				} else {

					$response_obj[ $file_index ]['invalid_msg'] = "{$field_label} must be of type: .jpg .jpeg .psd .ai .pdf .png .gif .tiff";
				}
			}
		}

		// // Save uploads to response
		$product_media_uploads = array(
			'media_1'	=> $response_obj['osd_file_upload_1']['value'],
			'media_2'	=> $response_obj['osd_file_upload_2']['value']
		);
		foreach( $response_obj as $form_field => $form_value ) {
			$email_fields[] = array(
				'label'			=> ucwords( str_replace( '_', ' ', $form_field ) ),
				'value'			=> $form_value['value']
			);
		}
		omh_debug("EMAIL FIELDS!");
		omh_debug($email_fields);

		
		do_action('omh_send_organization_start_design_form', $email_fields);

		self::add_message( 'Thanks, we\'ll contact you soon!', 'success', 'start-design-notice', 'osd-notices' );
		self::$response_data = $response_obj;
		self::send_ajax_response();
	}


	public static function new_omh_follower() {
		
		self::check_nonce('new-omh-follower');

		$omh_ajax = array_map( 'stripslashes_deep', $_POST['omh_ajax'] ) ?? array();
		$form_data = $omh_ajax['form_data'];

		$follower = new OMH_Model_Follower( (object) $form_data);

		$validation = $follower->validate();
		if( empty( $validation ) ) {

			// Check to see if same follower exists
			$follower_exists = OMH_Factory_Follower::query( 
				array(
					'where' => array(
						array(
							'name'	=> 'follower_email',
							'value'	=> $form_data['follower_email']
						),
						array(
							'name'	=> 'following_id',
							'value'	=> $form_data['following_id']
						),
						array(
							'name'	=> 'following_type',
							'value'	=> $form_data['following_type']
						)
					),
					'return' => 'count'
				)
			);

			if( !empty( $follower_exists ) ) {

				self::$response_status = 'success';
				self::add_message( 'Email address is already a follower of this object.', 'success', 'omh_new_follower', 'new-follower-modal-notices--hidden' );
			}
			else if( $follower->save() ) {

				self::$response_status = 'success';
				self::add_message( 'Successfully created follower.', 'success', 'omh_new_follower', 'new-follower-modal-notices--hidden' );
				OMH_Emails::send_admin_new_follower_notification($follower);

			} else {

				self::$response_status = 'fail';
				self::add_message( 'There was a problem submitting your request.', 'danger', 'omh_new_follower', 'new-follower-modal-notices' );
			}
		} else {
			self::$response_status = 'fail';

			if( isset( $validation['required'] ) ) {
				self::add_message( 'The following fields are required: ' . implode( ', ', $validation['required'] ), 'danger', 'omh_new_follower__required', 'new-follower-modal-notices' );
			}

			if( isset( $validation['unique'] ) ) {
				self::add_message( 'The following fields must be unique: ' . implode( ', ', $validation['unique'] ), 'danger', 'omh_new_follower__unique', 'new-follower-modal-notices' );
			}
		}

		self::$response_data = $form_data;
		self::send_ajax_response();
	}
}
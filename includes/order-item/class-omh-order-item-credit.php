<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Order Line Item (credit)
 */
class OMH_Order_Item_Credit extends WC_Order_Item {

	/**
	 * Order Data array. This is the core order data exposed in APIs since 3.0.0.
	 * 
	 * @var array
	 */
	protected $extra_data = array(
		'amount'	=> '',
		'total'		=> '',
	);

	/*
	|--------------------------------------------------------------------------
	| Setters
	|--------------------------------------------------------------------------
	*/

	/**
	 * Set credit amount
	 *
	 * @param 	string 				$value
	 * 
	 * @throws 	WC_Data_Exception
	 */
	public function set_amount( $value ) {
		$this->set_prop( 'amount', wc_format_decimal( $value ) );
	}

	/**
	 * Set credit total
	 *
	 * @param 	string 				$amount
	 * 
	 * @throws 	WC_Data_Exception
	 */
	public function set_total( $amount ) {
		$this->set_prop( 'total', wc_format_decimal( $amount ) );
	}

	/*
	|--------------------------------------------------------------------------
	| Getters
	|--------------------------------------------------------------------------
	*/

	/**
	 * Get order item type
	 *
	 * @return 	string
	 */
	public function get_type() {
		return 'credit';
	}

	/**
	 * Get credit amount
	 *
	 * @param  	string 				$context
	 * 
	 * @return 	string
	 */
	public function get_amount( $context = 'view' ) {
		return $this->get_prop( 'amount', $context );
	}

	/**
	 * Get credit total
	 *
	 * @param  	string 				$context
	 * 
	 * @return 	string
	 */
	public function get_total( $context = 'view' ) {
		return $this->get_prop( 'total', $context );
	}

	/*
	|--------------------------------------------------------------------------
	| Array Access Methods
	|--------------------------------------------------------------------------
	|
	| For backwards compatibility with legacy arrays.
	|
	*/

	/**
	 * offsetGet for ArrayAccess/Backwards compatibility.
	 * @deprecated Add deprecation notices in future release.
	 * @param string $offset
	 * @return mixed
	 */
	public function offsetGet( $offset ) {
		if ( 'credit_amount' === $offset ) {
			$offset = 'amount';
		} elseif ( 'credit_amount_total' === $offset ) {
			$offset = 'total';
		}
		return parent::offsetGet( $offset );
	}

	/**
	 * offsetSet for ArrayAccess/Backwards compatibility.
	 * @deprecated Add deprecation notices in future release.
	 * @param string $offset
	 * @param mixed $value
	 */
	public function offsetSet( $offset, $value ) {
		if ( 'credit_amount' === $offset ) {
			$offset = 'amount';
		} elseif ( 'credit_amount_total' === $offset ) {
			$offset = 'total';
		}
		parent::offsetSet( $offset, $value );
	}

	/**
	 * offsetExists for ArrayAccess
	 * @param string $offset
	 * @return bool
	 */
	public function offsetExists( $offset ) {
		if ( in_array( $offset, array( 'credit_amount', 'credit_amount_total' ) ) ) {
			return true;
		}
		return parent::offsetExists( $offset );
	}
}
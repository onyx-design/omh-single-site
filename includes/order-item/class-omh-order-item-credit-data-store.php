<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC Order Item Credit Data Store
 */
class OMH_Order_Item_Credit_Data_Store extends Abstract_WC_Order_Item_Type_Data_Store implements WC_Object_Data_Store_Interface, WC_Order_Item_Type_Data_Store_Interface {

	/**
	 * Data stored in meta keys.
	 *
	 * @since 3.0.0
	 * @var array
	 */
	protected $internal_meta_keys = array( '_credit_amount', '_line_subtotal', '_line_total' );

	/**
	 * Read/populate data properties specific to this order item.
	 *
	 * @param 	WC_Order_Item_Credit 	$item
	 */
	public function read( &$item ) {

		parent::read( $item );

		$id = $item->get_id();

		$item->set_props( 
			array(
				'amount'     => get_metadata( 'order_item', $id, '_fee_amount', true ),
				'total'      => get_metadata( 'order_item', $id, '_line_total', true ),
			)
		);

		$item->set_object_read( true );
	}

	/**
	 * Saves an item's data to the database / item meta.
	 * Ran after both create and update, so $id will be set.
	 *
	 * @param 	WC_Order_Item_Credit 	$item
	 */
	public function save_item_data( &$item ) {

		$id          = $item->get_id();

		$save_values = array(
			'_fee_amount'    => $item->get_amount( 'edit' ),
			'_line_total'    => $item->get_total( 'edit' ),
		);

		foreach ( $save_values as $key => $value ) {
			
			update_metadata( 'order_item', $id, $key, $value );
		}
	}
}

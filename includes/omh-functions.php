<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * GENERAL FUNCTIONS
 * 
 * Mainly generic helper functions
 **/

function is_onyx_user() {

	$current_user = wp_get_current_user();

	if( ( $current_user->user_email == 'dev@onyxdesign.net' ) && ( $current_user->user_login == 'ONYX' ) ) {
		return true;
	}

	return false;
}

if( !function_exists( 'omh_debug' ) ) {
	// Auto-init and access OMH_Debug class
	function omh_debug($arg=null) {
		$instance = OMH_Debug::instance();
		return func_num_args() ? $instance($arg) : $instance;
	}
}

if( !function_exists( 'omh_dev_log' ) ) {
	function omh_dev_log( $msg, $clear_log = false ) {
		$msg = ( is_object( $msg ) || is_array( $msg ) ) ? print_r( $msg, true ) : $msg ;
		if( $clear_log ) {file_put_contents( OMH_CONTENT_DIR.'/omh-dev-log.txt', "\r\n".date( 'Y-m-d H:i:s' ).substr( (string) microtime() , 1, 8 ).' - '.$msg );	}
		else file_put_contents( OMH_CONTENT_DIR.'/omh-dev-log.txt', "\r\n".date( 'Y-m-d H:i:s').substr( (string) microtime() , 1, 8 ).' - '.$msg, FILE_APPEND );
	}
}

if( !function_exists( 'ox_exec_time' ) ) {
	// Get script execution time
	function ox_exec_time() {
		return ( microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] );
	}
}

if( !function_exists( 'log_print_r' ) ) {
	function log_print_r( $log, $message = '' ) {

		$log = print_r( $log, true );

		if( $message ) {
			$log = "$message: $log";
		}

		error_log( $log );
	}
}

if( !function_exists( 'log_var' ) ) {
	function log_var( $log, $message = '' ) {

		$log = var_export( $log, true );

		if( $message ) {
			$log = "$message: $log";
		}

		error_log( $log );
	}
}

if( !function_exists( 'maybe_str_to_bool' ) ) {
	function maybe_str_to_bool( $boolstr = '' ) {
		if( !is_bool( $boolstr ) ) {

			$boolstr = strtolower( $boolstr );

			if( 'true' === $boolstr ) {
				return true;
			} elseif( 'false' === $boolstr ) {
				return false;
			} else {
				return false;
			}
		}

		return $boolstr;
	}
}

if( !function_exists( 'get_unique_filename' ) ) {
	function get_unique_filename( $filename, $path ) {

		$pathinfo = pathinfo( $filename );

		$files = array_map( function( $file ) {

			return pathinfo($file, PATHINFO_FILENAME);
		}, preg_grep( '~^' . $pathinfo['filename'] . '.*~', scandir( $path ) ) );

		$count = count( $files ) + 1;

		return ( $count > 1 ) ? $pathinfo['filename'] . '_' . $count . '.' . $pathinfo['extension'] : $filename;
	}
}

if( !function_exists( 'array_merge_recursive_distinct' ) ) {
	function array_merge_recursive_distinct() {

		if (func_num_args() < 2) {
			trigger_error( __FUNCTION__ .' needs two or more array arguments', E_USER_WARNING );
			return;
		}

		$arrays = func_get_args();
		$merged = array();
		while ( $arrays ) {
			
			$array = array_shift( $arrays );
			if (!is_array( $array ) ) {
				trigger_error( __FUNCTION__ .' encountered a non array argument', E_USER_WARNING );
				return;
			}

			if ( !$array ) {
				continue;
			}

			foreach ( $array as $key => $value ) {

				if ( is_string( $key ) ) {

					if ( is_array( $value ) && array_key_exists( $key, $merged ) && is_array( $merged[ $key ] ) ) {
						$merged[ $key ] = call_user_func( __FUNCTION__, $merged[ $key ], $value );
					} else {
						$merged[ $key ] = $value;
					}
				} else {
					$merged[] = $value;
				}
			}
		}
		return $merged;
	}
}

/**
 * Get the filesize in a human readable format
 * 
 * @param   string|int  $filesize 
 * 
 * @return  string
 */
if( !function_exists( 'human_readable_filesize' ) ) {
	function human_readable_filesize( $filesize ) {

		$filesize = (int) $filesize;

		// @see https://en.wikipedia.org/wiki/Kilobyte
		$sizes = array( 
					// Metric       Decimal     Binary  
			'B',    // Bytes        (B)         (B)
			'kB',   // Kilobytes    (kB)        (KiB)
			'MB',   // Megabytes    (MB)        (MiB)
			'GB',   // Gigabytes    (GB)        (GiB)
			'TB',   // Terabytes    (TB)        (TiB)
			'PB',   // Petabyes     (PB)        (PiB)
			'EB',   // Exabytes     (EB)        (EiB)
			'ZB',   // Zettabyes    (ZB)        (ZiB)
			'YB'    // Yottabytes   (YB)        (YiB)
		);

		// Get the factor by counting the digits, subtracting 1 and dividing by 3
		// ex. 65536 = 1 ()
		$factor = floor( ( strlen( $filesize ) - 1 ) / 3 );

		// Return the number format of the filesize divided by the bitshifted 1 ( add $factor multiplied by 10 to the bit to convert to 1024 )
		// Include the size format at the end and ignore errors if the format doesn't exist
		return number_format( ( $filesize / ( 1<<( $factor * 10 ) ) ), 2 ) . @$sizes[ $factor ];
	}
}

if( !function_exists( 'omh_validate_cart' ) ) {
	function omh_validate_cart() {

		$invalid_products = array();

		foreach( WC()->cart->get_cart_contents() as $cart_content ) {

			if( !isset( $cart_content['product_id'] ) ) {
				continue;
			}

			$cart_product = wc_get_product( $cart_content['product_id'] );

			$quantity = $cart_product->get_cart_quantity();

			if( $cart_product->requires_minimum_order_quantity( $quantity ) ) {
				$invalid_products[ $cart_product->get_id() ] = array(
					'name'		=> $cart_product->get_name(),
					'minimum'	=> $cart_product->minimum_order_quantity()
				);
			}
		}

		return $invalid_products;
	}
}

/**
 * USER FUNCTIONS
 **/

if( !function_exists( 'wp_set_current_user' ) ) {
	function wp_set_current_user( $id, $name = '' ) {
		global $current_user;

		if( isset( $current_user )
			&& ( $current_user instanceof WP_User )
			&& ( $id == $current_user->ID )
			&& ( null !== $id )
		) {
			return $current_user;
		}

		$current_user = new OMH_User( $id, $name );

		setup_userdata( $current_user->ID );

		do_action( 'set_current_user' );

		return $current_user;
	}
}

if( !function_exists( 'get_user_by' ) ) {
	function get_user_by( $field, $value ) {
		$userdata = OMH_User::get_data_by( $field, $value );

		if( !$userdata ) {
			return false;
		}

		$user = new OMH_User;
		$user->init( $userdata );

		return $user;
	}
}

/**
 * PRODUCT FUNCTIONS
 **/

/**
 * Get sales types
 *
 * @return 	array
 */
if( !function_exists( 'omh_get_sales_types' ) ) {
	function omh_get_sales_types() {

		return array(
			'retail'	=> 'Retail product',
			'bulk'		=> 'Bulk product',
			'campaign'	=> 'Campaign product',
		);
	}
}

/**
 * dev:todo This will replace the giant function in OMH AJAX - needs to be broken up more,
 * but this is a start as it can be used in multple situations
 */
if( !function_exists( 'create_omh_product' ) ) {
	function create_omh_product( $args ) {

		$default_args = array(
			'product_name'	=> '',
			'post_author'	=> get_current_user_id(),
			'meta'			=> array(
				'_design_approved_mh' 		=> '',
				'_design_approved_ha'		=> '',
				'_visibility_mh'			=> 'design_in_review',

				'_design_id'				=> '',
				'_mh_product_id'			=> '',
				'_mh_product_style_id'		=> '',
				'_mh_base_cost'				=> '8.00',

				// Product Template Info
				'_mh_blank_name'			=> '', // Template Name ( read only! )
				'_mh_manufacturer'			=> '', // Brand ( read only! )
				'_mh_blank_sku'				=> '', // Template SKU ( read only! )
				'_mh_blank_style_number'	=> '', // Template Style Number ( read only! )
				'_mh_blank_style_name'		=> '', // Template Style Name ( read only! )
				'_mh_blank_color'			=> '', // Template Color ( read only! )
				'_mh_blank_sizes'			=> '', // Available Sizes ( read only! )
				'_mh_blank_description'		=> '', // Template Description ( read only! )
				'_mh_customer_comment'		=> '',
				'_mh_sales_type'			=> 'retail',
				// '_mh_product_type'			=> 'campaign',
				'_mh_date_required'			=> '',
				'_mh_created_by'			=> get_current_user_id()
			),
			'product_image'	=> '',
			'attributes'	=> array(
				'sizes'			=> ''
			)
		);

		$args = array_merge_recursive_distinct( $default_args, $args );

		$post_data = array(
			'post_author'	=> $args['post_author'],
			'post_content'	=> '',
			'post_status'	=> 'publish',
			'post_title'	=> $args['product_name'],
			'post_parent'	=> '',
			'post_type'		=> 'product',
			'tax_input'		=> array(
				'product_type'	=> 'variable'
			)
		);

		$post_id = wp_insert_post( $post_data, true );
		$product_classname = OMH_Product_Manager::get_classname_from_sales_type( $args['meta']['_mh_sales_type'] );

		$product = new $product_classname( $post_id );

		$product->set_product_status( 'design_in_review' );

		// Build Product Sku
		// dev:todo No sku for parent product
		// $product->build_product_id();

		$product->set_manage_stock( false );
		$product->set_stock_status( 'instock' );

		foreach( $args['meta'] as $meta_name => $meta_value ) {
			$product->update_meta_data( $meta_name, $meta_value );
		}

		$product_attributes = array();
		$default_attributes = array();
		$garment_meta = array();

		if( $product_garments = array_filter( $args['attributes']['garments'] ) ) {

			$garment_term_ids = array();

			foreach( $product_garments as $product_garment ) {

				$product_garment_style = OMH()->garment_style_factory->get_by_inksoft_id( $product_garment['product_style_id'] );

				// If it doesn't exist, we need to call the InkSoft API
				if( !$product_garment_style ) {

					$inksoft_api_response = OMH_API_Inksoft::request( 'get_product', array( 
						'query_params' => array( 
							'ProductID' => $product_garment['product_id'],
							'Email' 	=> 'admin@merch.house',
							'ApiKey'	=> '161bf9b2-7d78-4614-ac03-9eef97dd2e37'
						) 
					) );

					$garment_product = OMH_Model_Garment_Product::get_or_create_from_api_response( $inksoft_api_response );

					if( false !== $garment_product ) {

						$garment_product->populate_style_data();

						$product_garment_style = OMH()->garment_style_factory->get_by_inksoft_id( $product_garment['product_style_id'] );
					}
				}

				if( $product_garment_style ) {

					$garment_term = omh_get_or_create_wc_attribute_term( 'pa_garment', $product_garment_style->get_term_label(), $product_garment_style->get_term_slug() );
					$garment_term = $product_garment_style->get_term();

					if( $garment_term ) {
						$garment_term_ids[] = $garment_term->term_id;
					}

					$garment_style_sizes = explode( ',', $product_garment_style->get_sizes() );
					$product_pricing = $product->get_product_price_tiers();

					// Set Garment meta
					$garment_meta[] = array(
						'garment_style_id'	=> $product_garment_style->get_id(),
						'featured'			=> false,
						'pricing'			=> array_fill( 0, count( $product_pricing ), 20 ),
						'sizes'				=> array_fill_keys( $garment_style_sizes, false ),
					);
				}
			}

			$garment_attribute = new WC_Product_Attribute();
			$garment_attribute->set_id( wc_attribute_taxonomy_id_by_name( 'pa_garment' ) );
			$garment_attribute->set_name( 'pa_garment' );
			$garment_attribute->set_options( $garment_term_ids );
			$garment_attribute->set_position( 0 );
			$garment_attribute->set_visible( true );
			$garment_attribute->set_variation( true );

			$product_attributes['pa_garment'] = $garment_attribute;

			if( $garment_term_ids ) {
				$default_attributes['pa_garment'] = get_term_by( 'id', current( $garment_term_ids ), 'pa_garment' )->slug;
			}
		}

		/*
		 * dev:improve The size attributes should be using the attributes when creating a garment style above
		 * This can be an issue if we try to create a product that has multiple garments where one garment has more styles!!!
		 * How to fix this:
		 * - Add sizes to an array when adding garment attributes to the product (array_diff)
		 * - Create the size attributes based on this created array
		 * - Remove $args['attributes']['sizes'] from the array?
		 */

		// Set attributes
		if( $product_sizes = $args['attributes']['sizes'] ) {

			$sizes = explode( ',', $product_sizes );
			$size_term_ids = array();

			foreach( $sizes as $size ) {

				$size_term = omh_get_or_create_wc_attribute_term( 'pa_size', $size );

				if( $size_term ) {
					$size_term_ids[] = $size_term->term_id;
				}
			}

			$size_attribute = new WC_Product_Attribute();
			$size_attribute->set_id( wc_attribute_taxonomy_id_by_name( 'pa_size' ) );
			$size_attribute->set_name( 'pa_size' );
			$size_attribute->set_options( $size_term_ids );
			$size_attribute->set_position( 1 );
			$size_attribute->set_visible( true );
			$size_attribute->set_variation( true );

			$product_attributes['pa_size'] = $size_attribute;

			$default_attributes['pa_size'] = get_term_by( 'id', current( $size_term_ids ), 'pa_size' )->slug;
		}
		
		$product->set_attributes( $product_attributes );
		$product->set_default_attributes( $default_attributes );

		$product->set_garments_meta( $garment_meta );

		// Set Placeholder Image
		// Make sure this is after setting the garment meta so that we overwrite the 0 Image ID
		if( $args['product_image'] ) {
			if( $attachment_id = OMH_Tool_Sideload_Media::insert_media_file_from_url( $args['product_image'] ) ) {
				$product->set_image_id( $attachment_id );
			}
		}

		// Associate product with a chapter
		if( $chapter = OMH()->session->get_chapter() ) {
			$product->set_chapter( $chapter->get_term() );


			$product->set_sku(
				strtoupper(
					implode(
						'-',
						array(
							$chapter->get_college()->get_college_letters(),
							$chapter->get_org()->get_org_letters(),
							$product->get_id()
						)
					)
				)
			);
		}

		$product->save();

		return $product;
	}
}

/**
 * DASHBOARD FUNCTIONS
 **/

function omh_get_dashboard_menu( $theme_location ) {
	global $post;

	$locations = get_nav_menu_locations();

	$menu = get_term( $locations[ $theme_location ], 'nav_menu' );
	$menu_items = wp_get_nav_menu_items( $menu->term_id, array( 'output_key' => 'ID' ) );

	if( false === $menu_items ) {

		if( is_super_admin() ) {
			return 'You must <a href="' . admin_url( 'nav-menus.php' ) . '">add a menu</a> in the backend';
		} else {
			return false;
		}
	}

	$mh_menu_items = array();

	foreach( $menu_items as $menu_item ) {

		if( !$menu_item->menu_item_parent ) {
			$mh_menu_items[ $menu_item->db_id ] = array( 'post' => $menu_item, 'children' => array() );
		}
		else {
			$mh_menu_items[ $menu_item->menu_item_parent ]['children'][] = $menu_item;
		}
	}

	$menu_ui_element = OMH_HTML_Tag::factory(
		array(
			'type'	=> 'ul',
			'class'	=> array(
				'nav',
				'navbar-nav',
				'flex-column'
			)
		)
	);

	$menu_item_defaults = array(
		'type'		=> 'li',
		'class'		=> 'nav-item'
	);

	$nav_link_defaults = array(
		'type'		=> 'a',
		'class'		=> 'nav-link'	
	);

	foreach( $mh_menu_items as $mh_menu_item ) {

		$menu_item_post = $mh_menu_item['post'];
		$menu_item_active = ( isset( $menu_item_post->object_id ) && ( $post->ID == $menu_item_post->object_id ) );
		$menu_item_tag = OMH_HTML_Tag::factory( $menu_item_defaults );

		$submenu_tag = false;

		$nav_link_args = array(
			'attrs'	=> array(
				'href'	=> $menu_item_post->url
			),
			'contents'	=> $menu_item_post->title
		);

		$nav_link_args = array_replace_recursive( $nav_link_defaults, $nav_link_args );
		$nav_link_tag = OMH_HTML_Tag::factory( $nav_link_args );
		
		$menu_item_tag->add_content( array( 'link' => $nav_link_tag ) );

		if( $mh_menu_item['children'] ) {

			$submenu_id = "sub-menu-{$menu_item_post->db_id}";
			$submenu_expanded = false;

			$menu_item_tag->set_attr( 'data-toggle', 'collapse' );
			$menu_item_tag->set_attr( 'data-target', "#{$submenu_id}" );
			$menu_item_tag->set_attr( 'aria-expanded', 'false' );

			$submenu_tag = OMH_HTML_Tag::factory(
				array(
					'type'		=> 'ul',
					'class'		=> 'collapse',
					'attrs'		=> array(
						'id'		=> $submenu_id
					)
				)
			);

			foreach( $mh_menu_item['children'] as $submenu_item_post ) {

				$submenu_item_tag = OMH_HTML_Tag::factory( $menu_item_defaults );

				$submenu_link_args = array(
					'attrs'	=> array(
						'href'	=> $submenu_item_post->url
					),
					'contents'	=> $submenu_item_post->post_title
				);

				$submenu_link_args = array_replace_recursive( $nav_link_defaults, $submenu_link_args );
				$submenu_link_tag = OMH_HTML_Tag::factory( $submenu_link_args );

				// Determine if submenu_item is active link
				if( isset( $submenu_item_post->object_id ) && ( $post->ID == $submenu_item_post->object_id ) ) {

					$submenu_expanded = true;
					$submenu_item_tag->add_class( 'current-page' );
					$menu_item_active = true;
				}

				$submenu_item_tag->add_content( array( 'link' => $submenu_link_tag ) );
				$submenu_tag->add_content( $submenu_item_tag );
			}

			if( $submenu_expanded ) {

				$submenu_tag->add_class( 'show' );
				$menu_item_tag->set_attr( 'aria-expanded', 'true' );

			}
			else {

				$menu_item_tag->add_class( 'collapsed' );
			}

			$menu_item_tag->add_content( $submenu_tag );
		}

		if( $menu_item_active ) {

			$menu_item_tag->add_class( 'current-page active' );
		}

		$menu_ui_element->add_content( $menu_item_tag );
	}

	return $menu_ui_element->render();
}

if( !function_exists( 'get_total_products' ) ) {
	function get_total_products() {

		$args = array(
			'post_type' => 'product',
		);

		$products = new WP_Query( $args );

		return $products->found_posts;
	}
}

if( !function_exists( 'get_completed_orders' ) ) {
	function get_completed_orders() {

		$args = array(
			'post_type'     => 'shop_order',
			'post_status'   => array( 'wc-processing', 'wc-completed' )
		);

		$orders = new WP_Query( $args );

		return $orders->found_posts;
	}
}

/**
 * TAXONOMY FUNCTIONS
 **/

/**
 * Get our custom taxonomy shop pages
 * 
 * @param 	string 	$product_cat
 * 
 * @return 	string
 */
 if( !function_exists( 'shop_url' ) ) {
	function shop_url( $product_cat = '', $product_tag = '' ) {

		$shop_link = '';

		if( $chapter = get_query_var( 'chapters', false ) ) {
			$shop_link = '/chapters/' . $chapter;
		} elseif( $organization = get_query_var( 'organizations' ) ) {
			$shop_link = '/organizations/' . $organization;
		} elseif( $college = get_query_var( 'colleges' ) ) {
			$shop_link = '/colleges/' . $college;
		} else {
			$shop_link = '/marketplace';
		}

		if( $product_cat ) {
			$shop_link .= '/category/' . $product_cat;
		} elseif( $product_tag ) {
			$shop_link .= '/tag/' . $product_tag;
		}

		return $shop_link;
	}
 }

/**
 * ATTRIBUTE FUNCTIONS
 **/

/**
 * Get or create a WC Attribute
 * 
 * @param 	int|string|array 	$wc_attribute
 * 
 * @return 	stdClass|bool
 */
if( !function_exists( 'omh_get_or_create_wc_attribute' ) ) {
	function omh_get_or_create_wc_attribute( $wc_attribute ) {

		if( is_numeric( $wc_attribute ) ) {
			return wc_get_attribute( $wc_attribute );
		} elseif( is_string( $wc_attribute ) ) {
			return wc_get_attribute( wc_attribute_taxonomy_id_by_name( $wc_attribute ) );
		} elseif( is_array( $wc_attribute ) 
			&& isset( $wc_attribute['slug'], $wc_attribute['name'] )
		) {

			// Get the list of current attributes to compare against
			$current_attributes = array_column( wc_get_attribute_taxonomies(), 'attribute_name' );

			// If the attribute is in our current list of attributes, return it, else create it
			if( in_array( $wc_attribute['slug'], $current_attributes ) ) {
				return wc_get_attribute( wc_attribute_taxonomy_id_by_name( $wc_attribute['slug'] ) );
			} else {
				$attribute = wc_create_attribute( $wc_attribute );
			}

			// If it didn't error out and isn't false (or ID of zero), return the attribute
			if( !is_wp_error( $attribute ) && $attribute ) {
				return wc_get_attribute( $attribute );
			}
		}

		return false;
	}
}

/**
 * Get or create a WC Attribute Term
 * 
 * If $term_slug isn't provided, it will slugify the $term_name
 * 
 * @param 	string 			$attribute_name 
 * @param 	string 			$term_name 
 * @param 	string|null 	$term_slug 
 * 
 * @return 	bool|WP_Term 	$attribute_term
 */
if( !function_exists( 'omh_get_or_create_wc_attribute_term' ) ) {
	function omh_get_or_create_wc_attribute_term( $attribute_name, $term_name, $term_slug = null ) {

		// Make sure to add 'pa_' to attribute names
		if( 0 !== strpos( $attribute_name, 'pa_' ) ) {
			$attribute_name = 'pa_' . $attribute_name;
		}

		// If the attribute doesn't exist, fail
		if( !omh_get_or_create_wc_attribute( $attribute_name ) ) {
			return false;
		}

		// If $term_slug isn't provided, create one using the $term_name
		if( !$term_slug ) {
			$term_slug = wc_sanitize_taxonomy_name( $term_name );
		}

		$term_slug = wc_sanitize_taxonomy_name( $term_slug );

		// Try to get the attribute by the slug
		$attribute_term = get_term_by( 'slug', $term_slug, $attribute_name, OBJECT );

		// If we don't have an attribute term, try to get it by the name
		if( false === $attribute_term ) {
			$attribute_term = get_term_by( 'name', $term_name, $attribute_name, OBJECT );
		}

		// If we still don't have it, we need to create it
		if( false === $attribute_term ) {

			$attribute_term = wp_insert_term( $term_name, $attribute_name, array( 'slug' => $term_slug ) );

			if( is_wp_error( $attribute_term ) ) {
				return false;
			}

			$attribute_term = get_term( $attribute_term['term_id'], $attribute_name, OBJECT );
		}

		return $attribute_term;
	}
}

if( !function_exists( 'omh_filter_wc_product_array' ) ) {
	function omh_filter_wc_product_array( $wc_products = array(), $filter_visible = true, $context_product = 0 ) {

		if( !$wc_products ) {
			return $wc_products;
		}

		$context_product = wc_get_product( $context_product );

		if( $filter_visible ) {
			$wc_products = array_filter( $wc_products, 'wc_products_array_filter_visible' );
		}

		// Run array_filter last to remove the empty values, if we have any
		$wc_product_ids = array_filter( array_map( 
			function( $wc_product ) use ( $context_product ) {

				// Just in case we were given an array of IDs or something foul, just in case...
				if( $wc_product = wc_get_product( $wc_product ) ) {

					// If we were given a context product, make sure to remove that from the array as well
					if( $context_product && $context_product === $wc_product->get_id() ) {
						return '';
					}

					return $wc_product->get_id();
				}

				return '';
			},
			$wc_products
		) );

		return $wc_product_ids;
	}
}

if( !function_exists( 'omh_get_order_items_by_product_id' ) ) {
	function omh_get_order_items_by_product_id( $product_id = null ) {

	    $order_items = array();

	    if( $product_id ) {
	        global $wpdb;

	        $query = "
	            SELECT wcoi.`order_item_id` AS 'ID'
	            FROM `{$wpdb->prefix}woocommerce_order_itemmeta` AS wcoi
	            WHERE wcoi.`meta_key` = '_product_id'
	            AND wcoi.`meta_value` = %s
	            GROUP BY `ID`;
	        ";

	        $order_item_ids = $wpdb->get_col( $wpdb->prepare( $query, $product_id ) );

	        $order_items = array_map( 'WC_Order_Factory::get_order_item', $order_item_ids );
	    }

	    return $order_items;
	}
}

if( !function_exists( 'omh_get_orders_by_product_id' ) ) {
	function omh_get_order_ids_by_product_id( $product_id, $return_type = 'ids', $limit = 20, $page = 0 ) {
		global $wpdb;

		if( !$product_id ) {
			return $return_type == 'count' ? 0 : array();
		}

		switch( $return_type ) {
			case 'count':
				$sql = "SELECT COUNT( DISTINCT `order_item`.`order_id` ) ";
				break;
			case 'ids':
			default:
				$sql = "SELECT DISTINCT `order_item`.`order_id` ";
				break;
		}

		$sql .= "FROM `{$wpdb->prefix}woocommerce_order_items` AS `order_item`
			LEFT JOIN `{$wpdb->prefix}woocommerce_order_itemmeta` AS `order_itemmeta`
				ON `order_item`.`order_item_id` = `order_itemmeta`.`order_item_id`
			LEFT JOIN `{$wpdb->posts}` AS `post`
				ON `order_item`.`order_id` = `post`.`ID`
			WHERE `post`.`post_type` = 'shop_order'
				AND `order_item`.`order_item_type` = 'line_item'
				AND `order_itemmeta`.`meta_key` = '_product_id'
				AND `order_itemmeta`.`meta_value` = %d
			ORDER BY `order_item`.`order_id` DESC
		";

		$prepare_args = array(
			$product_id
		);

		if( $limit && $limit != -1 ) {

			$limit_args = array(
				'%d',
				'%d'
			);

			$prepare_args[] = $page ?? 0;
			$prepare_args[] = $limit;

			$sql .= ' LIMIT ' . implode( ', ', $limit_args );
		}

		$prepared_sql = $wpdb->prepare( $sql, $prepare_args );

		if( $return_type == 'count' ) {
			$results = $wpdb->get_var( $prepared_sql );
		} else {
			$results = $wpdb->get_col( $prepared_sql );
		}

		return $results;
	}
}

/**
 * OMH Template Helper functions
 */

 if( !function_exists( 'is_chapter' ) ) {
	
	/**
	 * Return true if current request is a chapter taxonomy page
	 *
	 * @return boolean
	 */
	function is_chapter() {
		$term = get_queried_object();

		if( $term instanceof WP_Term 
			&& 'chapters' == $term->taxonomy 
		) {
			return true;
		}

		return false;
	}
}

if( !function_exists( 'is_organization' ) ) {
	
	/**
	 * Return true if current request is an organization taxonomy page
	 *
	 * @return boolean
	 */
	function is_organization() {
		$term = get_queried_object();

		if( $term instanceof WP_Term 
			&& 'organizations' == $term->taxonomy 
		) {
			return true;
		}

		return false;
	}
}

if( !function_exists( 'is_college' ) ) {
	
	/**
	 * Return true if current request is a college taxonomy page
	 *
	 * @return boolean
	 */
	function is_college() {
		$term = get_queried_object();

		if( $term instanceof WP_Term 
			&& 'colleges' == $term->taxonomy 
		) {
			return true;
		}

		return false;
	}
}
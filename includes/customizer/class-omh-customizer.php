<?php
defined( 'ABSPATH' ) || exit;

class OMH_Customizer {

	public function __construct() {

		// WooCommerce Store Notice Customizer
		add_action( 'customize_register', array( $this, 'wc_store_notice_customizer' ) );
	}

	public function wc_store_notice_customizer( $wp_customize ) {

		$wp_customize->add_setting(
			'woocommerce_demo_store_notice_dismissible',
			array(
				'default'				=> 'yes',
				'type'					=> 'options',
				'capability'			=> 'manage_woocommerce',
				'sanitize_callback'		=> 'wc_bool_to_string',
				'sanitize_js_callback'	=> 'wc_string_to_bool'
			)
		);

		$wp_customize->add_control(
			'woocommerce_demo_store_notice_dismissible',
			array(
				'label'		=> 'Notice is dismissible',
				'priority'	=> 100,
				'section'	=> 'woocommerce_store_notice',
				'settings'	=> 'woocommerce_demo_store_notice_dismissible',
				'type'		=> 'checkbox'
			)
		);
	}
}
new OMH_Customizer();
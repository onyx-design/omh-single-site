<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_List_Group extends OMH_HTML_UI_Element {

    /**
	 * @var 	string
	 */
	protected $bootstrap_class = 'list-group';

    /**
	 * @var 	string
	 */
    protected $type = 'div';
    
    /**
	 * @var 	array
	 */
	protected $custom_args = array(
        'text' => array(
            'required' => true
        ),
        'flush' => array(
            'default' => false
        ),
        'list' => array(
            'default' => false
        )
    );

    /**
	 * @var 	array
	 */
    protected $default_args = array(
        'type'
    );

    /**
	 * Set the text of the OMH_HTML_UI_List_Group
	 * 
	 * @param 	string 	$text
	 * 
	 * @return 	OMH_HTML_UI_List_Group
	 */
	public function set_text( $text ) {

		$this->text = $text;

		$this->add_content( array( 'text' => $text ) );

		return $this;
	}

	/**
	 * Get the text of the OMH_HTML_UI_List_Group
	 * 
	 * @return 	string
	 */
	public function get_text() {

		return $this->get_child( 'text' );
    }
    
    /**
	 * Get the text of the OMH_HTML_UI_List_Group
	 * 
	 * @param 	bool
     * 
     * @return 	OMH_HTML_UI_List_Group
	 */
    public function set_flush( $flush = false ) {

        $this->flush = $flush;

        $this->add_class( 'list-group-flush' );

        return $this;
    }

    /**
	 * Get the text of the OMH_HTML_UI_List_Group
	 * 
	 * @return 	bool
	 */
    public function get_flush() {

        return $this->flush;
    }

     /**
	 * Get the text of the OMH_HTML_UI_List_Group
	 * 
	 * @param 	bool
     * 
     * @return 	OMH_HTML_UI_List_Group
	 */
    public function set_list( $list = false ) {

        $this->list = $list;

        $this->set_type( $list ? 'ul' : 'div' );

        return $this;
    }

    /**
	 * Get the text of the OMH_HTML_UI_List_Group
	 * 
	 * @return 	bool
	 */
    public function get_list() {

        return $this->list;
    }

}
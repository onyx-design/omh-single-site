<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_Tag {

	/**
	 * @var 	string
	 */
	protected $type = 'div';

	/**
	 * @var 	array
	 */
	protected $attrs = array();

	/**
	 * @var 	array
	 */
	protected $contents = array();

	/**
	 * @var 	array|null
	 */
	protected $html = NULL;

	/**
	 * @var 	array
	 */
	const PROPERTIES = array( 'disabled', 'required', 'checked', 'readonly', 'selected', 'multiple' );

	/**
	 * @var 	array
	 */
	const ARGS_KEYS = array( 'type', 'class', 'style', 'attrs', 'contents' );

	/**
	 * Auto-construct
	 * 
	 * @param 	mixed 	$args
	 * 
	 * @return mixed object (OMH_HTML_Tag or subclass)
	 */
	public static function factory( $args = null ) {


		if( empty( $args ) ) {

			$classname = get_called_class();
		}
		else if ( is_array( $args ) ) {

			if( !isset( $args['type'] ) ) {

				$classname = get_called_class();
			}
			else if( is_a( $args['type'], 'OMH_HTML_Tag', true ) ) {

				$classname = $args['type'];
			}
			else {

				$classname = 'OMH_HTML_Tag';
			}
		}

		if( isset( $classname ) ) {

			$args = (array) $args;
			$args['type'] 	= $args['type'] ?? $classname;

			$args = new $classname( $args );
		}

		// if( empty( $args ) || ( is_array( $args ) ) ) {

		// 	if( empty( $args )
		// 		|| ( is_array( $args ) 
		// 			&& ( !isset( $args['type'] )
		// 				|| !is_a( $args['type'], 'OMH_HTML_Tag', true ) 
		// 			)
		// 		) 
		// 	) {

		// 		$classname = get_called_class();
		// 	}
		// 	else {
		// 	}
		// 	if( is_arra)
		// }
		// if( !( $args instanceof OMH_HTML_Tag ) ) {

		// 	if( is_array( $args ) 
		// 		&& isset( $args['type'] ) 
		// 	) {
				
		// 		$classname = is_a( $args['type'], 'OMH_HTML_Tag', true ) ? $args['type'] : 'OMH_HTML_Tag';
		// 	}
		// 	else if( 
		// 		empty( $args )
		// 		|| ( is_array( $args ) && !isset( $args['type'] ) )
		// 	) {

		// 		$classname = get_called_class();

		// 		$args = (array) $args;
		// 		$args['type'] = $classname;
		// 	}
		// 	else if( )

		// 	error_log( $classname );
		// 	error_log( get_called_class() );
		// 	error_log( print_r( $args, true ) );

		// 	if( isset( $classname ) ) {

		// 		$args = new $classname( $args );
		// 	}


		// }
		// if( is_array( $args )
		// 	&& isset( $args['type'] )
		// ) {

		// 	$classname = is_a( $args['type'], 'OMH_HTML_Tag', true ) ? $args['type'] : 'OMH_HTML_Tag';
			
		// 	$args = new $classname( $args );
		// }

		return $args;
	}

	/**
	 * Constructor
	 * 
	 * @param 	array 	$args
	 * 
	 * @return 	void
	 */
	protected function __construct( array $args ) { //dev:finish

		if( !isset( $args['type'] ) || is_a( $args['type'], 'OMH_HTML_Tag', true ) ) {

			$args['type'] = $this->type;
		}

		foreach( self::ARGS_KEYS as $arg_key ) {

			if( isset( $args[ $arg_key ] ) ) {

				call_user_func( array( $this, "set_{$arg_key}" ), $args[ $arg_key ] );
			}
		}

		if( property_exists( $this, 'custom_args' ) ) {

			foreach( $this->custom_args as $custom_arg => $schema ) {

				if( isset( $args[ $custom_arg ] ) && method_exists( $this, "set_{$custom_arg}" ) ) {

					call_user_func( array( $this, "set_{$custom_arg}" ), $args[ $custom_arg ] );
				}
			}
		}
	}

	/**
	 * Check whether this HTML Tag is self closing or not
	 * 
	 * @return 	boolean
	 */
	public function is_self_closing() {

		return in_array( $this->type, array( 'input', 'br', 'img' ) );
	}

	

	/**
	 * Get the type of HTML Tag
	 * 
	 * @return 	string
	 */
	protected function get_type() {

		return $this->type;
	}

	/**
	 * Set the type of HTML Tag
	 * 
	 * @param 	string 	$type 
	 * 
	 * @return 	string
	 */
	protected function set_type( $type = 'div' ) {

		if( !is_string( $type ) ) {

			// dev:improve Build Error Handling singleton class to throw these copy-pasted exceptions
			throw new \Exception( '$type must be a string, received: ' . print_r( $type, true ) );
		}

		$this->type = $type;

		return $this;
	}

	/**
	 * Check whether an attribute key is a property
	 * 
	 * @param  	string 		$attr
	 * 
	 * @return 	boolean
	 */
	protected function is_property( $attr ) {

		return in_array( $attr, self::PROPERTIES );
	}

	/**
	 * Get the attributes of the HTML Tag
	 * 
	 * @return 	array
	 */
	public function get_attrs( $return_raw = false ) {

		$attrs = $this->attrs;

		if ( !$return_raw ) {

			foreach( array( 'class', 'style' )  as $array_attr ) {

				if( $this->has_attr( $array_attr ) ) {

					$attrs[ $array_attr ] = $this->get_attr( $array_attr );
				}
			}
		}

		return $attrs;
	}

	/**
	 * Get an individual attribute (or property)
	 * 
	 * @param 	string 	$attr
	 * @param 	bool 	$return_raw 	return raw array value or string value (only applies to class and style)
	 * 
	 * @return 	mixed
	 */
	public function get_attr( $attr, $return_raw = false ) { //dev:finish

		if( !is_string( $attr ) ) {

			throw new \Exception( '$attr must be a string, received: ' . print_r( $attr, true ) );
		}
		else if( !$this->has_attr( $attr ) ) {

			return null;
		}
		else if ( !$return_raw && 'class' == $attr ) {

			return implode( ' ', array_keys( $this->attrs['class'] ) );
		}
		else if( !$return_raw && 'style' == $attr ) {

			$style_attr = null;

			if( $style_props = $this->get_attr( 'style', true ) ) {

				$style_attr = '';

				foreach( $style_props as $property => $value ) {

					$style_attr .= "{$property}:{$value};";
				}
			}

			return $style_attr;
		}
		else if( $this->is_property( $attr ) ) {

			return true;
		}
		else {

			return $this->attrs[ $attr ];
		}
	}

	/**
	 * Simple check if attr exists
	 * 
	 * @param 	string 	$attr
	 * 
	 * @return 	bool
	 */
	public function has_attr( string $attr ) {

		return isset( $this->attrs[ $attr ] );
	}

	/**
	 * Set multiple attributes (and/or properties)
	 * 
	 * @param 	array 	$attrs
	 * 
	 * @return 	array
	 */
	public function set_attrs( array $attrs ) {

		foreach( $attrs as $attr => $value ) {

			$this->set_attr( $attr, $value );
		}

		return $this;
	}

	/**
	 * Set single attribute (or property)
	 * 
	 * @param 	array 	$attr
	 * 
	 * @return 	mixed
	 */
	public function set_attr( string $attr, $value ) {

		if( !isset( $value ) || false === $value ) {

			unset( $this->attrs[ $attr ] );
		}
		else if( $this->is_property( $attr ) ) {

			if( isset( $this->attrs[ $attr ] ) && !$value ) {
				
				unset( $this->attrs[ $attr ] );
			}
			else if( !isset( $this->attrs[ $attr ] ) && $value ) {

				$this->attrs[ $attr ] = true;
			}
		}
		else if( 'class' == $attr || 'style' == $attr ) {

			call_user_func( array( $this, "set_{$attr}" ), $value );
		}
		else {

			//dev:improve - validate $attr string

			$this->attrs[ $attr ] = strval( $value );
		}
		
		return $this;
	}

	/**
	 * Remove single attribute (or property );
	 * 
	 * @param 	string 	$attr 
	 */
	public function remove_attr( string $attr ) {

		$this->set_attr( $attr, null );
	}

	/**
	 * Set class attribute
	 * 
	 * @param 	mixed 	$classes
	 * 
	 * @return mixed
	 */
	public function set_class( $classes ) {

		// if( empty( $classes ) ) {

		// 	return $this->set_attr( 'class', null );
		// }

		$classes = is_array( $classes ) ? $classes : explode( ' ', $classes );

		$this->attrs['class'] = array_fill_keys( array_unique( $classes ), true );

		return $this->attrs['class'];
	}

	/**
	 * Check if tag has class
	 * 
	 * @param 	string 	$class
	 * 
	 * @return 	bool
	 */
	public function has_class( string $class ) {

		return isset( $this->attrs['class'], $this->attrs['class'][ $class ] );
	}

	/**
	 * Add class(es) to class attr
	 * 
	 * @param 	mixed 	$classes
	 * 
	 * @return 	string
	 */
	public function add_class( $classes ) {

		// Maybe initialize class attr
		$this->attrs['class'] = isset( $this->attrs['class'] ) ? $this->attrs['class'] : array();

		$classes = is_array( $classes ) ? $classes : explode( ' ', $classes );

		foreach( $classes as $class ) {
			
			$this->attrs['class'][ $class ] = true;
		}

		return $this;
	}

	/**
	 * Remove class(es) from class attr
	 * 
	 * @param 	mixed 	$classes
	 */
	public function remove_class( $classes ) {

		if( $this->has_attr( 'class' ) ) {

			$this->attrs['class'] = array_diff_key( 
				$this->attrs['class'], 
				array_flip( (array) $classes ) 
			);
		}

		return $this;
	}

	public function toggle_class( $class, $state = null ) {

		if( !isset( $state ) ) {

			if( $this->has_class( $class ) ) {

				$this->remove_class( $class );
			}
			else {

				$this->add_class( $class );
			} 
		}
		else if( $state ) {

			$this->add_class( $class );
		}
		else {

			$this->remove_class( $class );
		}

		return $this;
	}

	/**
	 * Set style attribute
	 * 
	 * @param 	mixed 
	 * 
	 */
	public function set_style( $styles ) {

		if( empty( $styles ) ) {

			return $this->set_attr( 'style', null );
		}

		if( is_string( $styles ) ) {

			$css = array();

			foreach( explode( ';', $styles ) as $style_prop ) {

				$style_prop = explode( ':', $style_prop, 2 );

				if( 2 == count( $style_prop ) ) {

					$css[ trim( $style_prop[0] ) ] = $style_prop[1];
				}
			}

			$styles = $css;
		}

		if( is_array( $styles ) ) {

			$this->attrs['style'] = $styles;
		}

		return $this;
	}

	/**
	 * Set individual style attr css property
	 * 
	 * @param 	string 	$property
	 * @param 	string 	$value
	 */
	public function set_style_prop( string $property, string $value ) {

		if( empty( $value ) && $this->has_attr( 'style' ) ) {

			unset( $this->attrs['style'][ $property ] );
			return null;
		}

		// Maybe initialize style attr
		$this->attrs['style'] = $this->attrs['style'] ?: array();

		return $this->attrs['style'][ $property ] = $value;
	}

	/**
	 * Get contents
	 * 
	 * @return mixed
	 */
	public function get_contents() {

		return $this->is_self_closing() ? null : $this->contents;
	}

	/**
	 * Set (and initialize) contents
	 * 
	 * @param 	mixed 	$contents
	 * 
	 * 
	 */
	public function set_contents( $contents ) {

		if( !$this->is_self_closing() ) {

			$this->contents = array();

			$this->add_content( $contents );
		}

		return $this->get_contents();
	}

	/**
	 * Add content
	 * 
	 * @param 	mixed 	$content
	 * @param 	mixed 	$after 		true: append to end of the content
	 * 								false: prepend to beginning of the content
	 * 								index (int,str): append after index of given child
	 * 								
	 */
	public function add_content( $contents, $after = true, $move_existing_keys = false ) {

		if( !$this->is_self_closing() ) {

			// Initialize $contents into array of content items
			if( is_string( $contents ) 
				|| $contents instanceof OMH_HTML_Tag 
				|| ( is_array( $contents ) && isset( $contents['type'] ) )
			) {

				$contents = array( $contents );
			}

			// Initialize content items
			foreach( $contents as &$content ) {

				// Convert any compatible arrays into OMH_HTML_Tags
				if( !is_string( $content ) ) {

					$content = OMH_HTML_Tag::factory( $content );	
				}

				$content = ( $content instanceof OMH_HTML_Tag ) ? $content : strval( $content );
			}


			if( $after ) {

				if( is_bool( $after ) ) {

					$this->contents = array_merge( $this->contents, $contents );
				}
				else {



					// If after matches a key in the current contents array, insert new content after that key
					// NOTE: IF NEW $contents IS AN ASSOCIATIVE ARRAY, NEW KEYS WILL OVERWRITE OLD ONES NO MATTER WHAT
					$rebuilt_contents = array();
					$contents_added = false;

					foreach( $this->contents as $key => $content_item ) {

						$rebuilt_contents[ $key ] = $content_item;

						if( !$contents_added && $key == $after ) {

							// Add the new contents after the found index
							foreach( $contents as $new_key => $new_content_item ) {

								if( is_numeric( $new_key ) ) {

									$rebuilt_contents[] = $new_content_item;
								}
								else {

									
									if( $move_existing_keys && isset( $this->contents[ $new_key ] ) ) {
										// EXISTING NAMED CONTENT ITEMS WITH NAMES IN NEW CONTENTS WILL BE DELETED
										// THE NEW CONTENT ITEM WITH THAT KEY WILL BE PLACED IN THE NEW POSITION

										unset( $this->contents[ $new_key ] );
									}

									$rebuilt_contents[ $new_key ] = $new_content_item;
								}

							}

							$contents_added = true;
						}
					}

					// If new content hasn't been added (i.e. $after was not a key of $this->contents ), add it at the end
					if( !$contents_added ) {

						foreach( $contents as $new_key => $new_content_item ) {

							if( is_numeric( $new_key ) ) {

								$rebuilt_contents[] = $new_content_item;
							}
							else {

								
								if( $move_existing_keys && isset( $this->contents[ $new_key ] ) ) {
									// EXISTING NAMED CONTENT ITEMS WITH NAMES IN NEW CONTENTS WILL BE DELETED
									// THE NEW CONTENT ITEM WITH THAT KEY WILL BE PLACED IN THE NEW POSITION

									unset( $this->contents[ $new_key ] );
								}

								$rebuilt_contents[ $new_key ] = $new_content_item;
							}

						}
					}

					$this->contents = $rebuilt_contents;
				}
			}
			else {

				$this->contents = array_merge( $contents, $this->contents );
			}
		}
		
		return $this;
	}

	/**
	 * Remove content
	 * 
	 * @param 	string|int 	$index 
	 * @return 	bool
	 */
	public function remove_content( $index ) {

		if( $this->get_child( $index ) ) {

			if( is_string( $index ) ) {

				unset( $this->contents[ $index ] );

			} else if( is_int( $index ) ) {

				$child_index = 0;

				foreach( $this->get_contents() as $content_key => $content ) {

					if( $content instanceof OMH_HTML_Tag ) {

						if( $index === $child_index ) {

							unset( $this->contents[ $content_key ] );

							break;
						}
					}
				}
			}
		}

		return $this;
	}

	/**
	 * Get child element
	 * 
	 * @param 	mixed 	$index  	integer: get child by count index
	 * 								string: get child by string key reference
	 * 								falsey: get last child
	 * 
	 * @return mixed
	 */
	public function get_child( $index ) {

		$contents = $this->get_contents();

		if( $contents ) {

			if( is_string( $index ) ) {

				return $contents[ $index ] ?? null;
			}
			else if ( is_int( $index ) || empty( $index ) ) {

				/**
				 * Must do a manual counter because
				 * 	1) only OMH_HTML_Tag objects (and child classes) count (i.e. not text blocks)
				 * 	2) there could be associative keys that should still be counted as part of the index
				 */
				$child_index = 0;
				$last_child = null;

				foreach( $contents as $content ) {

					if( $content instanceof OMH_HTML_Tag ) {

						if( $index === $child_index ) {

							return $content;
						}
						else if( empty( $index ) ) {

							$last_child = $content;
						}

						$child_index++;
					}
				}

				return $last_child;
			}
		}

		return null;
	}

	/**
	 * Build html string of element (and children recursively)
	 */
	public function render() {

		$html = "<{$this->type}";

		// Build attributes and properties
		foreach( $this->get_attrs() as $attr => $value ) {

			$html .= $this->is_property( $attr ) ? " $attr" : " $attr=\"$value\"";
		}

		if( $this->is_self_closing() ) {

			$html .= " />";
			return $html;
		}

		$html .= ">";

		// Render and add contents
		foreach( $this->get_contents() as $content ) {

			$html .= ($content instanceof OMH_HTML_Tag ) ? $content->render() : $content;
		}

		$html .= "</{$this->type}>";
		
		$this->html = $html;

		return $this->html;
	}
	

	public function __toString() {

		return $this->render();
	}
}
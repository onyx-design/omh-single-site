<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Alert extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'alert';

	/**
	 * @var 	string
	 */
	protected $type = 'div';

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'text'	=> array(
			'required'	=> true,
		),
		'color'	=> array(
			'type'		=> 'UI Color',
			'default'	=> 'secondary'
		),
		'dismissible'	=> array(
			'type'		=> 'boolean',
			'default'	=> true
		)
	);

	/**
	 * @var 	string
	 */
	protected $text;

	protected $icon;

	/**
	 * @var 	bool
	 */
	protected $dismissible = TRUE;

	protected $default_args = array(
		'type'	=> 'div',
		'color'	=> 'secondary',
		'dismissible'	=> true,
		'contents'	=> array(
			'icon' => array(
				'type'	=> 'OMH_HTML_UI_Icon',
				'icon'	=> 'info'
			),
			'text' => 'Alert!'
		)
	);

	protected $default_icons = array(
		'primary'	=> 'check',
		'secondary'	=> 'question-circle',
		'success'	=> 'check',
		'danger'	=> 'exclamation-triangle',
		'warning'	=> 'exclamation-triangle',
		'info'		=> 'info',
		'light'		=> 'question-circle',
		'dark'		=> 'question-circle'
	);

	/**
	 * Set the text of the OMH_HTML_UI_Alert
	 * 
	 * @param 	string 	$text
	 * 
	 * @return 	OMH_HTML_UI_Alert
	 */
	public function set_text( $text ) {

		$this->text = $text;

		$this->add_content( array( 'text' => $text ) );

		return $this;
	}

	/**
	 * Get the text of the OMH_HTML_UI_Alert
	 * 
	 * @return 	string
	 */
	public function get_text() {

		// return $this->text;

		return $this->get_child( 'text' );
	}

	public function set_color( $color ) {

		parent::set_color( $color );

		$icon = isset( $this->default_icons[ $color ] ) ? $this->default_icons[ $color ] : '';

		$this->set_icon( $icon );

		return $this;
	}

	/**
	 * Get the color of the OMH_HTML_UI_Alert
	 * 
	 * @return 	string
	 */
	public function get_color() {

		return $this->color;
	}

	/**
	 * Get the icon of the OMH_HTML_UI_Alert
	 * 
	 * @param 	string 	$icon
	 * 
	 * @return 	OMH_HTML_UI_Alert
	 */
	public function set_icon( $icon ) {

		$this->icon = $icon;

		if( $icon ) {

			$this->add_content( array( 
				'icon' => array(
					'type'	=> 'OMH_HTML_UI_Icon',
					'icon'	=> $icon
				)
			) );
		} else {

			$this->remove_content('icon');

		}

		return $this;
	}

	/**
	 * Get the icon of the OMH_HTML_UI_Alert
	 * 
	 * @return 	OMH_HTML_UI_Icon
	 */
	public function get_icon() {

		return $this->get_child( 'icon' );
	}

	/**
	 * Set whether the OMH_HTML_UI_Alert is dismissible or not
	 * 
	 * @param 	bool 	$dismissible
	 * 
	 * @return 	OMH_HTML_UI_Alert
	 */
	public function set_dismissible( $dismissible = true ) {

		$this->dismissible = $dismissible;

		if( $dismissible ) {

			$this->add_content( array(
				'dismissible'	=> array(
					'type'			=> 'button',
					'class'			=> 'close',
					'attrs'			=> array(
						'aria-label'	=> 'Close',
						'data-dismiss'	=> 'alert',
					),
					'contents'		=> array(
						'text' => array(
							'type'			=> 'span',
							'attrs'			=> array(
								'aria-hidden'	=> 'true',
								'data-dismiss'	=> 'alert',
							),
							'contents'		=> '×'
						)
					)
				)
			) );
		} else {

			$this->remove_content( 'dismissible' );
		}

		return $this;
	}

	/**
	 * Get whether the OMH_HTML_UI_Alert is dismissible or not
	 * 
	 * @return 	bool
	 */
	public function get_dismissible() {

		return $this->dismissible;
	}
}
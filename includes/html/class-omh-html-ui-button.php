<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Button extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'btn';

	/**
	 * @var 	string
	 */
	protected $type = 'button';

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'label'	=> array(
			'type'		=> 'string',
			'required'	=> true,
		),
		'color'	=> array(
			'type'		=> 'UI Color',
			'default'	=> 'secondary',
		),
		'size'	=> array(
			'type'		=> 'UI Size',
		),
		'outline'	=> array(
			'type'		=> 'boolean',
			'default'	=> false,
		),
		'href'		=> array(),
		'onclick'	=> array(),
		'form'		=> array(),
		'value'		=> array(),
		'disabled'	=> array(
			'type'		=> 'boolean',
			'default'	=> false,
		),
		'popover'	=> array(
			'type'	=> 'array'
		)
	);

	/**
	 * @var 	string
	 */
	protected $label;

	/**
	 * @var 	bool
	 */
	protected $outline = FALSE;

	/**
	 * @var 	string
	 */
	protected $href;

	/**
	 * @var 	string
	 */
	protected $onclick;

	/**
	 * @var 	string
	 */
	protected $form;

	/**
	 * @var 	string
	 */
	protected $value;

	/**
	 * @var 	bool
	 */
	protected $disabled;

	/**
	 * @var 	OMH_HTML_UI_Element
	 */
	protected $dropdown;

	protected $popover;

	protected $default_args = array(
		'color'	=> 'secondary',
		'contents'	=> array(
			'label'	=> 'Button'
		)
	);

	public static function build( $label = 'Button', $color = 'secondary', $outline = false, $disabled = false, $href = null, $value = null, $onclick = null, $extra_args = array() ) {

		$args = array(
			'label'		=> $label,
			'color'		=> $color,
			'outline'	=> $outline,
			'disabled'	=> $disabled,
			'href'		=> $href,
			'value'		=> $value,
			'onclick'	=> $onclick
		);

		$args = array_merge( $args, $extra_args );

		return self::factory( $args );
	}

	/**
	 * Set the label of the OMH_HTML_UI_Button
	 * 
	 * @param 	string 	$label
	 * 
	 * @return 	OMH_HTML_UI_Button
	 */
	public function set_label( $label ) {

		$this->label = $label;

		$this->add_content( array( 'label' => $this->label ) );

		return $this;
	}

	/**
	 * Get the label of the OMH_HTML_UI_Button
	 * 
	 * @return 	string
	 */
	public function get_label() {

		// return $this->label;

		return $this->get_child( 'label' );
	}

	/**
	 * Get the color of the OMH_HTML_UI_Button
	 * 
	 * @return 	string
	 */
	public function get_color() {

		return $this->color;
	}

	/**
	 * Get the size of the OMH_HTML_UI_Button
	 * 
	 * @return 	string
	 */
	public function get_size() {

		return $this->size;
	}

	/**
	 * Set the outline of the OMH_HTML_UI_Button
	 * 
	 * @param 	bool 	$outline
	 * 
	 * @return 	OMH_HTML_UI_Button
	 */
	public function set_outline( $outline = FALSE ) {

		$this->outline = $outline;

		$this->set_color( 'outline-' . $this->get_color() );

		return $this;
	}

	/**
	 * Get the outline of the UI_Buton
	 * 
	 * @return 	bool
	 */
	public function get_outline() {

		return $this->outline;
	}

	/**
	 * Set the HREF of the OMH_HTML_UI_Button
	 * 
	 * @param 	string 	$href
	 * 
	 * @return 	OMH_HTML_UI_Button
	 */
	public function set_href( $href ) {

		$this->href = $href;

		// If we're setting HREF, then we're an anchor tag
		$this->set_type_anchor()
			->set_attr( 'href', $this->href );
		
		return $this;
	}

	/**
	 * Get the HREF of the OMH_HTML_UI_Button
	 * 
	 * @return 	string
	 */
	public function get_href() {

		return $this->href;
	}

	/**
	 * Set the onClick of the OMH_HTML_UI_Button
	 * 
	 * @param 	string 	$onclick
	 * 
	 * @return 	OMH_HTML_UI_Button
	 */
	public function set_onclick( $onclick ) {

		$this->onclick = $onclick;

		$this->set_attr('onclick', $onclick);

		return $this;
	}

	/**
	 * Get the onClick of the OMH_HTML_UI_Button
	 * 
	 * @return 	string
	 */
	public function get_onclick() {

		return $this->onclick;
	}

	/**
	 * Set the form of the OMH_HTML_UI_Button
	 * 
	 * @param 	string 	$form
	 * 
	 * @return 	OMH_HTML_UI_Button
	 */
	public function set_form( $form ) {

		$this->form = $form;

		// If we're setting Form, then we're a button tag
		$this->set_type_button()
			->set_attr( 'form', $this->form );		

		return $this;
	}

	/**
	 * Get the form of the OMH_HTML_UI_Button
	 * 
	 * @return 	string
	 */
	public function get_form() {

		return $this->form;
	}

	/**
	 * Set the value of the OMH_HTML_UI_Button
	 * 
	 * @param 	string 	$value
	 * 
	 * @return 	OMH_HTML_UI_Button
	 */
	public function set_value( $value ) {

		$this->value = $value;

		// If we're setting Value, then we're a button tag
		$this->set_type_button()
			->set_attr('value',$this->value);

		return $this;
	}

	/**
	 * Get the value of the OMH_HTML_UI_Button
	 * 
	 * @return 	string
	 */
	public function get_value() {

		return $this->value;
	}

	/**
	 * Set the disabled state of the OMH_HTML_UI_Button
	 * 
	 * @param 	bool 	$disabled
	 * 
	 * @return 	OMH_HTML_UI_Button
	 */
	public function set_disabled( $disabled ) {

		$this->disabled = $disabled;

		$this->set_attr( 'disabled', $this->disabled )
			->toggle_class( 'disabled', $this->disabled );

		return $this;

	}

	/**
	 * Get the disabled state of the OMH_HTML_UI_Button
	 * 
	 * @return 	bool
	 */
	public function get_disabled() {

		return $this->disabled;
	}

	/**
	 * Set the dropdown of the OMH_HTML_UI_Button
	 * 
	 * @param 	OMH_HTML_UI_Element 	$dropdown
	 * 
	 * @return 	OMH_HTML_UI_Button
	 */
	public function set_dropdown( $dropdown ) {

		$this->dropdown = $dropdown;

		return $this;
	}

	public function set_type_button() {

		$type_attr = $this->form ? 'submit' : 'button';

		$this->set_type( 'button' )
			->set_attr('type', $type_attr )
			->set_attr('role',null);

		return $this;
	}

	public function set_type_anchor() {

		$this->set_type( 'a' )
			->set_attr('type', null)
			->set_attr('role','button');

		return $this;
	}

	/**
	 * Get the dropdown of the OMH_HTML_UI_Button
	 * 
	 * @return 	OMH_HTML_UI_Element
	 */
	public function get_dropdown() {

		return $this->dropdown;
	}

	public function set_popover( $popover ) {

		$this->popover = $popover;

		return $this;
	}

	//dev:inmprove
	public function render() {

		if( isset( $this->popover ) ) {

			$popover_attrs = array(
				'data-toggle'	=> 'popover',
				'data-content'	=> $this->popover['content'],
				'data-trigger'	=> 'hover'
			);

			if( !empty( $this->popover['placement'] ) ) {
				$popover_attrs['data-placement'] = $this->popover['placement'];
			}

			if( !empty( $this->popover['trigger'] ) ) {
				$popover_attrs['data-trigger']	= $this->popover['trigger'];
			}

			if( $this->disabled ) {

				$popover_html = OMH_HTML_Tag::factory(
					array(
						'type'	=> 'span',
						'class'	=> 'd-inline-block',
						'attrs'	=> $popover_attrs,
						'contents'	=> parent::render()
					)
				);

				$this->html = $popover_html->render();

				return $this->html;
			}
			else {
				$this->set_attrs( $popover_attrs );
				return parent::render();
			}
		}
		else {
			return parent::render();
		}

	}

}
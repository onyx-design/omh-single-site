<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Input_Ajax_Search extends OMH_HTML_UI_Input {

	
	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'input_type' 	=> array(
			'required'		=> true,
			'default' 		=> 'text'
		),
		'disabled' 		=> array(
			'default' 		=> false
		),
		'size' 			=> array(
			'type' 			=> 'UI Size'
		),
		'placeholder' 	=> array(),		
		'form_text' 	=> array(),
		'invalid_feedback'=> array(),
		'match_field'	=> array(),
		'input_id' 		=> array(
			'required' 		=> true
		),
		'required'		=> array(
			'type'			=> 'boolean',
			'default'		=> null
		),
		'label' 		=> array(),
		'horizontal' 		=> array(
			'default' 	=> false
		),
		'accept'		=> array(),
		'value'			=> array(),
		'input_attrs'	=> array(),
		'input_class'	=> array(),
		'prepend'		=> array(),
		'append'		=> array(),
		'value_label' 			=> array(
			'default' 			=> ''
		),
		'search_model'		=> array(

		)
	);

	/**
	 * @var 	array
	 */
	protected $default_args = array(
		'type' 			=> 'div',
		'input_type' 	=> 'number',
		'disabled' 		=> false,
		'label' 		=> '',
		'horizontal' 	=> false,		
		'contents' 		=> array(
			'label' 			=> array(
				'type' 		=> 'label'
			),
			'input' 			=> array(
				'type' 		=> 'input',
				'class' 	=> 'form-control omh-field-input',
				'attrs'		=> array(
					'data-omh-field-type'	=> 'ajaxSearch'
				)
			),
			'form_text' 		=> array(
				'type' 		=> 'small',
				'class' 	=> 'form-text text-muted',
				'contents' 	=> array(
					'text' => ''
				)
			),
			'invalid_feedback' 	=> array(
				'type' 		=> 'div',
				'class' 	=> 'invalid-feedback',
				'contents' 	=> array(
					'text' => ''
				)
			)
		),
		'input_attrs'	=> array(
			'pattern'		=> false
		),
		'input_class'	=> '',
		'pattern'		=> false,
		'prepend'		=> null,
		'append'		=> null,
		'value_label'	=> null,
		'search_model'	=> null
	);

	protected $inherit_custom_args = true;

	protected $value_label;

	protected $search_model;

	public function should_have_input_group() {

		return true;
	}

	public function set_value_label( $value_label ) {

		$this->value_label = $value_label;

		$this->make_input_group();

		$input_group_tag = $this->get_tag_input_group();

		$input_group_tag->add_content(
			array(
				'value_label'	=> OMH_HTML_Tag::factory(
					array(
						'type'	=> 'input',
						'class'	=> 'form-control ajax-input-label',
						'attrs'	=> array(
							'value'	=> $this->value_label,
							'type'	=> 'text'
						)
					)
				),
				'ajax_search_results'	=> OMH_HTML_Tag::factory(
					array(
						'class'	=> 'ajax-search-results'
					)
				)
			)
		);

		return $this;
	}

	public function set_search_model( $search_model ) {

		$this->search_model = $search_model;

		$this->set_attr( 'data-omh-ajax-search-model', $this->search_model )
			->get_tag_input()->set_attr( 'data-omh-ajax-search-model', $this->search_model );

		return $this;
	}

}
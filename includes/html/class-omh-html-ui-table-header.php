<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Table_Header extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = '';

	/**
	 * @var 	string
	 */
	protected $type = 'th';

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'sortable'	=> array(
			'default'	=> false
		),
		'sorted'	=> array(
			'default'	=> false
		),
		'label'	=> array(
			'default'	=> 'Column'
		)
	);

	protected $default_args = array(
		'type'		=> 'th',
		'sortable'	=> false,
		'sorted'	=> false,
		'attrs'		=> array(
			'scope'		=> 'col'
		),
		'label'		=> 'Column'
	);


	protected $sortable = false;

	protected $sorted = false;

	protected $label = 'Column';

	/**
	 * Constructor!
	 * 
	 * @param 	array 	$args
	 * 
	 * @return 	void
	 */
	public function __construct( $args ) {

		parent::__construct( $args );

		$this->set_sortable();
	}
	
	public function get_label() {

		return $this->label;
	}

	public function set_label( $label ) {

		$this->label = $label;

		if( $button_tag = $this->get_tag_button() ) {

			$button_tag->set_label( $this->label );
		}

		return $this;
	}

	public function get_sorted() {

		return $this->sorted;
	}

	public function set_sorted( $sorted ) {

		$this->sorted = $sorted;

		$this->remove_class( array( 'sorted-desc', 'sorted-asc' ) );

		if( 'ASC' == $sorted || 'DESC' == $sorted ) {

			$this->add_class( 'sorted-' . strtolower( $sorted ) );
		}
		else {

			$this->add_class( 'sorted-none' );
		}

		return $this;
	}

	public function get_sortable() {

		return $this->sortable;
	}

	public function set_sortable( $sortable = null ) {

		$this->sortable = $sortable ?? $this->sortable;

		$this->toggle_class( 'sortable', $this->sortable );

		// $this->sortable = $sortable ?? $this->sortable;

		if( $this->get_sortable() ) {

			// $this->add_class( 'cell--btn' );

			// $this->sortable = $sortable;

			$this->get_tag_button( true );

			// $btn_classes = array( 'btn', 'btn-thead', 'btn-thead-sortable' );

			// if( $this->sorted ) {

			// 	$btn_classes[] = 'btn-thead-sorted-'. strtolower( $this->sorted );
			// }

			// $header_contents = array(
			// 	'type'		=> 'OMH_HTML_UI_Button',
			// 	'class' 	=> $btn_classes,
			// 	'contents'	=> array(
			// 		'label'		=> $this->get_label(),
			// 		'sorting'	=> array(
			// 			'type'		=> 'span',
			// 			'class'		=> 'field-sorting fa',
			// 		)
			// 	),
			// 	'color'		=> null
			// );
		}
		else {

			// $this->remove_class( 'cell--btn' );

			$this->add_content( array( 'label' => $this->get_label() ) );

			// $header_contents = $this->label;
		}

		return $this;

		// $this->set_contents( $header_contents );
	}

	public function get_tag_button( $auto_create = false ) {

		$button_tag = $this->get_child( 'button' );

		if( !$button_tag && $auto_create ) {

			$this->remove_content( 'label' );

			$this->add_class( 'cell--btn' );

			$this->add_content(
				array(
					'button'	=> OMH_HTML_UI_Button::factory( 
						array(
							'class' 	=> 'btn-thead btn--table-cell',
							'contents'	=> array(
								'label'		=> $this->get_label(),
								'sorting'	=> array(
									'type'		=> 'span',
									'class'		=> 'field-sorting',
								)
							),
							'color'		=> null
						)
					)
				)					
			);

			$button_tag = $this->get_child( 'button' );
		}

		return $button_tag;
	}

}
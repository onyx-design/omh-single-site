<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Table extends OMH_HTML_UI_Element {

	protected $type = 'div';

	protected $bootstrap_class = 'omh-table-wrap';

	protected $custom_args = array(
		'table_type'	=> array(
			'required'		=> true
		),
		'force_scope'	=> array(),
		// 'sections'		=> array(
		// 	'type'			=> 'array'
		// ),
		'table'			=> array(
			'type'			=> 'OMH_Table'
		),
		'load_table'	=> array(
			'type'			=> 'boolean',
			'default'		=> false
		),
		'default_filters'	=> array(
			'default'	=> null
		)
	);

	protected $table_type;

	protected $force_scope;

	protected $default_filters;

	// protected $sections = array();

	protected $table = null;

	protected $load_table = false;

	protected $default_args = array(
		'table'		=> null,
		'load_table'=> true,
		'contents'	=> array(
			'before' 	=> array(
				'type'		=> 'div',
				'class'		=> 'omh-table-section omh-table-section-before'
			),
			'search' 	=> array(
				'type'		=> 'div',
				'class'		=> 'omh-table-section omh-table-section-search'
			),
			'tabs' 	=> array(
				'type'		=> 'div',
				'class'		=> 'omh-table-section omh-table-section-tabs'
			),
			'no_results' 	=> array(
				'type'		=> 'div',
				'class'		=> 'omh-table-section omh-table-section-no_results'
			),
			'table_wrap' => array(
				'type'	=> 'div',
				'class'	=> 'omh-table-wrap-inner',
				'contents'	=> array(
					'table'		=> array(
						'type'		=> 'table',
						'class'		=> 'table omh-table',
						'contents'	=> array(
							'headers'	=> array(
								'type'	=> 'thead',
								'class'	=> 'omh-table-section omh-table-section-headers'
							),
							'results'	=> array(
								'type'	=> 'tbody',
								'class'	=> 'omh-table-section omh-table-section-results'
							)

						)
					)
				)
			),
			'pagination' 	=> array(
				'type'		=> 'div',
				'class'		=> 'omh-table-section omh-table-section-pagination'
			),
			'after' 	=> array(
				'type'		=> 'div',
				'class'		=> 'omh-table-section omh-table-section-after'
			)
		)
	);

	public function set_table_type( $table_type ) {

		$this->table_type = $table_type;

		$this->set_attr( 'data-table-type', $this->table_type );

		return $this;
	}

	public function set_force_scope( $force_scope ) {

		$this->force_scope = (array) $force_scope;

		$this->set_attr( 'data-force-scope', implode( ',', $this->force_scope ) );

		if( $this->table instanceof OMH_Table ) {

			$this->table->force_scope( $this->force_scope );
		}

		return $this;
	}

	public function set_table( $table ) {

		$this->table = $table;

		return $this;
	}

	public function set_default_filters( $default_filters ) {

		$this->default_filters = $default_filters;

		if( !empty( $this->default_filters ) ) {

			$this->set_attr( 'data-default-filters', htmlentities( json_encode( $this->default_filters ), ENT_QUOTES, 'UTF-8' ) );
		}

		return $this;
	}

	/**
	 * Determine whether table should be loaded in the backend, the frontend,
	 * or not at all (static)
	 *
	 * @param      <type>  $load_table  The load table
	 *
	 * @return     self    Returns $this for chaining
	 */
	public function set_load_table( $load_table ) {

		$this->load_table = $load_table;

		if( $this->load_table && $this->table instanceof OMH_Table ) {

			$this->remove_class('not-loaded')->add_class('loaded');

			$sections_html = $this->table->get_html();

			foreach( $sections_html as $section => $section_contents ) {

				$section_contents = empty( $section_contents ) ? '' : $section_contents;

				$this->set_section_contents( $section, $section_contents );				
			}
		}
		else {
			$this->remove_class('loaded')->add_class('not-loaded');
		}

		return $this;
	}

	public function get_tag_table() {

		return $this->get_child('table_wrap')->get_child('table');
	}

	public function set_section_contents( $section, $contents ) {

		$section_tag = $this->get_section( $section );

		if( $section_tag ) {

			$section_tag->set_contents( $contents );
		}

		return $this;
	}

	public function get_section( $section ) {

		if( in_array( $section, array( 'headers', 'results' ) ) ) {
			return $this->get_tag_table()->get_child( $section );
		}
		else {
			return $this->get_child( $section );
		}
	}
}
<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Select extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'form-group omh-field-wrap';

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'disabled' 		=> array(
			'default' 		=> false
		),
		'size' 			=> array(
			'type' 			=> 'UI Size'
		),
		'form_text' 	=> array(),
		'invalid_feedback'=> array(),
		'match_field'	=> array(),
		'input_id' 		=> array(
			'required' 		=> true
		),
		'required'		=> array(
			'type'			=> 'boolean',
			'default'		=> null
		),
		'label' 		=> array(),
		'horizontal' 		=> array(
			'default' 	=> false
		),
		'value'			=> array(),
		'input_attrs'	=> array(),
		// 'omh_field_type'=> array(),
		'input_class'	=> array(),
		'select_options'=> array()
	);

	/**
	 * @var 	array
	 */
	protected $default_args = array(
		'type' 			=> 'div',
		'input_type' 	=> 'text',
		'disabled' 		=> false,
		'label' 		=> '',
		'horizontal' 	=> false,		
		'contents' 		=> array(
			'label' 			=> array(
				'type' 		=> 'label'
			),
			'select' 			=> array(
				'type' 		=> 'select',
				'class' 	=> 'form-control omh-field-input',
				'attrs'		=> array(
					'data-omh-field-type'	=> 'select'
				)
			),
			'form_text' 		=> array(
				'type' 		=> 'small',
				'class' 	=> 'form-text text-muted',
				'contents' 	=> array(
					'text' => ''
				)
			),
			'invalid_feedback' 	=> array(
				'type' 		=> 'div',
				'class' 	=> 'invalid-feedback',
				'contents' 	=> array(
					'text' => ''
				)
			)
		),
		'input_attrs'	=> array(
			'pattern'		=> false
		),
		// 'omh_field_type'	=> 'base',
		'input_class'	=> '',
		'select_options'=> array()
	);

    /**
	 * @var 	string
	 */
	protected $type = 'div';

	/**
	 * @var 	string
	 */
	protected $value;

	/**
	 * @var 	bool
	 */
	protected $disabled;
	
	/**
	 * @var 	string
	 */
	protected $placeholder;

	/**
	 * @var 	string
	 */
	protected $form_text;

	/**
	 * @var 	string
	 */
	protected $invalid_feedback;

	/**
	 * @var 	string
	 */
	protected $input_id;

	/**
	 * @var 	string
	 */
	protected $label;

	/**
	 * @var 	array
	 */
	protected $input_attrs;

	protected $input_class;

	/**
	 * @var 	bool
	 */
	protected $required;

	protected $match_field;

	/**
	 * @var 	bool
	 */
	protected $horizontal;


	// protected $omh_field_type;

	protected $select_options;


	/**
	 * Get the input element of the form group
	 * 
	 * @return 	<input>
	 */
	public function get_tag_input() {

		$input_wrap = $this;

		// If horizontal, the input will be in the 'col' child
		if ( $this->horizontal ) {
			$input_wrap = $input_wrap->get_child('col');
		}

		// If there is an prepend or append, the input will be in the 'input_group' child
		$input_wrap = $input_wrap->get_child('input_group') ?? $input_wrap;

		return $input_wrap->get_child('select');
	}

	/**
	 * Get the label element of the form group
	 * 
	 * @return 	<label>
	 */
	protected function get_tag_label() {



		return $this->get_child('label');
	}

	/**
	 * Get the small.form-text of the form group
	 * 
	 * @return 	<small>
	 */
	protected function get_tag_form_text() {

		if ( $this->horizontal ) {
			return $this->get_child('col')->get_child('form_text');
		}
		return $this->get_child('form_text');
	}

	/**
	 * Get the div.invalid-feedbacl element of the form group
	 * 
	 * @return 	<div>
	 */
	protected function get_tag_invalid_feedback() {

		if ( $this->horizontal ) {
			return $this->get_child('col')->get_child('invalid_feedback');
		}
		return $this->get_child('invalid_feedback');
	}

	

	/**
	 * Set the size of the OMH_HTML_UI_Element
	 * 
	 * @param 	string 	$size
	 * 
	 * @return 	OMH_HTML_UI_Element
	 */
	public function set_size( $size ) {

		$this->size = $size;

		$input = $this->get_tag_input();

		$input->remove_class( $this->get_size_classes( true, 'form-control' ) );

		$input->add_class( $this->get_size_class( $size, 'form-control' ) );

		if( $this->should_have_input_group() 
			&& $input_group = $this->get_tag_input_group()
		) {

			$input_group->remove_class( $this->get_size_classes( true, 'input-group' ) );

			$input_group->add_class( $this->get_size_class( $size, 'input-group' ) );
		}

		return $this;
	}

	/**
	 * Set the placeholder attribute of the input
	 * 
	 * @param 	string 	$placeholder
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_placeholder( $placeholder ) {

		$input = $this->get_tag_input();
		$input->set_attr( 'placeholder', $placeholder );

		$this->placeholder = $placeholder;

		return $this;
	}

	/**
	 * Set the disabled attribute of the input
	 * 
	 * @param 	string 	$disabled
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_disabled( $disabled = false ) {

		$this->get_tag_input()->set_attr( 'disabled', $disabled );

		$this->disabled = $disabled;

		return $this;
	}
	
	/**
	 * Set the text of the small.form-text 
	 * 
	 * @param 	string 	$text
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_form_text( $text ) {

		$form_text = $this->get_tag_form_text();
		$form_text->set_contents( $text );

		$this->form_text = $text;

		return $this;
	}

	/**
	 * Set the text of the div.invalid-feedback
	 * 
	 * @param 	string 	$text
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_invalid_feedback( $text ) {

		$invalid_feedback = $this->get_tag_invalid_feedback();
		$invalid_feedback->set_contents( $text );

		$this->invalid_feedback = $text;

		return $this;
	}

	/**
	 * Set the text of the div.invalid-feedback
	 * 
	 * @param 	string 	$text
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_match_field( $match_field = null ) {

		$this->match_field = $match_field;

		$this->get_tag_input()->set_attr( 'data-match-field', $match_field );

		return $this;
	}

	/**
	 * Set the id of the input and wire it to the label
	 * 
	 * @param 	string 	$id
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_input_id( $id ) {

		$input = $this->get_tag_input();
		$input->set_attrs(
			array(
				'id' 	=> $id,
				'name'	=> $id
			)
		); // this maybe should be updated if need to differentiate [name] and [id] arises

		$label = $this->get_tag_label();
		$label->set_attr( 'for', $id );

		$this->input_id = $id;

		return $this;
	}

	/**
	 * Set the id of the input and wire it to the label
	 * 
	 * @param 	string 	$id
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_label( $label ) {

		$label_tag = $this->get_tag_label();

		

		if( false === $label ) { //dev:improve

			$this->remove_content( 'label' );
		}
		else {

			$label_tag = $this->get_tag_label();

			if( !$label_tag ) {

				$this->add_content( $label, false );
			}
			
			$this->get_tag_label()->set_contents( $label );

		}

		$this->label = $label;

		return $this;
	}

	/**
	 * Set the id of the input and wire it to the label
	 * 
	 * @param 	bool 	$horizontal
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_horizontal( $horizontal = false ) {

		if ( $horizontal ) {

			$this->add_class('row');

			$contents = array(
				'label' => array(
					'type' => 'label',
					'class' => 'col-sm-2 col-form-label',
					'contents' => array(
						'text' => $this->label
					)
				),
				'col' => array(
					'type' => 'div',
					'class' => 'col-sm-10',
					'contents' => array(
						'input' => $this->get_tag_input(),
						'form_text' => $this->get_tag_form_text(),
						'invalid_feedback' => $this->get_tag_invalid_feedback()
					)
				)
			);

			$this->set_contents( $contents );

		}

		$this->horizontal = $horizontal;

		return $this;
	}

	public function set_value( $value = '' ) {

		$this->value = $value;

		// $this->get_tag_input()->set_attr( 'value', $value );

		return $this;
	}

	public function set_required( $required = null ) {

		$this->required = $required;

		$this->get_tag_input()->set_attr( 'required', (bool) $required );

		return $this;
	}


	public function set_input_attrs( $input_attrs = array() ) {

		$this->input_attrs = $input_attrs;

		$input_tag = $this->get_tag_input();

		$input_tag->set_attrs( $input_attrs );

		return $this;
	}

	public function get_input_attrs() {

		return $this->input_attrs;
	}

	public function set_input_class( $input_class = '' ) {

		$this->input_class = $input_class;

		$input_tag = $this->get_tag_input();

		$input_tag->add_class( $this->input_class );

		return $this;
	}

	public function get_input_class() {

		return $this->input_class;
	}

	

	public function set_select_options( $select_options = array() ) {

		$this->select_options = $select_options;

		foreach( $this->select_options as $option_key => $option ) {

			$option_params 	= array(
				'type'	=> 'option'
			);

			// Allow shorthand [option_value] => [option_label] array 
			if( !is_array( $option ) ) {

				$option_value	= $option_key;
				$option_label 	= $option;
			}
			else {

				$option_value 	= $option['value'] ?? '';
				$option_label 	= $option['label'] ?? $option_value;
			}

			
			$selected 		= $option_value === $this->value ? 'selected' : null;

			$option_params 	= array(
				'type'		=> 'option',
				'value'		=> $option_value,
				'attrs'		=> array(
					'label'		=> $option_label,
					'value'		=> $option_value,
					'selected'	=> $selected 
				),
				'contents'	=> $option_label
			);

			if( is_array( $option ) ) {

				// Merge with raw $option in case other attrs/params were set 
				$option_params 	= array_merge_recursive( $option, $option_params );	
			}

			$this->get_tag_input()->add_content( OMH_HTML_Tag::factory( $option_params )	);
		}

		return $this;
	}
	

}
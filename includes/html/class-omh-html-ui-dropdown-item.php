<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Dropdown_Item extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $type = 'a';

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'dropdown-item';

	/**
	 * @var 	string
	 */
	protected $label;

	/**
	 * @var 	string
	 */
	protected $href;

	/**
	 * @var 	boolean
	 */
	protected $disabled;

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'href'	=> array(
			'default'	=> '#'
		),
		'label' => array(),
		'disabled'	=> array(
			'type'	=> 'boolean',
			'default'	=> false
		),
		'icon'	=> array()
	);

	/**
	 * @var 	array
	 */
	protected $default_args = array(
		'href'	=> '#',
		'label'	=> '',
		'disabled'	=> false,
		'icon'		=> false
	);

	public function set_href( $href ) {

		$this->href = $href;

		$this->set_attr( 'href', $this->href );

		return $this;
	}

	public function set_label( $label ) {
		$this->label = $label;

		$this->set_contents( $this->label );

		return $this;
	}

	public function set_disabled( $disabled ) {

		$this->disabled = $disabled;

		$this->set_attr( 'disabled', $this->disabled );

		if( $this->disabled ) {

			$this->add_class( 'disabled' );
		}
		else {

			$this->remove_class( 'disabled' );
		}

		return $this;

	}

	public function set_icon( $icon ) {

		$this->icon = $icon;

		if( !$this->icon ) {

			$this->remove_child( 'icon' );
		}
		else {

			$icon_tag = $this->get_child( 'icon' );

			if( !$icon_tag ) {

				$this->add_content(
					array(
						'icon'	=> OMH_HTML_UI_Icon::factory(
							array(
								'icon' => $this->icon
							)
						)
					),
					false
				);

				$icon_tag = $this->get_child( 'icon' );
			}

			$icon_tag->set_icon( $this->icon );

		}

		return $this;
	}

}
<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Nav_Item extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'nav-item';

	/**
	 * @var 	string
	 */
	protected $type = 'li';

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'name'		=> array(
			'required'	=> true
		),
		'destination'	=> array(),
		'active'	=> array(
			'type'		=> 'boolean',
			'default'	=> false
		)
	);

	protected $name;

	protected $destination;

	protected $active = FALSE;

	protected $default_args = array(
		'type'	=> 'li',
		'contents'	=> array(
			'link'		=> array(
				'type'		=> 'a',
				'class'		=> 'nav-link',
				'attrs'		=> array(
					'href'		=> '#'
				),
				'contents'	=> array(
					'text'		=> 'Tab 1'
				)
			)
		)
	);

	/**
	 * Constructor!
	 * 
	 * @param 	array 	$args
	 * 
	 * @return 	void
	 */
	// public function __construct( $args ) {

	// 	$args['type']	= self::CLASS;
	// }

	public function get_tag_link() {

		return $this->get_child( 'link' );
	}

	public function set_name( $name ) {

		$this->name = $name;

		$link = $this->get_tag_link();
		$link->add_content( array( 'text' => $name ) );

		return $this;
	}

	public function get_name() {

		return $this->name;
	}

	public function set_destination( $destination ) {

		$this->destination = $destination;

		$link = $this->get_tag_link();
		$link->set_attr( 'href', $destination );

		return $this;
	}

	public function set_active( $active = true ) {

		$this->active = $active;

		if( $active ) {

			$link = $this->get_tag_link();
			$link->add_class( 'active' );
		}

		return $this;
	}

	public function get_active() {

		return $this->active;
	}

}
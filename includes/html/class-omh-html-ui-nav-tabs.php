<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Nav_Tabs extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'nav nav-tabs';

	/**
	 * @var 	string
	 */
	protected $type = 'ul';

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'tabs'	=> array(
			'type'	=> 'array'
		),
		'active_tab'	=> array(
			'required'	=> true
		)
	);

	protected $tabs = array();

	protected $active_tab;

	protected $default_args = array(
		'type'	=> 'ul',
		'class'	=> 'nav nav-tabs',
		'tabs'	=> array()
	);

	protected $default_tab_args = array(
		'type'			=> 'OMH_HTML_UI_Nav_Item',
		'class'			=> 'nav-item',
		'active'		=> false
	);

	/**
	 * Constructor!
	 * 
	 * @param 	array 	$args
	 * 
	 * @return 	void
	 */
	// public function __construct( $args ) {

	// 	$args['type']	= self::CLASS;
	// }

	public function get_tag_item( $item ) {

		return $this->get_child( $item );
	}

	public function set_tabs( $tabs ) {

		$this->tabs = $tabs;

		$this->set_contents( '' );

		foreach( $tabs as $name => $tab_args ) {
			
			$active = ( $this->active_tab == $name ) ? true : false;

			$this->add_tab( $tab_args, $name );
		}

		return $this;
	}

	public function get_tabs() {

		$active_tab = $query_tab = empty( $_GET['tab'] ) ? null : $_GET['tab'];

		foreach( $this->tabs as $name => $destination ) {

			if( $active_tab == $name ) {

			}
		}

		return $this->tabs;
	}

	public function add_tab( $args, $name = null ) {

		$tab_args = array_replace_recursive( $this->default_tab_args, $args );

		$tab_item = $name ? array( $name => $tab_args ) : $tab_args;

		return $this->add_content( $tab_item );

		// return $this->add_content( array(
		// 	strtolower( $name ) 	=> array(
		// 		'type'			=> 'OMH_HTML_UI_Nav_Item',
		// 		'class'			=> 'nav-item',
		// 		'name'			=> $name,
		// 		'destination'	=> $destination,
		// 		'active'		=> $active,
		// 	)
		// ) );
	}

	public function set_active_tab( $active_tab ) {

		$this->active_tab = $active_tab;

		$item = $this->get_tag_item( $active_tab );
		$item->set_active( true );

		return $this;
	}

	public function get_active_tab() {

		return $this->active_tab;
	}
}
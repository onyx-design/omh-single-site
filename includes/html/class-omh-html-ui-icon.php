<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Icon extends OMH_HTML_UI_Element {

	protected $type = 'i';

	protected $bootstrap_class = 'fa';

	protected $custom_args = array(
		'icon'	=> array(
			'default'	=> 'info'
		)
	);

	protected $icon;

	protected $default_args = array(
		'icon'	=> 'info'
	);

	public function set_icon( $icon ) {

		foreach( (array) $this->get_attr( 'class', true ) as $class => $value ) {

			if( 0 === strpos( $class, 'fa-' ) ) {

				$this->remove_class( $class );
			}
		}

		$this->icon = $icon;

		$this->add_class( "fa-{$icon}" );

		return $this;
	}

	public function get_icon() {

		return $this->icon;
	}
}
<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Page_Item extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $type = 'li';

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'page-item';

	protected $label;

	/**
	 * @var 	string
	 */
	protected $page_num;

	protected $active = FALSE;

	protected $custom_args = array(
		'label'			=> array(
			'required'		=> true
		),
		'page_num'		=> array(
			'required'		=> true
		),
		'active'		=> array(
			'type'			=> 'boolean',
			'default'		=> false
		)
	);

	protected $default_args = array(
		'type'		=> 'li',
		'class'		=> 'omh-table-go-to-page',
		'contents'	=> array(
			'link'		=> array(
				'type'		=> 'a',
				'class'		=> 'page-link',
				'attrs'		=> array(
					'href'		=> '#'
				)
			)
		)
	);

	public function get_page_link() {

		return $this->get_child( 'link' );
	}

	public function set_label( $label ) {

		$this->label = $label;

		$link = $this->get_page_link();
		$link->add_content( 
			array(
				'label'	=> $label
			)
		);

		return $this;
	}

	public function get_label() {

		return $this->label;
	}

	public function set_page_num( $page_num ) {

		$this->page_num = $page_num;

		$link = $this->get_page_link();
		$link->set_attr( 'data-go-to-page', $page_num );

		return $this;
	}

	public function get_page_num() {

		return $this->page_num;
	}

	public function set_active( $active = true ) {

		$this->active = $active;

		if( $active ) {

			$this->add_class( 'active' );
		}

		return $this;
	}

	public function get_active() {

		return $this->active;
	}
}
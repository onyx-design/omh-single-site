<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Element extends OMH_HTML_Tag {

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = '';

	protected $color;

	/**
	 * @var 	array|null
	 */
	protected $color_classes = NULL;

	protected $size;

	protected $size_classes = NULL;

	const COLORS = array( 'primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark' );

	const SIZES = array( 'large' => 'lg', 'small' => 'sm' );

	/**
	 * Constructor!
	 * 
	 * @return 	void
	 */
	protected function __construct( $args ) {

		if( property_exists( $this, 'default_args' ) ) {

			$args = array_replace_recursive( $this->default_args, $args );
		}

		parent::__construct( $args );

		// Automatically add the bootstrap_class
		if( $this->bootstrap_class ) {

			$this->add_class( $this->bootstrap_class );
		}
	}

	/**
	 * Get the color class of the given color
	 * 
	 * @param 	string 	$color
	 * @param 	string|null 	$prefix
	 * 
	 * @return 	string
	 */
	public function get_color_class( $color, $prefix = null ) {

		$prefix = $prefix ?? $this->bootstrap_class;

		return "{$prefix}-{$color}";
	}

	/**
	 * Get the color classes of all colors
	 * 
	 * @param 	bool 	$force_refresh
	 * 
	 * @return 	array
	 */
	public function get_color_classes( $force_refresh = false ) {

		if( $force_refresh || !isset( $this->color_classes ) ) {

			$this->color_classes = array();

			foreach( self::COLORS as $color ) {

				$this->color_classes[] = $this->get_color_class( $color );
			}
		}

		return $this->color_classes;
	}

	/**
	 * Set the color of the OMH_HTML_UI_Element
	 * 
	 * @param 	string 	$color
	 * 
	 * @return 	OMH_HTML_UI_Element
	 */
	public function set_color( $color ) {

		$this->color = $color;

		$this->remove_class( $this->get_color_classes() );

		$this->add_class( $this->get_color_class( $color ) );

		return $this;
	}

	/**
	 * Get the size class of the given size
	 * 
	 * @param 	string 	$size
	 * @param 	bool 	$prefix
	 * 
	 * @return 	string
	 */
	public function get_size_class( $size, $prefix = null ) {

		$prefix = $prefix ?? $this->bootstrap_class;

		$size = isset( self::SIZES[ $size ] ) ? self::SIZES[ $size ] : $size;

		return "{$prefix}-{$size}";
	}

	/**
	 * Get the size classes of all sizes
	 * 
	 * @param 	bool 	$force_refresh
	 * 
	 * @return 	array
	 */
	public function get_size_classes( $force_refresh = false, $prefix = null ) {

		if( $force_refresh || !isset( $this->size_classes ) ) {

			$this->size_classes = array();

			foreach( self::SIZES as $size ) {

				$this->size_classes[] = $this->get_size_class( $size, $prefix );
			}
		}

		return $this->size_classes;
	}

	/**
	 * Set the size of the OMH_HTML_UI_Element
	 * 
	 * @param 	string 	$size
	 * 
	 * @return 	OMH_HTML_UI_Element
	 */
	public function set_size( $size ) {

		$this->size = $size;

		$this->remove_class( $this->get_size_classes() );

		$this->add_class( $this->get_size_class( $size ) );

		return $this;
	}

}
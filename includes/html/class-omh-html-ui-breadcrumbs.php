<?php
defined( 'ABSPATH' ) || exit;

class OMH_HTML_UI_Breadcrumbs extends OMH_HTML_UI_Element {

	protected $type = 'nav';

	protected $crumbs;

	protected $custom_args = array(
		'crumbs'	=> array(
			'type'		=> 'array',
		),
	);

	protected $default_args = array(
		'type'		=> 'nav',
		'attrs'		=> array(
			'aria-label'	=> 'breadcrumb'
		),
		'contents'	=> array(
			'crumbs'	=> array(
				'type'		=> 'ol',
				'class'		=> 'breadcrumb'
			)
		)
	);

	protected $default_crumb_args = array(
		'type'		=> 'OMH_HTML_UI_Crumb',
		'class'		=> 'breadcrumb-item',
		'active'	=> false
	);

	protected function get_crumbs_tag() {

		$crumbs = $this->get_child( 'crumbs' );

		return $crumbs;
	}

	protected function get_crumb( $crumb ) {

		return $this->get_child( $crumb );
	}

	protected function set_crumbs( $crumbs ) {

		$this->crumbs = $crumbs;

		$this->set_contents( '' );

		foreach( $crumbs as $crumb => $crumb_args ) {

			$active = ( $this->active_crumb == $crumb ) ? true : false;

			$this->add_crumb( $crumb_args, $crumb );
		}
	}

	public function get_crumbs() {

		return $this->crumbs;
	}

	public function add_crumb( $args, $name = null ) {

		$crumb_args = array_replace_recursive( $this->default_crumb_args, $args );

		$crumb = $name ? array( $name => $crumb_args ) : $crumb_args;

		$this->get_crumbs_tag()->add_content( $crumb );

		return $this;
	}

	public function set_active_crumb( $active_crumb ) {

		$this->active_crumb = $active_crumb;

		$page = $this->get_crumb( $active_crumb );
		$active_crumb->set_active( true );

		return $this;
	}

	public function get_active_crumb() {

		return $this->active_crumb;
	}
}
<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Pagination extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $type = 'ul';

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'pagination';

	/**
	 * @var 	array
	 */
	protected $pages;

	/**
	 * @var 
	 */
	protected $active_page;

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'pages' 	=> array(
			'type'			=> 'array'
		),
		'size'			=> array()
	);

	/**
	 * @var 	array
	 */
	protected $default_args = array(
		'type' 			=> 'nav',
		'class'			=> 'pagination',
		'pages'			=> array(),
	);

	protected $default_page_args = array(
		'type'			=> 'OMH_HTML_UI_Page_Item',
		'class'			=> 'page-item',
		'active'		=> false
	);

	protected function get_page_item( $item ) {

		return $this->get_child( $item );
	}

	protected function set_pages( $pages ) {

		$this->pages = $pages;

		$this->set_contents( '' );

		foreach( $pages as $name => $page_args ) {

			$active = ( $this->active_page == $name ) ? true : false;

			$this->add_page( $page_args, $name );
		}
	}

	public function get_pages() {

		return $this->pages;
	}

	public function add_page( $args, $name = null ) {

		$page_args = array_replace_recursive( $this->default_page_args, $args );

		$page_item = $name ? array( $name => $page_args ) : $page_args;

		return $this->add_content( $page_item );
	}

	public function set_active_page( $active_page ) {

		$this->active_page = $active_page;

		$page = $this->get_page_item( $active_page );
		$item->set_active( true );

		return $this;
	}

	public function get_active_page() {

		return $this->active_page;
	}
}
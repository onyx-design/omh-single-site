<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Dropdown extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $type = 'div';

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'btn-group';

	/**
	 * @var 	string
	 */
	protected $label;

	/**
	 * @var 	string
	 */
	protected $color;

	/**
	 * @var 	string/array
	 */
	protected $button_class;

	/**
	 * @var 	boolean
	 */
	protected $disabled;

	/**
	 * @var 	array
	 */
	protected $menu_items;

	/**
	 * @var 	UI_Button
	 */
	protected $split_button;

	/**
	 * @var 	UI_Size
	 */
	protected $size;

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'label' => array(
			'default'	=> ''
		),
		'color' => array(
			'type' 		=> 'UI Color',
			'default'	=> 'secondary'
		),
		'button_class'	=> array(),
		'split_button'	=> array(
			'type'		=> 'OMH_HTML_UI_Button'
		),
		'disabled'		=> array(
			'type'			=> 'boolean',
			'default'		=> false
		),
		'menu_items' => array(
			'type'		=> 'array'
		),
		'size'			=> array()
	);

	/**
	 * @var 	array
	 */
	protected $default_args = array(
		'type' => 'div',
		'label'	=> '',
		'color'	=> 'light',
		'button_class'	=> '',
		'split_button'	=> false,
		'disabled'	=> false,
		'menu_items'	=> array()
	);
	

	/**
	 * Get the dropdown button element
	 * 
	 * @return 	UI_Button
	 */
	protected function get_dropdown_button_tag() {

		$dropdown_button = $this->get_child('dropdown_button');

		// Maybe initialize dropdown button
		if( !$dropdown_button ) {

			$this->add_content( 
				array(
					'dropdown_button' => OMH_HTML_UI_Button::factory(
						array(
							'label'	=> $this->label,
							'color'	=> $this->color,
							'class'	=> 'dropdown-toggle',
							'attrs'	=> array(
								'data-toggle'	=> 'dropdown',
								'aria-haspopup'	=> 'true',
								'aria-expanded'	=> 'false'
							)
						)
					)
				),
				false
			);
		}

		return $this->get_child('dropdown_button');
	}

	/**
	 * Set the dropdown button color
	 * 
	 * @return OMH_HTML_UI_Dropdown
	 */
	public function set_color( $color ) {

		$this->color = $color;

		$this->get_dropdown_button_tag()->set_color( $this->color );

		return $this;
	}

	/**
	 * Set the dropdown button label
	 * 
	 * @return OMH_HTML_UI_Dropdown
	 */
	public function set_label( $label ) {

		$this->label = $label;

		$this->get_dropdown_button_tag()->set_label( $this->label );

		return $this;
	}

	/**
	 * Set the button class option
	 * 
	 * @return OMH_HTML_UI_Dropdown
	 */
	public function set_button_class( $button_class ) {

		$this->button_class = $button_class;

		$this->get_dropdown_button_tag()->add_class( $this->button_class );

		return $this;
	}

	/**
	 * Enable / Disable the dropdown button
	 * 
	 * @return OMH_HTML_UI_Dropdown
	 */
	public function set_disabled( $disabled ) {

		$this->disabled = $disabled;

		$this->get_dropdown_button_tag()->set_disabled( $this->disabled );

		return $this;
	}

	/**
	 * Set the size of the dropdown (buttons)
	 * 
	 * @return OMH_HTML_UI_Dropdown
	 */
	public function set_size( $size ) {

		$this->size = $size;

		$this->get_dropdown_button_tag()->set_size( $this->size );

		// Add sizing for split menu button

		return $this;
	}

	public function get_size() {
		return $this->size;
	}

	/**
	 * Get/Initialize dropdown menu
	 * 
	 * @return OMH_HTML_Tag
	 */
	protected function get_dropdown_menu_tag() {

		$dropdown_menu_tag = $this->get_child('dropdown_menu');

		if( !$dropdown_menu_tag ) {

			$this->add_content( 
				array(
					'dropdown_menu' => OMH_HTML_Tag::factory(
						array(
							'type'	=> 'div',
							'class'	=> 'dropdown-menu'
						)
					)
				),
				true
			);
		}

		return $this->get_child('dropdown_menu');
	}

	/**
	 * Build / Set the dropdown menu items
	 * 
	 * @return OMH_HTML_UI_Dropdown
	 */
	public function set_menu_items( $menu_items ) {

		$this->menu_items = (array) $menu_items;

		//$this->get_dropdown_menu_tag()->set_contents( $this->menu_items );

		$dropdown_menu = $this->get_dropdown_menu_tag();

		foreach( $this->menu_items as $menu_item_name => $menu_item ) {
			
			$dropdown_menu->add_content(
				array(
					$menu_item_name => OMH_HTML_UI_Dropdown_Item::factory(
						(array) $menu_item
					)
				)
			);
		}
		
		return $this;
	}

	/**
	 * Add a menu item
	 * 
	 * @return OMH_HTML_UI_Dropdown
	 */
	public function add_menu_item( $menu_item ) {

		$menu_item = (array) $menu_item;		

		foreach( $menu_item as $menu_item_name => $menu_item_args ) {

			$menu_item[ $menu_item_name ] = OMH_HTML_UI_Dropdown_Item::factory( $menu_item_args );
		}

		$this->get_dropdown_menu_tag()->add_content( $menu_item );

		return $this;
	}

	public function remove_menu_item( $menu_item_name ) {

		$this->get_dropdown_menu_tag()->remove_content( $menu_item_name );

		return $this;
	}

}















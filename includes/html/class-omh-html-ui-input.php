<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Input extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'form-group omh-field-wrap';

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'input_type' 	=> array(
			'required'		=> true,
			'default' 		=> 'text'
		),
		'disabled' 		=> array(
			'default' 		=> false
		),
		'size' 			=> array(
			'type' 			=> 'UI Size'
		),
		'placeholder' 	=> array(),		
		'form_text' 	=> array(),
		'invalid_feedback'=> array(),
		'match_field'	=> array(),
		'input_id' 		=> array(
			'required' 		=> true
		),
		'required'		=> array(
			'type'			=> 'boolean',
			'default'		=> null
		),
		'label' 		=> array(),
		'horizontal' 		=> array(
			'default' 	=> false
		),
		'accept'		=> array(),
		'value'			=> array(),
		'input_attrs'	=> array(),
		// 'omh_field_type'=> array(),
		'input_class'	=> array(),
		'prepend'		=> array(),
		'append'		=> array()
	);

	/**
	 * @var 	array
	 */
	protected $default_args = array(
		'type' 			=> 'div',
		'input_type' 	=> 'text',
		'disabled' 		=> false,
		'label' 		=> '',
		'horizontal' 	=> false,		
		'contents' 		=> array(
			'label' 			=> array(
				'type' 		=> 'label'
			),
			'input' 			=> array(
				'type' 		=> 'input',
				'class' 	=> 'form-control omh-field-input',
			),
			'form_text' 		=> array(
				'type' 		=> 'small',
				'class' 	=> 'form-text text-muted',
				'contents' 	=> array(
					'text' => ''
				)
			),
			'invalid_feedback' 	=> array(
				'type' 		=> 'div',
				'class' 	=> 'invalid-feedback',
				'contents' 	=> array(
					'text' => ''
				)
			)
		),
		// 'omh_field_type'	=> 'base',
		'input_attrs'	=> array(
			'pattern'		=> false
		),
		'input_class'	=> '',
		'pattern'		=> false,
		'prepend'		=> null,
		'append'		=> null
	);

    /**
	 * @var 	string
	 */
	protected $type = 'div';
	
	/**
	 * @var 	string
	 */
	protected $input_type = 'text';

	/**
	 * @var 	string
	 */
	protected $value;

	/**
	 * @var 	bool
	 */
	protected $disabled;
	
	/**
	 * @var 	string
	 */
	protected $placeholder;

	/**
	 * @var 	string
	 */
	protected $form_text;

	/**
	 * @var 	string
	 */
	protected $invalid_feedback;

	/**
	 * @var 	string
	 */
	protected $input_id;

	/**
	 * @var 	string
	 */
	protected $label;

	/**
	 * @var 	string
	 */
	protected $accept;

	/**
	 * @var 	array
	 */
	protected $input_attrs;

	protected $input_class;

	/**
	 * @var 	bool
	 */
	protected $required;

	protected $prepend;

	protected $append;

	protected $match_field;

	protected $tag_input_group = null;

	/**
	 * @var 	bool
	 */
	protected $horizontal;

	protected $field_type_map = array(
		'checkbox'		=> 'checkbox',
		'file'			=> 'file',
		'datepicker'	=> 'datePicker',
		'ajaxfile'		=> 'ajaxFile'
	);

	public static function build( $input_id = null, $label = '', $value = '', $input_type = 'text', $required = null, $extra_args = array() ) {

		$args = array(
			'input_id'	=> $input_id,
			'label'		=> $label,
			'input_type'=> $input_type,
			'value'		=> $value,
			'required'	=> $required
		);

		$args = array_merge( $args, $extra_args );

		return self::factory( $args );
	}

	/**
	 * Get the input element of the form group
	 * 
	 * @return 	<input>
	 */
	public function get_tag_input() {

		$input_wrap = $this;

		// If horizontal, the input will be in the 'col' child
		if ( $this->horizontal ) {
			$input_wrap = $input_wrap->get_child('col');
		}

		// If there is an prepend or append, the input will be in the 'input_group' child
		$input_wrap = $input_wrap->get_child('input_group') ?? $input_wrap;

		return $input_wrap->get_child('input');
	}

	/**
	 * Get the label element of the form group
	 * 
	 * @return 	<label>
	 */
	protected function get_tag_label() {



		return $this->get_child('label');
	}

	/**
	 * Get the small.form-text of the form group
	 * 
	 * @return 	<small>
	 */
	protected function get_tag_form_text() {

		if ( $this->horizontal ) {
			return $this->get_child('col')->get_child('form_text');
		}
		return $this->get_child('form_text');
	}

	/**
	 * Get the div.invalid-feedbacl element of the form group
	 * 
	 * @return 	<div>
	 */
	protected function get_tag_invalid_feedback() {

		if ( $this->horizontal ) {
			return $this->get_child('col')->get_child('invalid_feedback');
		}
		return $this->get_child('invalid_feedback');
	}

	
	
	/**
	 * Set the type attribute of the input
	 * 
	 * @param 	string 	$input_type
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_input_type( $input_type ) {


		if( 'datepicker' == $input_type ) {

			$this->get_tag_input()
				// ->set_attr('data-omh-field-type', 'datePicker')
				->add_class('omh_datepicker');
		}

		$input = $this->get_tag_input()->set_attr( 'type', $input_type );

		if ( $input_type == 'file' ) {
			$this->get_tag_input()
				// ->set_attr('data-omh-field-type', 'file')
				->remove_class('form-control')
				->add_class('form-control-file custom-file-input');
		} else {
			$this->get_tag_input()
				->remove_class('form-control-file')
				->add_class('form-control');
		}

		// Set data-omh-field-type attr
		$frontend_input_type = $this->field_type_map[ $input_type ] ?? 'base';
		$this->get_tag_input()->set_attr('data-omh-field-type', $frontend_input_type );

		$this->input_type = $input_type;

		return $this;
	}

	/**
	 * Set the size of the OMH_HTML_UI_Element
	 * 
	 * @param 	string 	$size
	 * 
	 * @return 	OMH_HTML_UI_Element
	 */
	public function set_size( $size ) {

		$this->size = $size;

		$input = $this->get_tag_input();

		$input->remove_class( $this->get_size_classes( true, 'form-control' ) );

		$input->add_class( $this->get_size_class( $size, 'form-control' ) );

		if( $this->should_have_input_group() 
			&& $input_group = $this->get_tag_input_group()
		) {

			$input_group->remove_class( $this->get_size_classes( true, 'input-group' ) );

			$input_group->add_class( $this->get_size_class( $size, 'input-group' ) );
		}

		return $this;
	}

	/**
	 * Set the placeholder attribute of the input
	 * 
	 * @param 	string 	$placeholder
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_placeholder( $placeholder ) {

		$input = $this->get_tag_input();
		$input->set_attr( 'placeholder', $placeholder );

		$this->placeholder = $placeholder;

		return $this;
	}

	/**
	 * Set the disabled attribute of the input
	 * 
	 * @param 	string 	$disabled
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_disabled( $disabled = false ) {

		$this->get_tag_input()->set_attr( 'disabled', $disabled );

		$this->disabled = $disabled;

		return $this;
	}
	
	/**
	 * Set the text of the small.form-text 
	 * 
	 * @param 	string 	$text
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_form_text( $text ) {

		$form_text = $this->get_tag_form_text();
		$form_text->set_contents( $text );

		$this->form_text = $text;

		return $this;
	}

	/**
	 * Set the text of the div.invalid-feedback
	 * 
	 * @param 	string 	$text
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_invalid_feedback( $text ) {

		$invalid_feedback = $this->get_tag_invalid_feedback();
		$invalid_feedback->set_contents( $text );

		$this->invalid_feedback = $text;

		return $this;
	}

	/**
	 * Set the text of the div.invalid-feedback
	 * 
	 * @param 	string 	$text
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_match_field( $match_field = null ) {

		$this->match_field = $match_field;

		$this->get_tag_input()->set_attr( 'data-match-field', $match_field );

		return $this;
	}

	/**
	 * Set the id of the input and wire it to the label
	 * 
	 * @param 	string 	$id
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_input_id( $id ) {

		$input = $this->get_tag_input();
		$input->set_attrs(
			array(
				'id' 	=> $id,
				'name'	=> $id
			)
		); // this maybe should be updated if need to differentiate [name] and [id] arises

		$label = $this->get_tag_label();
		$label->set_attr( 'for', $id );

		$this->input_id = $id;

		return $this;
	}

	/**
	 * Set the id of the input and wire it to the label
	 * 
	 * @param 	string 	$id
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_label( $label ) {

		$label_tag = $this->get_tag_label();

		

		if( false === $label ) { //dev:improve

			$this->remove_content( 'label' );
		}
		else {

			$label_tag = $this->get_tag_label();

			if( !$label_tag ) {

				$this->add_content( $label, false );
			}
			
			$this->get_tag_label()->set_contents( $label );

		}

		$this->label = $label;

		return $this;
	}

	/**
	 * Set the id of the input and wire it to the label
	 * 
	 * @param 	bool 	$horizontal
	 * 
	 * @return 	OMH_HTML_UI_Input
	 */
	public function set_horizontal( $horizontal = false ) {

		if ( $horizontal ) {

			$this->add_class('row');

			$contents = array(
				'label' => array(
					'type' => 'label',
					'class' => 'col-sm-2 col-form-label',
					'contents' => array(
						'text' => $this->label
					)
				),
				'col' => array(
					'type' => 'div',
					'class' => 'col-sm-10',
					'contents' => array(
						'input' => $this->get_tag_input(),
						'form_text' => $this->get_tag_form_text(),
						'invalid_feedback' => $this->get_tag_invalid_feedback()
					)
				)
			);

			$this->set_contents( $contents );

		}

		$this->horizontal = $horizontal;

		return $this;
	}

	public function set_value( $value = '' ) {
		$this->value = $value;
		if( 'checkbox' == $this->input_type ) {
			$this->get_tag_input()->set_attr( 'checked', maybe_str_to_bool( $value ) );
		}
		else {
			$this->get_tag_input()->set_attr( 'value', $value );
		}

		return $this;
	}

	public function set_required( $required = null ) {

		$this->required = $required;

		$this->get_tag_input()->set_attr( 'required', (bool) $required );

		return $this;
	}

	public function set_accept( $accept = null ) {

		$this->accept = $accept;

		$this->get_tag_input()->set_attr( 'accept', $accept );

		return $this;
	}

	// public function set_omh_field_type( $omh_field_type ) {

	// 	$this->omh_field_type = $omh_field_type;

	// 	$this->get_tag_input()->set_attr('data-omh-field-type', $this->omh_field_type );

	// 	return $this;
	// }

	public function set_input_attrs( $input_attrs = array() ) {

		$this->input_attrs = $input_attrs;

		$input_tag = $this->get_tag_input();

		$input_tag->set_attrs( $input_attrs );

		return $this;
	}

	public function get_input_attrs() {

		return $this->input_attrs;
	}

	public function set_input_class( $input_class = '' ) {

		$this->input_class = $input_class;

		$input_tag = $this->get_tag_input();

		$input_tag->add_class( $this->input_class );

		return $this;
	}

	public function get_input_class() {

		return $this->input_class;
	}
	
	protected function get_tag_input_group() {

		$input_group_wrap = $this->get_child( 'col' ) ?? $this;

		return $input_group_wrap->get_child( 'input_group' );
	}

	public function should_have_input_group() {

		return ( $this->prepend || $this->append );
	}

	protected function make_input_group() {

		if( !$this->get_tag_input_group() ) {

			$input_group_wrap = $this->get_child('col') ?? $this;

			//$input_tag = $this->get_tag_input();

			$input_group_tag = OMH_HTML_Tag::factory(
				array(
					'type'	=> 'div',
					'class'	=> 'input-group',
					'contents' => array(
						'input'	=> $this->get_tag_input()
					)
				)
			);

			if( $this->prepend ) {

				$input_group_prepend = array(
					'prepend' => OMH_HTML_Tag::factory(
						array(
							'type'	=> 'div',
							'class'	=> 'input-group-prepend',
							'contents'=> $this->prepare_input_group_content( $this->prepend )
						)
					)
				);

				$input_group_tag->add_content( $input_group_prepend, false );
			}

			if( $this->append ) {

				$input_group_append = array(
					'append' => OMH_HTML_Tag::factory(
						array(
							'type'	=> 'div',
							'class'	=> 'input-group-append',
							'contents'=> $this->prepare_input_group_content( $this->append )
						)
					)
				);

				$input_group_tag->add_content( $input_group_append, true );
			}

			$input_group_wrap->add_content(
				array(
					'input_group' => $input_group_tag
				),
				'label'
			);

			$input_group_wrap->remove_content('input');
		}

		if( $this->size ) {

			$this->set_size( $this->size );
		}

		return $this;
	}

	protected function destroy_input_group() {

		if( $this->get_tag_input_group() ) {

			$input_group_wrap = $this->get_child('col') ?? $this;

			$input_group_wrap->add_content( 
				array( 
					'input' => $this->get_tag_input()
				)
			)->remove_content('input_group');
		}

		return $this;
	}

	protected function prepare_input_group_content( $content ) {

		$prepared_content = array();
		
		if( is_string( $content ) || is_numeric( $content ) ) {

			array_push( $prepared_content, $this->wrap_input_group_content_text( $content ) );
		}
		else if( is_array( $content ) ) {

			foreach( $content as $name => $value ) {

				$prepared_content[ $name ] = $this->wrap_input_group_content_text( $value );
			}
		}

		return $prepared_content;
	}

	protected function wrap_input_group_content_text( $text ) {

		if( is_string( $text ) || is_numeric( $text ) ) {

			return OMH_HTML_Tag::factory( 
				array(
					'type'	=> 'span',
					'class'	=> 'input-group-text',
					'contents'	=> $text
				)
			);	
		}
		else {
			return $text;
		}
		
	}

	protected function set_input_group() {

		$input_group_tag = $this->get_tag_input_group();

		if( $this->should_have_input_group() && !$input_group_tag ) {

			$this->make_input_group();
		}
		else if( !$this->should_have_input_group() && $input_group_tag ) {

			$this->destroy_input_group();
		}

		return $this;
	}

	public function set_prepend( $prepend = null ) {

		$this->prepend = $prepend;

		return $this->set_input_group();

	}

	public function set_append( $append = null ) {

		$this->append = $append;

		return $this->set_input_group();
	}

}
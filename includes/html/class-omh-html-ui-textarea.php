<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Textarea extends OMH_HTML_UI_Input {

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'disabled' 			=> array(
			'default' 			=> false
		),
		'placeholder' 		=> array(),		
		'form_text' 		=> array(),
		'invalid_feedback' 	=> array(),
		'input_id' 			=> array(
			'required' 			=> true
		),
		'required'			=> array(
			'type'				=> 'boolean',
			'default'			=> null
		),
		'label' 			=> array(),
		'horizontal' 		=> array(
			'default' 			=> false
		),
		'input_type'		=> array(
			'default' 			=> 'textarea'
		),
		'input_class'		=> array(),
		'value'				=> array()
	);

	/**
	 * @var 	array
	 */
	protected $default_args = array(
		'type' 			=> 'div',
		'class' 		=> 'form-group',
		'disabled' 		=> false,
		'label' 		=> '',
		'horizontal' 	=> false,
		'input_type'	=> 'textarea',
		'contents' 		=> array(
			'label' 			=> array(
				'type' 				=> 'label'
			),
			'input' 			=> array(
				'type' 				=> 'textarea',
				'class' 			=> 'form-control omh-field-input'
			),
			'form_text' 		=> array(
				'type' 				=> 'small',
				'class' 			=> 'form-text text-muted',
				'contents' 			=> array(
					'text' 				=> ''
				)
			),
			'invalid_feedback' 	=> array(
				'type' 				=> 'div',
				'class' 			=> 'invalid-feedback',
				'contents' 			=> array(
					'text' 				=> ''
				)
			)
		),
		'input_class'	=> '',
		'value'			=> null
	);


	public static function build( $input_id = null, $label = '', $value = null, $input_type = 'text', $required = null, $extra_args = array() ) {

		$args = array(
			'input_id'	=> $input_id,
			'label'		=> $label,
			'value'		=> $value,
			'required'	=> $required
		);

		$args = array_merge( $args, $extra_args );

		return self::factory( $args );
	}

	public function set_value( $value = null ) {

		$this->value = $value;

		$this->get_tag_input()->set_contents( $value );

		return $this;
	}

}
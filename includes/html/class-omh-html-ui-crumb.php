<?php
defined( 'ABSPATH' ) || exit;

class OMH_HTML_UI_Crumb extends OMH_HTML_UI_Element {

	protected $type = 'li';

	protected $bootstrap_class = 'breadcrumb-item';

	protected $label;

	protected $url;

	protected $active = FALSE;

	protected $custom_args = array(
		'label'		=> array(
			'required'	=> true,
		),
		'url'		=> array(
			// 'required'	=> true,
		),
		'active'	=> array(
			'type'		=> 'boolean',
			'default'	=> false
		)
	);

	protected $default_args = array(
		'type'		=> 'li',
		'class'		=> 'breadcrumb-item',
		'contents'	=> array(
			'link'		=> array(
				'type'		=> 'a',
				'class'		=> 'breadcrumb-link',
				'attrs'		=> array(
					// 'href'		=> '#'
				)
			)
		)
	);

	public function get_crumb_link() {

		return $this->get_child( 'link' );
	}

	public function set_label( $label ) {

		$this->label = $label;

		$link = $this->get_crumb_link();
		$link->add_content(
			array(
				'label'	=> $label
			)
		);

		return $this;
	}

	public function get_label() {

		return $this->label;
	}

	public function set_url( $url ) {

		$this->url = $url;

		$link = $this->get_crumb_link();
		$link->set_attr( 'href', $url );

		return $this;
	}

	public function get_url() {

		return $this->url;
	}

	public function set_active( $active = true ) {

		$this->active = $active;

		if( $active ) {

			$this->add_class( 'active' );
		}

		return $this;
	}

	public function get_active() {

		return $this->active;
	}
}
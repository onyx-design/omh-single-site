<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_HTML_UI_Badge extends OMH_HTML_UI_Element {

	/**
	 * @var 	string
	 */
	protected $bootstrap_class = 'badge';

	/**
	 * @var 	string
	 */
	protected $type = 'span';

	/**
	 * @var 	string
	 */
	protected $label;
	
	/**
	 * @var 	string
	 */
	protected $color;

	/**
	 * @var 	string
	 */
	protected $tooltip;

	/**
	 * @var 	array
	 */
	protected $custom_args = array(
		'label'  => array(
			'required' 	=> true
		),
		'color' => array(
			'type' 		=> 'UI Color',
			'default'	=> 'secondary'
		),
		'size'	=> array(
			'type'		=> 'UI Size',
		),
		'tooltip' => array(
			'default' 	=> false
		)
	);

	/**
	 * @var 	array
	 */
	protected $default_args = array(
		'type' => 'span',
		'color'	=> 'secondary',
		'class' => 'badge-pill',
		'tooltip' => false,
		'contents' => array(
			'label' => 'Badge'
		)
	);	

	/**
	 * Constructor!
	 * 
	 * @param 	array 	$args
	 * 
	 * @return 	void
	 */
	// public function __construct( $args ) {

	// 	$args['type']	= self::CLASS;
	// }
	
	/**
	 * Set the label of the OMH_HTML_UI_Badge
	 * 
	 * @param 	string 	$label
	 * 
	 * @return 	OMH_HTML_UI_Badge
	 */
	public function set_label( $label ) {

		$this->label = $label;

		$this->set_contents( $this->label );

		return $this;
	}
	
	/**
	 * Get the label of the OMH_HTML_UI_Badge
	 * 
	 * @return 	string
	 */
	public function get_label() {

		return $this->label;
	}
	
	/**
	 * Set the color of the OMH_HTML_UI_Badge
	 * 
	 * @param 	string 	$color
	 * 
	 * @return 	OMH_HTML_UI_Badge
	 */
	public function set_color( $color ) {

		if ( in_array( $color, self::COLORS ) ) {

			// Remove current color classes
			$this->remove_class( $this->get_color_classes() );  

			// Add new color class
			$this->color = $color;

			$this->add_class( $this->get_color_class( $color ) );
		}

		return $this;
	}

	/**
	 * Get the color of the OMH_HTML_UI_Badge
	 * 
	 * @return 	string
	 */
	public function get_color() {

		return $this->color;
	}

	/**
	 * Set the tooltip text of the OMH_HTML_UI_Badge
	 * 
	 * @param 	string 	$tooltip
	 * 
	 * @return 	OMH_HTML_UI_Badge
	 */
	public function set_tooltip( $tooltip ) {

		$tooltip_attrs = array(
			'data-content'		=> null,
			'data-toggle'		=> null,
			'data-placement'	=> null,
			'data-delay'		=> null,
		);

		if( !empty( $tooltip ) ) {

			$tooltip_attrs['data-toggle'] = 'popover';
			$tooltip_attrs['data-container'] = 'body';
			$tooltip_attrs['data-trigger'] = 'hover';
			$tooltip_attrs['data-boundary'] = 'viewport';
			$tooltip_attrs['data-placement'] = 'bottom';

			if( is_string( $tooltip ) ) {
				
				$tooltip_attrs['data-content'] = $tooltip;
			}
			else if( is_array( $tooltip ) ) {

				foreach( $tooltip as $attr => $value ) {

					// Parse $attr
					if( 'tooltip' == $attr || 'title' == $attr ) {
						$attr = 'data-content';
					}
					else if( 0 !== strpos( $attr, 'data-' ) ) {
						$attr = 'data-' . $attr;
					}

					$tooltip_attrs[ $attr ] = $value;
				}
			}
		}

		$this->tooltip = $tooltip_attrs;

		return $this->set_attrs( $this->tooltip );

	}

	/**
	 * Get the tooltip of the OMH_HTML_UI_Badge
	 * 
	 * @return 	string
	 */
	public function get_tooltip() {

		return $this->tooltip;
	}



}
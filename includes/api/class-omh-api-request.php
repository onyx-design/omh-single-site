<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class OMH_API_Request {

	public static $api_name = 'OMH API';

	public static $last_api_call = null;

	public static $response_class = 'OMH_API_Response';

	public static $request_types = array();

	protected static $api_credentials = null;

	protected static $api_environments = array();	

	protected static $api_environment_default = 'live';

	protected static $default_params = '';

	public static function set_environment( $environment = null ) {

		// Default to previously set environment
		if( !isset( $environment ) && isset( static::$api_credentials ) ) {
			return static::$api_credentials;
		}

		$environment = $environment ?: static::$api_environment_default;

		return static::$api_credentials = 
			static::$api_environments[ $environment ] 
			?? current( static::$api_environments );
	}

	public static function request( $request_type, $request_data = array() ) {

		$response_class = static::$response_class;

		$request_url = static::request_url( $request_type, $request_data );

		// Build CURL request object
		$request_curl = static::build_curl( $request_url, $request_type, $request_data );

		// Maybe throttle request
		static::request_throttling();

		static::$last_api_call = microtime( true );

		// Execute request
		$call_result = curl_exec( $request_curl );
		$curl_info = curl_getinfo( $request_curl );
		curl_close( $request_curl );

		$request_type_name = $request_type;
		$request_type = static::$request_types[ $request_type_name ];
		$request_type['type'] = $request_type_name;

		return new $response_class( $call_result, $curl_info, $request_data, $request_type );		
	}

	public static function request_url( $request_type, $request_data = array() ) {

		$endpoint = static::request_endpoint( $request_type, $request_data );

		return static::$api_credentials['base_url'] . $endpoint;
	}

	public static function request_endpoint( $request_type, $request_data = array() ) {
		
		static::set_environment();

		// Initialize and validate $request_type
		$request_template = static::$request_types[ $request_type ] ?? null;

		if( !$request_type || !$request_template ) {
			// "Invalid {static::$api_name} request type: {$request_type}"
			return false;
		}

		$endpoint = $request_template['endpoint'];

		// Maybe add URL params to endpoint
		if( isset( $request_data['url_params'] ) ) {

			foreach( $request_data['url_params'] as $param_key => $param_value ) {

				$endpoint = str_replace( "[{$param_key}]", $param_value, $endpoint, $params_count );
				if( $params_count < 1 ) {
					// "{$param_key} not found in URL: {$endpoint}."
					return false;
				}
			}
		}

		// Maybe add query params to endpoint	
		if( isset( $request_data['query_params'] ) ) {

			$http_query = http_build_query( $request_data['query_params'] );

			$separator = static::$default_params ? static::$default_params . '&' : '?';
			$endpoint .= "{$separator}{$http_query}";			
		}

		return $endpoint;
	}

	public static function build_curl( $request_url, $request_type, $request_data = array() ) {
	}

	public static function request_throttling() {
		return true;
	}

	public static function get_api_credentials() {
		return static::$api_credentials;
	}
}
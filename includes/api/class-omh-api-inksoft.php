<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

error_reporting(E_ALL);
ini_set("log_errors", 1); 

class OMH_API_Inksoft extends OMH_API_Request {

	public static $api_name = 'InkSoft API';

	protected static $api_environments = array(
		'live' 	=> array( 
			'api_key' 		=> '161bf9b2-7d78-4614-ac03-9eef97dd2e37',
			'api_password' 	=> '97034835-627b-402d-b21b-5618a31c740c',
			'base_url'		=> 'stores.inksoft.com/mhnational/Api2/',
			'email'			=> 'admin@merch.house',
			'password'		=> 'Panda123'
		),
	);

	protected static $default_params = '?Format=JSON';

	public static $request_types = array(

		/*
		 * Product Endpoints
		 */

		// Get Products
		'get_products'				=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'GetProducts',
			'response_wrap'		=> 'Data'
		),
		// Get Single Product
		'get_product'				=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'GetProduct',
			'query_params'		=> array( 'ProductID', 'IncludePricing', 'IncludeCategories' ),
			'response_wrap'		=> 'Data'
		),
		// Get Product Base List
		'get_product_base_list'		=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'GetProductBaseList',
			'query_params'		=> array( 'CanDigitalPrint', 'CanEmbroider', 'CanScreenPrint', 'ColorTypes', 'HasProductArt', 'IncludeAllPublisherProducts', 'IncludeAllSides', 'IncludeAllStyles', 'IncludeCategories', 'IncludeCosts', 'IncludeInactiveProducts', 'IncludeInactiveStyles', 'IncludeKeywords', 'IncludePrices', 'IncludeSizes', 'IncludeStoreIds', 'IncludeStyleCounts', 'Index', 'ManufacturerIds', 'MaxResults', 'Predecorated', 'ProductCategoryIds', 'ProductIds', 'ProductTypes', 'SearchFilter', 'SearchText', 'SortFilters', 'StoreIds', 'SupplierIds' ),
			'response_wrap'		=> 'Data'
		),
		// Get Product/s Categories
		'get_product_categories'	=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'GetProductCategories',
			'query_params'		=> array( 'GetProductIds' )
		),
		// Get Product Images
		'get_product_images'		=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'GetProductImages',
			'query_params'		=> array( 'ProductID', 'ProductStyleId' )
		),
		// Get Products by Category
		'get_products_by_category'	=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'GetProductsByCategory',
			'query_params'		=> array( 'CategoryId', 'IncludePricing' )
		),
		// Get Manufacturers
		'get_manufacturers'			=> array(
			'method'			=> 'GET',
			'endpoint'			=> 'GetManufacturers',
			'query_params'		=> array( 'BlankProducts', 'StaticProducts', 'ProductType' ),
			'response_wrap'		=> 'Data'
		),

		/*
		 * Design Endpoints
		 */

		// Render Design
		'render_design'				=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'RenderDesignNow',
			'query_params'		=> array( 'DesignId', 'Email', 'ApiKey' ),
			'response_wrap'		=> 'Data'
		),

		/*
		 * Session Endpoints
		 */

		'sign_in'	=> array(
			'method'			=> 'POST',
			'endpoint'			=> 'SignIn',
			'query_params'		=> array( 'Email', 'GuestSessionToken', 'Password', 'RememberMe' ),
			'response_wrap'		=> 'Data'
		)
	);

	public static function request_url( $request_type, $request_data = array() ) {
		$endpoint = static::request_endpoint( $request_type, $request_data );

		return "https://" . static::$api_credentials['base_url'] . $endpoint;
	}

	public static function build_curl( $request_url, $request_type, $request_data = array() ) {

		$request_method = static::$request_types[ $request_type ]['method'];

		$curl = curl_init( $request_url );

		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $curl, CURLOPT_HEADER, false );
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, $request_method );

		// $headers = array( 'Content-Type: application/json' );
		$headers = array( 'Content-Type: application/x-www-form-urlencoded' );

		curl_setopt( $curl, CURLOPT_HTTPHEADER, $headers );

		/*
		 * If we don't have a session, generate it
		 */
		if( !isset( $_COOKIE['SessionToken'] ) ) {
			static::get_session();
		}
		curl_setopt( $curl, CURLOPT_COOKIE, 'SessionToken=' . $_COOKIE['SessionToken'] );
		
		if( 'POST' == $request_method || 'PUT' == $request_method ) {
			$request_body = json_encode( $request_data['request_body'] );
			curl_setopt( $curl, CURLOPT_POSTFIELDS, $request_body );
		}

		curl_setopt( $curl, CURLOPT_TIMEOUT, 25 );

		return $curl;
	}

	/**
	 * Login and get session
	 * 
	 * @param 	array 	$data
	 * 
	 * @return 	string 	$response_data
	 */
	public static function get_session() {
		
		static::set_environment();

		$response_class = static::$response_class;

		$request_url = static::request_url( 'sign_in', array( 'query_params' => array( 'Email' => static::$api_credentials['email'], 'Password' => static::$api_credentials['password'] ) ) );

		$curl = curl_init( $request_url );

		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt( $curl, CURLOPT_HEADER, false );
		curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );

		curl_setopt( $curl, CURLOPT_POSTFIELDS, NULL );

		$call_result = curl_exec( $curl );
		$curl_info = curl_getinfo( $curl );
		curl_close( $curl );

		$request_type_name = 'sign_in';
		$request_type = static::$request_types[ $request_type_name ];
		$request_type['type'] = $request_type_name;

		$response_data = new $response_class( $call_result, $curl_info, array(), $request_type );

		if( $response_data->data( 'Token' ) ) {
			$_COOKIE['SessionToken'] = $response_data->data( 'Token' );
		}

		return $response_data;
	}

	public static function request_throttling() {
		
		// API call throttling
		if( static::$last_api_call ) {
			$last_call_udiff = microtime( true ) - static::$last_api_call;
			if( $last_call_udiff < 500000 ) {
				usleep( min( 1000000 - $last_call_udiff, 500000 ) );
			}
		}
	}	
}
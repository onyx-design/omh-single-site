<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Merch House User Class
 *
 * The User class handles getting and setting user status
 *
 *
 * @class 		OMH_User
 * @package		OMH
 * @author 		ONYX Design
 */

class OMH_User extends WP_User {

	private static $allowed_roles = array(
		'administrator'	=> 'Super Admin',
		'house_admin'	=> 'House Admin',
		// dev:activate
		'house_member'	=> 'House Member',
		'customer'		=> 'Customer'
	);

	public function __construct( $user_id = 0, $name = '', $site_id = '', $invited_by = null ) {
		parent::__construct( $user_id, $name, $site_id );

		// Infinite loop
		if( $invited_by ) {
			$this->set_invited_by( $invited_by );
		}
	}

	/********************************
	 *		Helper Functions 		*
	 ********************************/

	/**
	 * Returns whether the user exists and the current user is this user
	 * 
	 * @see 	WP_User::exists()
	 * 
	 * @return 	bool
	 */
	public function is_logged_in() {
		return $this->exists() && ( get_current_user_id() == $this->ID );
	}

	public function login( $credentials = array() ) {

		if( !empty( $credentials ) ) {

			wp_signon( $credentials, is_ssl() );
		} else {

			$secure_cookie = is_ssl();
			$secure_cookie = apply_filters( 'secure_signon_cookie', $secure_cookie, $credentials );

			global $auth_secure_cookie; // XXX ugly hack to pass this to wp_authenticate_cookie
			$auth_secure_cookie = $secure_cookie;

			wp_set_current_user( $this->ID, $this->user_login );
			wp_set_auth_cookie( $this->ID, true, $secure_cookie );

			do_action( 'wp_login', $this->user_login, $this );
		}
	}

	/**
	 * Returns whether the user is activated or not
	 * 
	 * @return 	bool
	 */
	public function is_activated() {

		if( $this->has_role( array( 'administrator', 'customer' ), true, true ) ) {
			return true;
		}
		
		// If the user doesn't have an activation key and they are set as activated, return true
		return ( !$this->get_user_activation_key() && $this->get_account_activated() );
	}

	/**
	 * User is setup when they have first and last name
	 * 
	 * @return 	bool
	 */
	public function is_setup() {
		return ( !empty( $this->first_name ) && !empty( $this->last_name ) );
	}

	public function make_house_member( $chapter_id, $new_user = false ) {

		// We don't want to change admins or house admins
		if( $this->is_administrator() || $this->has_role( 'house_admin' ) ) {
			return false;
		}

		$user_chapter_id = $this->get_chapter_id();

		// Don't switch user's chapter
		if( $user_chapter_id ) {
			return false;
		}

		// Do the stuff, maybe send an email
		$this->set_role( 'house_member' );

		if( $this->get_chapter_id() != $chapter_id ) {
			$this->set_chapter_id( $chapter_id );
		}

		// Send email if user already exists to notify upgrade
		// if (!$new_user){
		// 	OMH_Emails::send_house_member_upgrade_notification( $this->ID );
		// }
	}

	/********************************
	 *		User Data Functions		*
	 ********************************/

	/**
	 * Save the user's data
	 * 
	 * @param 	array|WP_User 		$data
	 * 
	 * @return 	int|WP_Error|bool
	 */
	public function save_user_data( $data = array() ) {

		// If we have an array and the array isn't empty, update it!
		if( is_array( $data ) && !empty( $data ) ) {

			$data['ID'] = $this->ID;

			wp_update_user( $data );
		}
	}

	/**
	 * Get the user's full name
	 * 
	 * @return 	string|bool
	 */
	public function get_full_name() {

		if( $this->is_setup() ) {
			return ucwords( $this->first_name . ' ' . $this->last_name );
		} else {
			return '';
		}
	}

	/**
	 * Get the user's initials
	 * 
	 * @return 	string
	 */
	public function get_initials() {

		if( $this->is_setup() ) {
			return strtoupper( substr( $this->first_name, 0, 1 ) . substr( $this->last_name, 0, 1 ) );
		} else {
			return '';
		}
	}

	/**
	 * Get the user's email
	 * 
	 * @return 	string
	 */
	public function get_email() {

		// $user_data = get_userdata( $this->ID );

		// return $user_data->user_email;

		return $this->user_email;
	}

	/********************************
	 *		Meta Data Functions		*
	 ********************************/

	public function get_meta( $key, $single = false ) {

		return get_user_meta( $this->ID, $key, $single );
	}

	public function update_meta( $key, $value, $prev_value = '' ) {

		return update_user_meta( $this->ID, $key, $value, $prev_value );
	}

	public function delete_meta( $key, $value = '' ) {

		return delete_user_meta( $this->ID, $key, $value );
	}

	/**
	 * Get the user ID of who invited the current user
	 * 
	 * @param 	bool 	$return_user
	 * 
	 * @return 	OMH_User|int
	 */
	public function get_invited_by( $return_user = false ) {

		$invited_by = $this->get_meta( 'invited_by', true ) ?: false;

		if( $return_user && $invited_by ) {
			return new self( $invited_by );
		}

		return $invited_by;
	}

	/**
	 * Set the user ID of who invited the current user
	 * 
	 * @param 	int 	$invited_by
	 * 
	 * @return 	int
	 */
	public function set_invited_by( $invited_by ) {
		$this->update_meta( 'invited_by', $invited_by );

		return $this->get_invited_by();
	}

	public function get_chapter_id( $force_return = false ) {
		return $this->get_primary_chapter_id( $force_return );
	}

	public function set_chapter_id( $chapter_id = 0 ) {
		return $this->set_primary_chapter_id( $chapter_id );
	}

	/**
	 * Get the User's Primary Chapter ID
	 * 
	 * If the user isn't a super admin, house admin, or house member - return false
	 * If the user is a super admin and does not have primary chapter set, set it to the first chapter (this is mainly just for caching so we're not querying the table constantly)
	 * If the user is a house admin or house member, return the primary chapter id from meta
	 * 
	 * @param 	bool	$force_return
	 * 
	 * @return 	int|bool
	 */
	public function get_primary_chapter_id( $force_return = false ) {

		$primary_chapter_id = $this->get_meta( 'primary_chapter_id', true ) ?: false;

		if( $this->has_role( 'administrator' ) ) {

			if( !$primary_chapter_id ) {

				$primary_chapter = OMH()->chapter_factory->query( 
					array( 
						'posts_per_page' => 1, 
						'return' => 'ids' 
					) 
				);

				$primary_chapter_id = $primary_chapter[0];

				$this->set_primary_chapter_id( $primary_chapter_id );
			}

			return $primary_chapter_id;
		} elseif( $this->has_role( array( 'house_admin', 'house_member' ) ) || $force_return ) {

			return $primary_chapter_id;
		}

		return false;
	}

	/**
	 * Set the User's Primary Chapter ID
	 * 
	 * If the user isn't a super admin, house admin, or house member - return false
	 * 
	 * @param 	int 	$chapter_id 
	 * @return type
	 */
	public function set_primary_chapter_id( $chapter_id = 0 ) {

		// Only set the chapter if the user is in one of the following roles
		if( !$this->has_role( array( 'administrator', 'house_admin', 'house_member' ) ) ) {
			return false;
		}

		$this->update_meta( 'primary_chapter_id', $chapter_id );

		return $this->get_primary_chapter_id();
	}

	/**
	 * Get the User's Chapter Object
	 * 
	 * If the user has a chapter, return it, else return null
	 * 
	 * @param 	type|bool $force_return
	 * 
	 * @return 	OMH_Model_Chapter|null
	 */
	public function get_chapter( $force_return = false ) {

		if( $chapter_id = $this->get_chapter_id( $force_return ) ) {

			return OMH()->chapter_factory->read( $chapter_id );
		}

		return null;
	}

	/********************************
	 *		 Roles Functions		*
	 ********************************/

	/**
	 * Return whether the user has the supplied roles
	 * 
	 * dev:improve dev:todo Maybe break this out into different role functions because we need to make sure house admins have chapters to count them as a house admin
	 * 
	 * @param 	array|string 	$roles
	 * 
	 * @return 	bool
	 */
	public function has_role( $check_roles, $inclusive = true, $single = false ) {

		$check_roles = (array) $check_roles;
		$roles = $this->roles;

		if( in_array( 'administrator', $roles ) && $inclusive ) {
			return true;
		}

		if( $single && ( count( $roles ) > 1 ) ) {
			return false;
		}

		return !empty( array_intersect( $roles, $check_roles ) );
	}

	/**
	 * Return whether the user is a House Admin or House Member
	 * 
	 * @return 	bool
	 */
	public function has_house_role() {
		return $this->has_role( array_keys( $this->get_house_roles() ) );
	}

	/**
	 * Return whether the user is a super admin or not
	 * 
	 * @return 	bool
	 */
	public function is_administrator() {
		return $this->has_role( 'administrator' );
	}

	/**
	 * Return strictly whether the user is a house admin or not
	 * 
	 * @return 	bool
	 */
	public function is_house_admin( $strict = true ) {
		return $this->has_role( 'house_admin', !$strict );
	}

	public function is_house_member( $strict = true) {
		return $this->has_role( 'house_member', !$strict );
	}

	/**
	 * Get user's official house role based on role array order
	 * 
	 * If a user has multiple roles, make sure we return the highest role
	 * 
	 * @param 	bool 	$return_label
	 * 
	 * @return 	string
	 */
	public function get_house_role( $return_label = false ) {

		$house_role = null;

		foreach( $this->get_allowed_roles() as $role => $role_name ) {

			if( $this->has_role( $role ) ) {

				$house_role = $role;
				break;
			}
		}

		return ( $house_role && $return_label ) ? ucwords( str_replace( '_', ' ', $house_role ) ) : $house_role;
	}

	/**
	 * Get the user's House role badge
	 * 
	 * @return 	string|null
	 */
	public function get_house_role_badge() {

		$role_badges = array(
			'administrator'	=> array(
				'label'	=> 'Administrator',
				'color' => 'secondary'
			),
			'house_admin'	=> array(
				'label'	=> 'House Admin',
				'color' => 'primary'
			),
			'house_member'	=> array(
				'label'	=> 'House Member',
				'color'	=> 'info'
			),
			'customer'		=> array(
				'label'	=> 'Customer',
				'color' => 'info'
			)
		);

		$house_role = $this->get_house_role();

		if( isset( $role_badges[ $house_role ] ) ) {

			return OMH_HTML_UI_Badge::factory(
				$role_badges[ $house_role ]
			);
		}

		return null;
	}

	/**
	 * Return whether the user is a House Admin or House Member for supplied chapter
	 * 
	 * @return 	bool
	 */
	public function is_member_of_house( $chapter_id ) {

		if( $chapter_id instanceof OMH_Model_Chapter ) {
			$chapter_id = $chapter_id->get_id();
		}

		if( $chapter_id != $this->get_chapter_id() ) {
			return false;
		}

		return $this->has_house_role();
	}

	/**
	 * Return whether the user can use store credit
	 * 
	 * @return 	bool
	 */
	public function can_use_store_credit() {

		return $this->is_house_admin( false );
	}

	/********************************
	 *		Activation Functions	*
	 ********************************/

	/**
	 * Set account activated meta
	 * 
	 * @param 	bool 	$activated
	 * 
	 * @return 	bool
	 */
	public function set_account_activated( $activated ) {

		if( $activated ) {
			$this->remove_activation_keys();
		}

		$this->update_meta( 'account_activated', $activated );

		return $this->get_account_activated();
	}

	/**
	 * Get account activated meta
	 * 
	 * @return 	bool
	 */
	public function get_account_activated() {
		return $this->get_meta( 'account_activated', true );
	}

	/**
	 * Set the house admin activation key meta
	 * 
	 * @param 	string 	$activation_key
	 * 
	 * @return 	string
	 */
	private function set_house_admin_activation_key( $activation_key ) {

		$this->update_meta( 'house_admin_activation_key', $activation_key );

		return $this->get_user_activation_key();
	}

	private function get_house_admin_activation_key() {
		return $this->get_meta( 'house_admin_activation_key', true );
	}

	/**
	 * Set the house member activation key meta
	 * 
	 * @param 	string 	$activation_key
	 * 
	 * @return 	string
	 */
	private function set_house_member_activation_key( $activation_key ) {

		$this->update_meta( 'house_member_activation_key', $activation_key );

		return $this->get_user_activation_key();
	}

	private function get_house_member_activation_key() {
		return $this->get_meta( 'house_member_activation_key', true );
	}

	private function remove_activation_keys() {

		$this->delete_meta( 'house_admin_activation_key' );
		$this->delete_meta( 'house_member_activation_key' );
	}

	/**
	 * Generate the activation key for a house user depending on the role and return it
	 * 
	 * @return 	string
	 */
	public function generate_user_activation_key() {

		$activation_key = wp_generate_password( 20, false );

		if( $this->has_role( 'house_admin' ) ) {
			$this->set_house_admin_activation_key( $activation_key );
		} elseif( $this->has_role( 'house_member' ) ) {
			$this->set_house_member_activation_key( $activation_key );
		}

		return $this->get_user_activation_key();
	}

	/**
	 * Get the House user activation key meta
	 * 
	 * @return 	string
	 */
	public function get_user_activation_key() {

		// Return false for non house roles
		if( !$this->has_house_role() ) {
			return false;
		}

		if( $this->has_role( 'house_admin' ) ) {
			$activation_key = $this->get_house_admin_activation_key();
		} elseif( $this->has_role( 'house_member' ) ) {
			$activation_key = $this->get_house_member_activation_key();
		}

		/*
		 * If the user doesn't have an activation_key and they are not active, generate and return the activation_key
		 * Avoid infinite loops here by checking activation_key and account_activated rather than using helper functions
		 */
		if( !$activation_key && !$this->get_account_activated() ) {
			$activation_key = $this->generate_user_activation_key();
		}

		return $activation_key;
	}

	/**
	 * Send the user's activation email
	 */
	public function send_activation_email() {
		OMH_Emails::send_user_activation_notification( $this->ID );
	}

	/**
	 * Send email to admin stating that a user's account was activated
	 */
	public function send_admin_user_activated_email() {
		OMH_Emails::send_admin_user_activated_notification( $this->ID );
	}

	/**
	 * Run the user activation process depending on the role
	 * 
	 * If the user is not a house role, return true
	 * 
	 * @return 	bool
	 */
	public function activate_user() {
		if( !$this->has_house_role() ) {
			return true;
		}

		if( $this->has_role( 'house_admin' ) ) {
			$this->activate_house_admin();
		} elseif( $this->has_role( 'house_member' ) ) {
			$this->activate_house_member();
		}

		$this->set_account_activated( true );

		return true;
	}

	/**
	 * Run the user activation process for house admins
	 */
	private function activate_house_admin() {
		OMH_Emails::send_admin_user_activated_notification( $this->ID );
	}

	/**
	 * Run the user activation process for house members
	 */
	private function activate_house_member() {
	}

	/********************************
	 *		 Static Functions		*
	 ********************************/

	/**
	 * Get OMH User by User ID
	 * 
	 * @param 	int 	$user_id
	 * 
	 * @return 	bool|OMH_User
	 */
	public static function get_user( $user_id = 0 ) {

		if( !$user_id ) {
			return false;
		}

		return new self( $user_id );
	}

	/**
	 * Get allowed site roles
	 * 
	 * @return 	array
	 */
	public static function get_allowed_roles( $keys = false ) {

		$allowed_roles = static::$allowed_roles;

		if( $keys ) {
			return array_keys( $allowed_roles );
		}

		return $allowed_roles;
	}

	/**
	 * Get house roles (admin,member)
	 * 
	 * @return 	array
	 */
	public static function get_house_roles() {

		// Only return roles that start with 'house'
		return array_filter( static::$allowed_roles, function( $value, $key ) {

			return ( 0 === strpos( $key, 'house' ) );
		}, ARRAY_FILTER_USE_BOTH );
	}

	/**
	 * This function is a helper function while meta/keys are switched over to the new system
	 * 
	 * Originally, only House Admins had to be activated, but now more roles can be activated
	 * 
	 * @param 	string 	$role
	 * 
	 * @return 	string|bool
	 */
	public static function get_role_activation_key( $role = 'house_admin' ) {

		if( 'house_admin' === $role ) {
			return 'house_admin_activation_key';
		} elseif( 'house_member' === $role ) {
			return 'house_member_activation_key';
		}

		return false;
	}

	/**
	 * Get user by activation key and role
	 * 
	 * @param 	string 	$activation_key
	 * @param 	string 	$role
	 * 
	 * @return 	OMH_User|null
	 */
	public static function get_by_activation_key( $activation_key, $role = 'house_admin' ) {

		if( !in_array( $role, static::get_allowed_roles( true ) ) ) {
			return null;
		}

		$users = get_users(
			array(
				'role__in'		=> array( $role ),
				'meta_key'		=> static::get_role_activation_key( $role ),
				'meta_value'	=> $activation_key
			)
		);

		if( empty( $users ) ) {
			return null;
		}

		$user = current( $users );

		return static::get_user( $user->ID );
	}

	public static function get_by_email($email) {
		$users = get_users(
			array('search' => $email) ) ;

		if( empty( $users ) ) {
			return null;
		}

		$user = current( $users );

		return static::get_user( $user->ID );
	}
}
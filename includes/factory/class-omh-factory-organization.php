<?php
defined( 'ABSPATH' ) || exit;

class OMH_Factory_Organization extends OMH_Factory {

	protected static $table = 'omh_organizations';

	protected static $model = 'OMH_Model_Organization';
}
<?php
defined( 'ABSPATH' ) || exit;

class OMH_Factory_Garment_Style extends OMH_Factory {

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_garment_styles';

	protected static $model = 'OMH_Model_Garment_Style';

	/********************************
	 *			Functions			*
	 ********************************/

	public static function get_by_inksoft_id( $inksoft_id = null ) {

		if( !$inksoft_id ) {
			return false;
		}

		$styles = self::query(
			array(
				'where'	=> array(
					array(
						'name'		=> 'inksoft_style_id',
						'value'		=> $inksoft_id
					)
				)
			)
		);

		if( count( $styles ) > 0 ) {
			return current( $styles );
		}

		return false;
	}

	public static function get_by_garment_product_id( $garment_product_id = null ) {

		if( !$garment_product_id ) {
			return false;
		}

		$styles = self::query(
			array(
				'posts_per_page'	=> -1,
				'where' 			=> array(
					'name'		=> 'garment_product_id',
					'value'		=> $garment_product_id
				)
			)
		);

		if( count( $styles ) > 0 ) {
			return $styles;
		}

		return false;
	}
}
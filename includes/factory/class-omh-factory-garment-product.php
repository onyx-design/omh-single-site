<?php
defined( 'ABSPATH' ) || exit;

class OMH_Factory_Garment_Product extends OMH_Factory {

	protected static $table = 'omh_garment_products';

	protected static $model = 'OMH_Model_Garment_Product';

	public static function create( $data = array(), $defaults = array() ) {

		$defaults = array(
			'product_slug'				=> '',
			'inksoft_product_id'		=> 0,
			'inksoft_sku'				=> '',
			'garment_brand_id'			=> 0,
			'manufacturer_sku'			=> '',
			'product_name'				=> '',
			'product_long_description'	=> '',
		);

		return parent::create( $data, $defaults );
	}

	public static function get_by_inksoft_id( $inksoft_id = null ) {

		if( !$inksoft_id ) {
			return false;
		}

		$products = self::query(
			array(
				'where'	=> array(
					array(
						'name'		=> 'inksoft_product_id',
						'value'		=> $inksoft_id
					)
				)
			)
		);

		if( count( $products ) > 0 ) {
			return current( $products );
		}

		return false;
	}
}
<?php
defined( 'ABSPATH' ) || exit;

class OMH_Factory {

	/********************************
	 *			 Schema				*
	 ********************************/

	protected static $table = 'omh_table';

	protected static $model = 'OMH_Model';

	protected static $rows = null;

	public static $total_rows = null;

	/********************************
	 *			Functions			*
	 ********************************/

	public static function create( $data = array(), $defaults = null ) {
		global $wpdb;

		if( !$defaults ) {
			$defaults = static::$model::$defaults;
		}

		$data = array_filter( $data, function( $data_val, $data_key ) use( $defaults ) { 

			if( !$data_val && isset( $defaults[ $data_key ] ) ) {
				return false;
			}

			return true;
		}, ARRAY_FILTER_USE_BOTH );

		$data = wp_parse_args( $data, $defaults );

		$row = new static::$model( (object) $data );

		// $valid = static::validate( $row_id );
		// if( !empty( $valid ) ) {
		// 	return false;
		// }

		$inserted = $wpdb->insert( $wpdb->base_prefix . static::$table, $row->to_array() );

		if( false !== $inserted ) {

			$row_id = (int) $wpdb->insert_id;

			return static::read( $row_id );
		}

		return false;
	}

	public static function read( $row_id = false, $output = OBJECT ) {
		global $wpdb;

		$row_id = static::get_row_id( $row_id );

		if( !$row_id ) {
			return false;
		}

		$_omh_model = wp_cache_get( $row_id, static::$table );

		if( !$_omh_model ) {
			
			$_omh_model = $wpdb->get_row(
				$wpdb->prepare(
					"
					SELECT *
					FROM {$wpdb->base_prefix}" . static::$table .
					"
					WHERE ID = %d
					LIMIT 1
					",
					$row_id
				)
			);

			if( !$_omh_model ) {
				return false;
			}
		}

		$_row =  new static::$model( $_omh_model );

		if( !$_row ) {
			return false;
		}

		if( $output == ARRAY_A ) {
			return $_row->to_array();
		} elseif( $output == ARRAY_N ) {
			return array_values( $_row->to_array() );
		}

		return $_row;
	}

	public static function update( $row_id = false, $data = array() ) {
		global $wpdb;

		$row_id = static::get_row_id( $row_id );

		if( !$row_id ) {
			return false;
		}

		// $valid = static::validate( $row_id );
		// if( !empty( $valid ) ) {
		// 	return false;
		// }

		$updated = $wpdb->update( $wpdb->base_prefix . static::$table, $data, array( 'ID' => $row_id ) );

		if( false !== $updated ) {

			$row = static::read( $row_id );

			return $row;
		}

		return false;
	}

	public static function delete( $row_id = false ) {
		global $wpdb;

		$row_id = static::get_row_id( $row_id );

		if( !$row_id ) {
			return false;
		}

		$row = static::read( $row_id );

		return $wpdb->delete( $wpdb->base_prefix . static::$table, array( 'ID' => $row->get_id() ) );
	}

	public static function query( $args = array(), $get_total_rows = false ) {
		global $wpdb;

		$defaults = array(
			'posts_per_page' 	=> 50,
			'where'				=> array(),
			'orderby'			=> 'ID',
			'order'				=> 'ASC',
			'return'			=> OBJECT,
		);

		$args = wp_parse_args( $args, $defaults );

		$table = $wpdb->base_prefix . static::$table;

		$select = "$table.*";
		$where = '';
		$query_type = 'results';

		if( isset( $args['search'] ) && method_exists( static::$model, 'get_search_args' ) ) {

			$args['where'][] = static::$model::get_search_args( $args['search'] );
		}

		if( isset( $args['where'] ) && is_array( $args['where'] ) && $args['where'] ) {

			$built_where = static::build_where( $args['where'], $table );

			$where .= $built_where ? " AND $built_where" : '';
		}

		if( isset( $args['return'] ) ) {

			if( is_array( $args['return'] ) ) {

				$select = "$table." . implode( ", $table.", $args['return'] );
				$query_type = count( $args['return'] ) > 1 ? 'results' : 'col';
			} elseif( 'ids' === $args['return'] ) {

				$select = "$table.ID";
				$query_type = 'col';
			} elseif( 'count'	=== $args['return'] ) {

				$select = "COUNT( $table.ID )";
				$query_type = 'var';
			}
		}

		$order_by = "ORDER BY $table.{$args['orderby']} {$args['order']}";

		$limit = '';

		if( isset( $args['posts_per_page'] ) && $args['posts_per_page'] > 0 ) {

			if( ( $args['posts_per_page'] == 1 ) && ( $args['return'] === OBJECT ) ) {
				$query_type = 'row';
			}

			$paged = isset( $args['paged'] ) ? absint( $args['paged'] ) : 1;
			$page_start = absint( ( $paged - 1 ) * $args['posts_per_page'] ) . ', ';

			$limit .= "LIMIT $page_start {$args['posts_per_page']}";
		}

		$query = "SELECT $select FROM $table WHERE 1=1 $where $order_by $limit";

		switch( $query_type ) {
			case 'col':
				$rows = $wpdb->get_col( $query );
				break;
			case 'var':
				$rows = $wpdb->get_var( $query );
				break;
			case 'row':
				$rows = $wpdb->get_row( $query );
				break;
			case 'results':
			default:
				$rows = $wpdb->get_results( $query );
				break;
		}

		if( $get_total_rows ) {
			
			$count_query = "SELECT COUNT(*) FROM $table WHERE 1=1 $where";

			static::$total_rows = $wpdb->get_var( $count_query );

		}
		

		if( $rows ) {

			if( OBJECT === $args['return'] ) {

				if( is_array( $rows ) ) {
					$rows = array_map( 'static::read', $rows );
				} else {
					$rows = static::read( $rows );
				}
			} elseif( 'count' === $args['return'] 
				|| 'ids' === $args['return']
				|| is_array( $args['return'] )
			) {

				$rows = $rows;
			} else {

				if( is_array( $rows ) ) {
					$rows = array_column( $rows, $select );
				} else {
					$rows = isset( $rows->$select ) ? $rows->$select : '';
				}
			}
		}

		// static::$total_rows = count( $rows );

		static::$rows = $rows;

		return $rows;
	}

	public static function build_where( $where = array(), $table = '' ) {
		global $wpdb;

		// Allow custom where strings
		if( !is_array( $where ) ) {
			return $where;
		}

		$where_str = '';

		// If $table or $where wasn't passed in, just return an empty string
		if( !$table || empty( $where ) ) {
			return $where_str;
		}

		// Default $compare
		$compare = isset( $where['compare'] ) ? $where['compare'] : 'AND';

		foreach( $where as $key => $where_clause ) {

			// Ignore the compare keys as those are just used to change the $compare
			if( 'compare' === $key ) {
				continue;
			}

			// If $where_str isn't empty, toss a compare in before we append the next string
			if( $where_str ) {
				$where_str .= " $compare ";
			}

			// If we don't have name or value keys, we need to go down another level
			if( !isset( $where_clause['name'], $where_clause['value'] ) ) {
				$where_str .= '(' . static::build_where( $where_clause, $table ) . ')';
			} else {

				$where_compare = isset( $where_clause['compare'] ) ? $where_clause['compare'] : '=';

				$where_str .= $wpdb->prepare( 
					"$table.{$where_clause['name']} $where_compare %s", 
					$where_clause['value']
				);
			}
		}

		return $where_str;
	}

	public static function validate( $row_id ) {

		if( !$row_id instanceof OMH_Model ) {

			$row_id = static::get_row_id( $row_id );

			if( !$row_id ) {
				return false;
			}

			$row = static::read( $row_id );

			if( !$row ) {
				return false;
			}
		} else {

			$row = $row_id;
		}

		return $row->validate();
	}

	public static function total_rows( $query = array(), $force = false ) {

		$default_query = array(
			'posts_per_page' => -1,
			'return' => 'count' 
		);

		if( !isset( static::$total_rows ) || $force ) {

			$query = wp_parse_args( $default_query, $query );

			static::$total_rows = static::query( $query );
		}

		return static::$total_rows;
	}

	public static function get_row_id( $row = null ) {

		if( is_numeric( $row ) ) {
			return $row;
		} elseif( $row instanceof OMH_Model ) {
			return $row->ID;
		} elseif( is_object( $row ) && !empty( $row->ID ) ) {
			return $row->ID;
		} else {
			return false;
		}
	}

	public static function get_by_term_id( $term_id ) {

		return static::get_by_term_field( 'id', $term_id );
	}

	public static function get_by_term_slug( $term_slug ) {

		return static::get_by_term_field( 'slug', $term_slug );
	}

	public static function get_by_term_field( $term_field = 'slug', $term_value ) {

		if( !( $taxonomy = static::$model::get_taxonomy() ) ) {
			return false;
		}

		if( $term = get_term_by( $term_field, $term_value, $taxonomy ) ) {
			$object = static::query(
				array(
					'posts_per_page'	=> 1,
					'where'				=> array(
						array(
							'name'	=> 'term_id',
							'value'	=> $term->term_id
						)
					)
				)
			);

			return $object;
		}

		return false;
	}
}
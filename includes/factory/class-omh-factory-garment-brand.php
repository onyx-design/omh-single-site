<?php
defined( 'ABSPATH' ) || exit;

class OMH_Factory_Garment_Brand extends OMH_Factory {

	protected static $table = 'omh_garment_brands';

	protected static $model = 'OMH_Model_Garment_Brand';

	public static function get_by_inksoft_id( $inksoft_id = null ) {

		if( !$inksoft_id ) {
			return false;
		}

		$brands = self::query(
			array(
				'where'	=> array(
					array(
						'name'		=> 'inksoft_brand_id',
						'value'		=> $inksoft_id
					)
				)
			)
		);

		if( count( $brands ) > 0 ) {
			return current( $brands );
		}

		return false;
	}
}
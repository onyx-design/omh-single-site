<?php
defined( 'ABSPATH' ) || exit;

class OMH_Factory_College extends OMH_Factory {

	protected static $table = 'omh_colleges';

	protected static $model = 'OMH_Model_College';
}
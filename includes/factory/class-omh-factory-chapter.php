<?php
defined( 'ABSPATH' ) || exit;

class OMH_Factory_Chapter extends OMH_Factory {

	protected static $table = 'omh_chapters';

	protected static $model = 'OMH_Model_Chapter';

	public static function get_by_url_structure( $url_structure = null ) {

		if( !$url_structure ) {
			return false;
		}

		$chapter = self::query(
			array(
				'posts_per_page' 	=> 1,
				'where'				=> array(
					array(
						'name'		=> 'url_structure',
						'value'		=> $url_structure
					)
				)
			)
		);

		return $chapter;
	}

	/**
	 * Get orders from a single chapter
	 * 
	 * $return_type: 	ids
	 * 					count
	 * 
	 * @param 	OMH_Model_Chapter|null 	$chapter 
	 * @param 	string 					$return_type 
	 * @param 	int 					$limit
	 * @param 	int 					$page
	 * 
	 * @return 	array
	 */
	public static function get_chapter_orders( $chapter = null, $return_type = 'ids', $limit = 20, $page = 0 ) {
		global $wpdb;

		if( !$chapter ) {
			$chapter = OMH()->chapter_factory->read( $chapter );
		}

		if( !$chapter ) {
			return $return_type == 'count' ? 0 : array();
		}

		$chapter_term = $chapter->get_term();

		switch( $return_type ) {
			case 'count':
				$sql = "SELECT COUNT( DISTINCT `order_item`.`order_id` ) ";
				break;
			case 'ids':
			default:
				$sql = "SELECT DISTINCT `order_item`.`order_id` ";
				break;
		}

		$sql .= "FROM `{$wpdb->prefix}woocommerce_order_items` AS `order_item`
			LEFT JOIN `{$wpdb->prefix}woocommerce_order_itemmeta` AS `order_itemmeta`
				ON `order_itemmeta`.`order_item_id` = `order_item`.`order_item_id`
				AND `order_itemmeta`.`meta_key` = '_product_id'
			LEFT JOIN `{$wpdb->prefix}term_relationships` AS `term_relationship`
				ON `term_relationship`.`object_id` = `order_itemmeta`.`meta_value`
				AND `term_relationship`.`term_taxonomy_id` = %d
			WHERE `order_item`.`order_item_type` = 'line_item'
				AND `term_relationship`.`object_id` IS NOT NULL
			ORDER BY `order_item`.`order_id` DESC
		";

		$prepare_args = array(
			$chapter_term ? $chapter_term->term_taxonomy_id : 0
		);

		if( $limit && $limit != -1 ) {

			$limit_args = array(
				'%d',
				'%d'
			);

			$prepare_args[] = $page ?? 0;
			$prepare_args[] = $limit;

			$sql .= ' LIMIT ' . implode( ', ', $limit_args );
		}

		$prepared_sql = $wpdb->prepare( $sql, $prepare_args );

		if( $return_type == 'count' ) {
			$results = $wpdb->get_var( $prepared_sql );
		} else {
			$results = $wpdb->get_col( $prepared_sql );
		}

		return $results;
	}
}
<?php
defined( 'ABSPATH' ) || exit;

class OMH_Factory_Garment_Color extends OMH_Factory {

	protected static $table = 'omh_garment_colors';

	protected static $model = 'OMH_Model_Garment_Color';

	public static function get_by_color_name( $color_name = null ) {

		if( !$color_name ) {
			return false;
		}

		$colors = self::query(
			array(
				'where'	=> array(
					array(
						'name'		=> 'color_name',
						'value'		=> $color_name
					)
				)
			)
		);

		if( count( $colors ) > 0 ) {
			return current( $colors );
		}

		return false;
	}
}
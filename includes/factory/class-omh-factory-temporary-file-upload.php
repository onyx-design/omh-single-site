<?php
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

class OMH_Factory_Temporary_File_Upload extends OMH_Factory {

	protected static $table = 'omh_temp_uploads';

	protected static $model = 'OMH_Model_Temporary_File_Upload';
}
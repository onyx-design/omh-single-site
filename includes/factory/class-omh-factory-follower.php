<?php
defined( 'ABSPATH' ) || exit;

class OMH_Factory_Follower extends OMH_Factory {

	protected static $table = 'omh_followers';

	protected static $model = 'OMH_Model_Follower';

	

	public static function get_object_followers_count( $id, $type ) {

		return static::query(
			array(
				'where' => array(
					array(
						'name'	=> 'following_id',
						'value'	=> $id
					),
					array(
						'name'	=> 'following_type',
						'value'	=> $type
					)
				),
				'return' 	=> 'count'
			)
		);
	}

	public static function get_followers_export_csv_path() {
		return OMH_CONTENT_DIR . '/omh-followers-export.csv';
	}

	public static function get_followers_export_csv_url() {
		return OMH_CONTENT_URL . '/omh-followers-export.csv';
	}

	public static function generate_followers_export_csv() {

		global $wpdb;

		$followers = $wpdb->get_results("SELECT f.ID, f.follower_email, f.following_id, f.following_type, f.meta_created, c.chapter_name, o.org_name FROM `wp_omh_followers` f ".
			"LEFT JOIN wp_omh_chapters c ON f.following_type = 'chapter' AND f.following_id = c.ID ".
			"LEFT JOIN wp_omh_organizations o ON f.following_type = 'organization' AND f.following_id = o.ID", ARRAY_A );
		
		$csv_contents = 'Follower ID, Email, Following ID, Following Type, Date Subscribed, Chapter Name, Org Name' . "\r\n";
		foreach( $followers as $follower ) {
			$csv_contents .= implode( ',', $follower ) . "\r\n";
		}

		file_put_contents( static::get_followers_export_csv_path(), $csv_contents, LOCK_EX );
	}

	
}
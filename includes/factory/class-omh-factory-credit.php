<?php
	defined( 'ABSPATH' ) || exit;

	/**
	 * dev:todo Look over the hooks commented out here
	 */
	class OMH_Factory_Credit extends OMH_Factory {

		protected static $table = 'omh_credit';

		protected static $model = 'OMH_Model_Credit';

		public function __construct() {

			// Add Credit When an Order is made
			add_action( 'woocommerce_order_status_processing', array( $this, 'add_order_credit' ), 10, 1 );

			// Debit for Retail Order Refunds/Cancellations
			add_action( 'woocommerce_order_status_cancelled', array( $this, 'retail_order_debit' ), 10, 1 );
			add_action( 'woocommerce_order_status_refunded', array( $this, 'retail_order_debit' ), 10, 1 );

			/**
			 * Line Items
			 **/
			// add_action( 'woocommerce_checkout_create_order_line_item', array( $this, 'add_order_line_item_chapter' ), 10, 4 );
			add_filter( 'woocommerce_get_order_item_classname', array( $this, 'order_item_credit_classname' ), 10, 3 );
			add_filter( 'woocommerce_get_items_key', array( $this, 'order_item_credit_key' ), 10, 2 );
			add_filter( 'woocommerce_order_type_to_group', array( $this, 'order_type_credit_to_group' ), 10, 1 );
			add_filter( 'woocommerce_data_stores', array( $this, 'credit_wc_data_stores' ), 10 );

			/**
			 * Checkout
			 **/
			add_filter( 'woocommerce_billing_fields', array( $this, 'checkout_add_credit_field' ), 10, 1 );
			add_filter( 'woocommerce_form_field', array( $this, 'checkout_credit_field' ), 10, 4 );
			add_filter( 'woocommerce_update_order_review_fragments', array( $this, 'checkout_reload_billing_fields' ), 10, 1 );
			add_filter( 'woocommerce_checkout_get_value', array( $this, 'checkout_field_value' ), 10, 2 );
			add_action( 'woocommerce_checkout_update_order_review', array( $this, 'checkout_add_store_credit_meta' ) );
			add_action( 'woocommerce_review_order_before_order_total', array( $this, 'checkout_display_applied_store_credit' ) );
			add_action( 'woocommerce_after_checkout_validation', array( $this, 'checkout_validate_store_credit' ), 10, 2 );

			add_filter( 'woocommerce_get_order_item_totals', array( $this, 'order_details_store_credit_display' ), 10, 3 );

			add_filter( 'woocommerce_cart_get_total', array( $this, 'checkout_update_cart_total' ), 10, 1 );
			add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'checkout_custom_order_meta' ), 10, 2 );

			add_filter( 'woocommerce_cart_needs_payment', array( $this, 'checkout_free_order' ), 100, 2 );
			add_action( 'woocommerce_payment_complete', array( $this, 'checkout_order_complete' ), 10, 1 );

			/**
			 * Admin
			 **/
			add_action( 'woocommerce_admin_order_totals_after_shipping', array( $this, 'admin_display_store_credit_totals' ), 10, 1 );
		}

		public function get_by_chapter_and_order( $chapter_id, $order_id ) {

			return $this->query(
				array(
					'posts_per_page' => -1,
					'where'          => array(
						array(
							'name'  => 'chapter_id',
							'value' => $chapter_id,
						),
						array(
							'name'  => 'order_id',
							'value' => $order_id,
						),
					),
				)
			);
		}

		public function get_credit_by_chapter_and_order( $chapter_id, $order_id ) {

			return $this->query(
				array(
					'posts_per_page' => -1,
					'where'          => array(
						array(
							'name'  => 'type',
							'value' => 'credit',
						),
						array(
							'name'  => 'chapter_id',
							'value' => $chapter_id,
						),
						array(
							'name'  => 'order_id',
							'value' => $order_id,
						),
					),
				)
			);
		}

		public function debit( $amount, $order_id = 0, $chapter_id = 0 ) {

			if ( $amount && is_numeric( $amount ) ) {

				if ( $order = wc_get_order( $order_id ) ) {

					$ledger_date = DateTime::createFromFormat( 'U', time() );

					if ( !$chapter_id && $chapter = OMH_Session::get_chapter() ) {

						$chapter_id = $chapter->get_id();
					}

					$debit_args = array(
						'type'        => 'debit',
						'amount'      => ( -1 * ( $amount * 100 ) ),
						'order_id'    => $order_id,
						'chapter_id'  => $chapter_id,
						'notes'       => '',
						'ledger_date' => $ledger_date->format( 'Y-m-d H:i:s' ),
					);

					if ( $inserted = self::create( $debit_args ) ) {

						$order->add_order_note( wc_price( $amount ) . ' of store credit applied to order.' );
					} else {

						$order->add_order_note( 'There was a problem saving applied store credit.' );
					}

					$order->save();
				}
			}
		}

		public function credit( $amount, $order_id = 0, $chapter_id = 0 ) {

			if ( $amount && is_numeric( $amount ) ) {

				if ( $order = wc_get_order( $order_id ) ) {

					$ledger_date = DateTime::createFromFormat( 'U', time() );

					if ( !$chapter_id && $chapter = OMH_Session::get_chapter() ) {

						$chapter_id = $chapter->get_id();
					}

					$credit_args = array(
						'type'        => 'credit',
						'amount'      => ( $amount * 100 ),
						'order_id'    => $order_id,
						'chapter_id'  => $chapter_id,
						'notes'       => '',
						'ledger_date' => $ledger_date->format( 'Y-m-d H:i:s' ),
					);

					if ( $inserted = self::create( $credit_args ) ) {

						$order->add_order_note( wc_price( $amount ) . " of Store Crdit added to chapter_id: {$chapter_id}" );
					} else {

						$order->add_order_note( "There was a problem adding store credit for chapter_id: {$chapter_id}" );
					}

					$order->save();
				}
			}
		}

		public function add_order_line_item_chapter( $item, $cart_item_key, $values, $order ) {

			$product_variation = $item->get_product();
			$product_variable  = $product_variation->get_product();
			$product_chapter   = $product_variable->get_chapter();

			if ( $product_chapter ) {
				wc_add_order_item_meta( $item->get_id(), '_chapter_id', $product_chapter->get_id() );
			}

			$item->save();
		}

		/**
		 * Remove all rows from the OMH Credit table where the credit type is "Credit"
		 *
		 * @return 	bool
		 */
		public static function reset_credits( $chapter_id = 0 ) {

			if ( !$chapter_id ) {

				if ( $chapter = OMH_Session::get_chapter() ) {
					$chapter_id = $chapter->get_id();
				} else {
					return false;
				}
			}

			global $wpdb;

			return $wpdb->delete( $wpdb->prefix . 'omh_credit', array( 'type' => 'credit', 'chapter_id' => $chapter_id ) );
		}

		public static function get_total_credit( $chapter_id = 0, $raw = true ) {
			global $wpdb;

			if ( !$chapter_id ) {

				if ( $chapter = OMH_Session::get_chapter() ) {
					$chapter_id = $chapter->get_id();
				} else {
					return 0;
				}
			}

			$query = "SELECT SUM(amount) FROM {$wpdb->prefix}omh_credit WHERE chapter_id = {$chapter_id}";

			$amount = $wpdb->get_var( $query ) ?: 0;

			if ( $raw || !$amount ) {
				return $amount;
			}

			// Return total store credit in dollars
			return $amount / 100;
		}


		/**
		 * Calculate earnings per chapter from an order
		 *
		 * @param [type] $order_id
		 * @param [type] $credit_type
		 * @param string $write_mode
		 * @return void
		 */
		public function calculate_order_credit( $order_id, $credit_type, $save_mode = 'create' ) {
			
			$order_credit = array();

			if ( $order = wc_get_order( $order_id ) ) {

				// if ( $order->can_add_chapter_credit() ) {

					foreach ( $order->get_items() as $line_item ) {

						$product_variation = $line_item->get_product();

						if (  ( $product_variation instanceof OMH_Product_Variation )
							&& ( $product_variable = wc_get_product( $product_variation->get_parent_id() ) )
							&& ( $product_garment = $product_variation->get_product_garment() )
						) {

							$chapter = $product_variable->get_chapter();

							$line_item_credit = 0;

							// If Campaign Product, calculate using base price and retail price
							if ( $product_variable->get_sales_type() === 'campaign' ) {
								$line_item_credit = ( $product_garment->get_retail_price() - $product_garment->get_base_price() ) * $line_item->get_quantity();
							
							} elseif ( $product_variable->get_sales_type() === 'retail' ) {
								$line_item_credit = $line_item->get_total() * ( $product_variable->get_earnings_percentage() / 100 );
							}

							if ( $line_item_credit ) {

								$chapter_id = $chapter ? $chapter->get_id() : 0;

								if ( !isset( $order_credit[$chapter_id] ) ) {
									$order_credit[$chapter_id] = 0;
								}

								// Save credit in cents
								$order_credit[$chapter_id] += ( $line_item_credit * 100 );
							}
						}
					}
				// }
			}

			// Round amounts
			$order_credit = array_map( 'round', $order_credit );

			if( $order_credit && $order && $save_mode ) {
				$this->save_order_credit( $order, $order_credit, $credit_type );
			}

			return $order_credit;
		}

		
		protected function save_order_credit( $order, $order_credit, $credit_type ) {

			foreach ( $order_credit as $credit_chapter_id => $order_credit_line ) {

				// dev:improve This might not be required now with the decoupled flow
				// Do not give credit to chapters if they are using store credit
				if ( $order && $order->get_applied_store_credit() && $user = $order->get_user() ) {

					$chapter = $user->get_chapter();

					if ( $chapter->get_id() == $credit_chapter_id ) {
						continue;
					}
				}

				// Check if credit has already been generate for this order
				$credit_exists = $this->get_credit_by_chapter_and_order( $credit_chapter_id, $order->get_id() );

				if ( !empty( $credit_exists ) ) {
					continue;
				}

				// Determine order note strings
				switch ( $credit_type ) {
					case 'credit':
						$success_verb = 'added';
						$error_verb = 'adding';
					break;
					case 'debit':
						$order_credit_line = -$order_credit_line;
						$success_verb = 'removed';
						$error_verb = 'removing';
					break;
				}

				$credit_args = array(
					'type'        => $credit_type,
					'amount'      => $order_credit_line,
					'order_id'    => $order ? $order->get_id() : null,
					'chapter_id'  => $credit_chapter_id,
					'notes'       => '',
					'ledger_date' => $order ? $order->get_date_paid( 'Y-m-d H:i:s' ) : null,
				);

				if ( $inserted = self::create( $credit_args ) ) {
					$order->add_order_note( wc_price( abs( $order_credit_line ) / 100 ) . " of Store Credit {$success_verb} to chapter_id: {$credit_chapter_id}" );
				} else {
					$order->add_order_note( "There was a problem {$error_verb} store credit for chapter_id: {$credit_chapter_id}" );
				}
			}
		}

		/**
		 * Hook to run when a retail or campaign order is paid
		 *
		 * @param 	int 	$order_id
		 *
		 * @return	null
		 */
		public function add_order_credit( $order_id ) {
			$this->calculate_order_credit( $order_id, "credit" );
		}

		/**
		 * Hook to run when a retail order is cancelled/refunded
		 *
		 * @param 	int 	$order_id
		 *
		 * @return	null
		 */
		function retail_order_debit( $order_id ) {
			$this->calculate_order_credit( $order_id, "debit" );
		}

		/**
		 *
		 *
		 * @param 	string 	$classname
		 * @param 	string 	$item_type
		 * @param 	int 	$id
		 *
		 * @return 	string
		 */
		function order_item_credit_classname( $classname, $item_type, $id ) {

			if ( 'credit' === $item_type ) {
				return 'OMH_Order_Item_Credit';
			}

			return $classname;
		}

		/**
		 *
		 *
		 * @param 	string 			$key
		 * @param 	WC_Order_Item 	$item
		 *
		 * @return 	string
		 */
		function order_item_credit_key( $key, $item ) {

			if ( is_a( $item, 'OMH_Order_Item_Credit' ) ) {
				return 'credit_lines';
			}

			return $key;
		}

		/**
		 * Tell WooCommerce what group Store Credit is included in
		 *
		 * @param 	array 	$groups
		 *
		 * @return 	array
		 */
		function order_type_credit_to_group( $groups ) {

			$credit = array(
				'credit' => 'credit_lines',
			);

			return array_merge( $groups, $credit );
		}

		/**
		 * Tell WooCommerce that Store Credit uses its own Data Store
		 *
		 * @param 	array 	$stores
		 *
		 * @return 	array
		 */
		function credit_wc_data_stores( $stores ) {
			$stores['order-item-credit'] = 'OMH_Order_Item_Credit_Data_Store';

			return $stores;
		}

		/**
		 * CHECKOUT
		 **/

		/**
		 * Add chapter store credit field to checkout
		 *
		 * @param 	array 	$fields
		 *
		 * @return 	array
		 */
		function checkout_add_credit_field( $fields = array() ) {
			global $post;

			$order = wc_get_order( $post->ID );
			$user  = wp_get_current_user();

			if ( $order && $order->has_bulk_product() && $user->can_use_store_credit() ) {

				$chapter = OMH_Session::get_chapter();

				$available_store_credit = $chapter->get_available_store_credit( false ) ?: 0;

				$fields['order_chapter_store_credit'] = array(
					'label'             => "Apply Chapter's Store Credit",
					'type'              => 'number',
					'class'             => array( 'form-row-wide', 'update_totals_on_change' ),
					'description'       => wc_price( $available_store_credit ) . ' of Chapter store credit available',
					'placeholder'       => 'Use store credit from past orders',
					'priority'          => 200,
					'validate'          => array( 'store-credit' ),
					'custom_attributes' => array(
						'min'  => 0,
						'max'  => $available_store_credit,
						'step' => 0.01,
					),
					'required'          => false,
				);
			}

			return $fields;
		}

		function checkout_credit_field( $field, $key, $args, $value ) {

			if ( $key === 'order_chapter_store_credit' ) {

				// $field = str_replace( 'aria-hidden="true"', '', $field );
				$field = str_replace( 'description', 'checkout-description', $field );
			}

			return $field;
		}

		/**
		 * If a user can apply store credit, we want to make sure to refresh the billing field values
		 * so that we can validate and check store credit every chance we get
		 *
		 * @param 	array 	$cart_fragments
		 *
		 * @return 	array
		 */
		function checkout_reload_billing_fields( $cart_fragments ) {

			$user = wp_get_current_user();

			if ( $user->can_use_store_credit() ) {

				ob_start();
				WC()->checkout->checkout_form_billing();
				$woocommerce_billing_fields = ob_get_clean();

				$cart_fragments['.woocommerce-billing-fields'] = $woocommerce_billing_fields;
			}

			return $cart_fragments;
		}

		/**
		 * On checkout, if posted data is detected, we need to autofill those values
		 * @see
		 *
		 * @param 	null|string 	$value
		 * @param 	string 			$input
		 *
		 * @return 	string 			$value
		 */
		function checkout_field_value( $value, $input ) {

			// Only replace the value if it exists in the posted data, and a value isn't already set
			if ( !$value && isset( $_POST['post_data'] ) && $_POST['post_data'] ) {

				// Post Data is a http query string, so we need to parses it
				$data = array();
				parse_str( $_POST['post_data'], $data );

				if ( isset( $data[$input] ) && $data[$input] ) {

					switch ( $input ) {
					// Sanitize and validate store credit
					case 'order_chapter_store_credit':
						return $this->sanitize_chapter_store_credit( $data[$input] );
						break;
					default:
						return $data[$input];
						break;
					}
				}
			}

			return $value;
		}

		/**
		 * If store credit is posted, add custom meta to the WooCommerce
		 *
		 * @param 	string 	$posted_data
		 */
		function checkout_add_store_credit_meta( $posted_data ) {

			$user = wp_get_current_user();

			if ( $user->can_use_store_credit() ) {

				$data = array();
				parse_str( $posted_data, $data );

				if ( isset( $data['order_chapter_store_credit'] ) ) {

					WC()->session->set( 'applied_store_credit', ( $data['order_chapter_store_credit'] ? $this->sanitize_chapter_store_credit( $data['order_chapter_store_credit'] ) : null ) );
					WC()->cart->store_credit = $this->sanitize_chapter_store_credit( $data['order_chapter_store_credit'] ?: 0 );
				} else {
					WC()->cart->store_credit = 0;
				}
			}
		}

		/**
		 * Display applied chapter store credit on checkout page
		 */
		function checkout_display_applied_store_credit() {
			global $post;

			$order = wc_get_order( $post->ID );
			$user  = wp_get_current_user();

			if ( $order && $order->has_bulk_product() && $user->can_use_store_credit() ) {

				$applied_store_credit = isset( WC()->cart->store_credit ) ? ( -1 * WC()->cart->store_credit ) : 0;
			?>
			<tr class="cart-store-credit">
				<th>Store Credit</th>
				<td><?php echo wc_price( $applied_store_credit ); ?></td>
			</tr>
		<?php
			}
				}

				/**
				 * Validate store credit on checkout
				 *
				 * @param 	array 	$data
				 * @param 	array 	$errors
				 */
				function checkout_validate_store_credit( $data, $errors ) {

					foreach ( WC()->checkout()->get_checkout_fields() as $fieldset_key => $fieldset ) {

						foreach ( $fieldset as $key => $field ) {

							if ( isset( $field['validate'] ) && in_array( 'store-credit', $field['validate'] ) ) {

								$user = wp_get_current_user();

								if ( !empty( $data[$key] ) && $user->can_use_store_credit() ) {

									// $chapter = $user->get_chapter();
									$chapter = OMH_Session::get_chapter();

									$available_store_credit = $chapter->get_available_store_credit( false );
									$applied_store_credit   = $data[$key];

									// dev:improve dev:todo Calculate total with subtotal and tax as we're changing the carts total on the fly
									$cart_total = WC()->cart->get_subtotal( 'raw' ) + WC()->cart->get_total_tax( 'raw' ) + WC()->cart->get_shipping_total( 'raw' );

									if ( $applied_store_credit < 0 ) {

										$errors->add( 'validation', "You cannot add negative credit to your order." );
									}

									// If there isn't any available store credit, return an error
									if ( !( $available_store_credit ) || ( $available_store_credit < 0 ) ) {

										$errors->add( 'validation', "You do not have enough store credit." );
									}

									// If the applied store credit is more than the available store credit, return an error
									if ( $applied_store_credit > $available_store_credit ) {

										$errors->add( 'validation', "You do not have enough store credit." );
									}

									// If applied store credit is more than the cart total, return an error
									if ( $applied_store_credit > $cart_total ) {

										$errors->add( 'validation', "You cannot use more credit than the total cost of the order." );
									}
								}
							}
						}
					}
				}

				function order_details_store_credit_display( $total_rows, $order, $tax_display ) {

					$new_total_rows = array();

					foreach ( $total_rows as $total_row_key => $total_row ) {

						if (  ( $total_row_key == 'order_total' ) && $order->get_applied_store_credit() ) {
							$new_total_rows['store_credit'] = array(
								'label' => 'Applied Store Credit:',
								'value' => wc_price( $order->get_applied_store_credit( true ) ),
							);
						}

						$new_total_rows[$total_row_key] = $total_row;
					}

					return $new_total_rows;
				}

				/**
				 * Sanitize/Validate store credit based on card total and applied credit
				 *
				 * @param 	string 	$applied_store_credit
				 *
				 * @return 	string
				 */
				function sanitize_chapter_store_credit( $applied_store_credit ) {

					$user = wp_get_current_user();

					if ( $user->can_use_store_credit() && is_numeric( $applied_store_credit ) ) {

						// $chapter = $user->get_chapter();
						$chapter = OMH_Session::get_chapter();

						$available_store_credit = $chapter->get_available_store_credit( false );

						$cart_total = WC()->cart->get_subtotal( 'raw' ) + WC()->cart->get_total_tax( 'raw' ) + WC()->cart->get_shipping_total( 'raw' );

						// If there isn't any available store credit, return 0
						if ( !( $available_store_credit ) || ( $available_store_credit < 0 ) || ( $applied_store_credit < 0 ) ) {
							$applied_store_credit = 0;
						}

						// If the applied store credit is more than the available store credit, return the available store credit
						if ( $applied_store_credit > $available_store_credit ) {
							$applied_store_credit = $available_store_credit;
						}

						// If applied store credit is more than the cart total, return the cart total
						if ( $applied_store_credit > $cart_total ) {
							$applied_store_credit = $cart_total;
						}

						// // If there isn't any available store credit, return 0
						// if( !( $available_store_credit ) || ( $available_store_credit < 0 ) || ( $applied_store_credit < 0 ) ) {
						// 	return 0;
						// // If the applied store credit is more than the available store credit, return the available store credit
						// } elseif( $applied_store_credit > $available_store_credit ) {
						// 	return $available_store_credit;
						// // If applied store credit is more than the cart total, return the cart total
						// } elseif( $applied_store_credit > $cart_total ) {
						// 	return $cart_total;
						// }

						return $applied_store_credit;
					}

					return 0;
				}

				/**
				 * Display Store Credit on the admin order screen
				 *
				 * @param 	int 	$order_id
				 */
				function admin_display_store_credit_totals( $order_id ) {

					$order = wc_get_order( $order_id );

					// If order exists, always display store credit, whether any was used or not.
					if ( $order ) {

						$applied_store_credit = $order->get_applied_store_credit( true );
					?>
			<tr>
				<td class="label">Store Credit</td>
				<td width="1%"></td>
				<td class="store-credit">
					<?php echo wc_price( $applied_store_credit ); ?>
				</td>
			</tr>
<?php
	}
		}

		/**
		 * Update the cart total
		 *
		 * @param 	string 	$total
		 *
		 * @return 	string
		 */
		function checkout_update_cart_total( $total ) {

			if ( isset( WC()->cart->store_credit ) && is_numeric( WC()->cart->store_credit ) ) {
				$total = $total - $this->sanitize_chapter_store_credit( WC()->cart->store_credit );
			}

			return $total;
		}

		function checkout_custom_order_meta( $order_id, $data ) {

			if ( isset( $data['order_chapter_store_credit'] ) ) {

				$sanitized_store_credit = $this->sanitize_chapter_store_credit( $data['order_chapter_store_credit'] );

				$order   = wc_get_order( $order_id );
				$chapter = OMH_Session::get_chapter();

				$order->set_applied_store_credit( $sanitized_store_credit );
				$order->set_total( $order->get_total() - $sanitized_store_credit );
				$order->save();

				self::debit( $sanitized_store_credit, $order_id, $chapter->get_id() );
			}
		}

		function checkout_free_order( $requires_payment, $cart ) {

			// If we're in the context of a payment gateway, we need to check that store credit is in the session
			if ( !property_exists( $cart, 'store_credit' ) ) {

				$session_credit = WC()->session->get( 'applied_store_credit' ) ?: 0;

				if (  ( $cart->get_total( 'raw' ) - $session_credit ) <= 0 ) {
					$requires_payment = false;
				}
			}

			return $requires_payment;
		}

		/**
		 * If an order has been successfully checked out, remove the session store credit just in case
		 *
		 * @param 	int 	$order_id
		 */
		function checkout_order_complete( $order_id ) {

			if ( WC()->session->get( 'applied_store_credit' ) ) {
				WC()->session->set( 'applied_store_credit', null );
			}
		}
}

<?php
/* 
Abstract singleton class
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

trait OMH_Singleton_Class_Abstract {

	/**
	 * The single instance of the class.
	 *
	 */
	protected static $_instance = null;

	/**
	 * Get Singleton Class' Instance.
	 *
	 * Ensures only one instance of class is loaded or can be loaded.
	 */
	public static function instance() {
		
		if( is_null( static::$_instance ) ) {
			$class = get_called_class();
			static::$_instance = new $class();
		}
		return static::$_instance;
	}

	/**
	 * Cloning is forbidden.
	 */
	public function __clone() {
		//wc_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'woocommerce' ), '2.1' );
		// Need to add error handling at the request level //dev:results //dev:error_handling //dev:improve
		return false;
	}

	/**
	 * Unserializing instances of this class is forbidden.
	 */
	public function __wakeup() {
		//wc_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'woocommerce' ), '2.1' );
		// Need to add error handling at the request level //dev:results //dev:error_handling //dev:improve
		return false;
	}

	/**
	 * Magic method for easy static access
	 * /
	public static function __callStatic( $name, $arguments ) {

		//static::instance()
	}
	/* */

	/**
	 * Constructor must be inaccessible
	 */
	protected function __construct() {
	}
}
<?php
defined('ABSPATH') || exit;

class OMH_Order_Manager
{

	public static function init()
	{

		/**
		 * Override WC Order Class
		 * 
		 * @see WC_Order_Factory::get_order()
		 */
		add_filter('woocommerce_order_class', array(__CLASS__, 'order_class'), 10, 3);

		add_action('woocommerce_payment_complete', array(__CLASS__, 'save_related_chapters'), 10, 1);
		add_action('woocommerce_payment_complete', array(__CLASS__, 'save_related_products'), 10, 1);
		add_action('woocommerce_payment_complete', array(__CLASS__, 'save_referral_code'), 10, 1);

		add_action('woocommerce_order_status_cancelled', array(__CLASS__, 'handle_cancelled_order'), 10, 1);
	}

	public static function order_class($classname, $order_type, $order_id = null)
	{

		if ('shop_order' === $order_type) {
			return "OMH_Order";
		}

		return $classname;
	}

	public static function is_bulk_order($order_id)
	{

		if (!('shop_order' == get_post_type($order_id))) {
			return false;
		}

		$bulk_product_id = get_post_meta($order_id, 'bulk_product_id', true);

		if (!$bulk_product_id) {
			return false;
		}

		$bulk_product = wc_get_product($bulk_product_id);

		if ($bulk_product->get_sales_type() !== 'bulk') {
			return false;
		}

		return true;
	}

	public static function save_related_chapters($order_id)
	{

		if ($order = wc_get_order($order_id)) {

			$related_chapters = $order->generate_related_chapters();

			update_post_meta($order->get_id(), '_related_chapters', $related_chapters);
		}
	}

	public static function save_related_products($order_id)
	{

		if ($order = wc_get_order($order_id)) {

			$related_retail_products = $order->generate_related_retail_products();
			$related_campaign_products = $order->generate_related_campaign_products();

			if (!empty($related_retail_products)) {
				update_post_meta($order->get_id(), '_related_retail_products', $related_retail_products);
			}

			if (!empty($related_campaign_products)) {
				update_post_meta($order->get_id(), '_related_campaign_products', $related_campaign_products);
			}
		}
	}

	public static function save_referral_code($order_id) {
		if ($order = wc_get_order($order_id)) {
			if(isset($_COOKIE['referral'])){
				$referral_code = $_COOKIE['referral'];
				update_post_meta($order->get_id(), '_referral', $referral_code );
				unset($_COOKIE['referral']);
			}
		}
	}

	public static function handle_cancelled_order($order_id)
	{
		// Changes Order Quantity -- Store Credit is handled in omh-factory-credit
		if ($order = wc_get_order($order_id)) {
			$order_items = $order->get_items();

			foreach ($order_items as $order_item) {

				// Change Order Quantity
				$product = wc_get_product($order_item["product_id"]);
				$quantity = $order_item["quantity"];
				$product_sales = $product->get_total_sales();
				if ($product_sales > 0) {
					$product->set_total_sales($product_sales - $quantity);
					$product->save();
				}
			}
		}
	}
}
OMH_Order_Manager::init();

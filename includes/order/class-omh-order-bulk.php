<?php
defined( 'ABSPATH' ) || exit;

class OMH_Order_Bulk extends OMH_Order {
	
	protected $sales_type = 'bulk';

	/**
	 * Sales Type
	 * 
	 * @return 	string
	 */
	public function get_sales_type() {
		return $this->sales_type;
	}

	/********************************************
	 * 			  	CREDIT FUNCTIONS 			*
	 ********************************************/

	/**
	 * Return whether or not this order can have chapter credit added
	 * 
	 * @return 	bool
	 */
	public function can_add_chapter_credit() {
		return true;
	}

	/********************************************
	 * 			  	PRODUCT FUNCTIONS 			*
	 ********************************************/

	/**
	 * @tag Bulk Order
	 * Get bulk product id by using get meta method
	 * @return int
	*/
	public function get_bulk_product_id() {
		return $this->get_meta('bulk_product_id');
	}

	/**
	 * @tag Bulk Order
	*/
	public function set_bulk_product_id( $bulk_product_id = 0 ) {
		$this->update_meta_data( 'bulk_product_id', $bulk_product_id );

		return $this->get_bulk_product_id();
	}

	/**
	 * @tag Bulk Order
	*/
	public function get_bulk_product() {
		return wc_get_product( $this->get_bulk_product_id() );
	} 

	/**
	 * @param Array - an array of product variation ids prefixed with 'qty' as the key and thier respective quantities as the value
	 * @param Bool - used as a guard to remove all line item quantities before creating new ones 
	 * 
	 * @return Array
	 */
	public function set_bulk_order_quantities( $bulk_order_quantities = array(), $replace_all = true ) {

		if( $replace_all ) {
			// Clear Order Quantities
			$this->remove_order_items( 'line_item' );
		}

		foreach( $bulk_order_quantities as $product_variation_id => $product_variation_qty ) {

			if( !$product_variation_qty ) {
				continue;
			}

			$product_variation = wc_get_product( str_replace( 'qty_', '', $product_variation_id ) );

			$order_item_id = $this->add_product(
				$product_variation,
				$product_variation_qty
			);
		}

		$this->calculate_totals();

		return $this->get_items( 'line_item' );
	}

	/********************************************
	 * 			  	HELPER FUNCTIONS 			*
	 ********************************************/

	 /**
	  * @tag Bulk Order
	  * @param Integer - the product it to create the bulk order from
	  * 
	  * @return OMH_Order_Bulk
	  */
	 public static function create_bulk_order( $product_id ) {

		if( !$product_id ) {
			return null;
		}

		$product = wc_get_product( $product_id );

		if( $product->get_sales_type() !== 'bulk' ) {
			return null;
		}

		// get the classname what this php function is being called and instantiate it
		$classname = get_called_class();
		$bulk_order = new $classname();

		// set bulk product id with the product id param and save this "links" the order with the product
		$bulk_order->set_bulk_product_id( $product_id );
		$bulk_order->save();

		// set active bulk order with the bulk order generated id and save this "links" product with order
		$product->set_active_bulk_order_id( $bulk_order->get_id() );
		$product->save();

		return $bulk_order;
	 }
}
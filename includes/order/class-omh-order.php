<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Order extends WC_Order {

	public function __construct( $order = 0 ) {

		try {
			parent::__construct( $order );
		} catch( Exception $e ) {
			return false;
		}
	}

	public function calculate_totals( $and_taxes = true ) {

		$cart_subtotal      = 0;
		$cart_total         = 0;
		$fee_total          = 0;
		$credit_total		= 0;
		$shipping_total     = 0;
		$cart_subtotal_tax  = 0;
		$cart_total_tax     = 0;

		// Maybe calculate bulk line item prices
		if( $this->has_bulk_product() ) {
			$this->set_bulk_line_item_totals();
		}

		// Sum line item costs.
		foreach ( $this->get_items() as $item ) {

			$cart_subtotal += $item->get_subtotal();
			$cart_total    += $item->get_total();
		}

		// Sum shipping costs.
		foreach ( $this->get_shipping_methods() as $shipping ) {

			$shipping_total += $shipping->get_total();
		}

		$this->set_shipping_total( $shipping_total );

		// Sum fee costs.
		foreach ( $this->get_fees() as $item ) {

			$amount = $item->get_amount();

			if ( 0 > $amount ) {

				$item->set_total( $amount );
				$max_discount = round( $cart_total + $fee_total + $shipping_total, wc_get_price_decimals() ) * -1;

				if ( $item->get_total() < $max_discount ) {

					$item->set_total( $max_discount );
				}
			}

			$fee_total += $item->get_total();
		}

		// Calculate taxes for items, shipping, discounts.
		if ( $and_taxes ) {

			$this->calculate_taxes();
		}

		// Sum taxes.
		foreach ( $this->get_items() as $item ) {

			$cart_subtotal_tax += $item->get_subtotal_tax();
			$cart_total_tax    += $item->get_total_tax();
		}

		// Sum store credit.
		foreach( $this->get_credits() as $item ) {

			$amount = $item->get_amount();

			if( 0 > $amount ) {

				$item->set_total( $amount );
			}

			$credit_total += $item->get_total();
		}

		$this->set_discount_total( $cart_subtotal - $cart_total );
		$this->set_discount_tax( $cart_subtotal_tax - $cart_total_tax );
		$this->set_total( round( $cart_total + $fee_total + $credit_total + $this->get_shipping_total() + $this->get_cart_tax() + $this->get_shipping_tax(), wc_get_price_decimals() ) );
		$this->save();

		return $this->get_total();
	}

	/**
	 * Get Order Customer's role as a badge
	 * 
	 * dev:improve Will the guest badge ever show? Does $this->get_user() always return a user?
	 * 
	 * @return 	OMH_HTML_UI_Badge
	 */
	function get_user_role_badge() {

		if( $order_user = $this->get_user() ) {
			$order_user_role = $order_user->get_house_role_badge();

			if ( $order_user_role ) {
				return $order_user_role->render();
			}
			else {
				return OMH_HTML_UI_Badge::factory(
					array(
						'label' => 'Guest',
						'color' => 'secondary'
					)
				)->render();
			}
		}
	}

	/**
	 * Get the Order status to be displayed on the frontend
	 * 
	 * @return type
	 */
	public function get_order_status_label( $return_slug = false ) {

		$status_map = array(
			'pending'		=> 'Pending',
			'failed'		=> 'Failed',
			'refunded'		=> 'Refunded',
			'cancelled'		=> 'Cancelled',
			'completed'		=> 'Completed',
			'on-hold'		=> 'On Hold',
			'processing'	=> 'Processing'
		);

		$status = $this->get_status();

		if( $return_slug ) {

			return $status;
		}

		return $status_map[ $status ];
	}

	/**
	 * Return the data for displaying the order status badge
	 * 
	 * @param 	array 	$custom_status_status
	 * 
	 * @return 	array
	 */
	function get_order_status_badge( $custom_status = array() ) {

		$status_colors = array(
			'Submissions Open' 		=> 'primary',
			'Open' 					=> 'info',
			'Submissions Closed'	=> 'info',
			'In Production' 		=> 'warning',
			'Processing'			=> 'warning',
			'Design In Review' 		=> 'warning',
			'Completed' 			=> 'success',			
			'Design Approved'		=> 'success',			
			'Cancelled'				=> 'secondary',
			'Submissions Cancelled' => 'secondary'
		);

		$order_status_status = $this->get_order_status_label();

		$order_status_text = $order_status_status;

		if( !empty( $custom_status ) && isset( $custom_status[ $order_status_status ] ) ) {
			$order_status_text = $custom_status[ $order_status_status ];
		}

		return array(
			'text' 		=> $order_status_text,
			'color' 	=> $status_colors[$order_status_status]
		);
	}

	public function get_chapter_line_items( $chapter = null ) {

		if( !$chapter ) {
			$chapter = OMH_Session::get_chapter();
		}

		if( !$chapter ) {
			return 0;
		}

		$line_items = array();

		foreach( $this->get_items( 'line_item' ) as $line_item ) {

			$product = wc_get_product( $line_item->get_product_id() );
			$product_chapter = $product->get_chapter();

			if( $product_chapter && ( $product_chapter->get_id() === $chapter->get_id() ) ) {
				$line_items[] = $line_item;
			}
		}

		return $line_items;
	}

	public function get_chapter_earnings( $chapter = null ) {

		if( !$chapter ) {
			$chapter = OMH_Session::get_chapter();
		}

		if( !$chapter ) {
			return 0;
		}

		$earnings = 0;

		$credits = OMH()->credit_factory->get_by_chapter_and_order( $chapter->get_id(), $this->get_id() );

		foreach( $credits as $credit ) {
			$earnings += $credit->get_amount();
		}

		return $earnings / 100;
	}

	/********************************************
	 * 			  	META FUNCTIONS 				*
	 ********************************************/

	public function get_tracking_url() {

		$tracking_number = $this->get_tracking();
		$tracking_carrier = $this->get_tracking_carrier();

		if(empty($tracking_number)){
			return false;
		}
		switch($tracking_carrier) {
			case "USPS":
				return "https://tools.usps.com/go/TrackConfirmAction?tLabels={$tracking_number}"; // USPS LINK
			case "UPS":
				return "https://www.ups.com/track?loc=en_US&tracknum={$tracking_number}"; // UPS Link
			default:
				return "https://www.google.com/search?q={$tracking_number}";
		}
	}

	public function get_tracking() {
		return $this->get_meta( '_mh_order_tracking_url', true );
	}

	public function set_tracking( $tracking_url, $tracking_carrier = null ) {

		$tracking_url = $this->update_meta_data( '_mh_order_tracking_url', $tracking_url );

		if( $tracking_carrier ) {

			$this->set_tracking_carrier( $tracking_carrier );
		}

		return $this->get_tracking();
	}

	public function get_tracking_carrier() {

		return $this->get_meta( '_mh_order_tracking_carrier', true );
	}

	public function set_tracking_carrier( $tracking_carrier ) {

		$tracking_carrier = $this->update_meta_data( '_mh_order_tracking_carrier', $tracking_carrier );

		return $this->get_tracking_carrier();
	}
	
	public function generate_related_retail_products() {

		$related_retail_products = array();

		foreach( $this->get_items() as $line_item ) {

			$product = wc_get_product( $line_item->get_product_id() );

			if( $product && ( $product->get_sales_type() === 'retail' ) ) {

				$related_retail_products[ $product->get_id() ] = $product->get_id();
			}
		}

		return $related_retail_products;
	}

	public function generate_related_campaign_products() {

		$related_campaign_products = array();

		foreach( $this->get_items() as $line_item ) {

			$product = wc_get_product( $line_item->get_product_id() );

			if( $product && ( $product->get_sales_type() === 'campaign' ) ) {

				$related_campaign_products[ $product->get_id() ] = $product->get_id();
			}
		}

		return $related_campaign_products;
	}

	public function generate_related_chapters() {

		$related_chapters = array();

		foreach( $this->get_items() as $line_item ) {

			$product = wc_get_product( $line_item->get_product_id() );
			$product_chapter = $product->get_chapter();

			if( $product_chapter ) {

				$related_chapters[ $product_chapter->get_id() ] = strval( $product_chapter->get_id() );
			}
		}

		return $related_chapters;
	}

	public function get_related_chapters() {

		return $this->get_meta( '_related_chapters', true );
	}

	public function set_related_chapters( $related_chapters = array() ) {

		$this->update_meta_data( '_related_chapters', $related_chapters );

		return $this->get_related_chapters();
	}

	/********************************************
	 * 			  	CREDIT FUNCTIONS 			*
	 ********************************************/

	/**
	 * Return whether or not this order can have chapter credit added
	 * 
	 * @return 	bool
	 */
	public function can_add_chapter_credit() {

		if( $this->has_bulk_product() ) {
			return false;
		}

		return true;
	}

	public function get_applied_store_credit( $return_negative = false ) {

		$applied_store_credit = $this->get_meta( '_applied_store_credit', true ) ?: 0;

		return $return_negative ? ( -1 * $applied_store_credit ) : $applied_store_credit;
	}

	public function set_applied_store_credit( $applied_store_credit ) {

		$applied_store_credit = $this->update_meta_data( '_applied_store_credit', $applied_store_credit );
		
		return $this->get_applied_store_credit();
	}

	/**
	 * Get the applied Store Credits on the order
	 * 
	 * @return 	array
	 */
	public function get_credits() {
		return $this->get_items( 'credit' );
	}

	/**
	 * Add Store Credit to the order
	 * 
	 * @param 	string|int 	$credit
	 * 
	 * @return 	bool
	 */
	private function add_store_credit( $credit ) {

		// $credit = $this->sanitize_store_credit( $credit );
		
		// Make negative to remove the credit from the order
		$credit = -1 * $credit;

		$applied_credit = new OMH_Order_Item_Credit();
		$applied_credit->set_amount( $credit );
		$applied_credit->set_total( $credit );
		$applied_credit->set_name( 'Applied Store Credit' );

		$this->add_item( $applied_credit );
		$this->calculate_totals( true );
		$this->save();

		return true;
	}

	/**
	 * Returns the amount of store credit applied to this order
	 * 
	 * @return 	string
	 */
	public function OLD_get_applied_store_credit( $show_negative = true ) {

		$store_credit = 0;

		foreach( $this->get_credits() as $item ) {

			$store_credit += $item->get_amount();
		}

		if( !$show_negative ) {
			$store_credit = $store_credit * -1;
		}

		return $store_credit;
	}

	/**
	 * Returns whether the order has store credit applied or not
	 * 
	 * @return 	boolean
	 */
	public function has_applied_store_credit() {

		if( count( $this->get_credits() ) > 0 ) {
			return true;
		}

		return false;
	}

	public function validate_applied_store_credit() {

		if( $this->has_applied_store_credit() ) {

			$credit = $this->get_applied_store_credit( false );

			if( !$total_store_credit = get_omh_total_store_credit( false ) ) {

				wc_add_notice( 'You do not have enough store credit. Applied credit has been removed from the order.', 'error' );
				$this->remove_applied_store_credit();
				return false;
			}

			if( $total_store_credit < 0 ) {

				wc_add_notice( 'You do not have enough store credit. Applied credit has been removed from the order.', 'error' );
				$this->remove_applied_store_credit();
				return false;
			}

			if( $credit > $total_store_credit ) {

				wc_add_notice( 'You do not have enough store credit. Applied credit has been updated to use the total store credit.', 'error' );
				$this->apply_store_credit( $credit );
				return false;
			}

			if( $credit > $this->get_order_total( true, false ) ) {

				wc_add_notice( 'You cannot use more credit than the total cost of the order. Applied credit has been adjusted to only use amount equal to the total of the order.', 'error' );
				$this->apply_store_credit( $credit );
				return false;
			}
		}

		return true;
	}

	/********************************************
	 * 			  	PRODUCT FUNCTIONS 			*
	 ********************************************/

	/**
	 * @tag Bulk Order
	*/
	public function get_bulk_product_id() {	
		return $this->get_meta( 'bulk_product_id' );
	}

	/**
	 * @tag Bulk Order
	*/
	public function set_bulk_product_id( $bulk_product_id = 0 ) {

		$this->update_meta_data( 'bulk_product_id', $bulk_product_id );

		return $this->get_bulk_product_id();
	}

	/**
	 * @tag Bulk Order
	*/
	public function get_bulk_product( $refresh = false ) {

		// Save on order after first get

		return wc_get_product( $this->get_bulk_product_id() );
	}

	public function set_bulk_order_quantities( $bulk_order_quantities = array(), $replace_all = true ) {

		if( $replace_all ) {
			// Clear Order Quantities
			$this->remove_order_items( 'line_item' );
		}

		foreach( $bulk_order_quantities as $product_variation_id => $product_variation_qty ) {

			if( !$product_variation_qty ) {
				continue;
			}

			$product_variation = wc_get_product( str_replace( 'qty_', '', $product_variation_id ) );

			$order_item_id = $this->add_product(
				$product_variation,
				$product_variation_qty
			);
		}

		$this->calculate_totals();

		return $this->get_items( 'line_item' );
	}

	/**
	 * Calculate total quantity, active price tier, and variation prices
	 * Then call parent::calculate_totals as normal
	 */
	public function set_bulk_line_item_totals() {

		$order_total_qty = $this->get_item_count( 'line_item' );

		$bulk_product = $this->get_bulk_product();

		if( $bulk_product ) {

			foreach( $this->get_items( 'line_item' ) as $line_item ) {

				$line_item_variation = wc_get_product( $line_item->get_variation_id() );

				$line_item_price = $line_item_variation->get_tier_price( $order_total_qty );

				$line_item_subtotal = $line_item_price * $line_item->get_quantity();

				$line_item->set_subtotal( $line_item_subtotal );
				$line_item->set_total( $line_item_subtotal );
			}
		}
	}

	public function has_bulk_product() {

	   foreach( $this->get_items( 'line_item' ) as $item_id => $line_item ) {

		   $line_item_product = wc_get_product( $line_item->get_product_id() );

		   if( $line_item_product && $line_item_product->get_sales_type() === 'bulk' ) {
			   return true;
		   }
	   }

	   return false;
	}

	/********************************************
	 * 			  	HELPER FUNCTIONS 			*
	 ********************************************/

	 /**
	  * @tag Bulk Order
	  * 
	  * @param 	int		$product_id
	  * @param 	array 	$order_data		Optional: Form data from edit bulk order screen
	  */
	 public static function create_bulk_order( $product_id, $order_data = array() ) {

		if( !$product_id ) {
			return null;
		}

		$product = wc_get_product( $product_id );

		if( $product->get_sales_type() !== 'bulk' ) {
			return null;
		}

		$classname = get_called_class();

		$bulk_order = new $classname();

		$bulk_order->set_bulk_product_id( $product_id );
		$bulk_order->set_customer_id( get_current_user_id() );

		$bulk_order->save();

		$bulk_order_quantities = array();

		foreach( $order_data as $order_data_key => $order_data_value ) {

			if( strpos( $order_data_key, 'qty_' ) === 0 ) {
				$bulk_order_quantities[ $order_data_key ] = $order_data_value;
			}
		}

		if( !empty( $bulk_order_quantities ) ) {
			$bulk_order->set_bulk_order_quantities( $bulk_order_quantities );
		}

		$product->set_active_bulk_order_id( $bulk_order->get_id() );
		$product->set_product_status('pending');
		$product->save();

		return $bulk_order->get_id();
	 }
}
<?php
defined( 'ABSPATH' ) || exit;

class OMH_Frontend {

	public function __construct() {

		/**
		 * Redirect Hooks
		 **/

		// Redirect the user after WooCommerce Login
		add_filter( 'woocommerce_login_redirect', array( $this, 'user_login_redirect' ), 10, 2 );
		// Redirect the user after logout
		add_action( 'wp_logout', array( $this, 'user_logout_redirect' ), 10 );
		// Redirect the user if they attempt to go to any of WooCommerce's account pages
		add_action( 'template_redirect', array( $this, 'user_my_account_redirect' ), 10 );
		// Redirect the user if they attempt to go to the old signup page
		add_action( 'template_redirect',  array( $this, 'redirect_from_signup' ), 10 );
		// Replace WooCommerce Notices with our own
		add_filter( 'wc_get_template', array( $this, 'omh_get_template' ), 10, 5 );

		add_filter( '404_template_hierarchy', array( $this, 'custom_404_hierarchy' ) );

		/**
		 * User Hooks
		 */

		add_filter( 'woocommerce_min_password_strength', array( $this, 'min_strength_requirement' ) );
		add_filter( 'password_hint', array( $this, 'password_hint' ) );
		// Change Registration Behavior if user has been signed up as house_member
		add_filter( 'woocommerce_registration_error_email_exists', array( $this, 'registration_handler') , 10, 2);

		/**
		 * Dashboard Hooks
		 **/

		// Permissions
		add_action( 'template_redirect', array( $this, 'dashboard_permissions' ), 50 );
		add_filter( 'body_class', array( $this, 'dashboard_body_class' ), 10, 1 );

		add_action( 'get_header', array( $this, 'header' ), 10, 1 );
		add_action( 'get_footer', array( $this, 'footer' ), 10, 1 );
		
		add_action( 'wp_footer', array( $this, 'enable_modals' ), 10, 1 );

		/**
		 * Check for referral links
		 */
		add_action('get_header', array($this, 'check_referral_link'));
		add_filter('query_vars', array( $this, 'query_vars'), 10, 1);

		/**
		 * WooCommerce Store Notice
		 **/

		add_action( 'template_redirect', array( $this, 'store_notice' ) );
		add_filter( 'body_class', array( $this, 'wc_body_class' ), 99 );
		add_filter( 'woocommerce_demo_store', array( $this, 'store_notice_message' ), 10, 2 );

		/**
		 * WooCommerce Product Hooks
		 **/
		add_action( 'woocommerce_product_query', array( $this, 'shop_product_query' ), 100, 1 );
		add_filter( 'woocommerce_product_is_visible', array( $this, 'product_visibility' ), 100, 2 );
		add_filter( 'woocommerce_is_purchasable', array( $this, 'product_purchasability' ), 100, 2 );
		add_filter( 'woocommerce_variation_is_purchasable', array( $this, 'product_variation_purchasability' ), 100, 2 );
		add_action( 'template_redirect', array( $this, 'product_details_page_visibility' ) );

		/**
		 * WooCommerce Order Hooks
		 **/
		add_filter( 'woocommerce_order_needs_payment', array( $this, 'order_needs_payment' ), 10, 3 );

		/**
		 * Nav Menu Hooks
		 **/

		add_filter( 'wp_nav_menu_objects', array( $this, 'nav_menu_logout' ), 10, 2 );
		add_filter( 'wp_get_nav_menu_items', array( $this, 'nav_menu_items' ), 10, 3 );

		// Visibility
		add_filter( 'nav_menu_roles_item_visibility', array( $this, 'nav_menu_roles' ), 10, 2 );

		/**
		 * WooCommerce Product Grid Items
		 **/

		// Product Sorting
		add_filter( 'woocommerce_get_catalog_ordering_args', array( $this, 'product_sorting' ), 25, 1 );
		
		// Chapter Name on product		
		remove_action( 'woocommerce_before_shop_loop_item_title', 'nectar_product_thumbnail_with_cart', 10 );
		remove_action( 'woocommerce_before_shop_loop_item_title', 'nectar_product_thumbnail_with_cart_alt', 10 );
		remove_action( 'woocommerce_before_shop_loop_item_title', 'nectar_product_thumbnail_material', 10 );
		remove_action( 'woocommerce_before_shop_loop_item_title', 'nectar_product_thumbnail_minimal', 10 );
		add_action( 'woocommerce_before_shop_loop_item_title', array( $this, 'omh_before_product_grid_item_title' ), 10 );

		/**
		 * WooCommerce Related Products
		 **/
		add_filter( 'woocommerce_related_products', array( $this, 'related_products' ), 10, 3 );

		/**
		 * WooCommerce PDP
		 **/

		add_action( 'woocommerce_single_product_summary', array( $this, 'omh_pdp_campaign_status' ), 10, 6 );

		// Custom Product Tabs
		add_filter( 'woocommerce_show_variation_price', '__return_true' );
		add_filter( 'woocommerce_product_tabs', array( $this, 'omh_remove_product_tabs' ), 10, 1 );
		add_filter( 'woocommerce_product_tabs', array( $this, 'omh_custom_product_tabs' ), 15, 1 );

		add_action ('omh_woocommerce_after_product_tabs', array ( $this, 'omh_pdp_signup_cta'), 16,1);

		add_filter( 'woocommerce_single_product_image_thumbnail_html', array( $this, 'omh_pdp_thumb_html' ), 999, 4 );		

		// add_filter( 'woocommerce_price_trim_zeros', '__return_true' );
		

		// Garment Information
		add_action( 'omh_product_garment_information', array( $this, 'omh_display_product_attributes' ), 10, 1 );
		add_filter( 'omh_product_garment_information_heading', '__return_null' );

		/**
		 * Chapter, Organization, and College
		 **/
		add_action( 'wp_head', array( $this, 'remove_salient_actions' ) );
		add_action( 'woocommerce_before_main_content', array( $this, 'custom_taxonomy_header' ), 5 );
		add_action( 'woocommerce_before_main_content', array( $this, 'custom_marketplace_header' ), 5 );
		add_action( 'woocommerce_before_main_content', array( $this, 'current_active_campaigns' ), 20 );
		add_filter( 'wpseo_title', array( $this, 'custom_taxonomy_page_title' ), 10, 1 );
		add_filter( 'body_class', array( $this, 'custom_taxonomy_body_class' ), 10, 1 );
		add_action( 'woocommerce_product_query', array( $this, 'product_national_visibility' ), 10, 1 );
		add_action( 'pre_get_posts', array( $this, 'product_search_visibility' ) );

		/**
		 * WooCommerce Breadcrumbs
		 **/
		add_filter( 'woocommerce_get_breadcrumb', array( $this, 'custom_breadcrumbs' ), 10, 2 );

		/**
		 * WooCommerce Cart
		 */
		add_action('woocommerce_before_cart', array($this, 'add_cart_header'), 10, 0);
		add_filter('woocommerce_cart_item_product', array($this, 'format_cart_item_name'), 10, 3);
		add_filter('woocommerce_get_item_data', array($this, 'format_cart_item_data' ), 10, 2);

		/**
		 * WooCommerce Checkout
		 **/
		add_action( 'woocommerce_before_checkout_form', array( $this, 'maybe_remove_coupon_form' ), 0, 1 );
		add_filter( 'woocommerce_package_rates', array( $this, 'calculate_shipping_rates' ), 10, 2 );
		add_action( 'woocommerce_before_cart_table', array( $this, 'cart_notices' ), 10 );
		add_filter( 'woocommerce_checkout_fields', array( $this, 'checkout_fields' ), 10, 1 );
		add_action( 'woocommerce_checkout_init', array( $this, 'redirect_checkout' ) );
		add_action( 'woocommerce_proceed_to_checkout', array( $this, 'disallow_cart_checkout' ), 10 );

		// Don't allow saving of payment methods
		add_filter( 'wc_stripe_display_save_payment_method_checkbox', '__return_false' );

		/**
		 * WooCommerce Order Confirmation
		 **/

		add_filter( 'woocommerce_thankyou_order_received_text', array( $this, 'order_confirmation_thank_you_text' ), 10, 2 );
		add_action( 'woocommerce_order_details_after_customer_details', array( $this, 'order_info_thank_you_text' ), 10, 2 );
		add_action( 'woocommerce_order_details_after_order_table', array( $this, 'order_info_anonymous' ), 10, 2 );
		add_action( 'woocommerce_order_details_after_order_table', array( $this, 'order_confirmation_signup_modal'), 10, 2);
	}

	/**
	 * Redirect the user after WooCommerce Login
	 * 
	 * @param 	string 	$redirect 
	 * @param 	WP_User $user 
	 * 
	 * @return 	string
	 */
	public function user_login_redirect( $redirect, $user ) {

		if( isset( $_GET['redirect'] ) ) {
			$redirect = site_url( $_GET['redirect'] );
		} else {
			$redirect = site_url( '/dashboard/' );
		}

		return $redirect;
	}

	/**
	 * Redirect the user after logout
	 */
	public function user_logout_redirect() {

		delete_user_meta( get_current_user_id(), 'session_tokens' );
		
		wp_clear_auth_cookie();

		if( !isset( $_REQUEST['redirect_to'] ) ) {
			wp_redirect( home_url() );
		} else {
			wp_redirect( $_REQUEST['redirect_to'] );
		}
		exit;
	}

	/**
	 * Redirect the user if they attempt to go to any of WooCommerce's account pages
	 * 
	 * @return 	void
	 */
	public function user_my_account_redirect() {
		global $wp, $post;

		if( is_user_logged_in() && $omh_user = wp_get_current_user() ) {

			// If the logged in user is a house admin/member trying to go to a dashboard screen but they have not activated their account, send them to the activation page
			// dev:activate
			if( $omh_user->has_role( array( 
				'house_admin', 
				'house_member' 
			), false ) 
				&& OMH()->is_dashboard() 
				&& $post->post_name !== 'activation' 
				&& !$omh_user->is_activated() 
			) {
				wp_redirect( OMH()->dashboard_url( '/activation/?key=' . $omh_user->get_user_activation_key() ) );
				exit;
			} else
			if( is_account_page() ) {
				wp_redirect( OMH()->dashboard_url( '/dashboard/' ) );
				exit;
			}
		}

		// If the user isn't logged in and attempting to reset their password
		if( !is_user_logged_in()
			&& is_account_page()
			&& isset( $wp->query_vars['lost-password'] )
			&& !empty( $_GET['show-reset-form'] ) 
		) {

			if ( isset( $_COOKIE[ 'wp-resetpass-' . COOKIEHASH ] ) && 0 < strpos( $_COOKIE[ 'wp-resetpass-' . COOKIEHASH ], ':' ) ) {
				list( $user_login, $user_key ) = array_map( 'wc_clean', explode( ':', wp_unslash( $_COOKIE[ 'wp-resetpass-' . COOKIEHASH ] ), 2 ) );
				$user = new OMH_User( check_password_reset_key( $user_key, $user_login ) );

				// If the user isn't activated/setup force them to activate
				// dev:todo Setup user activation?
				// if ( is_object( $user )
				// 	 && $user instanceof OMH_User
				// 	 && !$user->is_activated()
				// 	) {

				// 	$user_activation_key = $user->get_activation_key();

				// 	wp_redirect( site_url( '/activate?key=' . $user_activation_key ) );
				// 	exit;
				// }
			}
		}
	}

	/**
	 * Redirect 
	 */
	public function redirect_from_signup() {
		$path = get_query_var('pagename');
		if($path == 'signup'){
			wp_redirect( home_url() . "#signup" );
		}
	}

	/**
	 * Replace WooCommerce Notices with our own
	 */
	public function omh_get_template( $located, $template_name, $args, $template_path, $default_path ) {

		if( 0 === strpos( $template_name, 'notices' ) ) {
			$located = OMH_TEMPLATE_PATH . 'mh-wc-notice.php';
		}

		return $located;
	}

	/**
	 * Custom 404 page templates
	 * 
	 * @param 	array 	$templates 
	 * 
	 * @return 	array
	 */
	public function custom_404_hierarchy( $templates ) {

		if( !is_array( $templates ) ) {
			return $templates;
		}

		return $templates;
	}

	/** 
	 * Reduce the strength requirement on the woocommerce password.
	 * 
	 * Strength Settings
	 * 4 = Strong (default)
	 * 3 = Medium
	 * 2 = Medium Weak
	 * 1 = Weak
	 * 0 = Very Weak / Anything
	 * 
	 * @param 	int 	$strength
	 * 
	 * @return 	int
	 */
	public function min_strength_requirement( $strength ) {
		return 0;
	}

	/**
	 * Use our own password hint and validation to ignore WPEngine's rules
	 * 
	 * @param   string  $hint
	 * 
	 * @return  string  $hint
	 */
	public static function password_hint( $hint = 'Your password must contain at least 6 characters, 1 letter, and 1 number.' ) {
		return $hint;
	}

	/**
	 * Function to handle if user trying to register has been previously created as house member
	 * 
	 * @param   string  $errors - the incoming WP Error string
	 * @param   string  $sanitized_user_login - Email used in registration form
	 * 
	 * @return  string  $errors
	 */
	public function registration_handler($errors, $sanitized_user_login) {
		$omh_user = OMH_USER::get_by_email($sanitized_user_login);
		if ($omh_user->is_house_member() && !$omh_user->is_activated()){
			$activationString = "It looks like this user is already registered as a house member but not activated. Please check your email for an activation.";
			OMH_Emails::send_house_member_verification_notification($omh_user->ID);
			return $activationString;
		} else {
			return $errors;
		}
	}

	private function redirect_404() {
		global $wp_query;

		$wp_query->set_404();
		status_header(404);
	}

	/**
	 * Redirect user to login page if they are not logged in and try to access the dashboard
	 */
	public function dashboard_permissions() {
		global $post;

		// Make sure these permissions only run on dashboard pages
		if( OMH()->is_dashboard() ) {

			if( !is_user_logged_in() && $post->post_name !== 'activation') {

				wp_redirect( site_url( 'my-account' ) . '?redirect=' . urlencode( $_SERVER['REQUEST_URI'] ) );
				exit;
			}

			$omh_user = wp_get_current_user();

			// If a non admin is attempting to access the MH Admin, redirect them
			if( OMH()->is_mh_admin() ) {

				if( !$omh_user->is_administrator() ) {

					wp_redirect( OMH()->dashboard_url() );
					exit;
				} else {
					return;
				}
			}

			// If we're trying to access a url without chapters in it, and we're not a customer, but we do have a primary chapter, let's find our chapter url
			if( !get_query_var( 'chapters' ) 
				&& ( $omh_user->has_house_role() || $omh_user->is_administrator() )
				// && !$omh_user->has_role( 'customer', false ) 
				&& $omh_user->get_primary_chapter_id() 
			) {

				$path = get_query_var( 'pagename' );

				wp_redirect( OMH()->dashboard_url( $path ) );
				exit;
			}

			// If we're a customer (or a house admin without a chapter) and we're trying to access the dashboard, redirect us to the account details screen
			if( ( $omh_user->has_role( 'customer', false ) || !$omh_user->get_primary_chapter_id() ) && ( get_query_var( 'pagename' ) === 'dashboard' ) ) {

				wp_redirect( OMH()->dashboard_url( 'account-details' ) );
				exit;

			// If we're logged in and going to "My Account" pages, don't do anything for now
			} elseif( in_array( get_query_var( 'pagename' ), array( 'account-details', 'password-change', 'my-addresses', 'my-orders', 'activation' ) ) ) {

			// If we're a house admin or an admin trying to access a chapter dashboard, let's go through some permissions
			} elseif( $chapter_url_structure = get_query_var( 'chapters', false ) ) {

				$chapter = OMH()->chapter_factory->get_by_url_structure( $chapter_url_structure );

				if( $chapter ) {

					// Admins change their chapter when they try to access chapter dashboard, easy
					if( $omh_user->has_role( 'administrator' ) ) {

						OMH()->session->set_chapter( $chapter->get_id() );

					// If we're a house admin, we need to make sure we have access to view this chapter, if we don't let's redirect to our chapter dashboard, if we don't have a chapter dashboard that's taken care of above
					} elseif( $omh_user->has_role( 'house_admin' ) || $omh_user->has_role( 'house_member' ) ) {

						if( OMH()->session->get_chapter()->get_id() !== $chapter->get_id() ) {

							$path = get_query_var( 'pagename' );

							wp_redirect( OMH()->dashboard_url( $path ) );
							exit;
						}

					// We should not be able to get here
					} else {

						// dev:improve dev:onboarding
						wp_redirect( OMH()->dashboard_url( 'account-details' ) );
						exit;
					}
				} else {
					$this->redirect_404();
				}
			} else {
				$this->redirect_404();
			}
		}
	}

	/**
	 * Add CSS Class to main Dashboard page body
	 * 
	 * @param 	array 	$body_classes
	 * 
	 * @return 	array
	 */
	public function dashboard_body_class( $body_classes ) {

		if( get_option( 'omh_dashboard_page_id' ) == get_queried_object_id() ) {

			$body_classes[] = 'main-dashboard-page';
		}

		return $body_classes;
	}

	/**
	 * Take control of permissions
	 **/

	/**
	 * Output Buffer the WP Header
	 * 
	 * @param 	string 	$name
	 */
	public function header( $name ) {

		
		if( OMH()->is_dashboard() ) {
			ob_start();
		}
	}

	/**
	 * Output Buffer the WP Footer
	 * 
	 * @param 	string 	$name
	 */
	public function footer( $name ) {

		include( OMH_TEMPLATE_PATH . 'loading-overlay.php' );

		if( OMH()->is_dashboard() ) {
			echo ob_get_clean();
		}

		$omh_user = wp_get_current_user();

		// if(!$omh_user->is_logged_in()) {

		// }
	}

	public function enable_modals() {

		ob_start();
		include (OMH_TEMPLATE_PATH . 'modals/house-signup-modal.php');
		include (OMH_TEMPLATE_PATH . 'modals/start-design-modal.php');

		if( is_chapter() || is_organization() ) {
			include (OMH_TEMPLATE_PATH . 'modals/new-follower-modal.php');
		}
		echo ob_get_clean();

	}

	public function check_referral_link() {
		if($referral_code = get_query_var("referral")) {
			$referral_code = sanitize_text_field($referral_code);
			// Set cookie to last for 30 days 
			setcookie('referral', $referral_code, time()+60*60*24*30, "/");
		}
	}

	/**
	 * Add referral_code to list of acceptable query vars
	 */
	public function query_vars($qvars) {
		$qvars[] = "referral";
		return $qvars;
	}

	/**
	 * Only display the WC Store Notice on certain pages
	 */
	public function store_notice() {

		remove_action( 'wp_footer', 'woocommerce_demo_store' );

		if( !OMH()->is_dashboard() ) {

			$omh_user = wp_get_current_user();

			if( $omh_user->has_role( 'administrator' ) 
				|| ( !$omh_user->has_role( 'house_admin' ) || !$omh_user->is_logged_in() )
			) {
				add_action( 'wp_footer', 'woocommerce_demo_store' );
			}
		}

	}

	/**
	 * Remove WC Store Notice from Dashboard and other checks
	 * 
	 * @param 	array 	$classes
	 * 
	 * @return 	array
	 */
	public function wc_body_class( $classes ) {

		if( is_store_notice_showing() ) {

			$store_notice_class_key = array_search( 'woocommerce-demo-store', $classes );

			$omh_user = wp_get_current_user();

			if( OMH()->is_dashboard() ) {
				unset( $classes[ array_search( 'woocommerce-demo-store', $classes ) ] );
			} elseif( $omh_user->is_logged_in() && $omh_user->has_role( 'house_admin', false ) ) {
				unset( $classes[ array_search( 'woocommerce-demo-store', $classes ) ] );
			}
		}
		
		return $classes;
	}

	/**
	 * Change the format of the WC Store Notice
	 * 
	 * @param 	string 	$message 
	 * @param 	string 	$notice
	 * 
	 * @return 	string
	 */
	public function store_notice_message( $message, $notice ) {

		$dismiss = ( 'no' !== get_option( 'woocommerce_demo_store_notice_dismissible', 'no' ) ) ? '<a href="#" class="woocommerce-store-notice__dismiss-link">
			<button class="close" aria-label="Close" data-dismiss="alert">
				<span aria-hidden="true" data-dismiss="alert">×</span>
			</button>
		</a>' : '';

		$message = sprintf( '<p class="woocommerce-store-notice demo_store">%s %s
			</p>', $notice, $dismiss );

		return $message;
	}

	/**
	 * @tag Product Status
	 * @tag Visibility
	 */
	public function product_visibility( $visible, $product_id ) {

		$product = wc_get_product( $product_id );

		if( 'variable' === $product->get_type() ) {
			if( in_array( $product->get_sales_type(), array('retail','campaign') ) ) {
				return $product->is_product_visible( 'chapter' );
			}
		}

		return false;
	}

	/**
	 * Controls whether the PDP is visible or not
	 * 
	 * dev:improve This will currently 404 for everyone, including super admin
	 *  
	 * @tag Product Status
	 * @tag Visibility
	 * 
	 * @return 	void
	 */
	public function product_details_page_visibility() {

		if( is_product() ) {
			global $post;

			$product = wc_get_product( $post->ID );

			if( !$product->is_product_visible( 'link' ) ) {
				$this->redirect_404();
			}
		}	
	}

	/**
	 * Hide hidden products from WordPress search as this is not pre-built into WooCommerce
	 * 
	 * @tag Product Status
	 * @tag Visibility
	 * @tag Search
	 * 
	 * @param 	WP_Query|bool 	$query 
	 * 
	 * @return 	void
	 */
	public function product_search_visibility( $query = false ) {

		if( !is_admin() && is_search() && $query->is_search ) {

			/**
			 * Get all product statuses that allow for search visibility 
			 * and add that to the search tax query
			 */
			$search_visibility = OMH_Product_Variable::get_visible_product_statuses( 'search' );

			$query->set(
				'tax_query',
				array(
					array(
						'taxonomy'	=> 'product_status',
						'field'		=> 'slug',
						'terms'		=> $search_visibility,
						'operator'	=> 'IN'
					)
				)
			);
		}
	}

	/**
	 * Filter WC Query for whether products should be visible on national pages or not
	 * 
	 * @tag Product Status
	 * @tag Visibility
	 */
	public function product_national_visibility( $query ) {
		$visible_statuses = OMH_Product_Variable::get_visible_product_statuses( 'national' );
		
		$tax_query = array(
			'relation'	=> 'AND',
			array(
				'taxonomy'	=> 'product_status',
				'field'		=> 'slug',
				'terms'		=> $visible_statuses,
				'operator'	=> 'IN'
			)
		);
		$query->set( 'tax_query', $tax_query );
	}

	public function product_purchasability( $purchasable, $product ) {

		if( 'variable' === $product->get_type() ) {

			if( $purchasable ) {
				$purchasable = $product->is_product_purchasable();
			}
		}

		return $purchasable;
	}

	public function product_variation_purchasability( $purchasable, $product ) {

		if( 'variation' === $product->get_type() ) {

			$parent = wc_get_product( $product->get_parent_id() );

			$purchasable = $parent->is_product_purchasable();
		}

		return $purchasable;
	}

	/**
	 * Allow for zero cost orders
	 * 
	 * @param type $needs_payment 
	 * @param type $order 
	 * @param type $valid_order_statuses 
	 * @return type
	 */
	public function order_needs_payment( $needs_payment, $order, $valid_order_statuses ) {

		return $order->has_status( $valid_order_statuses );
	}

	/**
	 * Show or Hide Nav Items based on Role
	 * 
	 * This is a "maximum" or "strict" addon to the Nav Menu Roles Plugin
	 * This will check that the user ONLY has the role in the plugin
	 * 
	 * @param boolean $visible 
	 * @param WP_Post $item 
	 * 
	 * @return boolean $visible
	 */
	public function nav_menu_roles( $visible, $item ) {

		$nav_menu_roles = get_post_meta( $item->ID, '_nav_menu_role', true );

		if( $nav_menu_roles && is_array( $nav_menu_roles ) ) {

			$omh_user = wp_get_current_user();
			$roles = $omh_user->roles;

			if( array_intersect( $nav_menu_roles, $roles ) ) {

				$visible = true;
			} else {

				$visible = false;
			}
		}

		return $visible;
	}

	/**
	 * Fix the URL for WP Logout in the menu
	 * 
	 * @param 	array 	$items
	 * @param 	array 	$args
	 * 
	 * @return 	array 	$items
	 */
	public function nav_menu_logout( $items, $args ) {

		if( 'top_nav' == $args->theme_location ) {

			foreach( $items as $item_id => $item ) {
				
				if( 'logout' == $item->post_name ) {

					if( !is_user_logged_in() ) {

						unset( $items[ $item_id ] );
					} else {

						$item->url = wp_logout_url();
					}
				}
			}
		}

		return $items;
	}

	public function nav_menu_items( $nav_menu_items, $menu, $args ) {

		foreach( $nav_menu_items as &$nav_menu_item ) {

			if( isset( $nav_menu_item->url ) ) {
				// Dynamic Dashboard URL
				$nav_menu_item->url = str_replace( '#?dashboard', untrailingslashit( OMH()->dashboard_url() ), $nav_menu_item->url );

				if( $omh_chapter = OMH_Session::get_chapter() ) {

					$term_link = $omh_chapter->get_term_link();

					if( $term_link && !is_wp_error( $term_link ) ) {
						// Dynamic Chapter URL
						$nav_menu_item->url = str_replace( '#?chapter', $omh_chapter->get_term_link(), $nav_menu_item->url );
					}
				}

				if( $nav_menu_item->title === 'My Account' && ( $nav_menu_item->menu_item_parent == 0 ) ) {

					$nav_menu_item->title = '<span class="icon-in-menu icon-default-style icon-salient-m-user"></span>';
					$nav_menu_item->classes = array(
						'my-account-menu-item'
					);
				}
			}
		}

		return $nav_menu_items;
	}

	public function product_sorting( $args ) {

		/**
		 * dev:todo This needs to be uncommented, but it broke when we changes visibility
		 */
		// unset( $args['meta_key'] );
		$args['orderby'] = 'ID';
		$args['order'] = 'DESC';

		return $args;
	}

	public function omh_before_product_grid_item_title() {
		global $product;
		global $woocommerce;
		global $product_hover_alt_image;
		global $nectar_quick_view_in_use;

		$chapter = $product instanceof WC_Product_Variable ? $product->get_chapter() : false;
		?>
		<div class="background-color-expand"></div>
	   	<div class="product-wrap">
	   		<?php
				$product_second_image = null;
				if ( $product_hover_alt_image == '1' ) {

					if ( $woocommerce && version_compare( $woocommerce->version, '3.0', '>=' ) ) {
						$product_attach_ids = $product->get_gallery_image_ids();
					} else {
						$product_attach_ids = $product->get_gallery_attachment_ids();
					}

					if ( isset( $product_attach_ids[0] ) ) {
						$product_second_image = wp_get_attachment_image( $product_attach_ids[0], 'shop_catalog', false, array( 'class' => 'hover-gallery-image' ) );
					}
				}
			?>
			<a href="<?php echo esc_url( get_permalink() ); ?>">
				<?php echo woocommerce_get_product_thumbnail() . $product_second_image; ?>
			</a>
			<div class="product-meta pgi-meta">
				<?php if( $chapter ): ?>
					<a href="<?php echo $chapter->get_organization()->get_term_link();?>">
					<span class="product-chapter greek-letters">
						<?php echo $chapter->get_organization()->get_org_greek_letters(); ?>
					</span>
					</a>
					<a href="<?php echo $chapter->get_term_link(); ?>" class="pgi-chapter-link">
						<span>
							<?php echo $chapter->get_chapter_name(); ?>
						</span>
					</a>
				<?php endif; ?>
				<?php if( $product->get_sales_type() === 'campaign' ): ?>
					<div class="pull-right">
						<span class="badge-pill badge-primary badge">
							ENDS ON <?php echo date( 'm/d', strtotime( $product->get_campaign_end_date() ) ); ?>
						</span>
					</div>
				<?php endif; ?>
				<a href="<?php echo esc_url( get_permalink() ); ?>" class="pgi-product-title">
					<?php do_action( 'woocommerce_shop_loop_item_title' ); ?>
				</a>
				<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
				<div class="price-hover-wrap">
					<?php do_action( 'nectar_woo_minimal_price' ); ?>
					<div class="product-add-to-cart" data-nectar-quickview="<?php echo $nectar_quick_view_in_use; ?>">
						<a href="<?php echo esc_url( get_permalink() ); ?>">
							<span>View Product</span>
						</a>
					</div>
				</div>
			</div>
	   	</div>
	   	<?php
	}

	/**
	 * dev:todo What should we be displaying here?
	 * 
	 * @tag Product Status
	 * @tag Visibility
	 */
	public function related_products( $related_products, $product_id, $args ) {

		$related_products 	= array();
		$product 			= wc_get_product( $product_id );
		$limit 				= $args['limit'] ?? 4;

		if( $product_chapter = $product->get_chapter() ) {

			$related_chapter_products = omh_filter_wc_product_array( $product_chapter->get_public_products( null, true ), true, $product_id );
			shuffle( $related_chapter_products );

			$related_products += $related_chapter_products;

			if( ( count( $related_products ) < 4 ) 
				&& $product_organization = $product_chapter->get_organization() 
			) {

				$related_organization_products = omh_filter_wc_product_array( $product_organization->get_public_products(), true, $product_id );
				shuffle( $related_organization_products );

				$related_products += $related_organization_products;
			}

			if( count( $related_products ) < 4 ) {

				$product_query = new WP_Query(
					array(
						'post_type'			=> 'product',
						'posts_per_page'	=> 50,
						'order'				=> 'DESC',
						'orderby'			=> 'rand',
						'meta_query'		=> array(
							array(
								'key'		=> '_visibility_mh',
								'value'		=> array( 'national' ),
								'compare'	=> 'IN'
							)
						)
					)
				);

				$randomized_products = omh_filter_wc_product_array( array_map( 'wc_get_product', $product_query->get_posts() ), true, $product_id );

				$related_products += $randomized_products;
			}
		}

		return array_slice( array_unique( $related_products ), 0, $limit );
	}

	public function omh_pdp_campaign_status() {
		global $product;

		if( $product->get_sales_type() === 'campaign' ) {
		?>
			<div class="pdp__meta-item pdp__meta-item--campaign-info clearfix">
				<div class="pdp-campaign-status pull-left">
					<?php echo $product->get_product_status_badge( false ); ?>
				</div>
				<div class="pdp-campaign-date-ending pull-right">
					ENDS ON <?php echo date( 'm/d', strtotime( $product->get_campaign_end_date() ) ); ?>
				</div>
			</div>
		<?php
		}
	}

	/*
	 * Custom Product Tabs
	 */

	/**
	 * Remove Tags from the Product page
	 * 
	 * @param 	array 	$tabs 
	 * 
	 * @return 	array
	 */
	public function omh_remove_product_tabs( $tabs ) {

		$remove_tabs = array(
			'description',
			'reviews',
			'additional_information'
		);

		foreach( $remove_tabs as $remove_tab ) {
			unset( $tabs[ $remove_tab ] );
		}

		return $tabs;
	}

	/**
	 * Change Tags on the Product page
	 * 
	 * @param 	array 	$tabs 
	 * 
	 * @return 	array
	 */
	public function omh_custom_product_tabs( $tabs ) {

		global $product, $post;

		if( $product ) {

			$tabs['garment_details'] = array(
				'title'		=> 'Details',
				'priority'	=> 20,
				'callback'	=> array( $this, 'omh_product_tab_garment_information' )
			);

			// $tabs['bulk_pricing'] = array(
			// 	'title'		=> 'Bulk Pricing',
			// 	'priority'	=> 30,
			// 	'callback'	=> array( $this, 'omh_product_tab_bulk_pricing' )
			// );
		}

		return $tabs;
	}

	public function omh_pdp_signup_cta() {
		if ((is_user_logged_in()) && 
		($omh_user = wp_get_current_user()) && 
		($omh_user->has_role( array( 'house_admin', 'house_member' )) 
		)){
			return;
		}
		?>
		<div class="pdp-signup-cta">
			<h4 class="pdp-signup-cta__heading">
				Sign up today!
			</h4>
			<p class="pdp-signup-cta__body">
				Get your chapter an online storefront and manage all of your apparel needs in one centralized place.
			</p>
			<button class="btn-primary btn-sm btn omh-trigger-signup-modal pdp-signup-cta__button ">Join Now - It's Free!</button>
		</div>
		<?php
	}

	public function omh_product_tab_garment_information() {
		
		include( OMH_TEMPLATE_PATH . 'woocommerce/single-product/tabs/tab-garment-details.php' );
	}


	// public function omh_product_tab_bulk_pricing() {

	// 	include( OMH_TEMPLATE_PATH . 'woocommerce/single-product/tabs/tab-bulk-pricing.php' );
	// }

	public function omh_pdp_thumb_html( $thumb_html, $attachment_id, $post_id, $image_class ) {

		$thumb_html = str_replace( ' class=', ' data-attachment-id="'. $attachment_id . '" class=', $thumb_html );
		// sprintf( '<div class="thumb"><div class="thumb-inner">%s</div></div>', $img_size )

		return $thumb_html;
	}




	/**
	 * Display custom data in Tags on the Product page
	 * 
	 * @param 	WC_Product 	$product
	 * 
	 * @return 	string
	 */
	public function omh_display_product_attributes( $product ) {

		$product_id = new WC_Product_Attribute();
		$product_id->set_name('Product ID');
		$product_id->set_options( array( $product->get_sku() ) );
		$product_id->set_position( 1 );
		$product_id->set_visible( true );
		$product_id->set_variation( false );

		$manufacturer = new WC_Product_Attribute();
		$manufacturer->set_name( 'Brand' );
		$manufacturer->set_options( array( $product->get_meta( '_mh_manufacturer' ) ) );
		$manufacturer->set_position( 2 );
		$manufacturer->set_visible( true );
		$manufacturer->set_variation( false );

		$style_number = new WC_Product_Attribute();
		$style_number->set_name( 'Style Number' );
		$style_number->set_options( array( $product->get_meta( '_mh_blank_style_number' ) ) );
		$style_number->set_position( 3 );
		$style_number->set_visible( true );
		$style_number->set_variation( false );

		$style_name = new WC_Product_Attribute();
		$style_name->set_name( 'Style Name' );
		$style_name->set_options( array( $product->get_meta( '_mh_blank_style_name' ) ) );
		$style_name->set_position( 4 );
		$style_name->set_visible( true );
		$style_name->set_variation( false );

		$color = new WC_Product_Attribute();
		$color->set_name( 'Garment Color' );
		$color->set_options( array( $product->get_meta( '_mh_blank_color' ) ) );
		$color->set_position( 5 );
		$color->set_visible( true );
		$color->set_variation( false );

		$material = new WC_Product_Attribute();
		$material->set_name( 'Material' );
		$material->set_options( array( $product->get_meta( '_mh_blank_material' ) ) );
		$material->set_position( 6 );
		$material->set_visible( true );
		$material->set_variation( false );
		        
		$new_attributes = array(
			'mh_product_id'			=> $product_id,
			'mh_manufacturer'		=> $manufacturer,
			'mh_blank_style_number'	=> $style_number,
			'mh_blank_style_name'	=> $style_name,
			'mh_blank_color'		=> $color,
			'mh_blank_material'		=> $material
		);

		wc_get_template( 'single-product/product-attributes.php', array(
			'product'				=> $product,
			'attributes'			=> $new_attributes,
			'display_dimensions'	=> false,
		) );
	}

	public function remove_salient_actions() {
		// Remove Salient Shop Titles from WooCommerce shop pages
		remove_action( 'nectar_shop_header_markup', 'salient_woo_shop_title', 10 );
	}

	public function custom_taxonomy_header() {

		$term = get_queried_object();

		if( $term instanceof WP_Term ) {

			$allowed_taxonomies = array( 'chapters', 'organizations', 'colleges' );

			if( ( 'WP_Term' == get_class( $term ) ) 
				&& in_array( $term->taxonomy, $allowed_taxonomies )
			) {

				$header_logo = '';
				$header_image = OMH()->plugin_url() . '/assets/images/mh-default-header.jpg';
				$header_title = 'Products';
				$header_subtitle = '';
				$header_location = false;

				ob_start();
				switch( $term->taxonomy ) {
					case 'chapters':
						include OMH_TEMPLATE_PATH . 'header-chapter.php';
						break;
					case 'organizations':
						include OMH_TEMPLATE_PATH . 'header-organization.php';
						break;
					case 'colleges':
						include OMH_TEMPLATE_PATH . 'header-college.php';
						break;
				}
				echo ob_get_clean();
			}
		}
	}
	
	public function custom_marketplace_header() {
		if(is_shop() || is_product_category()){
			ob_start();
			include OMH_TEMPLATE_PATH . 'header-marketplace.php';
			echo ob_get_clean();
		}
	}

	/**
	 * Only allow retail products from showing up on shop pages
	 */
	function shop_product_query( $query ){

		$query->query_vars['tax_query'][] = array(
			'taxonomy'	=> 'sales_type',
			'field'		=> 'slug',
			'terms'		=> array(
				'retail'
			)
		);

	}


	/**
	 * Show current active campaigns on chapter shop pages
	 */
	public function current_active_campaigns() {

		$term = get_queried_object();
		
		if( $term instanceof WP_Term ) {

			if( ( 'WP_Term' === get_class( $term ) ) 
				&& ( $term->taxonomy === 'chapters' ) 
			) {
				
				$campaign_query = new WP_Query( array(
					'post_type'	=> 'product',
					'posts_per_page'	=> -1,
					'tax_query'	=> array(
						'relation'	=> 'AND',
						array(
							'taxonomy'	=> 'sales_type',
							'field'		=> 'slug',
							'terms'		=> 'campaign'
						),
						array(
							'taxonomy'	=> 'chapters',
							'field'		=> 'slug',
							'terms'		=> $term->slug
						)
					)
				) );
				
				// dev:note Removed the if( woocommerce_product_loop() ) wrap here because it was hiding campaign products if there were no retail
				$visible_products_campaign = array_filter( $campaign_query->get_posts(), function($product) {
					if( $product = wc_get_product( $product ) ) {
						return $product->is_visible() && $product->is_purchasable();
					}
					return false;
				});

				if ( !empty( $visible_products_campaign ) ) {

					echo '<h4 class="shop-section-title">Live Campaigns</h4>';
					
					woocommerce_product_loop_start();
						
					while( $campaign_query->have_posts() ) {
						$campaign_query->the_post();
						wc_get_template_part( 'content', 'product' );
					}

					wp_reset_postdata();
										
					woocommerce_product_loop_end();
					$retail_query = new WP_Query( array(
						'post_type'	=> 'product',
						'posts_per_page'	=> -1,
						'tax_query'	=> array(
							'relation'	=> 'AND',
							array(
								'taxonomy'	=> 'sales_type',
								'field'		=> 'slug',
								'terms'		=> 'retail'
							),
							array(
								'taxonomy'	=> 'chapters',
								'field'		=> 'slug',
								'terms'		=> $term->slug
							)
						)
					) );

					$visible_products_retail = array_filter( $retail_query->get_posts(), function($product) {
						if( $product = wc_get_product( $product ) ) {
							return $product->is_visible() && $product->is_purchasable();
						}
						return false;
					});

					if ($visible_products_retail){
						echo '<h4 class="shop-section-title shop-title--retail-products">Retail Products</h4>';	
					}
				}
			}
		}
	}

	public function custom_taxonomy_page_title( $page_title ) {
		global $wp_query;

		if( is_archive() ) {

			if( get_query_var( 'organizations' ) || get_query_var( 'colleges' ) || get_query_var( 'chapters' ) ) {
				$page_title = str_replace( 'Archives', 'Shop', $page_title );
			}
		}

		return $page_title;
	}

	public function custom_taxonomy_body_class( $body_classes ) {
		global $wp_query;

		$object = get_queried_object();

		if( $object instanceof WP_Term ) {
			$body_classes[] = "archive-{$object->taxonomy}";
		}

		return $body_classes;
	}


	/**
	 * Cart Actions
	 */

	 public function add_cart_header() {
		 echo '<h2 class="omh_cart_header">My Cart</h2>';
	 }


	public function format_cart_item_name($data, $cart_item, $cart_item_key) {

		$data->set_name($data->get_title());


		return $data;
	}

	public function format_cart_item_data($item_data, $cart_item){

		// Remove Garment Details
		unset($item_data[0]);

		// Add Garment Color
		$garment = $cart_item['data']->get_product_garment();
		$garment_color = $garment->get_garment_style()->get_garment_color()->get_color_name();

		omh_debug($item_data);

		$color = array('key' => "Color", "value" => $garment_color);

		array_push($item_data, $color);

		return $item_data;
	}

	/**
	 * Checkout Actions
	 **/

	/**
	 * Change Checkout Fields
	 * 
	 * @param 	array 	$fields
	 * 
	 * @return 	array
	 */
	public function checkout_fields( $fields ) {

		// Remove 'special notes for delivery' as Merch House does not control that
		$fields['order']['order_comments']['placeholder'] = 'Notes about your order';

		// Change 'Company' to 'Organization'
		$fields['billing']['billing_company']['label'] = 'Organization name';

		return $fields;
	}

	public function maybe_remove_coupon_form( $checkout ) {
		
		if(!OMH_Cart_Bulk::cart_contains()) {
			remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 ); 
		}
	}

	public function calculate_shipping_rates( $rates, $package ) {

		foreach( $rates as $rate ) {

			if( 'free_shipping' === $rate->get_method_id() ) {
				$rate->set_cost( 0 );
			} elseif( 'flat_rate' === $rate->get_method_id() ) {

				$quantity = array_sum( array_column( $package['contents'], 'quantity' ) );

				if (OMH_Cart_Bulk::cart_contains()) {
					$rate->set_cost(0);
					$rate->set_label('Free Shipping');
				} elseif (1 == $quantity) {
					$rate->set_cost(5);
				} elseif (2 == $quantity) {
					$rate->set_cost(8);
				} elseif (3 == $quantity) {
					$rate->set_cost(10);
				} elseif (4 == $quantity) {
					$rate->set_cost(12);
				} elseif ($quantity >= 5) {
					$rate->set_cost(15);
				}
			}
		}

		return $rates;
	}

	public function cart_notices() {

		$omh_notices = array();

		if( $invalid_products = omh_validate_cart() ) {

			$notice_msg = 'Some products in your cart require a minimum quantity to be added to your cart across all garments and colors before proceeding to checkout.<ul class="ml-5">';

			foreach( $invalid_products as $invalid_product ) {

				if( isset( $invalid_product['name'], $invalid_product['minimum'] ) ) {

					$notice_msg .= "<li>{$invalid_product['name']} requires a minimum quantity of {$invalid_product['minimum']}</li>";
				}
			}

			$notice_msg .= '</ul>';

			$omh_notices[] = array(
				'text'			=> $notice_msg,
				'color'			=> 'warning',
				'dismissible'	=> false,
			);
		}
		?>
			<div id="mh-dashboard-notices">
				<?php
					foreach( $omh_notices as $omh_notice ) {

						echo OMH_HTML_UI_Alert::factory( $omh_notice );
					}
				?>
			</div>
		<?php
	}

	/**
	 * Redirect from Checkout if Cart is invalid
	 * 
	 * @param 	WC_Checkout 	$checkout
	 */
	public function redirect_checkout( $checkout ) {

		if( !empty( omh_validate_cart() ) ) {

			wp_redirect( wc_get_cart_url() );
			exit;
		}
	}

	/**
	 * Description
	 * @return type
	 */
	public function disallow_cart_checkout() {

		if( !empty( omh_validate_cart() ) ) {

			remove_action( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );

			echo '<button class="checkout-button button alt" disabled>Proceed to checkout</button>';
		}
	}

	public function custom_breadcrumbs( $crumbs, $breadcrumbs ) {
		global $wp_query;

		$omh_crumbs = new OMH_Breadcrumbs;

		$omh_crumbs->generate();

		return $omh_crumbs->get_breadcrumbs();
	}

	public function order_confirmation_thank_you_text( $thank_you_text, $order ) {

		return "<div class='omh_order_thank_you'>Thank you for your order. Your order # is {$order->get_order_number()} and a receipt has been emailed to you.</div>";
	}

	public function order_info_thank_you_text () {

		echo "<div class='omh_order_confirmation_info'>
		<h4 class='omh_order_confirmation_heading'>Retail Items</h4>
		<p>Please allow 7-10 business days for production as these are made to order items. You will receive an email once your order has shipped.</p>
		<h4 class='omh_order_confirmation_heading'>Campaign Items</h4>
		<p>If you ordered a campaign item, it will ship after the campaign has ended. Please see the campaign link's description for more info.</p>
		</div>";
	}

	public function order_info_anonymous () {
		if ((is_user_logged_in()) && 
		($omh_user = wp_get_current_user()) && 
		($omh_user->has_role( array( 'administrator', 'house_admin', 'house_member' )) 
		)){
			return;
		}
		else {
			$this->order_info_thank_you_text();
		}
	}

	public function order_confirmation_signup_modal(){
		if ((is_user_logged_in()) && 
			($omh_user = wp_get_current_user()) && 
			($omh_user->has_role( array( 'administrator', 'house_admin', 'house_member' )) 
			)){
				return;
			}
		else {
			echo "<div class='omh_order_signup'><h4 class='omh-order-signup__heading'>Does your chapter have an online storefront on Merch House?</h4> <button class='btn btn-primary btn-sm omh-trigger-signup-modal omh-order-signup__button'>Sign up - It's Free!</button></div>";
		}
	}
}
return new OMH_Frontend();
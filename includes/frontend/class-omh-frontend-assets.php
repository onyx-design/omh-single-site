<?php
defined( 'ABSPATH' ) || exit;

class OMH_Frontend_Assets {

	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'frontend_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'frontend_scripts' ) );
	}

	public function frontend_styles() {

		wp_enqueue_style( 'omh-dashboard', OMH()->plugin_url() . '/dist/css/dashboard.css', array('main-styles', 'dynamic-css', 'woocommerce'), OMH()->version );
	}

	public function frontend_scripts() {
		global $post;

		$omh_js_deps = array( 'jquery', 'nectar-frontend' );

		if( is_product() ) {
			$omh_js_deps[] = 'flickity';
			// $omh_js_deps = array_merge( $omh_js_deps, array( 'flickity', 'wc-add-to-cart-variation', 'nectar-single-product' ) );
		}

		wp_enqueue_script( 'omh', OMH()->plugin_url() . '/dist/js/omh.js', $omh_js_deps, OMH()->version, true );
		wp_localize_script( 'omh', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

		// Check slugs for conditional enqueueing
		if( isset( $post->post_name ) && 'create-product' == $post->post_name ) {
			wp_enqueue_script( 'inksoft_designer', 'https://stores.inksoft.com/designer/html5/common/js/launcher.js', array(), '1.0.0', true );	
		}


		wp_deregister_script('underscore');
		wp_register_script('underscore', OMH()->plugin_url() . '/assets/js/vendor/lodash.min.js', array(), null);

		wp_dequeue_script( 'wc-password-strength-meter' );
	}
}
return new OMH_Frontend_Assets();
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( !class_exists( 'OMH_Email_User_Activation' ) ) {

	/**
	 * User Activation Email
	 * 
	 * An email sent to the user when their account has been created
	 */
	class OMH_Email_User_Activation extends WC_Email {

		/**
		 * Constructor
		 */
		public function __construct() {
			
			$this->id             = 'user_activation_notification';
			$this->title          = 'User Activation Notification';
			$this->description    = 'User Activation emails are sent to the user when their account is created and requires activation';
			$this->template_html  = 'emails/user-activation-notification-html.php';
			$this->template_plain = 'emails/user-activation-notification-plain.php';
			$this->placeholders   = array(
				'{chapter_name}'		=> '',
				'{activation_link}'		=> '',
				'{house_role}'			=> '',
			);

			// Triggers for this email
			add_action( 'omh_email_' . $this->id, array( $this, 'trigger' ), 10, 1 );

			// Call parent constructor
			parent::__construct();
		}

		/**
		 * Get email subject
		 * 
		 */
		public function get_default_subject() {
			
			return __( 'Activate your {house_role} account for {chapter_name}', OMH_TEXT_DOMAIN );

		}

		/**
		 * Get email heading
		 * 
		 */
		public function get_default_heading() {

			return __( '{house_role} Account Activation', OMH_TEXT_DOMAIN );

		}

		public function get_greeting() {

			return $this->get_option( 'greeting', $this->get_default_greeting() );
		}

		public function get_closing() {

			return $this->get_option( 'closing', $this->get_default_closing() );
		}

		/**
		 * Get email greeting
		 * 
		 */
		public function get_default_greeting() {

			return __( 'Hello', OMH_TEXT_DOMAIN );
		}

		/**
		 * Get email closing
		 * 
		 */
		public function get_default_closing() {

			return __( 'Merch House', OMH_TEXT_DOMAIN );
		}

		/**
		 * Trigger the sending of this email
		 * 
		 */
		public function trigger( $user_id ) {

			$this->setup_locale();

			$omh_user = get_user_by( 'id', $user_id );
			$chapter = $omh_user->get_chapter();

			$this->object 							= $omh_user;
			$this->placeholders['{chapter_name}']	= $chapter ? $chapter->get_chapter_name() : 'Merch House';
			$this->chapter_name						= $chapter ? $chapter->get_chapter_name() : 'Merch House';
			$this->placeholders['{activation_link}']= OMH()->dashboard_url( 'activation?key=' . $omh_user->get_user_activation_key(), $chapter->get_id() );
			$this->activation_link					= OMH()->dashboard_url( 'activation?key=' . $omh_user->get_user_activation_key(), $chapter->get_id() );
			$this->placeholders['{house_role}']		= $omh_user->get_house_role( true );

			if( $this->is_enabled() ) {
				$this->send( $omh_user->get_email(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
			}

			$this->restore_locale();

		}

		/**
		 * Get content HTML
		 * 
		 */
		public function get_content_html() {

			return wc_get_template_html( 
				$this->template_html, 
				array(
					'product'			=> $this->object,
					'email_heading' 	=> $this->get_heading(),
					'greeting'			=> $this->get_greeting(),
					'chapter_name'		=> $this->placeholders['{chapter_name}'],
					'activation_link'	=> $this->placeholders['{activation_link}'],
					'house_role'		=> $this->placeholders['{house_role}'],
					'closing'			=> $this->get_closing(),
					'sent_to_admin' 	=> true,
					'plain_text'    	=> false,
					'email'				=> $this,
				),
				'',
				OMH_PATH . '/templates/'
			);

		}

		/**
		 * Get content plain
		 * 
		 */
		public function get_content_plain() {

			return wc_get_template_html( 
				$this->template_plain, 
				array(
					'product'			=> $this->object,
					'email_heading' 	=> $this->get_heading(),
					'greeting'			=> $this->get_greeting(),
					'chapter_name'		=> $this->placeholders['{chapter_name}'],
					'activation_link'	=> $this->placeholders['{activation_link}'],
					'house_role'		=> $this->placeholders['{house_role}'],
					'closing'			=> $this->get_closing(),
					'sent_to_admin' 	=> true,
					'plain_text'    	=> true,
					'email'				=> $this,
				),
				'',
				OMH_PATH . '/templates/'
			);

		}

		/**
		 * Initialize settings form fields
		 */
		public function init_form_fields() {
			$this->form_fields = array(

				'enabled' => array(
					'title'         => __( 'Enable/Disable', 'woocommerce' ),
					'type'          => 'checkbox',
					'label'         => __( 'Enable this email notification', 'woocommerce' ),
					'default'       => 'yes',
				),

				'subject' => array(
					'title'         => __( 'Subject', 'woocommerce' ),
					'type'          => 'text',
					'desc_tip'      => true,
					'description'   => sprintf( __( 'Available placeholders: %s', 'woocommerce' ),
						'<code>{chapter_name}</code>
						<br>
						<code>{activation_link}</code>'
					),
					'placeholder'   => $this->get_default_subject(),
					'default'       => '',
				),

				'heading' => array(
					'title'         => __( 'Email heading', 'woocommerce' ),
					'type'          => 'text',
					'desc_tip'      => true,
					'description'   => sprintf( __( 'Available placeholders: %s', 'woocommerce' ),
						'<code>{chapter_name}</code>
						<br>
						<code>{activation_link}</code>'
					),
					'placeholder'   => $this->get_default_heading(),
					'default'       => '',
				),

				'greeting' => array(
					'title'         => __( 'Email Greeting', 'woocommerce' ),
					'type'          => 'text',
					'placeholder'   => $this->get_default_greeting(),
					'default'       => 'Hello',
				),

				'closing' => array(
					'title'         => __( 'Email Closing', 'woocommerce' ),
					'type'          => 'text',
					'placeholder'   => $this->get_default_closing(),
					'default'       => 'Merch House',
				),

				'email_type' => array(
					'title'         => __( 'Email type', 'woocommerce' ),
					'type'          => 'select',
					'description'   => __( 'Choose which format of email to send.', 'woocommerce' ),
					'default'       => 'html',
					'class'         => 'email_type wc-enhanced-select',
					'options'       => $this->get_email_type_options(),
					'desc_tip'      => true,
				),
			);
		}
	}

}

return new OMH_Email_User_Activation();
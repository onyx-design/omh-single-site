<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( !class_exists( 'OMH_Email_Chapter_Signup' ) ) {

	/**
	 * Signup Form Email
	 * 
	 * An email sent to the Administrator when a Signup Form has been submitted
	 */
	class OMH_Email_Chapter_Signup extends WC_Email {

		/**
		 * Constructor
		 */
		public function __construct() {
			
			$this->id             = 'design_created';
			$this->title          = __( 'Chapter Signup', OMH_TEXT_DOMAIN );
			$this->description    = __( 'Chapter Signup Emails are sent when a house ', OMH_TEXT_DOMAIN );
			$this->template_html  = 'emails/chapter-signup-html.php';
			$this->template_plain = 'emails/chapter-signup-plain.php';
			$this->placeholders   = array(
				'{site_url}'		=> site_url()
			);

			// Triggers for this email
			add_action( 'omh_email_' . $this->id, array( $this, 'trigger' ), 10, 1 );

			// Call parent constructor
			parent::__construct();
		}
		/**
		 * Get email subject
		 * 
		 */
		public function get_default_subject() {
			
			return __( 'Thanks for signing up!', OMH_TEXT_DOMAIN );

		}

		/**
		 * Get email heading
		 * 
		 */
		public function get_default_heading() {

			return __( 'Thanks for signing up!', OMH_TEXT_DOMAIN );

		}

		public function get_greeting() {

			return $this->get_option( 'greeting', $this->get_default_greeting() );
		}

		public function get_closing() {

			return $this->get_option( 'closing', $this->get_default_closing() );
		}

		/**
		 * Get email greeting
		 * 
		 */
		public function get_default_greeting() {

			return __( 'Hello', OMH_TEXT_DOMAIN );
		}

		/**
		 * Get email closing
		 * 
		 */
		public function get_default_closing() {

			return __( 'Merch House', OMH_TEXT_DOMAIN );
		}

		/**
		 * Trigger the sending of this email
		 * 
		 */
		public function trigger( $form_fields ) {

			// Get users Email Address
			$emailIndex = array_search("Email", array_column($form_fields, 'label'));
			$emailAddress = $form_fields[$emailIndex]['value'];
			$this->recipient = $emailAddress;

			// Get Name
			$nameIndex = array_search("Name", array_column($form_fields, 'label'));
			$nameText = $form_fields[$nameIndex]['value'];
			$this->name = $nameText;
			
			// Get College
			$collegeIndex = array_search("College", array_column($form_fields, 'label'));
			$collegeText = $form_fields[$collegeIndex]['value'];
			$this->college = $collegeText;
			
			// // Get Chapter Name
			$chapterIndex = array_search("Organization", array_column($form_fields, 'label'));
			$chapterText = $form_fields[$chapterIndex]['value'];
			$this->chapter = $chapterText;


			$this->setup_locale();
			$this->form_fields 						= $form_fields;
			$this->placeholders['{site_url}']		= site_url();
			$this->site_url							= site_url();

			if( $this->is_enabled() && $this->get_recipient() ) {
				$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
			}

			$this->restore_locale();

		}

		/**
		 * Get content HTML
		 * 
		 */
		public function get_content_html() {

			return wc_get_template_html( 
				$this->template_html, 
				array(
					'email_heading' 	=> $this->get_heading(),
					'greeting'			=> $this->get_greeting(),
					'site_url'			=> $this->placeholders['{site_url}'],
					'form_fields'		=> $this->form_fields,
					'name'				=> $this->name,
					'college'			=> $this->college,
					'chapter'			=> $this->chapter,
					'closing'			=> $this->get_closing(),
					'sent_to_admin' 	=> true,
					'plain_text'    	=> false,
					'email'				=> $this,
				),
				'',
				OMH_PATH . '/templates/'
			);

		}

		/**
		 * Get content plain
		 * 
		 */
		public function get_content_plain() {

			return wc_get_template_html( 
				$this->template_plain, 
				array(
					'email_heading' 	=> $this->get_heading(),
					'greeting'			=> $this->get_greeting(),
					'site_url'			=> $this->placeholders['{site_url}'],
					'form_fields'		=> $this->form_fields,
					'closing'			=> $this->get_closing(),
					'sent_to_admin' 	=> true,
					'plain_text'    	=> true,
					'email'				=> $this,
				),
				'',
				OMH_PATH . '/templates/'
			);

		}

		/**
		 * Initialize settings form fields
		 */
		public function init_form_fields() {
			$this->form_fields = array(

				'enabled' => array(
					'title'         => __( 'Enable/Disable', 'woocommerce' ),
					'type'          => 'checkbox',
					'label'         => __( 'Enable this email notification', 'woocommerce' ),
					'default'       => 'yes',
				),

				'subject' => array(
					'title'         => __( 'Subject', 'woocommerce' ),
					'type'          => 'text',
					'desc_tip'      => true,
					'description'   => sprintf( __( 'Available placeholders: %s', 'woocommerce' ),
						'<code>{site_url}</code>'
					),
					'placeholder'   => $this->get_default_subject(),
					'default'       => '',
				),

				'heading' => array(
					'title'         => __( 'Email heading', 'woocommerce' ),
					'type'          => 'text',
					'desc_tip'      => true,
					'description'   => sprintf( __( 'Available placeholders: %s', 'woocommerce' ),
						'<code>{site_url}</code>'
					),
					'placeholder'   => $this->get_default_heading(),
					'default'       => '',
				),

				'greeting' => array(
					'title'         => __( 'Email Greeting', 'woocommerce' ),
					'type'          => 'text',
					'placeholder'   => $this->get_default_greeting(),
					'default'       => 'Hello',
				),

				'closing' => array(
					'title'         => __( 'Email Closing', 'woocommerce' ),
					'type'          => 'text',
					'placeholder'   => $this->get_default_closing(),
					'default'       => 'Merch House',
				),

				'email_type' => array(
					'title'         => __( 'Email type', 'woocommerce' ),
					'type'          => 'select',
					'description'   => __( 'Choose which format of email to send.', 'woocommerce' ),
					'default'       => 'html',
					'class'         => 'email_type wc-enhanced-select',
					'options'       => $this->get_email_type_options(),
					'desc_tip'      => true,
				),
			);
		}
	}

}

return new OMH_Email_Chapter_Signup();
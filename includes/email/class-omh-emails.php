<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * This class contains the action Triggers for sending out WooCommerce Emails.
 * When an action occurs, this class will send out the email for that action.
 * 
 * dev:todo This can be setup so that emails can be turned off
 */
class OMH_Emails {

	public function __construct() {

		// Disable user update password
		add_filter( 'send_password_change_email', '__return_false' );

		// Disable user update email
		add_filter( 'send_email_change_email', '__return_false' );

		// Add Custom WC Emails
		add_filter( 'woocommerce_email_classes', array( $this, 'omh_wc_custom_emails' ) );

		// Send emails to House Admins when a design is created
		add_action( 'omh_send_design_created', array( $this, 'send_design_created_email' ), 10, 1 );

		// Send emails to House Admins when a design is approved
		// add_action( 'omh_send_design_approved', array( $this, 'send_design_approved_email' ), 10, 2 );

		// Send emails to Merch House for Signup Form
		add_action( 'omh_send_signup_form', array( $this, 'send_signup_form_email' ), 10, 1 );
		
		// Send emails to Merch House for Start Design Form
		add_action( 'omh_send_start_design_form', array( $this, 'send_start_design_form_email' ), 10, 1 );

		// Send emails to Merch House for Start Design Form
		add_action( 'omh_send_organization_start_design_form', array( $this, 'send_organization_start_design_form_email' ), 10, 1 );

		// Send emails to new house signup
		add_action( 'omh_send_chapter_signup', array( $this,'send_chapter_signup_email'), 10, 1 );

		// Send emails to Merch House alerting them of new users
		add_action( 'omh_admin_new_user_notification', array( $this, 'send_admin_new_user_notification' ), 10, 1 );

		// Send email to new user
		add_action( 'omh_new_user_notification', array( $this, 'send_new_user_notification' ), 10, 1 );

		// Send email to Merch House alerting them of new activated user
		add_action( 'omh_new_user_notification', array( $this, 'send_admin_user_activated_notification' ), 10, 1 );

		// Send email to new user for activation
		add_action( 'omh_user_activation_notification', array( $this, 'send_user_activation_notification' ), 10, 1 );

		// Send email when house member is invited
		add_action( 'omh_house_member_welcome_notification', array( $this, 'send_house_member_welcome_notification' ), 10, 1 );

		// Send email when new user when already added as house member
		add_action( 'omh_house_member_verification_notification', array( $this, 'send_house_member_verification_notification' ), 10, 1 );

		// Send email when user is upgraded to house member
		add_action( 'omh_house_member_upgrade_notification', array( $this, 'send_house_member_upgrade_notification' ), 10, 1 );

		// Send email when someone follows a chapter or organization (clicks the Follow button)
		add_action( 'omh_admin_new_follower_notification', array( $this, 'send_admin_new_follower_notification' ), 10, 1 );

		// Add the Tracking information to the emails
		add_filter( 'woocommerce_email_format_string', array( $this, 'custom_wc_email_tokens' ), 10, 2 );

		add_action( 'woocommerce_email', array( $this, 'unhook_wc_emails' ) );
	}

	/**
	 * WooCommerce custom emails
	 **/
	public function omh_wc_custom_emails( $email_classes ) {
		
		// New Emails
		$email_classes['OMH_Email_Design_Created']					= include( OMH_PATH . '/includes/email/class-omh-email-design-created.php' );
		// $email_classes['OMH_Email_Design_Approved'] 				= include( OMH_PATH . '/includes/email/class-omh-email-design-approved.php' );
		$email_classes['OMH_Email_Signup_Form']						= include( OMH_PATH . '/includes/email/class-omh-email-signup-form.php' );
		$email_classes['OMH_Email_Start_Design_Form']				= include( OMH_PATH . '/includes/email/class-omh-email-start-design-form.php' );
		$email_classes['OMH_Email_Organization_Start_Design_Form']	= include( OMH_PATH . '/includes/email/class-omh-email-organization-start-design-form.php' );
		$email_classes['OMH_Email_Chapter_Signup']					= include( OMH_PATH . '/includes/email/class-omh-email-chapter-signup.php' );
		$email_classes['OMH_Email_Admin_New_User']					= include( OMH_PATH . '/includes/email/class-omh-email-admin-new-user-notification.php' );
		$email_classes['OMH_Email_New_User']						= include( OMH_PATH . '/includes/email/class-omh-email-new-user-notification.php' );
		$email_classes['OMH_Email_Admin_User_Activated']			= include( OMH_PATH . '/includes/email/class-omh-email-admin-user-activated.php' );
		$email_classes['OMH_Email_Admin_House_Member_List']			= include( OMH_PATH . '/includes/email/class-omh-email-admin-house-member-list.php' );
		$email_classes['OMH_Email_User_Activation']					= include( OMH_PATH . '/includes/email/class-omh-email-user-activation.php' );
		$email_classes['OMH_Email_House_Member_Welcome']			= include( OMH_PATH . '/includes/email/class-omh-email-house-member-welcome.php' );
		$email_classes['OMH_Email_House_Member_Upgrade']			= include( OMH_PATH . '/includes/email/class-omh-email-house-member-upgrade.php' );
		$email_classes['OMH_Email_House_Member_Verification']		= include( OMH_PATH . '/includes/email/class-omh-email-house-member-verification.php' );

		$email_classes['OMH_Email_Admin_New_Follower']				= include( OMH_PATH . '/includes/email/class-omh-email-admin-new-follower.php' );

    	return $email_classes;
	}

	public static function send_design_created_email( $post_id ) {
		WC()->mailer()->emails['OMH_Email_Design_Created']->trigger( $post_id );
	}

	// public static function send_design_approved_email( $post_id, $order_id = false ) {
	// 	WC()->mailer()->emails['OMH_Email_Design_Approved']->trigger( $post_id, $order_id );
	// }

	public static function send_signup_form_email( $form_fields ) {
		WC()->mailer()->emails['OMH_Email_Signup_Form']->trigger( $form_fields );
	}

	public static function send_start_design_form_email( $form_fields ) {
		WC()->mailer()->emails['OMH_Email_Start_Design_Form']->trigger( $form_fields );
	}

	public static function send_organization_start_design_form_email( $form_fields ) {
		WC()->mailer()->emails['OMH_Email_Organization_Start_Design_Form']->trigger( $form_fields );
	}

	public static function send_chapter_signup_email( $form_fields ) {
		WC()->mailer()->emails['OMH_Email_Chapter_Signup']->trigger( $form_fields );
	}

	public static function send_admin_new_user_notification( $user_id ) {
		WC()->mailer()->emails['OMH_Email_Admin_New_User']->trigger( $user_id );
	}

	public static function send_new_user_notification( $user_id ) {
		WC()->mailer()->emails['OMH_Email_New_User']->trigger( $user_id );
	}

	public static function send_admin_user_activated_notification( $user_id ) {
		WC()->mailer()->emails['OMH_Email_Admin_User_Activated']->trigger( $user_id );
	}

	public static function send_admin_house_member_list( $results ) {
		WC()->mailer()->emails['OMH_Email_Admin_House_Member_List']->trigger( $results );
	}

	public static function send_user_activation_notification( $user_id ) {
		WC()->mailer()->emails['OMH_Email_User_Activation']->trigger( $user_id );
	}

	public static function send_house_member_welcome_notification( $user_id ) {
		WC()->mailer()->emails['OMH_Email_House_Member_Welcome']->trigger( $user_id );
	}

	public static function send_house_member_upgrade_notification( $user_id ) {
		WC()->mailer()->emails['OMH_Email_House_Member_Upgrade']->trigger( $user_id );
	}

	public static function send_house_member_verification_notification( $user_id ) {
		WC()->mailer()->emails['OMH_Email_House_Member_Verification']->trigger( $user_id );
	}

	public static function send_admin_new_follower_notification( $follower ) {
		WC()->mailer()->emails['OMH_Email_Admin_New_Follower']->trigger( $follower );
	}

	public function custom_wc_email_tokens( $replace, $wc_email ) {

		$custom_tokens = array(
			// '{tracking_details}'	=> 'get_email_tracking_details',
			'{order_date}'			=> 'get_order_date'
		);

		if( $object = $wc_email->object ) {

			foreach( $custom_tokens as $custom_token => $token_function ) {
				$replace = str_replace( $custom_token, call_user_func( array( $this, $token_function ), $object ), $replace );
			}
		}

		return $replace;
	}

	public function get_order_date( $order ) {

		if( method_exists( $order, 'get_date_paid' ) && ( $date_paid = $order->get_date_paid() ) ) {

			return $date_paid->date( 'F d, Y h:i:s' );
		}

		return '';

	}

  // this is now defunct
	public function get_email_tracking_details( $order ) {

		$tracking_details = '';

		if( method_exists( $order, 'get_tracking' ) ) {

			if( $order->get_tracking() ) {

				$tracking_url = $order->get_tracking_url();

				ob_start();
				?>
					<h2>Tracking details</h2>
					<p>
						<a href="<?php echo $tracking_url; ?>" target="_blank">Track Your Order</a>
					</p>
				<?php
				$tracking_details = ob_get_clean();
			}
		}

		return $tracking_details;
	}

	public function unhook_wc_emails( $email_class ) {

		// Remove Order Note Emails
		remove_action( 'woocommerce_new_customer_note_notification', array( $email_class->emails['WC_Email_Customer_Note'], 'trigger' ) );
	}

	public static function get_subscription_users( $subscription_list, $roles = null, $chapter_id = null) {
		$query_params = array();

		// Subscription List
		if (!isset($subscription_list)){
			return new WP_Error('invalid_list','Invalid List');
		}

		$subscription_key = '_omh_notifications_' . $subscription_list;
		
		$subscription_meta = array(
			'relation' => 'OR',
			array(
				'key'		=> $subscription_key,
				'value'		=> 'true',
				'compare'	=> '='
			),
			array(
				'key'		=> $subscription_key,
				'value'		=> '',
				'compare'	=> 'NOT EXISTS',
			)
		);
		$query_params['meta_query']['subscription_meta'] = $subscription_meta;

		// User Roles
		if(!empty($roles)){
			
			$roles = (array) $roles;
			
			// Verify roles selected are allowed
			$allowed_roles = OMH_User::get_allowed_roles(true);
			$disallowed_roles = array_diff( $roles, $allowed_roles);
			if (!empty($disallowed_roles)){
				return new WP_Error('invalid_role', 'Invalid Role Type');
			}
			else {
				$query_params['role__in'] = $roles;
			}
		}

		// Chapters
		if($chapter_id !== false){
			// if chapter id is not set, get session
			if(!isset($chapter_id)){
				$chapter_id = OMH()->session->get_chapter()->get_id();
			}

			$chapter_meta = array(
				'relation' => 'AND',
				array(
				'key' => 'primary_chapter_id',
				'value' => $chapter_id
				)
			);
			$query_params['meta_query']['chapter_meta'] = $chapter_meta;
		}

		$users = get_users($query_params) ;
		if( empty( $users ) ) {
			return null;
		}
		
		// return $users;
		$c = count($users);
		return $c;
	}
}
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( !class_exists( 'OMH_Email_Design_Approved' ) ) {

	/**
	 * Design Approved Email
	 * 
	 * An email sent to the House Admin(s) when a design has been approved
	 */
	class OMH_Email_Design_Approved extends WC_Email {

		/**
		 * Constructor
		 */
		public function __construct() {
			
			$this->id             = 'design_approved';
			$this->title          = __( 'Design Approved', OMH_TEXT_DOMAIN );
			$this->description    = __( 'Design Approved emails are sent when a design has been approved by both Merch House and the House Admin', OMH_TEXT_DOMAIN );
			$this->template_html  = 'emails/design-approved-html.php';
			$this->template_plain = 'emails/design-approved-plain.php';
			$this->placeholders   = array(
				'{site_title}'		=> $this->get_blogname(),
				'{design_name}'		=> ''
			);

			// Triggers for this email
			add_action( 'omh_email_' . $this->id, array( $this, 'trigger' ), 10, 1 );

			// Call parent constructor
			parent::__construct();

			// Other settings
			$this->recipient = $this->get_recipients();

		}

		public function get_recipients() {
			$users = get_users( array( 'role' => 'house_admin', 'fields' => array( 'user_email' ) ) );

			$recipients = '';

			if( $users ) {
				foreach ( $users as $user ) {
					$recipients .= $user->user_email . ',';
				}
			}

			return rtrim( $recipients, ',' );

		}

		/**
		 * Get email subject
		 * 
		 */
		public function get_default_subject() {
			
			return __( '[{site_title}]: Your "{design_name}" Design Has Been Approved', OMH_TEXT_DOMAIN );

		}

		/**
		 * Get email heading
		 * 
		 */
		public function get_default_heading() {

			return __( '"{design_name}" Design Approved', OMH_TEXT_DOMAIN );

		}

		public function get_greeting() {

			return $this->get_option( 'greeting', $this->get_default_greeting() );
		}

		public function get_closing() {

			return $this->get_option( 'closing', $this->get_default_closing() );
		}

		/**
		 * Get email greeting
		 * 
		 */
		public function get_default_greeting() {

			return __( 'Hello', OMH_TEXT_DOMAIN );
		}

		/**
		 * Get email closing
		 * 
		 */
		public function get_default_closing() {

			return __( 'Merch House', OMH_TEXT_DOMAIN );
		}

		/**
		 * Trigger the sending of this email
		 * 
		 */
		public function trigger( $product_id, $order_id = false ) {

			$this->setup_locale();

			$this->object 							= wc_get_product( $product_id );
			$this->placeholders['{design_name}']	= get_the_title( $product_id );
			$this->design_name						= get_the_title( $product_id );
			$this->edit_design						= site_url() . '/dashboard/edit-product?id=' . $product_id;

			// If we got an order ID, send it to the template
			$this->start_bulk_order					= false;
			if( $order_id ) {
				$this->start_bulk_order 			= site_url() . '/dashboard/order-view/?id=' . $order_id;
			}

			if( $this->is_enabled() && $this->get_recipient() ) {
				$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
			}

			$this->restore_locale();

		}

		/**
		 * Get content HTML
		 * 
		 */
		public function get_content_html() {

			return wc_get_template_html( 
				$this->template_html, 
				array(
					'product'			=> $this->object,
					'email_heading' 	=> $this->get_heading(),
					'greeting'			=> $this->get_greeting(),
					'design_name'		=> $this->placeholders['{design_name}'],
					'edit_design'		=> $this->edit_design,
					'start_bulk_order'	=> $this->start_bulk_order,
					'closing'			=> $this->get_closing(),
					'sent_to_admin' 	=> true,
					'plain_text'    	=> false,
					'email'				=> $this,
				),
				'',
				OMH_PATH . '/templates/'
			);

		}

		/**
		 * Get content plain
		 * 
		 */
		public function get_content_plain() {

			return wc_get_template_html( 
				$this->template_plain, 
				array(
					'product'			=> $this->object,
					'email_heading' 	=> $this->get_heading(),
					'greeting'			=> $this->get_greeting(),
					'design_name'		=> $this->placeholders['{design_name}'],
					'edit_design'		=> $this->edit_design,
					'start_bulk_order'	=> $this->start_bulk_order,
					'closing'			=> $this->get_closing(),
					'sent_to_admin' 	=> true,
					'plain_text'    	=> true,
					'email'				=> $this,
				),
				'',
				OMH_PATH . '/templates/'
			);

		}

		/**
		 * Initialize settings form fields
		 */
		public function init_form_fields() {
			$this->form_fields = array(

				'enabled' => array(
					'title'         => __( 'Enable/Disable', 'woocommerce' ),
					'type'          => 'checkbox',
					'label'         => __( 'Enable this email notification', 'woocommerce' ),
					'default'       => 'yes',
				),

				'recipient' => array(
					'title'         => __( 'Recipient(s)', 'woocommerce' ),
					'type'          => 'text',
					'description'   => sprintf( __( 'Enter recipients (comma separated) for this email. Defaults to %s.', 'woocommerce' ), '<code>' . $this->get_recipients() . '</code>' ),
					'placeholder'   => '',
					'default'       => '',
					'desc_tip'      => true,
				),

				'subject' => array(
					'title'         => __( 'Subject', 'woocommerce' ),
					'type'          => 'text',
					'desc_tip'      => true,
					'description'   => sprintf( __( 'Available placeholders: %s', 'woocommerce' ),
						'<code>{site_title}</code>
						<br>
						<code>{design_name}</code>'
					),
					'placeholder'   => $this->get_default_subject(),
					'default'       => '',
				),

				'heading' => array(
					'title'         => __( 'Email heading', 'woocommerce' ),
					'type'          => 'text',
					'desc_tip'      => true,
					'description'   => sprintf( __( 'Available placeholders: %s', 'woocommerce' ),
						'<code>{site_title}</code>
						<br>
						<code>{design_name}</code>'
					),
					'placeholder'   => $this->get_default_heading(),
					'default'       => '',
				),

				'greeting' => array(
					'title'         => __( 'Email Greeting', 'woocommerce' ),
					'type'          => 'text',
					'placeholder'   => $this->get_default_greeting(),
					'default'       => 'Hello',
				),

				'closing' => array(
					'title'         => __( 'Email Closing', 'woocommerce' ),
					'type'          => 'text',
					'placeholder'   => $this->get_default_closing(),
					'default'       => 'Merch House',
				),

				'email_type' => array(
					'title'         => __( 'Email type', 'woocommerce' ),
					'type'          => 'select',
					'description'   => __( 'Choose which format of email to send.', 'woocommerce' ),
					'default'       => 'html',
					'class'         => 'email_type wc-enhanced-select',
					'options'       => $this->get_email_type_options(),
					'desc_tip'      => true,
				),
			);
		}
	}

}

return new OMH_Email_Design_Approved();
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( !class_exists( 'OMH_Email_Admin_New_Follower' ) ) {

	/**
	 * New Store Follower Admin Email
	 * 
	 * An email sent to the House Admin(s) when someone follows a store
	 */
	class OMH_Email_Admin_New_Follower extends WC_Email {

		public $follower;

		/**
		 * Constructor
		 */
		public function __construct() {
			
			$this->id             = 'admin_new_follower_notification';
			$this->title          = __( 'New Store Follower Notification', OMH_TEXT_DOMAIN );
			$this->description    = __( 'MH Admin notification email when someone follows a store', OMH_TEXT_DOMAIN );
			$this->template_html  = 'emails/admin-new-follower-html.php';
			$this->template_plain = 'emails/admin-new-follower-plain.php';
			$this->placeholders   = array(
				'{follower_email}'		=> '',
				'{following_type}'		=> '',
				'{following_store_name}'=> '',
				'{site_url}'		=> site_url()
			);

			// Triggers for this email
			add_action( 'omh_email_' . $this->id, array( $this, 'trigger' ), 10, 1 );

			// Call parent constructor
			parent::__construct();

			// Other settings
			$this->recipient = $this->get_recipients();

		}

		public function get_recipients() {

			$recipients = get_option( 'admin_email', 'admin@merch.house' );

			return rtrim( $recipients, ',' );

		}

		/**
		 * Get email subject
		 * 
		 */
		public function get_default_subject() {

			$subject = '';
			
			if( !OMH()->is_live_site() ) {
				$subject .= '[DEV] ';
			}

			$subject .= 'New Store Follower - {following_store_name}';

			return __( $subject, OMH_TEXT_DOMAIN );

		}

		/**
		 * Get email heading
		 * 
		 */
		public function get_default_heading() {

			return __( 'New Store Follower', OMH_TEXT_DOMAIN );

		}

		public function get_greeting() {

			return $this->get_default_greeting();
		}

		public function get_closing() {

			return $this->get_default_closing();
		}

		/**
		 * Get email greeting
		 * 
		 */
		public function get_default_greeting() {

			return __( 'Hello', OMH_TEXT_DOMAIN );
		}

		/**
		 * Get email closing
		 * 
		 */
		public function get_default_closing() {

			return __( 'Merch House', OMH_TEXT_DOMAIN );
		}

		/**
		 * Trigger the sending of this email
		 * 
		 */
		public function trigger( $follower ) {

			$this->setup_locale();


			$this->object 							= $follower;
			$this->follower 						= $this->object;


			$this->placeholders['{follower_email}'] 	= $follower->get_follower_email();
			$this->placeholders['{following_type}'] 	= $follower->get_following_type();
			$this->placeholders['{following_store_name}'] = $follower->get_following_store_name();


			$this->placeholders['{site_url}']		= site_url();

			if( $this->is_enabled() && $this->get_recipient() ) {
				$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
			}

			$this->restore_locale();

		}

		/**
		 * Get content HTML
		 * 
		 */
		public function get_content_html() {

			return wc_get_template_html( 
				$this->template_html, 
				array(
					'follower'			=> $this->object,
					'email_heading' 	=> $this->get_heading(),
					'greeting'			=> $this->get_greeting(),
					'following_store_name'=> $this->placeholders['{following_store_name}'],
					'following_type'	=> $this->placeholders['{following_type}'],
					'follower_email'	=> $this->placeholders['{follower_email}'],
					'site_url'			=> $this->placeholders['{site_url}'],
					'closing'			=> $this->get_closing(),
					'sent_to_admin' 	=> true,
					'plain_text'    	=> false,
					'email'				=> $this,
				),
				'',
				OMH_PATH . '/templates/'
			);

		}

		/**
		 * Get content plain
		 * 
		 */
		public function get_content_plain() {

			return wc_get_template_html( 
				$this->template_plain, 
				array(
					'follower'			=> $this->object,
					'email_heading' 	=> $this->get_heading(),
					'greeting'			=> $this->get_greeting(),
					'following_store_name'=> $this->placeholders['{following_store_name}'],
					'following_type'	=> $this->placeholders['{following_type}'],
					'follower_email'	=> $this->placeholders['{follower_email}'],
					'site_url'			=> $this->placeholders['{site_url}'],
					'closing'			=> $this->get_closing(),
					'sent_to_admin' 	=> true,
					'plain_text'    	=> true,
					'email'				=> $this,
				),
				'',
				OMH_PATH . '/templates/'
			);

		}

		/**
		 * Initialize settings form fields
		 */
		public function init_form_fields() {
			$this->form_fields = array(

				'enabled' => array(
					'title'         => __( 'Enable/Disable', 'woocommerce' ),
					'type'          => 'checkbox',
					'label'         => __( 'Enable this email notification', 'woocommerce' ),
					'default'       => 'yes',
				),

			

		

				'email_type' => array(
					'title'         => __( 'Email type', 'woocommerce' ),
					'type'          => 'select',
					'description'   => __( 'Choose which format of email to send.', 'woocommerce' ),
					'default'       => 'html',
					'class'         => 'email_type wc-enhanced-select',
					'options'       => $this->get_email_type_options(),
					'desc_tip'      => true,
				),
			);
		}
	}

}

return new OMH_Email_Admin_New_Follower();
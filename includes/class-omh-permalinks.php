<?php
defined( 'ABSPATH' ) || exit;

/**
 * Controls the custom permalink structures for our application
 * 
 * @tag Permalinks
 */
class OMH_Permalinks {

	public function __construct() {

		add_action( 'init', array( $this, 'mh_admin_rules' ), 10 );
		add_action( 'init', array( $this, 'dashboard_rules' ), 10 );
		add_action( 'init', array( $this, 'taxonomy_rules' ), 15 );
	}

	/**
	 * Create URLs for MH Admin Screens
	 * 
	 * Be aware that WordPress permalinks use the fallback structure,
	 * 	- The order that rewrite rules are added, are the order they are used
	 * 
	 * @tag Permalinks
	 */
	public function mh_admin_rules() {

		/**
		 * Global Garments
		 **/

		// When browsing to "https://domain/mh-admin/global-garments/products/", tell WordPress to load the page with the slug "global-garments-products"
		add_rewrite_rule( '^mh-admin/global-garments/products/?', 'index.php?pagename=mh-admin/global-garments-products', 'top' );

		// When browsing to "https://domain/mh-admin/global-garments/brands/", tell WordPress to load the page with the slug "global-garments-brands"
		add_rewrite_rule( '^mh-admin/global-garments/brands/?', 'index.php?pagename=mh-admin/global-garments-brands', 'top' );

		// When browsing to "https://domain/mh-admin/global-garments/colors/", tell WordPress to load the page with the slug "global-garments-colors"
		add_rewrite_rule( '^mh-admin/global-garments/colors/?', 'index.php?pagename=mh-admin/global-garments-colors', 'top' );

		// When browsing to "https://domain/mh-admin/global-garments/styles/", tell WordPress to load the page with the slug "global-garments-styles"
		add_rewrite_rule( '^mh-admin/global-garments/styles/?', 'index.php?pagename=mh-admin/global-garments-styles', 'top' );

		// When browsing to "https://domain/mh-admin/global-garments/", tell WordPress to load the page with the slug "global-garments-products"
		// This is the fallback, meaning if nothing comes after "/global-garments/" in the URL, then this rewrite rule is used
		add_rewrite_rule( '^mh-admin/global-garments/?', 'index.php?pagename=mh-admin/global-garments-products', 'top' );

		/**
		 * Redirect MH Admin to Products
		 **/
		add_rewrite_rule( '^mh-admin?$', 'index.php?pagename=mh-admin/products', 'top' );
	}

	/**
	 * @tag Permalinks
	 */
	public function dashboard_rules() {

		// Dashboard Chapter Pages
		add_rewrite_rule( '^chapters/([^/]*)/dashboard/([^/]*)/?', 'index.php?chapters=$matches[1]&pagename=dashboard/$matches[2]', 'top' );

		add_rewrite_rule( '^chapters/([^/]*)/dashboard/?', 'index.php?chapters=$matches[1]&pagename=dashboard', 'top' );
	}

	/**
	 * @tag Permalinks
	 */
	public function taxonomy_rules() {

		// Chapter Products
		add_rewrite_rule( '^chapters/([^/]*)/products/([^/]*)/page/([^/]*)/?', 		'index.php?chapters=$matches[1]&product=$matches[2]&paged=$matches[3]', 'top' );
		add_rewrite_rule( '^chapters/([^/]*)/products/([^/]*)/?', 					'index.php?chapters=$matches[1]&product=$matches[2]', 'top' );

		// Allow us to filter chapters by product category
		add_rewrite_rule( '^chapters/([^/]*)/category/([^/]*)/page/([^/]*)/?', 		'index.php?chapters=$matches[1]&product_cat=$matches[2]&paged=$matches[3]', 'top' );
		add_rewrite_rule( '^chapters/([^/]*)/category/([^/]*)/?', 					'index.php?chapters=$matches[1]&product_cat=$matches[2]', 'top' );

		// Allow us to filter organizations by product category
		add_rewrite_rule( '^organizations/([^/]*)/category/([^/]*)/page/([^/]*)/?', 'index.php?organizations=$matches[1]&product_cat=$matches[2]&paged=$matches[3]', 'top' );
		add_rewrite_rule( '^organizations/([^/]*)/category/([^/]*)/?', 				'index.php?organizations=$matches[1]&product_cat=$matches[2]', 'top' );

		// Allow us to filter colleges by product category
		add_rewrite_rule( '^colleges/([^/]*)/category/([^/]*)/page/([^/]*)/?', 		'index.php?colleges=$matches[1]&product_cat=$matches[2]&paged=$matches[3]', 'top' );
		add_rewrite_rule( '^colleges/([^/]*)/category/([^/]*)/?', 					'index.php?colleges=$matches[1]&product_cat=$matches[2]', 'top' );

		// Allow us to filter chapters by product tag
		add_rewrite_rule( '^chapters/([^/]*)/tag/([^/]*)/page/([^/]*)/?', 			'index.php?chapters=$matches[1]&product_tag=$matches[2]&paged=$matches[3]', 'top' );
		add_rewrite_rule( '^chapters/([^/]*)/tag/([^/]*)/?', 						'index.php?chapters=$matches[1]&product_tag=$matches[2]', 'top' );

		// Allow us to filter organizations by product tag
		add_rewrite_rule( '^organizations/([^/]*)/tag/([^/]*)/page/([^/]*)/?', 		'index.php?organizations=$matches[1]&product_tag=$matches[2]&paged=$matches[3]', 'top' );
		add_rewrite_rule( '^organizations/([^/]*)/tag/([^/]*)/?', 					'index.php?organizations=$matches[1]&product_tag=$matches[2]', 'top' );
		
		// Allow us to filter colleges by product tag
		add_rewrite_rule( '^colleges/([^/]*)/tag/([^/]*)/page/([^/]*)/?', 			'index.php?colleges=$matches[1]&product_tag=$matches[2]&paged=$matches[3]', 'top' );
		add_rewrite_rule( '^colleges/([^/]*)/tag/([^/]*)/?', 						'index.php?colleges=$matches[1]&product_tag=$matches[2]', 'top' );
	}
}

return new OMH_Permalinks;
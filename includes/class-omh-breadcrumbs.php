<?php
defined( 'ABSPATH' ) || exit;

class OMH_Breadcrumbs {

	private $crumbs = array();

	public function add_crumb( $name, $link = '' ) {

		$this->crumbs[] = array(
			strip_tags( $name ),
			$link
		);

		return $this;
	}

	public function reset() {
		$this->crumbs = array();

		return $this;
	}

	/**
	 * Return the breadcrumbs array
	 * 
	 * @return 	array
	 */
	public function get_breadcrumbs() {
		return $this->crumbs;
	}

	/**
	 * Return breadcrumbs using our HTML classes
	 * 
	 * @return 	string
	 */
	public function render_breadcrumbs() {

	}

	public function generate() {
		global $wp_query;

		$this->add_crumbs_marketplace();

		if( get_query_var( 'colleges' ) ) {
			$this->add_crumbs_college();
		}

		if( get_query_var( 'organizations' ) ) {
			$this->add_crumbs_organization();
		}

		if( get_query_var( 'chapters' ) ) {
			$this->add_crumbs_chapter();
		}

		if( get_query_var( 'product' ) ) {
			$this->add_crumbs_product();
		}

		if (get_query_var('product_cat')){
			$this->add_crumbs_category();
		}

		return $this;
	}

	public function add_crumbs_marketplace() {

		$this->add_crumb('Marketplace', get_permalink( wc_get_page_id( 'shop' )));

		return $this;
	}

	/**
	 * Add crumbs for product term (category or tag)
	 */
	public function add_crumbs_product_term() {

		if( $term_slug = get_query_var( 'product_cat', get_query_var( 'product_tag', false ) ) ) {

			$term = get_term_by( 'slug', $term_slug, 'product_cat' );

			if( !$term ) {
				$term = get_term_by( 'slug', $term_slug, 'product_tag' );
			}

			if( $term ) {
				$this->add_crumb( $term->name, get_term_link( $term ) );
			}
		}

		return $this;
	}

	public function add_crumbs_organization() {

		if( $org_slug = get_query_var( 'organizations', false ) ) {

			$organization = OMH()->organization_factory->get_by_term_slug( $org_slug );

			if( $organization ) {

				$this->add_crumb( $organization->get_org_name(), $organization->get_term_link() );
			}
		}

		return $this;
	}

	public function add_crumbs_college() {

		if( $college_slug = get_query_var( 'colleges', false ) ) {

			$college = OMH()->college_factory->get_by_term_slug( $college_slug );

			if( $college ) {

				$this->add_crumb( $college->get_college_name(), $college->get_term_link() );
			}
		}

		return $this;
	}

	public function add_crumbs_chapter() {

		if( $chapter_url = get_query_var( 'chapters', false ) ) {

			$chapter = OMH()->chapter_factory->get_by_url_structure( $chapter_url );

			if( $chapter ) {
				$organization = $chapter->get_org();

				$this->add_crumb( $organization->get_org_name(), $organization->get_term_link() );

				$this->add_crumb( $chapter->get_chapter_name(), $chapter->get_term_link() );

			}
		}

		return $this;
	}

	public function add_crumbs_product( $post_id = 0, $permalink = '' ) {

		if( !$post_id ) {
			global $post;
		} else {
			$post = get_post( $post_id );
		}

		if( !$permalink ) {
			$permalink = get_permalink( $post );
		}

		if( 'product' === get_post_type( $post ) ) {

			$this->add_crumb( get_the_title( $post ), $permalink );
		}

		return $this;
	}

	public function add_crumbs_category() {
		$this->add_crumbs_product_term();
	}
}
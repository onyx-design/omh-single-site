<?php
defined( 'ABSPATH' ) || exit;

class OMH_Cart_Bulk {

	public static $cart_item_key = 'bulk_order';

	public static function init() {

		// @tag To Do
		// Add function hook to clear cart if the user moves away from checkout page with bulk order in their cart
		add_action( 'template_redirect', array( __CLASS__, 'maybe_remove_cart_bulk_order' ), 5 );

		// Make sure bulk meta data persists between sessions
		add_filter( 'woocommerce_get_cart_item_from_session', array( __CLASS__, 'get_cart_item_from_session' ), 10, 3 );
		add_action( 'woocommerce_cart_loaded_from_session', array( __CLASS__, 'cart_items_loaded_from_session' ), 10 );

		// Make sure fees are added to the cart
		add_action( 'woocommerce_cart_calculate_fees', array( __CLASS__, 'maybe_add_fees' ), 10, 1 );
		
		// Check if a user is requesting to pay for a bulk order, needs to happen after $wp->query_vars are set
		add_action( 'template_redirect', array( __CLASS__, 'maybe_setup_cart' ), 100 );

		add_action( 'woocommerce_remove_cart_item', array( __CLASS__, 'maybe_remove_items' ), 10, 1 );
		add_action( 'woocommerce_before_cart_item_quantity_zero', array( __CLASS__, 'maybe_remove_items' ), 10, 1 );

		// Use original order price when ordering products with addons (to ensure the adds on prices are included)
		add_filter( 'woocommerce_product_addons_adjust_price', array( __CLASS__, 'product_addons_adjust_price' ), 10, 2 );

		// When loading checkout address details, use the bulk order address details for bulk orders
		add_filter( 'woocommerce_checkout_get_value', array( __CLASS__, 'checkout_get_value' ), 10, 2 );

		// If the shipping address on a bulk order differs to the order's billing address, check the "Ship to different address" automatically to make sure the bulk order's fields are used by default
		add_filter( 'woocommerce_ship_to_different_address_checked', array( __CLASS__, 'maybe_check_ship_to_different_address' ), 100, 1 );

		add_filter( 'woocommerce_get_item_data', array( __CLASS__, 'display_line_item_data_in_cart' ), 10, 2 );

		add_action( 'woocommerce_checkout_create_order_line_item',  array( __CLASS__, 'add_order_line_item_meta' ), 10, 3 );

		// For order items created as part of a bulk order, keep a record of the cart item key so that we can match it later once the order item has been saved and has an ID
		add_action( 'woocommerce_checkout_create_order_line_item', array( __CLASS__, 'add_line_item_meta' ), 10, 3 );

		// After order meta is saved, get the order line item ID for the bulk order so we can update it later
		add_action( 'woocommerce_checkout_update_order_meta', array( __CLASS__, 'set_order_item_id' ), 10, 2 );

		// Don't display cart item key meta stored above on the Edit Order screen
		add_action( 'woocommerce_hidden_order_itemmeta', array( __CLASS__, 'hidden_order_itemmeta' ), 10 );

		// Set URL parameter for manual subscription renewals
		add_filter( 'woocommerce_get_checkout_payment_url', array( __CLASS__, 'get_checkout_payment_url' ), 10, 2 );

		// When a failed renewal order is paid for via checkout, make sure WC_Checkout::create_order() preserves its "failed" status until it is paid
		add_filter( 'woocommerce_default_order_status', array( __CLASS__, 'maybe_preserve_order_status' ) );

		// When a failed/pending bulk order is paid for via checkout, ensure a new order isn't created due to mismatched cart hashes
		add_filter( 'woocommerce_create_order', array( __CLASS__, 'update_cart_hash' ), 10, 1 );
		
		// When a user is prevented from paying for a failed/pending bulk order because they aren't logged in, redirect them back after login
		add_filter( 'woocommerce_login_redirect', array( __CLASS__, 'maybe_redirect_after_login' ), 10 , 1 );

		// Once we have finished updating the bulk order on checkout, update the session cart so the cart changes are honoured.
		add_action( 'woocommerce_checkout_order_processed', array( __CLASS__, 'update_session_cart_after_updating_bulk_order' ), 10 );

		add_filter( 'wc_dynamic_pricing_apply_cart_item_adjustment', array( __CLASS__, 'prevent_compounding_dynamic_discounts' ), 10, 2 );
		
		// Trigger special hook when payment is completed on bulk orders
		add_action( 'woocommerce_payment_complete', array( __CLASS__, 'trigger_bulk_payment_complete' ), 10 );
	}

	public static function maybe_remove_cart_bulk_order() {

		$cart_item = static::cart_contains();
		$not_checkout = ( is_page() && !is_checkout() ) || is_single() || is_tax() && !is_404();

		if( $cart_item && $not_checkout ) {
			
			$order = static::get_order( $cart_item );

			WC()->cart->empty_cart();

			//remove the bulk order flag
			unset( WC()->session->order_awaiting_payment );

			$bulk_order_removed_notice = 'The bulk product has been removed from your cart as it can only be edited from the bulk product screen. If this was a mistake, you can go back and <a href="'. esc_url( $order->get_checkout_payment_url() ) . '">proceed to checkout</a>';

			wc_clear_notices();
			wc_add_notice( $bulk_order_removed_notice, 'notice' );
			$omh_notices[] = array(
				'text'			=> $bulk_order_removed_notice,
				'color'			=> 'primary',
				'dismissible'	=> true
			);
		}
	}

	/**
	 * Redirect back to pay for an order after successfully logging in.
	 */
	public static function maybe_redirect_after_login( $redirect ) {

		if ( isset( $_GET['omh_redirect'], $_GET['omh_redirect_id'] ) && 'pay_for_order' == $_GET['omh_redirect'] ) {

			$order = wc_get_order( $_GET['omh_redirect_id'] );

			if ( $order ) {
				$redirect = $order->get_checkout_payment_url();
			}
		}

		return $redirect;
	}

	/**
	 * Force an update to the session cart after updating bulk order line items.
	 */
	public function update_session_cart_after_updating_bulk_order() {

		if ( static::cart_contains() ) {
			// Update the cart stored in the session with the new data
			WC()->session->cart = WC()->cart->get_cart_for_session();
		}
	}

	/**
	* Prevent compounding dynamic discounts on cart items.
	* Dynamic discounts are copied from the subscription to the renewal order and so don't need to be applied again in the cart.
	*
	* @param bool Whether to apply the dynamic discount
	* @param string The cart item key of the cart item the dynamic discount is being applied to.
	* @return bool
	* @since  2.1.4
	*/
	function prevent_compounding_dynamic_discounts( $adjust_price, $cart_item_key ) {

		if ( $adjust_price && isset( WC()->cart->cart_contents[ $cart_item_key ][ static::$cart_item_key ] ) ) {
			$adjust_price = false;
		}

		return $adjust_price;
	}

	/**
	 * Restore bulk flag when cart is reset and modify Product object with bulk order related info
	 */
	public static function get_cart_item_from_session( $cart_item_session_data, $cart_item, $key ) {

		if ( isset( $cart_item[ static::$cart_item_key ]['bulk_order_id'] ) ) {
			$cart_item_session_data[ static::$cart_item_key ] = $cart_item[ static::$cart_item_key ];

			$_product = $cart_item_session_data['data'];

			// Need to get the original order price, not the current price
			$order = static::get_order( $cart_item );
			
			if ( $order ) {
				
				$order_items = $order->get_items();

				$bulk_item = $order_items[ $cart_item_session_data[ static::$cart_item_key ]['line_item_id'] ];

				if( !$bulk_item ) {
					return $cart_item_session_data;
				}

				$price = $bulk_item['line_subtotal'];

				if ( $_product->is_taxable() && $order->get_prices_include_tax() ) {
					$price += $bulk_item['subtotal_tax'];
				}

				$_product->set_price( $price / $bulk_item['qty'] );

				$line_item_name = is_callable( $bulk_item, 'get_name' ) ? $bulk_item->get_name() : $bulk_item['name'];
				static::set_objects_property( $_product, 'name', $line_item_name, 'set_prop_only' );

				// Make sure the same quantity is added
				$cart_item_session_data['quantity'] = $bulk_item['qty'];
			}
		}

		return $cart_item_session_data;
	}

	/**
	 * Does some housekeeping. Fires after the items have been passed through the get items from session filter. Because
	 * that filter is not good for removing cart items, we need to work around that by doing it later, in the cart
	 * loaded from session action.
	 *
	 * This checks cart items whether underlying bulk orders they depend on exist. If not, they are
	 * removed from the cart.
	 */
	public static function cart_items_loaded_from_session( $cart ) {

		$removed_count_order = 0;

		foreach ( $cart->cart_contents as $key => $item ) {

			if ( isset( $item[ static::$cart_item_key ]['bulk_order_id'] ) 
				&& !OMH_Order_Manager::is_bulk_order( $item[ static::$cart_item_key ]['bulk_order_id'] ) 
			) {

				$cart->remove_cart_item( $key );
				$removed_count_order++;
				continue;
			}
		}

		if ( $removed_count_order ) {

			$error_message = _n( 'We couldn\'t find the original bulk order for an item in your cart. The item was removed.', 'We couldn\'t find the original bulk orders for items in your cart. The items were removed.', $removed_count_order );

			if ( !wc_has_notice( $error_message, 'notice' ) ) {
				wc_add_notice( $error_message, 'notice' );
			}
		}
	}

	/**
	 * Add order fee line items to the cart when a bulk order is in the cart.
	 *
	 * @param WC_Cart $cart
	 */
	public static function maybe_add_fees( $cart ) {

		if ( $cart_item = static::cart_contains() ) {

			$order = static::get_order( $cart_item );

			/**
			 * Allow other plugins to remove/add fees of an existing order prior to building the cart without changing the saved order values
			 * (e.g. payment gateway based fees can remove fees and later can add new fees depending on the actual selected payment gateway)
			 *
			 * @param WC_Order $order is renderd by reference - change meta data of this object
			 * @param WC_Cart $cart
			 */
			do_action( 'woocommerce_adjust_order_fees_for_setup_cart_for_' . static::$cart_item_key, $order, $cart );

			if ( $order instanceof WC_Order ) {

				foreach ( $order->get_fees() as $fee ) {
					$cart->add_fee( $fee['name'], $fee['line_total'], abs( $fee['line_tax'] ) > 0, $fee['tax_class'] );
				}
			}
		}
	}

	/**
	 * 
	 * 
	 * @tag 	Bulk Product
	 * 
	 * @return 	void
	 */
	public static function maybe_setup_cart() {

		global $wp;

		if ( isset( $_GET['pay_for_order'] ) && isset( $_GET['key'] ) && isset( $wp->query_vars['order-pay'] ) ) {

			// Pay for existing order
			$user = OMH_User::get_user( get_current_user_id() );
			$order_key = $_GET['key'];
			$order_id = ( isset( $wp->query_vars['order-pay'] ) ) ? $wp->query_vars['order-pay'] : absint( $_GET['order_id'] );
			$order = wc_get_order( $wp->query_vars['order-pay'] );
			$product = $order->get_bulk_product();
			$product_chapter = $product->get_chapter();

			// TODO: Which statuses will be used for this?
			if ( static::get_objects_property( $order, 'order_key' ) == $order_key 
				&& $order->has_status( array( 'pending', 'failed' ) ) 
				&& $order->has_bulk_product()
			) {
				
				// If a user isn't logged in, allow them to login first and then redirect back
				if( !is_user_logged_in() ) {

					$redirect = add_query_arg(
						array(
							'omh_redirect'		=> 'pay_for_order',
							'omh_redirect_id'	=> $order_id,
						),
						get_permalink( wc_get_page_id( 'myaccount' ) )
					);

					wp_safe_redirect( $redirect );
					exit;
					
				// dev:todo Check if order belongs to chapter?
				} elseif( !$user->is_house_admin( false ) || !$user->is_member_of_house( $product_chapter->get_id() ) ) {

					wc_add_notice( 'That doesn\'t appear to be your order.', 'error' );

					wp_safe_redirect( get_permalink( wc_get_page_id( 'myaccount' ) ) );
					exit;
				}

				static::setup_cart(
					$order,
					array(
						'bulk_order_id'	=> $order_id,
					)
				);

				if( WC()->cart->cart_contents_count != 0 ) {

					// Bulk order's ID in session so it can be re-used after payment
					WC()->session->set( 'order_awaiting_payment', $order_id );
					wc_clear_notices();
					wc_add_notice( __( 'Complete checkout to pay for your order. Beware: Moving away from this screen will clear your cart.', 'success' ) );
				}

				WC()->cart->shipping_total = 0;

				wp_safe_redirect( wc_get_checkout_url() );
				exit;
			}
		}
	}

	/**
	 * Removes all the bulk items from the cart if a bulk item is removed.
	 *
	 * @param string $cart_item_key The cart item key of the item removed from the cart
	 */
	public static function maybe_remove_items( $cart_item_key ) {

		if ( isset( WC()->cart->cart_contents[ $cart_item_key ][ static::$cart_item_key ]['bulk_order_id'] ) ) {

			$removed_item_count = 0;
			$order_id    		= WC()->cart->cart_contents[ $cart_item_key ][ static::$cart_item_key ]['bulk_order_id'];

			foreach ( WC()->cart->cart_contents as $key => $cart_item ) {

				if ( isset( $cart_item[ static::$cart_item_key ] ) && $order_id == $cart_item[ static::$cart_item_key ]['bulk_order_id'] ) {
					WC()->cart->removed_cart_contents[ $key ] = WC()->cart->cart_contents[ $key ];
					unset( WC()->cart->cart_contents[ $key ] );
					$removed_item_count++;
				}
			}

			//remove the bulk order flag
			unset( WC()->session->order_awaiting_payment );

			if ( $removed_item_count > 1 && 'woocommerce_before_cart_item_quantity_zero' == current_filter() ) {
				wc_add_notice( 'All bulk items have been removed from the cart.', 'notice' );
			}
		}
	}

	/**
	 * When restoring the cart from the session, if the cart item contains addons, as well as
	 * a bulk order, do not adjust the price because the original order's price will
	 * be used, and this includes the addons amounts.
	 */
	public static function product_addons_adjust_price( $adjust_price, $cart_item ) {

		if ( true === $adjust_price && isset( $cart_item[ static::$cart_item_key ] ) ) {
			$adjust_price = false;
		}

		return $adjust_price;
	}

	/**
	 * Returns address details from the bulk order if the checkout is for a bulk order
	 *
	 * @param string $value Default checkout field value
	 * @param string $key The checkout form field name/key
	 * 
	 * @return string $value Checkout field value
	 */
	public static function checkout_get_value( $value, $key ) {

		// Only hook in after WC()->checkout() has been initialised
		if ( static::cart_contains() && did_action( 'woocommerce_checkout_init' ) > 0 ) {

			// Guard against the fake WC_Checkout singleton
			remove_filter( 'woocommerce_checkout_get_value', array( __CLASS__, 'checkout_get_value' ), 10 );

			if ( is_callable( array( WC()->checkout(), 'get_checkout_fields' ) ) ) {
				$address_fields = array_merge( WC()->checkout()->get_checkout_fields( 'billing' ), WC()->checkout()->get_checkout_fields( 'shipping' ) );
			} else {
				$address_fields = array_merge( WC()->checkout()->checkout_fields['billing'], WC()->checkout()->checkout_fields['shipping'] );
			}

			add_filter( 'woocommerce_checkout_get_value', array( __CLASS__, 'checkout_get_value' ), 10, 2 );

			if ( array_key_exists( $key, $address_fields ) && false !== ( $item = static::cart_contains() ) ) {

				// Get the most specific order object
				$order = static::get_order( $item );

				if ( ( $order_value = static::get_objects_property( $order, $key ) ) ) {
					$value = $order_value;
				}
			}
		}

		return $value;
	}

	/**
	 * If the cart contains a bulk order that needs to ship to an address that is different
	 * to the order's billing address, tell the checkout to toggle the ship to a different address
	 * checkbox and make sure the shipping fields are displayed by default.
	 *
	 * @param bool $ship_to_different_address Whether the order will ship to a different address
	 * 
	 * @return bool $ship_to_different_address
	 */
	public static function maybe_check_ship_to_different_address( $ship_to_different_address ) {

		if ( !$ship_to_different_address && false !== ( $item = static::cart_contains() ) ) {

			$order = static::get_order( $item );

			$bulk_shipping_address = $order->get_address( 'shipping' );
			$bulk_billing_address  = $order->get_address( 'billing' );

			if ( isset( $bulk_billing_address['email'] ) ) {
				unset( $bulk_billing_address['email'] );
			}

			if ( isset( $bulk_billing_address['phone'] ) ) {
				unset( $bulk_billing_address['phone'] );
			}

			// If the order's addresses are different, we need to display the shipping fields otherwise the billing address will override it
			if ( $bulk_shipping_address != $bulk_billing_address ) {
				$ship_to_different_address = 1;
			}
		}

		return $ship_to_different_address;
	}

	/**
	 * Add custom line item meta to the cart item data so it's displayed in the cart.
	 *
	 * @param array $cart_item_data
	 * @param array $cart_item
	 */
	public static function display_line_item_data_in_cart( $cart_item_data, $cart_item ) {

		if ( !empty( $cart_item[ static::$cart_item_key ]['custom_line_item_meta'] ) ) {

			foreach ( $cart_item[ static::$cart_item_key ]['custom_line_item_meta'] as $item_meta_key => $value ) {

				$cart_item_data[] = array(
					'key'    => $item_meta_key,
					'value'  => $value,
					'hidden' => substr( $item_meta_key, 0, 1 ) === '_', // meta keys prefixed with an `_` are hidden by default
				);
			}
		}

		return $cart_item_data;
	}

	/**
	 * Add custom line item meta from the old line item into the new line item meta
	 *
	 * @param WC_Order_Item_Product
	 * @param string $cart_item_key
	 * @param array $cart_item_data
	 */
	public static function add_order_line_item_meta( $item, $cart_item_key, $cart_item_data ) {

		if ( !empty( $cart_item_data[ static::$cart_item_key ]['custom_line_item_meta'] ) ) {

			foreach ( $cart_item_data[ static::$cart_item_key ]['custom_line_item_meta'] as $meta_key => $value ) {
				$item->add_meta_data( $meta_key, $value );
			}
		}
	}

	/**
	 * For order items created as part of a bulk order, keep a record of the cart item key so that we can match it
	 * later in @see static::set_order_item_id() once the order item has been saved and has an ID.
	 */
	public static function add_line_item_meta( $order_item, $cart_item_key, $cart_item ) {

		if ( isset( $cart_item[ static::$cart_item_key ] ) ) {

			// Store the cart item key on the line item so that we can link it later on to the order line item ID
			$order_item->add_meta_data( '_cart_item_key_' . static::$cart_item_key, $cart_item_key );
		}
	}

	/**
	 * After order meta is saved, get the order line item ID for this bulk order and keep a record of it in
	 * the cart so we can update it later.
	 */
	public static function set_order_item_id( $order_id, $posted_checkout_data ) {

		$order = wc_get_order( $order_id );

		foreach ( $order->get_items( 'line_item' ) as $order_item_id => $order_item ) {

			$cart_item_key = $order_item->get_meta( '_cart_item_key_' . static::$cart_item_key );

			if ( ! empty( $cart_item_key ) ) {

				// Update the line_item_id to the new corresponding item_id
				static::set_cart_item_order_item_id( $cart_item_key, $order_item_id );
			}
		}
	}

	/**
	 * Do not display cart item key order item meta keys
	 */
	public static function hidden_order_itemmeta( $hidden_meta_keys ) {

		$hidden_meta_keys[] = '_cart_item_key_' . static::$cart_item_key;

		return $hidden_meta_keys;
	}

	/**
	 * Flag payment of bulk orders via an extra URL param.
	 */
	public static function get_checkout_payment_url( $pay_url, $order ) {

		if ( OMH_Order_Manager::is_bulk_order( $order ) ) {
			$pay_url = add_query_arg( array( static::$cart_item_key => 'true' ), $pay_url );
		}

		return $pay_url;
	}

	/**
	 * When a failed bulk order is being paid for via checkout, make sure WC_Checkout::create_order() preserves its
	 * status as 'failed' until it is paid. By default, it will always set it to 'pending', but we need it left as 'failed'
	 * so that we can correctly identify the status change
	 *
	 * @param string Default order status for orders paid for via checkout. Default 'pending'
	 */
	public static function maybe_preserve_order_status( $order_status ) {

		if ( null !== WC()->session && 'failed' !== $order_status ) {

			$order_id = absint( WC()->session->order_awaiting_payment );

			// Guard against infinite loops in WC where default order staus is set in WC_Abstract_Order::__construct()
			remove_filter( 'woocommerce_default_order_status', array( __CLASS__, __FUNCTION__ ), 10 );

			if ( $order_id > 0 && ( $order = wc_get_order( $order_id ) ) 
				&& OMH_Order_Manager::is_bulk_order( $order ) 
				&& $order->has_status( 'failed' ) 
			) {
				$order_status = 'failed';
			}

			add_filter( 'woocommerce_default_order_status', array( __CLASS__, __FUNCTION__ ) );
		}

		return $order_status;
	}

	/**
	 * Before allowing payment on an order awaiting payment via checkout, WC >= 2.6 validates
	 * order items haven't changed by checking for a cart hash on the order, so we need to set
	 * that here. 
	 * 
	 * @see WC_Checkout::create_order()
	 */
	protected static function set_cart_hash( $order_id ) {

		$order = wc_get_order( $order_id );

		// Use cart hash generator introduced in WooCommerce 3.6
		if ( is_callable( array( WC()->cart, 'get_cart_hash' ) ) ) {
			$cart_hash = WC()->cart->get_cart_hash();
		} else {
			$cart_hash = md5( json_encode( wc_clean( WC()->cart->get_cart_for_session() ) ) . WC()->cart->total );
		}

		static::set_objects_property( $order, 'cart_hash', $cart_hash );
	}

	/**
	 * Right before WC processes a bulk cart through the checkout, set the cart hash.
	 * This ensures legitimate changes to taxes and shipping methods don't cause a new order to be created.
	 *
	 * @param Mixed | An order generated by third party plugins
	 * @return Mixed | The unchanged order param
	 */
	public static function update_cart_hash( $order ) {
		
		if ( $item = static::cart_contains() ) {

			if ( isset( $item[ static::$cart_item_key ]['bulk_order_id'] ) ) {
				$order_id = $item[ static::$cart_item_key ]['bulk_order_id'];
			} else {
				$order_id = '';
			}
			
			if ( $order_id ) {
				static::set_cart_hash( $order_id );
			}
		}

		return $order;
	}

	/**
	 * 
	 * 
	 * @tag 	Bulk Product
	 * 
	 * @param 	OMH_Order 	$order
	 * @param 	array 		$cart_item_data 
	 * 
	 * @return 	void
	 */
	public static function setup_cart( $order, $cart_item_data ) {

		WC()->cart->empty_cart( true );
		$success = true;

		foreach ( $order->get_items() as $item_id => $line_item ) {

			$variations = array();
			$item_data  = array();
			$custom_line_item_meta   = array();
			$reserved_item_meta_keys = array(
				'_item_meta',
				'_item_meta_array',
				'_qty',
				'_tax_class',
				'_product_id',
				'_variation_id',
				'_line_subtotal',
				'_line_total',
				'_line_tax',
				'_line_tax_data',
				'_line_subtotal_tax',
				'_cart_item_key_' . static::$cart_item_key,
				'Backordered',
			);

			$product_id = $line_item->get_product_id();
			$quantity = $line_item->get_quantity();
			$variation_id = $line_item->get_variation_id();
			$item_name = $line_item->get_name();

			// foreach ( $line_item->get_meta_data() as $meta ) {
			// 	if ( taxonomy_is_product_attribute( $meta->key ) ) {
			// 		$variations[ $meta->key ] = $meta->value;
			// 	} elseif ( meta_is_product_attribute( $meta->key, $meta->value, $product_id ) ) {
			// 		$variations[ $meta->key ] = $meta->value;
			// 	} elseif ( ! in_array( $meta->key, $reserved_item_meta_keys ) ) {
			// 		$custom_line_item_meta[ $meta->key ] = $meta->value;
			// 	}
			// }

			$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', $product_id );
			$product = wc_get_product( $product_id );

			// The notice displayed when a product has been deleted and the customer attempts to make a bulk order payment for a failed order
			$product_deleted_error_message = 'The %s product has been deleted, please contact us for assistance.';

			// Display error message for deleted products
			if ( false === $product ) {

				wc_add_notice( sprintf( $product_deleted_error_message, $item_name ), 'error' );

			// Make sure we don't actually need the variation ID (if the product was a variation, it will have a variation ID)
			} elseif ( $product->is_type( array( 'variable' ) ) && ! empty( $variation_id ) ) {

				$variation = wc_get_product( $variation_id );

				// Display error message for deleted product variations
				if ( false === $variation ) {
					wc_add_notice( sprintf( $product_deleted_error_message, $item_name ), 'error' );
				}
			}

			$cart_item_data['line_item_id'] = $item_id;
			$cart_item_data['custom_line_item_meta'] = $custom_line_item_meta;

			$item_data = apply_filters( 'woocommerce_order_again_cart_item_data', array( static::$cart_item_key => $cart_item_data ), $line_item, $order );

			if ( !apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id, $variations, $item_data ) ) {
				continue;
			}

			$cart_item_key = WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variations, $item_data );
			$success       = $success && (bool) $cart_item_key;
		}

		// If a product failed to be added to the cart prevent partially paying for the order by removing all cart items.
		if ( !$success && $order->has_bulk_product() ) {

			$order->delete();
			
			wc_add_notice( sprintf( 'Bulk order #%s has not been added to the cart.', $order->get_order_number() ) , 'error' );
			WC()->cart->empty_cart( true );
		}

		WC()->cart->shipping_total = 0;

		do_action( 'woocommerce_setup_cart_for_' . static::$cart_item_key, $order, $cart_item_data );
	}

	/**
	 * Checks the cart to see if it contains a bulk item.
	 */
	public static function cart_contains() {

		$contains_bulk = false;
	
		if ( ! empty( WC()->cart->cart_contents ) ) {
			foreach ( WC()->cart->cart_contents as $cart_item ) {
				if ( isset( $cart_item[ static::$cart_item_key ] ) ) {
					$contains_bulk = $cart_item;
					break;
				}
			}
		}
	
		return apply_filters( 'cart_contains_bulk', $contains_bulk );
	}

	/**
	 * Get the order object used to construct the bulk cart
	 */
	protected static function get_order( $cart_item = '' ) {

		$order = false;

		if ( empty( $cart_item ) ) {
			$cart_item = static::cart_contains();
		}

		if ( false !== $cart_item  && isset( $cart_item[ static::$cart_item_key ] ) ) {
			$order = wc_get_order( $cart_item[ static::$cart_item_key ]['bulk_order_id'] );
		}

		return $order;
	}

	/**
	 * After updating bulk order line items, update the values stored in cart item data
	 * which would now reference old line item IDs
	 *
	 * @param string $cart_item_key
	 * @param int $order_item_id
	 */
	protected static function set_cart_item_order_item_id( $cart_item_key, $order_item_id ) {
		WC()->cart->cart_contents[ $cart_item_key ][ static::$cart_item_key ]['line_item_id'] = $order_item_id;
	}

	/**
	 * Trigger a special hook for payments on a completed bulk order
	 */
	public static function trigger_bulk_payment_complete( $order_id ) {
		if ( OMH_Order_Manager::is_bulk_order( $order_id ) ) {
			do_action( 'woocommerce_bulk_order_payment_complete', $order_id );
		}
	}

	/**
	 * Add a prefix to a string if it doesn't already have it
	 * 
	 * @tag WCS
	 *
	 * @param string
	 * @param string
	 *
	 * @return string
	 */
	public static function maybe_prefix_key( $key, $prefix = '_' ) {
		return ( substr( $key, 0, strlen( $prefix ) ) != $prefix ) ? $prefix . $key : $key;
	}

	/**
	 * @tag WCS
	 */
	public static function get_objects_property( $object, $property, $single = 'single', $default = null ) {
		$value = ! is_null( $default ) ? $default : ( ( 'single' === $single ) ? null : array() );
	
		if ( ! is_object( $object ) ) {
			return $value;
		}
	
		$prefixed_key          = static::maybe_prefix_key( $property );
		$property_function_map = array(
			'order_version'  => 'version',
			'order_currency' => 'currency',
			'order_date'     => 'date_created',
			'date'           => 'date_created',
			'cart_discount'  => 'total_discount',
		);
	
		if ( isset( $property_function_map[ $property ] ) ) {
			$property = $property_function_map[ $property ];
		}
	
		switch ( $property ) {
			case 'post' :

				// In order to keep backwards compatibility it's required to use the parent data for variations.
				if ( method_exists( $object, 'is_type' ) && $object->is_type( 'variation' ) ) {
					$value = get_post( static::get_objects_property( $object, 'parent_id' ) );
				} else {
					$value = get_post( static::get_objects_property( $object, 'id' ) );
				}
				break;
			case 'post_status' :

				$value = static::get_objects_property( $object, 'post' )->post_status;
				break;
			case 'variation_data' :

				$value = wc_get_product_variation_attributes( static::get_objects_property( $object, 'id' ) );
				break;
			default :
				$function_name = 'get_' . $property;
	
				if ( is_callable( array( $object, $function_name ) ) ) {
					$value = $object->$function_name();
				} else {
					// If we don't have a method for this specific property, but we are using WC 3.0, it may be set as meta data on the object so check if we can use that.
					if ( $object->meta_exists( $prefixed_key ) ) {
						if ( 'single' === $single ) {
							$value = $object->get_meta( $prefixed_key, true );
						} else {
							// WC_Data::get_meta() returns an array of stdClass objects with id, key & value properties when meta is available.
							$value = wp_list_pluck( $object->get_meta( $prefixed_key, false ), 'value' );
						}
					} elseif ( 'single' === $single && isset( $object->$property ) ) {
						$value = $object->$property;
					} elseif ( strtolower( $property ) !== 'id' && metadata_exists( 'post', static::get_objects_property( $object, 'id' ), $prefixed_key ) ) {
						// If we couldn't find a property or function, fallback to using post meta
						if ( 'single' === $single ) {
							$value = get_post_meta( static::get_objects_property( $object, 'id' ), $prefixed_key, true );
						} else {
							// Get all the meta values.
							$value = get_post_meta( static::get_objects_property( $object, 'id' ), $prefixed_key, false );
						}
					}
				}
				break;
		}
	
		return $value;
	}

	/**
	 * @tag WCS
	 */
	public static function set_objects_property( &$object, $key, $value, $save = 'save', $meta_id = '', $prefix_meta_key = 'prefix_meta_key' ) {

		$prefixed_key = static::maybe_prefix_key( $key );

		// WC will automatically set/update these keys when a shipping/billing address attribute changes so we can ignore these keys
		if ( in_array( $prefixed_key, array( '_shipping_address_index', '_billing_address_index' ) ) ) {
			return;
		}

		// Special cases where properties with setters which don't map nicely to their function names
		$meta_setters_map = array(
			'_cart_discount'         => 'set_discount_total',
			'_cart_discount_tax'     => 'set_discount_tax',
			'_customer_user'         => 'set_customer_id',
			'_order_tax'             => 'set_cart_tax',
			'_order_shipping'        => 'set_shipping_total',
			'_sale_price_dates_from' => 'set_date_on_sale_from',
			'_sale_price_dates_to'   => 'set_date_on_sale_to',
		);

		// If we have a 3.0 object with a predefined setter function, use it
		if ( isset( $meta_setters_map[ $prefixed_key ] ) && is_callable( array( $object, $meta_setters_map[ $prefixed_key ] ) ) ) {
			$function = $meta_setters_map[ $prefixed_key ];
			$object->$function( $value );

		// If we have a 3.0 object, use the setter if available.
		} elseif ( is_callable( array( $object, 'set' . $prefixed_key ) ) ) {

			// Prices include tax is stored as a boolean in props but saved in the database as a string yes/no, so we need to normalise it here to make sure if we have a string that it's converted to a boolean before being set
			if ( '_prices_include_tax' === $prefixed_key && ! is_bool( $value ) ) {
				$value = 'yes' === $value;
			}

			$object->{ "set$prefixed_key" }( $value );

		// If there is a setter without the order prefix (eg set_order_total -> set_total)
		} elseif ( is_callable( array( $object, 'set' . str_replace( '_order', '', $prefixed_key ) ) ) ) {
			$function_name = 'set' . str_replace( '_order', '', $prefixed_key );
			$object->$function_name( $value );

		// If there is no setter, treat as meta within the 3.0.x object.
		} elseif ( is_callable( array( $object, 'update_meta_data' ) ) ) {
			$meta_key = ( 'prefix_meta_key' === $prefix_meta_key ) ? $prefixed_key : $key;
			$object->update_meta_data( $meta_key, $value, $meta_id );

		} elseif ( 'name' === $key ) {
			$object->post->post_title = $value;
		} else {
			$object->$key = $value;
		}

		// Save the data
		if ( 'save' === $save ) {
			if ( is_callable( array( $object, 'save' ) ) ) {
				$object->save();
			} elseif ( 'date_created' == $key ) {
				wp_update_post( array( 'ID' => static::get_objects_property( $object, 'id' ), 'post_date' => get_date_from_gmt( $value ), 'post_date_gmt' => $value ) );
			} elseif ( 'name' === $key ) {
				wp_update_post( array( 'ID' => static::get_objects_property( $object, 'id' ), 'post_title' => $value ) );
			} else {
				$meta_key = ( 'prefix_meta_key' === $prefix_meta_key ) ? $prefixed_key : $key;

				if ( ! empty( $meta_id ) ) {
					update_metadata_by_mid( 'post', $meta_id, $value, $meta_key );
				} else {
					update_post_meta( static::get_objects_property( $object, 'id' ), $meta_key, $value );
				}
			}
		}
	}
}
OMH_Cart_Bulk::init();
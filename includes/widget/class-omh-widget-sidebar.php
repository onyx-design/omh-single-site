<?php
defined( 'ABSPATH' ) || exit;

class OMH_Widget_Sidebar extends WP_Widget {

	function __construct() {

		$widget_ops = array(
			'classname'		=> 'omh_sidebar_widget',
			'description'	=> 'OMH Sidebar Widget for Chapters and Organizations.',
		);

		parent::__construct( 'omh_sidebar_widget', 'OMH Sidebar Widget', $widget_ops );
	}

	public function widget( $args, $instance ) {

		$term = get_queried_object();

		$taxonomies = array( 'chapters', 'organizations', 'colleges' );

		if( ( 'WP_Term' == get_class( $term ) ) 
			&& in_array( $term->taxonomy, $taxonomies )
		) {

			switch( $term->taxonomy ) {
				case 'chapters':
					$model = OMH()->chapter_factory->get_by_term_slug( $term->slug );
					break;
				case 'organizations':
					$model = OMH()->organization_factory->get_by_term_slug( $term->slug );
					break;
				case 'colleges':
					$model = OMH()->college_factory->get_by_term_slug( $term->slug );
					break;
			}
			$publicly_available_categories = $model->get_publicly_available_categories();
			
		}
		else if(is_shop() || is_product_category() ){
			// Set Publicly available categories
			$publicly_available_categories = OMH_Model::get_all_public_categories();
		}

		if( isset( $publicly_available_categories ) ) {
			include OMH_TEMPLATE_PATH . 'widgets/widget-sidebar.php';
		}
	}
}
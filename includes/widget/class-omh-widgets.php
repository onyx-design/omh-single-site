<?php
defined( 'ABSPATH' ) ?? exit;

class OMH_Widget {

	public function __construct() {

		add_action( 'widgets_init', array( $this, 'register_sidebars' ) );
		add_action( 'widgets_init', array( $this, 'register_widgets' ) );
	}

	public function register_sidebars() {

		// register_sidebar(
		// 	array(
		// 		'name'          => 'Chapter Sidebar',
		// 		'id'            => 'chapter-sidebar',
		// 		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		// 		'after_widget'  => '</div>',
		// 		'before_title'  => '<h4>',
		// 		'after_title'   => '</h4>',
		// 	)
		// );
	}

	public function register_widgets() {

		register_widget( 'OMH_Widget_Sidebar' );
	}
}
new OMH_Widget;
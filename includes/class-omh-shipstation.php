<?php
// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

class OMH_Shipstation {

	public function __construct() {

		add_action( 'woocommerce_shipstation_shipnotify', array( $this, 'shipping_tracking' ), 10, 2 );

		
		add_filter('woocommerce_shipstation_export_custom_field_2', array($this, 'export_referral_code'));

		// add_filter( 'woocommerce_email_order_meta_fields', array( $this, 'shipping_tracking_email_meta' ), 10, 3 );

		// dev:note These functions are no longer required as the garments are now WC Attributes that should be picked up automatically
		// add_filter( 'woocommerce_order_item_get_formatted_meta_data', array( $this, 'add_garment_description' ) );
		// add_filter( 'woocommerce_order_item_display_meta_key', array( $this, 'add_garment_display_key' ) );
	}

	/**
	 * Action to update the Shipstation tracking information
	 * 
	 * @param 	WC_Order 	$order
	 * @param 	array 		$order_details
	 */
	public function shipping_tracking( $order, $order_details ) {

		if( $order = wc_get_order( $order ) ) {

			$tracking_number = $order_details['tracking_number'] ?? '';
			$tracking_carrier = $order_details['carrier'] ?? '';

			$order->set_tracking( $tracking_number, $tracking_carrier );

			$order->save();
		}
	}

	public function export_referral_code(){
		return '_referral';
	}

	public function shipping_tracking_email_meta( $order_meta, $sent_to_admin, $order ) {

		$order_meta[] = array(
			'label'		=> 'Tracking Details',
			'value'		=> $order->get_tracking_url()
		);

		return $order_meta;
	}

	/**
	 * dev:note This function is no longer required as Garments are WC Attributes
	 * 
	 * @param 	array 					$formatted_meta
	 * @param 	WC_Order_Item_product 	$order_item
	 * 
	 * @return 	array
	 */
	public function add_garment_description( $formatted_meta, $order_item ) {

		// Only run this filter when Shipstation is being called from the WooCommerce API
		if( !isset( $_GET['wc-api'] ) || $_GET['wc-api'] != 'wc_shipstation' ) {
			return $formatted_meta;
		}

		if( $order_item instanceof WC_Order_Item_Product ) {

			$garment_description_exists = false;
			foreach( $formatted_meta as $meta ) {

				if( $meta->key === 'garment_description' ) {
					$garment_description_exists = true;
				}
			}

			if( !$garment_description_exists ) {

				$parent_product = wc_get_product( $order_item->get_product()->get_parent_id() );

				$stdObj = new stdClass();

				$stdObj->key = 'garment_description';
				$stdObj->value = "{$parent_product->get_meta('_mh_manufacturer')} - {$parent_product->get_meta('_mh_blank_style_number')} - {$parent_product->get_meta('_mh_blank_color')}";
				$stdObj->display_key = 'Garment';
				$stdObj->display_value = "{$parent_product->get_meta('_mh_manufacturer')} - {$parent_product->get_meta('_mh_blank_style_number')} - {$parent_product->get_meta('_mh_blank_color')}";

				$formatted_meta[] = $stdObj;
			}
		}

		return $formatted_meta;
	}

	/**
	 * dev:note This function is no longer required as Garments are WC Attributes
	 * 
	 * @param 	string 					$display_key
	 * @param 	array 					$meta
	 * @param 	WC_Order_Item_Product 	$order_item
	 * 
	 * @return 	string
	 */
	public function add_garment_display_key( $display_key, $meta, $order_item ) {

		// Only run this filter when Shipstation is being called from the WooCommerce API
		if( !isset( $_GET['wc-api'] ) || $_GET['wc-api'] != 'wc_shipstation' ) {
			return $display_key;
		}

		if( $order_item instanceof WC_Order_Item_Product ) {

			if( 'garment_description' === $display_key ) {
				$display_key = 'Garment';
			}
		}

		return $display_key;
	}
}
return new OMH_Shipstation;
<?php
/**
 * Plugin Name:     ONYX Merch House - Single Site
 * Description:     ONYX Merch House Custom Functionality for Single Site
 * Author:          ONYX Design
 * Author URI:      https://onyxdesign.net
 * Text Domain:     onyx-merch-house
 * Domain Path:     /languages
 * Version:         1.4.14
 *
 * @package         Onyx_Merch_House
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit; 

define( 'OMH_DEBUG', false );

if( OMH_DEBUG ) {
	// Define start time for debugging & performance measurement
	define( 'OMH_START_EXEC', ( microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"] ) );	
}

/**
 * Main OMH Class
 *
 * @class Onyx_Merch_House
 */
final class Onyx_Merch_House {
	/**
	 * @var string
	 */
	public $version = '1.4.14';

	/**
	 * @var Onyx_Merch_House The single instance of the class
	 */
	protected static $_instance = null;

	public $session = null;

	public $task_manager = null;
	/**
	 * Factories
	 **/

	public $credit_factory = null;

	public $chapter_factory = null;

	public $college_factory = null;

	public $organization_factory = null;

	public $garment_product_factory = null;

	public $garment_style_factory = null;

	public $garment_brand_factory = null;

	public $garment_color_factory = null;

	public $temp_upload_factory = null;

	public $follower_factory = null;

	/**
	 * Main Onyx_Merch_House Instance
	 *
	 * Ensures only one instance of Onyx_Merch_House is / can be loaded
	 *
	 * @static
	 * @see OMH()
	 * @return Onyx_Merch_House - Main instance
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	/**
	 * Onyx_Merch_House Constructor
	 */
	public function __construct() {
		$this->define_constants();
		$this->includes();
		$this->init_hooks();
	}

	/**
	 * Hook into actions and filters
	 */
	private function init_hooks() {
		register_activation_hook( OMH_PLUGIN_FILE, array( 'OMH_Install', 'install' ) );
		add_action( 'init', array( $this, 'init' ), 0 );
	}

	/**
	 * Define OMH Constants
	 */
	private function define_constants() {
		define( 'OMH_SLUG', 'omh' );
		define( 'OMH_PLUGIN_FILE', __FILE__ );
		define( 'OMH_PATH', realpath( dirname(__FILE__) ) );
		define( 'OMH_URL', plugins_url() . '/' . basename(dirname(__FILE__)) . '/' );
		define( 'OMH_TEXT_DOMAIN', 'onyx-merch-house' );
		define( 'OMH_TEMPLATE_PATH', OMH_PATH . '/templates/' );

		define( 'OMH_CONTENT_DIR', WP_CONTENT_DIR . '/omh' );
		define( 'OMH_CONTENT_URL', WP_CONTENT_URL . '/omh' );

		define( 'OMH_TEMP_UPLOAD_DIR', OMH_CONTENT_DIR . '/temp-uploads' );
		define( 'OMH_TEMP_UPLOAD_URL', OMH_CONTENT_URL . '/temp-uploads' );
		
		define( 'OMH_DEFAULT_CHAPTER_EARNINGS_PERCENT', 5 );
	}

	/**
	 * Include required core files used in admin
	 */
	public function includes() {

		/**
		 * Autoloader
		 */
		include_once( OMH_PATH . '/includes/class-omh-autoloader.php' );

		/**
		 * Core
		 */
		include_once( OMH_PATH . '/includes/class-omh-permalinks.php' );
		include_once( OMH_PATH . '/includes/omh-functions.php' );
		include_once( OMH_PATH . '/includes/class-omh-install.php' );
		include_once( OMH_PATH . '/includes/class-omh-taxonomies.php' );
		include_once( OMH_PATH . '/includes/class-omh-shipstation.php' );
		include_once( OMH_PATH . '/includes/customizer/class-omh-customizer.php' );
		include_once( OMH_PATH . '/includes/product/class-omh-product-manager.php' );
		include_once( OMH_PATH . '/includes/order/class-omh-order-manager.php' );
		include_once( OMH_PATH . '/includes/class-omh-cart-bulk.php' );
		include_once( OMH_PATH . '/includes/job/class-omh-task-manager.php' );

		/**
		 * Widgets & Shortcodes
		 */
		include_once( OMH_PATH . '/includes/shortcode/class-omh-shortcodes.php' );
		include_once( OMH_PATH . '/includes/widget/class-omh-widgets.php' );

		// If not on live, make sure error_reporting and logging are on
		if( !$this->is_live_site() && OMH_DEBUG ) {

			omh_debug()->set_active();
		}
	}

	public function admin_includes() {

		include_once( OMH_PATH . '/includes/admin/class-omh-admin.php' );
		include_once( OMH_PATH . '/includes/admin/class-omh-admin-assets.php' );
		include_once( OMH_PATH . '/includes/admin/class-omh-admin-page.php' );
		include_once( OMH_PATH . '/includes/admin/class-omh-admin-template-loader.php' );
		include_once( OMH_PATH . '/includes/admin/class-omh-admin-product-page.php' );
	}

	public function frontend_includes() {

		include_once( OMH_PATH . '/includes/frontend/class-omh-frontend.php' );
		include_once( OMH_PATH . '/includes/frontend/class-omh-frontend-assets.php' );
		include_once( OMH_PATH . '/includes/frontend/class-omh-frontend-template-loader.php' );
	}

	/**
	 * Init OMH when Wordpress Initialized
	 */
	public function init() {

		// Check OMH maintenance mode
		if( !empty( get_option('omh_maintenance_mode_enabled') )
			&& !is_super_admin() 
			&& (get_current_user_id() !== 4800 )
			&& !(!is_user_logged_in() && ('wp-login.php' == $GLOBALS['pagenow']))
		) {
			omh_debug('omh_maintenance_enabled');
			if( !is_front_page() ) {
				omh_debug( '!is_front_page()' );
				add_action( 'template_include', function() {return OMH_TEMPLATE_PATH . 'pages/template-omh-maintenance.php';}, 9999 );
			} else {
				omh_debug('redirect home_url');	
				wp_redirect(home_url());
				die();
			}
		}
			

		if( !$this->is_live_site() ) {
			add_filter( 'https_local_ssl_verify', '__return_false' );
			add_filter( 'https_ssl_verify', '__return_false' );
		}

		/*
		 * Factories
		 */

		$this->credit_factory = new OMH_Factory_Credit();
		$this->chapter_factory = new OMH_Factory_Chapter();
		$this->organization_factory = new OMH_Factory_Organization();
		$this->college_factory = new OMH_Factory_College();

		$this->garment_product_factory = new OMH_Factory_Garment_Product();
		$this->garment_style_factory = new OMH_Factory_Garment_Style();
		$this->garment_brand_factory = new OMH_Factory_Garment_Brand();
		$this->garment_color_factory = new OMH_Factory_Garment_Color();
		
		$this->temp_upload_factory = new OMH_Factory_Temporary_File_Upload();
		
		$this->follower_factory = new OMH_Factory_Follower();

		/**
		 * Check for request to download OMH Followers export CSV
		 */
		if( isset( $_GET['export_followers'] ) 
			&& 'true' == $_GET['export_followers']
		) {

			$omh_user = wp_get_current_user();

			if( $omh_user && $omh_user->has_role('administrator') ) {

				ini_set("max_execution_time", 600 );
			
				OMH_Factory_Follower::generate_followers_export_csv();
				
				$export_file = OMH_Factory_Follower::get_followers_export_csv_path();
				header('Content-Type: text/csv');
				header("Content-disposition: attachment; filename=\"" . basename($export_file) . "\""); 
				header("Pragma: no-cache");
				header("Expires: 0");
				readfile( $export_file ); 
				exit();
			}
		}

		/*
		 * Classes
		 */

		$this->session = OMH_Session::instance();


		/**
		 * Initialize Task Manager
		 */
		$this->task_manager = new OMH_Task_Manager();

		/*
		 * Actions and Filters
		 */

		// Email Actions and Filters
		new OMH_Emails();

		if ( $this->is_request( 'admin' ) && !$this->is_request( 'ajax' ) ) {
			// JUST BECAUSE is_admin() IS TRUE DOESN'T MEAN WE ARE IN THE ADMIN, is_admin() RETURNS TRUE FOR ALL AJAX REQUESTS ALSO

			$this->admin_includes();
		}

		// Classes & actions loaded for the frontend
		if ( $this->is_request( 'frontend' ) ) {
			$this->frontend_includes();
		}

		if ( $this->is_request( 'ajax' ) ) {
			OMH_AJAX::init();
		}
	}

	/**
	 * Clear Page Cache
	 * 
	 * @param   int|null    $post_id
	 */
	public function clear_page_cache( $post_id = null ) {

		// Clear WordPress Object Cache
		if( $post_id ) {
			clean_post_cache( $post_id );
		}
		clean_post_cache( wc_get_page_id( 'shop' ) );

		wc_delete_product_transients( $post_id );

		// Clear WPEngine Varnish Cache
		if( class_exists( 'WpeCommon' ) ) {

			if( method_exists( 'WpeCommon', 'purge_varnish_cache' ) ) {

				if( $post_id ) {
					WpeCommon::purge_varnish_cache( $post_id, true );
				}
				WpeCommon::purge_varnish_cache( wc_get_page_id( 'shop' ), true );
			}

			if( method_exists( 'WpeCommon', 'clear_maxcdn_cache' ) ) {
				WpeCommon::clear_maxcdn_cache();
			}

			if( method_exists( 'WpeCommon', 'purge_memcached' ) ) {
				WpeCommon::purge_memcached();
			}
		}
	}

	/**
	 * Check that password has 6 characters, 1 letter, and 1 number
	 * 
	 * @param   string  $password
	 * 
	 * @return  bool
	 */
	public function is_strong_password( $password ) {

        $contains_letters = preg_match('/[A-Za-z]/', $password );
        $contains_numbers = preg_match( '/[0-9]/', $password );
        $contains_six_characters = 6 <= strlen( $password );

        return $contains_letters && $contains_numbers && $contains_six_characters;
	}

	public function maybe_define_constant( $name, $value ) {
		if( !defined( $name ) ) {
			define( $name, $value );
		}
	}

	public function page_templates() {
		return array_filter( array_map( function( $template_path ) {

			if( in_array( $template_path, array( '.', '..' ) ) ) {
				return '';
			}
			
			return 'omh-' . wp_basename( $template_path );
		}, scandir( OMH_TEMPLATE_PATH . 'pages/' ) ) );
	}

	/**
	 * Return whether the current page is an MH Admin screen
	 */
	public function is_mh_admin() {
		global $post;

		if( $post->post_parent ) {

			$parent = get_post( $post->post_parent );

			return ( $parent->post_name === 'mh-admin' );
		}

		return false;
	}

	/**
	 * Return whether the current page is a Dashboard screen
	 */
	public function is_dashboard()
	{
		global $post;

		// Empty Post ID is coming in as string with value ' ' 
		if (!$post || !$post->ID || (strcmp($post->ID, ' ') === 0)) {
			return false;
		}

		$is_dashboard = false;

		if ($post->post_name === 'dashboard') {
			$is_dashboard = true;
		} elseif (0 === strpos(get_page_template_slug(), 'omh-template-dashboard')) {
			$is_dashboard = true;
		} elseif ($post->post_parent) {

			$parent = get_post($post->post_parent);

			$is_dashboard = $parent->post_name === 'dashboard';
		}

		return (
			($post->post_type === 'page')
			&& $is_dashboard);
	}

	/**
	 * Returns the Dashboard URL with trailing slash
	 * 
	 * dev:improve dev:todo Return the current chapter's links
	 * 
	 * @return string
	 */
	public function dashboard_url( $path = null, $chapter_id = null ) {
		if($chapter_id) {
			$chapter = OMH()->chapter_factory->read( $chapter_id );

			$url = site_url( "/chapters/{$chapter->get_url_structure()}/dashboard/" );
		} elseif( OMH()->session && $chapter = OMH()->session->get_chapter() ) {

			$url = site_url( "/chapters/{$chapter->get_url_structure()}/dashboard/" );
		} else {

			$url = site_url( '/dashboard/' );
		}

		if( $path && ( $path !== 'dashboard' ) ) {
			$url .= $path;
		}

		return $url;
	}

	public function mh_admin_url( $path = null ) {

		$url = site_url( '/mh-admin/' );

		if( $path ) {
			$url .= $path;
		}

		return $url;
	}

	/**
	 * Returns the Dashboard Template path
	 * 
	 * @param 	string 	$template
	 * 
	 * @return 	string
	 */
	public function dashboard_template_path( $template = '' ) {
		return OMH_TEMPLATE_PATH . 'dashboard/' . $template . '.php';
	}

	/**
	 * Checks whether a Dashboard Template exists within the plugin's template directory
	 * 
	 * @param 	string 	$template
	 * 
	 * @return 	bool
	 */
	public function dashboard_template_exists( $template = '' ) {
		return file_exists( OMH_TEMPLATE_PATH . 'dashboard/' . $template . '.php' );
	}

	/**
     *  Verify OMH Directories
     */
	public function verify_omh_dir( $directory ) {

	    if( !is_dir( untrailingslashit( $directory ) ) 
	        && !mkdir( untrailingslashit( $directory ), 0777, true )
	    ) {
	        return false;
	    }
	    return true;
	}

	/**
	 * Get the plugin url.
	 *
	 * @return string
	 */
	public function plugin_url() {
		return untrailingslashit( plugins_url( '/', OMH_PLUGIN_FILE ) );
	}

	/**
	 * Get the plugin path.
	 *
	 * @return string
	 */
	public function plugin_path() {
		return untrailingslashit( plugin_dir_path( OMH_PLUGIN_FILE ) );
	}

	/**
	 * Get the template path.
	 *
	 * @return string
	 */
	public function template_path() {
		return apply_filters( 'omh_template_path', 'omh/' );
	}

	/**
	 * Check if current request matches a supplied type
	 *
	 * @param string $type admin, ajax, cron or admin
	 * @return bool
	 */
	public function is_request( $type ) {
		switch ( $type ) {
			case 'admin' :
				return is_admin();
			case 'ajax' :
				return defined( 'DOING_AJAX' ) && DOING_AJAX;
			case 'cron' :
				return defined( 'DOING_CRON' ) && DOING_CRON;
			case 'frontend' :
				return ( ! is_admin() || defined( 'DOING_AJAX' ) ) && ! defined( 'DOING_CRON' );
			default :
				return false;
		}
	}

	/**
	 * Determine if current environment is live site
	 * 
	 * @return bool
	 **/
	public static function is_live_site() {
		return false !== strpos( get_site_url(), 'merch.house' );
	}
}

/**
 * Returns the main instance of OMH to prevent the need to use globals.
 *
 * @return Onyx_Merch_House
 */
function OMH() {
	return Onyx_Merch_House::instance();
}

// Make it happen
OMH();

if( OMH_DEBUG ) {
	$omh_load_time = (float) ox_exec_time() - OMH_START_EXEC;	
}

const gulp = require('gulp');
const clean = require('gulp-clean');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const webpack = require('webpack');
const gulpWebpack = require('webpack-stream');

// Config
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

// Webpack Config
const webpackOptions = require('./webpack.config.js');

// Sass Config
const sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed'
};

// Autoprefixer config
const autoprefixerOptions = {
  browsers: ['last 2 versions', '> 0.5%', 'ie > 8'],
  remove: false
};

// Command Line config
const configOption = process.argv[2];
const environments = {
    dev: '--dev',
    prod: '--prod'
}

// Tasks
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

// Wipe the old dist folder
gulp.task('clean', () => {
    return gulp
        .src('dist')
        .pipe( clean() )
});

// Task: .scss --> .css
gulp.task('parseScss', () => {
	return gulp
		.src('assets/css/*.scss')
		.pipe(sourcemaps.init())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./dist/css'))
});


// Primary Build
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

gulp.task('default', ['clean', 'parseScss'], () => {
    // Dependency Tasks: 
        // 1. wipe the dist folder
        // 2. perform the first sass build

    // If running BUILD: turn off webpack watch
    if ( configOption === environments.prod ) {
        webpackOptions.watch = false;
    }
    // If running WATCH (or just gulp): watch .scss files for compilation
    else {
        gulp.watch('assets/css/**/*.scss',['parseScss']);
    }

    // Kick off Webpack
    return gulp.src('assets/js/omh.js')
        .pipe(gulpWebpack(
            webpackOptions,
            webpack,
            (err, stats) => {
                // Log errors
                if (err) console.log('ERROR:', err);            
                // Log compilation time
                console.log(`\nFinished building in ${stats.endTime - stats.startTime} ms`)
            }
        ))
        .pipe(gulp.dest('dist/js/'))

});

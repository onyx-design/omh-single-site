/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 12);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var $ = jQuery;

function OMHFormField(input, parent, options) {

	this.initialized = false;

	this.settings = _.defaultsDeep(options, {
		origValue: null,
		presetValue: null,
		validationClasses: true
	});

	// parent is either the OMHForm object or extended OMHFormFieldAdvanced field
	this.parent = _.defaultTo(parent, null);

	// Optional Hook for DOM preparation
	_.invoke(this, 'domInit');

	this.$input = _.defaultTo(this.$input, $(input));
	this.inputElem = _.defaultTo(this.inputElem, _.nth(this.$input, 0));
	_.set(this, 'inputElem.omhFormField', this); // Make this object accessible via the DOM

	// Determine omh-field-wrap element (if any)
	this.$wrap = _.defaultTo(this.$wrap, this.$input.closest('.omh-field-wrap'));
	this.wrapElem = _.defaultTo(this.wrapElem, _.nth(this.$wrap, 0));
	_.set(this, 'wrapElem.omhFormField', this); // Make this object accessible via the DOM

	// Set up custom jQuery set of items that will receive field state classes
	this.$fieldStateElems = _.defaultTo(this.$fieldStateElems, $([]));
	this.$fieldStateElems = this.$fieldStateElems.add(this.$wrap).add(this.$input).add('[data-omh-field-state="' + this.getName() + '"]');

	this.$fieldLabel = _.defaultTo(this.$fieldLabel, this.$wrap.children('label').first());
	this.$invalidFeedback = _.defaultTo(this.$invalidFeedback, this.$wrap.find('.invalid-feedback').eq(0));

	this.$fieldResetBtns = _.defaultTo(this.$fieldResetBtns, $('.btn.input-reset-btn[for="' + this.getName() + '"]'));
	this.$fieldFocusBtns = _.defaultTo(this.$fieldFocusBtns, $('.btn.input-focus-btn[for="' + this.getName() + '"]'));

	// Hook for custom code to modify options //dev:improve
	this.$input.trigger('omhFieldBeforeInit.' + this.getName(), [this, this.settings]);

	// Optional Method Hook for additional var initialization 
	_.invoke(this, 'varsInit');

	// Maybe preset value
	if (_.get(this.settings, 'presetValue', null)) {
		this.setValue(this.settings.presetValue, true);
	}

	// Initialize values for state tracking
	this.initValue = this.getValue();
	this.origValue = this.getValue();
	this.currValue = this.getValue();

	this.reset(true);

	// Init event handlers
	this.$input.on('focus', this._onFocus.bind(this)).on('blur', this._onBlur.bind(this)).on('input', this._onInput.bind(this)).on('change', this._onChange.bind(this)).on('click', this._onClick.bind(this));

	// Possible custom button event handlers
	this.$fieldResetBtns.on('click', this.reset.bind(this));
	this.$fieldFocusBtns.on('click', this.focus.bind(this));

	this.$fieldStateElems.addClass('initialized');

	// Add this field to its Form
	_.invoke(this, 'parent.addField', this);

	return OMHFormField.prototype.completeInit.call(this);
}

OMHFormField.prototype = $.extend({}, OMHFormField.prototype, {
	completeInit: function completeInit() {
		// Set this.initialized to true
		this.initialized = true;

		// But return false to revert this.initialized if this constructor
		// was run from a child class
		return false;
	},
	focus: function focus() {
		this.$input.focus();
	},
	getType: function getType() {
		return this.$input.attr('type');
	},
	getName: function getName() {
		return this.$input.data('omh-field-name') || this.$input.attr('name');
	},
	getNiceName: function getNiceName() {
		return this.$fieldLabel.length ? this.$fieldLabel.text() : this.getName();
	},
	// Value getters and setters
	getValue: function getValue() {
		var value = this.$input.val();

		switch (this.$input.data('omh-var-type')) {
			case 'int':
				return parseInt(value);
				break;
			case 'price':
				return parseFloat(value).toFixed(2);
				break;
			default:
				return value;
				break;
		}
		// return this.$input.val();
	},
	setValue: function setValue(value, setOrigValue) {
		setOrigValue = _.defaultTo(setOrigValue, false);
		this.$input.val(value);
		this.currValue = value;
		if (setOrigValue) {
			this.origValue = value;
			this.$input.trigger('change'); // For select2
		}

		return value;
	},
	clearValue: function clearValue(clearOrig, reset) {

		this.setValue(null);

		if (_.defaultTo(Boolean(clearOrig), true)) {
			this.origValue = null;
		}

		if (_.defaultTo(Boolean(reset), true)) {
			this.reset();
		}

		this.$input.trigger('clearValue');
	},
	// State getters and setters
	isOrigValue: function isOrigValue() {
		return _.isEqual(this.getValue(), this.origValue);
	},
	checkOrig: function checkOrig() {
		this.$fieldStateElems.toggleClass('orig-value', this.isOrigValue()).toggleClass('changed-value', !this.isOrigValue());
	},
	getParentEventNamespace: function getParentEventNamespace() {
		return _.get(this.parent, 'namespace', '');
	},
	resetState: function resetState() {
		// Reset state classes
		this.$fieldStateElems.addClass('never-focused never-input never-changed orig-value').removeClass('changed-value was-validated is-valid is-invalid');

		// Remove all feedback messages //dev:improve
		this.$wrap.children('.invalid-feedback, .valid-feedback').remove();
	},
	reset: function reset(quietly) {
		quietly = _.defaultTo(quietly, false);

		// Reset the input's value
		this.setValue(this.origValue);

		this.resetState();

		// Maybe pre-validate
		if (this.$input.hasClass('pre-validate')) {
			this.validate();
		}

		// Event hook for calc-fields.js
		if (!quietly) {

			this.$input.trigger('reset').trigger({ type: 'reset', namespace: this.getParentEventNamespace() }).trigger({ type: 'change', isReset: true }).trigger({ type: 'change', isReset: true, namespace: this.getParentEventNamespace() }).trigger('calcFields');
		}
	},
	deleteSelf: function deleteSelf(quietly) {
		quietly = _.defaultTo(quietly, false);

		_.invoke(this.parent, 'deleteField', [this, quietly]);
	},
	handleResponse: function handleResponse(response) {

		this.processResponseValue(response);

		if ('success' == response.status) {
			this.reset(true);
		} else {
			this.checkOrig();
			this.validate(false);
		}

		this.$fieldStateElems.addClass('never-changed-after-response');
	},
	processResponseValue: function processResponseValue(response) {

		this.setValue(_.get(response, ['data', this.getName(), 'value']), true);

		this.origValue = this.getValue();
	},

	// Event Handlers
	_onFocus: function _onFocus() {
		this.$fieldStateElems.removeClass('never-focused');
	},
	_onBlur: function _onBlur() {
		this.validate();
	},
	_onInput: function _onInput() {
		this.$fieldStateElems.removeClass('never-input');
		this.checkOrig();
		if (this.$input.hasClass('was-validated')) {
			this.validate();
		}
		this.$input.trigger({ type: 'input', namespace: this.getParentEventNamespace() });
	},
	_onChange: function _onChange(event) {

		// Don't count the change if this is a reset
		if (!_.get(event, 'isReset')) {
			this.$fieldStateElems.removeClass('never-changed never-input');
		}

		this.checkOrig();
		this.validate();
	},
	_onClick: function _onClick() {
		this.checkOrig();
	},
	isValid: function isValid() {

		var isValid = _.defaultTo(_.invoke(this.inputElem, 'checkValidity'), true);

		// Validate match-field (fields that must have matching values)
		if (this.$input.data('match-field')) {
			isValid = this.validateMatchField() && isValid;
		}

		return isValid;
	},
	// Validation
	validate: function validate(addClass) {

		if (_.defaultTo(addClass, true)) {
			this.$fieldStateElems.addClass('was-validated');
		}

		var validationMessage = this.inputElem.validationMessage;

		var isValid = this.isValid();

		// Check for custom validation messages //dev:improve
		if (!isValid && this.$input.attr('pattern') && this.$input.data('invalid-msg-pattern')) {
			validationMessage = this.$input.data('invalid-msg-pattern');
		}

		// Maybe toggle state classes
		if (this.settings.validationClasses) {
			this.$fieldStateElems.toggleClass('is-valid', isValid).toggleClass('is-invalid', !isValid);
		}

		// Set (enable/disable) default feedback message //dev:improve
		this.setFeedbackMsg(validationMessage, 'invalid', 'default', !isValid);
	},
	validatePattern: function validatePattern() {},
	validateMatchField: function validateMatchField() {
		var matchField = this.parent.getField(this.$input.data('match-field'));
		var matchValid = true;

		if (matchField && !(matchField.$input.hasClass('never-changed') || matchField.$input.hasClass('never-changed-after-response'))) {
			matchValid = matchField.getValue() == this.getValue();

			// Validate the matchField too. Check whether the match-invalid class has been set to prevent infinite loop
			if (matchValid == this.$input.hasClass('match-invalid')) {
				this.$input.toggleClass('match-invalid', !matchValid);
				matchField.validate();
			}

			this.$input.toggleClass('match-invalid', !matchValid);

			var invalidMatchMsg = 'Value must match ' + matchField.getNiceName() + ' field.';

			this.setFeedbackMsg(invalidMatchMsg, 'invalid', 'invalid-match', !matchValid);
		}

		return matchValid;
	},
	setFeedbackMsg: function setFeedbackMsg(msg, type, feedbackId, enabled) {
		msg = _.defaultTo(msg, '');
		type = _.defaultTo(type, 'invalid');
		enabled = _.defaultTo(Boolean(enabled), true);

		// See if there is an existing feedback message with this ID
		var $existingMsg = null;
		if (_.defaultTo(feedbackId, null)) {
			$existingMsg = this.$wrap.children('[data-feedback-id="' + feedbackId + '"]').first();
		}

		if ($existingMsg.length) {
			$existingMsg.html(msg).toggle(enabled);
		} else if (enabled) {
			var msgClass = type + '-feedback';
			$('<div>', { class: msgClass }).attr('data-feedback-id', feedbackId).html(msg).toggle(enabled).appendTo(this.$wrap);
		}
	}
});

module.exports = OMHFormField;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldAdvanced(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		autoInitFields: true,
		subfieldsIsArray: false,
		pruneFieldsOnSetValue: false
	});

	// Child field instances
	this.fields = options.subfieldsIsArray ? [] : {};

	this.initialized = _formFieldBase2.default.call(this, input, parent, options);

	this.settings.namespace = this.namespace = _.get(this.settings, 'namespace', _.defaultTo(this.$wrap.data('omh-namespace'), this.getName()));

	this.settings.childFieldSelector = '[data-omh-subfield-of="' + this.namespace + '"]';

	if (this.settings.autoInitFields) {
		this.initChildFields();
	}

	// Initialize values for state tracking //dev:improve
	this.initValue = this.getValue();
	this.origValue = this.getValue();
	this.currValue = this.getValue();

	var namespacedEvents = ['input.' + this.namespace, 'change.' + this.namespace, 'reset.' + this.namespace];

	$(document).on(namespacedEvents.join(' '), this._onInput.bind(this));

	return OMHFormFieldAdvanced.prototype.completeInit.call(this);
}

OMHFormFieldAdvanced.prototype = $.extend({}, _formFieldBase2.default.prototype, OMHFormFieldAdvanced.prototype, {
	// getName: function() {
	// 	return this.$wrap.attr('name');
	// },
	initChildFields: function initChildFields() {

		var self = this,
		    $childFields = $(this.settings.childFieldSelector);

		$childFields.each(function (index, childField) {

			var options = {};

			// Check for preset value of child
			if (_.has(self.settings.presetValue, $(childField).attr('name'))) {

				options.presetValue = self.settings.presetValue[$(childField).attr('name')];
			}

			$(childField).OMHFormField(self, options);
		});

		// $childField.OMHFormField( this );
	},
	// Called from child fields via OMHFormFieldBase constructor
	addField: function addField(field) {
		//dev:improve
		if (this.settings.subfieldsIsArray) {
			this.fields.push(field);
		} else {
			this.fields[field.getName()] = field;
		}
	},
	// Will often be overwritten for advanced customization
	getValue: function getValue() {
		return this.settings.subfieldsIsArray ? _.toArray(_.invokeMap(this.fields, 'getValue')) : _.zipObject(_.keys(this.fields), _.invokeMap(this.fields, 'getValue'));
	},
	setValue: function setValue(value, setOrigValue) {
		setOrigValue = _.defaultTo(setOrigValue, false);

		this.currValue = value;

		// Field Pruning: Remove fields not found in new setValue
		// Maybe prepare list of previous field keys to know which ones to prune
		var pruneKeys = [];
		if (this.settings.pruneFieldsOnSetValue) {
			pruneKeys = _.keys(this.fields);
		}

		var self = this;
		_.forEach(value, function (fieldValue, fieldIndex) {

			var childField = _.defaultTo(self.fields[fieldIndex]);

			if (childField) {

				childField.setValue(fieldValue, setOrigValue);

				// Maybe update field pruning key tracking
				if (self.settings.pruneFieldsOnSetValue) {
					_.pull(pruneKeys, fieldIndex);
				}
			}
			// self.fields[ fieldIndex ].setValue( fieldValue );
		});

		_.map(pruneKeys, function (deleteFieldKey) {
			self.deleteField(self.fields[deleteFieldKey]);
		});

		if (setOrigValue) {
			this.origValue = this.getValue();
		}

		// if( this.settings.subfieldsIsArray ) {
		// 	var self = this;
		// 	_.forEach( this.fields, function( field, index) {
		// 		field.setValue( value[ index ] );
		// 	});
		// }
	},

	isValid: function isValid() {
		return _.every(_.invokeMap(this.fields, 'isValid'));
	},
	reset: function reset(quietly) {
		quietly = _.defaultTo(quietly, false);

		_formFieldBase2.default.prototype.reset.call(this, quietly);

		_.invokeMap(this.fields, 'reset', quietly);
	},
	deleteField: function deleteField(field, quietly) {
		quietly = _.defaultTo(quietly, false);

		field.$wrap.remove();
		// priceTier.$tierHeader.remove();

		$(document).trigger('fieldDeleted', [field]);

		if (this.settings.subfieldsIsArray) {
			_.remove(this.fields, field);
		} else {
			_.unset(this.fields, field.getName());
		}

		if (!quietly) {
			this.$input.trigger({ type: 'change', namespace: this.getParentEventNamespace() });
		}
	}
});

module.exports = OMHFormFieldAdvanced;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = jQuery;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),
/* 5 */
/***/ (function(module, exports) {

/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {/* globals __webpack_amd_options__ */
module.exports = __webpack_amd_options__;

/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldCheckbox(input, parent, options) {
	this.initialized = false;

	options = _.defaultsDeep(options, {
		presetValue: false
	});

	return _formFieldBase2.default.call(this, input, parent, options);
}

OMHFormFieldCheckbox.prototype = $.extend({}, _formFieldBase2.default.prototype, OMHFormFieldCheckbox.prototype, {
	getValue: function getValue() {
		return this.$input.prop('checked');
	},
	setValue: function setValue(value, setOrigValue) {
		setOrigValue = _.defaultTo(setOrigValue, false);
		this.$input.prop('checked', Boolean(value));
		this.currValue = value;
		if (setOrigValue) {
			this.origValue = value;
		}

		return value;
	}
});

module.exports = OMHFormFieldCheckbox;

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldProductPriceTier(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		createDom: true,
		tierIndex: 1,
		tierValue: 1,
		tiersType: 'product'
	});

	this.initialized = _formFieldBase2.default.call(this, input, parent, options);

	this.$deleteBtn = this.$tierHeader.find('.price-tier__remove');

	this.$deleteBtn.click(this.deletePriceTier.bind(this));

	this.$input.trigger('productPriceTierAdded', [this, this.settings]);

	return OMHFormFieldProductPriceTier.prototype.completeInit.call(this);
}

OMHFormFieldProductPriceTier.prototype = _.create(_formFieldBase2.default.prototype, {

	domInit: function domInit() {

		// Optionally create the Price Tier's Dom nodes (th & td)
		if (this.settings.createDom) {

			this.parent.$headerRow.append(this.parent.templates.tierHeader(this.settings));

			this.parent.$valuesRow.append(this.parent.templates.tierValue(this.settings));
		}
	},
	varsInit: function varsInit() {
		this.$input = this.parent.$valuesRow.find('[data-price-tier="' + this.settings.tierIndex + '"]');
		this.$wrap = this.$input.parent();
		this.$tierHeader = this.parent.$headerRow.find('[data-price-tier="' + this.settings.tierIndex + '"]');
	},
	getValue: function getValue() {
		return parseInt(this.$input.val());
	},
	_onChange: function _onChange(event) {

		_formFieldBase2.default.prototype._onChange.call(this, event);

		this.$input.trigger('productPriceTierUpdated', [this]);

		this.parent.recalcTiers();
	},
	isHighestTier: function isHighestTier() {
		return this.settings.tierIndex == this.parent.fields.length;
	},
	deletePriceTier: function deletePriceTier() {
		if (this.isHighestTier()) {
			this.parent.deletePriceTier(this);
		}
	}

});

module.exports = OMHFormFieldProductPriceTier;

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldGarmentPriceTier(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		createDom: true,
		tierIndex: 1,
		tierValue: 1,
		tiersType: 'garment'
	});

	this.initialized = _formFieldBase2.default.call(this, input, parent, options);

	// this.$deleteBtn = this.$tierHeader.find('.price-tier__remove');

	// this.$deleteBtn.click(this.deletePriceTier.bind(this));

	return OMHFormFieldGarmentPriceTier.prototype.completeInit.call(this);
}

OMHFormFieldGarmentPriceTier.prototype = _.create(_formFieldBase2.default.prototype, {

	domInit: function domInit() {

		// Optionally create the Price Tier's Dom nodes (th & td)
		if (this.settings.createDom) {

			this.parent.$headerRow.append(this.parent.templates.tierHeader(this.settings));

			this.parent.$valuesRow.append(this.parent.templates.tierValue(this.settings));
		}
	},
	varsInit: function varsInit() {
		this.$input = this.parent.$valuesRow.find('[data-price-tier="' + this.settings.tierIndex + '"]');
		this.$wrap = this.$input.parent();
		this.$tierHeader = this.parent.$headerRow.find('[data-price-tier="' + this.settings.tierIndex + '"]');
		this.$tierMinQty = this.$tierHeader.find('.product-price-tier-qty');
	},
	getValue: function getValue() {
		return parseFloat(this.$input.val());
	}
	// _onChange: function(event) {

	// 	OMHFormField.prototype._onChange.call( this, event );

	// 	this.parent.recalcTiers();
	// },
	// isHighestTier: function() {
	// 	return ( this.settings.tierIndex == this.parent.fields.length );
	// },
	// deletePriceTier: function() {
	// 	if( this.isHighestTier() ) {
	// 		this.parent.deletePriceTier(this);
	// 	}
	// }

});

module.exports = OMHFormFieldGarmentPriceTier;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldAdvanced = __webpack_require__(1);

var _formFieldAdvanced2 = _interopRequireDefault(_formFieldAdvanced);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

// import OMHFormFieldPriceTiers 	from './form-field-price-tiers.js';
// import OMHFormField 			from './form-field-base.js';

function OMHFormFieldGarment(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		productPriceTiers: null,
		createDom: true,
		tiersType: 'garment',
		garmentStyleId: null,
		garmentStyle: null,
		presetValue: null
	});

	options.garmentFullName = _.get(options.garmentStyle, 'style_full_name', '');

	this.initialized = _formFieldAdvanced2.default.call(this, input, parent, options);

	return OMHFormFieldGarment.prototype.completeInit.call(this);
}

OMHFormFieldGarment.prototype = _.create(_formFieldAdvanced2.default.prototype, {
	getProductPriceTiers: function getProductPriceTiers() {
		return this.parent.getProductPriceTiers();
	},
	domInit: function domInit() {
		this.settings.garmentNamespace = 'product_garments[' + this.settings.garmentStyleId + ']';
		// this.settings.garmentFullName = _.get( this.settings.garmentStyle, 'style_full_name' );
		// this.settings.garmentImgSrc = '//stores.inksoft.com/' + this.settings.garmentStyle.inksoft_image_url;

		// Optionally create the Price Tier's Dom nodes (th & td)
		if (this.settings.createDom) {

			var templateClone = this.parent.templates.garmentRow(this.settings);

			$(templateClone).insertBefore(this.parent.$tableBody.children().last());

			this.$input = this.parent.$tableBody.children('[name="' + this.settings.garmentStyleId + '"]');

			// Refresh / add Calc fields
			omh.CalcFields.initCalcFields();
		}
	},
	varsInit: function varsInit() {
		this.$removeGarment = this.$wrap.find('input.remove-garment').eq(0);
	},
	shouldDelete: function shouldDelete() {
		return Boolean(this.$removeGarment.prop('checked'));
	},
	completeInit: function completeInit() {

		this.$wrap.find('.omh-garment-name').html(this.settings.garmentFullName);
		// this.$wrap.find('.garment-blank-image').attr('src',this.settings.garmentImgSrc);
		this.$wrap.removeClass('row--template');

		return _formFieldAdvanced2.default.prototype.completeInit(this);
	}
});

module.exports = OMHFormFieldGarment;

/***/ }),
/* 10 */,
/* 11 */,
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var _lodash = __webpack_require__(13);

var _lodash2 = _interopRequireDefault(_lodash);

var _clipboard = __webpack_require__(14);

var _clipboard2 = _interopRequireDefault(_clipboard);

__webpack_require__(15);

__webpack_require__(16);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

__webpack_require__(17);
__webpack_require__(18);

// Moved debug flag out of omh assignment so it can be used immediately in omh componennts
global.omhDebug = window.omhDebug = true;

global.omh = window.omh = {
	debug: omhDebug,
	ajax: __webpack_require__(19),
	Drawers: __webpack_require__(20),
	FormFields: __webpack_require__(21),
	Forms: __webpack_require__(33),
	Tables: __webpack_require__(34),
	// Graphs: 			require('./modules/graphs'),
	Notices: __webpack_require__(35),
	CalcFields: __webpack_require__(36),
	// BulkOrder:  		require('./modules/bulk-order'),
	Modals: __webpack_require__(37),
	PriceWidget: __webpack_require__(38),
	// CreateProduct: 		require('./modules/create-product'),
	// AdminEditProduct: 	require('./modules/admin-edit-product'),
	// TempUpload: 		require('./modules/temp-upload'),
	pdp: __webpack_require__(39)
};

(function (i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;i[r] = i[r] || function () {
		(i[r].q = i[r].q || []).push(arguments);
	}, i[r].l = 1 * new Date();a = s.createElement(o), m = s.getElementsByTagName(o)[0];a.async = 1;a.src = g;m.parentNode.insertBefore(a, m);
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

ga('create', 'UA-109289476-1', 'auto');
ga('send', 'pageview');

jQuery(document).ready(function ($) {
	new _clipboard2.default('.copy-to-clipboard');

	$('.form-control').click(function (e) {
		if ($(this).prop('readonly')) {
			// Auto-select contents of disabled cells
			$(this).select();
		}
	});

	jQuery('[data-toggle="popover"]').popover();

	// disable mousewheel on a input number field when in focus
	// (to prevent Cromium browsers changing the value when scrolling)
	$(document).on('focus', 'input[type=number]', function (e) {
		$(this).on('mousewheel.disableScroll', function (e) {
			e.preventDefault();
		});
	});
	$(document).on('blur', 'input[type=number]', function (e) {
		$(this).off('mousewheel.disableScroll');
	}).on('omh_save_user_account_details', function (event, responseData) {

		var userInitials = _lodash2.default.get(responseData, 'data.user_initials.value'),
		    userFullName = _lodash2.default.get(responseData, 'data.full_name.value');

		$('.dashboard-user-initials').text(userInitials);
		$('.dashboard-user-fullname').text(userFullName);
	});

	// Home button animation
	if ($('#mh-home-hero .nectar-button').length) {
		var ctaType = function ctaType() {
			// If currentPart is not blank and does not equal the current word
			// we are mid-typing
			if (typeForward == -1 && currentPart || typeForward == 1 && currentPart != currentWord) {
				letterIndex += typeForward; // Advance (or reduce) the slice length
				currentPart = currentWord.slice(0, letterIndex);
				$homeCta.html(ctaPrefix + currentPart);
				setTimeout(ctaType, typingSpeed);
			} else {

				// Maybe advance word
				if (typeForward == -1) {
					suffixIndex++;
					suffixIndex = suffixIndex >= ctaSuffixes.length ? 0 : suffixIndex;
					currentWord = ctaSuffixes[suffixIndex];
				}

				// Always switch direction
				typeForward = typeForward * -1;

				setTimeout(ctaType, typingPause);
			}
		};

		var $homeCta = $('#mh-home-hero .nectar-button span').eq(0),
		    ctaPrefix = 'Search ',
		    ctaSuffixes = ['Products', 'Fraternities', 'Sororities'],
		    typingSpeed = 100,
		    typingPause = 600,
		    suffixIndex = 0,
		    letterIndex = 1,
		    typeForward = 1;

		var currentWord = ctaSuffixes[suffixIndex],
		    currentPart = '';

		setTimeout(ctaType, 1500);
	}

	// Login Page
	$('#login-register-links #customer-register').on('click', function (e) {
		$('.woocommerce-account .main-content .nectar-form-controls .control:last-child').click();
	});

	// Add Signup modal attributes
	$('.omh-trigger-signup-modal').attr('data-toggle', 'modal');
	$('.omh-trigger-signup-modal').attr('data-target', '#signup-modal');

	if (window.location.hash === "#signup") {
		$("#signup-modal").modal();
	}
	// Add Start design modal attributes
	$('.omh-trigger-start-design-modal').attr('data-toggle', 'modal');
	$('.omh-trigger-start-design-modal').attr('data-target', '#start-design-modal');

	// $(document).on('show.bs.modal', '.modal', function() {
	// 	$(".woocommerce-store-notice").css("display", "none")
	// })

	// $(document).on('hide.bs.modal', '.modal', function() {
	// 	$(".woocommerce-store-notice").css("display", "block")
	// })

	// Fire custom load event to be able to run inline JS after OMH and theme code has ran
	$(document).trigger('omhloaded');
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global, module) {var __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/**
 * @license
 * Lodash lodash.com/license | Underscore.js 1.8.3 underscorejs.org/LICENSE
 */
;(function () {
  function n(n, t) {
    return n.set(t[0], t[1]), n;
  }function t(n, t) {
    return n.add(t), n;
  }function r(n, t, r) {
    switch (r.length) {case 0:
        return n.call(t);case 1:
        return n.call(t, r[0]);case 2:
        return n.call(t, r[0], r[1]);case 3:
        return n.call(t, r[0], r[1], r[2]);}return n.apply(t, r);
  }function e(n, t, r, e) {
    for (var u = -1, i = null == n ? 0 : n.length; ++u < i;) {
      var o = n[u];t(e, o, r(o), n);
    }return e;
  }function u(n, t) {
    for (var r = -1, e = null == n ? 0 : n.length; ++r < e && false !== t(n[r], r, n);) {}return n;
  }function i(n, t) {
    for (var r = null == n ? 0 : n.length; r-- && false !== t(n[r], r, n);) {}
    return n;
  }function o(n, t) {
    for (var r = -1, e = null == n ? 0 : n.length; ++r < e;) {
      if (!t(n[r], r, n)) return false;
    }return true;
  }function f(n, t) {
    for (var r = -1, e = null == n ? 0 : n.length, u = 0, i = []; ++r < e;) {
      var o = n[r];t(o, r, n) && (i[u++] = o);
    }return i;
  }function c(n, t) {
    return !(null == n || !n.length) && -1 < d(n, t, 0);
  }function a(n, t, r) {
    for (var e = -1, u = null == n ? 0 : n.length; ++e < u;) {
      if (r(t, n[e])) return true;
    }return false;
  }function l(n, t) {
    for (var r = -1, e = null == n ? 0 : n.length, u = Array(e); ++r < e;) {
      u[r] = t(n[r], r, n);
    }return u;
  }function s(n, t) {
    for (var r = -1, e = t.length, u = n.length; ++r < e;) {
      n[u + r] = t[r];
    }return n;
  }function h(n, t, r, e) {
    var u = -1,
        i = null == n ? 0 : n.length;for (e && i && (r = n[++u]); ++u < i;) {
      r = t(r, n[u], u, n);
    }return r;
  }function p(n, t, r, e) {
    var u = null == n ? 0 : n.length;for (e && u && (r = n[--u]); u--;) {
      r = t(r, n[u], u, n);
    }return r;
  }function _(n, t) {
    for (var r = -1, e = null == n ? 0 : n.length; ++r < e;) {
      if (t(n[r], r, n)) return true;
    }return false;
  }function v(n, t, r) {
    var e;return r(n, function (n, r, u) {
      if (t(n, r, u)) return e = r, false;
    }), e;
  }function g(n, t, r, e) {
    var u = n.length;for (r += e ? 1 : -1; e ? r-- : ++r < u;) {
      if (t(n[r], r, n)) return r;
    }return -1;
  }function d(n, t, r) {
    if (t === t) n: {
      --r;for (var e = n.length; ++r < e;) {
        if (n[r] === t) {
          n = r;break n;
        }
      }n = -1;
    } else n = g(n, b, r);return n;
  }function y(n, t, r, e) {
    --r;for (var u = n.length; ++r < u;) {
      if (e(n[r], t)) return r;
    }return -1;
  }function b(n) {
    return n !== n;
  }function x(n, t) {
    var r = null == n ? 0 : n.length;return r ? k(n, t) / r : P;
  }function j(n) {
    return function (t) {
      return null == t ? F : t[n];
    };
  }function w(n) {
    return function (t) {
      return null == n ? F : n[t];
    };
  }function m(n, t, r, e, u) {
    return u(n, function (n, u, i) {
      r = e ? (e = false, n) : t(r, n, u, i);
    }), r;
  }function A(n, t) {
    var r = n.length;for (n.sort(t); r--;) {
      n[r] = n[r].c;
    }return n;
  }function k(n, t) {
    for (var r, e = -1, u = n.length; ++e < u;) {
      var i = t(n[e]);i !== F && (r = r === F ? i : r + i);
    }return r;
  }function E(n, t) {
    for (var r = -1, e = Array(n); ++r < n;) {
      e[r] = t(r);
    }return e;
  }function O(n, t) {
    return l(t, function (t) {
      return [t, n[t]];
    });
  }function S(n) {
    return function (t) {
      return n(t);
    };
  }function I(n, t) {
    return l(t, function (t) {
      return n[t];
    });
  }function R(n, t) {
    return n.has(t);
  }function z(n, t) {
    for (var r = -1, e = n.length; ++r < e && -1 < d(t, n[r], 0);) {}return r;
  }function W(n, t) {
    for (var r = n.length; r-- && -1 < d(t, n[r], 0);) {}return r;
  }function B(n) {
    return "\\" + Tn[n];
  }function L(n) {
    var t = -1,
        r = Array(n.size);return n.forEach(function (n, e) {
      r[++t] = [e, n];
    }), r;
  }function U(n, t) {
    return function (r) {
      return n(t(r));
    };
  }function C(n, t) {
    for (var r = -1, e = n.length, u = 0, i = []; ++r < e;) {
      var o = n[r];o !== t && "__lodash_placeholder__" !== o || (n[r] = "__lodash_placeholder__", i[u++] = r);
    }return i;
  }function D(n) {
    var t = -1,
        r = Array(n.size);return n.forEach(function (n) {
      r[++t] = n;
    }), r;
  }function M(n) {
    var t = -1,
        r = Array(n.size);return n.forEach(function (n) {
      r[++t] = [n, n];
    }), r;
  }function T(n) {
    if (Bn.test(n)) {
      for (var t = zn.lastIndex = 0; zn.test(n);) {
        ++t;
      }n = t;
    } else n = tt(n);return n;
  }function $(n) {
    return Bn.test(n) ? n.match(zn) || [] : n.split("");
  }var F,
      N = 1 / 0,
      P = NaN,
      Z = [["ary", 128], ["bind", 1], ["bindKey", 2], ["curry", 8], ["curryRight", 16], ["flip", 512], ["partial", 32], ["partialRight", 64], ["rearg", 256]],
      q = /\b__p\+='';/g,
      V = /\b(__p\+=)''\+/g,
      K = /(__e\(.*?\)|\b__t\))\+'';/g,
      G = /&(?:amp|lt|gt|quot|#39);/g,
      H = /[&<>"']/g,
      J = RegExp(G.source),
      Y = RegExp(H.source),
      Q = /<%-([\s\S]+?)%>/g,
      X = /<%([\s\S]+?)%>/g,
      nn = /<%=([\s\S]+?)%>/g,
      tn = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
      rn = /^\w*$/,
      en = /^\./,
      un = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,
      on = /[\\^$.*+?()[\]{}|]/g,
      fn = RegExp(on.source),
      cn = /^\s+|\s+$/g,
      an = /^\s+/,
      ln = /\s+$/,
      sn = /\{(?:\n\/\* \[wrapped with .+\] \*\/)?\n?/,
      hn = /\{\n\/\* \[wrapped with (.+)\] \*/,
      pn = /,? & /,
      _n = /[^\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\x7f]+/g,
      vn = /\\(\\)?/g,
      gn = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g,
      dn = /\w*$/,
      yn = /^[-+]0x[0-9a-f]+$/i,
      bn = /^0b[01]+$/i,
      xn = /^\[object .+?Constructor\]$/,
      jn = /^0o[0-7]+$/i,
      wn = /^(?:0|[1-9]\d*)$/,
      mn = /[\xc0-\xd6\xd8-\xf6\xf8-\xff\u0100-\u017f]/g,
      An = /($^)/,
      kn = /['\n\r\u2028\u2029\\]/g,
      En = "[\\ufe0e\\ufe0f]?(?:[\\u0300-\\u036f\\ufe20-\\ufe2f\\u20d0-\\u20ff]|\\ud83c[\\udffb-\\udfff])?(?:\\u200d(?:[^\\ud800-\\udfff]|(?:\\ud83c[\\udde6-\\uddff]){2}|[\\ud800-\\udbff][\\udc00-\\udfff])[\\ufe0e\\ufe0f]?(?:[\\u0300-\\u036f\\ufe20-\\ufe2f\\u20d0-\\u20ff]|\\ud83c[\\udffb-\\udfff])?)*",
      On = "(?:[\\u2700-\\u27bf]|(?:\\ud83c[\\udde6-\\uddff]){2}|[\\ud800-\\udbff][\\udc00-\\udfff])" + En,
      Sn = "(?:[^\\ud800-\\udfff][\\u0300-\\u036f\\ufe20-\\ufe2f\\u20d0-\\u20ff]?|[\\u0300-\\u036f\\ufe20-\\ufe2f\\u20d0-\\u20ff]|(?:\\ud83c[\\udde6-\\uddff]){2}|[\\ud800-\\udbff][\\udc00-\\udfff]|[\\ud800-\\udfff])",
      In = RegExp("['\u2019]", "g"),
      Rn = RegExp("[\\u0300-\\u036f\\ufe20-\\ufe2f\\u20d0-\\u20ff]", "g"),
      zn = RegExp("\\ud83c[\\udffb-\\udfff](?=\\ud83c[\\udffb-\\udfff])|" + Sn + En, "g"),
      Wn = RegExp(["[A-Z\\xc0-\\xd6\\xd8-\\xde]?[a-z\\xdf-\\xf6\\xf8-\\xff]+(?:['\u2019](?:d|ll|m|re|s|t|ve))?(?=[\\xac\\xb1\\xd7\\xf7\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf\\u2000-\\u206f \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000]|[A-Z\\xc0-\\xd6\\xd8-\\xde]|$)|(?:[A-Z\\xc0-\\xd6\\xd8-\\xde]|[^\\ud800-\\udfff\\xac\\xb1\\xd7\\xf7\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf\\u2000-\\u206f \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000\\d+\\u2700-\\u27bfa-z\\xdf-\\xf6\\xf8-\\xffA-Z\\xc0-\\xd6\\xd8-\\xde])+(?:['\u2019](?:D|LL|M|RE|S|T|VE))?(?=[\\xac\\xb1\\xd7\\xf7\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf\\u2000-\\u206f \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000]|[A-Z\\xc0-\\xd6\\xd8-\\xde](?:[a-z\\xdf-\\xf6\\xf8-\\xff]|[^\\ud800-\\udfff\\xac\\xb1\\xd7\\xf7\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf\\u2000-\\u206f \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000\\d+\\u2700-\\u27bfa-z\\xdf-\\xf6\\xf8-\\xffA-Z\\xc0-\\xd6\\xd8-\\xde])|$)|[A-Z\\xc0-\\xd6\\xd8-\\xde]?(?:[a-z\\xdf-\\xf6\\xf8-\\xff]|[^\\ud800-\\udfff\\xac\\xb1\\xd7\\xf7\\x00-\\x2f\\x3a-\\x40\\x5b-\\x60\\x7b-\\xbf\\u2000-\\u206f \\t\\x0b\\f\\xa0\\ufeff\\n\\r\\u2028\\u2029\\u1680\\u180e\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200a\\u202f\\u205f\\u3000\\d+\\u2700-\\u27bfa-z\\xdf-\\xf6\\xf8-\\xffA-Z\\xc0-\\xd6\\xd8-\\xde])+(?:['\u2019](?:d|ll|m|re|s|t|ve))?|[A-Z\\xc0-\\xd6\\xd8-\\xde]+(?:['\u2019](?:D|LL|M|RE|S|T|VE))?|\\d*(?:(?:1ST|2ND|3RD|(?![123])\\dTH)\\b)|\\d*(?:(?:1st|2nd|3rd|(?![123])\\dth)\\b)|\\d+", On].join("|"), "g"),
      Bn = RegExp("[\\u200d\\ud800-\\udfff\\u0300-\\u036f\\ufe20-\\ufe2f\\u20d0-\\u20ff\\ufe0e\\ufe0f]"),
      Ln = /[a-z][A-Z]|[A-Z]{2,}[a-z]|[0-9][a-zA-Z]|[a-zA-Z][0-9]|[^a-zA-Z0-9 ]/,
      Un = "Array Buffer DataView Date Error Float32Array Float64Array Function Int8Array Int16Array Int32Array Map Math Object Promise RegExp Set String Symbol TypeError Uint8Array Uint8ClampedArray Uint16Array Uint32Array WeakMap _ clearTimeout isFinite parseInt setTimeout".split(" "),
      Cn = {};
  Cn["[object Float32Array]"] = Cn["[object Float64Array]"] = Cn["[object Int8Array]"] = Cn["[object Int16Array]"] = Cn["[object Int32Array]"] = Cn["[object Uint8Array]"] = Cn["[object Uint8ClampedArray]"] = Cn["[object Uint16Array]"] = Cn["[object Uint32Array]"] = true, Cn["[object Arguments]"] = Cn["[object Array]"] = Cn["[object ArrayBuffer]"] = Cn["[object Boolean]"] = Cn["[object DataView]"] = Cn["[object Date]"] = Cn["[object Error]"] = Cn["[object Function]"] = Cn["[object Map]"] = Cn["[object Number]"] = Cn["[object Object]"] = Cn["[object RegExp]"] = Cn["[object Set]"] = Cn["[object String]"] = Cn["[object WeakMap]"] = false;
  var Dn = {};Dn["[object Arguments]"] = Dn["[object Array]"] = Dn["[object ArrayBuffer]"] = Dn["[object DataView]"] = Dn["[object Boolean]"] = Dn["[object Date]"] = Dn["[object Float32Array]"] = Dn["[object Float64Array]"] = Dn["[object Int8Array]"] = Dn["[object Int16Array]"] = Dn["[object Int32Array]"] = Dn["[object Map]"] = Dn["[object Number]"] = Dn["[object Object]"] = Dn["[object RegExp]"] = Dn["[object Set]"] = Dn["[object String]"] = Dn["[object Symbol]"] = Dn["[object Uint8Array]"] = Dn["[object Uint8ClampedArray]"] = Dn["[object Uint16Array]"] = Dn["[object Uint32Array]"] = true, Dn["[object Error]"] = Dn["[object Function]"] = Dn["[object WeakMap]"] = false;var Mn,
      Tn = { "\\": "\\", "'": "'", "\n": "n", "\r": "r", "\u2028": "u2028", "\u2029": "u2029" },
      $n = parseFloat,
      Fn = parseInt,
      Nn = (typeof global === "undefined" ? "undefined" : _typeof(global)) == "object" && global && global.Object === Object && global,
      Pn = (typeof self === "undefined" ? "undefined" : _typeof(self)) == "object" && self && self.Object === Object && self,
      Zn = Nn || Pn || Function("return this")(),
      qn = ( false ? "undefined" : _typeof(exports)) == "object" && exports && !exports.nodeType && exports,
      Vn = qn && ( false ? "undefined" : _typeof(module)) == "object" && module && !module.nodeType && module,
      Kn = Vn && Vn.exports === qn,
      Gn = Kn && Nn.process;
  n: {
    try {
      Mn = Gn && Gn.binding && Gn.binding("util");break n;
    } catch (n) {}Mn = void 0;
  }var Hn = Mn && Mn.isArrayBuffer,
      Jn = Mn && Mn.isDate,
      Yn = Mn && Mn.isMap,
      Qn = Mn && Mn.isRegExp,
      Xn = Mn && Mn.isSet,
      nt = Mn && Mn.isTypedArray,
      tt = j("length"),
      rt = w({ "\xc0": "A", "\xc1": "A", "\xc2": "A", "\xc3": "A", "\xc4": "A", "\xc5": "A", "\xe0": "a", "\xe1": "a", "\xe2": "a", "\xe3": "a", "\xe4": "a", "\xe5": "a", "\xc7": "C", "\xe7": "c", "\xd0": "D", "\xf0": "d", "\xc8": "E", "\xc9": "E", "\xca": "E", "\xcb": "E", "\xe8": "e", "\xe9": "e", "\xea": "e", "\xeb": "e", "\xcc": "I", "\xcd": "I", "\xce": "I",
    "\xcf": "I", "\xec": "i", "\xed": "i", "\xee": "i", "\xef": "i", "\xd1": "N", "\xf1": "n", "\xd2": "O", "\xd3": "O", "\xd4": "O", "\xd5": "O", "\xd6": "O", "\xd8": "O", "\xf2": "o", "\xf3": "o", "\xf4": "o", "\xf5": "o", "\xf6": "o", "\xf8": "o", "\xd9": "U", "\xda": "U", "\xdb": "U", "\xdc": "U", "\xf9": "u", "\xfa": "u", "\xfb": "u", "\xfc": "u", "\xdd": "Y", "\xfd": "y", "\xff": "y", "\xc6": "Ae", "\xe6": "ae", "\xde": "Th", "\xfe": "th", "\xdf": "ss", "\u0100": "A", "\u0102": "A", "\u0104": "A", "\u0101": "a", "\u0103": "a", "\u0105": "a", "\u0106": "C", "\u0108": "C", "\u010A": "C",
    "\u010C": "C", "\u0107": "c", "\u0109": "c", "\u010B": "c", "\u010D": "c", "\u010E": "D", "\u0110": "D", "\u010F": "d", "\u0111": "d", "\u0112": "E", "\u0114": "E", "\u0116": "E", "\u0118": "E", "\u011A": "E", "\u0113": "e", "\u0115": "e", "\u0117": "e", "\u0119": "e", "\u011B": "e", "\u011C": "G", "\u011E": "G", "\u0120": "G", "\u0122": "G", "\u011D": "g", "\u011F": "g", "\u0121": "g", "\u0123": "g", "\u0124": "H", "\u0126": "H", "\u0125": "h", "\u0127": "h", "\u0128": "I", "\u012A": "I", "\u012C": "I", "\u012E": "I", "\u0130": "I", "\u0129": "i", "\u012B": "i", "\u012D": "i",
    "\u012F": "i", "\u0131": "i", "\u0134": "J", "\u0135": "j", "\u0136": "K", "\u0137": "k", "\u0138": "k", "\u0139": "L", "\u013B": "L", "\u013D": "L", "\u013F": "L", "\u0141": "L", "\u013A": "l", "\u013C": "l", "\u013E": "l", "\u0140": "l", "\u0142": "l", "\u0143": "N", "\u0145": "N", "\u0147": "N", "\u014A": "N", "\u0144": "n", "\u0146": "n", "\u0148": "n", "\u014B": "n", "\u014C": "O", "\u014E": "O", "\u0150": "O", "\u014D": "o", "\u014F": "o", "\u0151": "o", "\u0154": "R", "\u0156": "R", "\u0158": "R", "\u0155": "r", "\u0157": "r", "\u0159": "r", "\u015A": "S", "\u015C": "S",
    "\u015E": "S", "\u0160": "S", "\u015B": "s", "\u015D": "s", "\u015F": "s", "\u0161": "s", "\u0162": "T", "\u0164": "T", "\u0166": "T", "\u0163": "t", "\u0165": "t", "\u0167": "t", "\u0168": "U", "\u016A": "U", "\u016C": "U", "\u016E": "U", "\u0170": "U", "\u0172": "U", "\u0169": "u", "\u016B": "u", "\u016D": "u", "\u016F": "u", "\u0171": "u", "\u0173": "u", "\u0174": "W", "\u0175": "w", "\u0176": "Y", "\u0177": "y", "\u0178": "Y", "\u0179": "Z", "\u017B": "Z", "\u017D": "Z", "\u017A": "z", "\u017C": "z", "\u017E": "z", "\u0132": "IJ", "\u0133": "ij", "\u0152": "Oe", "\u0153": "oe",
    "\u0149": "'n", "\u017F": "s" }),
      et = w({ "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#39;" }),
      ut = w({ "&amp;": "&", "&lt;": "<", "&gt;": ">", "&quot;": '"', "&#39;": "'" }),
      it = function w(En) {
    function On(n) {
      if (xu(n) && !af(n) && !(n instanceof Mn)) {
        if (n instanceof zn) return n;if (ci.call(n, "__wrapped__")) return Pe(n);
      }return new zn(n);
    }function Sn() {}function zn(n, t) {
      this.__wrapped__ = n, this.__actions__ = [], this.__chain__ = !!t, this.__index__ = 0, this.__values__ = F;
    }function Mn(n) {
      this.__wrapped__ = n, this.__actions__ = [], this.__dir__ = 1, this.__filtered__ = false, this.__iteratees__ = [], this.__takeCount__ = 4294967295, this.__views__ = [];
    }function Tn(n) {
      var t = -1,
          r = null == n ? 0 : n.length;for (this.clear(); ++t < r;) {
        var e = n[t];this.set(e[0], e[1]);
      }
    }function Nn(n) {
      var t = -1,
          r = null == n ? 0 : n.length;for (this.clear(); ++t < r;) {
        var e = n[t];this.set(e[0], e[1]);
      }
    }function Pn(n) {
      var t = -1,
          r = null == n ? 0 : n.length;for (this.clear(); ++t < r;) {
        var e = n[t];this.set(e[0], e[1]);
      }
    }function qn(n) {
      var t = -1,
          r = null == n ? 0 : n.length;for (this.__data__ = new Pn(); ++t < r;) {
        this.add(n[t]);
      }
    }function Vn(n) {
      this.size = (this.__data__ = new Nn(n)).size;
    }function Gn(n, t) {
      var r,
          e = af(n),
          u = !e && cf(n),
          i = !e && !u && sf(n),
          o = !e && !u && !i && gf(n),
          u = (e = e || u || i || o) ? E(n.length, ri) : [],
          f = u.length;for (r in n) {
        !t && !ci.call(n, r) || e && ("length" == r || i && ("offset" == r || "parent" == r) || o && ("buffer" == r || "byteLength" == r || "byteOffset" == r) || Re(r, f)) || u.push(r);
      }return u;
    }function tt(n) {
      var t = n.length;return t ? n[cr(0, t - 1)] : F;
    }function ot(n, t) {
      return Te(Mr(n), gt(t, 0, n.length));
    }function ft(n) {
      return Te(Mr(n));
    }function ct(n, t, r) {
      (r === F || hu(n[t], r)) && (r !== F || t in n) || _t(n, t, r);
    }function at(n, t, r) {
      var e = n[t];ci.call(n, t) && hu(e, r) && (r !== F || t in n) || _t(n, t, r);
    }function lt(n, t) {
      for (var r = n.length; r--;) {
        if (hu(n[r][0], t)) return r;
      }return -1;
    }function st(n, t, r, e) {
      return oo(n, function (n, u, i) {
        t(e, n, r(n), i);
      }), e;
    }function ht(n, t) {
      return n && Tr(t, Lu(t), n);
    }function pt(n, t) {
      return n && Tr(t, Uu(t), n);
    }function _t(n, t, r) {
      "__proto__" == t && Ei ? Ei(n, t, { configurable: true, enumerable: true, value: r, writable: true }) : n[t] = r;
    }function vt(n, t) {
      for (var r = -1, e = t.length, u = Hu(e), i = null == n; ++r < e;) {
        u[r] = i ? F : Wu(n, t[r]);
      }return u;
    }function gt(n, t, r) {
      return n === n && (r !== F && (n = n <= r ? n : r), t !== F && (n = n >= t ? n : t)), n;
    }function dt(n, t, r, e, i, o) {
      var f,
          c = 1 & t,
          a = 2 & t,
          l = 4 & t;if (r && (f = i ? r(n, e, i, o) : r(n)), f !== F) return f;if (!bu(n)) return n;if (e = af(n)) {
        if (f = Ee(n), !c) return Mr(n, f);
      } else {
        var s = yo(n),
            h = "[object Function]" == s || "[object GeneratorFunction]" == s;if (sf(n)) return Wr(n, c);if ("[object Object]" == s || "[object Arguments]" == s || h && !i) {
          if (f = a || h ? {} : Oe(n), !c) return a ? Fr(n, pt(f, n)) : $r(n, ht(f, n));
        } else {
          if (!Dn[s]) return i ? n : {};f = Se(n, s, dt, c);
        }
      }if (o || (o = new Vn()), i = o.get(n)) return i;o.set(n, f);var a = l ? a ? ye : de : a ? Uu : Lu,
          p = e ? F : a(n);return u(p || n, function (e, u) {
        p && (u = e, e = n[u]), at(f, u, dt(e, t, r, u, n, o));
      }), f;
    }function yt(n) {
      var t = Lu(n);return function (r) {
        return bt(r, n, t);
      };
    }function bt(n, t, r) {
      var e = r.length;if (null == n) return !e;for (n = ni(n); e--;) {
        var u = r[e],
            i = t[u],
            o = n[u];if (o === F && !(u in n) || !i(o)) return false;
      }return true;
    }function xt(n, t, r) {
      if (typeof n != "function") throw new ei("Expected a function");return jo(function () {
        n.apply(F, r);
      }, t);
    }function jt(n, t, r, e) {
      var u = -1,
          i = c,
          o = true,
          f = n.length,
          s = [],
          h = t.length;
      if (!f) return s;r && (t = l(t, S(r))), e ? (i = a, o = false) : 200 <= t.length && (i = R, o = false, t = new qn(t));n: for (; ++u < f;) {
        var p = n[u],
            _ = null == r ? p : r(p),
            p = e || 0 !== p ? p : 0;if (o && _ === _) {
          for (var v = h; v--;) {
            if (t[v] === _) continue n;
          }s.push(p);
        } else i(t, _, e) || s.push(p);
      }return s;
    }function wt(n, t) {
      var r = true;return oo(n, function (n, e, u) {
        return r = !!t(n, e, u);
      }), r;
    }function mt(n, t, r) {
      for (var e = -1, u = n.length; ++e < u;) {
        var i = n[e],
            o = t(i);if (null != o && (f === F ? o === o && !Au(o) : r(o, f))) var f = o,
            c = i;
      }return c;
    }function At(n, t) {
      var r = [];return oo(n, function (n, e, u) {
        t(n, e, u) && r.push(n);
      }), r;
    }function kt(n, t, r, e, u) {
      var i = -1,
          o = n.length;for (r || (r = Ie), u || (u = []); ++i < o;) {
        var f = n[i];0 < t && r(f) ? 1 < t ? kt(f, t - 1, r, e, u) : s(u, f) : e || (u[u.length] = f);
      }return u;
    }function Et(n, t) {
      return n && co(n, t, Lu);
    }function Ot(n, t) {
      return n && ao(n, t, Lu);
    }function St(n, t) {
      return f(t, function (t) {
        return gu(n[t]);
      });
    }function It(n, t) {
      t = Rr(t, n);for (var r = 0, e = t.length; null != n && r < e;) {
        n = n[$e(t[r++])];
      }return r && r == e ? n : F;
    }function Rt(n, t, r) {
      return t = t(n), af(n) ? t : s(t, r(n));
    }function zt(n) {
      if (null == n) n = n === F ? "[object Undefined]" : "[object Null]";else if (ki && ki in ni(n)) {
        var t = ci.call(n, ki),
            r = n[ki];try {
          n[ki] = F;var e = true;
        } catch (n) {}var u = si.call(n);e && (t ? n[ki] = r : delete n[ki]), n = u;
      } else n = si.call(n);return n;
    }function Wt(n, t) {
      return n > t;
    }function Bt(n, t) {
      return null != n && ci.call(n, t);
    }function Lt(n, t) {
      return null != n && t in ni(n);
    }function Ut(n, t, r) {
      for (var e = r ? a : c, u = n[0].length, i = n.length, o = i, f = Hu(i), s = 1 / 0, h = []; o--;) {
        var p = n[o];o && t && (p = l(p, S(t))), s = Mi(p.length, s), f[o] = !r && (t || 120 <= u && 120 <= p.length) ? new qn(o && p) : F;
      }var p = n[0],
          _ = -1,
          v = f[0];n: for (; ++_ < u && h.length < s;) {
        var g = p[_],
            d = t ? t(g) : g,
            g = r || 0 !== g ? g : 0;
        if (v ? !R(v, d) : !e(h, d, r)) {
          for (o = i; --o;) {
            var y = f[o];if (y ? !R(y, d) : !e(n[o], d, r)) continue n;
          }v && v.push(d), h.push(g);
        }
      }return h;
    }function Ct(n, t, r) {
      var e = {};return Et(n, function (n, u, i) {
        t(e, r(n), u, i);
      }), e;
    }function Dt(n, t, e) {
      return t = Rr(t, n), n = 2 > t.length ? n : It(n, vr(t, 0, -1)), t = null == n ? n : n[$e(Ge(t))], null == t ? F : r(t, n, e);
    }function Mt(n) {
      return xu(n) && "[object Arguments]" == zt(n);
    }function Tt(n) {
      return xu(n) && "[object ArrayBuffer]" == zt(n);
    }function $t(n) {
      return xu(n) && "[object Date]" == zt(n);
    }function Ft(n, t, r, e, u) {
      if (n === t) t = true;else if (null == n || null == t || !xu(n) && !xu(t)) t = n !== n && t !== t;else n: {
        var i = af(n),
            o = af(t),
            f = i ? "[object Array]" : yo(n),
            c = o ? "[object Array]" : yo(t),
            f = "[object Arguments]" == f ? "[object Object]" : f,
            c = "[object Arguments]" == c ? "[object Object]" : c,
            a = "[object Object]" == f,
            o = "[object Object]" == c;if ((c = f == c) && sf(n)) {
          if (!sf(t)) {
            t = false;break n;
          }i = true, a = false;
        }if (c && !a) u || (u = new Vn()), t = i || gf(n) ? _e(n, t, r, e, Ft, u) : ve(n, t, f, r, e, Ft, u);else {
          if (!(1 & r) && (i = a && ci.call(n, "__wrapped__"), f = o && ci.call(t, "__wrapped__"), i || f)) {
            n = i ? n.value() : n, t = f ? t.value() : t, u || (u = new Vn()), t = Ft(n, t, r, e, u);break n;
          }if (c) {
            t: if (u || (u = new Vn()), i = 1 & r, f = de(n), o = f.length, c = de(t).length, o == c || i) {
              for (a = o; a--;) {
                var l = f[a];if (!(i ? l in t : ci.call(t, l))) {
                  t = false;break t;
                }
              }if ((c = u.get(n)) && u.get(t)) t = c == t;else {
                c = true, u.set(n, t), u.set(t, n);for (var s = i; ++a < o;) {
                  var l = f[a],
                      h = n[l],
                      p = t[l];if (e) var _ = i ? e(p, h, l, t, n, u) : e(h, p, l, n, t, u);if (_ === F ? h !== p && !Ft(h, p, r, e, u) : !_) {
                    c = false;break;
                  }s || (s = "constructor" == l);
                }c && !s && (r = n.constructor, e = t.constructor, r != e && "constructor" in n && "constructor" in t && !(typeof r == "function" && r instanceof r && typeof e == "function" && e instanceof e) && (c = false)), u.delete(n), u.delete(t), t = c;
              }
            } else t = false;
          } else t = false;
        }
      }return t;
    }function Nt(n) {
      return xu(n) && "[object Map]" == yo(n);
    }function Pt(n, t, r, e) {
      var u = r.length,
          i = u,
          o = !e;if (null == n) return !i;for (n = ni(n); u--;) {
        var f = r[u];if (o && f[2] ? f[1] !== n[f[0]] : !(f[0] in n)) return false;
      }for (; ++u < i;) {
        var f = r[u],
            c = f[0],
            a = n[c],
            l = f[1];if (o && f[2]) {
          if (a === F && !(c in n)) return false;
        } else {
          if (f = new Vn(), e) var s = e(a, l, c, n, t, f);if (s === F ? !Ft(l, a, 3, e, f) : !s) return false;
        }
      }return true;
    }function Zt(n) {
      return !(!bu(n) || li && li in n) && (gu(n) ? _i : xn).test(Fe(n));
    }function qt(n) {
      return xu(n) && "[object RegExp]" == zt(n);
    }function Vt(n) {
      return xu(n) && "[object Set]" == yo(n);
    }function Kt(n) {
      return xu(n) && yu(n.length) && !!Cn[zt(n)];
    }function Gt(n) {
      return typeof n == "function" ? n : null == n ? Nu : (typeof n === "undefined" ? "undefined" : _typeof(n)) == "object" ? af(n) ? Xt(n[0], n[1]) : Qt(n) : Vu(n);
    }function Ht(n) {
      if (!Le(n)) return Ci(n);var t,
          r = [];for (t in ni(n)) {
        ci.call(n, t) && "constructor" != t && r.push(t);
      }return r;
    }function Jt(n, t) {
      return n < t;
    }function Yt(n, t) {
      var r = -1,
          e = pu(n) ? Hu(n.length) : [];return oo(n, function (n, u, i) {
        e[++r] = t(n, u, i);
      }), e;
    }function Qt(n) {
      var t = me(n);return 1 == t.length && t[0][2] ? Ue(t[0][0], t[0][1]) : function (r) {
        return r === n || Pt(r, n, t);
      };
    }function Xt(n, t) {
      return We(n) && t === t && !bu(t) ? Ue($e(n), t) : function (r) {
        var e = Wu(r, n);return e === F && e === t ? Bu(r, n) : Ft(t, e, 3);
      };
    }function nr(n, t, r, e, u) {
      n !== t && co(t, function (i, o) {
        if (bu(i)) {
          u || (u = new Vn());var f = u,
              c = n[o],
              a = t[o],
              l = f.get(a);if (l) ct(n, o, l);else {
            var l = e ? e(c, a, o + "", n, t, f) : F,
                s = l === F;if (s) {
              var h = af(a),
                  p = !h && sf(a),
                  _ = !h && !p && gf(a),
                  l = a;h || p || _ ? af(c) ? l = c : _u(c) ? l = Mr(c) : p ? (s = false, l = Wr(a, true)) : _ ? (s = false, l = Lr(a, true)) : l = [] : wu(a) || cf(a) ? (l = c, cf(c) ? l = Ru(c) : (!bu(c) || r && gu(c)) && (l = Oe(a))) : s = false;
            }s && (f.set(a, l), nr(l, a, r, e, f), f.delete(a)), ct(n, o, l);
          }
        } else f = e ? e(n[o], i, o + "", n, t, u) : F, f === F && (f = i), ct(n, o, f);
      }, Uu);
    }function tr(n, t) {
      var r = n.length;if (r) return t += 0 > t ? r : 0, Re(t, r) ? n[t] : F;
    }function rr(n, t, r) {
      var e = -1;return t = l(t.length ? t : [Nu], S(je())), n = Yt(n, function (n) {
        return { a: l(t, function (t) {
            return t(n);
          }), b: ++e, c: n };
      }), A(n, function (n, t) {
        var e;n: {
          e = -1;for (var u = n.a, i = t.a, o = u.length, f = r.length; ++e < o;) {
            var c = Ur(u[e], i[e]);if (c) {
              e = e >= f ? c : c * ("desc" == r[e] ? -1 : 1);
              break n;
            }
          }e = n.b - t.b;
        }return e;
      });
    }function er(n, t) {
      return ur(n, t, function (t, r) {
        return Bu(n, r);
      });
    }function ur(n, t, r) {
      for (var e = -1, u = t.length, i = {}; ++e < u;) {
        var o = t[e],
            f = It(n, o);r(f, o) && pr(i, Rr(o, n), f);
      }return i;
    }function ir(n) {
      return function (t) {
        return It(t, n);
      };
    }function or(n, t, r, e) {
      var u = e ? y : d,
          i = -1,
          o = t.length,
          f = n;for (n === t && (t = Mr(t)), r && (f = l(n, S(r))); ++i < o;) {
        for (var c = 0, a = t[i], a = r ? r(a) : a; -1 < (c = u(f, a, c, e));) {
          f !== n && wi.call(f, c, 1), wi.call(n, c, 1);
        }
      }return n;
    }function fr(n, t) {
      for (var r = n ? t.length : 0, e = r - 1; r--;) {
        var u = t[r];
        if (r == e || u !== i) {
          var i = u;Re(u) ? wi.call(n, u, 1) : mr(n, u);
        }
      }
    }function cr(n, t) {
      return n + zi(Fi() * (t - n + 1));
    }function ar(n, t) {
      var r = "";if (!n || 1 > t || 9007199254740991 < t) return r;do {
        t % 2 && (r += n), (t = zi(t / 2)) && (n += n);
      } while (t);return r;
    }function lr(n, t) {
      return wo(Ce(n, t, Nu), n + "");
    }function sr(n) {
      return tt(Du(n));
    }function hr(n, t) {
      var r = Du(n);return Te(r, gt(t, 0, r.length));
    }function pr(n, t, r, e) {
      if (!bu(n)) return n;t = Rr(t, n);for (var u = -1, i = t.length, o = i - 1, f = n; null != f && ++u < i;) {
        var c = $e(t[u]),
            a = r;if (u != o) {
          var l = f[c],
              a = e ? e(l, c, f) : F;
          a === F && (a = bu(l) ? l : Re(t[u + 1]) ? [] : {});
        }at(f, c, a), f = f[c];
      }return n;
    }function _r(n) {
      return Te(Du(n));
    }function vr(n, t, r) {
      var e = -1,
          u = n.length;for (0 > t && (t = -t > u ? 0 : u + t), r = r > u ? u : r, 0 > r && (r += u), u = t > r ? 0 : r - t >>> 0, t >>>= 0, r = Hu(u); ++e < u;) {
        r[e] = n[e + t];
      }return r;
    }function gr(n, t) {
      var r;return oo(n, function (n, e, u) {
        return r = t(n, e, u), !r;
      }), !!r;
    }function dr(n, t, r) {
      var e = 0,
          u = null == n ? e : n.length;if (typeof t == "number" && t === t && 2147483647 >= u) {
        for (; e < u;) {
          var i = e + u >>> 1,
              o = n[i];null !== o && !Au(o) && (r ? o <= t : o < t) ? e = i + 1 : u = i;
        }return u;
      }return yr(n, t, Nu, r);
    }function yr(n, t, r, e) {
      t = r(t);for (var u = 0, i = null == n ? 0 : n.length, o = t !== t, f = null === t, c = Au(t), a = t === F; u < i;) {
        var l = zi((u + i) / 2),
            s = r(n[l]),
            h = s !== F,
            p = null === s,
            _ = s === s,
            v = Au(s);(o ? e || _ : a ? _ && (e || h) : f ? _ && h && (e || !p) : c ? _ && h && !p && (e || !v) : p || v ? 0 : e ? s <= t : s < t) ? u = l + 1 : i = l;
      }return Mi(i, 4294967294);
    }function br(n, t) {
      for (var r = -1, e = n.length, u = 0, i = []; ++r < e;) {
        var o = n[r],
            f = t ? t(o) : o;if (!r || !hu(f, c)) {
          var c = f;i[u++] = 0 === o ? 0 : o;
        }
      }return i;
    }function xr(n) {
      return typeof n == "number" ? n : Au(n) ? P : +n;
    }function jr(n) {
      if (typeof n == "string") return n;
      if (af(n)) return l(n, jr) + "";if (Au(n)) return uo ? uo.call(n) : "";var t = n + "";return "0" == t && 1 / n == -N ? "-0" : t;
    }function wr(n, t, r) {
      var e = -1,
          u = c,
          i = n.length,
          o = true,
          f = [],
          l = f;if (r) o = false, u = a;else if (200 <= i) {
        if (u = t ? null : po(n)) return D(u);o = false, u = R, l = new qn();
      } else l = t ? [] : f;n: for (; ++e < i;) {
        var s = n[e],
            h = t ? t(s) : s,
            s = r || 0 !== s ? s : 0;if (o && h === h) {
          for (var p = l.length; p--;) {
            if (l[p] === h) continue n;
          }t && l.push(h), f.push(s);
        } else u(l, h, r) || (l !== f && l.push(h), f.push(s));
      }return f;
    }function mr(n, t) {
      return t = Rr(t, n), n = 2 > t.length ? n : It(n, vr(t, 0, -1)), null == n || delete n[$e(Ge(t))];
    }function Ar(n, t, r, e) {
      for (var u = n.length, i = e ? u : -1; (e ? i-- : ++i < u) && t(n[i], i, n);) {}return r ? vr(n, e ? 0 : i, e ? i + 1 : u) : vr(n, e ? i + 1 : 0, e ? u : i);
    }function kr(n, t) {
      var r = n;return r instanceof Mn && (r = r.value()), h(t, function (n, t) {
        return t.func.apply(t.thisArg, s([n], t.args));
      }, r);
    }function Er(n, t, r) {
      var e = n.length;if (2 > e) return e ? wr(n[0]) : [];for (var u = -1, i = Hu(e); ++u < e;) {
        for (var o = n[u], f = -1; ++f < e;) {
          f != u && (i[u] = jt(i[u] || o, n[f], t, r));
        }
      }return wr(kt(i, 1), t, r);
    }function Or(n, t, r) {
      for (var e = -1, u = n.length, i = t.length, o = {}; ++e < u;) {
        r(o, n[e], e < i ? t[e] : F);
      }return o;
    }function Sr(n) {
      return _u(n) ? n : [];
    }function Ir(n) {
      return typeof n == "function" ? n : Nu;
    }function Rr(n, t) {
      return af(n) ? n : We(n, t) ? [n] : mo(zu(n));
    }function zr(n, t, r) {
      var e = n.length;return r = r === F ? e : r, !t && r >= e ? n : vr(n, t, r);
    }function Wr(n, t) {
      if (t) return n.slice();var r = n.length,
          r = yi ? yi(r) : new n.constructor(r);return n.copy(r), r;
    }function Br(n) {
      var t = new n.constructor(n.byteLength);return new di(t).set(new di(n)), t;
    }function Lr(n, t) {
      return new n.constructor(t ? Br(n.buffer) : n.buffer, n.byteOffset, n.length);
    }function Ur(n, t) {
      if (n !== t) {
        var r = n !== F,
            e = null === n,
            u = n === n,
            i = Au(n),
            o = t !== F,
            f = null === t,
            c = t === t,
            a = Au(t);if (!f && !a && !i && n > t || i && o && c && !f && !a || e && o && c || !r && c || !u) return 1;if (!e && !i && !a && n < t || a && r && u && !e && !i || f && r && u || !o && u || !c) return -1;
      }return 0;
    }function Cr(n, t, r, e) {
      var u = -1,
          i = n.length,
          o = r.length,
          f = -1,
          c = t.length,
          a = Di(i - o, 0),
          l = Hu(c + a);for (e = !e; ++f < c;) {
        l[f] = t[f];
      }for (; ++u < o;) {
        (e || u < i) && (l[r[u]] = n[u]);
      }for (; a--;) {
        l[f++] = n[u++];
      }return l;
    }function Dr(n, t, r, e) {
      var u = -1,
          i = n.length,
          o = -1,
          f = r.length,
          c = -1,
          a = t.length,
          l = Di(i - f, 0),
          s = Hu(l + a);
      for (e = !e; ++u < l;) {
        s[u] = n[u];
      }for (l = u; ++c < a;) {
        s[l + c] = t[c];
      }for (; ++o < f;) {
        (e || u < i) && (s[l + r[o]] = n[u++]);
      }return s;
    }function Mr(n, t) {
      var r = -1,
          e = n.length;for (t || (t = Hu(e)); ++r < e;) {
        t[r] = n[r];
      }return t;
    }function Tr(n, t, r, e) {
      var u = !r;r || (r = {});for (var i = -1, o = t.length; ++i < o;) {
        var f = t[i],
            c = e ? e(r[f], n[f], f, r, n) : F;c === F && (c = n[f]), u ? _t(r, f, c) : at(r, f, c);
      }return r;
    }function $r(n, t) {
      return Tr(n, vo(n), t);
    }function Fr(n, t) {
      return Tr(n, go(n), t);
    }function Nr(n, t) {
      return function (r, u) {
        var i = af(r) ? e : st,
            o = t ? t() : {};return i(r, n, je(u, 2), o);
      };
    }function Pr(n) {
      return lr(function (t, r) {
        var e = -1,
            u = r.length,
            i = 1 < u ? r[u - 1] : F,
            o = 2 < u ? r[2] : F,
            i = 3 < n.length && typeof i == "function" ? (u--, i) : F;for (o && ze(r[0], r[1], o) && (i = 3 > u ? F : i, u = 1), t = ni(t); ++e < u;) {
          (o = r[e]) && n(t, o, e, i);
        }return t;
      });
    }function Zr(n, t) {
      return function (r, e) {
        if (null == r) return r;if (!pu(r)) return n(r, e);for (var u = r.length, i = t ? u : -1, o = ni(r); (t ? i-- : ++i < u) && false !== e(o[i], i, o);) {}return r;
      };
    }function qr(n) {
      return function (t, r, e) {
        var u = -1,
            i = ni(t);e = e(t);for (var o = e.length; o--;) {
          var f = e[n ? o : ++u];if (false === r(i[f], f, i)) break;
        }return t;
      };
    }function Vr(n, t, r) {
      function e() {
        return (this && this !== Zn && this instanceof e ? i : n).apply(u ? r : this, arguments);
      }var u = 1 & t,
          i = Hr(n);return e;
    }function Kr(n) {
      return function (t) {
        t = zu(t);var r = Bn.test(t) ? $(t) : F,
            e = r ? r[0] : t.charAt(0);return t = r ? zr(r, 1).join("") : t.slice(1), e[n]() + t;
      };
    }function Gr(n) {
      return function (t) {
        return h($u(Tu(t).replace(In, "")), n, "");
      };
    }function Hr(n) {
      return function () {
        var t = arguments;switch (t.length) {case 0:
            return new n();case 1:
            return new n(t[0]);case 2:
            return new n(t[0], t[1]);case 3:
            return new n(t[0], t[1], t[2]);case 4:
            return new n(t[0], t[1], t[2], t[3]);case 5:
            return new n(t[0], t[1], t[2], t[3], t[4]);case 6:
            return new n(t[0], t[1], t[2], t[3], t[4], t[5]);case 7:
            return new n(t[0], t[1], t[2], t[3], t[4], t[5], t[6]);}var r = io(n.prototype),
            t = n.apply(r, t);return bu(t) ? t : r;
      };
    }function Jr(n, t, e) {
      function u() {
        for (var o = arguments.length, f = Hu(o), c = o, a = xe(u); c--;) {
          f[c] = arguments[c];
        }return c = 3 > o && f[0] !== a && f[o - 1] !== a ? [] : C(f, a), o -= c.length, o < e ? fe(n, t, Xr, u.placeholder, F, f, c, F, F, e - o) : r(this && this !== Zn && this instanceof u ? i : n, this, f);
      }var i = Hr(n);return u;
    }function Yr(n) {
      return function (t, r, e) {
        var u = ni(t);if (!pu(t)) {
          var i = je(r, 3);t = Lu(t), r = function r(n) {
            return i(u[n], n, u);
          };
        }return r = n(t, r, e), -1 < r ? u[i ? t[r] : r] : F;
      };
    }function Qr(n) {
      return ge(function (t) {
        var r = t.length,
            e = r,
            u = zn.prototype.thru;for (n && t.reverse(); e--;) {
          var i = t[e];if (typeof i != "function") throw new ei("Expected a function");if (u && !o && "wrapper" == be(i)) var o = new zn([], true);
        }for (e = o ? e : r; ++e < r;) {
          var i = t[e],
              u = be(i),
              f = "wrapper" == u ? _o(i) : F,
              o = f && Be(f[0]) && 424 == f[1] && !f[4].length && 1 == f[9] ? o[be(f[0])].apply(o, f[3]) : 1 == i.length && Be(i) ? o[u]() : o.thru(i);
        }return function () {
          var n = arguments,
              e = n[0];if (o && 1 == n.length && af(e)) return o.plant(e).value();for (var u = 0, n = r ? t[u].apply(this, n) : e; ++u < r;) {
            n = t[u].call(this, n);
          }return n;
        };
      });
    }function Xr(n, t, r, e, u, i, o, f, c, a) {
      function l() {
        for (var d = arguments.length, y = Hu(d), b = d; b--;) {
          y[b] = arguments[b];
        }if (_) {
          var x,
              j = xe(l),
              b = y.length;for (x = 0; b--;) {
            y[b] === j && ++x;
          }
        }if (e && (y = Cr(y, e, u, _)), i && (y = Dr(y, i, o, _)), d -= x, _ && d < a) return j = C(y, j), fe(n, t, Xr, l.placeholder, r, y, j, f, c, a - d);if (j = h ? r : this, b = p ? j[n] : n, d = y.length, f) {
          x = y.length;for (var w = Mi(f.length, x), m = Mr(y); w--;) {
            var A = f[w];y[w] = Re(A, x) ? m[A] : F;
          }
        } else v && 1 < d && y.reverse();return s && c < d && (y.length = c), this && this !== Zn && this instanceof l && (b = g || Hr(b)), b.apply(j, y);
      }var s = 128 & t,
          h = 1 & t,
          p = 2 & t,
          _ = 24 & t,
          v = 512 & t,
          g = p ? F : Hr(n);return l;
    }function ne(n, t) {
      return function (r, e) {
        return Ct(r, n, t(e));
      };
    }function te(n, t) {
      return function (r, e) {
        var u;if (r === F && e === F) return t;if (r !== F && (u = r), e !== F) {
          if (u === F) return e;typeof r == "string" || typeof e == "string" ? (r = jr(r), e = jr(e)) : (r = xr(r), e = xr(e)), u = n(r, e);
        }return u;
      };
    }function re(n) {
      return ge(function (t) {
        return t = l(t, S(je())), lr(function (e) {
          var u = this;return n(t, function (n) {
            return r(n, u, e);
          });
        });
      });
    }function ee(n, t) {
      t = t === F ? " " : jr(t);var r = t.length;return 2 > r ? r ? ar(t, n) : t : (r = ar(t, Ri(n / T(t))), Bn.test(t) ? zr($(r), 0, n).join("") : r.slice(0, n));
    }function ue(n, t, e, u) {
      function i() {
        for (var t = -1, c = arguments.length, a = -1, l = u.length, s = Hu(l + c), h = this && this !== Zn && this instanceof i ? f : n; ++a < l;) {
          s[a] = u[a];
        }for (; c--;) {
          s[a++] = arguments[++t];
        }return r(h, o ? e : this, s);
      }var o = 1 & t,
          f = Hr(n);return i;
    }function ie(n) {
      return function (t, r, e) {
        e && typeof e != "number" && ze(t, r, e) && (r = e = F), t = Eu(t), r === F ? (r = t, t = 0) : r = Eu(r), e = e === F ? t < r ? 1 : -1 : Eu(e);var u = -1;r = Di(Ri((r - t) / (e || 1)), 0);for (var i = Hu(r); r--;) {
          i[n ? r : ++u] = t, t += e;
        }return i;
      };
    }function oe(n) {
      return function (t, r) {
        return typeof t == "string" && typeof r == "string" || (t = Iu(t), r = Iu(r)), n(t, r);
      };
    }function fe(n, t, r, e, u, i, o, f, c, a) {
      var l = 8 & t,
          s = l ? o : F;o = l ? F : o;var h = l ? i : F;return i = l ? F : i, t = (t | (l ? 32 : 64)) & ~(l ? 64 : 32), 4 & t || (t &= -4), u = [n, t, u, h, s, i, o, f, c, a], r = r.apply(F, u), Be(n) && xo(r, u), r.placeholder = e, De(r, n, t);
    }function ce(n) {
      var t = Xu[n];return function (n, r) {
        if (n = Iu(n), r = null == r ? 0 : Mi(Ou(r), 292)) {
          var e = (zu(n) + "e").split("e"),
              e = t(e[0] + "e" + (+e[1] + r)),
              e = (zu(e) + "e").split("e");return +(e[0] + "e" + (+e[1] - r));
        }return t(n);
      };
    }function ae(n) {
      return function (t) {
        var r = yo(t);return "[object Map]" == r ? L(t) : "[object Set]" == r ? M(t) : O(t, n(t));
      };
    }function le(n, t, r, e, u, i, o, f) {
      var c = 2 & t;if (!c && typeof n != "function") throw new ei("Expected a function");var a = e ? e.length : 0;if (a || (t &= -97, e = u = F), o = o === F ? o : Di(Ou(o), 0), f = f === F ? f : Ou(f), a -= u ? u.length : 0, 64 & t) {
        var l = e,
            s = u;e = u = F;
      }var h = c ? F : _o(n);return i = [n, t, r, e, u, l, s, i, o, f], h && (r = i[1], n = h[1], t = r | n, e = 128 == n && 8 == r || 128 == n && 256 == r && i[7].length <= h[8] || 384 == n && h[7].length <= h[8] && 8 == r, 131 > t || e) && (1 & n && (i[2] = h[2], t |= 1 & r ? 0 : 4), (r = h[3]) && (e = i[3], i[3] = e ? Cr(e, r, h[4]) : r, i[4] = e ? C(i[3], "__lodash_placeholder__") : h[4]), (r = h[5]) && (e = i[5], i[5] = e ? Dr(e, r, h[6]) : r, i[6] = e ? C(i[5], "__lodash_placeholder__") : h[6]), (r = h[7]) && (i[7] = r), 128 & n && (i[8] = null == i[8] ? h[8] : Mi(i[8], h[8])), null == i[9] && (i[9] = h[9]), i[0] = h[0], i[1] = t), n = i[0], t = i[1], r = i[2], e = i[3], u = i[4], f = i[9] = i[9] === F ? c ? 0 : n.length : Di(i[9] - a, 0), !f && 24 & t && (t &= -25), De((h ? lo : xo)(t && 1 != t ? 8 == t || 16 == t ? Jr(n, t, f) : 32 != t && 33 != t || u.length ? Xr.apply(F, i) : ue(n, t, r, e) : Vr(n, t, r), i), n, t);
    }function se(n, t, r, e) {
      return n === F || hu(n, ii[r]) && !ci.call(e, r) ? t : n;
    }function he(n, t, r, e, u, i) {
      return bu(n) && bu(t) && (i.set(t, n), nr(n, t, F, he, i), i.delete(t)), n;
    }function pe(n) {
      return wu(n) ? F : n;
    }function _e(n, t, r, e, u, i) {
      var o = 1 & r,
          f = n.length,
          c = t.length;if (f != c && !(o && c > f)) return false;if ((c = i.get(n)) && i.get(t)) return c == t;var c = -1,
          a = true,
          l = 2 & r ? new qn() : F;
      for (i.set(n, t), i.set(t, n); ++c < f;) {
        var s = n[c],
            h = t[c];if (e) var p = o ? e(h, s, c, t, n, i) : e(s, h, c, n, t, i);if (p !== F) {
          if (p) continue;a = false;break;
        }if (l) {
          if (!_(t, function (n, t) {
            if (!R(l, t) && (s === n || u(s, n, r, e, i))) return l.push(t);
          })) {
            a = false;break;
          }
        } else if (s !== h && !u(s, h, r, e, i)) {
          a = false;break;
        }
      }return i.delete(n), i.delete(t), a;
    }function ve(n, t, r, e, u, i, o) {
      switch (r) {case "[object DataView]":
          if (n.byteLength != t.byteLength || n.byteOffset != t.byteOffset) break;n = n.buffer, t = t.buffer;case "[object ArrayBuffer]":
          if (n.byteLength != t.byteLength || !i(new di(n), new di(t))) break;
          return true;case "[object Boolean]":case "[object Date]":case "[object Number]":
          return hu(+n, +t);case "[object Error]":
          return n.name == t.name && n.message == t.message;case "[object RegExp]":case "[object String]":
          return n == t + "";case "[object Map]":
          var f = L;case "[object Set]":
          if (f || (f = D), n.size != t.size && !(1 & e)) break;return (r = o.get(n)) ? r == t : (e |= 2, o.set(n, t), t = _e(f(n), f(t), e, u, i, o), o.delete(n), t);case "[object Symbol]":
          if (eo) return eo.call(n) == eo.call(t);}return false;
    }function ge(n) {
      return wo(Ce(n, F, Ve), n + "");
    }function de(n) {
      return Rt(n, Lu, vo);
    }function ye(n) {
      return Rt(n, Uu, go);
    }function be(n) {
      for (var t = n.name + "", r = Ji[t], e = ci.call(Ji, t) ? r.length : 0; e--;) {
        var u = r[e],
            i = u.func;if (null == i || i == n) return u.name;
      }return t;
    }function xe(n) {
      return (ci.call(On, "placeholder") ? On : n).placeholder;
    }function je() {
      var n = On.iteratee || Pu,
          n = n === Pu ? Gt : n;return arguments.length ? n(arguments[0], arguments[1]) : n;
    }function we(n, t) {
      var r = n.__data__,
          e = typeof t === "undefined" ? "undefined" : _typeof(t);return ("string" == e || "number" == e || "symbol" == e || "boolean" == e ? "__proto__" !== t : null === t) ? r[typeof t == "string" ? "string" : "hash"] : r.map;
    }function me(n) {
      for (var t = Lu(n), r = t.length; r--;) {
        var e = t[r],
            u = n[e];t[r] = [e, u, u === u && !bu(u)];
      }return t;
    }function Ae(n, t) {
      var r = null == n ? F : n[t];return Zt(r) ? r : F;
    }function ke(n, t, r) {
      t = Rr(t, n);for (var e = -1, u = t.length, i = false; ++e < u;) {
        var o = $e(t[e]);if (!(i = null != n && r(n, o))) break;n = n[o];
      }return i || ++e != u ? i : (u = null == n ? 0 : n.length, !!u && yu(u) && Re(o, u) && (af(n) || cf(n)));
    }function Ee(n) {
      var t = n.length,
          r = n.constructor(t);return t && "string" == typeof n[0] && ci.call(n, "index") && (r.index = n.index, r.input = n.input), r;
    }function Oe(n) {
      return typeof n.constructor != "function" || Le(n) ? {} : io(bi(n));
    }function Se(r, e, u, i) {
      var o = r.constructor;switch (e) {case "[object ArrayBuffer]":
          return Br(r);case "[object Boolean]":case "[object Date]":
          return new o(+r);case "[object DataView]":
          return e = i ? Br(r.buffer) : r.buffer, new r.constructor(e, r.byteOffset, r.byteLength);case "[object Float32Array]":case "[object Float64Array]":case "[object Int8Array]":case "[object Int16Array]":case "[object Int32Array]":case "[object Uint8Array]":case "[object Uint8ClampedArray]":
        case "[object Uint16Array]":case "[object Uint32Array]":
          return Lr(r, i);case "[object Map]":
          return e = i ? u(L(r), 1) : L(r), h(e, n, new r.constructor());case "[object Number]":case "[object String]":
          return new o(r);case "[object RegExp]":
          return e = new r.constructor(r.source, dn.exec(r)), e.lastIndex = r.lastIndex, e;case "[object Set]":
          return e = i ? u(D(r), 1) : D(r), h(e, t, new r.constructor());case "[object Symbol]":
          return eo ? ni(eo.call(r)) : {};}
    }function Ie(n) {
      return af(n) || cf(n) || !!(mi && n && n[mi]);
    }function Re(n, t) {
      return t = null == t ? 9007199254740991 : t, !!t && (typeof n == "number" || wn.test(n)) && -1 < n && 0 == n % 1 && n < t;
    }function ze(n, t, r) {
      if (!bu(r)) return false;var e = typeof t === "undefined" ? "undefined" : _typeof(t);return !!("number" == e ? pu(r) && Re(t, r.length) : "string" == e && t in r) && hu(r[t], n);
    }function We(n, t) {
      if (af(n)) return false;var r = typeof n === "undefined" ? "undefined" : _typeof(n);return !("number" != r && "symbol" != r && "boolean" != r && null != n && !Au(n)) || rn.test(n) || !tn.test(n) || null != t && n in ni(t);
    }function Be(n) {
      var t = be(n),
          r = On[t];return typeof r == "function" && t in Mn.prototype && (n === r || (t = _o(r), !!t && n === t[0]));
    }function Le(n) {
      var t = n && n.constructor;
      return n === (typeof t == "function" && t.prototype || ii);
    }function Ue(n, t) {
      return function (r) {
        return null != r && r[n] === t && (t !== F || n in ni(r));
      };
    }function Ce(n, t, e) {
      return t = Di(t === F ? n.length - 1 : t, 0), function () {
        for (var u = arguments, i = -1, o = Di(u.length - t, 0), f = Hu(o); ++i < o;) {
          f[i] = u[t + i];
        }for (i = -1, o = Hu(t + 1); ++i < t;) {
          o[i] = u[i];
        }return o[t] = e(f), r(n, this, o);
      };
    }function De(n, t, r) {
      var e = t + "";t = wo;var u,
          i = Ne;return u = (u = e.match(hn)) ? u[1].split(pn) : [], r = i(u, r), (i = r.length) && (u = i - 1, r[u] = (1 < i ? "& " : "") + r[u], r = r.join(2 < i ? ", " : " "), e = e.replace(sn, "{\n/* [wrapped with " + r + "] */\n")), t(n, e);
    }function Me(n) {
      var t = 0,
          r = 0;return function () {
        var e = Ti(),
            u = 16 - (e - r);if (r = e, 0 < u) {
          if (800 <= ++t) return arguments[0];
        } else t = 0;return n.apply(F, arguments);
      };
    }function Te(n, t) {
      var r = -1,
          e = n.length,
          u = e - 1;for (t = t === F ? e : t; ++r < t;) {
        var e = cr(r, u),
            i = n[e];n[e] = n[r], n[r] = i;
      }return n.length = t, n;
    }function $e(n) {
      if (typeof n == "string" || Au(n)) return n;var t = n + "";return "0" == t && 1 / n == -N ? "-0" : t;
    }function Fe(n) {
      if (null != n) {
        try {
          return fi.call(n);
        } catch (n) {}return n + "";
      }return "";
    }function Ne(n, t) {
      return u(Z, function (r) {
        var e = "_." + r[0];t & r[1] && !c(n, e) && n.push(e);
      }), n.sort();
    }function Pe(n) {
      if (n instanceof Mn) return n.clone();var t = new zn(n.__wrapped__, n.__chain__);return t.__actions__ = Mr(n.__actions__), t.__index__ = n.__index__, t.__values__ = n.__values__, t;
    }function Ze(n, t, r) {
      var e = null == n ? 0 : n.length;return e ? (r = null == r ? 0 : Ou(r), 0 > r && (r = Di(e + r, 0)), g(n, je(t, 3), r)) : -1;
    }function qe(n, t, r) {
      var e = null == n ? 0 : n.length;if (!e) return -1;var u = e - 1;return r !== F && (u = Ou(r), u = 0 > r ? Di(e + u, 0) : Mi(u, e - 1)), g(n, je(t, 3), u, true);
    }function Ve(n) {
      return (null == n ? 0 : n.length) ? kt(n, 1) : [];
    }function Ke(n) {
      return n && n.length ? n[0] : F;
    }function Ge(n) {
      var t = null == n ? 0 : n.length;return t ? n[t - 1] : F;
    }function He(n, t) {
      return n && n.length && t && t.length ? or(n, t) : n;
    }function Je(n) {
      return null == n ? n : Ni.call(n);
    }function Ye(n) {
      if (!n || !n.length) return [];var t = 0;return n = f(n, function (n) {
        if (_u(n)) return t = Di(n.length, t), true;
      }), E(t, function (t) {
        return l(n, j(t));
      });
    }function Qe(n, t) {
      if (!n || !n.length) return [];var e = Ye(n);return null == t ? e : l(e, function (n) {
        return r(t, F, n);
      });
    }function Xe(n) {
      return n = On(n), n.__chain__ = true, n;
    }function nu(n, t) {
      return t(n);
    }function tu() {
      return this;
    }function ru(n, t) {
      return (af(n) ? u : oo)(n, je(t, 3));
    }function eu(n, t) {
      return (af(n) ? i : fo)(n, je(t, 3));
    }function uu(n, t) {
      return (af(n) ? l : Yt)(n, je(t, 3));
    }function iu(n, t, r) {
      return t = r ? F : t, t = n && null == t ? n.length : t, le(n, 128, F, F, F, F, t);
    }function ou(n, t) {
      var r;if (typeof t != "function") throw new ei("Expected a function");return n = Ou(n), function () {
        return 0 < --n && (r = t.apply(this, arguments)), 1 >= n && (t = F), r;
      };
    }function fu(n, t, r) {
      return t = r ? F : t, n = le(n, 8, F, F, F, F, F, t), n.placeholder = fu.placeholder, n;
    }function cu(n, t, r) {
      return t = r ? F : t, n = le(n, 16, F, F, F, F, F, t), n.placeholder = cu.placeholder, n;
    }function au(n, t, r) {
      function e(t) {
        var r = c,
            e = a;return c = a = F, _ = t, s = n.apply(e, r);
      }function u(n) {
        var r = n - p;return n -= _, p === F || r >= t || 0 > r || g && n >= l;
      }function i() {
        var n = Jo();if (u(n)) return o(n);var r,
            e = jo;r = n - _, n = t - (n - p), r = g ? Mi(n, l - r) : n, h = e(i, r);
      }function o(n) {
        return h = F, d && c ? e(n) : (c = a = F, s);
      }function f() {
        var n = Jo(),
            r = u(n);if (c = arguments, a = this, p = n, r) {
          if (h === F) return _ = n = p, h = jo(i, t), v ? e(n) : s;if (g) return h = jo(i, t), e(p);
        }return h === F && (h = jo(i, t)), s;
      }var c,
          a,
          l,
          s,
          h,
          p,
          _ = 0,
          v = false,
          g = false,
          d = true;if (typeof n != "function") throw new ei("Expected a function");return t = Iu(t) || 0, bu(r) && (v = !!r.leading, l = (g = "maxWait" in r) ? Di(Iu(r.maxWait) || 0, t) : l, d = "trailing" in r ? !!r.trailing : d), f.cancel = function () {
        h !== F && ho(h), _ = 0, c = p = a = h = F;
      }, f.flush = function () {
        return h === F ? s : o(Jo());
      }, f;
    }function lu(n, t) {
      function r() {
        var e = arguments,
            u = t ? t.apply(this, e) : e[0],
            i = r.cache;return i.has(u) ? i.get(u) : (e = n.apply(this, e), r.cache = i.set(u, e) || i, e);
      }if (typeof n != "function" || null != t && typeof t != "function") throw new ei("Expected a function");return r.cache = new (lu.Cache || Pn)(), r;
    }function su(n) {
      if (typeof n != "function") throw new ei("Expected a function");return function () {
        var t = arguments;switch (t.length) {case 0:
            return !n.call(this);case 1:
            return !n.call(this, t[0]);case 2:
            return !n.call(this, t[0], t[1]);case 3:
            return !n.call(this, t[0], t[1], t[2]);}return !n.apply(this, t);
      };
    }function hu(n, t) {
      return n === t || n !== n && t !== t;
    }function pu(n) {
      return null != n && yu(n.length) && !gu(n);
    }function _u(n) {
      return xu(n) && pu(n);
    }function vu(n) {
      if (!xu(n)) return false;var t = zt(n);return "[object Error]" == t || "[object DOMException]" == t || typeof n.message == "string" && typeof n.name == "string" && !wu(n);
    }function gu(n) {
      return !!bu(n) && (n = zt(n), "[object Function]" == n || "[object GeneratorFunction]" == n || "[object AsyncFunction]" == n || "[object Proxy]" == n);
    }function du(n) {
      return typeof n == "number" && n == Ou(n);
    }function yu(n) {
      return typeof n == "number" && -1 < n && 0 == n % 1 && 9007199254740991 >= n;
    }function bu(n) {
      var t = typeof n === "undefined" ? "undefined" : _typeof(n);return null != n && ("object" == t || "function" == t);
    }function xu(n) {
      return null != n && (typeof n === "undefined" ? "undefined" : _typeof(n)) == "object";
    }function ju(n) {
      return typeof n == "number" || xu(n) && "[object Number]" == zt(n);
    }function wu(n) {
      return !(!xu(n) || "[object Object]" != zt(n)) && (n = bi(n), null === n || (n = ci.call(n, "constructor") && n.constructor, typeof n == "function" && n instanceof n && fi.call(n) == hi));
    }function mu(n) {
      return typeof n == "string" || !af(n) && xu(n) && "[object String]" == zt(n);
    }function Au(n) {
      return (typeof n === "undefined" ? "undefined" : _typeof(n)) == "symbol" || xu(n) && "[object Symbol]" == zt(n);
    }function ku(n) {
      if (!n) return [];if (pu(n)) return mu(n) ? $(n) : Mr(n);
      if (Ai && n[Ai]) {
        n = n[Ai]();for (var t, r = []; !(t = n.next()).done;) {
          r.push(t.value);
        }return r;
      }return t = yo(n), ("[object Map]" == t ? L : "[object Set]" == t ? D : Du)(n);
    }function Eu(n) {
      return n ? (n = Iu(n), n === N || n === -N ? 1.7976931348623157e308 * (0 > n ? -1 : 1) : n === n ? n : 0) : 0 === n ? n : 0;
    }function Ou(n) {
      n = Eu(n);var t = n % 1;return n === n ? t ? n - t : n : 0;
    }function Su(n) {
      return n ? gt(Ou(n), 0, 4294967295) : 0;
    }function Iu(n) {
      if (typeof n == "number") return n;if (Au(n)) return P;if (bu(n) && (n = typeof n.valueOf == "function" ? n.valueOf() : n, n = bu(n) ? n + "" : n), typeof n != "string") return 0 === n ? n : +n;
      n = n.replace(cn, "");var t = bn.test(n);return t || jn.test(n) ? Fn(n.slice(2), t ? 2 : 8) : yn.test(n) ? P : +n;
    }function Ru(n) {
      return Tr(n, Uu(n));
    }function zu(n) {
      return null == n ? "" : jr(n);
    }function Wu(n, t, r) {
      return n = null == n ? F : It(n, t), n === F ? r : n;
    }function Bu(n, t) {
      return null != n && ke(n, t, Lt);
    }function Lu(n) {
      return pu(n) ? Gn(n) : Ht(n);
    }function Uu(n) {
      if (pu(n)) n = Gn(n, true);else if (bu(n)) {
        var t,
            r = Le(n),
            e = [];for (t in n) {
          ("constructor" != t || !r && ci.call(n, t)) && e.push(t);
        }n = e;
      } else {
        if (t = [], null != n) for (r in ni(n)) {
          t.push(r);
        }n = t;
      }return n;
    }function Cu(n, t) {
      if (null == n) return {};var r = l(ye(n), function (n) {
        return [n];
      });return t = je(t), ur(n, r, function (n, r) {
        return t(n, r[0]);
      });
    }function Du(n) {
      return null == n ? [] : I(n, Lu(n));
    }function Mu(n) {
      return Nf(zu(n).toLowerCase());
    }function Tu(n) {
      return (n = zu(n)) && n.replace(mn, rt).replace(Rn, "");
    }function $u(n, t, r) {
      return n = zu(n), t = r ? F : t, t === F ? Ln.test(n) ? n.match(Wn) || [] : n.match(_n) || [] : n.match(t) || [];
    }function Fu(n) {
      return function () {
        return n;
      };
    }function Nu(n) {
      return n;
    }function Pu(n) {
      return Gt(typeof n == "function" ? n : dt(n, 1));
    }function Zu(n, t, r) {
      var e = Lu(t),
          i = St(t, e);null != r || bu(t) && (i.length || !e.length) || (r = t, t = n, n = this, i = St(t, Lu(t)));var o = !(bu(r) && "chain" in r && !r.chain),
          f = gu(n);return u(i, function (r) {
        var e = t[r];n[r] = e, f && (n.prototype[r] = function () {
          var t = this.__chain__;if (o || t) {
            var r = n(this.__wrapped__);return (r.__actions__ = Mr(this.__actions__)).push({ func: e, args: arguments, thisArg: n }), r.__chain__ = t, r;
          }return e.apply(n, s([this.value()], arguments));
        });
      }), n;
    }function qu() {}function Vu(n) {
      return We(n) ? j($e(n)) : ir(n);
    }function Ku() {
      return [];
    }function Gu() {
      return false;
    }En = null == En ? Zn : it.defaults(Zn.Object(), En, it.pick(Zn, Un));var Hu = En.Array,
        Ju = En.Date,
        Yu = En.Error,
        Qu = En.Function,
        Xu = En.Math,
        ni = En.Object,
        ti = En.RegExp,
        ri = En.String,
        ei = En.TypeError,
        ui = Hu.prototype,
        ii = ni.prototype,
        oi = En["__core-js_shared__"],
        fi = Qu.prototype.toString,
        ci = ii.hasOwnProperty,
        ai = 0,
        li = function () {
      var n = /[^.]+$/.exec(oi && oi.keys && oi.keys.IE_PROTO || "");return n ? "Symbol(src)_1." + n : "";
    }(),
        si = ii.toString,
        hi = fi.call(ni),
        pi = Zn._,
        _i = ti("^" + fi.call(ci).replace(on, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$"),
        vi = Kn ? En.Buffer : F,
        gi = En.Symbol,
        di = En.Uint8Array,
        yi = vi ? vi.f : F,
        bi = U(ni.getPrototypeOf, ni),
        xi = ni.create,
        ji = ii.propertyIsEnumerable,
        wi = ui.splice,
        mi = gi ? gi.isConcatSpreadable : F,
        Ai = gi ? gi.iterator : F,
        ki = gi ? gi.toStringTag : F,
        Ei = function () {
      try {
        var n = Ae(ni, "defineProperty");return n({}, "", {}), n;
      } catch (n) {}
    }(),
        Oi = En.clearTimeout !== Zn.clearTimeout && En.clearTimeout,
        Si = Ju && Ju.now !== Zn.Date.now && Ju.now,
        Ii = En.setTimeout !== Zn.setTimeout && En.setTimeout,
        Ri = Xu.ceil,
        zi = Xu.floor,
        Wi = ni.getOwnPropertySymbols,
        Bi = vi ? vi.isBuffer : F,
        Li = En.isFinite,
        Ui = ui.join,
        Ci = U(ni.keys, ni),
        Di = Xu.max,
        Mi = Xu.min,
        Ti = Ju.now,
        $i = En.parseInt,
        Fi = Xu.random,
        Ni = ui.reverse,
        Pi = Ae(En, "DataView"),
        Zi = Ae(En, "Map"),
        qi = Ae(En, "Promise"),
        Vi = Ae(En, "Set"),
        Ki = Ae(En, "WeakMap"),
        Gi = Ae(ni, "create"),
        Hi = Ki && new Ki(),
        Ji = {},
        Yi = Fe(Pi),
        Qi = Fe(Zi),
        Xi = Fe(qi),
        no = Fe(Vi),
        to = Fe(Ki),
        ro = gi ? gi.prototype : F,
        eo = ro ? ro.valueOf : F,
        uo = ro ? ro.toString : F,
        io = function () {
      function n() {}return function (t) {
        return bu(t) ? xi ? xi(t) : (n.prototype = t, t = new n(), n.prototype = F, t) : {};
      };
    }();On.templateSettings = { escape: Q, evaluate: X, interpolate: nn, variable: "", imports: { _: On } }, On.prototype = Sn.prototype, On.prototype.constructor = On, zn.prototype = io(Sn.prototype), zn.prototype.constructor = zn, Mn.prototype = io(Sn.prototype), Mn.prototype.constructor = Mn, Tn.prototype.clear = function () {
      this.__data__ = Gi ? Gi(null) : {}, this.size = 0;
    }, Tn.prototype.delete = function (n) {
      return n = this.has(n) && delete this.__data__[n], this.size -= n ? 1 : 0, n;
    }, Tn.prototype.get = function (n) {
      var t = this.__data__;return Gi ? (n = t[n], "__lodash_hash_undefined__" === n ? F : n) : ci.call(t, n) ? t[n] : F;
    }, Tn.prototype.has = function (n) {
      var t = this.__data__;return Gi ? t[n] !== F : ci.call(t, n);
    }, Tn.prototype.set = function (n, t) {
      var r = this.__data__;return this.size += this.has(n) ? 0 : 1, r[n] = Gi && t === F ? "__lodash_hash_undefined__" : t, this;
    }, Nn.prototype.clear = function () {
      this.__data__ = [], this.size = 0;
    }, Nn.prototype.delete = function (n) {
      var t = this.__data__;return n = lt(t, n), !(0 > n) && (n == t.length - 1 ? t.pop() : wi.call(t, n, 1), --this.size, true);
    }, Nn.prototype.get = function (n) {
      var t = this.__data__;return n = lt(t, n), 0 > n ? F : t[n][1];
    }, Nn.prototype.has = function (n) {
      return -1 < lt(this.__data__, n);
    }, Nn.prototype.set = function (n, t) {
      var r = this.__data__,
          e = lt(r, n);return 0 > e ? (++this.size, r.push([n, t])) : r[e][1] = t, this;
    }, Pn.prototype.clear = function () {
      this.size = 0, this.__data__ = { hash: new Tn(), map: new (Zi || Nn)(), string: new Tn() };
    }, Pn.prototype.delete = function (n) {
      return n = we(this, n).delete(n), this.size -= n ? 1 : 0, n;
    }, Pn.prototype.get = function (n) {
      return we(this, n).get(n);
    }, Pn.prototype.has = function (n) {
      return we(this, n).has(n);
    }, Pn.prototype.set = function (n, t) {
      var r = we(this, n),
          e = r.size;return r.set(n, t), this.size += r.size == e ? 0 : 1, this;
    }, qn.prototype.add = qn.prototype.push = function (n) {
      return this.__data__.set(n, "__lodash_hash_undefined__"), this;
    }, qn.prototype.has = function (n) {
      return this.__data__.has(n);
    }, Vn.prototype.clear = function () {
      this.__data__ = new Nn(), this.size = 0;
    }, Vn.prototype.delete = function (n) {
      var t = this.__data__;return n = t.delete(n), this.size = t.size, n;
    }, Vn.prototype.get = function (n) {
      return this.__data__.get(n);
    }, Vn.prototype.has = function (n) {
      return this.__data__.has(n);
    }, Vn.prototype.set = function (n, t) {
      var r = this.__data__;if (r instanceof Nn) {
        var e = r.__data__;if (!Zi || 199 > e.length) return e.push([n, t]), this.size = ++r.size, this;r = this.__data__ = new Pn(e);
      }return r.set(n, t), this.size = r.size, this;
    };var oo = Zr(Et),
        fo = Zr(Ot, true),
        co = qr(),
        ao = qr(true),
        lo = Hi ? function (n, t) {
      return Hi.set(n, t), n;
    } : Nu,
        so = Ei ? function (n, t) {
      return Ei(n, "toString", { configurable: true, enumerable: false, value: Fu(t), writable: true });
    } : Nu,
        ho = Oi || function (n) {
      return Zn.clearTimeout(n);
    },
        po = Vi && 1 / D(new Vi([, -0]))[1] == N ? function (n) {
      return new Vi(n);
    } : qu,
        _o = Hi ? function (n) {
      return Hi.get(n);
    } : qu,
        vo = Wi ? function (n) {
      return null == n ? [] : (n = ni(n), f(Wi(n), function (t) {
        return ji.call(n, t);
      }));
    } : Ku,
        go = Wi ? function (n) {
      for (var t = []; n;) {
        s(t, vo(n)), n = bi(n);
      }return t;
    } : Ku,
        yo = zt;(Pi && "[object DataView]" != yo(new Pi(new ArrayBuffer(1))) || Zi && "[object Map]" != yo(new Zi()) || qi && "[object Promise]" != yo(qi.resolve()) || Vi && "[object Set]" != yo(new Vi()) || Ki && "[object WeakMap]" != yo(new Ki())) && (yo = function yo(n) {
      var t = zt(n);if (n = (n = "[object Object]" == t ? n.constructor : F) ? Fe(n) : "") switch (n) {case Yi:
          return "[object DataView]";case Qi:
          return "[object Map]";case Xi:
          return "[object Promise]";case no:
          return "[object Set]";case to:
          return "[object WeakMap]";}return t;
    });var bo = oi ? gu : Gu,
        xo = Me(lo),
        jo = Ii || function (n, t) {
      return Zn.setTimeout(n, t);
    },
        wo = Me(so),
        mo = function (n) {
      n = lu(n, function (n) {
        return 500 === t.size && t.clear(), n;
      });var t = n.cache;return n;
    }(function (n) {
      var t = [];return en.test(n) && t.push(""), n.replace(un, function (n, r, e, u) {
        t.push(e ? u.replace(vn, "$1") : r || n);
      }), t;
    }),
        Ao = lr(function (n, t) {
      return _u(n) ? jt(n, kt(t, 1, _u, true)) : [];
    }),
        ko = lr(function (n, t) {
      var r = Ge(t);return _u(r) && (r = F), _u(n) ? jt(n, kt(t, 1, _u, true), je(r, 2)) : [];
    }),
        Eo = lr(function (n, t) {
      var r = Ge(t);return _u(r) && (r = F), _u(n) ? jt(n, kt(t, 1, _u, true), F, r) : [];
    }),
        Oo = lr(function (n) {
      var t = l(n, Sr);return t.length && t[0] === n[0] ? Ut(t) : [];
    }),
        So = lr(function (n) {
      var t = Ge(n),
          r = l(n, Sr);return t === Ge(r) ? t = F : r.pop(), r.length && r[0] === n[0] ? Ut(r, je(t, 2)) : [];
    }),
        Io = lr(function (n) {
      var t = Ge(n),
          r = l(n, Sr);return (t = typeof t == "function" ? t : F) && r.pop(), r.length && r[0] === n[0] ? Ut(r, F, t) : [];
    }),
        Ro = lr(He),
        zo = ge(function (n, t) {
      var r = null == n ? 0 : n.length,
          e = vt(n, t);return fr(n, l(t, function (n) {
        return Re(n, r) ? +n : n;
      }).sort(Ur)), e;
    }),
        Wo = lr(function (n) {
      return wr(kt(n, 1, _u, true));
    }),
        Bo = lr(function (n) {
      var t = Ge(n);return _u(t) && (t = F), wr(kt(n, 1, _u, true), je(t, 2));
    }),
        Lo = lr(function (n) {
      var t = Ge(n),
          t = typeof t == "function" ? t : F;return wr(kt(n, 1, _u, true), F, t);
    }),
        Uo = lr(function (n, t) {
      return _u(n) ? jt(n, t) : [];
    }),
        Co = lr(function (n) {
      return Er(f(n, _u));
    }),
        Do = lr(function (n) {
      var t = Ge(n);return _u(t) && (t = F), Er(f(n, _u), je(t, 2));
    }),
        Mo = lr(function (n) {
      var t = Ge(n),
          t = typeof t == "function" ? t : F;return Er(f(n, _u), F, t);
    }),
        To = lr(Ye),
        $o = lr(function (n) {
      var t = n.length,
          t = 1 < t ? n[t - 1] : F,
          t = typeof t == "function" ? (n.pop(), t) : F;return Qe(n, t);
    }),
        Fo = ge(function (n) {
      function t(t) {
        return vt(t, n);
      }var r = n.length,
          e = r ? n[0] : 0,
          u = this.__wrapped__;return !(1 < r || this.__actions__.length) && u instanceof Mn && Re(e) ? (u = u.slice(e, +e + (r ? 1 : 0)), u.__actions__.push({ func: nu, args: [t], thisArg: F }), new zn(u, this.__chain__).thru(function (n) {
        return r && !n.length && n.push(F), n;
      })) : this.thru(t);
    }),
        No = Nr(function (n, t, r) {
      ci.call(n, r) ? ++n[r] : _t(n, r, 1);
    }),
        Po = Yr(Ze),
        Zo = Yr(qe),
        qo = Nr(function (n, t, r) {
      ci.call(n, r) ? n[r].push(t) : _t(n, r, [t]);
    }),
        Vo = lr(function (n, t, e) {
      var u = -1,
          i = typeof t == "function",
          o = pu(n) ? Hu(n.length) : [];return oo(n, function (n) {
        o[++u] = i ? r(t, n, e) : Dt(n, t, e);
      }), o;
    }),
        Ko = Nr(function (n, t, r) {
      _t(n, r, t);
    }),
        Go = Nr(function (n, t, r) {
      n[r ? 0 : 1].push(t);
    }, function () {
      return [[], []];
    }),
        Ho = lr(function (n, t) {
      if (null == n) return [];var r = t.length;return 1 < r && ze(n, t[0], t[1]) ? t = [] : 2 < r && ze(t[0], t[1], t[2]) && (t = [t[0]]), rr(n, kt(t, 1), []);
    }),
        Jo = Si || function () {
      return Zn.Date.now();
    },
        Yo = lr(function (n, t, r) {
      var e = 1;if (r.length) var u = C(r, xe(Yo)),
          e = 32 | e;return le(n, e, t, r, u);
    }),
        Qo = lr(function (n, t, r) {
      var e = 3;if (r.length) var u = C(r, xe(Qo)),
          e = 32 | e;return le(t, e, n, r, u);
    }),
        Xo = lr(function (n, t) {
      return xt(n, 1, t);
    }),
        nf = lr(function (n, t, r) {
      return xt(n, Iu(t) || 0, r);
    });lu.Cache = Pn;var tf = lr(function (n, t) {
      t = 1 == t.length && af(t[0]) ? l(t[0], S(je())) : l(kt(t, 1), S(je()));var e = t.length;return lr(function (u) {
        for (var i = -1, o = Mi(u.length, e); ++i < o;) {
          u[i] = t[i].call(this, u[i]);
        }return r(n, this, u);
      });
    }),
        rf = lr(function (n, t) {
      return le(n, 32, F, t, C(t, xe(rf)));
    }),
        ef = lr(function (n, t) {
      return le(n, 64, F, t, C(t, xe(ef)));
    }),
        uf = ge(function (n, t) {
      return le(n, 256, F, F, F, t);
    }),
        of = oe(Wt),
        ff = oe(function (n, t) {
      return n >= t;
    }),
        cf = Mt(function () {
      return arguments;
    }()) ? Mt : function (n) {
      return xu(n) && ci.call(n, "callee") && !ji.call(n, "callee");
    },
        af = Hu.isArray,
        lf = Hn ? S(Hn) : Tt,
        sf = Bi || Gu,
        hf = Jn ? S(Jn) : $t,
        pf = Yn ? S(Yn) : Nt,
        _f = Qn ? S(Qn) : qt,
        vf = Xn ? S(Xn) : Vt,
        gf = nt ? S(nt) : Kt,
        df = oe(Jt),
        yf = oe(function (n, t) {
      return n <= t;
    }),
        bf = Pr(function (n, t) {
      if (Le(t) || pu(t)) Tr(t, Lu(t), n);else for (var r in t) {
        ci.call(t, r) && at(n, r, t[r]);
      }
    }),
        xf = Pr(function (n, t) {
      Tr(t, Uu(t), n);
    }),
        jf = Pr(function (n, t, r, e) {
      Tr(t, Uu(t), n, e);
    }),
        wf = Pr(function (n, t, r, e) {
      Tr(t, Lu(t), n, e);
    }),
        mf = ge(vt),
        Af = lr(function (n) {
      return n.push(F, se), r(jf, F, n);
    }),
        kf = lr(function (n) {
      return n.push(F, he), r(Rf, F, n);
    }),
        Ef = ne(function (n, t, r) {
      n[t] = r;
    }, Fu(Nu)),
        Of = ne(function (n, t, r) {
      ci.call(n, t) ? n[t].push(r) : n[t] = [r];
    }, je),
        Sf = lr(Dt),
        If = Pr(function (n, t, r) {
      nr(n, t, r);
    }),
        Rf = Pr(function (n, t, r, e) {
      nr(n, t, r, e);
    }),
        zf = ge(function (n, t) {
      var r = {};if (null == n) return r;var e = false;t = l(t, function (t) {
        return t = Rr(t, n), e || (e = 1 < t.length), t;
      }), Tr(n, ye(n), r), e && (r = dt(r, 7, pe));for (var u = t.length; u--;) {
        mr(r, t[u]);
      }return r;
    }),
        Wf = ge(function (n, t) {
      return null == n ? {} : er(n, t);
    }),
        Bf = ae(Lu),
        Lf = ae(Uu),
        Uf = Gr(function (n, t, r) {
      return t = t.toLowerCase(), n + (r ? Mu(t) : t);
    }),
        Cf = Gr(function (n, t, r) {
      return n + (r ? "-" : "") + t.toLowerCase();
    }),
        Df = Gr(function (n, t, r) {
      return n + (r ? " " : "") + t.toLowerCase();
    }),
        Mf = Kr("toLowerCase"),
        Tf = Gr(function (n, t, r) {
      return n + (r ? "_" : "") + t.toLowerCase();
    }),
        $f = Gr(function (n, t, r) {
      return n + (r ? " " : "") + Nf(t);
    }),
        Ff = Gr(function (n, t, r) {
      return n + (r ? " " : "") + t.toUpperCase();
    }),
        Nf = Kr("toUpperCase"),
        Pf = lr(function (n, t) {
      try {
        return r(n, F, t);
      } catch (n) {
        return vu(n) ? n : new Yu(n);
      }
    }),
        Zf = ge(function (n, t) {
      return u(t, function (t) {
        t = $e(t), _t(n, t, Yo(n[t], n));
      }), n;
    }),
        qf = Qr(),
        Vf = Qr(true),
        Kf = lr(function (n, t) {
      return function (r) {
        return Dt(r, n, t);
      };
    }),
        Gf = lr(function (n, t) {
      return function (r) {
        return Dt(n, r, t);
      };
    }),
        Hf = re(l),
        Jf = re(o),
        Yf = re(_),
        Qf = ie(),
        Xf = ie(true),
        nc = te(function (n, t) {
      return n + t;
    }, 0),
        tc = ce("ceil"),
        rc = te(function (n, t) {
      return n / t;
    }, 1),
        ec = ce("floor"),
        uc = te(function (n, t) {
      return n * t;
    }, 1),
        ic = ce("round"),
        oc = te(function (n, t) {
      return n - t;
    }, 0);return On.after = function (n, t) {
      if (typeof t != "function") throw new ei("Expected a function");return n = Ou(n), function () {
        if (1 > --n) return t.apply(this, arguments);
      };
    }, On.ary = iu, On.assign = bf, On.assignIn = xf, On.assignInWith = jf, On.assignWith = wf, On.at = mf, On.before = ou, On.bind = Yo, On.bindAll = Zf, On.bindKey = Qo, On.castArray = function () {
      if (!arguments.length) return [];var n = arguments[0];return af(n) ? n : [n];
    }, On.chain = Xe, On.chunk = function (n, t, r) {
      if (t = (r ? ze(n, t, r) : t === F) ? 1 : Di(Ou(t), 0), r = null == n ? 0 : n.length, !r || 1 > t) return [];for (var e = 0, u = 0, i = Hu(Ri(r / t)); e < r;) {
        i[u++] = vr(n, e, e += t);
      }return i;
    }, On.compact = function (n) {
      for (var t = -1, r = null == n ? 0 : n.length, e = 0, u = []; ++t < r;) {
        var i = n[t];i && (u[e++] = i);
      }return u;
    }, On.concat = function () {
      var n = arguments.length;if (!n) return [];for (var t = Hu(n - 1), r = arguments[0]; n--;) {
        t[n - 1] = arguments[n];
      }return s(af(r) ? Mr(r) : [r], kt(t, 1));
    }, On.cond = function (n) {
      var t = null == n ? 0 : n.length,
          e = je();return n = t ? l(n, function (n) {
        if ("function" != typeof n[1]) throw new ei("Expected a function");return [e(n[0]), n[1]];
      }) : [], lr(function (e) {
        for (var u = -1; ++u < t;) {
          var i = n[u];if (r(i[0], this, e)) return r(i[1], this, e);
        }
      });
    }, On.conforms = function (n) {
      return yt(dt(n, 1));
    }, On.constant = Fu, On.countBy = No, On.create = function (n, t) {
      var r = io(n);return null == t ? r : ht(r, t);
    }, On.curry = fu, On.curryRight = cu, On.debounce = au, On.defaults = Af, On.defaultsDeep = kf, On.defer = Xo, On.delay = nf, On.difference = Ao, On.differenceBy = ko, On.differenceWith = Eo, On.drop = function (n, t, r) {
      var e = null == n ? 0 : n.length;
      return e ? (t = r || t === F ? 1 : Ou(t), vr(n, 0 > t ? 0 : t, e)) : [];
    }, On.dropRight = function (n, t, r) {
      var e = null == n ? 0 : n.length;return e ? (t = r || t === F ? 1 : Ou(t), t = e - t, vr(n, 0, 0 > t ? 0 : t)) : [];
    }, On.dropRightWhile = function (n, t) {
      return n && n.length ? Ar(n, je(t, 3), true, true) : [];
    }, On.dropWhile = function (n, t) {
      return n && n.length ? Ar(n, je(t, 3), true) : [];
    }, On.fill = function (n, t, r, e) {
      var u = null == n ? 0 : n.length;if (!u) return [];for (r && typeof r != "number" && ze(n, t, r) && (r = 0, e = u), u = n.length, r = Ou(r), 0 > r && (r = -r > u ? 0 : u + r), e = e === F || e > u ? u : Ou(e), 0 > e && (e += u), e = r > e ? 0 : Su(e); r < e;) {
        n[r++] = t;
      }return n;
    }, On.filter = function (n, t) {
      return (af(n) ? f : At)(n, je(t, 3));
    }, On.flatMap = function (n, t) {
      return kt(uu(n, t), 1);
    }, On.flatMapDeep = function (n, t) {
      return kt(uu(n, t), N);
    }, On.flatMapDepth = function (n, t, r) {
      return r = r === F ? 1 : Ou(r), kt(uu(n, t), r);
    }, On.flatten = Ve, On.flattenDeep = function (n) {
      return (null == n ? 0 : n.length) ? kt(n, N) : [];
    }, On.flattenDepth = function (n, t) {
      return null != n && n.length ? (t = t === F ? 1 : Ou(t), kt(n, t)) : [];
    }, On.flip = function (n) {
      return le(n, 512);
    }, On.flow = qf, On.flowRight = Vf, On.fromPairs = function (n) {
      for (var t = -1, r = null == n ? 0 : n.length, e = {}; ++t < r;) {
        var u = n[t];e[u[0]] = u[1];
      }return e;
    }, On.functions = function (n) {
      return null == n ? [] : St(n, Lu(n));
    }, On.functionsIn = function (n) {
      return null == n ? [] : St(n, Uu(n));
    }, On.groupBy = qo, On.initial = function (n) {
      return (null == n ? 0 : n.length) ? vr(n, 0, -1) : [];
    }, On.intersection = Oo, On.intersectionBy = So, On.intersectionWith = Io, On.invert = Ef, On.invertBy = Of, On.invokeMap = Vo, On.iteratee = Pu, On.keyBy = Ko, On.keys = Lu, On.keysIn = Uu, On.map = uu, On.mapKeys = function (n, t) {
      var r = {};return t = je(t, 3), Et(n, function (n, e, u) {
        _t(r, t(n, e, u), n);
      }), r;
    }, On.mapValues = function (n, t) {
      var r = {};return t = je(t, 3), Et(n, function (n, e, u) {
        _t(r, e, t(n, e, u));
      }), r;
    }, On.matches = function (n) {
      return Qt(dt(n, 1));
    }, On.matchesProperty = function (n, t) {
      return Xt(n, dt(t, 1));
    }, On.memoize = lu, On.merge = If, On.mergeWith = Rf, On.method = Kf, On.methodOf = Gf, On.mixin = Zu, On.negate = su, On.nthArg = function (n) {
      return n = Ou(n), lr(function (t) {
        return tr(t, n);
      });
    }, On.omit = zf, On.omitBy = function (n, t) {
      return Cu(n, su(je(t)));
    }, On.once = function (n) {
      return ou(2, n);
    }, On.orderBy = function (n, t, r, e) {
      return null == n ? [] : (af(t) || (t = null == t ? [] : [t]), r = e ? F : r, af(r) || (r = null == r ? [] : [r]), rr(n, t, r));
    }, On.over = Hf, On.overArgs = tf, On.overEvery = Jf, On.overSome = Yf, On.partial = rf, On.partialRight = ef, On.partition = Go, On.pick = Wf, On.pickBy = Cu, On.property = Vu, On.propertyOf = function (n) {
      return function (t) {
        return null == n ? F : It(n, t);
      };
    }, On.pull = Ro, On.pullAll = He, On.pullAllBy = function (n, t, r) {
      return n && n.length && t && t.length ? or(n, t, je(r, 2)) : n;
    }, On.pullAllWith = function (n, t, r) {
      return n && n.length && t && t.length ? or(n, t, F, r) : n;
    }, On.pullAt = zo, On.range = Qf, On.rangeRight = Xf, On.rearg = uf, On.reject = function (n, t) {
      return (af(n) ? f : At)(n, su(je(t, 3)));
    }, On.remove = function (n, t) {
      var r = [];if (!n || !n.length) return r;var e = -1,
          u = [],
          i = n.length;for (t = je(t, 3); ++e < i;) {
        var o = n[e];t(o, e, n) && (r.push(o), u.push(e));
      }return fr(n, u), r;
    }, On.rest = function (n, t) {
      if (typeof n != "function") throw new ei("Expected a function");return t = t === F ? t : Ou(t), lr(n, t);
    }, On.reverse = Je, On.sampleSize = function (n, t, r) {
      return t = (r ? ze(n, t, r) : t === F) ? 1 : Ou(t), (af(n) ? ot : hr)(n, t);
    }, On.set = function (n, t, r) {
      return null == n ? n : pr(n, t, r);
    }, On.setWith = function (n, t, r, e) {
      return e = typeof e == "function" ? e : F, null == n ? n : pr(n, t, r, e);
    }, On.shuffle = function (n) {
      return (af(n) ? ft : _r)(n);
    }, On.slice = function (n, t, r) {
      var e = null == n ? 0 : n.length;return e ? (r && typeof r != "number" && ze(n, t, r) ? (t = 0, r = e) : (t = null == t ? 0 : Ou(t), r = r === F ? e : Ou(r)), vr(n, t, r)) : [];
    }, On.sortBy = Ho, On.sortedUniq = function (n) {
      return n && n.length ? br(n) : [];
    }, On.sortedUniqBy = function (n, t) {
      return n && n.length ? br(n, je(t, 2)) : [];
    }, On.split = function (n, t, r) {
      return r && typeof r != "number" && ze(n, t, r) && (t = r = F), r = r === F ? 4294967295 : r >>> 0, r ? (n = zu(n)) && (typeof t == "string" || null != t && !_f(t)) && (t = jr(t), !t && Bn.test(n)) ? zr($(n), 0, r) : n.split(t, r) : [];
    }, On.spread = function (n, t) {
      if (typeof n != "function") throw new ei("Expected a function");return t = null == t ? 0 : Di(Ou(t), 0), lr(function (e) {
        var u = e[t];return e = zr(e, 0, t), u && s(e, u), r(n, this, e);
      });
    }, On.tail = function (n) {
      var t = null == n ? 0 : n.length;return t ? vr(n, 1, t) : [];
    }, On.take = function (n, t, r) {
      return n && n.length ? (t = r || t === F ? 1 : Ou(t), vr(n, 0, 0 > t ? 0 : t)) : [];
    }, On.takeRight = function (n, t, r) {
      var e = null == n ? 0 : n.length;return e ? (t = r || t === F ? 1 : Ou(t), t = e - t, vr(n, 0 > t ? 0 : t, e)) : [];
    }, On.takeRightWhile = function (n, t) {
      return n && n.length ? Ar(n, je(t, 3), false, true) : [];
    }, On.takeWhile = function (n, t) {
      return n && n.length ? Ar(n, je(t, 3)) : [];
    }, On.tap = function (n, t) {
      return t(n), n;
    }, On.throttle = function (n, t, r) {
      var e = true,
          u = true;if (typeof n != "function") throw new ei("Expected a function");return bu(r) && (e = "leading" in r ? !!r.leading : e, u = "trailing" in r ? !!r.trailing : u), au(n, t, { leading: e, maxWait: t, trailing: u });
    }, On.thru = nu, On.toArray = ku, On.toPairs = Bf, On.toPairsIn = Lf, On.toPath = function (n) {
      return af(n) ? l(n, $e) : Au(n) ? [n] : Mr(mo(zu(n)));
    }, On.toPlainObject = Ru, On.transform = function (n, t, r) {
      var e = af(n),
          i = e || sf(n) || gf(n);if (t = je(t, 4), null == r) {
        var o = n && n.constructor;r = i ? e ? new o() : [] : bu(n) && gu(o) ? io(bi(n)) : {};
      }return (i ? u : Et)(n, function (n, e, u) {
        return t(r, n, e, u);
      }), r;
    }, On.unary = function (n) {
      return iu(n, 1);
    }, On.union = Wo, On.unionBy = Bo, On.unionWith = Lo, On.uniq = function (n) {
      return n && n.length ? wr(n) : [];
    }, On.uniqBy = function (n, t) {
      return n && n.length ? wr(n, je(t, 2)) : [];
    }, On.uniqWith = function (n, t) {
      return t = typeof t == "function" ? t : F, n && n.length ? wr(n, F, t) : [];
    }, On.unset = function (n, t) {
      return null == n || mr(n, t);
    }, On.unzip = Ye, On.unzipWith = Qe, On.update = function (n, t, r) {
      return null == n ? n : pr(n, t, Ir(r)(It(n, t)), void 0);
    }, On.updateWith = function (n, t, r, e) {
      return e = typeof e == "function" ? e : F, null != n && (n = pr(n, t, Ir(r)(It(n, t)), e)), n;
    }, On.values = Du, On.valuesIn = function (n) {
      return null == n ? [] : I(n, Uu(n));
    }, On.without = Uo, On.words = $u, On.wrap = function (n, t) {
      return rf(Ir(t), n);
    }, On.xor = Co, On.xorBy = Do, On.xorWith = Mo, On.zip = To, On.zipObject = function (n, t) {
      return Or(n || [], t || [], at);
    }, On.zipObjectDeep = function (n, t) {
      return Or(n || [], t || [], pr);
    }, On.zipWith = $o, On.entries = Bf, On.entriesIn = Lf, On.extend = xf, On.extendWith = jf, Zu(On, On), On.add = nc, On.attempt = Pf, On.camelCase = Uf, On.capitalize = Mu, On.ceil = tc, On.clamp = function (n, t, r) {
      return r === F && (r = t, t = F), r !== F && (r = Iu(r), r = r === r ? r : 0), t !== F && (t = Iu(t), t = t === t ? t : 0), gt(Iu(n), t, r);
    }, On.clone = function (n) {
      return dt(n, 4);
    }, On.cloneDeep = function (n) {
      return dt(n, 5);
    }, On.cloneDeepWith = function (n, t) {
      return t = typeof t == "function" ? t : F, dt(n, 5, t);
    }, On.cloneWith = function (n, t) {
      return t = typeof t == "function" ? t : F, dt(n, 4, t);
    }, On.conformsTo = function (n, t) {
      return null == t || bt(n, t, Lu(t));
    }, On.deburr = Tu, On.defaultTo = function (n, t) {
      return null == n || n !== n ? t : n;
    }, On.divide = rc, On.endsWith = function (n, t, r) {
      n = zu(n), t = jr(t);var e = n.length,
          e = r = r === F ? e : gt(Ou(r), 0, e);return r -= t.length, 0 <= r && n.slice(r, e) == t;
    }, On.eq = hu, On.escape = function (n) {
      return (n = zu(n)) && Y.test(n) ? n.replace(H, et) : n;
    }, On.escapeRegExp = function (n) {
      return (n = zu(n)) && fn.test(n) ? n.replace(on, "\\$&") : n;
    }, On.every = function (n, t, r) {
      var e = af(n) ? o : wt;return r && ze(n, t, r) && (t = F), e(n, je(t, 3));
    }, On.find = Po, On.findIndex = Ze, On.findKey = function (n, t) {
      return v(n, je(t, 3), Et);
    }, On.findLast = Zo, On.findLastIndex = qe, On.findLastKey = function (n, t) {
      return v(n, je(t, 3), Ot);
    }, On.floor = ec, On.forEach = ru, On.forEachRight = eu, On.forIn = function (n, t) {
      return null == n ? n : co(n, je(t, 3), Uu);
    }, On.forInRight = function (n, t) {
      return null == n ? n : ao(n, je(t, 3), Uu);
    }, On.forOwn = function (n, t) {
      return n && Et(n, je(t, 3));
    }, On.forOwnRight = function (n, t) {
      return n && Ot(n, je(t, 3));
    }, On.get = Wu, On.gt = of, On.gte = ff, On.has = function (n, t) {
      return null != n && ke(n, t, Bt);
    }, On.hasIn = Bu, On.head = Ke, On.identity = Nu, On.includes = function (n, t, r, e) {
      return n = pu(n) ? n : Du(n), r = r && !e ? Ou(r) : 0, e = n.length, 0 > r && (r = Di(e + r, 0)), mu(n) ? r <= e && -1 < n.indexOf(t, r) : !!e && -1 < d(n, t, r);
    }, On.indexOf = function (n, t, r) {
      var e = null == n ? 0 : n.length;return e ? (r = null == r ? 0 : Ou(r), 0 > r && (r = Di(e + r, 0)), d(n, t, r)) : -1;
    }, On.inRange = function (n, t, r) {
      return t = Eu(t), r === F ? (r = t, t = 0) : r = Eu(r), n = Iu(n), n >= Mi(t, r) && n < Di(t, r);
    }, On.invoke = Sf, On.isArguments = cf, On.isArray = af, On.isArrayBuffer = lf, On.isArrayLike = pu, On.isArrayLikeObject = _u, On.isBoolean = function (n) {
      return true === n || false === n || xu(n) && "[object Boolean]" == zt(n);
    }, On.isBuffer = sf, On.isDate = hf, On.isElement = function (n) {
      return xu(n) && 1 === n.nodeType && !wu(n);
    }, On.isEmpty = function (n) {
      if (null == n) return true;if (pu(n) && (af(n) || typeof n == "string" || typeof n.splice == "function" || sf(n) || gf(n) || cf(n))) return !n.length;var t = yo(n);if ("[object Map]" == t || "[object Set]" == t) return !n.size;if (Le(n)) return !Ht(n).length;for (var r in n) {
        if (ci.call(n, r)) return false;
      }return true;
    }, On.isEqual = function (n, t) {
      return Ft(n, t);
    }, On.isEqualWith = function (n, t, r) {
      var e = (r = typeof r == "function" ? r : F) ? r(n, t) : F;return e === F ? Ft(n, t, F, r) : !!e;
    }, On.isError = vu, On.isFinite = function (n) {
      return typeof n == "number" && Li(n);
    }, On.isFunction = gu, On.isInteger = du, On.isLength = yu, On.isMap = pf, On.isMatch = function (n, t) {
      return n === t || Pt(n, t, me(t));
    }, On.isMatchWith = function (n, t, r) {
      return r = typeof r == "function" ? r : F, Pt(n, t, me(t), r);
    }, On.isNaN = function (n) {
      return ju(n) && n != +n;
    }, On.isNative = function (n) {
      if (bo(n)) throw new Yu("Unsupported core-js use. Try https://npms.io/search?q=ponyfill.");
      return Zt(n);
    }, On.isNil = function (n) {
      return null == n;
    }, On.isNull = function (n) {
      return null === n;
    }, On.isNumber = ju, On.isObject = bu, On.isObjectLike = xu, On.isPlainObject = wu, On.isRegExp = _f, On.isSafeInteger = function (n) {
      return du(n) && -9007199254740991 <= n && 9007199254740991 >= n;
    }, On.isSet = vf, On.isString = mu, On.isSymbol = Au, On.isTypedArray = gf, On.isUndefined = function (n) {
      return n === F;
    }, On.isWeakMap = function (n) {
      return xu(n) && "[object WeakMap]" == yo(n);
    }, On.isWeakSet = function (n) {
      return xu(n) && "[object WeakSet]" == zt(n);
    }, On.join = function (n, t) {
      return null == n ? "" : Ui.call(n, t);
    }, On.kebabCase = Cf, On.last = Ge, On.lastIndexOf = function (n, t, r) {
      var e = null == n ? 0 : n.length;if (!e) return -1;var u = e;if (r !== F && (u = Ou(r), u = 0 > u ? Di(e + u, 0) : Mi(u, e - 1)), t === t) {
        for (r = u + 1; r-- && n[r] !== t;) {}n = r;
      } else n = g(n, b, u, true);return n;
    }, On.lowerCase = Df, On.lowerFirst = Mf, On.lt = df, On.lte = yf, On.max = function (n) {
      return n && n.length ? mt(n, Nu, Wt) : F;
    }, On.maxBy = function (n, t) {
      return n && n.length ? mt(n, je(t, 2), Wt) : F;
    }, On.mean = function (n) {
      return x(n, Nu);
    }, On.meanBy = function (n, t) {
      return x(n, je(t, 2));
    }, On.min = function (n) {
      return n && n.length ? mt(n, Nu, Jt) : F;
    }, On.minBy = function (n, t) {
      return n && n.length ? mt(n, je(t, 2), Jt) : F;
    }, On.stubArray = Ku, On.stubFalse = Gu, On.stubObject = function () {
      return {};
    }, On.stubString = function () {
      return "";
    }, On.stubTrue = function () {
      return true;
    }, On.multiply = uc, On.nth = function (n, t) {
      return n && n.length ? tr(n, Ou(t)) : F;
    }, On.noConflict = function () {
      return Zn._ === this && (Zn._ = pi), this;
    }, On.noop = qu, On.now = Jo, On.pad = function (n, t, r) {
      n = zu(n);var e = (t = Ou(t)) ? T(n) : 0;return !t || e >= t ? n : (t = (t - e) / 2, ee(zi(t), r) + n + ee(Ri(t), r));
    }, On.padEnd = function (n, t, r) {
      n = zu(n);var e = (t = Ou(t)) ? T(n) : 0;return t && e < t ? n + ee(t - e, r) : n;
    }, On.padStart = function (n, t, r) {
      n = zu(n);var e = (t = Ou(t)) ? T(n) : 0;return t && e < t ? ee(t - e, r) + n : n;
    }, On.parseInt = function (n, t, r) {
      return r || null == t ? t = 0 : t && (t = +t), $i(zu(n).replace(an, ""), t || 0);
    }, On.random = function (n, t, r) {
      if (r && typeof r != "boolean" && ze(n, t, r) && (t = r = F), r === F && (typeof t == "boolean" ? (r = t, t = F) : typeof n == "boolean" && (r = n, n = F)), n === F && t === F ? (n = 0, t = 1) : (n = Eu(n), t === F ? (t = n, n = 0) : t = Eu(t)), n > t) {
        var e = n;n = t, t = e;
      }return r || n % 1 || t % 1 ? (r = Fi(), Mi(n + r * (t - n + $n("1e-" + ((r + "").length - 1))), t)) : cr(n, t);
    }, On.reduce = function (n, t, r) {
      var e = af(n) ? h : m,
          u = 3 > arguments.length;return e(n, je(t, 4), r, u, oo);
    }, On.reduceRight = function (n, t, r) {
      var e = af(n) ? p : m,
          u = 3 > arguments.length;return e(n, je(t, 4), r, u, fo);
    }, On.repeat = function (n, t, r) {
      return t = (r ? ze(n, t, r) : t === F) ? 1 : Ou(t), ar(zu(n), t);
    }, On.replace = function () {
      var n = arguments,
          t = zu(n[0]);return 3 > n.length ? t : t.replace(n[1], n[2]);
    }, On.result = function (n, t, r) {
      t = Rr(t, n);var e = -1,
          u = t.length;for (u || (u = 1, n = F); ++e < u;) {
        var i = null == n ? F : n[$e(t[e])];i === F && (e = u, i = r), n = gu(i) ? i.call(n) : i;
      }return n;
    }, On.round = ic, On.runInContext = w, On.sample = function (n) {
      return (af(n) ? tt : sr)(n);
    }, On.size = function (n) {
      if (null == n) return 0;if (pu(n)) return mu(n) ? T(n) : n.length;var t = yo(n);return "[object Map]" == t || "[object Set]" == t ? n.size : Ht(n).length;
    }, On.snakeCase = Tf, On.some = function (n, t, r) {
      var e = af(n) ? _ : gr;return r && ze(n, t, r) && (t = F), e(n, je(t, 3));
    }, On.sortedIndex = function (n, t) {
      return dr(n, t);
    }, On.sortedIndexBy = function (n, t, r) {
      return yr(n, t, je(r, 2));
    }, On.sortedIndexOf = function (n, t) {
      var r = null == n ? 0 : n.length;if (r) {
        var e = dr(n, t);if (e < r && hu(n[e], t)) return e;
      }return -1;
    }, On.sortedLastIndex = function (n, t) {
      return dr(n, t, true);
    }, On.sortedLastIndexBy = function (n, t, r) {
      return yr(n, t, je(r, 2), true);
    }, On.sortedLastIndexOf = function (n, t) {
      if (null == n ? 0 : n.length) {
        var r = dr(n, t, true) - 1;if (hu(n[r], t)) return r;
      }return -1;
    }, On.startCase = $f, On.startsWith = function (n, t, r) {
      return n = zu(n), r = null == r ? 0 : gt(Ou(r), 0, n.length), t = jr(t), n.slice(r, r + t.length) == t;
    }, On.subtract = oc, On.sum = function (n) {
      return n && n.length ? k(n, Nu) : 0;
    }, On.sumBy = function (n, t) {
      return n && n.length ? k(n, je(t, 2)) : 0;
    }, On.template = function (n, t, r) {
      var e = On.templateSettings;r && ze(n, t, r) && (t = F), n = zu(n), t = jf({}, t, e, se), r = jf({}, t.imports, e.imports, se);var u,
          i,
          o = Lu(r),
          f = I(r, o),
          c = 0;r = t.interpolate || An;var a = "__p+='";r = ti((t.escape || An).source + "|" + r.source + "|" + (r === nn ? gn : An).source + "|" + (t.evaluate || An).source + "|$", "g");var l = "sourceURL" in t ? "//# sourceURL=" + t.sourceURL + "\n" : "";if (n.replace(r, function (t, r, e, o, f, l) {
        return e || (e = o), a += n.slice(c, l).replace(kn, B), r && (u = true, a += "'+__e(" + r + ")+'"), f && (i = true, a += "';" + f + ";\n__p+='"), e && (a += "'+((__t=(" + e + "))==null?'':__t)+'"), c = l + t.length, t;
      }), a += "';", (t = t.variable) || (a = "with(obj){" + a + "}"), a = (i ? a.replace(q, "") : a).replace(V, "$1").replace(K, "$1;"), a = "function(" + (t || "obj") + "){" + (t ? "" : "obj||(obj={});") + "var __t,__p=''" + (u ? ",__e=_.escape" : "") + (i ? ",__j=Array.prototype.join;function print(){__p+=__j.call(arguments,'')}" : ";") + a + "return __p}", t = Pf(function () {
        return Qu(o, l + "return " + a).apply(F, f);
      }), t.source = a, vu(t)) throw t;return t;
    }, On.times = function (n, t) {
      if (n = Ou(n), 1 > n || 9007199254740991 < n) return [];
      var r = 4294967295,
          e = Mi(n, 4294967295);for (t = je(t), n -= 4294967295, e = E(e, t); ++r < n;) {
        t(r);
      }return e;
    }, On.toFinite = Eu, On.toInteger = Ou, On.toLength = Su, On.toLower = function (n) {
      return zu(n).toLowerCase();
    }, On.toNumber = Iu, On.toSafeInteger = function (n) {
      return n ? gt(Ou(n), -9007199254740991, 9007199254740991) : 0 === n ? n : 0;
    }, On.toString = zu, On.toUpper = function (n) {
      return zu(n).toUpperCase();
    }, On.trim = function (n, t, r) {
      return (n = zu(n)) && (r || t === F) ? n.replace(cn, "") : n && (t = jr(t)) ? (n = $(n), r = $(t), t = z(n, r), r = W(n, r) + 1, zr(n, t, r).join("")) : n;
    }, On.trimEnd = function (n, t, r) {
      return (n = zu(n)) && (r || t === F) ? n.replace(ln, "") : n && (t = jr(t)) ? (n = $(n), t = W(n, $(t)) + 1, zr(n, 0, t).join("")) : n;
    }, On.trimStart = function (n, t, r) {
      return (n = zu(n)) && (r || t === F) ? n.replace(an, "") : n && (t = jr(t)) ? (n = $(n), t = z(n, $(t)), zr(n, t).join("")) : n;
    }, On.truncate = function (n, t) {
      var r = 30,
          e = "...";if (bu(t)) var u = "separator" in t ? t.separator : u,
          r = "length" in t ? Ou(t.length) : r,
          e = "omission" in t ? jr(t.omission) : e;n = zu(n);var i = n.length;if (Bn.test(n)) var o = $(n),
          i = o.length;if (r >= i) return n;if (i = r - T(e), 1 > i) return e;
      if (r = o ? zr(o, 0, i).join("") : n.slice(0, i), u === F) return r + e;if (o && (i += r.length - i), _f(u)) {
        if (n.slice(i).search(u)) {
          var f = r;for (u.global || (u = ti(u.source, zu(dn.exec(u)) + "g")), u.lastIndex = 0; o = u.exec(f);) {
            var c = o.index;
          }r = r.slice(0, c === F ? i : c);
        }
      } else n.indexOf(jr(u), i) != i && (u = r.lastIndexOf(u), -1 < u && (r = r.slice(0, u)));return r + e;
    }, On.unescape = function (n) {
      return (n = zu(n)) && J.test(n) ? n.replace(G, ut) : n;
    }, On.uniqueId = function (n) {
      var t = ++ai;return zu(n) + t;
    }, On.upperCase = Ff, On.upperFirst = Nf, On.each = ru, On.eachRight = eu, On.first = Ke, Zu(On, function () {
      var n = {};return Et(On, function (t, r) {
        ci.call(On.prototype, r) || (n[r] = t);
      }), n;
    }(), { chain: false }), On.VERSION = "4.17.4", u("bind bindKey curry curryRight partial partialRight".split(" "), function (n) {
      On[n].placeholder = On;
    }), u(["drop", "take"], function (n, t) {
      Mn.prototype[n] = function (r) {
        r = r === F ? 1 : Di(Ou(r), 0);var e = this.__filtered__ && !t ? new Mn(this) : this.clone();return e.__filtered__ ? e.__takeCount__ = Mi(r, e.__takeCount__) : e.__views__.push({ size: Mi(r, 4294967295), type: n + (0 > e.__dir__ ? "Right" : "") }), e;
      }, Mn.prototype[n + "Right"] = function (t) {
        return this.reverse()[n](t).reverse();
      };
    }), u(["filter", "map", "takeWhile"], function (n, t) {
      var r = t + 1,
          e = 1 == r || 3 == r;Mn.prototype[n] = function (n) {
        var t = this.clone();return t.__iteratees__.push({ iteratee: je(n, 3), type: r }), t.__filtered__ = t.__filtered__ || e, t;
      };
    }), u(["head", "last"], function (n, t) {
      var r = "take" + (t ? "Right" : "");Mn.prototype[n] = function () {
        return this[r](1).value()[0];
      };
    }), u(["initial", "tail"], function (n, t) {
      var r = "drop" + (t ? "" : "Right");Mn.prototype[n] = function () {
        return this.__filtered__ ? new Mn(this) : this[r](1);
      };
    }), Mn.prototype.compact = function () {
      return this.filter(Nu);
    }, Mn.prototype.find = function (n) {
      return this.filter(n).head();
    }, Mn.prototype.findLast = function (n) {
      return this.reverse().find(n);
    }, Mn.prototype.invokeMap = lr(function (n, t) {
      return typeof n == "function" ? new Mn(this) : this.map(function (r) {
        return Dt(r, n, t);
      });
    }), Mn.prototype.reject = function (n) {
      return this.filter(su(je(n)));
    }, Mn.prototype.slice = function (n, t) {
      n = Ou(n);var r = this;return r.__filtered__ && (0 < n || 0 > t) ? new Mn(r) : (0 > n ? r = r.takeRight(-n) : n && (r = r.drop(n)), t !== F && (t = Ou(t), r = 0 > t ? r.dropRight(-t) : r.take(t - n)), r);
    }, Mn.prototype.takeRightWhile = function (n) {
      return this.reverse().takeWhile(n).reverse();
    }, Mn.prototype.toArray = function () {
      return this.take(4294967295);
    }, Et(Mn.prototype, function (n, t) {
      var r = /^(?:filter|find|map|reject)|While$/.test(t),
          e = /^(?:head|last)$/.test(t),
          u = On[e ? "take" + ("last" == t ? "Right" : "") : t],
          i = e || /^find/.test(t);u && (On.prototype[t] = function () {
        function t(n) {
          return n = u.apply(On, s([n], f)), e && h ? n[0] : n;
        }var o = this.__wrapped__,
            f = e ? [1] : arguments,
            c = o instanceof Mn,
            a = f[0],
            l = c || af(o);
        l && r && typeof a == "function" && 1 != a.length && (c = l = false);var h = this.__chain__,
            p = !!this.__actions__.length,
            a = i && !h,
            c = c && !p;return !i && l ? (o = c ? o : new Mn(this), o = n.apply(o, f), o.__actions__.push({ func: nu, args: [t], thisArg: F }), new zn(o, h)) : a && c ? n.apply(this, f) : (o = this.thru(t), a ? e ? o.value()[0] : o.value() : o);
      });
    }), u("pop push shift sort splice unshift".split(" "), function (n) {
      var t = ui[n],
          r = /^(?:push|sort|unshift)$/.test(n) ? "tap" : "thru",
          e = /^(?:pop|shift)$/.test(n);On.prototype[n] = function () {
        var n = arguments;if (e && !this.__chain__) {
          var u = this.value();return t.apply(af(u) ? u : [], n);
        }return this[r](function (r) {
          return t.apply(af(r) ? r : [], n);
        });
      };
    }), Et(Mn.prototype, function (n, t) {
      var r = On[t];if (r) {
        var e = r.name + "";(Ji[e] || (Ji[e] = [])).push({ name: t, func: r });
      }
    }), Ji[Xr(F, 2).name] = [{ name: "wrapper", func: F }], Mn.prototype.clone = function () {
      var n = new Mn(this.__wrapped__);return n.__actions__ = Mr(this.__actions__), n.__dir__ = this.__dir__, n.__filtered__ = this.__filtered__, n.__iteratees__ = Mr(this.__iteratees__), n.__takeCount__ = this.__takeCount__, n.__views__ = Mr(this.__views__), n;
    }, Mn.prototype.reverse = function () {
      if (this.__filtered__) {
        var n = new Mn(this);n.__dir__ = -1, n.__filtered__ = true;
      } else n = this.clone(), n.__dir__ *= -1;return n;
    }, Mn.prototype.value = function () {
      var n,
          t = this.__wrapped__.value(),
          r = this.__dir__,
          e = af(t),
          u = 0 > r,
          i = e ? t.length : 0;n = i;for (var o = this.__views__, f = 0, c = -1, a = o.length; ++c < a;) {
        var l = o[c],
            s = l.size;switch (l.type) {case "drop":
            f += s;break;case "dropRight":
            n -= s;break;case "take":
            n = Mi(n, f + s);break;case "takeRight":
            f = Di(f, n - s);}
      }if (n = { start: f, end: n }, o = n.start, f = n.end, n = f - o, o = u ? f : o - 1, f = this.__iteratees__, c = f.length, a = 0, l = Mi(n, this.__takeCount__), !e || !u && i == n && l == n) return kr(t, this.__actions__);e = [];n: for (; n-- && a < l;) {
        for (o += r, u = -1, i = t[o]; ++u < c;) {
          var h = f[u],
              s = h.type,
              h = (0, h.iteratee)(i);if (2 == s) i = h;else if (!h) {
            if (1 == s) continue n;break n;
          }
        }e[a++] = i;
      }return e;
    }, On.prototype.at = Fo, On.prototype.chain = function () {
      return Xe(this);
    }, On.prototype.commit = function () {
      return new zn(this.value(), this.__chain__);
    }, On.prototype.next = function () {
      this.__values__ === F && (this.__values__ = ku(this.value()));
      var n = this.__index__ >= this.__values__.length;return { done: n, value: n ? F : this.__values__[this.__index__++] };
    }, On.prototype.plant = function (n) {
      for (var t, r = this; r instanceof Sn;) {
        var e = Pe(r);e.__index__ = 0, e.__values__ = F, t ? u.__wrapped__ = e : t = e;var u = e,
            r = r.__wrapped__;
      }return u.__wrapped__ = n, t;
    }, On.prototype.reverse = function () {
      var n = this.__wrapped__;return n instanceof Mn ? (this.__actions__.length && (n = new Mn(this)), n = n.reverse(), n.__actions__.push({ func: nu, args: [Je], thisArg: F }), new zn(n, this.__chain__)) : this.thru(Je);
    }, On.prototype.toJSON = On.prototype.valueOf = On.prototype.value = function () {
      return kr(this.__wrapped__, this.__actions__);
    }, On.prototype.first = On.prototype.head, Ai && (On.prototype[Ai] = tu), On;
  }();"function" == "function" && _typeof(__webpack_require__(5)) == "object" && __webpack_require__(5) ? (Zn._ = it, !(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
    return it;
  }).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__))) : Vn ? ((Vn.exports = it)._ = it, qn._ = it) : Zn._ = it;
}).call(undefined);
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3), __webpack_require__(4)(module)))

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * clipboard.js v2.0.0
 * https://zenorocha.github.io/clipboard.js
 * 
 * Licensed MIT © Zeno Rocha
 */
!function (t, e) {
  "object" == ( false ? "undefined" : _typeof(exports)) && "object" == ( false ? "undefined" : _typeof(module)) ? module.exports = e() :  true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? exports.ClipboardJS = e() : t.ClipboardJS = e();
}(undefined, function () {
  return function (t) {
    function e(o) {
      if (n[o]) return n[o].exports;var r = n[o] = { i: o, l: !1, exports: {} };return t[o].call(r.exports, r, r.exports, e), r.l = !0, r.exports;
    }var n = {};return e.m = t, e.c = n, e.i = function (t) {
      return t;
    }, e.d = function (t, n, o) {
      e.o(t, n) || Object.defineProperty(t, n, { configurable: !1, enumerable: !0, get: o });
    }, e.n = function (t) {
      var n = t && t.__esModule ? function () {
        return t.default;
      } : function () {
        return t;
      };return e.d(n, "a", n), n;
    }, e.o = function (t, e) {
      return Object.prototype.hasOwnProperty.call(t, e);
    }, e.p = "", e(e.s = 3);
  }([function (t, e, n) {
    var o, r, i;!function (a, c) {
      r = [t, n(7)], o = c, void 0 !== (i = "function" == typeof o ? o.apply(e, r) : o) && (t.exports = i);
    }(0, function (t, e) {
      "use strict";
      function n(t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
      }var o = function (t) {
        return t && t.__esModule ? t : { default: t };
      }(e),
          r = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (t) {
        return typeof t === "undefined" ? "undefined" : _typeof(t);
      } : function (t) {
        return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t === "undefined" ? "undefined" : _typeof(t);
      },
          i = function () {
        function t(t, e) {
          for (var n = 0; n < e.length; n++) {
            var o = e[n];o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
          }
        }return function (e, n, o) {
          return n && t(e.prototype, n), o && t(e, o), e;
        };
      }(),
          a = function () {
        function t(e) {
          n(this, t), this.resolveOptions(e), this.initSelection();
        }return i(t, [{ key: "resolveOptions", value: function value() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};this.action = t.action, this.container = t.container, this.emitter = t.emitter, this.target = t.target, this.text = t.text, this.trigger = t.trigger, this.selectedText = "";
          } }, { key: "initSelection", value: function value() {
            this.text ? this.selectFake() : this.target && this.selectTarget();
          } }, { key: "selectFake", value: function value() {
            var t = this,
                e = "rtl" == document.documentElement.getAttribute("dir");this.removeFake(), this.fakeHandlerCallback = function () {
              return t.removeFake();
            }, this.fakeHandler = this.container.addEventListener("click", this.fakeHandlerCallback) || !0, this.fakeElem = document.createElement("textarea"), this.fakeElem.style.fontSize = "12pt", this.fakeElem.style.border = "0", this.fakeElem.style.padding = "0", this.fakeElem.style.margin = "0", this.fakeElem.style.position = "absolute", this.fakeElem.style[e ? "right" : "left"] = "-9999px";var n = window.pageYOffset || document.documentElement.scrollTop;this.fakeElem.style.top = n + "px", this.fakeElem.setAttribute("readonly", ""), this.fakeElem.value = this.text, this.container.appendChild(this.fakeElem), this.selectedText = (0, o.default)(this.fakeElem), this.copyText();
          } }, { key: "removeFake", value: function value() {
            this.fakeHandler && (this.container.removeEventListener("click", this.fakeHandlerCallback), this.fakeHandler = null, this.fakeHandlerCallback = null), this.fakeElem && (this.container.removeChild(this.fakeElem), this.fakeElem = null);
          } }, { key: "selectTarget", value: function value() {
            this.selectedText = (0, o.default)(this.target), this.copyText();
          } }, { key: "copyText", value: function value() {
            var t = void 0;try {
              t = document.execCommand(this.action);
            } catch (e) {
              t = !1;
            }this.handleResult(t);
          } }, { key: "handleResult", value: function value(t) {
            this.emitter.emit(t ? "success" : "error", { action: this.action, text: this.selectedText, trigger: this.trigger, clearSelection: this.clearSelection.bind(this) });
          } }, { key: "clearSelection", value: function value() {
            this.trigger && this.trigger.focus(), window.getSelection().removeAllRanges();
          } }, { key: "destroy", value: function value() {
            this.removeFake();
          } }, { key: "action", set: function set() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "copy";if (this._action = t, "copy" !== this._action && "cut" !== this._action) throw new Error('Invalid "action" value, use either "copy" or "cut"');
          }, get: function get() {
            return this._action;
          } }, { key: "target", set: function set(t) {
            if (void 0 !== t) {
              if (!t || "object" !== (void 0 === t ? "undefined" : r(t)) || 1 !== t.nodeType) throw new Error('Invalid "target" value, use a valid Element');if ("copy" === this.action && t.hasAttribute("disabled")) throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');if ("cut" === this.action && (t.hasAttribute("readonly") || t.hasAttribute("disabled"))) throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');this._target = t;
            }
          }, get: function get() {
            return this._target;
          } }]), t;
      }();t.exports = a;
    });
  }, function (t, e, n) {
    function o(t, e, n) {
      if (!t && !e && !n) throw new Error("Missing required arguments");if (!c.string(e)) throw new TypeError("Second argument must be a String");if (!c.fn(n)) throw new TypeError("Third argument must be a Function");if (c.node(t)) return r(t, e, n);if (c.nodeList(t)) return i(t, e, n);if (c.string(t)) return a(t, e, n);throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList");
    }function r(t, e, n) {
      return t.addEventListener(e, n), { destroy: function destroy() {
          t.removeEventListener(e, n);
        } };
    }function i(t, e, n) {
      return Array.prototype.forEach.call(t, function (t) {
        t.addEventListener(e, n);
      }), { destroy: function destroy() {
          Array.prototype.forEach.call(t, function (t) {
            t.removeEventListener(e, n);
          });
        } };
    }function a(t, e, n) {
      return u(document.body, t, e, n);
    }var c = n(6),
        u = n(5);t.exports = o;
  }, function (t, e) {
    function n() {}n.prototype = { on: function on(t, e, n) {
        var o = this.e || (this.e = {});return (o[t] || (o[t] = [])).push({ fn: e, ctx: n }), this;
      }, once: function once(t, e, n) {
        function o() {
          r.off(t, o), e.apply(n, arguments);
        }var r = this;return o._ = e, this.on(t, o, n);
      }, emit: function emit(t) {
        var e = [].slice.call(arguments, 1),
            n = ((this.e || (this.e = {}))[t] || []).slice(),
            o = 0,
            r = n.length;for (o; o < r; o++) {
          n[o].fn.apply(n[o].ctx, e);
        }return this;
      }, off: function off(t, e) {
        var n = this.e || (this.e = {}),
            o = n[t],
            r = [];if (o && e) for (var i = 0, a = o.length; i < a; i++) {
          o[i].fn !== e && o[i].fn._ !== e && r.push(o[i]);
        }return r.length ? n[t] = r : delete n[t], this;
      } }, t.exports = n;
  }, function (t, e, n) {
    var o, r, i;!function (a, c) {
      r = [t, n(0), n(2), n(1)], o = c, void 0 !== (i = "function" == typeof o ? o.apply(e, r) : o) && (t.exports = i);
    }(0, function (t, e, n, o) {
      "use strict";
      function r(t) {
        return t && t.__esModule ? t : { default: t };
      }function i(t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
      }function a(t, e) {
        if (!t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return !e || "object" != (typeof e === "undefined" ? "undefined" : _typeof(e)) && "function" != typeof e ? t : e;
      }function c(t, e) {
        if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + (typeof e === "undefined" ? "undefined" : _typeof(e)));t.prototype = Object.create(e && e.prototype, { constructor: { value: t, enumerable: !1, writable: !0, configurable: !0 } }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e);
      }function u(t, e) {
        var n = "data-clipboard-" + t;if (e.hasAttribute(n)) return e.getAttribute(n);
      }var l = r(e),
          s = r(n),
          f = r(o),
          d = "function" == typeof Symbol && "symbol" == _typeof(Symbol.iterator) ? function (t) {
        return typeof t === "undefined" ? "undefined" : _typeof(t);
      } : function (t) {
        return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t === "undefined" ? "undefined" : _typeof(t);
      },
          h = function () {
        function t(t, e) {
          for (var n = 0; n < e.length; n++) {
            var o = e[n];o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(t, o.key, o);
          }
        }return function (e, n, o) {
          return n && t(e.prototype, n), o && t(e, o), e;
        };
      }(),
          p = function (t) {
        function e(t, n) {
          i(this, e);var o = a(this, (e.__proto__ || Object.getPrototypeOf(e)).call(this));return o.resolveOptions(n), o.listenClick(t), o;
        }return c(e, t), h(e, [{ key: "resolveOptions", value: function value() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};this.action = "function" == typeof t.action ? t.action : this.defaultAction, this.target = "function" == typeof t.target ? t.target : this.defaultTarget, this.text = "function" == typeof t.text ? t.text : this.defaultText, this.container = "object" === d(t.container) ? t.container : document.body;
          } }, { key: "listenClick", value: function value(t) {
            var e = this;this.listener = (0, f.default)(t, "click", function (t) {
              return e.onClick(t);
            });
          } }, { key: "onClick", value: function value(t) {
            var e = t.delegateTarget || t.currentTarget;this.clipboardAction && (this.clipboardAction = null), this.clipboardAction = new l.default({ action: this.action(e), target: this.target(e), text: this.text(e), container: this.container, trigger: e, emitter: this });
          } }, { key: "defaultAction", value: function value(t) {
            return u("action", t);
          } }, { key: "defaultTarget", value: function value(t) {
            var e = u("target", t);if (e) return document.querySelector(e);
          } }, { key: "defaultText", value: function value(t) {
            return u("text", t);
          } }, { key: "destroy", value: function value() {
            this.listener.destroy(), this.clipboardAction && (this.clipboardAction.destroy(), this.clipboardAction = null);
          } }], [{ key: "isSupported", value: function value() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : ["copy", "cut"],
                e = "string" == typeof t ? [t] : t,
                n = !!document.queryCommandSupported;return e.forEach(function (t) {
              n = n && !!document.queryCommandSupported(t);
            }), n;
          } }]), e;
      }(s.default);t.exports = p;
    });
  }, function (t, e) {
    function n(t, e) {
      for (; t && t.nodeType !== o;) {
        if ("function" == typeof t.matches && t.matches(e)) return t;t = t.parentNode;
      }
    }var o = 9;if ("undefined" != typeof Element && !Element.prototype.matches) {
      var r = Element.prototype;r.matches = r.matchesSelector || r.mozMatchesSelector || r.msMatchesSelector || r.oMatchesSelector || r.webkitMatchesSelector;
    }t.exports = n;
  }, function (t, e, n) {
    function o(t, e, n, o, r) {
      var a = i.apply(this, arguments);return t.addEventListener(n, a, r), { destroy: function destroy() {
          t.removeEventListener(n, a, r);
        } };
    }function r(t, e, n, r, i) {
      return "function" == typeof t.addEventListener ? o.apply(null, arguments) : "function" == typeof n ? o.bind(null, document).apply(null, arguments) : ("string" == typeof t && (t = document.querySelectorAll(t)), Array.prototype.map.call(t, function (t) {
        return o(t, e, n, r, i);
      }));
    }function i(t, e, n, o) {
      return function (n) {
        n.delegateTarget = a(n.target, e), n.delegateTarget && o.call(t, n);
      };
    }var a = n(4);t.exports = r;
  }, function (t, e) {
    e.node = function (t) {
      return void 0 !== t && t instanceof HTMLElement && 1 === t.nodeType;
    }, e.nodeList = function (t) {
      var n = Object.prototype.toString.call(t);return void 0 !== t && ("[object NodeList]" === n || "[object HTMLCollection]" === n) && "length" in t && (0 === t.length || e.node(t[0]));
    }, e.string = function (t) {
      return "string" == typeof t || t instanceof String;
    }, e.fn = function (t) {
      return "[object Function]" === Object.prototype.toString.call(t);
    };
  }, function (t, e) {
    function n(t) {
      var e;if ("SELECT" === t.nodeName) t.focus(), e = t.value;else if ("INPUT" === t.nodeName || "TEXTAREA" === t.nodeName) {
        var n = t.hasAttribute("readonly");n || t.setAttribute("readonly", ""), t.select(), t.setSelectionRange(0, t.value.length), n || t.removeAttribute("readonly"), e = t.value;
      } else {
        t.hasAttribute("contenteditable") && t.focus();var o = window.getSelection(),
            r = document.createRange();r.selectNodeContents(t), o.removeAllRanges(), o.addRange(r), e = o.toString();
      }return e;
    }t.exports = n;
  }]);
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(4)(module)))

/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*! jQuery UI - v1.12.1+CommonJS - 2018-02-10
 * http://jqueryui.com
 * Includes: widget.js
 * Copyright jQuery Foundation and other contributors; Licensed MIT */

(function (factory) {
  if (true) {

    // AMD. Register as an anonymous module.
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(2)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === "object") {

    // Node/CommonJS
    factory(require("jquery"));
  } else {

    // Browser globals
    factory(jQuery);
  }
})(function ($) {

  $.ui = $.ui || {};

  var version = $.ui.version = "1.12.1";

  /*!
   * jQuery UI Widget 1.12.1
   * http://jqueryui.com
   *
   * Copyright jQuery Foundation and other contributors
   * Released under the MIT license.
   * http://jquery.org/license
   */

  //>>label: Widget
  //>>group: Core
  //>>description: Provides a factory for creating stateful widgets with a common API.
  //>>docs: http://api.jqueryui.com/jQuery.widget/
  //>>demos: http://jqueryui.com/widget/


  var widgetUuid = 0;
  var widgetSlice = Array.prototype.slice;

  $.cleanData = function (orig) {
    return function (elems) {
      var events, elem, i;
      for (i = 0; (elem = elems[i]) != null; i++) {
        try {

          // Only trigger remove when necessary to save time
          events = $._data(elem, "events");
          if (events && events.remove) {
            $(elem).triggerHandler("remove");
          }

          // Http://bugs.jquery.com/ticket/8235
        } catch (e) {}
      }
      orig(elems);
    };
  }($.cleanData);

  $.widget = function (name, base, prototype) {
    var existingConstructor, constructor, basePrototype;

    // ProxiedPrototype allows the provided prototype to remain unmodified
    // so that it can be used as a mixin for multiple widgets (#8876)
    var proxiedPrototype = {};

    var namespace = name.split(".")[0];
    name = name.split(".")[1];
    var fullName = namespace + "-" + name;

    if (!prototype) {
      prototype = base;
      base = $.Widget;
    }

    if ($.isArray(prototype)) {
      prototype = $.extend.apply(null, [{}].concat(prototype));
    }

    // Create selector for plugin
    $.expr[":"][fullName.toLowerCase()] = function (elem) {
      return !!$.data(elem, fullName);
    };

    $[namespace] = $[namespace] || {};
    existingConstructor = $[namespace][name];
    constructor = $[namespace][name] = function (options, element) {

      // Allow instantiation without "new" keyword
      if (!this._createWidget) {
        return new constructor(options, element);
      }

      // Allow instantiation without initializing for simple inheritance
      // must use "new" keyword (the code above always passes args)
      if (arguments.length) {
        this._createWidget(options, element);
      }
    };

    // Extend with the existing constructor to carry over any static properties
    $.extend(constructor, existingConstructor, {
      version: prototype.version,

      // Copy the object used to create the prototype in case we need to
      // redefine the widget later
      _proto: $.extend({}, prototype),

      // Track widgets that inherit from this widget in case this widget is
      // redefined after a widget inherits from it
      _childConstructors: []
    });

    basePrototype = new base();

    // We need to make the options hash a property directly on the new instance
    // otherwise we'll modify the options hash on the prototype that we're
    // inheriting from
    basePrototype.options = $.widget.extend({}, basePrototype.options);
    $.each(prototype, function (prop, value) {
      if (!$.isFunction(value)) {
        proxiedPrototype[prop] = value;
        return;
      }
      proxiedPrototype[prop] = function () {
        function _super() {
          return base.prototype[prop].apply(this, arguments);
        }

        function _superApply(args) {
          return base.prototype[prop].apply(this, args);
        }

        return function () {
          var __super = this._super;
          var __superApply = this._superApply;
          var returnValue;

          this._super = _super;
          this._superApply = _superApply;

          returnValue = value.apply(this, arguments);

          this._super = __super;
          this._superApply = __superApply;

          return returnValue;
        };
      }();
    });
    constructor.prototype = $.widget.extend(basePrototype, {

      // TODO: remove support for widgetEventPrefix
      // always use the name + a colon as the prefix, e.g., draggable:start
      // don't prefix for widgets that aren't DOM-based
      widgetEventPrefix: existingConstructor ? basePrototype.widgetEventPrefix || name : name
    }, proxiedPrototype, {
      constructor: constructor,
      namespace: namespace,
      widgetName: name,
      widgetFullName: fullName
    });

    // If this widget is being redefined then we need to find all widgets that
    // are inheriting from it and redefine all of them so that they inherit from
    // the new version of this widget. We're essentially trying to replace one
    // level in the prototype chain.
    if (existingConstructor) {
      $.each(existingConstructor._childConstructors, function (i, child) {
        var childPrototype = child.prototype;

        // Redefine the child widget using the same prototype that was
        // originally used, but inherit from the new version of the base
        $.widget(childPrototype.namespace + "." + childPrototype.widgetName, constructor, child._proto);
      });

      // Remove the list of existing child constructors from the old constructor
      // so the old child constructors can be garbage collected
      delete existingConstructor._childConstructors;
    } else {
      base._childConstructors.push(constructor);
    }

    $.widget.bridge(name, constructor);

    return constructor;
  };

  $.widget.extend = function (target) {
    var input = widgetSlice.call(arguments, 1);
    var inputIndex = 0;
    var inputLength = input.length;
    var key;
    var value;

    for (; inputIndex < inputLength; inputIndex++) {
      for (key in input[inputIndex]) {
        value = input[inputIndex][key];
        if (input[inputIndex].hasOwnProperty(key) && value !== undefined) {

          // Clone objects
          if ($.isPlainObject(value)) {
            target[key] = $.isPlainObject(target[key]) ? $.widget.extend({}, target[key], value) :

            // Don't extend strings, arrays, etc. with objects
            $.widget.extend({}, value);

            // Copy everything else by reference
          } else {
            target[key] = value;
          }
        }
      }
    }
    return target;
  };

  $.widget.bridge = function (name, object) {
    var fullName = object.prototype.widgetFullName || name;
    $.fn[name] = function (options) {
      var isMethodCall = typeof options === "string";
      var args = widgetSlice.call(arguments, 1);
      var returnValue = this;

      if (isMethodCall) {

        // If this is an empty collection, we need to have the instance method
        // return undefined instead of the jQuery instance
        if (!this.length && options === "instance") {
          returnValue = undefined;
        } else {
          this.each(function () {
            var methodValue;
            var instance = $.data(this, fullName);

            if (options === "instance") {
              returnValue = instance;
              return false;
            }

            if (!instance) {
              return $.error("cannot call methods on " + name + " prior to initialization; " + "attempted to call method '" + options + "'");
            }

            if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
              return $.error("no such method '" + options + "' for " + name + " widget instance");
            }

            methodValue = instance[options].apply(instance, args);

            if (methodValue !== instance && methodValue !== undefined) {
              returnValue = methodValue && methodValue.jquery ? returnValue.pushStack(methodValue.get()) : methodValue;
              return false;
            }
          });
        }
      } else {

        // Allow multiple hashes to be passed on init
        if (args.length) {
          options = $.widget.extend.apply(null, [options].concat(args));
        }

        this.each(function () {
          var instance = $.data(this, fullName);
          if (instance) {
            instance.option(options || {});
            if (instance._init) {
              instance._init();
            }
          } else {
            $.data(this, fullName, new object(options, this));
          }
        });
      }

      return returnValue;
    };
  };

  $.Widget = function () /* options, element */{};
  $.Widget._childConstructors = [];

  $.Widget.prototype = {
    widgetName: "widget",
    widgetEventPrefix: "",
    defaultElement: "<div>",

    options: {
      classes: {},
      disabled: false,

      // Callbacks
      create: null
    },

    _createWidget: function _createWidget(options, element) {
      element = $(element || this.defaultElement || this)[0];
      this.element = $(element);
      this.uuid = widgetUuid++;
      this.eventNamespace = "." + this.widgetName + this.uuid;

      this.bindings = $();
      this.hoverable = $();
      this.focusable = $();
      this.classesElementLookup = {};

      if (element !== this) {
        $.data(element, this.widgetFullName, this);
        this._on(true, this.element, {
          remove: function remove(event) {
            if (event.target === element) {
              this.destroy();
            }
          }
        });
        this.document = $(element.style ?

        // Element within the document
        element.ownerDocument :

        // Element is window or document
        element.document || element);
        this.window = $(this.document[0].defaultView || this.document[0].parentWindow);
      }

      this.options = $.widget.extend({}, this.options, this._getCreateOptions(), options);

      this._create();

      if (this.options.disabled) {
        this._setOptionDisabled(this.options.disabled);
      }

      this._trigger("create", null, this._getCreateEventData());
      this._init();
    },

    _getCreateOptions: function _getCreateOptions() {
      return {};
    },

    _getCreateEventData: $.noop,

    _create: $.noop,

    _init: $.noop,

    destroy: function destroy() {
      var that = this;

      this._destroy();
      $.each(this.classesElementLookup, function (key, value) {
        that._removeClass(value, key);
      });

      // We can probably remove the unbind calls in 2.0
      // all event bindings should go through this._on()
      this.element.off(this.eventNamespace).removeData(this.widgetFullName);
      this.widget().off(this.eventNamespace).removeAttr("aria-disabled");

      // Clean up events and states
      this.bindings.off(this.eventNamespace);
    },

    _destroy: $.noop,

    widget: function widget() {
      return this.element;
    },

    option: function option(key, value) {
      var options = key;
      var parts;
      var curOption;
      var i;

      if (arguments.length === 0) {

        // Don't return a reference to the internal hash
        return $.widget.extend({}, this.options);
      }

      if (typeof key === "string") {

        // Handle nested keys, e.g., "foo.bar" => { foo: { bar: ___ } }
        options = {};
        parts = key.split(".");
        key = parts.shift();
        if (parts.length) {
          curOption = options[key] = $.widget.extend({}, this.options[key]);
          for (i = 0; i < parts.length - 1; i++) {
            curOption[parts[i]] = curOption[parts[i]] || {};
            curOption = curOption[parts[i]];
          }
          key = parts.pop();
          if (arguments.length === 1) {
            return curOption[key] === undefined ? null : curOption[key];
          }
          curOption[key] = value;
        } else {
          if (arguments.length === 1) {
            return this.options[key] === undefined ? null : this.options[key];
          }
          options[key] = value;
        }
      }

      this._setOptions(options);

      return this;
    },

    _setOptions: function _setOptions(options) {
      var key;

      for (key in options) {
        this._setOption(key, options[key]);
      }

      return this;
    },

    _setOption: function _setOption(key, value) {
      if (key === "classes") {
        this._setOptionClasses(value);
      }

      this.options[key] = value;

      if (key === "disabled") {
        this._setOptionDisabled(value);
      }

      return this;
    },

    _setOptionClasses: function _setOptionClasses(value) {
      var classKey, elements, currentElements;

      for (classKey in value) {
        currentElements = this.classesElementLookup[classKey];
        if (value[classKey] === this.options.classes[classKey] || !currentElements || !currentElements.length) {
          continue;
        }

        // We are doing this to create a new jQuery object because the _removeClass() call
        // on the next line is going to destroy the reference to the current elements being
        // tracked. We need to save a copy of this collection so that we can add the new classes
        // below.
        elements = $(currentElements.get());
        this._removeClass(currentElements, classKey);

        // We don't use _addClass() here, because that uses this.options.classes
        // for generating the string of classes. We want to use the value passed in from
        // _setOption(), this is the new value of the classes option which was passed to
        // _setOption(). We pass this value directly to _classes().
        elements.addClass(this._classes({
          element: elements,
          keys: classKey,
          classes: value,
          add: true
        }));
      }
    },

    _setOptionDisabled: function _setOptionDisabled(value) {
      this._toggleClass(this.widget(), this.widgetFullName + "-disabled", null, !!value);

      // If the widget is becoming disabled, then nothing is interactive
      if (value) {
        this._removeClass(this.hoverable, null, "ui-state-hover");
        this._removeClass(this.focusable, null, "ui-state-focus");
      }
    },

    enable: function enable() {
      return this._setOptions({ disabled: false });
    },

    disable: function disable() {
      return this._setOptions({ disabled: true });
    },

    _classes: function _classes(options) {
      var full = [];
      var that = this;

      options = $.extend({
        element: this.element,
        classes: this.options.classes || {}
      }, options);

      function processClassString(classes, checkOption) {
        var current, i;
        for (i = 0; i < classes.length; i++) {
          current = that.classesElementLookup[classes[i]] || $();
          if (options.add) {
            current = $($.unique(current.get().concat(options.element.get())));
          } else {
            current = $(current.not(options.element).get());
          }
          that.classesElementLookup[classes[i]] = current;
          full.push(classes[i]);
          if (checkOption && options.classes[classes[i]]) {
            full.push(options.classes[classes[i]]);
          }
        }
      }

      this._on(options.element, {
        "remove": "_untrackClassesElement"
      });

      if (options.keys) {
        processClassString(options.keys.match(/\S+/g) || [], true);
      }
      if (options.extra) {
        processClassString(options.extra.match(/\S+/g) || []);
      }

      return full.join(" ");
    },

    _untrackClassesElement: function _untrackClassesElement(event) {
      var that = this;
      $.each(that.classesElementLookup, function (key, value) {
        if ($.inArray(event.target, value) !== -1) {
          that.classesElementLookup[key] = $(value.not(event.target).get());
        }
      });
    },

    _removeClass: function _removeClass(element, keys, extra) {
      return this._toggleClass(element, keys, extra, false);
    },

    _addClass: function _addClass(element, keys, extra) {
      return this._toggleClass(element, keys, extra, true);
    },

    _toggleClass: function _toggleClass(element, keys, extra, add) {
      add = typeof add === "boolean" ? add : extra;
      var shift = typeof element === "string" || element === null,
          options = {
        extra: shift ? keys : extra,
        keys: shift ? element : keys,
        element: shift ? this.element : element,
        add: add
      };
      options.element.toggleClass(this._classes(options), add);
      return this;
    },

    _on: function _on(suppressDisabledCheck, element, handlers) {
      var delegateElement;
      var instance = this;

      // No suppressDisabledCheck flag, shuffle arguments
      if (typeof suppressDisabledCheck !== "boolean") {
        handlers = element;
        element = suppressDisabledCheck;
        suppressDisabledCheck = false;
      }

      // No element argument, shuffle and use this.element
      if (!handlers) {
        handlers = element;
        element = this.element;
        delegateElement = this.widget();
      } else {
        element = delegateElement = $(element);
        this.bindings = this.bindings.add(element);
      }

      $.each(handlers, function (event, handler) {
        function handlerProxy() {

          // Allow widgets to customize the disabled handling
          // - disabled as an array instead of boolean
          // - disabled class as method for disabling individual parts
          if (!suppressDisabledCheck && (instance.options.disabled === true || $(this).hasClass("ui-state-disabled"))) {
            return;
          }
          return (typeof handler === "string" ? instance[handler] : handler).apply(instance, arguments);
        }

        // Copy the guid so direct unbinding works
        if (typeof handler !== "string") {
          handlerProxy.guid = handler.guid = handler.guid || handlerProxy.guid || $.guid++;
        }

        var match = event.match(/^([\w:-]*)\s*(.*)$/);
        var eventName = match[1] + instance.eventNamespace;
        var selector = match[2];

        if (selector) {
          delegateElement.on(eventName, selector, handlerProxy);
        } else {
          element.on(eventName, handlerProxy);
        }
      });
    },

    _off: function _off(element, eventName) {
      eventName = (eventName || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace;
      element.off(eventName).off(eventName);

      // Clear the stack to avoid memory leaks (#10056)
      this.bindings = $(this.bindings.not(element).get());
      this.focusable = $(this.focusable.not(element).get());
      this.hoverable = $(this.hoverable.not(element).get());
    },

    _delay: function _delay(handler, delay) {
      function handlerProxy() {
        return (typeof handler === "string" ? instance[handler] : handler).apply(instance, arguments);
      }
      var instance = this;
      return setTimeout(handlerProxy, delay || 0);
    },

    _hoverable: function _hoverable(element) {
      this.hoverable = this.hoverable.add(element);
      this._on(element, {
        mouseenter: function mouseenter(event) {
          this._addClass($(event.currentTarget), null, "ui-state-hover");
        },
        mouseleave: function mouseleave(event) {
          this._removeClass($(event.currentTarget), null, "ui-state-hover");
        }
      });
    },

    _focusable: function _focusable(element) {
      this.focusable = this.focusable.add(element);
      this._on(element, {
        focusin: function focusin(event) {
          this._addClass($(event.currentTarget), null, "ui-state-focus");
        },
        focusout: function focusout(event) {
          this._removeClass($(event.currentTarget), null, "ui-state-focus");
        }
      });
    },

    _trigger: function _trigger(type, event, data) {
      var prop, orig;
      var callback = this.options[type];

      data = data || {};
      event = $.Event(event);
      event.type = (type === this.widgetEventPrefix ? type : this.widgetEventPrefix + type).toLowerCase();

      // The original event may come from any element
      // so we need to reset the target on the new event
      event.target = this.element[0];

      // Copy original event properties over to the new event
      orig = event.originalEvent;
      if (orig) {
        for (prop in orig) {
          if (!(prop in event)) {
            event[prop] = orig[prop];
          }
        }
      }

      this.element.trigger(event, data);
      return !($.isFunction(callback) && callback.apply(this.element[0], [event].concat(data)) === false || event.isDefaultPrevented());
    }
  };

  $.each({ show: "fadeIn", hide: "fadeOut" }, function (method, defaultEffect) {
    $.Widget.prototype["_" + method] = function (element, options, callback) {
      if (typeof options === "string") {
        options = { effect: options };
      }

      var hasOptions;
      var effectName = !options ? method : options === true || typeof options === "number" ? defaultEffect : options.effect || defaultEffect;

      options = options || {};
      if (typeof options === "number") {
        options = { duration: options };
      }

      hasOptions = !$.isEmptyObject(options);
      options.complete = callback;

      if (options.delay) {
        element.delay(options.delay);
      }

      if (hasOptions && $.effects && $.effects.effect[effectName]) {
        element[method](options);
      } else if (effectName !== method && element[effectName]) {
        element[effectName](options.duration, options.easing, callback);
      } else {
        element.queue(function (next) {
          $(this)[method]();
          if (callback) {
            callback.call(element[0]);
          }
          next();
        });
      }
    };
  });

  var widget = $.widget;
});

/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
 * jQuery File Upload Plugin
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* jshint nomen:false */
/* global define, require, window, document, location, Blob, FormData */

;(function (factory) {
    'use strict';
    // Browser globals:

    factory(window.jQuery);
})(function ($) {
    'use strict';

    // Detect file input support, based on
    // http://viljamis.com/blog/2012/file-upload-support-on-mobile/

    $.support.fileInput = !(new RegExp(
    // Handle devices which give false positives for the feature detection:
    '(Android (1\\.[0156]|2\\.[01]))' + '|(Windows Phone (OS 7|8\\.0))|(XBLWP)|(ZuneWP)|(WPDesktop)' + '|(w(eb)?OSBrowser)|(webOS)' + '|(Kindle/(1\\.0|2\\.[05]|3\\.0))').test(window.navigator.userAgent) ||
    // Feature detection for all other devices:
    $('<input type="file"/>').prop('disabled'));

    // The FileReader API is not actually used, but works as feature detection,
    // as some Safari versions (5?) support XHR file uploads via the FormData API,
    // but not non-multipart XHR file uploads.
    // window.XMLHttpRequestUpload is not available on IE10, so we check for
    // window.ProgressEvent instead to detect XHR2 file upload capability:
    $.support.xhrFileUpload = !!(window.ProgressEvent && window.FileReader);
    $.support.xhrFormDataFileUpload = !!window.FormData;

    // Detect support for Blob slicing (required for chunked uploads):
    $.support.blobSlice = window.Blob && (Blob.prototype.slice || Blob.prototype.webkitSlice || Blob.prototype.mozSlice);

    // Helper function to create drag handlers for dragover/dragenter/dragleave:
    function getDragHandler(type) {
        var isDragOver = type === 'dragover';
        return function (e) {
            e.dataTransfer = e.originalEvent && e.originalEvent.dataTransfer;
            var dataTransfer = e.dataTransfer;
            if (dataTransfer && $.inArray('Files', dataTransfer.types) !== -1 && this._trigger(type, $.Event(type, { delegatedEvent: e })) !== false) {
                e.preventDefault();
                if (isDragOver) {
                    dataTransfer.dropEffect = 'copy';
                }
            }
        };
    }

    // The fileupload widget listens for change events on file input fields defined
    // via fileInput setting and paste or drop events of the given dropZone.
    // In addition to the default jQuery Widget methods, the fileupload widget
    // exposes the "add" and "send" methods, to add or directly send files using
    // the fileupload API.
    // By default, files added via file input selection, paste, drag & drop or
    // "add" method are uploaded immediately, but it is possible to override
    // the "add" callback option to queue file uploads.
    $.widget('blueimp.fileupload', {

        options: {
            // The drop target element(s), by the default the complete document.
            // Set to null to disable drag & drop support:
            dropZone: $(document),
            // The paste target element(s), by the default undefined.
            // Set to a DOM node or jQuery object to enable file pasting:
            pasteZone: undefined,
            // The file input field(s), that are listened to for change events.
            // If undefined, it is set to the file input fields inside
            // of the widget element on plugin initialization.
            // Set to null to disable the change listener.
            fileInput: undefined,
            // By default, the file input field is replaced with a clone after
            // each input field change event. This is required for iframe transport
            // queues and allows change events to be fired for the same file
            // selection, but can be disabled by setting the following option to false:
            replaceFileInput: true,
            // The parameter name for the file form data (the request argument name).
            // If undefined or empty, the name property of the file input field is
            // used, or "files[]" if the file input name property is also empty,
            // can be a string or an array of strings:
            paramName: undefined,
            // By default, each file of a selection is uploaded using an individual
            // request for XHR type uploads. Set to false to upload file
            // selections in one request each:
            singleFileUploads: true,
            // To limit the number of files uploaded with one XHR request,
            // set the following option to an integer greater than 0:
            limitMultiFileUploads: undefined,
            // The following option limits the number of files uploaded with one
            // XHR request to keep the request size under or equal to the defined
            // limit in bytes:
            limitMultiFileUploadSize: undefined,
            // Multipart file uploads add a number of bytes to each uploaded file,
            // therefore the following option adds an overhead for each file used
            // in the limitMultiFileUploadSize configuration:
            limitMultiFileUploadSizeOverhead: 512,
            // Set the following option to true to issue all file upload requests
            // in a sequential order:
            sequentialUploads: false,
            // To limit the number of concurrent uploads,
            // set the following option to an integer greater than 0:
            limitConcurrentUploads: undefined,
            // Set the following option to true to force iframe transport uploads:
            forceIframeTransport: false,
            // Set the following option to the location of a redirect url on the
            // origin server, for cross-domain iframe transport uploads:
            redirect: undefined,
            // The parameter name for the redirect url, sent as part of the form
            // data and set to 'redirect' if this option is empty:
            redirectParamName: undefined,
            // Set the following option to the location of a postMessage window,
            // to enable postMessage transport uploads:
            postMessage: undefined,
            // By default, XHR file uploads are sent as multipart/form-data.
            // The iframe transport is always using multipart/form-data.
            // Set to false to enable non-multipart XHR uploads:
            multipart: true,
            // To upload large files in smaller chunks, set the following option
            // to a preferred maximum chunk size. If set to 0, null or undefined,
            // or the browser does not support the required Blob API, files will
            // be uploaded as a whole.
            maxChunkSize: undefined,
            // When a non-multipart upload or a chunked multipart upload has been
            // aborted, this option can be used to resume the upload by setting
            // it to the size of the already uploaded bytes. This option is most
            // useful when modifying the options object inside of the "add" or
            // "send" callbacks, as the options are cloned for each file upload.
            uploadedBytes: undefined,
            // By default, failed (abort or error) file uploads are removed from the
            // global progress calculation. Set the following option to false to
            // prevent recalculating the global progress data:
            recalculateProgress: true,
            // Interval in milliseconds to calculate and trigger progress events:
            progressInterval: 100,
            // Interval in milliseconds to calculate progress bitrate:
            bitrateInterval: 500,
            // By default, uploads are started automatically when adding files:
            autoUpload: true,

            // Error and info messages:
            messages: {
                uploadedBytes: 'Uploaded bytes exceed file size'
            },

            // Translation function, gets the message key to be translated
            // and an object with context specific data as arguments:
            i18n: function i18n(message, context) {
                message = this.messages[message] || message.toString();
                if (context) {
                    $.each(context, function (key, value) {
                        message = message.replace('{' + key + '}', value);
                    });
                }
                return message;
            },

            // Additional form data to be sent along with the file uploads can be set
            // using this option, which accepts an array of objects with name and
            // value properties, a function returning such an array, a FormData
            // object (for XHR file uploads), or a simple object.
            // The form of the first fileInput is given as parameter to the function:
            formData: function formData(form) {
                return form.serializeArray();
            },

            // The add callback is invoked as soon as files are added to the fileupload
            // widget (via file input selection, drag & drop, paste or add API call).
            // If the singleFileUploads option is enabled, this callback will be
            // called once for each file in the selection for XHR file uploads, else
            // once for each file selection.
            //
            // The upload starts when the submit method is invoked on the data parameter.
            // The data object contains a files property holding the added files
            // and allows you to override plugin options as well as define ajax settings.
            //
            // Listeners for this callback can also be bound the following way:
            // .bind('fileuploadadd', func);
            //
            // data.submit() returns a Promise object and allows to attach additional
            // handlers using jQuery's Deferred callbacks:
            // data.submit().done(func).fail(func).always(func);
            add: function add(e, data) {
                if (e.isDefaultPrevented()) {
                    return false;
                }
                if (data.autoUpload || data.autoUpload !== false && $(this).fileupload('option', 'autoUpload')) {
                    data.process().done(function () {
                        data.submit();
                    });
                }
            },

            // Other callbacks:

            // Callback for the submit event of each file upload:
            // submit: function (e, data) {}, // .bind('fileuploadsubmit', func);

            // Callback for the start of each file upload request:
            // send: function (e, data) {}, // .bind('fileuploadsend', func);

            // Callback for successful uploads:
            // done: function (e, data) {}, // .bind('fileuploaddone', func);

            // Callback for failed (abort or error) uploads:
            // fail: function (e, data) {}, // .bind('fileuploadfail', func);

            // Callback for completed (success, abort or error) requests:
            // always: function (e, data) {}, // .bind('fileuploadalways', func);

            // Callback for upload progress events:
            // progress: function (e, data) {}, // .bind('fileuploadprogress', func);

            // Callback for global upload progress events:
            // progressall: function (e, data) {}, // .bind('fileuploadprogressall', func);

            // Callback for uploads start, equivalent to the global ajaxStart event:
            // start: function (e) {}, // .bind('fileuploadstart', func);

            // Callback for uploads stop, equivalent to the global ajaxStop event:
            // stop: function (e) {}, // .bind('fileuploadstop', func);

            // Callback for change events of the fileInput(s):
            // change: function (e, data) {}, // .bind('fileuploadchange', func);

            // Callback for paste events to the pasteZone(s):
            // paste: function (e, data) {}, // .bind('fileuploadpaste', func);

            // Callback for drop events of the dropZone(s):
            // drop: function (e, data) {}, // .bind('fileuploaddrop', func);

            // Callback for dragover events of the dropZone(s):
            // dragover: function (e) {}, // .bind('fileuploaddragover', func);

            // Callback before the start of each chunk upload request (before form data initialization):
            // chunkbeforesend: function (e, data) {}, // .bind('fileuploadchunkbeforesend', func);

            // Callback for the start of each chunk upload request:
            // chunksend: function (e, data) {}, // .bind('fileuploadchunksend', func);

            // Callback for successful chunk uploads:
            // chunkdone: function (e, data) {}, // .bind('fileuploadchunkdone', func);

            // Callback for failed (abort or error) chunk uploads:
            // chunkfail: function (e, data) {}, // .bind('fileuploadchunkfail', func);

            // Callback for completed (success, abort or error) chunk upload requests:
            // chunkalways: function (e, data) {}, // .bind('fileuploadchunkalways', func);

            // The plugin options are used as settings object for the ajax calls.
            // The following are jQuery ajax settings required for the file uploads:
            processData: false,
            contentType: false,
            cache: false,
            timeout: 0
        },

        // A list of options that require reinitializing event listeners and/or
        // special initialization code:
        _specialOptions: ['fileInput', 'dropZone', 'pasteZone', 'multipart', 'forceIframeTransport'],

        _blobSlice: $.support.blobSlice && function () {
            var slice = this.slice || this.webkitSlice || this.mozSlice;
            return slice.apply(this, arguments);
        },

        _BitrateTimer: function _BitrateTimer() {
            this.timestamp = Date.now ? Date.now() : new Date().getTime();
            this.loaded = 0;
            this.bitrate = 0;
            this.getBitrate = function (now, loaded, interval) {
                var timeDiff = now - this.timestamp;
                if (!this.bitrate || !interval || timeDiff > interval) {
                    this.bitrate = (loaded - this.loaded) * (1000 / timeDiff) * 8;
                    this.loaded = loaded;
                    this.timestamp = now;
                }
                return this.bitrate;
            };
        },

        _isXHRUpload: function _isXHRUpload(options) {
            return !options.forceIframeTransport && (!options.multipart && $.support.xhrFileUpload || $.support.xhrFormDataFileUpload);
        },

        _getFormData: function _getFormData(options) {
            var formData;
            if ($.type(options.formData) === 'function') {
                return options.formData(options.form);
            }
            if ($.isArray(options.formData)) {
                return options.formData;
            }
            if ($.type(options.formData) === 'object') {
                formData = [];
                $.each(options.formData, function (name, value) {
                    formData.push({ name: name, value: value });
                });
                return formData;
            }
            return [];
        },

        _getTotal: function _getTotal(files) {
            var total = 0;
            $.each(files, function (index, file) {
                total += file.size || 1;
            });
            return total;
        },

        _initProgressObject: function _initProgressObject(obj) {
            var progress = {
                loaded: 0,
                total: 0,
                bitrate: 0
            };
            if (obj._progress) {
                $.extend(obj._progress, progress);
            } else {
                obj._progress = progress;
            }
        },

        _initResponseObject: function _initResponseObject(obj) {
            var prop;
            if (obj._response) {
                for (prop in obj._response) {
                    if (obj._response.hasOwnProperty(prop)) {
                        delete obj._response[prop];
                    }
                }
            } else {
                obj._response = {};
            }
        },

        _onProgress: function _onProgress(e, data) {
            if (e.lengthComputable) {
                var now = Date.now ? Date.now() : new Date().getTime(),
                    loaded;
                if (data._time && data.progressInterval && now - data._time < data.progressInterval && e.loaded !== e.total) {
                    return;
                }
                data._time = now;
                loaded = Math.floor(e.loaded / e.total * (data.chunkSize || data._progress.total)) + (data.uploadedBytes || 0);
                // Add the difference from the previously loaded state
                // to the global loaded counter:
                this._progress.loaded += loaded - data._progress.loaded;
                this._progress.bitrate = this._bitrateTimer.getBitrate(now, this._progress.loaded, data.bitrateInterval);
                data._progress.loaded = data.loaded = loaded;
                data._progress.bitrate = data.bitrate = data._bitrateTimer.getBitrate(now, loaded, data.bitrateInterval);
                // Trigger a custom progress event with a total data property set
                // to the file size(s) of the current upload and a loaded data
                // property calculated accordingly:
                this._trigger('progress', $.Event('progress', { delegatedEvent: e }), data);
                // Trigger a global progress event for all current file uploads,
                // including ajax calls queued for sequential file uploads:
                this._trigger('progressall', $.Event('progressall', { delegatedEvent: e }), this._progress);
            }
        },

        _initProgressListener: function _initProgressListener(options) {
            var that = this,
                xhr = options.xhr ? options.xhr() : $.ajaxSettings.xhr();
            // Accesss to the native XHR object is required to add event listeners
            // for the upload progress event:
            if (xhr.upload) {
                $(xhr.upload).bind('progress', function (e) {
                    var oe = e.originalEvent;
                    // Make sure the progress event properties get copied over:
                    e.lengthComputable = oe.lengthComputable;
                    e.loaded = oe.loaded;
                    e.total = oe.total;
                    that._onProgress(e, options);
                });
                options.xhr = function () {
                    return xhr;
                };
            }
        },

        _deinitProgressListener: function _deinitProgressListener(options) {
            var xhr = options.xhr ? options.xhr() : $.ajaxSettings.xhr();
            if (xhr.upload) {
                $(xhr.upload).unbind('progress');
            }
        },

        _isInstanceOf: function _isInstanceOf(type, obj) {
            // Cross-frame instanceof check
            return Object.prototype.toString.call(obj) === '[object ' + type + ']';
        },

        _initXHRData: function _initXHRData(options) {
            var that = this,
                formData,
                file = options.files[0],

            // Ignore non-multipart setting if not supported:
            multipart = options.multipart || !$.support.xhrFileUpload,
                paramName = $.type(options.paramName) === 'array' ? options.paramName[0] : options.paramName;
            options.headers = $.extend({}, options.headers);
            if (options.contentRange) {
                options.headers['Content-Range'] = options.contentRange;
            }
            if (!multipart || options.blob || !this._isInstanceOf('File', file)) {
                options.headers['Content-Disposition'] = 'attachment; filename="' + encodeURI(file.uploadName || file.name) + '"';
            }
            if (!multipart) {
                options.contentType = file.type || 'application/octet-stream';
                options.data = options.blob || file;
            } else if ($.support.xhrFormDataFileUpload) {
                if (options.postMessage) {
                    // window.postMessage does not allow sending FormData
                    // objects, so we just add the File/Blob objects to
                    // the formData array and let the postMessage window
                    // create the FormData object out of this array:
                    formData = this._getFormData(options);
                    if (options.blob) {
                        formData.push({
                            name: paramName,
                            value: options.blob
                        });
                    } else {
                        $.each(options.files, function (index, file) {
                            formData.push({
                                name: $.type(options.paramName) === 'array' && options.paramName[index] || paramName,
                                value: file
                            });
                        });
                    }
                } else {
                    if (that._isInstanceOf('FormData', options.formData)) {
                        formData = options.formData;
                    } else {
                        formData = new FormData();
                        $.each(this._getFormData(options), function (index, field) {
                            formData.append(field.name, field.value);
                        });
                    }
                    if (options.blob) {
                        formData.append(paramName, options.blob, file.uploadName || file.name);
                    } else {
                        $.each(options.files, function (index, file) {
                            // This check allows the tests to run with
                            // dummy objects:
                            if (that._isInstanceOf('File', file) || that._isInstanceOf('Blob', file)) {
                                formData.append($.type(options.paramName) === 'array' && options.paramName[index] || paramName, file, file.uploadName || file.name);
                            }
                        });
                    }
                }
                options.data = formData;
            }
            // Blob reference is not needed anymore, free memory:
            options.blob = null;
        },

        _initIframeSettings: function _initIframeSettings(options) {
            var targetHost = $('<a></a>').prop('href', options.url).prop('host');
            // Setting the dataType to iframe enables the iframe transport:
            options.dataType = 'iframe ' + (options.dataType || '');
            // The iframe transport accepts a serialized array as form data:
            options.formData = this._getFormData(options);
            // Add redirect url to form data on cross-domain uploads:
            if (options.redirect && targetHost && targetHost !== location.host) {
                options.formData.push({
                    name: options.redirectParamName || 'redirect',
                    value: options.redirect
                });
            }
        },

        _initDataSettings: function _initDataSettings(options) {
            if (this._isXHRUpload(options)) {
                if (!this._chunkedUpload(options, true)) {
                    if (!options.data) {
                        this._initXHRData(options);
                    }
                    this._initProgressListener(options);
                }
                if (options.postMessage) {
                    // Setting the dataType to postmessage enables the
                    // postMessage transport:
                    options.dataType = 'postmessage ' + (options.dataType || '');
                }
            } else {
                this._initIframeSettings(options);
            }
        },

        _getParamName: function _getParamName(options) {
            var fileInput = $(options.fileInput),
                paramName = options.paramName;
            if (!paramName) {
                paramName = [];
                fileInput.each(function () {
                    var input = $(this),
                        name = input.prop('name') || 'files[]',
                        i = (input.prop('files') || [1]).length;
                    while (i) {
                        paramName.push(name);
                        i -= 1;
                    }
                });
                if (!paramName.length) {
                    paramName = [fileInput.prop('name') || 'files[]'];
                }
            } else if (!$.isArray(paramName)) {
                paramName = [paramName];
            }
            return paramName;
        },

        _initFormSettings: function _initFormSettings(options) {
            // Retrieve missing options from the input field and the
            // associated form, if available:
            if (!options.form || !options.form.length) {
                options.form = $(options.fileInput.prop('form'));
                // If the given file input doesn't have an associated form,
                // use the default widget file input's form:
                if (!options.form.length) {
                    options.form = $(this.options.fileInput.prop('form'));
                }
            }
            options.paramName = this._getParamName(options);
            if (!options.url) {
                options.url = options.form.prop('action') || location.href;
            }
            // The HTTP request method must be "POST" or "PUT":
            options.type = (options.type || $.type(options.form.prop('method')) === 'string' && options.form.prop('method') || '').toUpperCase();
            if (options.type !== 'POST' && options.type !== 'PUT' && options.type !== 'PATCH') {
                options.type = 'POST';
            }
            if (!options.formAcceptCharset) {
                options.formAcceptCharset = options.form.attr('accept-charset');
            }
        },

        _getAJAXSettings: function _getAJAXSettings(data) {
            var options = $.extend({}, this.options, data);
            this._initFormSettings(options);
            this._initDataSettings(options);
            return options;
        },

        // jQuery 1.6 doesn't provide .state(),
        // while jQuery 1.8+ removed .isRejected() and .isResolved():
        _getDeferredState: function _getDeferredState(deferred) {
            if (deferred.state) {
                return deferred.state();
            }
            if (deferred.isResolved()) {
                return 'resolved';
            }
            if (deferred.isRejected()) {
                return 'rejected';
            }
            return 'pending';
        },

        // Maps jqXHR callbacks to the equivalent
        // methods of the given Promise object:
        _enhancePromise: function _enhancePromise(promise) {
            promise.success = promise.done;
            promise.error = promise.fail;
            promise.complete = promise.always;
            return promise;
        },

        // Creates and returns a Promise object enhanced with
        // the jqXHR methods abort, success, error and complete:
        _getXHRPromise: function _getXHRPromise(resolveOrReject, context, args) {
            var dfd = $.Deferred(),
                promise = dfd.promise();
            context = context || this.options.context || promise;
            if (resolveOrReject === true) {
                dfd.resolveWith(context, args);
            } else if (resolveOrReject === false) {
                dfd.rejectWith(context, args);
            }
            promise.abort = dfd.promise;
            return this._enhancePromise(promise);
        },

        // Adds convenience methods to the data callback argument:
        _addConvenienceMethods: function _addConvenienceMethods(e, data) {
            var that = this,
                getPromise = function getPromise(args) {
                return $.Deferred().resolveWith(that, args).promise();
            };
            data.process = function (resolveFunc, rejectFunc) {
                if (resolveFunc || rejectFunc) {
                    data._processQueue = this._processQueue = (this._processQueue || getPromise([this])).then(function () {
                        if (data.errorThrown) {
                            return $.Deferred().rejectWith(that, [data]).promise();
                        }
                        return getPromise(arguments);
                    }).then(resolveFunc, rejectFunc);
                }
                return this._processQueue || getPromise([this]);
            };
            data.submit = function () {
                if (this.state() !== 'pending') {
                    data.jqXHR = this.jqXHR = that._trigger('submit', $.Event('submit', { delegatedEvent: e }), this) !== false && that._onSend(e, this);
                }
                return this.jqXHR || that._getXHRPromise();
            };
            data.abort = function () {
                if (this.jqXHR) {
                    return this.jqXHR.abort();
                }
                this.errorThrown = 'abort';
                that._trigger('fail', null, this);
                return that._getXHRPromise(false);
            };
            data.state = function () {
                if (this.jqXHR) {
                    return that._getDeferredState(this.jqXHR);
                }
                if (this._processQueue) {
                    return that._getDeferredState(this._processQueue);
                }
            };
            data.processing = function () {
                return !this.jqXHR && this._processQueue && that._getDeferredState(this._processQueue) === 'pending';
            };
            data.progress = function () {
                return this._progress;
            };
            data.response = function () {
                return this._response;
            };
        },

        // Parses the Range header from the server response
        // and returns the uploaded bytes:
        _getUploadedBytes: function _getUploadedBytes(jqXHR) {
            var range = jqXHR.getResponseHeader('Range'),
                parts = range && range.split('-'),
                upperBytesPos = parts && parts.length > 1 && parseInt(parts[1], 10);
            return upperBytesPos && upperBytesPos + 1;
        },

        // Uploads a file in multiple, sequential requests
        // by splitting the file up in multiple blob chunks.
        // If the second parameter is true, only tests if the file
        // should be uploaded in chunks, but does not invoke any
        // upload requests:
        _chunkedUpload: function _chunkedUpload(options, testOnly) {
            options.uploadedBytes = options.uploadedBytes || 0;
            var that = this,
                file = options.files[0],
                fs = file.size,
                ub = options.uploadedBytes,
                mcs = options.maxChunkSize || fs,
                slice = this._blobSlice,
                dfd = $.Deferred(),
                promise = dfd.promise(),
                jqXHR,
                _upload;
            if (!(this._isXHRUpload(options) && slice && (ub || ($.type(mcs) === 'function' ? mcs(options) : mcs) < fs)) || options.data) {
                return false;
            }
            if (testOnly) {
                return true;
            }
            if (ub >= fs) {
                file.error = options.i18n('uploadedBytes');
                return this._getXHRPromise(false, options.context, [null, 'error', file.error]);
            }
            // The chunk upload method:
            _upload = function upload() {
                // Clone the options object for each chunk upload:
                var o = $.extend({}, options),
                    currentLoaded = o._progress.loaded;
                o.blob = slice.call(file, ub, ub + ($.type(mcs) === 'function' ? mcs(o) : mcs), file.type);
                // Store the current chunk size, as the blob itself
                // will be dereferenced after data processing:
                o.chunkSize = o.blob.size;
                // Expose the chunk bytes position range:
                o.contentRange = 'bytes ' + ub + '-' + (ub + o.chunkSize - 1) + '/' + fs;
                // Trigger chunkbeforesend to allow form data to be updated for this chunk
                that._trigger('chunkbeforesend', null, o);
                // Process the upload data (the blob and potential form data):
                that._initXHRData(o);
                // Add progress listeners for this chunk upload:
                that._initProgressListener(o);
                jqXHR = (that._trigger('chunksend', null, o) !== false && $.ajax(o) || that._getXHRPromise(false, o.context)).done(function (result, textStatus, jqXHR) {
                    ub = that._getUploadedBytes(jqXHR) || ub + o.chunkSize;
                    // Create a progress event if no final progress event
                    // with loaded equaling total has been triggered
                    // for this chunk:
                    if (currentLoaded + o.chunkSize - o._progress.loaded) {
                        that._onProgress($.Event('progress', {
                            lengthComputable: true,
                            loaded: ub - o.uploadedBytes,
                            total: ub - o.uploadedBytes
                        }), o);
                    }
                    options.uploadedBytes = o.uploadedBytes = ub;
                    o.result = result;
                    o.textStatus = textStatus;
                    o.jqXHR = jqXHR;
                    that._trigger('chunkdone', null, o);
                    that._trigger('chunkalways', null, o);
                    if (ub < fs) {
                        // File upload not yet complete,
                        // continue with the next chunk:
                        _upload();
                    } else {
                        dfd.resolveWith(o.context, [result, textStatus, jqXHR]);
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    o.jqXHR = jqXHR;
                    o.textStatus = textStatus;
                    o.errorThrown = errorThrown;
                    that._trigger('chunkfail', null, o);
                    that._trigger('chunkalways', null, o);
                    dfd.rejectWith(o.context, [jqXHR, textStatus, errorThrown]);
                }).always(function () {
                    that._deinitProgressListener(o);
                });
            };
            this._enhancePromise(promise);
            promise.abort = function () {
                return jqXHR.abort();
            };
            _upload();
            return promise;
        },

        _beforeSend: function _beforeSend(e, data) {
            if (this._active === 0) {
                // the start callback is triggered when an upload starts
                // and no other uploads are currently running,
                // equivalent to the global ajaxStart event:
                this._trigger('start');
                // Set timer for global bitrate progress calculation:
                this._bitrateTimer = new this._BitrateTimer();
                // Reset the global progress values:
                this._progress.loaded = this._progress.total = 0;
                this._progress.bitrate = 0;
            }
            // Make sure the container objects for the .response() and
            // .progress() methods on the data object are available
            // and reset to their initial state:
            this._initResponseObject(data);
            this._initProgressObject(data);
            data._progress.loaded = data.loaded = data.uploadedBytes || 0;
            data._progress.total = data.total = this._getTotal(data.files) || 1;
            data._progress.bitrate = data.bitrate = 0;
            this._active += 1;
            // Initialize the global progress values:
            this._progress.loaded += data.loaded;
            this._progress.total += data.total;
        },

        _onDone: function _onDone(result, textStatus, jqXHR, options) {
            var total = options._progress.total,
                response = options._response;
            if (options._progress.loaded < total) {
                // Create a progress event if no final progress event
                // with loaded equaling total has been triggered:
                this._onProgress($.Event('progress', {
                    lengthComputable: true,
                    loaded: total,
                    total: total
                }), options);
            }
            response.result = options.result = result;
            response.textStatus = options.textStatus = textStatus;
            response.jqXHR = options.jqXHR = jqXHR;
            this._trigger('done', null, options);
        },

        _onFail: function _onFail(jqXHR, textStatus, errorThrown, options) {
            var response = options._response;
            if (options.recalculateProgress) {
                // Remove the failed (error or abort) file upload from
                // the global progress calculation:
                this._progress.loaded -= options._progress.loaded;
                this._progress.total -= options._progress.total;
            }
            response.jqXHR = options.jqXHR = jqXHR;
            response.textStatus = options.textStatus = textStatus;
            response.errorThrown = options.errorThrown = errorThrown;
            this._trigger('fail', null, options);
        },

        _onAlways: function _onAlways(jqXHRorResult, textStatus, jqXHRorError, options) {
            // jqXHRorResult, textStatus and jqXHRorError are added to the
            // options object via done and fail callbacks
            this._trigger('always', null, options);
        },

        _onSend: function _onSend(e, data) {
            if (!data.submit) {
                this._addConvenienceMethods(e, data);
            }
            var that = this,
                jqXHR,
                aborted,
                slot,
                pipe,
                options = that._getAJAXSettings(data),
                send = function send() {
                that._sending += 1;
                // Set timer for bitrate progress calculation:
                options._bitrateTimer = new that._BitrateTimer();
                jqXHR = jqXHR || ((aborted || that._trigger('send', $.Event('send', { delegatedEvent: e }), options) === false) && that._getXHRPromise(false, options.context, aborted) || that._chunkedUpload(options) || $.ajax(options)).done(function (result, textStatus, jqXHR) {
                    that._onDone(result, textStatus, jqXHR, options);
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    that._onFail(jqXHR, textStatus, errorThrown, options);
                }).always(function (jqXHRorResult, textStatus, jqXHRorError) {
                    that._deinitProgressListener(options);
                    that._onAlways(jqXHRorResult, textStatus, jqXHRorError, options);
                    that._sending -= 1;
                    that._active -= 1;
                    if (options.limitConcurrentUploads && options.limitConcurrentUploads > that._sending) {
                        // Start the next queued upload,
                        // that has not been aborted:
                        var nextSlot = that._slots.shift();
                        while (nextSlot) {
                            if (that._getDeferredState(nextSlot) === 'pending') {
                                nextSlot.resolve();
                                break;
                            }
                            nextSlot = that._slots.shift();
                        }
                    }
                    if (that._active === 0) {
                        // The stop callback is triggered when all uploads have
                        // been completed, equivalent to the global ajaxStop event:
                        that._trigger('stop');
                    }
                });
                return jqXHR;
            };
            this._beforeSend(e, options);
            if (this.options.sequentialUploads || this.options.limitConcurrentUploads && this.options.limitConcurrentUploads <= this._sending) {
                if (this.options.limitConcurrentUploads > 1) {
                    slot = $.Deferred();
                    this._slots.push(slot);
                    pipe = slot.then(send);
                } else {
                    this._sequence = this._sequence.then(send, send);
                    pipe = this._sequence;
                }
                // Return the piped Promise object, enhanced with an abort method,
                // which is delegated to the jqXHR object of the current upload,
                // and jqXHR callbacks mapped to the equivalent Promise methods:
                pipe.abort = function () {
                    aborted = [undefined, 'abort', 'abort'];
                    if (!jqXHR) {
                        if (slot) {
                            slot.rejectWith(options.context, aborted);
                        }
                        return send();
                    }
                    return jqXHR.abort();
                };
                return this._enhancePromise(pipe);
            }
            return send();
        },

        _onAdd: function _onAdd(e, data) {
            var that = this,
                result = true,
                options = $.extend({}, this.options, data),
                files = data.files,
                filesLength = files.length,
                limit = options.limitMultiFileUploads,
                limitSize = options.limitMultiFileUploadSize,
                overhead = options.limitMultiFileUploadSizeOverhead,
                batchSize = 0,
                paramName = this._getParamName(options),
                paramNameSet,
                paramNameSlice,
                fileSet,
                i,
                j = 0;
            if (!filesLength) {
                return false;
            }
            if (limitSize && files[0].size === undefined) {
                limitSize = undefined;
            }
            if (!(options.singleFileUploads || limit || limitSize) || !this._isXHRUpload(options)) {
                fileSet = [files];
                paramNameSet = [paramName];
            } else if (!(options.singleFileUploads || limitSize) && limit) {
                fileSet = [];
                paramNameSet = [];
                for (i = 0; i < filesLength; i += limit) {
                    fileSet.push(files.slice(i, i + limit));
                    paramNameSlice = paramName.slice(i, i + limit);
                    if (!paramNameSlice.length) {
                        paramNameSlice = paramName;
                    }
                    paramNameSet.push(paramNameSlice);
                }
            } else if (!options.singleFileUploads && limitSize) {
                fileSet = [];
                paramNameSet = [];
                for (i = 0; i < filesLength; i = i + 1) {
                    batchSize += files[i].size + overhead;
                    if (i + 1 === filesLength || batchSize + files[i + 1].size + overhead > limitSize || limit && i + 1 - j >= limit) {
                        fileSet.push(files.slice(j, i + 1));
                        paramNameSlice = paramName.slice(j, i + 1);
                        if (!paramNameSlice.length) {
                            paramNameSlice = paramName;
                        }
                        paramNameSet.push(paramNameSlice);
                        j = i + 1;
                        batchSize = 0;
                    }
                }
            } else {
                paramNameSet = paramName;
            }
            data.originalFiles = files;
            $.each(fileSet || files, function (index, element) {
                var newData = $.extend({}, data);
                newData.files = fileSet ? element : [element];
                newData.paramName = paramNameSet[index];
                that._initResponseObject(newData);
                that._initProgressObject(newData);
                that._addConvenienceMethods(e, newData);
                result = that._trigger('add', $.Event('add', { delegatedEvent: e }), newData);
                return result;
            });
            return result;
        },

        _replaceFileInput: function _replaceFileInput(data) {
            var input = data.fileInput,
                inputClone = input.clone(true),
                restoreFocus = input.is(document.activeElement);
            // Add a reference for the new cloned file input to the data argument:
            data.fileInputClone = inputClone;
            $('<form></form>').append(inputClone)[0].reset();
            // Detaching allows to insert the fileInput on another form
            // without loosing the file input value:
            input.after(inputClone).detach();
            // If the fileInput had focus before it was detached,
            // restore focus to the inputClone.
            if (restoreFocus) {
                inputClone.focus();
            }
            // Avoid memory leaks with the detached file input:
            $.cleanData(input.unbind('remove'));
            // Replace the original file input element in the fileInput
            // elements set with the clone, which has been copied including
            // event handlers:
            this.options.fileInput = this.options.fileInput.map(function (i, el) {
                if (el === input[0]) {
                    return inputClone[0];
                }
                return el;
            });
            // If the widget has been initialized on the file input itself,
            // override this.element with the file input clone:
            if (input[0] === this.element[0]) {
                this.element = inputClone;
            }
        },

        _handleFileTreeEntry: function _handleFileTreeEntry(entry, path) {
            var that = this,
                dfd = $.Deferred(),
                entries = [],
                dirReader,
                errorHandler = function errorHandler(e) {
                if (e && !e.entry) {
                    e.entry = entry;
                }
                // Since $.when returns immediately if one
                // Deferred is rejected, we use resolve instead.
                // This allows valid files and invalid items
                // to be returned together in one set:
                dfd.resolve([e]);
            },
                successHandler = function successHandler(entries) {
                that._handleFileTreeEntries(entries, path + entry.name + '/').done(function (files) {
                    dfd.resolve(files);
                }).fail(errorHandler);
            },
                readEntries = function readEntries() {
                dirReader.readEntries(function (results) {
                    if (!results.length) {
                        successHandler(entries);
                    } else {
                        entries = entries.concat(results);
                        readEntries();
                    }
                }, errorHandler);
            };
            path = path || '';
            if (entry.isFile) {
                if (entry._file) {
                    // Workaround for Chrome bug #149735
                    entry._file.relativePath = path;
                    dfd.resolve(entry._file);
                } else {
                    entry.file(function (file) {
                        file.relativePath = path;
                        dfd.resolve(file);
                    }, errorHandler);
                }
            } else if (entry.isDirectory) {
                dirReader = entry.createReader();
                readEntries();
            } else {
                // Return an empty list for file system items
                // other than files or directories:
                dfd.resolve([]);
            }
            return dfd.promise();
        },

        _handleFileTreeEntries: function _handleFileTreeEntries(entries, path) {
            var that = this;
            return $.when.apply($, $.map(entries, function (entry) {
                return that._handleFileTreeEntry(entry, path);
            })).then(function () {
                return Array.prototype.concat.apply([], arguments);
            });
        },

        _getDroppedFiles: function _getDroppedFiles(dataTransfer) {
            dataTransfer = dataTransfer || {};
            var items = dataTransfer.items;
            if (items && items.length && (items[0].webkitGetAsEntry || items[0].getAsEntry)) {
                return this._handleFileTreeEntries($.map(items, function (item) {
                    var entry;
                    if (item.webkitGetAsEntry) {
                        entry = item.webkitGetAsEntry();
                        if (entry) {
                            // Workaround for Chrome bug #149735:
                            entry._file = item.getAsFile();
                        }
                        return entry;
                    }
                    return item.getAsEntry();
                }));
            }
            return $.Deferred().resolve($.makeArray(dataTransfer.files)).promise();
        },

        _getSingleFileInputFiles: function _getSingleFileInputFiles(fileInput) {
            fileInput = $(fileInput);
            var entries = fileInput.prop('webkitEntries') || fileInput.prop('entries'),
                files,
                value;
            if (entries && entries.length) {
                return this._handleFileTreeEntries(entries);
            }
            files = $.makeArray(fileInput.prop('files'));
            if (!files.length) {
                value = fileInput.prop('value');
                if (!value) {
                    return $.Deferred().resolve([]).promise();
                }
                // If the files property is not available, the browser does not
                // support the File API and we add a pseudo File object with
                // the input value as name with path information removed:
                files = [{ name: value.replace(/^.*\\/, '') }];
            } else if (files[0].name === undefined && files[0].fileName) {
                // File normalization for Safari 4 and Firefox 3:
                $.each(files, function (index, file) {
                    file.name = file.fileName;
                    file.size = file.fileSize;
                });
            }
            return $.Deferred().resolve(files).promise();
        },

        _getFileInputFiles: function _getFileInputFiles(fileInput) {
            if (!(fileInput instanceof $) || fileInput.length === 1) {
                return this._getSingleFileInputFiles(fileInput);
            }
            return $.when.apply($, $.map(fileInput, this._getSingleFileInputFiles)).then(function () {
                return Array.prototype.concat.apply([], arguments);
            });
        },

        _onChange: function _onChange(e) {
            var that = this,
                data = {
                fileInput: $(e.target),
                form: $(e.target.form)
            };
            this._getFileInputFiles(data.fileInput).always(function (files) {
                data.files = files;
                if (that.options.replaceFileInput) {
                    that._replaceFileInput(data);
                }
                if (that._trigger('change', $.Event('change', { delegatedEvent: e }), data) !== false) {
                    that._onAdd(e, data);
                }
            });
        },

        _onPaste: function _onPaste(e) {
            var items = e.originalEvent && e.originalEvent.clipboardData && e.originalEvent.clipboardData.items,
                data = { files: [] };
            if (items && items.length) {
                $.each(items, function (index, item) {
                    var file = item.getAsFile && item.getAsFile();
                    if (file) {
                        data.files.push(file);
                    }
                });
                if (this._trigger('paste', $.Event('paste', { delegatedEvent: e }), data) !== false) {
                    this._onAdd(e, data);
                }
            }
        },

        _onDrop: function _onDrop(e) {
            e.dataTransfer = e.originalEvent && e.originalEvent.dataTransfer;
            var that = this,
                dataTransfer = e.dataTransfer,
                data = {};
            if (dataTransfer && dataTransfer.files && dataTransfer.files.length) {
                e.preventDefault();
                this._getDroppedFiles(dataTransfer).always(function (files) {
                    data.files = files;
                    if (that._trigger('drop', $.Event('drop', { delegatedEvent: e }), data) !== false) {
                        that._onAdd(e, data);
                    }
                });
            }
        },

        _onDragOver: getDragHandler('dragover'),

        _onDragEnter: getDragHandler('dragenter'),

        _onDragLeave: getDragHandler('dragleave'),

        _initEventHandlers: function _initEventHandlers() {
            if (this._isXHRUpload(this.options)) {
                this._on(this.options.dropZone, {
                    dragover: this._onDragOver,
                    drop: this._onDrop,
                    // event.preventDefault() on dragenter is required for IE10+:
                    dragenter: this._onDragEnter,
                    // dragleave is not required, but added for completeness:
                    dragleave: this._onDragLeave
                });
                this._on(this.options.pasteZone, {
                    paste: this._onPaste
                });
            }
            if ($.support.fileInput) {
                this._on(this.options.fileInput, {
                    change: this._onChange
                });
            }
        },

        _destroyEventHandlers: function _destroyEventHandlers() {
            this._off(this.options.dropZone, 'dragenter dragleave dragover drop');
            this._off(this.options.pasteZone, 'paste');
            this._off(this.options.fileInput, 'change');
        },

        _destroy: function _destroy() {
            this._destroyEventHandlers();
        },

        _setOption: function _setOption(key, value) {
            var reinit = $.inArray(key, this._specialOptions) !== -1;
            if (reinit) {
                this._destroyEventHandlers();
            }
            this._super(key, value);
            if (reinit) {
                this._initSpecialOptions();
                this._initEventHandlers();
            }
        },

        _initSpecialOptions: function _initSpecialOptions() {
            var options = this.options;
            if (options.fileInput === undefined) {
                options.fileInput = this.element.is('input[type="file"]') ? this.element : this.element.find('input[type="file"]');
            } else if (!(options.fileInput instanceof $)) {
                options.fileInput = $(options.fileInput);
            }
            if (!(options.dropZone instanceof $)) {
                options.dropZone = $(options.dropZone);
            }
            if (!(options.pasteZone instanceof $)) {
                options.pasteZone = $(options.pasteZone);
            }
        },

        _getRegExp: function _getRegExp(str) {
            var parts = str.split('/'),
                modifiers = parts.pop();
            parts.shift();
            return new RegExp(parts.join('/'), modifiers);
        },

        _isRegExpOption: function _isRegExpOption(key, value) {
            return key !== 'url' && $.type(value) === 'string' && /^\/.*\/[igm]{0,3}$/.test(value);
        },

        _initDataAttributes: function _initDataAttributes() {
            var that = this,
                options = this.options,
                data = this.element.data();
            // Initialize options set via HTML5 data-attributes:
            $.each(this.element[0].attributes, function (index, attr) {
                var key = attr.name.toLowerCase(),
                    value;
                if (/^data-/.test(key)) {
                    // Convert hyphen-ated key to camelCase:
                    key = key.slice(5).replace(/-[a-z]/g, function (str) {
                        return str.charAt(1).toUpperCase();
                    });
                    value = data[key];
                    if (that._isRegExpOption(key, value)) {
                        value = that._getRegExp(value);
                    }
                    options[key] = value;
                }
            });
        },

        _create: function _create() {
            this._initDataAttributes();
            this._initSpecialOptions();
            this._slots = [];
            this._sequence = this._getXHRPromise(true);
            this._sending = this._active = 0;
            this._initProgressObject(this);
            this._initEventHandlers();
        },

        // This method is exposed to the widget API and allows to query
        // the number of active uploads:
        active: function active() {
            return this._active;
        },

        // This method is exposed to the widget API and allows to query
        // the widget upload progress.
        // It returns an object with loaded, total and bitrate properties
        // for the running uploads:
        progress: function progress() {
            return this._progress;
        },

        // This method is exposed to the widget API and allows adding files
        // using the fileupload API. The data parameter accepts an object which
        // must have a files property and can contain additional options:
        // .fileupload('add', {files: filesList});
        add: function add(data) {
            var that = this;
            if (!data || this.options.disabled) {
                return;
            }
            if (data.fileInput && !data.files) {
                this._getFileInputFiles(data.fileInput).always(function (files) {
                    data.files = files;
                    that._onAdd(null, data);
                });
            } else {
                data.files = $.makeArray(data.files);
                this._onAdd(null, data);
            }
        },

        // This method is exposed to the widget API and allows sending files
        // using the fileupload API. The data parameter accepts an object which
        // must have a files or fileInput property and can contain additional options:
        // .fileupload('send', {files: filesList});
        // The method returns a Promise object for the file upload call.
        send: function send(data) {
            if (data && !this.options.disabled) {
                if (data.fileInput && !data.files) {
                    var that = this,
                        dfd = $.Deferred(),
                        promise = dfd.promise(),
                        jqXHR,
                        aborted;
                    promise.abort = function () {
                        aborted = true;
                        if (jqXHR) {
                            return jqXHR.abort();
                        }
                        dfd.reject(null, 'abort', 'abort');
                        return promise;
                    };
                    this._getFileInputFiles(data.fileInput).always(function (files) {
                        if (aborted) {
                            return;
                        }
                        if (!files.length) {
                            dfd.reject();
                            return;
                        }
                        data.files = files;
                        jqXHR = that._onSend(null, data);
                        jqXHR.then(function (result, textStatus, jqXHR) {
                            dfd.resolve(result, textStatus, jqXHR);
                        }, function (jqXHR, textStatus, errorThrown) {
                            dfd.reject(jqXHR, textStatus, errorThrown);
                        });
                    });
                    return this._enhancePromise(promise);
                }
                data.files = $.makeArray(data.files);
                if (data.files.length) {
                    return this._onSend(null, data);
                }
            }
            return this._getXHRPromise(false, data && data.context);
        }

    });
});

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
  * Bootstrap v4.3.1 (https://getbootstrap.com/)
  * Copyright 2011-2019 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
  * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  */
!function (t, e) {
  "object" == ( false ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? e(exports, __webpack_require__(2)) :  true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports, __webpack_require__(2)], __WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : e((t = t || self).bootstrap = {}, t.jQuery);
}(undefined, function (t, p) {
  "use strict";
  function i(t, e) {
    for (var n = 0; n < e.length; n++) {
      var i = e[n];i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
    }
  }function s(t, e, n) {
    return e && i(t.prototype, e), n && i(t, n), t;
  }function l(o) {
    for (var t = 1; t < arguments.length; t++) {
      var r = null != arguments[t] ? arguments[t] : {},
          e = Object.keys(r);"function" == typeof Object.getOwnPropertySymbols && (e = e.concat(Object.getOwnPropertySymbols(r).filter(function (t) {
        return Object.getOwnPropertyDescriptor(r, t).enumerable;
      }))), e.forEach(function (t) {
        var e, n, i;e = o, i = r[n = t], n in e ? Object.defineProperty(e, n, { value: i, enumerable: !0, configurable: !0, writable: !0 }) : e[n] = i;
      });
    }return o;
  }p = p && p.hasOwnProperty("default") ? p.default : p;var e = "transitionend";function n(t) {
    var e = this,
        n = !1;return p(this).one(m.TRANSITION_END, function () {
      n = !0;
    }), setTimeout(function () {
      n || m.triggerTransitionEnd(e);
    }, t), this;
  }var m = { TRANSITION_END: "bsTransitionEnd", getUID: function getUID(t) {
      for (; t += ~~(1e6 * Math.random()), document.getElementById(t);) {}return t;
    }, getSelectorFromElement: function getSelectorFromElement(t) {
      var e = t.getAttribute("data-target");if (!e || "#" === e) {
        var n = t.getAttribute("href");e = n && "#" !== n ? n.trim() : "";
      }try {
        return document.querySelector(e) ? e : null;
      } catch (t) {
        return null;
      }
    }, getTransitionDurationFromElement: function getTransitionDurationFromElement(t) {
      if (!t) return 0;var e = p(t).css("transition-duration"),
          n = p(t).css("transition-delay"),
          i = parseFloat(e),
          o = parseFloat(n);return i || o ? (e = e.split(",")[0], n = n.split(",")[0], 1e3 * (parseFloat(e) + parseFloat(n))) : 0;
    }, reflow: function reflow(t) {
      return t.offsetHeight;
    }, triggerTransitionEnd: function triggerTransitionEnd(t) {
      p(t).trigger(e);
    }, supportsTransitionEnd: function supportsTransitionEnd() {
      return Boolean(e);
    }, isElement: function isElement(t) {
      return (t[0] || t).nodeType;
    }, typeCheckConfig: function typeCheckConfig(t, e, n) {
      for (var i in n) {
        if (Object.prototype.hasOwnProperty.call(n, i)) {
          var o = n[i],
              r = e[i],
              s = r && m.isElement(r) ? "element" : (a = r, {}.toString.call(a).match(/\s([a-z]+)/i)[1].toLowerCase());if (!new RegExp(o).test(s)) throw new Error(t.toUpperCase() + ': Option "' + i + '" provided type "' + s + '" but expected type "' + o + '".');
        }
      }var a;
    }, findShadowRoot: function findShadowRoot(t) {
      if (!document.documentElement.attachShadow) return null;if ("function" != typeof t.getRootNode) return t instanceof ShadowRoot ? t : t.parentNode ? m.findShadowRoot(t.parentNode) : null;var e = t.getRootNode();return e instanceof ShadowRoot ? e : null;
    } };p.fn.emulateTransitionEnd = n, p.event.special[m.TRANSITION_END] = { bindType: e, delegateType: e, handle: function handle(t) {
      if (p(t.target).is(this)) return t.handleObj.handler.apply(this, arguments);
    } };var o = "alert",
      r = "bs.alert",
      a = "." + r,
      c = p.fn[o],
      h = { CLOSE: "close" + a, CLOSED: "closed" + a, CLICK_DATA_API: "click" + a + ".data-api" },
      u = "alert",
      f = "fade",
      d = "show",
      g = function () {
    function i(t) {
      this._element = t;
    }var t = i.prototype;return t.close = function (t) {
      var e = this._element;t && (e = this._getRootElement(t)), this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e);
    }, t.dispose = function () {
      p.removeData(this._element, r), this._element = null;
    }, t._getRootElement = function (t) {
      var e = m.getSelectorFromElement(t),
          n = !1;return e && (n = document.querySelector(e)), n || (n = p(t).closest("." + u)[0]), n;
    }, t._triggerCloseEvent = function (t) {
      var e = p.Event(h.CLOSE);return p(t).trigger(e), e;
    }, t._removeElement = function (e) {
      var n = this;if (p(e).removeClass(d), p(e).hasClass(f)) {
        var t = m.getTransitionDurationFromElement(e);p(e).one(m.TRANSITION_END, function (t) {
          return n._destroyElement(e, t);
        }).emulateTransitionEnd(t);
      } else this._destroyElement(e);
    }, t._destroyElement = function (t) {
      p(t).detach().trigger(h.CLOSED).remove();
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = p(this),
            e = t.data(r);e || (e = new i(this), t.data(r, e)), "close" === n && e[n](this);
      });
    }, i._handleDismiss = function (e) {
      return function (t) {
        t && t.preventDefault(), e.close(this);
      };
    }, s(i, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }]), i;
  }();p(document).on(h.CLICK_DATA_API, '[data-dismiss="alert"]', g._handleDismiss(new g())), p.fn[o] = g._jQueryInterface, p.fn[o].Constructor = g, p.fn[o].noConflict = function () {
    return p.fn[o] = c, g._jQueryInterface;
  };var _ = "button",
      v = "bs.button",
      y = "." + v,
      E = ".data-api",
      b = p.fn[_],
      w = "active",
      C = "btn",
      T = "focus",
      S = '[data-toggle^="button"]',
      D = '[data-toggle="buttons"]',
      I = 'input:not([type="hidden"])',
      A = ".active",
      O = ".btn",
      N = { CLICK_DATA_API: "click" + y + E, FOCUS_BLUR_DATA_API: "focus" + y + E + " blur" + y + E },
      k = function () {
    function n(t) {
      this._element = t;
    }var t = n.prototype;return t.toggle = function () {
      var t = !0,
          e = !0,
          n = p(this._element).closest(D)[0];if (n) {
        var i = this._element.querySelector(I);if (i) {
          if ("radio" === i.type) if (i.checked && this._element.classList.contains(w)) t = !1;else {
            var o = n.querySelector(A);o && p(o).removeClass(w);
          }if (t) {
            if (i.hasAttribute("disabled") || n.hasAttribute("disabled") || i.classList.contains("disabled") || n.classList.contains("disabled")) return;i.checked = !this._element.classList.contains(w), p(i).trigger("change");
          }i.focus(), e = !1;
        }
      }e && this._element.setAttribute("aria-pressed", !this._element.classList.contains(w)), t && p(this._element).toggleClass(w);
    }, t.dispose = function () {
      p.removeData(this._element, v), this._element = null;
    }, n._jQueryInterface = function (e) {
      return this.each(function () {
        var t = p(this).data(v);t || (t = new n(this), p(this).data(v, t)), "toggle" === e && t[e]();
      });
    }, s(n, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }]), n;
  }();p(document).on(N.CLICK_DATA_API, S, function (t) {
    t.preventDefault();var e = t.target;p(e).hasClass(C) || (e = p(e).closest(O)), k._jQueryInterface.call(p(e), "toggle");
  }).on(N.FOCUS_BLUR_DATA_API, S, function (t) {
    var e = p(t.target).closest(O)[0];p(e).toggleClass(T, /^focus(in)?$/.test(t.type));
  }), p.fn[_] = k._jQueryInterface, p.fn[_].Constructor = k, p.fn[_].noConflict = function () {
    return p.fn[_] = b, k._jQueryInterface;
  };var L = "carousel",
      x = "bs.carousel",
      P = "." + x,
      H = ".data-api",
      j = p.fn[L],
      R = { interval: 5e3, keyboard: !0, slide: !1, pause: "hover", wrap: !0, touch: !0 },
      F = { interval: "(number|boolean)", keyboard: "boolean", slide: "(boolean|string)", pause: "(string|boolean)", wrap: "boolean", touch: "boolean" },
      M = "next",
      W = "prev",
      U = "left",
      B = "right",
      q = { SLIDE: "slide" + P, SLID: "slid" + P, KEYDOWN: "keydown" + P, MOUSEENTER: "mouseenter" + P, MOUSELEAVE: "mouseleave" + P, TOUCHSTART: "touchstart" + P, TOUCHMOVE: "touchmove" + P, TOUCHEND: "touchend" + P, POINTERDOWN: "pointerdown" + P, POINTERUP: "pointerup" + P, DRAG_START: "dragstart" + P, LOAD_DATA_API: "load" + P + H, CLICK_DATA_API: "click" + P + H },
      K = "carousel",
      Q = "active",
      V = "slide",
      Y = "carousel-item-right",
      z = "carousel-item-left",
      X = "carousel-item-next",
      G = "carousel-item-prev",
      $ = "pointer-event",
      J = ".active",
      Z = ".active.carousel-item",
      tt = ".carousel-item",
      et = ".carousel-item img",
      nt = ".carousel-item-next, .carousel-item-prev",
      it = ".carousel-indicators",
      ot = "[data-slide], [data-slide-to]",
      rt = '[data-ride="carousel"]',
      st = { TOUCH: "touch", PEN: "pen" },
      at = function () {
    function r(t, e) {
      this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this.touchStartX = 0, this.touchDeltaX = 0, this._config = this._getConfig(e), this._element = t, this._indicatorsElement = this._element.querySelector(it), this._touchSupported = "ontouchstart" in document.documentElement || 0 < navigator.maxTouchPoints, this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent), this._addEventListeners();
    }var t = r.prototype;return t.next = function () {
      this._isSliding || this._slide(M);
    }, t.nextWhenVisible = function () {
      !document.hidden && p(this._element).is(":visible") && "hidden" !== p(this._element).css("visibility") && this.next();
    }, t.prev = function () {
      this._isSliding || this._slide(W);
    }, t.pause = function (t) {
      t || (this._isPaused = !0), this._element.querySelector(nt) && (m.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null;
    }, t.cycle = function (t) {
      t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval));
    }, t.to = function (t) {
      var e = this;this._activeElement = this._element.querySelector(Z);var n = this._getItemIndex(this._activeElement);if (!(t > this._items.length - 1 || t < 0)) if (this._isSliding) p(this._element).one(q.SLID, function () {
        return e.to(t);
      });else {
        if (n === t) return this.pause(), void this.cycle();var i = n < t ? M : W;this._slide(i, this._items[t]);
      }
    }, t.dispose = function () {
      p(this._element).off(P), p.removeData(this._element, x), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null;
    }, t._getConfig = function (t) {
      return t = l({}, R, t), m.typeCheckConfig(L, t, F), t;
    }, t._handleSwipe = function () {
      var t = Math.abs(this.touchDeltaX);if (!(t <= 40)) {
        var e = t / this.touchDeltaX;0 < e && this.prev(), e < 0 && this.next();
      }
    }, t._addEventListeners = function () {
      var e = this;this._config.keyboard && p(this._element).on(q.KEYDOWN, function (t) {
        return e._keydown(t);
      }), "hover" === this._config.pause && p(this._element).on(q.MOUSEENTER, function (t) {
        return e.pause(t);
      }).on(q.MOUSELEAVE, function (t) {
        return e.cycle(t);
      }), this._config.touch && this._addTouchEventListeners();
    }, t._addTouchEventListeners = function () {
      var n = this;if (this._touchSupported) {
        var e = function e(t) {
          n._pointerEvent && st[t.originalEvent.pointerType.toUpperCase()] ? n.touchStartX = t.originalEvent.clientX : n._pointerEvent || (n.touchStartX = t.originalEvent.touches[0].clientX);
        },
            i = function i(t) {
          n._pointerEvent && st[t.originalEvent.pointerType.toUpperCase()] && (n.touchDeltaX = t.originalEvent.clientX - n.touchStartX), n._handleSwipe(), "hover" === n._config.pause && (n.pause(), n.touchTimeout && clearTimeout(n.touchTimeout), n.touchTimeout = setTimeout(function (t) {
            return n.cycle(t);
          }, 500 + n._config.interval));
        };p(this._element.querySelectorAll(et)).on(q.DRAG_START, function (t) {
          return t.preventDefault();
        }), this._pointerEvent ? (p(this._element).on(q.POINTERDOWN, function (t) {
          return e(t);
        }), p(this._element).on(q.POINTERUP, function (t) {
          return i(t);
        }), this._element.classList.add($)) : (p(this._element).on(q.TOUCHSTART, function (t) {
          return e(t);
        }), p(this._element).on(q.TOUCHMOVE, function (t) {
          var e;(e = t).originalEvent.touches && 1 < e.originalEvent.touches.length ? n.touchDeltaX = 0 : n.touchDeltaX = e.originalEvent.touches[0].clientX - n.touchStartX;
        }), p(this._element).on(q.TOUCHEND, function (t) {
          return i(t);
        }));
      }
    }, t._keydown = function (t) {
      if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {case 37:
          t.preventDefault(), this.prev();break;case 39:
          t.preventDefault(), this.next();}
    }, t._getItemIndex = function (t) {
      return this._items = t && t.parentNode ? [].slice.call(t.parentNode.querySelectorAll(tt)) : [], this._items.indexOf(t);
    }, t._getItemByDirection = function (t, e) {
      var n = t === M,
          i = t === W,
          o = this._getItemIndex(e),
          r = this._items.length - 1;if ((i && 0 === o || n && o === r) && !this._config.wrap) return e;var s = (o + (t === W ? -1 : 1)) % this._items.length;return -1 === s ? this._items[this._items.length - 1] : this._items[s];
    }, t._triggerSlideEvent = function (t, e) {
      var n = this._getItemIndex(t),
          i = this._getItemIndex(this._element.querySelector(Z)),
          o = p.Event(q.SLIDE, { relatedTarget: t, direction: e, from: i, to: n });return p(this._element).trigger(o), o;
    }, t._setActiveIndicatorElement = function (t) {
      if (this._indicatorsElement) {
        var e = [].slice.call(this._indicatorsElement.querySelectorAll(J));p(e).removeClass(Q);var n = this._indicatorsElement.children[this._getItemIndex(t)];n && p(n).addClass(Q);
      }
    }, t._slide = function (t, e) {
      var n,
          i,
          o,
          r = this,
          s = this._element.querySelector(Z),
          a = this._getItemIndex(s),
          l = e || s && this._getItemByDirection(t, s),
          c = this._getItemIndex(l),
          h = Boolean(this._interval);if (o = t === M ? (n = z, i = X, U) : (n = Y, i = G, B), l && p(l).hasClass(Q)) this._isSliding = !1;else if (!this._triggerSlideEvent(l, o).isDefaultPrevented() && s && l) {
        this._isSliding = !0, h && this.pause(), this._setActiveIndicatorElement(l);var u = p.Event(q.SLID, { relatedTarget: l, direction: o, from: a, to: c });if (p(this._element).hasClass(V)) {
          p(l).addClass(i), m.reflow(l), p(s).addClass(n), p(l).addClass(n);var f = parseInt(l.getAttribute("data-interval"), 10);this._config.interval = f ? (this._config.defaultInterval = this._config.defaultInterval || this._config.interval, f) : this._config.defaultInterval || this._config.interval;var d = m.getTransitionDurationFromElement(s);p(s).one(m.TRANSITION_END, function () {
            p(l).removeClass(n + " " + i).addClass(Q), p(s).removeClass(Q + " " + i + " " + n), r._isSliding = !1, setTimeout(function () {
              return p(r._element).trigger(u);
            }, 0);
          }).emulateTransitionEnd(d);
        } else p(s).removeClass(Q), p(l).addClass(Q), this._isSliding = !1, p(this._element).trigger(u);h && this.cycle();
      }
    }, r._jQueryInterface = function (i) {
      return this.each(function () {
        var t = p(this).data(x),
            e = l({}, R, p(this).data());"object" == (typeof i === "undefined" ? "undefined" : _typeof(i)) && (e = l({}, e, i));var n = "string" == typeof i ? i : e.slide;if (t || (t = new r(this, e), p(this).data(x, t)), "number" == typeof i) t.to(i);else if ("string" == typeof n) {
          if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');t[n]();
        } else e.interval && e.ride && (t.pause(), t.cycle());
      });
    }, r._dataApiClickHandler = function (t) {
      var e = m.getSelectorFromElement(this);if (e) {
        var n = p(e)[0];if (n && p(n).hasClass(K)) {
          var i = l({}, p(n).data(), p(this).data()),
              o = this.getAttribute("data-slide-to");o && (i.interval = !1), r._jQueryInterface.call(p(n), i), o && p(n).data(x).to(o), t.preventDefault();
        }
      }
    }, s(r, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }, { key: "Default", get: function get() {
        return R;
      } }]), r;
  }();p(document).on(q.CLICK_DATA_API, ot, at._dataApiClickHandler), p(window).on(q.LOAD_DATA_API, function () {
    for (var t = [].slice.call(document.querySelectorAll(rt)), e = 0, n = t.length; e < n; e++) {
      var i = p(t[e]);at._jQueryInterface.call(i, i.data());
    }
  }), p.fn[L] = at._jQueryInterface, p.fn[L].Constructor = at, p.fn[L].noConflict = function () {
    return p.fn[L] = j, at._jQueryInterface;
  };var lt = "collapse",
      ct = "bs.collapse",
      ht = "." + ct,
      ut = p.fn[lt],
      ft = { toggle: !0, parent: "" },
      dt = { toggle: "boolean", parent: "(string|element)" },
      pt = { SHOW: "show" + ht, SHOWN: "shown" + ht, HIDE: "hide" + ht, HIDDEN: "hidden" + ht, CLICK_DATA_API: "click" + ht + ".data-api" },
      mt = "show",
      gt = "collapse",
      _t = "collapsing",
      vt = "collapsed",
      yt = "width",
      Et = "height",
      bt = ".show, .collapsing",
      wt = '[data-toggle="collapse"]',
      Ct = function () {
    function a(e, t) {
      this._isTransitioning = !1, this._element = e, this._config = this._getConfig(t), this._triggerArray = [].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'));for (var n = [].slice.call(document.querySelectorAll(wt)), i = 0, o = n.length; i < o; i++) {
        var r = n[i],
            s = m.getSelectorFromElement(r),
            a = [].slice.call(document.querySelectorAll(s)).filter(function (t) {
          return t === e;
        });null !== s && 0 < a.length && (this._selector = s, this._triggerArray.push(r));
      }this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle();
    }var t = a.prototype;return t.toggle = function () {
      p(this._element).hasClass(mt) ? this.hide() : this.show();
    }, t.show = function () {
      var t,
          e,
          n = this;if (!this._isTransitioning && !p(this._element).hasClass(mt) && (this._parent && 0 === (t = [].slice.call(this._parent.querySelectorAll(bt)).filter(function (t) {
        return "string" == typeof n._config.parent ? t.getAttribute("data-parent") === n._config.parent : t.classList.contains(gt);
      })).length && (t = null), !(t && (e = p(t).not(this._selector).data(ct)) && e._isTransitioning))) {
        var i = p.Event(pt.SHOW);if (p(this._element).trigger(i), !i.isDefaultPrevented()) {
          t && (a._jQueryInterface.call(p(t).not(this._selector), "hide"), e || p(t).data(ct, null));var o = this._getDimension();p(this._element).removeClass(gt).addClass(_t), this._element.style[o] = 0, this._triggerArray.length && p(this._triggerArray).removeClass(vt).attr("aria-expanded", !0), this.setTransitioning(!0);var r = "scroll" + (o[0].toUpperCase() + o.slice(1)),
              s = m.getTransitionDurationFromElement(this._element);p(this._element).one(m.TRANSITION_END, function () {
            p(n._element).removeClass(_t).addClass(gt).addClass(mt), n._element.style[o] = "", n.setTransitioning(!1), p(n._element).trigger(pt.SHOWN);
          }).emulateTransitionEnd(s), this._element.style[o] = this._element[r] + "px";
        }
      }
    }, t.hide = function () {
      var t = this;if (!this._isTransitioning && p(this._element).hasClass(mt)) {
        var e = p.Event(pt.HIDE);if (p(this._element).trigger(e), !e.isDefaultPrevented()) {
          var n = this._getDimension();this._element.style[n] = this._element.getBoundingClientRect()[n] + "px", m.reflow(this._element), p(this._element).addClass(_t).removeClass(gt).removeClass(mt);var i = this._triggerArray.length;if (0 < i) for (var o = 0; o < i; o++) {
            var r = this._triggerArray[o],
                s = m.getSelectorFromElement(r);if (null !== s) p([].slice.call(document.querySelectorAll(s))).hasClass(mt) || p(r).addClass(vt).attr("aria-expanded", !1);
          }this.setTransitioning(!0);this._element.style[n] = "";var a = m.getTransitionDurationFromElement(this._element);p(this._element).one(m.TRANSITION_END, function () {
            t.setTransitioning(!1), p(t._element).removeClass(_t).addClass(gt).trigger(pt.HIDDEN);
          }).emulateTransitionEnd(a);
        }
      }
    }, t.setTransitioning = function (t) {
      this._isTransitioning = t;
    }, t.dispose = function () {
      p.removeData(this._element, ct), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null;
    }, t._getConfig = function (t) {
      return (t = l({}, ft, t)).toggle = Boolean(t.toggle), m.typeCheckConfig(lt, t, dt), t;
    }, t._getDimension = function () {
      return p(this._element).hasClass(yt) ? yt : Et;
    }, t._getParent = function () {
      var t,
          n = this;m.isElement(this._config.parent) ? (t = this._config.parent, "undefined" != typeof this._config.parent.jquery && (t = this._config.parent[0])) : t = document.querySelector(this._config.parent);var e = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]',
          i = [].slice.call(t.querySelectorAll(e));return p(i).each(function (t, e) {
        n._addAriaAndCollapsedClass(a._getTargetFromElement(e), [e]);
      }), t;
    }, t._addAriaAndCollapsedClass = function (t, e) {
      var n = p(t).hasClass(mt);e.length && p(e).toggleClass(vt, !n).attr("aria-expanded", n);
    }, a._getTargetFromElement = function (t) {
      var e = m.getSelectorFromElement(t);return e ? document.querySelector(e) : null;
    }, a._jQueryInterface = function (i) {
      return this.each(function () {
        var t = p(this),
            e = t.data(ct),
            n = l({}, ft, t.data(), "object" == (typeof i === "undefined" ? "undefined" : _typeof(i)) && i ? i : {});if (!e && n.toggle && /show|hide/.test(i) && (n.toggle = !1), e || (e = new a(this, n), t.data(ct, e)), "string" == typeof i) {
          if ("undefined" == typeof e[i]) throw new TypeError('No method named "' + i + '"');e[i]();
        }
      });
    }, s(a, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }, { key: "Default", get: function get() {
        return ft;
      } }]), a;
  }();p(document).on(pt.CLICK_DATA_API, wt, function (t) {
    "A" === t.currentTarget.tagName && t.preventDefault();var n = p(this),
        e = m.getSelectorFromElement(this),
        i = [].slice.call(document.querySelectorAll(e));p(i).each(function () {
      var t = p(this),
          e = t.data(ct) ? "toggle" : n.data();Ct._jQueryInterface.call(t, e);
    });
  }), p.fn[lt] = Ct._jQueryInterface, p.fn[lt].Constructor = Ct, p.fn[lt].noConflict = function () {
    return p.fn[lt] = ut, Ct._jQueryInterface;
  };for (var Tt = "undefined" != typeof window && "undefined" != typeof document, St = ["Edge", "Trident", "Firefox"], Dt = 0, It = 0; It < St.length; It += 1) {
    if (Tt && 0 <= navigator.userAgent.indexOf(St[It])) {
      Dt = 1;break;
    }
  }var At = Tt && window.Promise ? function (t) {
    var e = !1;return function () {
      e || (e = !0, window.Promise.resolve().then(function () {
        e = !1, t();
      }));
    };
  } : function (t) {
    var e = !1;return function () {
      e || (e = !0, setTimeout(function () {
        e = !1, t();
      }, Dt));
    };
  };function Ot(t) {
    return t && "[object Function]" === {}.toString.call(t);
  }function Nt(t, e) {
    if (1 !== t.nodeType) return [];var n = t.ownerDocument.defaultView.getComputedStyle(t, null);return e ? n[e] : n;
  }function kt(t) {
    return "HTML" === t.nodeName ? t : t.parentNode || t.host;
  }function Lt(t) {
    if (!t) return document.body;switch (t.nodeName) {case "HTML":case "BODY":
        return t.ownerDocument.body;case "#document":
        return t.body;}var e = Nt(t),
        n = e.overflow,
        i = e.overflowX,
        o = e.overflowY;return (/(auto|scroll|overlay)/.test(n + o + i) ? t : Lt(kt(t))
    );
  }var xt = Tt && !(!window.MSInputMethodContext || !document.documentMode),
      Pt = Tt && /MSIE 10/.test(navigator.userAgent);function Ht(t) {
    return 11 === t ? xt : 10 === t ? Pt : xt || Pt;
  }function jt(t) {
    if (!t) return document.documentElement;for (var e = Ht(10) ? document.body : null, n = t.offsetParent || null; n === e && t.nextElementSibling;) {
      n = (t = t.nextElementSibling).offsetParent;
    }var i = n && n.nodeName;return i && "BODY" !== i && "HTML" !== i ? -1 !== ["TH", "TD", "TABLE"].indexOf(n.nodeName) && "static" === Nt(n, "position") ? jt(n) : n : t ? t.ownerDocument.documentElement : document.documentElement;
  }function Rt(t) {
    return null !== t.parentNode ? Rt(t.parentNode) : t;
  }function Ft(t, e) {
    if (!(t && t.nodeType && e && e.nodeType)) return document.documentElement;var n = t.compareDocumentPosition(e) & Node.DOCUMENT_POSITION_FOLLOWING,
        i = n ? t : e,
        o = n ? e : t,
        r = document.createRange();r.setStart(i, 0), r.setEnd(o, 0);var s,
        a,
        l = r.commonAncestorContainer;if (t !== l && e !== l || i.contains(o)) return "BODY" === (a = (s = l).nodeName) || "HTML" !== a && jt(s.firstElementChild) !== s ? jt(l) : l;var c = Rt(t);return c.host ? Ft(c.host, e) : Ft(t, Rt(e).host);
  }function Mt(t) {
    var e = "top" === (1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "top") ? "scrollTop" : "scrollLeft",
        n = t.nodeName;if ("BODY" !== n && "HTML" !== n) return t[e];var i = t.ownerDocument.documentElement;return (t.ownerDocument.scrollingElement || i)[e];
  }function Wt(t, e) {
    var n = "x" === e ? "Left" : "Top",
        i = "Left" === n ? "Right" : "Bottom";return parseFloat(t["border" + n + "Width"], 10) + parseFloat(t["border" + i + "Width"], 10);
  }function Ut(t, e, n, i) {
    return Math.max(e["offset" + t], e["scroll" + t], n["client" + t], n["offset" + t], n["scroll" + t], Ht(10) ? parseInt(n["offset" + t]) + parseInt(i["margin" + ("Height" === t ? "Top" : "Left")]) + parseInt(i["margin" + ("Height" === t ? "Bottom" : "Right")]) : 0);
  }function Bt(t) {
    var e = t.body,
        n = t.documentElement,
        i = Ht(10) && getComputedStyle(n);return { height: Ut("Height", e, n, i), width: Ut("Width", e, n, i) };
  }var qt = function () {
    function i(t, e) {
      for (var n = 0; n < e.length; n++) {
        var i = e[n];i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
      }
    }return function (t, e, n) {
      return e && i(t.prototype, e), n && i(t, n), t;
    };
  }(),
      Kt = function Kt(t, e, n) {
    return e in t ? Object.defineProperty(t, e, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : t[e] = n, t;
  },
      Qt = Object.assign || function (t) {
    for (var e = 1; e < arguments.length; e++) {
      var n = arguments[e];for (var i in n) {
        Object.prototype.hasOwnProperty.call(n, i) && (t[i] = n[i]);
      }
    }return t;
  };function Vt(t) {
    return Qt({}, t, { right: t.left + t.width, bottom: t.top + t.height });
  }function Yt(t) {
    var e = {};try {
      if (Ht(10)) {
        e = t.getBoundingClientRect();var n = Mt(t, "top"),
            i = Mt(t, "left");e.top += n, e.left += i, e.bottom += n, e.right += i;
      } else e = t.getBoundingClientRect();
    } catch (t) {}var o = { left: e.left, top: e.top, width: e.right - e.left, height: e.bottom - e.top },
        r = "HTML" === t.nodeName ? Bt(t.ownerDocument) : {},
        s = r.width || t.clientWidth || o.right - o.left,
        a = r.height || t.clientHeight || o.bottom - o.top,
        l = t.offsetWidth - s,
        c = t.offsetHeight - a;if (l || c) {
      var h = Nt(t);l -= Wt(h, "x"), c -= Wt(h, "y"), o.width -= l, o.height -= c;
    }return Vt(o);
  }function zt(t, e) {
    var n = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
        i = Ht(10),
        o = "HTML" === e.nodeName,
        r = Yt(t),
        s = Yt(e),
        a = Lt(t),
        l = Nt(e),
        c = parseFloat(l.borderTopWidth, 10),
        h = parseFloat(l.borderLeftWidth, 10);n && o && (s.top = Math.max(s.top, 0), s.left = Math.max(s.left, 0));var u = Vt({ top: r.top - s.top - c, left: r.left - s.left - h, width: r.width, height: r.height });if (u.marginTop = 0, u.marginLeft = 0, !i && o) {
      var f = parseFloat(l.marginTop, 10),
          d = parseFloat(l.marginLeft, 10);u.top -= c - f, u.bottom -= c - f, u.left -= h - d, u.right -= h - d, u.marginTop = f, u.marginLeft = d;
    }return (i && !n ? e.contains(a) : e === a && "BODY" !== a.nodeName) && (u = function (t, e) {
      var n = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
          i = Mt(e, "top"),
          o = Mt(e, "left"),
          r = n ? -1 : 1;return t.top += i * r, t.bottom += i * r, t.left += o * r, t.right += o * r, t;
    }(u, e)), u;
  }function Xt(t) {
    if (!t || !t.parentElement || Ht()) return document.documentElement;for (var e = t.parentElement; e && "none" === Nt(e, "transform");) {
      e = e.parentElement;
    }return e || document.documentElement;
  }function Gt(t, e, n, i) {
    var o = 4 < arguments.length && void 0 !== arguments[4] && arguments[4],
        r = { top: 0, left: 0 },
        s = o ? Xt(t) : Ft(t, e);if ("viewport" === i) r = function (t) {
      var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
          n = t.ownerDocument.documentElement,
          i = zt(t, n),
          o = Math.max(n.clientWidth, window.innerWidth || 0),
          r = Math.max(n.clientHeight, window.innerHeight || 0),
          s = e ? 0 : Mt(n),
          a = e ? 0 : Mt(n, "left");return Vt({ top: s - i.top + i.marginTop, left: a - i.left + i.marginLeft, width: o, height: r });
    }(s, o);else {
      var a = void 0;"scrollParent" === i ? "BODY" === (a = Lt(kt(e))).nodeName && (a = t.ownerDocument.documentElement) : a = "window" === i ? t.ownerDocument.documentElement : i;var l = zt(a, s, o);if ("HTML" !== a.nodeName || function t(e) {
        var n = e.nodeName;if ("BODY" === n || "HTML" === n) return !1;if ("fixed" === Nt(e, "position")) return !0;var i = kt(e);return !!i && t(i);
      }(s)) r = l;else {
        var c = Bt(t.ownerDocument),
            h = c.height,
            u = c.width;r.top += l.top - l.marginTop, r.bottom = h + l.top, r.left += l.left - l.marginLeft, r.right = u + l.left;
      }
    }var f = "number" == typeof (n = n || 0);return r.left += f ? n : n.left || 0, r.top += f ? n : n.top || 0, r.right -= f ? n : n.right || 0, r.bottom -= f ? n : n.bottom || 0, r;
  }function $t(t, e, i, n, o) {
    var r = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;if (-1 === t.indexOf("auto")) return t;var s = Gt(i, n, r, o),
        a = { top: { width: s.width, height: e.top - s.top }, right: { width: s.right - e.right, height: s.height }, bottom: { width: s.width, height: s.bottom - e.bottom }, left: { width: e.left - s.left, height: s.height } },
        l = Object.keys(a).map(function (t) {
      return Qt({ key: t }, a[t], { area: (e = a[t], e.width * e.height) });var e;
    }).sort(function (t, e) {
      return e.area - t.area;
    }),
        c = l.filter(function (t) {
      var e = t.width,
          n = t.height;return e >= i.clientWidth && n >= i.clientHeight;
    }),
        h = 0 < c.length ? c[0].key : l[0].key,
        u = t.split("-")[1];return h + (u ? "-" + u : "");
  }function Jt(t, e, n) {
    var i = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : null;return zt(n, i ? Xt(e) : Ft(e, n), i);
  }function Zt(t) {
    var e = t.ownerDocument.defaultView.getComputedStyle(t),
        n = parseFloat(e.marginTop || 0) + parseFloat(e.marginBottom || 0),
        i = parseFloat(e.marginLeft || 0) + parseFloat(e.marginRight || 0);return { width: t.offsetWidth + i, height: t.offsetHeight + n };
  }function te(t) {
    var e = { left: "right", right: "left", bottom: "top", top: "bottom" };return t.replace(/left|right|bottom|top/g, function (t) {
      return e[t];
    });
  }function ee(t, e, n) {
    n = n.split("-")[0];var i = Zt(t),
        o = { width: i.width, height: i.height },
        r = -1 !== ["right", "left"].indexOf(n),
        s = r ? "top" : "left",
        a = r ? "left" : "top",
        l = r ? "height" : "width",
        c = r ? "width" : "height";return o[s] = e[s] + e[l] / 2 - i[l] / 2, o[a] = n === a ? e[a] - i[c] : e[te(a)], o;
  }function ne(t, e) {
    return Array.prototype.find ? t.find(e) : t.filter(e)[0];
  }function ie(t, n, e) {
    return (void 0 === e ? t : t.slice(0, function (t, e, n) {
      if (Array.prototype.findIndex) return t.findIndex(function (t) {
        return t[e] === n;
      });var i = ne(t, function (t) {
        return t[e] === n;
      });return t.indexOf(i);
    }(t, "name", e))).forEach(function (t) {
      t.function && console.warn("`modifier.function` is deprecated, use `modifier.fn`!");var e = t.function || t.fn;t.enabled && Ot(e) && (n.offsets.popper = Vt(n.offsets.popper), n.offsets.reference = Vt(n.offsets.reference), n = e(n, t));
    }), n;
  }function oe(t, n) {
    return t.some(function (t) {
      var e = t.name;return t.enabled && e === n;
    });
  }function re(t) {
    for (var e = [!1, "ms", "Webkit", "Moz", "O"], n = t.charAt(0).toUpperCase() + t.slice(1), i = 0; i < e.length; i++) {
      var o = e[i],
          r = o ? "" + o + n : t;if ("undefined" != typeof document.body.style[r]) return r;
    }return null;
  }function se(t) {
    var e = t.ownerDocument;return e ? e.defaultView : window;
  }function ae(t, e, n, i) {
    n.updateBound = i, se(t).addEventListener("resize", n.updateBound, { passive: !0 });var o = Lt(t);return function t(e, n, i, o) {
      var r = "BODY" === e.nodeName,
          s = r ? e.ownerDocument.defaultView : e;s.addEventListener(n, i, { passive: !0 }), r || t(Lt(s.parentNode), n, i, o), o.push(s);
    }(o, "scroll", n.updateBound, n.scrollParents), n.scrollElement = o, n.eventsEnabled = !0, n;
  }function le() {
    var t, e;this.state.eventsEnabled && (cancelAnimationFrame(this.scheduleUpdate), this.state = (t = this.reference, e = this.state, se(t).removeEventListener("resize", e.updateBound), e.scrollParents.forEach(function (t) {
      t.removeEventListener("scroll", e.updateBound);
    }), e.updateBound = null, e.scrollParents = [], e.scrollElement = null, e.eventsEnabled = !1, e));
  }function ce(t) {
    return "" !== t && !isNaN(parseFloat(t)) && isFinite(t);
  }function he(n, i) {
    Object.keys(i).forEach(function (t) {
      var e = "";-1 !== ["width", "height", "top", "right", "bottom", "left"].indexOf(t) && ce(i[t]) && (e = "px"), n.style[t] = i[t] + e;
    });
  }var ue = Tt && /Firefox/i.test(navigator.userAgent);function fe(t, e, n) {
    var i = ne(t, function (t) {
      return t.name === e;
    }),
        o = !!i && t.some(function (t) {
      return t.name === n && t.enabled && t.order < i.order;
    });if (!o) {
      var r = "`" + e + "`",
          s = "`" + n + "`";console.warn(s + " modifier is required by " + r + " modifier in order to work, be sure to include it before " + r + "!");
    }return o;
  }var de = ["auto-start", "auto", "auto-end", "top-start", "top", "top-end", "right-start", "right", "right-end", "bottom-end", "bottom", "bottom-start", "left-end", "left", "left-start"],
      pe = de.slice(3);function me(t) {
    var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
        n = pe.indexOf(t),
        i = pe.slice(n + 1).concat(pe.slice(0, n));return e ? i.reverse() : i;
  }var ge = "flip",
      _e = "clockwise",
      ve = "counterclockwise";function ye(t, o, r, e) {
    var s = [0, 0],
        a = -1 !== ["right", "left"].indexOf(e),
        n = t.split(/(\+|\-)/).map(function (t) {
      return t.trim();
    }),
        i = n.indexOf(ne(n, function (t) {
      return -1 !== t.search(/,|\s/);
    }));n[i] && -1 === n[i].indexOf(",") && console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");var l = /\s*,\s*|\s+/,
        c = -1 !== i ? [n.slice(0, i).concat([n[i].split(l)[0]]), [n[i].split(l)[1]].concat(n.slice(i + 1))] : [n];return (c = c.map(function (t, e) {
      var n = (1 === e ? !a : a) ? "height" : "width",
          i = !1;return t.reduce(function (t, e) {
        return "" === t[t.length - 1] && -1 !== ["+", "-"].indexOf(e) ? (t[t.length - 1] = e, i = !0, t) : i ? (t[t.length - 1] += e, i = !1, t) : t.concat(e);
      }, []).map(function (t) {
        return function (t, e, n, i) {
          var o = t.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
              r = +o[1],
              s = o[2];if (!r) return t;if (0 !== s.indexOf("%")) return "vh" !== s && "vw" !== s ? r : ("vh" === s ? Math.max(document.documentElement.clientHeight, window.innerHeight || 0) : Math.max(document.documentElement.clientWidth, window.innerWidth || 0)) / 100 * r;var a = void 0;switch (s) {case "%p":
              a = n;break;case "%":case "%r":default:
              a = i;}return Vt(a)[e] / 100 * r;
        }(t, n, o, r);
      });
    })).forEach(function (n, i) {
      n.forEach(function (t, e) {
        ce(t) && (s[i] += t * ("-" === n[e - 1] ? -1 : 1));
      });
    }), s;
  }var Ee = { placement: "bottom", positionFixed: !1, eventsEnabled: !0, removeOnDestroy: !1, onCreate: function onCreate() {}, onUpdate: function onUpdate() {}, modifiers: { shift: { order: 100, enabled: !0, fn: function fn(t) {
          var e = t.placement,
              n = e.split("-")[0],
              i = e.split("-")[1];if (i) {
            var o = t.offsets,
                r = o.reference,
                s = o.popper,
                a = -1 !== ["bottom", "top"].indexOf(n),
                l = a ? "left" : "top",
                c = a ? "width" : "height",
                h = { start: Kt({}, l, r[l]), end: Kt({}, l, r[l] + r[c] - s[c]) };t.offsets.popper = Qt({}, s, h[i]);
          }return t;
        } }, offset: { order: 200, enabled: !0, fn: function fn(t, e) {
          var n = e.offset,
              i = t.placement,
              o = t.offsets,
              r = o.popper,
              s = o.reference,
              a = i.split("-")[0],
              l = void 0;return l = ce(+n) ? [+n, 0] : ye(n, r, s, a), "left" === a ? (r.top += l[0], r.left -= l[1]) : "right" === a ? (r.top += l[0], r.left += l[1]) : "top" === a ? (r.left += l[0], r.top -= l[1]) : "bottom" === a && (r.left += l[0], r.top += l[1]), t.popper = r, t;
        }, offset: 0 }, preventOverflow: { order: 300, enabled: !0, fn: function fn(t, i) {
          var e = i.boundariesElement || jt(t.instance.popper);t.instance.reference === e && (e = jt(e));var n = re("transform"),
              o = t.instance.popper.style,
              r = o.top,
              s = o.left,
              a = o[n];o.top = "", o.left = "", o[n] = "";var l = Gt(t.instance.popper, t.instance.reference, i.padding, e, t.positionFixed);o.top = r, o.left = s, o[n] = a, i.boundaries = l;var c = i.priority,
              h = t.offsets.popper,
              u = { primary: function primary(t) {
              var e = h[t];return h[t] < l[t] && !i.escapeWithReference && (e = Math.max(h[t], l[t])), Kt({}, t, e);
            }, secondary: function secondary(t) {
              var e = "right" === t ? "left" : "top",
                  n = h[e];return h[t] > l[t] && !i.escapeWithReference && (n = Math.min(h[e], l[t] - ("right" === t ? h.width : h.height))), Kt({}, e, n);
            } };return c.forEach(function (t) {
            var e = -1 !== ["left", "top"].indexOf(t) ? "primary" : "secondary";h = Qt({}, h, u[e](t));
          }), t.offsets.popper = h, t;
        }, priority: ["left", "right", "top", "bottom"], padding: 5, boundariesElement: "scrollParent" }, keepTogether: { order: 400, enabled: !0, fn: function fn(t) {
          var e = t.offsets,
              n = e.popper,
              i = e.reference,
              o = t.placement.split("-")[0],
              r = Math.floor,
              s = -1 !== ["top", "bottom"].indexOf(o),
              a = s ? "right" : "bottom",
              l = s ? "left" : "top",
              c = s ? "width" : "height";return n[a] < r(i[l]) && (t.offsets.popper[l] = r(i[l]) - n[c]), n[l] > r(i[a]) && (t.offsets.popper[l] = r(i[a])), t;
        } }, arrow: { order: 500, enabled: !0, fn: function fn(t, e) {
          var n;if (!fe(t.instance.modifiers, "arrow", "keepTogether")) return t;var i = e.element;if ("string" == typeof i) {
            if (!(i = t.instance.popper.querySelector(i))) return t;
          } else if (!t.instance.popper.contains(i)) return console.warn("WARNING: `arrow.element` must be child of its popper element!"), t;var o = t.placement.split("-")[0],
              r = t.offsets,
              s = r.popper,
              a = r.reference,
              l = -1 !== ["left", "right"].indexOf(o),
              c = l ? "height" : "width",
              h = l ? "Top" : "Left",
              u = h.toLowerCase(),
              f = l ? "left" : "top",
              d = l ? "bottom" : "right",
              p = Zt(i)[c];a[d] - p < s[u] && (t.offsets.popper[u] -= s[u] - (a[d] - p)), a[u] + p > s[d] && (t.offsets.popper[u] += a[u] + p - s[d]), t.offsets.popper = Vt(t.offsets.popper);var m = a[u] + a[c] / 2 - p / 2,
              g = Nt(t.instance.popper),
              _ = parseFloat(g["margin" + h], 10),
              v = parseFloat(g["border" + h + "Width"], 10),
              y = m - t.offsets.popper[u] - _ - v;return y = Math.max(Math.min(s[c] - p, y), 0), t.arrowElement = i, t.offsets.arrow = (Kt(n = {}, u, Math.round(y)), Kt(n, f, ""), n), t;
        }, element: "[x-arrow]" }, flip: { order: 600, enabled: !0, fn: function fn(p, m) {
          if (oe(p.instance.modifiers, "inner")) return p;if (p.flipped && p.placement === p.originalPlacement) return p;var g = Gt(p.instance.popper, p.instance.reference, m.padding, m.boundariesElement, p.positionFixed),
              _ = p.placement.split("-")[0],
              v = te(_),
              y = p.placement.split("-")[1] || "",
              E = [];switch (m.behavior) {case ge:
              E = [_, v];break;case _e:
              E = me(_);break;case ve:
              E = me(_, !0);break;default:
              E = m.behavior;}return E.forEach(function (t, e) {
            if (_ !== t || E.length === e + 1) return p;_ = p.placement.split("-")[0], v = te(_);var n,
                i = p.offsets.popper,
                o = p.offsets.reference,
                r = Math.floor,
                s = "left" === _ && r(i.right) > r(o.left) || "right" === _ && r(i.left) < r(o.right) || "top" === _ && r(i.bottom) > r(o.top) || "bottom" === _ && r(i.top) < r(o.bottom),
                a = r(i.left) < r(g.left),
                l = r(i.right) > r(g.right),
                c = r(i.top) < r(g.top),
                h = r(i.bottom) > r(g.bottom),
                u = "left" === _ && a || "right" === _ && l || "top" === _ && c || "bottom" === _ && h,
                f = -1 !== ["top", "bottom"].indexOf(_),
                d = !!m.flipVariations && (f && "start" === y && a || f && "end" === y && l || !f && "start" === y && c || !f && "end" === y && h);(s || u || d) && (p.flipped = !0, (s || u) && (_ = E[e + 1]), d && (y = "end" === (n = y) ? "start" : "start" === n ? "end" : n), p.placement = _ + (y ? "-" + y : ""), p.offsets.popper = Qt({}, p.offsets.popper, ee(p.instance.popper, p.offsets.reference, p.placement)), p = ie(p.instance.modifiers, p, "flip"));
          }), p;
        }, behavior: "flip", padding: 5, boundariesElement: "viewport" }, inner: { order: 700, enabled: !1, fn: function fn(t) {
          var e = t.placement,
              n = e.split("-")[0],
              i = t.offsets,
              o = i.popper,
              r = i.reference,
              s = -1 !== ["left", "right"].indexOf(n),
              a = -1 === ["top", "left"].indexOf(n);return o[s ? "left" : "top"] = r[n] - (a ? o[s ? "width" : "height"] : 0), t.placement = te(e), t.offsets.popper = Vt(o), t;
        } }, hide: { order: 800, enabled: !0, fn: function fn(t) {
          if (!fe(t.instance.modifiers, "hide", "preventOverflow")) return t;var e = t.offsets.reference,
              n = ne(t.instance.modifiers, function (t) {
            return "preventOverflow" === t.name;
          }).boundaries;if (e.bottom < n.top || e.left > n.right || e.top > n.bottom || e.right < n.left) {
            if (!0 === t.hide) return t;t.hide = !0, t.attributes["x-out-of-boundaries"] = "";
          } else {
            if (!1 === t.hide) return t;t.hide = !1, t.attributes["x-out-of-boundaries"] = !1;
          }return t;
        } }, computeStyle: { order: 850, enabled: !0, fn: function fn(t, e) {
          var n = e.x,
              i = e.y,
              o = t.offsets.popper,
              r = ne(t.instance.modifiers, function (t) {
            return "applyStyle" === t.name;
          }).gpuAcceleration;void 0 !== r && console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");var s,
              a,
              l,
              c,
              h,
              u,
              f,
              d,
              p,
              m,
              g,
              _,
              v,
              y,
              E = void 0 !== r ? r : e.gpuAcceleration,
              b = jt(t.instance.popper),
              w = Yt(b),
              C = { position: o.position },
              T = (s = t, a = window.devicePixelRatio < 2 || !ue, l = s.offsets, c = l.popper, h = l.reference, u = Math.round, f = Math.floor, d = function d(t) {
            return t;
          }, p = u(h.width), m = u(c.width), g = -1 !== ["left", "right"].indexOf(s.placement), _ = -1 !== s.placement.indexOf("-"), y = a ? u : d, { left: (v = a ? g || _ || p % 2 == m % 2 ? u : f : d)(p % 2 == 1 && m % 2 == 1 && !_ && a ? c.left - 1 : c.left), top: y(c.top), bottom: y(c.bottom), right: v(c.right) }),
              S = "bottom" === n ? "top" : "bottom",
              D = "right" === i ? "left" : "right",
              I = re("transform"),
              A = void 0,
              O = void 0;if (O = "bottom" === S ? "HTML" === b.nodeName ? -b.clientHeight + T.bottom : -w.height + T.bottom : T.top, A = "right" === D ? "HTML" === b.nodeName ? -b.clientWidth + T.right : -w.width + T.right : T.left, E && I) C[I] = "translate3d(" + A + "px, " + O + "px, 0)", C[S] = 0, C[D] = 0, C.willChange = "transform";else {
            var N = "bottom" === S ? -1 : 1,
                k = "right" === D ? -1 : 1;C[S] = O * N, C[D] = A * k, C.willChange = S + ", " + D;
          }var L = { "x-placement": t.placement };return t.attributes = Qt({}, L, t.attributes), t.styles = Qt({}, C, t.styles), t.arrowStyles = Qt({}, t.offsets.arrow, t.arrowStyles), t;
        }, gpuAcceleration: !0, x: "bottom", y: "right" }, applyStyle: { order: 900, enabled: !0, fn: function fn(t) {
          var e, n;return he(t.instance.popper, t.styles), e = t.instance.popper, n = t.attributes, Object.keys(n).forEach(function (t) {
            !1 !== n[t] ? e.setAttribute(t, n[t]) : e.removeAttribute(t);
          }), t.arrowElement && Object.keys(t.arrowStyles).length && he(t.arrowElement, t.arrowStyles), t;
        }, onLoad: function onLoad(t, e, n, i, o) {
          var r = Jt(o, e, t, n.positionFixed),
              s = $t(n.placement, r, e, t, n.modifiers.flip.boundariesElement, n.modifiers.flip.padding);return e.setAttribute("x-placement", s), he(e, { position: n.positionFixed ? "fixed" : "absolute" }), n;
        }, gpuAcceleration: void 0 } } },
      be = function () {
    function r(t, e) {
      var n = this,
          i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};!function (t, e) {
        if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
      }(this, r), this.scheduleUpdate = function () {
        return requestAnimationFrame(n.update);
      }, this.update = At(this.update.bind(this)), this.options = Qt({}, r.Defaults, i), this.state = { isDestroyed: !1, isCreated: !1, scrollParents: [] }, this.reference = t && t.jquery ? t[0] : t, this.popper = e && e.jquery ? e[0] : e, this.options.modifiers = {}, Object.keys(Qt({}, r.Defaults.modifiers, i.modifiers)).forEach(function (t) {
        n.options.modifiers[t] = Qt({}, r.Defaults.modifiers[t] || {}, i.modifiers ? i.modifiers[t] : {});
      }), this.modifiers = Object.keys(this.options.modifiers).map(function (t) {
        return Qt({ name: t }, n.options.modifiers[t]);
      }).sort(function (t, e) {
        return t.order - e.order;
      }), this.modifiers.forEach(function (t) {
        t.enabled && Ot(t.onLoad) && t.onLoad(n.reference, n.popper, n.options, t, n.state);
      }), this.update();var o = this.options.eventsEnabled;o && this.enableEventListeners(), this.state.eventsEnabled = o;
    }return qt(r, [{ key: "update", value: function value() {
        return function () {
          if (!this.state.isDestroyed) {
            var t = { instance: this, styles: {}, arrowStyles: {}, attributes: {}, flipped: !1, offsets: {} };t.offsets.reference = Jt(this.state, this.popper, this.reference, this.options.positionFixed), t.placement = $t(this.options.placement, t.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), t.originalPlacement = t.placement, t.positionFixed = this.options.positionFixed, t.offsets.popper = ee(this.popper, t.offsets.reference, t.placement), t.offsets.popper.position = this.options.positionFixed ? "fixed" : "absolute", t = ie(this.modifiers, t), this.state.isCreated ? this.options.onUpdate(t) : (this.state.isCreated = !0, this.options.onCreate(t));
          }
        }.call(this);
      } }, { key: "destroy", value: function value() {
        return function () {
          return this.state.isDestroyed = !0, oe(this.modifiers, "applyStyle") && (this.popper.removeAttribute("x-placement"), this.popper.style.position = "", this.popper.style.top = "", this.popper.style.left = "", this.popper.style.right = "", this.popper.style.bottom = "", this.popper.style.willChange = "", this.popper.style[re("transform")] = ""), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this;
        }.call(this);
      } }, { key: "enableEventListeners", value: function value() {
        return function () {
          this.state.eventsEnabled || (this.state = ae(this.reference, this.options, this.state, this.scheduleUpdate));
        }.call(this);
      } }, { key: "disableEventListeners", value: function value() {
        return le.call(this);
      } }]), r;
  }();be.Utils = ("undefined" != typeof window ? window : global).PopperUtils, be.placements = de, be.Defaults = Ee;var we = "dropdown",
      Ce = "bs.dropdown",
      Te = "." + Ce,
      Se = ".data-api",
      De = p.fn[we],
      Ie = new RegExp("38|40|27"),
      Ae = { HIDE: "hide" + Te, HIDDEN: "hidden" + Te, SHOW: "show" + Te, SHOWN: "shown" + Te, CLICK: "click" + Te, CLICK_DATA_API: "click" + Te + Se, KEYDOWN_DATA_API: "keydown" + Te + Se, KEYUP_DATA_API: "keyup" + Te + Se },
      Oe = "disabled",
      Ne = "show",
      ke = "dropup",
      Le = "dropright",
      xe = "dropleft",
      Pe = "dropdown-menu-right",
      He = "position-static",
      je = '[data-toggle="dropdown"]',
      Re = ".dropdown form",
      Fe = ".dropdown-menu",
      Me = ".navbar-nav",
      We = ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)",
      Ue = "top-start",
      Be = "top-end",
      qe = "bottom-start",
      Ke = "bottom-end",
      Qe = "right-start",
      Ve = "left-start",
      Ye = { offset: 0, flip: !0, boundary: "scrollParent", reference: "toggle", display: "dynamic" },
      ze = { offset: "(number|string|function)", flip: "boolean", boundary: "(string|element)", reference: "(string|element)", display: "string" },
      Xe = function () {
    function c(t, e) {
      this._element = t, this._popper = null, this._config = this._getConfig(e), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners();
    }var t = c.prototype;return t.toggle = function () {
      if (!this._element.disabled && !p(this._element).hasClass(Oe)) {
        var t = c._getParentFromElement(this._element),
            e = p(this._menu).hasClass(Ne);if (c._clearMenus(), !e) {
          var n = { relatedTarget: this._element },
              i = p.Event(Ae.SHOW, n);if (p(t).trigger(i), !i.isDefaultPrevented()) {
            if (!this._inNavbar) {
              if ("undefined" == typeof be) throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");var o = this._element;"parent" === this._config.reference ? o = t : m.isElement(this._config.reference) && (o = this._config.reference, "undefined" != typeof this._config.reference.jquery && (o = this._config.reference[0])), "scrollParent" !== this._config.boundary && p(t).addClass(He), this._popper = new be(o, this._menu, this._getPopperConfig());
            }"ontouchstart" in document.documentElement && 0 === p(t).closest(Me).length && p(document.body).children().on("mouseover", null, p.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), p(this._menu).toggleClass(Ne), p(t).toggleClass(Ne).trigger(p.Event(Ae.SHOWN, n));
          }
        }
      }
    }, t.show = function () {
      if (!(this._element.disabled || p(this._element).hasClass(Oe) || p(this._menu).hasClass(Ne))) {
        var t = { relatedTarget: this._element },
            e = p.Event(Ae.SHOW, t),
            n = c._getParentFromElement(this._element);p(n).trigger(e), e.isDefaultPrevented() || (p(this._menu).toggleClass(Ne), p(n).toggleClass(Ne).trigger(p.Event(Ae.SHOWN, t)));
      }
    }, t.hide = function () {
      if (!this._element.disabled && !p(this._element).hasClass(Oe) && p(this._menu).hasClass(Ne)) {
        var t = { relatedTarget: this._element },
            e = p.Event(Ae.HIDE, t),
            n = c._getParentFromElement(this._element);p(n).trigger(e), e.isDefaultPrevented() || (p(this._menu).toggleClass(Ne), p(n).toggleClass(Ne).trigger(p.Event(Ae.HIDDEN, t)));
      }
    }, t.dispose = function () {
      p.removeData(this._element, Ce), p(this._element).off(Te), this._element = null, (this._menu = null) !== this._popper && (this._popper.destroy(), this._popper = null);
    }, t.update = function () {
      this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate();
    }, t._addEventListeners = function () {
      var e = this;p(this._element).on(Ae.CLICK, function (t) {
        t.preventDefault(), t.stopPropagation(), e.toggle();
      });
    }, t._getConfig = function (t) {
      return t = l({}, this.constructor.Default, p(this._element).data(), t), m.typeCheckConfig(we, t, this.constructor.DefaultType), t;
    }, t._getMenuElement = function () {
      if (!this._menu) {
        var t = c._getParentFromElement(this._element);t && (this._menu = t.querySelector(Fe));
      }return this._menu;
    }, t._getPlacement = function () {
      var t = p(this._element.parentNode),
          e = qe;return t.hasClass(ke) ? (e = Ue, p(this._menu).hasClass(Pe) && (e = Be)) : t.hasClass(Le) ? e = Qe : t.hasClass(xe) ? e = Ve : p(this._menu).hasClass(Pe) && (e = Ke), e;
    }, t._detectNavbar = function () {
      return 0 < p(this._element).closest(".navbar").length;
    }, t._getOffset = function () {
      var e = this,
          t = {};return "function" == typeof this._config.offset ? t.fn = function (t) {
        return t.offsets = l({}, t.offsets, e._config.offset(t.offsets, e._element) || {}), t;
      } : t.offset = this._config.offset, t;
    }, t._getPopperConfig = function () {
      var t = { placement: this._getPlacement(), modifiers: { offset: this._getOffset(), flip: { enabled: this._config.flip }, preventOverflow: { boundariesElement: this._config.boundary } } };return "static" === this._config.display && (t.modifiers.applyStyle = { enabled: !1 }), t;
    }, c._jQueryInterface = function (e) {
      return this.each(function () {
        var t = p(this).data(Ce);if (t || (t = new c(this, "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) ? e : null), p(this).data(Ce, t)), "string" == typeof e) {
          if ("undefined" == typeof t[e]) throw new TypeError('No method named "' + e + '"');t[e]();
        }
      });
    }, c._clearMenus = function (t) {
      if (!t || 3 !== t.which && ("keyup" !== t.type || 9 === t.which)) for (var e = [].slice.call(document.querySelectorAll(je)), n = 0, i = e.length; n < i; n++) {
        var o = c._getParentFromElement(e[n]),
            r = p(e[n]).data(Ce),
            s = { relatedTarget: e[n] };if (t && "click" === t.type && (s.clickEvent = t), r) {
          var a = r._menu;if (p(o).hasClass(Ne) && !(t && ("click" === t.type && /input|textarea/i.test(t.target.tagName) || "keyup" === t.type && 9 === t.which) && p.contains(o, t.target))) {
            var l = p.Event(Ae.HIDE, s);p(o).trigger(l), l.isDefaultPrevented() || ("ontouchstart" in document.documentElement && p(document.body).children().off("mouseover", null, p.noop), e[n].setAttribute("aria-expanded", "false"), p(a).removeClass(Ne), p(o).removeClass(Ne).trigger(p.Event(Ae.HIDDEN, s)));
          }
        }
      }
    }, c._getParentFromElement = function (t) {
      var e,
          n = m.getSelectorFromElement(t);return n && (e = document.querySelector(n)), e || t.parentNode;
    }, c._dataApiKeydownHandler = function (t) {
      if ((/input|textarea/i.test(t.target.tagName) ? !(32 === t.which || 27 !== t.which && (40 !== t.which && 38 !== t.which || p(t.target).closest(Fe).length)) : Ie.test(t.which)) && (t.preventDefault(), t.stopPropagation(), !this.disabled && !p(this).hasClass(Oe))) {
        var e = c._getParentFromElement(this),
            n = p(e).hasClass(Ne);if (n && (!n || 27 !== t.which && 32 !== t.which)) {
          var i = [].slice.call(e.querySelectorAll(We));if (0 !== i.length) {
            var o = i.indexOf(t.target);38 === t.which && 0 < o && o--, 40 === t.which && o < i.length - 1 && o++, o < 0 && (o = 0), i[o].focus();
          }
        } else {
          if (27 === t.which) {
            var r = e.querySelector(je);p(r).trigger("focus");
          }p(this).trigger("click");
        }
      }
    }, s(c, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }, { key: "Default", get: function get() {
        return Ye;
      } }, { key: "DefaultType", get: function get() {
        return ze;
      } }]), c;
  }();p(document).on(Ae.KEYDOWN_DATA_API, je, Xe._dataApiKeydownHandler).on(Ae.KEYDOWN_DATA_API, Fe, Xe._dataApiKeydownHandler).on(Ae.CLICK_DATA_API + " " + Ae.KEYUP_DATA_API, Xe._clearMenus).on(Ae.CLICK_DATA_API, je, function (t) {
    t.preventDefault(), t.stopPropagation(), Xe._jQueryInterface.call(p(this), "toggle");
  }).on(Ae.CLICK_DATA_API, Re, function (t) {
    t.stopPropagation();
  }), p.fn[we] = Xe._jQueryInterface, p.fn[we].Constructor = Xe, p.fn[we].noConflict = function () {
    return p.fn[we] = De, Xe._jQueryInterface;
  };var Ge = "modal",
      $e = "bs.modal",
      Je = "." + $e,
      Ze = p.fn[Ge],
      tn = { backdrop: !0, keyboard: !0, focus: !0, show: !0 },
      en = { backdrop: "(boolean|string)", keyboard: "boolean", focus: "boolean", show: "boolean" },
      nn = { HIDE: "hide" + Je, HIDDEN: "hidden" + Je, SHOW: "show" + Je, SHOWN: "shown" + Je, FOCUSIN: "focusin" + Je, RESIZE: "resize" + Je, CLICK_DISMISS: "click.dismiss" + Je, KEYDOWN_DISMISS: "keydown.dismiss" + Je, MOUSEUP_DISMISS: "mouseup.dismiss" + Je, MOUSEDOWN_DISMISS: "mousedown.dismiss" + Je, CLICK_DATA_API: "click" + Je + ".data-api" },
      on = "modal-dialog-scrollable",
      rn = "modal-scrollbar-measure",
      sn = "modal-backdrop",
      an = "modal-open",
      ln = "fade",
      cn = "show",
      hn = ".modal-dialog",
      un = ".modal-body",
      fn = '[data-toggle="modal"]',
      dn = '[data-dismiss="modal"]',
      pn = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
      mn = ".sticky-top",
      gn = function () {
    function o(t, e) {
      this._config = this._getConfig(e), this._element = t, this._dialog = t.querySelector(hn), this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._isTransitioning = !1, this._scrollbarWidth = 0;
    }var t = o.prototype;return t.toggle = function (t) {
      return this._isShown ? this.hide() : this.show(t);
    }, t.show = function (t) {
      var e = this;if (!this._isShown && !this._isTransitioning) {
        p(this._element).hasClass(ln) && (this._isTransitioning = !0);var n = p.Event(nn.SHOW, { relatedTarget: t });p(this._element).trigger(n), this._isShown || n.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), this._setEscapeEvent(), this._setResizeEvent(), p(this._element).on(nn.CLICK_DISMISS, dn, function (t) {
          return e.hide(t);
        }), p(this._dialog).on(nn.MOUSEDOWN_DISMISS, function () {
          p(e._element).one(nn.MOUSEUP_DISMISS, function (t) {
            p(t.target).is(e._element) && (e._ignoreBackdropClick = !0);
          });
        }), this._showBackdrop(function () {
          return e._showElement(t);
        }));
      }
    }, t.hide = function (t) {
      var e = this;if (t && t.preventDefault(), this._isShown && !this._isTransitioning) {
        var n = p.Event(nn.HIDE);if (p(this._element).trigger(n), this._isShown && !n.isDefaultPrevented()) {
          this._isShown = !1;var i = p(this._element).hasClass(ln);if (i && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), p(document).off(nn.FOCUSIN), p(this._element).removeClass(cn), p(this._element).off(nn.CLICK_DISMISS), p(this._dialog).off(nn.MOUSEDOWN_DISMISS), i) {
            var o = m.getTransitionDurationFromElement(this._element);p(this._element).one(m.TRANSITION_END, function (t) {
              return e._hideModal(t);
            }).emulateTransitionEnd(o);
          } else this._hideModal();
        }
      }
    }, t.dispose = function () {
      [window, this._element, this._dialog].forEach(function (t) {
        return p(t).off(Je);
      }), p(document).off(nn.FOCUSIN), p.removeData(this._element, $e), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._isTransitioning = null, this._scrollbarWidth = null;
    }, t.handleUpdate = function () {
      this._adjustDialog();
    }, t._getConfig = function (t) {
      return t = l({}, tn, t), m.typeCheckConfig(Ge, t, en), t;
    }, t._showElement = function (t) {
      var e = this,
          n = p(this._element).hasClass(ln);this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.setAttribute("aria-modal", !0), p(this._dialog).hasClass(on) ? this._dialog.querySelector(un).scrollTop = 0 : this._element.scrollTop = 0, n && m.reflow(this._element), p(this._element).addClass(cn), this._config.focus && this._enforceFocus();var i = p.Event(nn.SHOWN, { relatedTarget: t }),
          o = function o() {
        e._config.focus && e._element.focus(), e._isTransitioning = !1, p(e._element).trigger(i);
      };if (n) {
        var r = m.getTransitionDurationFromElement(this._dialog);p(this._dialog).one(m.TRANSITION_END, o).emulateTransitionEnd(r);
      } else o();
    }, t._enforceFocus = function () {
      var e = this;p(document).off(nn.FOCUSIN).on(nn.FOCUSIN, function (t) {
        document !== t.target && e._element !== t.target && 0 === p(e._element).has(t.target).length && e._element.focus();
      });
    }, t._setEscapeEvent = function () {
      var e = this;this._isShown && this._config.keyboard ? p(this._element).on(nn.KEYDOWN_DISMISS, function (t) {
        27 === t.which && (t.preventDefault(), e.hide());
      }) : this._isShown || p(this._element).off(nn.KEYDOWN_DISMISS);
    }, t._setResizeEvent = function () {
      var e = this;this._isShown ? p(window).on(nn.RESIZE, function (t) {
        return e.handleUpdate(t);
      }) : p(window).off(nn.RESIZE);
    }, t._hideModal = function () {
      var t = this;this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._element.removeAttribute("aria-modal"), this._isTransitioning = !1, this._showBackdrop(function () {
        p(document.body).removeClass(an), t._resetAdjustments(), t._resetScrollbar(), p(t._element).trigger(nn.HIDDEN);
      });
    }, t._removeBackdrop = function () {
      this._backdrop && (p(this._backdrop).remove(), this._backdrop = null);
    }, t._showBackdrop = function (t) {
      var e = this,
          n = p(this._element).hasClass(ln) ? ln : "";if (this._isShown && this._config.backdrop) {
        if (this._backdrop = document.createElement("div"), this._backdrop.className = sn, n && this._backdrop.classList.add(n), p(this._backdrop).appendTo(document.body), p(this._element).on(nn.CLICK_DISMISS, function (t) {
          e._ignoreBackdropClick ? e._ignoreBackdropClick = !1 : t.target === t.currentTarget && ("static" === e._config.backdrop ? e._element.focus() : e.hide());
        }), n && m.reflow(this._backdrop), p(this._backdrop).addClass(cn), !t) return;if (!n) return void t();var i = m.getTransitionDurationFromElement(this._backdrop);p(this._backdrop).one(m.TRANSITION_END, t).emulateTransitionEnd(i);
      } else if (!this._isShown && this._backdrop) {
        p(this._backdrop).removeClass(cn);var o = function o() {
          e._removeBackdrop(), t && t();
        };if (p(this._element).hasClass(ln)) {
          var r = m.getTransitionDurationFromElement(this._backdrop);p(this._backdrop).one(m.TRANSITION_END, o).emulateTransitionEnd(r);
        } else o();
      } else t && t();
    }, t._adjustDialog = function () {
      var t = this._element.scrollHeight > document.documentElement.clientHeight;!this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px");
    }, t._resetAdjustments = function () {
      this._element.style.paddingLeft = "", this._element.style.paddingRight = "";
    }, t._checkScrollbar = function () {
      var t = document.body.getBoundingClientRect();this._isBodyOverflowing = t.left + t.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth();
    }, t._setScrollbar = function () {
      var o = this;if (this._isBodyOverflowing) {
        var t = [].slice.call(document.querySelectorAll(pn)),
            e = [].slice.call(document.querySelectorAll(mn));p(t).each(function (t, e) {
          var n = e.style.paddingRight,
              i = p(e).css("padding-right");p(e).data("padding-right", n).css("padding-right", parseFloat(i) + o._scrollbarWidth + "px");
        }), p(e).each(function (t, e) {
          var n = e.style.marginRight,
              i = p(e).css("margin-right");p(e).data("margin-right", n).css("margin-right", parseFloat(i) - o._scrollbarWidth + "px");
        });var n = document.body.style.paddingRight,
            i = p(document.body).css("padding-right");p(document.body).data("padding-right", n).css("padding-right", parseFloat(i) + this._scrollbarWidth + "px");
      }p(document.body).addClass(an);
    }, t._resetScrollbar = function () {
      var t = [].slice.call(document.querySelectorAll(pn));p(t).each(function (t, e) {
        var n = p(e).data("padding-right");p(e).removeData("padding-right"), e.style.paddingRight = n || "";
      });var e = [].slice.call(document.querySelectorAll("" + mn));p(e).each(function (t, e) {
        var n = p(e).data("margin-right");"undefined" != typeof n && p(e).css("margin-right", n).removeData("margin-right");
      });var n = p(document.body).data("padding-right");p(document.body).removeData("padding-right"), document.body.style.paddingRight = n || "";
    }, t._getScrollbarWidth = function () {
      var t = document.createElement("div");t.className = rn, document.body.appendChild(t);var e = t.getBoundingClientRect().width - t.clientWidth;return document.body.removeChild(t), e;
    }, o._jQueryInterface = function (n, i) {
      return this.each(function () {
        var t = p(this).data($e),
            e = l({}, tn, p(this).data(), "object" == (typeof n === "undefined" ? "undefined" : _typeof(n)) && n ? n : {});if (t || (t = new o(this, e), p(this).data($e, t)), "string" == typeof n) {
          if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');t[n](i);
        } else e.show && t.show(i);
      });
    }, s(o, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }, { key: "Default", get: function get() {
        return tn;
      } }]), o;
  }();p(document).on(nn.CLICK_DATA_API, fn, function (t) {
    var e,
        n = this,
        i = m.getSelectorFromElement(this);i && (e = document.querySelector(i));var o = p(e).data($e) ? "toggle" : l({}, p(e).data(), p(this).data());"A" !== this.tagName && "AREA" !== this.tagName || t.preventDefault();var r = p(e).one(nn.SHOW, function (t) {
      t.isDefaultPrevented() || r.one(nn.HIDDEN, function () {
        p(n).is(":visible") && n.focus();
      });
    });gn._jQueryInterface.call(p(e), o, this);
  }), p.fn[Ge] = gn._jQueryInterface, p.fn[Ge].Constructor = gn, p.fn[Ge].noConflict = function () {
    return p.fn[Ge] = Ze, gn._jQueryInterface;
  };var _n = ["background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href"],
      vn = { "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i], a: ["target", "href", "title", "rel"], area: [], b: [], br: [], col: [], code: [], div: [], em: [], hr: [], h1: [], h2: [], h3: [], h4: [], h5: [], h6: [], i: [], img: ["src", "alt", "title", "width", "height"], li: [], ol: [], p: [], pre: [], s: [], small: [], span: [], sub: [], sup: [], strong: [], u: [], ul: [] },
      yn = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,
      En = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i;function bn(t, s, e) {
    if (0 === t.length) return t;if (e && "function" == typeof e) return e(t);for (var n = new window.DOMParser().parseFromString(t, "text/html"), a = Object.keys(s), l = [].slice.call(n.body.querySelectorAll("*")), i = function i(t, e) {
      var n = l[t],
          i = n.nodeName.toLowerCase();if (-1 === a.indexOf(n.nodeName.toLowerCase())) return n.parentNode.removeChild(n), "continue";var o = [].slice.call(n.attributes),
          r = [].concat(s["*"] || [], s[i] || []);o.forEach(function (t) {
        (function (t, e) {
          var n = t.nodeName.toLowerCase();if (-1 !== e.indexOf(n)) return -1 === _n.indexOf(n) || Boolean(t.nodeValue.match(yn) || t.nodeValue.match(En));for (var i = e.filter(function (t) {
            return t instanceof RegExp;
          }), o = 0, r = i.length; o < r; o++) {
            if (n.match(i[o])) return !0;
          }return !1;
        })(t, r) || n.removeAttribute(t.nodeName);
      });
    }, o = 0, r = l.length; o < r; o++) {
      i(o);
    }return n.body.innerHTML;
  }var wn = "tooltip",
      Cn = "bs.tooltip",
      Tn = "." + Cn,
      Sn = p.fn[wn],
      Dn = "bs-tooltip",
      In = new RegExp("(^|\\s)" + Dn + "\\S+", "g"),
      An = ["sanitize", "whiteList", "sanitizeFn"],
      On = { animation: "boolean", template: "string", title: "(string|element|function)", trigger: "string", delay: "(number|object)", html: "boolean", selector: "(string|boolean)", placement: "(string|function)", offset: "(number|string|function)", container: "(string|element|boolean)", fallbackPlacement: "(string|array)", boundary: "(string|element)", sanitize: "boolean", sanitizeFn: "(null|function)", whiteList: "object" },
      Nn = { AUTO: "auto", TOP: "top", RIGHT: "right", BOTTOM: "bottom", LEFT: "left" },
      kn = { animation: !0, template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>', trigger: "hover focus", title: "", delay: 0, html: !1, selector: !1, placement: "top", offset: 0, container: !1, fallbackPlacement: "flip", boundary: "scrollParent", sanitize: !0, sanitizeFn: null, whiteList: vn },
      Ln = "show",
      xn = "out",
      Pn = { HIDE: "hide" + Tn, HIDDEN: "hidden" + Tn, SHOW: "show" + Tn, SHOWN: "shown" + Tn, INSERTED: "inserted" + Tn, CLICK: "click" + Tn, FOCUSIN: "focusin" + Tn, FOCUSOUT: "focusout" + Tn, MOUSEENTER: "mouseenter" + Tn, MOUSELEAVE: "mouseleave" + Tn },
      Hn = "fade",
      jn = "show",
      Rn = ".tooltip-inner",
      Fn = ".arrow",
      Mn = "hover",
      Wn = "focus",
      Un = "click",
      Bn = "manual",
      qn = function () {
    function i(t, e) {
      if ("undefined" == typeof be) throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = t, this.config = this._getConfig(e), this.tip = null, this._setListeners();
    }var t = i.prototype;return t.enable = function () {
      this._isEnabled = !0;
    }, t.disable = function () {
      this._isEnabled = !1;
    }, t.toggleEnabled = function () {
      this._isEnabled = !this._isEnabled;
    }, t.toggle = function (t) {
      if (this._isEnabled) if (t) {
        var e = this.constructor.DATA_KEY,
            n = p(t.currentTarget).data(e);n || (n = new this.constructor(t.currentTarget, this._getDelegateConfig()), p(t.currentTarget).data(e, n)), n._activeTrigger.click = !n._activeTrigger.click, n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n);
      } else {
        if (p(this.getTipElement()).hasClass(jn)) return void this._leave(null, this);this._enter(null, this);
      }
    }, t.dispose = function () {
      clearTimeout(this._timeout), p.removeData(this.element, this.constructor.DATA_KEY), p(this.element).off(this.constructor.EVENT_KEY), p(this.element).closest(".modal").off("hide.bs.modal"), this.tip && p(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, (this._activeTrigger = null) !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null;
    }, t.show = function () {
      var e = this;if ("none" === p(this.element).css("display")) throw new Error("Please use show on visible elements");var t = p.Event(this.constructor.Event.SHOW);if (this.isWithContent() && this._isEnabled) {
        p(this.element).trigger(t);var n = m.findShadowRoot(this.element),
            i = p.contains(null !== n ? n : this.element.ownerDocument.documentElement, this.element);if (t.isDefaultPrevented() || !i) return;var o = this.getTipElement(),
            r = m.getUID(this.constructor.NAME);o.setAttribute("id", r), this.element.setAttribute("aria-describedby", r), this.setContent(), this.config.animation && p(o).addClass(Hn);var s = "function" == typeof this.config.placement ? this.config.placement.call(this, o, this.element) : this.config.placement,
            a = this._getAttachment(s);this.addAttachmentClass(a);var l = this._getContainer();p(o).data(this.constructor.DATA_KEY, this), p.contains(this.element.ownerDocument.documentElement, this.tip) || p(o).appendTo(l), p(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new be(this.element, o, { placement: a, modifiers: { offset: this._getOffset(), flip: { behavior: this.config.fallbackPlacement }, arrow: { element: Fn }, preventOverflow: { boundariesElement: this.config.boundary } }, onCreate: function onCreate(t) {
            t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t);
          }, onUpdate: function onUpdate(t) {
            return e._handlePopperPlacementChange(t);
          } }), p(o).addClass(jn), "ontouchstart" in document.documentElement && p(document.body).children().on("mouseover", null, p.noop);var c = function c() {
          e.config.animation && e._fixTransition();var t = e._hoverState;e._hoverState = null, p(e.element).trigger(e.constructor.Event.SHOWN), t === xn && e._leave(null, e);
        };if (p(this.tip).hasClass(Hn)) {
          var h = m.getTransitionDurationFromElement(this.tip);p(this.tip).one(m.TRANSITION_END, c).emulateTransitionEnd(h);
        } else c();
      }
    }, t.hide = function (t) {
      var e = this,
          n = this.getTipElement(),
          i = p.Event(this.constructor.Event.HIDE),
          o = function o() {
        e._hoverState !== Ln && n.parentNode && n.parentNode.removeChild(n), e._cleanTipClass(), e.element.removeAttribute("aria-describedby"), p(e.element).trigger(e.constructor.Event.HIDDEN), null !== e._popper && e._popper.destroy(), t && t();
      };if (p(this.element).trigger(i), !i.isDefaultPrevented()) {
        if (p(n).removeClass(jn), "ontouchstart" in document.documentElement && p(document.body).children().off("mouseover", null, p.noop), this._activeTrigger[Un] = !1, this._activeTrigger[Wn] = !1, this._activeTrigger[Mn] = !1, p(this.tip).hasClass(Hn)) {
          var r = m.getTransitionDurationFromElement(n);p(n).one(m.TRANSITION_END, o).emulateTransitionEnd(r);
        } else o();this._hoverState = "";
      }
    }, t.update = function () {
      null !== this._popper && this._popper.scheduleUpdate();
    }, t.isWithContent = function () {
      return Boolean(this.getTitle());
    }, t.addAttachmentClass = function (t) {
      p(this.getTipElement()).addClass(Dn + "-" + t);
    }, t.getTipElement = function () {
      return this.tip = this.tip || p(this.config.template)[0], this.tip;
    }, t.setContent = function () {
      var t = this.getTipElement();this.setElementContent(p(t.querySelectorAll(Rn)), this.getTitle()), p(t).removeClass(Hn + " " + jn);
    }, t.setElementContent = function (t, e) {
      "object" != (typeof e === "undefined" ? "undefined" : _typeof(e)) || !e.nodeType && !e.jquery ? this.config.html ? (this.config.sanitize && (e = bn(e, this.config.whiteList, this.config.sanitizeFn)), t.html(e)) : t.text(e) : this.config.html ? p(e).parent().is(t) || t.empty().append(e) : t.text(p(e).text());
    }, t.getTitle = function () {
      var t = this.element.getAttribute("data-original-title");return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t;
    }, t._getOffset = function () {
      var e = this,
          t = {};return "function" == typeof this.config.offset ? t.fn = function (t) {
        return t.offsets = l({}, t.offsets, e.config.offset(t.offsets, e.element) || {}), t;
      } : t.offset = this.config.offset, t;
    }, t._getContainer = function () {
      return !1 === this.config.container ? document.body : m.isElement(this.config.container) ? p(this.config.container) : p(document).find(this.config.container);
    }, t._getAttachment = function (t) {
      return Nn[t.toUpperCase()];
    }, t._setListeners = function () {
      var i = this;this.config.trigger.split(" ").forEach(function (t) {
        if ("click" === t) p(i.element).on(i.constructor.Event.CLICK, i.config.selector, function (t) {
          return i.toggle(t);
        });else if (t !== Bn) {
          var e = t === Mn ? i.constructor.Event.MOUSEENTER : i.constructor.Event.FOCUSIN,
              n = t === Mn ? i.constructor.Event.MOUSELEAVE : i.constructor.Event.FOCUSOUT;p(i.element).on(e, i.config.selector, function (t) {
            return i._enter(t);
          }).on(n, i.config.selector, function (t) {
            return i._leave(t);
          });
        }
      }), p(this.element).closest(".modal").on("hide.bs.modal", function () {
        i.element && i.hide();
      }), this.config.selector ? this.config = l({}, this.config, { trigger: "manual", selector: "" }) : this._fixTitle();
    }, t._fixTitle = function () {
      var t = _typeof(this.element.getAttribute("data-original-title"));(this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""));
    }, t._enter = function (t, e) {
      var n = this.constructor.DATA_KEY;(e = e || p(t.currentTarget).data(n)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), p(t.currentTarget).data(n, e)), t && (e._activeTrigger["focusin" === t.type ? Wn : Mn] = !0), p(e.getTipElement()).hasClass(jn) || e._hoverState === Ln ? e._hoverState = Ln : (clearTimeout(e._timeout), e._hoverState = Ln, e.config.delay && e.config.delay.show ? e._timeout = setTimeout(function () {
        e._hoverState === Ln && e.show();
      }, e.config.delay.show) : e.show());
    }, t._leave = function (t, e) {
      var n = this.constructor.DATA_KEY;(e = e || p(t.currentTarget).data(n)) || (e = new this.constructor(t.currentTarget, this._getDelegateConfig()), p(t.currentTarget).data(n, e)), t && (e._activeTrigger["focusout" === t.type ? Wn : Mn] = !1), e._isWithActiveTrigger() || (clearTimeout(e._timeout), e._hoverState = xn, e.config.delay && e.config.delay.hide ? e._timeout = setTimeout(function () {
        e._hoverState === xn && e.hide();
      }, e.config.delay.hide) : e.hide());
    }, t._isWithActiveTrigger = function () {
      for (var t in this._activeTrigger) {
        if (this._activeTrigger[t]) return !0;
      }return !1;
    }, t._getConfig = function (t) {
      var e = p(this.element).data();return Object.keys(e).forEach(function (t) {
        -1 !== An.indexOf(t) && delete e[t];
      }), "number" == typeof (t = l({}, this.constructor.Default, e, "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t ? t : {})).delay && (t.delay = { show: t.delay, hide: t.delay }), "number" == typeof t.title && (t.title = t.title.toString()), "number" == typeof t.content && (t.content = t.content.toString()), m.typeCheckConfig(wn, t, this.constructor.DefaultType), t.sanitize && (t.template = bn(t.template, t.whiteList, t.sanitizeFn)), t;
    }, t._getDelegateConfig = function () {
      var t = {};if (this.config) for (var e in this.config) {
        this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
      }return t;
    }, t._cleanTipClass = function () {
      var t = p(this.getTipElement()),
          e = t.attr("class").match(In);null !== e && e.length && t.removeClass(e.join(""));
    }, t._handlePopperPlacementChange = function (t) {
      var e = t.instance;this.tip = e.popper, this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement));
    }, t._fixTransition = function () {
      var t = this.getTipElement(),
          e = this.config.animation;null === t.getAttribute("x-placement") && (p(t).removeClass(Hn), this.config.animation = !1, this.hide(), this.show(), this.config.animation = e);
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = p(this).data(Cn),
            e = "object" == (typeof n === "undefined" ? "undefined" : _typeof(n)) && n;if ((t || !/dispose|hide/.test(n)) && (t || (t = new i(this, e), p(this).data(Cn, t)), "string" == typeof n)) {
          if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');t[n]();
        }
      });
    }, s(i, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }, { key: "Default", get: function get() {
        return kn;
      } }, { key: "NAME", get: function get() {
        return wn;
      } }, { key: "DATA_KEY", get: function get() {
        return Cn;
      } }, { key: "Event", get: function get() {
        return Pn;
      } }, { key: "EVENT_KEY", get: function get() {
        return Tn;
      } }, { key: "DefaultType", get: function get() {
        return On;
      } }]), i;
  }();p.fn[wn] = qn._jQueryInterface, p.fn[wn].Constructor = qn, p.fn[wn].noConflict = function () {
    return p.fn[wn] = Sn, qn._jQueryInterface;
  };var Kn = "popover",
      Qn = "bs.popover",
      Vn = "." + Qn,
      Yn = p.fn[Kn],
      zn = "bs-popover",
      Xn = new RegExp("(^|\\s)" + zn + "\\S+", "g"),
      Gn = l({}, qn.Default, { placement: "right", trigger: "click", content: "", template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>' }),
      $n = l({}, qn.DefaultType, { content: "(string|element|function)" }),
      Jn = "fade",
      Zn = "show",
      ti = ".popover-header",
      ei = ".popover-body",
      ni = { HIDE: "hide" + Vn, HIDDEN: "hidden" + Vn, SHOW: "show" + Vn, SHOWN: "shown" + Vn, INSERTED: "inserted" + Vn, CLICK: "click" + Vn, FOCUSIN: "focusin" + Vn, FOCUSOUT: "focusout" + Vn, MOUSEENTER: "mouseenter" + Vn, MOUSELEAVE: "mouseleave" + Vn },
      ii = function (t) {
    var e, n;function i() {
      return t.apply(this, arguments) || this;
    }n = t, (e = i).prototype = Object.create(n.prototype), (e.prototype.constructor = e).__proto__ = n;var o = i.prototype;return o.isWithContent = function () {
      return this.getTitle() || this._getContent();
    }, o.addAttachmentClass = function (t) {
      p(this.getTipElement()).addClass(zn + "-" + t);
    }, o.getTipElement = function () {
      return this.tip = this.tip || p(this.config.template)[0], this.tip;
    }, o.setContent = function () {
      var t = p(this.getTipElement());this.setElementContent(t.find(ti), this.getTitle());var e = this._getContent();"function" == typeof e && (e = e.call(this.element)), this.setElementContent(t.find(ei), e), t.removeClass(Jn + " " + Zn);
    }, o._getContent = function () {
      return this.element.getAttribute("data-content") || this.config.content;
    }, o._cleanTipClass = function () {
      var t = p(this.getTipElement()),
          e = t.attr("class").match(Xn);null !== e && 0 < e.length && t.removeClass(e.join(""));
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = p(this).data(Qn),
            e = "object" == (typeof n === "undefined" ? "undefined" : _typeof(n)) ? n : null;if ((t || !/dispose|hide/.test(n)) && (t || (t = new i(this, e), p(this).data(Qn, t)), "string" == typeof n)) {
          if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');t[n]();
        }
      });
    }, s(i, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }, { key: "Default", get: function get() {
        return Gn;
      } }, { key: "NAME", get: function get() {
        return Kn;
      } }, { key: "DATA_KEY", get: function get() {
        return Qn;
      } }, { key: "Event", get: function get() {
        return ni;
      } }, { key: "EVENT_KEY", get: function get() {
        return Vn;
      } }, { key: "DefaultType", get: function get() {
        return $n;
      } }]), i;
  }(qn);p.fn[Kn] = ii._jQueryInterface, p.fn[Kn].Constructor = ii, p.fn[Kn].noConflict = function () {
    return p.fn[Kn] = Yn, ii._jQueryInterface;
  };var oi = "scrollspy",
      ri = "bs.scrollspy",
      si = "." + ri,
      ai = p.fn[oi],
      li = { offset: 10, method: "auto", target: "" },
      ci = { offset: "number", method: "string", target: "(string|element)" },
      hi = { ACTIVATE: "activate" + si, SCROLL: "scroll" + si, LOAD_DATA_API: "load" + si + ".data-api" },
      ui = "dropdown-item",
      fi = "active",
      di = '[data-spy="scroll"]',
      pi = ".nav, .list-group",
      mi = ".nav-link",
      gi = ".nav-item",
      _i = ".list-group-item",
      vi = ".dropdown",
      yi = ".dropdown-item",
      Ei = ".dropdown-toggle",
      bi = "offset",
      wi = "position",
      Ci = function () {
    function n(t, e) {
      var n = this;this._element = t, this._scrollElement = "BODY" === t.tagName ? window : t, this._config = this._getConfig(e), this._selector = this._config.target + " " + mi + "," + this._config.target + " " + _i + "," + this._config.target + " " + yi, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, p(this._scrollElement).on(hi.SCROLL, function (t) {
        return n._process(t);
      }), this.refresh(), this._process();
    }var t = n.prototype;return t.refresh = function () {
      var e = this,
          t = this._scrollElement === this._scrollElement.window ? bi : wi,
          o = "auto" === this._config.method ? t : this._config.method,
          r = o === wi ? this._getScrollTop() : 0;this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), [].slice.call(document.querySelectorAll(this._selector)).map(function (t) {
        var e,
            n = m.getSelectorFromElement(t);if (n && (e = document.querySelector(n)), e) {
          var i = e.getBoundingClientRect();if (i.width || i.height) return [p(e)[o]().top + r, n];
        }return null;
      }).filter(function (t) {
        return t;
      }).sort(function (t, e) {
        return t[0] - e[0];
      }).forEach(function (t) {
        e._offsets.push(t[0]), e._targets.push(t[1]);
      });
    }, t.dispose = function () {
      p.removeData(this._element, ri), p(this._scrollElement).off(si), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null;
    }, t._getConfig = function (t) {
      if ("string" != typeof (t = l({}, li, "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t ? t : {})).target) {
        var e = p(t.target).attr("id");e || (e = m.getUID(oi), p(t.target).attr("id", e)), t.target = "#" + e;
      }return m.typeCheckConfig(oi, t, ci), t;
    }, t._getScrollTop = function () {
      return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
    }, t._getScrollHeight = function () {
      return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
    }, t._getOffsetHeight = function () {
      return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
    }, t._process = function () {
      var t = this._getScrollTop() + this._config.offset,
          e = this._getScrollHeight(),
          n = this._config.offset + e - this._getOffsetHeight();if (this._scrollHeight !== e && this.refresh(), n <= t) {
        var i = this._targets[this._targets.length - 1];this._activeTarget !== i && this._activate(i);
      } else {
        if (this._activeTarget && t < this._offsets[0] && 0 < this._offsets[0]) return this._activeTarget = null, void this._clear();for (var o = this._offsets.length; o--;) {
          this._activeTarget !== this._targets[o] && t >= this._offsets[o] && ("undefined" == typeof this._offsets[o + 1] || t < this._offsets[o + 1]) && this._activate(this._targets[o]);
        }
      }
    }, t._activate = function (e) {
      this._activeTarget = e, this._clear();var t = this._selector.split(",").map(function (t) {
        return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]';
      }),
          n = p([].slice.call(document.querySelectorAll(t.join(","))));n.hasClass(ui) ? (n.closest(vi).find(Ei).addClass(fi), n.addClass(fi)) : (n.addClass(fi), n.parents(pi).prev(mi + ", " + _i).addClass(fi), n.parents(pi).prev(gi).children(mi).addClass(fi)), p(this._scrollElement).trigger(hi.ACTIVATE, { relatedTarget: e });
    }, t._clear = function () {
      [].slice.call(document.querySelectorAll(this._selector)).filter(function (t) {
        return t.classList.contains(fi);
      }).forEach(function (t) {
        return t.classList.remove(fi);
      });
    }, n._jQueryInterface = function (e) {
      return this.each(function () {
        var t = p(this).data(ri);if (t || (t = new n(this, "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e), p(this).data(ri, t)), "string" == typeof e) {
          if ("undefined" == typeof t[e]) throw new TypeError('No method named "' + e + '"');t[e]();
        }
      });
    }, s(n, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }, { key: "Default", get: function get() {
        return li;
      } }]), n;
  }();p(window).on(hi.LOAD_DATA_API, function () {
    for (var t = [].slice.call(document.querySelectorAll(di)), e = t.length; e--;) {
      var n = p(t[e]);Ci._jQueryInterface.call(n, n.data());
    }
  }), p.fn[oi] = Ci._jQueryInterface, p.fn[oi].Constructor = Ci, p.fn[oi].noConflict = function () {
    return p.fn[oi] = ai, Ci._jQueryInterface;
  };var Ti = "bs.tab",
      Si = "." + Ti,
      Di = p.fn.tab,
      Ii = { HIDE: "hide" + Si, HIDDEN: "hidden" + Si, SHOW: "show" + Si, SHOWN: "shown" + Si, CLICK_DATA_API: "click" + Si + ".data-api" },
      Ai = "dropdown-menu",
      Oi = "active",
      Ni = "disabled",
      ki = "fade",
      Li = "show",
      xi = ".dropdown",
      Pi = ".nav, .list-group",
      Hi = ".active",
      ji = "> li > .active",
      Ri = '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
      Fi = ".dropdown-toggle",
      Mi = "> .dropdown-menu .active",
      Wi = function () {
    function i(t) {
      this._element = t;
    }var t = i.prototype;return t.show = function () {
      var n = this;if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && p(this._element).hasClass(Oi) || p(this._element).hasClass(Ni))) {
        var t,
            i,
            e = p(this._element).closest(Pi)[0],
            o = m.getSelectorFromElement(this._element);if (e) {
          var r = "UL" === e.nodeName || "OL" === e.nodeName ? ji : Hi;i = (i = p.makeArray(p(e).find(r)))[i.length - 1];
        }var s = p.Event(Ii.HIDE, { relatedTarget: this._element }),
            a = p.Event(Ii.SHOW, { relatedTarget: i });if (i && p(i).trigger(s), p(this._element).trigger(a), !a.isDefaultPrevented() && !s.isDefaultPrevented()) {
          o && (t = document.querySelector(o)), this._activate(this._element, e);var l = function l() {
            var t = p.Event(Ii.HIDDEN, { relatedTarget: n._element }),
                e = p.Event(Ii.SHOWN, { relatedTarget: i });p(i).trigger(t), p(n._element).trigger(e);
          };t ? this._activate(t, t.parentNode, l) : l();
        }
      }
    }, t.dispose = function () {
      p.removeData(this._element, Ti), this._element = null;
    }, t._activate = function (t, e, n) {
      var i = this,
          o = (!e || "UL" !== e.nodeName && "OL" !== e.nodeName ? p(e).children(Hi) : p(e).find(ji))[0],
          r = n && o && p(o).hasClass(ki),
          s = function s() {
        return i._transitionComplete(t, o, n);
      };if (o && r) {
        var a = m.getTransitionDurationFromElement(o);p(o).removeClass(Li).one(m.TRANSITION_END, s).emulateTransitionEnd(a);
      } else s();
    }, t._transitionComplete = function (t, e, n) {
      if (e) {
        p(e).removeClass(Oi);var i = p(e.parentNode).find(Mi)[0];i && p(i).removeClass(Oi), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !1);
      }if (p(t).addClass(Oi), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !0), m.reflow(t), t.classList.contains(ki) && t.classList.add(Li), t.parentNode && p(t.parentNode).hasClass(Ai)) {
        var o = p(t).closest(xi)[0];if (o) {
          var r = [].slice.call(o.querySelectorAll(Fi));p(r).addClass(Oi);
        }t.setAttribute("aria-expanded", !0);
      }n && n();
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = p(this),
            e = t.data(Ti);if (e || (e = new i(this), t.data(Ti, e)), "string" == typeof n) {
          if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');e[n]();
        }
      });
    }, s(i, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }]), i;
  }();p(document).on(Ii.CLICK_DATA_API, Ri, function (t) {
    t.preventDefault(), Wi._jQueryInterface.call(p(this), "show");
  }), p.fn.tab = Wi._jQueryInterface, p.fn.tab.Constructor = Wi, p.fn.tab.noConflict = function () {
    return p.fn.tab = Di, Wi._jQueryInterface;
  };var Ui = "toast",
      Bi = "bs.toast",
      qi = "." + Bi,
      Ki = p.fn[Ui],
      Qi = { CLICK_DISMISS: "click.dismiss" + qi, HIDE: "hide" + qi, HIDDEN: "hidden" + qi, SHOW: "show" + qi, SHOWN: "shown" + qi },
      Vi = "fade",
      Yi = "hide",
      zi = "show",
      Xi = "showing",
      Gi = { animation: "boolean", autohide: "boolean", delay: "number" },
      $i = { animation: !0, autohide: !0, delay: 500 },
      Ji = '[data-dismiss="toast"]',
      Zi = function () {
    function i(t, e) {
      this._element = t, this._config = this._getConfig(e), this._timeout = null, this._setListeners();
    }var t = i.prototype;return t.show = function () {
      var t = this;p(this._element).trigger(Qi.SHOW), this._config.animation && this._element.classList.add(Vi);var e = function e() {
        t._element.classList.remove(Xi), t._element.classList.add(zi), p(t._element).trigger(Qi.SHOWN), t._config.autohide && t.hide();
      };if (this._element.classList.remove(Yi), this._element.classList.add(Xi), this._config.animation) {
        var n = m.getTransitionDurationFromElement(this._element);p(this._element).one(m.TRANSITION_END, e).emulateTransitionEnd(n);
      } else e();
    }, t.hide = function (t) {
      var e = this;this._element.classList.contains(zi) && (p(this._element).trigger(Qi.HIDE), t ? this._close() : this._timeout = setTimeout(function () {
        e._close();
      }, this._config.delay));
    }, t.dispose = function () {
      clearTimeout(this._timeout), this._timeout = null, this._element.classList.contains(zi) && this._element.classList.remove(zi), p(this._element).off(Qi.CLICK_DISMISS), p.removeData(this._element, Bi), this._element = null, this._config = null;
    }, t._getConfig = function (t) {
      return t = l({}, $i, p(this._element).data(), "object" == (typeof t === "undefined" ? "undefined" : _typeof(t)) && t ? t : {}), m.typeCheckConfig(Ui, t, this.constructor.DefaultType), t;
    }, t._setListeners = function () {
      var t = this;p(this._element).on(Qi.CLICK_DISMISS, Ji, function () {
        return t.hide(!0);
      });
    }, t._close = function () {
      var t = this,
          e = function e() {
        t._element.classList.add(Yi), p(t._element).trigger(Qi.HIDDEN);
      };if (this._element.classList.remove(zi), this._config.animation) {
        var n = m.getTransitionDurationFromElement(this._element);p(this._element).one(m.TRANSITION_END, e).emulateTransitionEnd(n);
      } else e();
    }, i._jQueryInterface = function (n) {
      return this.each(function () {
        var t = p(this),
            e = t.data(Bi);if (e || (e = new i(this, "object" == (typeof n === "undefined" ? "undefined" : _typeof(n)) && n), t.data(Bi, e)), "string" == typeof n) {
          if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');e[n](this);
        }
      });
    }, s(i, null, [{ key: "VERSION", get: function get() {
        return "4.3.1";
      } }, { key: "DefaultType", get: function get() {
        return Gi;
      } }, { key: "Default", get: function get() {
        return $i;
      } }]), i;
  }();p.fn[Ui] = Zi._jQueryInterface, p.fn[Ui].Constructor = Zi, p.fn[Ui].noConflict = function () {
    return p.fn[Ui] = Ki, Zi._jQueryInterface;
  }, function () {
    if ("undefined" == typeof p) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");var t = p.fn.jquery.split(" ")[0].split(".");if (t[0] < 2 && t[1] < 9 || 1 === t[0] && 9 === t[1] && t[2] < 1 || 4 <= t[0]) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0");
  }(), t.Util = m, t.Alert = g, t.Button = k, t.Carousel = at, t.Collapse = Ct, t.Dropdown = Xe, t.Modal = gn, t.Popover = ii, t.Scrollspy = Ci, t.Tab = Wi, t.Toast = Zi, t.Tooltip = qn, Object.defineProperty(t, "__esModule", { value: !0 });
});
//# sourceMappingURL=bootstrap.bundle.min.js.map
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/* flatpickr v4.5.7,, @license MIT */
!function (e, t) {
  "object" == ( false ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = t() :  true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (t),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : (e = e || self).flatpickr = t();
}(undefined, function () {
  "use strict";
  var _e = function e() {
    return (_e = Object.assign || function (e) {
      for (var t, n = 1, a = arguments.length; n < a; n++) {
        for (var i in t = arguments[n]) {
          Object.prototype.hasOwnProperty.call(t, i) && (e[i] = t[i]);
        }
      }return e;
    }).apply(this, arguments);
  },
      t = ["onChange", "onClose", "onDayCreate", "onDestroy", "onKeyDown", "onMonthChange", "onOpen", "onParseConfig", "onReady", "onValueUpdate", "onYearChange", "onPreCalendarPosition"],
      n = { _disable: [], _enable: [], allowInput: !1, altFormat: "F j, Y", altInput: !1, altInputClass: "form-control input", animate: "object" == (typeof window === "undefined" ? "undefined" : _typeof(window)) && -1 === window.navigator.userAgent.indexOf("MSIE"), ariaDateFormat: "F j, Y", clickOpens: !0, closeOnSelect: !0, conjunction: ", ", dateFormat: "Y-m-d", defaultHour: 12, defaultMinute: 0, defaultSeconds: 0, disable: [], disableMobile: !1, enable: [], enableSeconds: !1, enableTime: !1, errorHandler: function errorHandler(e) {
      return "undefined" != typeof console && console.warn(e);
    }, getWeek: function getWeek(e) {
      var t = new Date(e.getTime());t.setHours(0, 0, 0, 0), t.setDate(t.getDate() + 3 - (t.getDay() + 6) % 7);var n = new Date(t.getFullYear(), 0, 4);return 1 + Math.round(((t.getTime() - n.getTime()) / 864e5 - 3 + (n.getDay() + 6) % 7) / 7);
    }, hourIncrement: 1, ignoredFocusElements: [], inline: !1, locale: "default", minuteIncrement: 5, mode: "single", nextArrow: "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 17 17'><g></g><path d='M13.207 8.472l-7.854 7.854-0.707-0.707 7.146-7.146-7.146-7.148 0.707-0.707 7.854 7.854z' /></svg>", noCalendar: !1, now: new Date(), onChange: [], onClose: [], onDayCreate: [], onDestroy: [], onKeyDown: [], onMonthChange: [], onOpen: [], onParseConfig: [], onReady: [], onValueUpdate: [], onYearChange: [], onPreCalendarPosition: [], plugins: [], position: "auto", positionElement: void 0, prevArrow: "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' viewBox='0 0 17 17'><g></g><path d='M5.207 8.471l7.146 7.147-0.707 0.707-7.853-7.854 7.854-7.853 0.707 0.707-7.147 7.146z' /></svg>", shorthandCurrentMonth: !1, showMonths: 1, static: !1, time_24hr: !1, weekNumbers: !1, wrap: !1 },
      a = { weekdays: { shorthand: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], longhand: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"] }, months: { shorthand: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"], longhand: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"] }, daysInMonth: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31], firstDayOfWeek: 0, ordinal: function ordinal(e) {
      var t = e % 100;if (t > 3 && t < 21) return "th";switch (t % 10) {case 1:
          return "st";case 2:
          return "nd";case 3:
          return "rd";default:
          return "th";}
    }, rangeSeparator: " to ", weekAbbreviation: "Wk", scrollTitle: "Scroll to increment", toggleTitle: "Click to toggle", amPM: ["AM", "PM"], yearAriaLabel: "Year" },
      _i = function _i(e) {
    return ("0" + e).slice(-2);
  },
      o = function o(e) {
    return !0 === e ? 1 : 0;
  };function r(e, t, n) {
    var a;return void 0 === n && (n = !1), function () {
      var i = this,
          o = arguments;null !== a && clearTimeout(a), a = window.setTimeout(function () {
        a = null, n || e.apply(i, o);
      }, t), n && !a && e.apply(i, o);
    };
  }var l = function l(e) {
    return e instanceof Array ? e : [e];
  };function c(e, t, n) {
    if (!0 === n) return e.classList.add(t);e.classList.remove(t);
  }function d(e, t, n) {
    var a = window.document.createElement(e);return t = t || "", n = n || "", a.className = t, void 0 !== n && (a.textContent = n), a;
  }function s(e) {
    for (; e.firstChild;) {
      e.removeChild(e.firstChild);
    }
  }function u(e, t) {
    var n = d("div", "numInputWrapper"),
        a = d("input", "numInput " + e),
        i = d("span", "arrowUp"),
        o = d("span", "arrowDown");if (-1 === navigator.userAgent.indexOf("MSIE 9.0") ? a.type = "number" : (a.type = "text", a.pattern = "\\d*"), void 0 !== t) for (var r in t) {
      a.setAttribute(r, t[r]);
    }return n.appendChild(a), n.appendChild(i), n.appendChild(o), n;
  }var f = function f() {},
      m = function m(e, t, n) {
    return n.months[t ? "shorthand" : "longhand"][e];
  },
      g = { D: f, F: function F(e, t, n) {
      e.setMonth(n.months.longhand.indexOf(t));
    }, G: function G(e, t) {
      e.setHours(parseFloat(t));
    }, H: function H(e, t) {
      e.setHours(parseFloat(t));
    }, J: function J(e, t) {
      e.setDate(parseFloat(t));
    }, K: function K(e, t, n) {
      e.setHours(e.getHours() % 12 + 12 * o(new RegExp(n.amPM[1], "i").test(t)));
    }, M: function M(e, t, n) {
      e.setMonth(n.months.shorthand.indexOf(t));
    }, S: function S(e, t) {
      e.setSeconds(parseFloat(t));
    }, U: function U(e, t) {
      return new Date(1e3 * parseFloat(t));
    }, W: function W(e, t) {
      var n = parseInt(t);return new Date(e.getFullYear(), 0, 2 + 7 * (n - 1), 0, 0, 0, 0);
    }, Y: function Y(e, t) {
      e.setFullYear(parseFloat(t));
    }, Z: function Z(e, t) {
      return new Date(t);
    }, d: function d(e, t) {
      e.setDate(parseFloat(t));
    }, h: function h(e, t) {
      e.setHours(parseFloat(t));
    }, i: function i(e, t) {
      e.setMinutes(parseFloat(t));
    }, j: function j(e, t) {
      e.setDate(parseFloat(t));
    }, l: f, m: function m(e, t) {
      e.setMonth(parseFloat(t) - 1);
    }, n: function n(e, t) {
      e.setMonth(parseFloat(t) - 1);
    }, s: function s(e, t) {
      e.setSeconds(parseFloat(t));
    }, u: function u(e, t) {
      return new Date(parseFloat(t));
    }, w: f, y: function y(e, t) {
      e.setFullYear(2e3 + parseFloat(t));
    } },
      p = { D: "(\\w+)", F: "(\\w+)", G: "(\\d\\d|\\d)", H: "(\\d\\d|\\d)", J: "(\\d\\d|\\d)\\w+", K: "", M: "(\\w+)", S: "(\\d\\d|\\d)", U: "(.+)", W: "(\\d\\d|\\d)", Y: "(\\d{4})", Z: "(.+)", d: "(\\d\\d|\\d)", h: "(\\d\\d|\\d)", i: "(\\d\\d|\\d)", j: "(\\d\\d|\\d)", l: "(\\w+)", m: "(\\d\\d|\\d)", n: "(\\d\\d|\\d)", s: "(\\d\\d|\\d)", u: "(.+)", w: "(\\d\\d|\\d)", y: "(\\d{2})" },
      h = { Z: function Z(e) {
      return e.toISOString();
    }, D: function D(e, t, n) {
      return t.weekdays.shorthand[h.w(e, t, n)];
    }, F: function F(e, t, n) {
      return m(h.n(e, t, n) - 1, !1, t);
    }, G: function G(e, t, n) {
      return _i(h.h(e, t, n));
    }, H: function H(e) {
      return _i(e.getHours());
    }, J: function J(e, t) {
      return void 0 !== t.ordinal ? e.getDate() + t.ordinal(e.getDate()) : e.getDate();
    }, K: function K(e, t) {
      return t.amPM[o(e.getHours() > 11)];
    }, M: function M(e, t) {
      return m(e.getMonth(), !0, t);
    }, S: function S(e) {
      return _i(e.getSeconds());
    }, U: function U(e) {
      return e.getTime() / 1e3;
    }, W: function W(e, t, n) {
      return n.getWeek(e);
    }, Y: function Y(e) {
      return e.getFullYear();
    }, d: function d(e) {
      return _i(e.getDate());
    }, h: function h(e) {
      return e.getHours() % 12 ? e.getHours() % 12 : 12;
    }, i: function i(e) {
      return _i(e.getMinutes());
    }, j: function j(e) {
      return e.getDate();
    }, l: function l(e, t) {
      return t.weekdays.longhand[e.getDay()];
    }, m: function m(e) {
      return _i(e.getMonth() + 1);
    }, n: function n(e) {
      return e.getMonth() + 1;
    }, s: function s(e) {
      return e.getSeconds();
    }, u: function u(e) {
      return e.getTime();
    }, w: function w(e) {
      return e.getDay();
    }, y: function y(e) {
      return String(e.getFullYear()).substring(2);
    } },
      v = function v(e) {
    var t = e.config,
        i = void 0 === t ? n : t,
        o = e.l10n,
        r = void 0 === o ? a : o;return function (e, t, n) {
      var a = n || r;return void 0 !== i.formatDate ? i.formatDate(e, t, a) : t.split("").map(function (t, n, o) {
        return h[t] && "\\" !== o[n - 1] ? h[t](e, a, i) : "\\" !== t ? t : "";
      }).join("");
    };
  },
      D = function D(e) {
    var t = e.config,
        i = void 0 === t ? n : t,
        o = e.l10n,
        r = void 0 === o ? a : o;return function (e, t, a, o) {
      if (0 === e || e) {
        var l,
            c = o || r,
            d = e;if (e instanceof Date) l = new Date(e.getTime());else if ("string" != typeof e && void 0 !== e.toFixed) l = new Date(e);else if ("string" == typeof e) {
          var s = t || (i || n).dateFormat,
              u = String(e).trim();if ("today" === u) l = new Date(), a = !0;else if (/Z$/.test(u) || /GMT$/.test(u)) l = new Date(e);else if (i && i.parseDate) l = i.parseDate(e, s);else {
            l = i && i.noCalendar ? new Date(new Date().setHours(0, 0, 0, 0)) : new Date(new Date().getFullYear(), 0, 1, 0, 0, 0, 0);for (var f = void 0, m = [], h = 0, v = 0, D = ""; h < s.length; h++) {
              var w = s[h],
                  b = "\\" === w,
                  y = "\\" === s[h - 1] || b;if (p[w] && !y) {
                D += p[w];var C = new RegExp(D).exec(e);C && (f = !0) && m["Y" !== w ? "push" : "unshift"]({ fn: g[w], val: C[++v] });
              } else b || (D += ".");m.forEach(function (e) {
                var t = e.fn,
                    n = e.val;return l = t(l, n, c) || l;
              });
            }l = f ? l : void 0;
          }
        }if (l instanceof Date && !isNaN(l.getTime())) return !0 === a && l.setHours(0, 0, 0, 0), l;i.errorHandler(new Error("Invalid date provided: " + d));
      }
    };
  };function w(e, t, n) {
    return void 0 === n && (n = !0), !1 !== n ? new Date(e.getTime()).setHours(0, 0, 0, 0) - new Date(t.getTime()).setHours(0, 0, 0, 0) : e.getTime() - t.getTime();
  }var b = function b(e, t, n) {
    return e > Math.min(t, n) && e < Math.max(t, n);
  },
      y = { DAY: 864e5 };"function" != typeof Object.assign && (Object.assign = function (e) {
    for (var t = [], n = 1; n < arguments.length; n++) {
      t[n - 1] = arguments[n];
    }if (!e) throw TypeError("Cannot convert undefined or null to object");for (var a = function a(t) {
      t && Object.keys(t).forEach(function (n) {
        return e[n] = t[n];
      });
    }, i = 0, o = t; i < o.length; i++) {
      a(o[i]);
    }return e;
  });var C = 300;function M(n, f) {
    var g = { config: _e({}, E.defaultConfig), l10n: a };function h(e) {
      return e.bind(g);
    }function M() {
      var e = g.config;!1 === e.weekNumbers && 1 === e.showMonths || !0 !== e.noCalendar && window.requestAnimationFrame(function () {
        if (void 0 !== g.calendarContainer && (g.calendarContainer.style.visibility = "hidden", g.calendarContainer.style.display = "block"), void 0 !== g.daysContainer) {
          var t = (g.days.offsetWidth + 1) * e.showMonths;g.daysContainer.style.width = t + "px", g.calendarContainer.style.width = t + (void 0 !== g.weekWrapper ? g.weekWrapper.offsetWidth : 0) + "px", g.calendarContainer.style.removeProperty("visibility"), g.calendarContainer.style.removeProperty("display");
        }
      });
    }function x(e) {
      0 === g.selectedDates.length && ne(), void 0 !== e && "blur" !== e.type && function (e) {
        e.preventDefault();var t = "keydown" === e.type,
            n = e.target;void 0 !== g.amPM && e.target === g.amPM && (g.amPM.textContent = g.l10n.amPM[o(g.amPM.textContent === g.l10n.amPM[0])]);var a = parseFloat(n.getAttribute("min")),
            r = parseFloat(n.getAttribute("max")),
            l = parseFloat(n.getAttribute("step")),
            c = parseInt(n.value, 10),
            d = e.delta || (t ? 38 === e.which ? 1 : -1 : 0),
            s = c + l * d;if (void 0 !== n.value && 2 === n.value.length) {
          var u = n === g.hourElement,
              f = n === g.minuteElement;s < a ? (s = r + s + o(!u) + (o(u) && o(!g.amPM)), f && Y(void 0, -1, g.hourElement)) : s > r && (s = n === g.hourElement ? s - r - o(!g.amPM) : a, f && Y(void 0, 1, g.hourElement)), g.amPM && u && (1 === l ? s + c === 23 : Math.abs(s - c) > l) && (g.amPM.textContent = g.l10n.amPM[o(g.amPM.textContent === g.l10n.amPM[0])]), n.value = _i(s);
        }
      }(e);var t = g._input.value;T(), ve(), g._input.value !== t && g._debouncedChange();
    }function T() {
      if (void 0 !== g.hourElement && void 0 !== g.minuteElement) {
        var e,
            t,
            n = (parseInt(g.hourElement.value.slice(-2), 10) || 0) % 24,
            a = (parseInt(g.minuteElement.value, 10) || 0) % 60,
            i = void 0 !== g.secondElement ? (parseInt(g.secondElement.value, 10) || 0) % 60 : 0;void 0 !== g.amPM && (e = n, t = g.amPM.textContent, n = e % 12 + 12 * o(t === g.l10n.amPM[1]));var r = void 0 !== g.config.minTime || g.config.minDate && g.minDateHasTime && g.latestSelectedDateObj && 0 === w(g.latestSelectedDateObj, g.config.minDate, !0);if (void 0 !== g.config.maxTime || g.config.maxDate && g.maxDateHasTime && g.latestSelectedDateObj && 0 === w(g.latestSelectedDateObj, g.config.maxDate, !0)) {
          var l = void 0 !== g.config.maxTime ? g.config.maxTime : g.config.maxDate;(n = Math.min(n, l.getHours())) === l.getHours() && (a = Math.min(a, l.getMinutes())), a === l.getMinutes() && (i = Math.min(i, l.getSeconds()));
        }if (r) {
          var c = void 0 !== g.config.minTime ? g.config.minTime : g.config.minDate;(n = Math.max(n, c.getHours())) === c.getHours() && (a = Math.max(a, c.getMinutes())), a === c.getMinutes() && (i = Math.max(i, c.getSeconds()));
        }O(n, a, i);
      }
    }function k(e) {
      var t = e || g.latestSelectedDateObj;t && O(t.getHours(), t.getMinutes(), t.getSeconds());
    }function I() {
      var e = g.config.defaultHour,
          t = g.config.defaultMinute,
          n = g.config.defaultSeconds;if (void 0 !== g.config.minDate) {
        var a = g.config.minDate.getHours(),
            i = g.config.minDate.getMinutes();(e = Math.max(e, a)) === a && (t = Math.max(i, t)), e === a && t === i && (n = g.config.minDate.getSeconds());
      }if (void 0 !== g.config.maxDate) {
        var o = g.config.maxDate.getHours(),
            r = g.config.maxDate.getMinutes();(e = Math.min(e, o)) === o && (t = Math.min(r, t)), e === o && t === r && (n = g.config.maxDate.getSeconds());
      }O(e, t, n);
    }function O(e, t, n) {
      void 0 !== g.latestSelectedDateObj && g.latestSelectedDateObj.setHours(e % 24, t, n || 0, 0), g.hourElement && g.minuteElement && !g.isMobile && (g.hourElement.value = _i(g.config.time_24hr ? e : (12 + e) % 12 + 12 * o(e % 12 == 0)), g.minuteElement.value = _i(t), void 0 !== g.amPM && (g.amPM.textContent = g.l10n.amPM[o(e >= 12)]), void 0 !== g.secondElement && (g.secondElement.value = _i(n)));
    }function S(e) {
      var t = parseInt(e.target.value) + (e.delta || 0);(t / 1e3 > 1 || "Enter" === e.key && !/[^\d]/.test(t.toString())) && V(t);
    }function _(e, t, n, a) {
      return t instanceof Array ? t.forEach(function (t) {
        return _(e, t, n, a);
      }) : e instanceof Array ? e.forEach(function (e) {
        return _(e, t, n, a);
      }) : (e.addEventListener(t, n, a), void g._handlers.push({ element: e, event: t, handler: n, options: a }));
    }function N(e) {
      return function (t) {
        1 === t.which && e(t);
      };
    }function F() {
      fe("onChange");
    }function P(e) {
      var t = void 0 !== e ? g.parseDate(e) : g.latestSelectedDateObj || (g.config.minDate && g.config.minDate > g.now ? g.config.minDate : g.config.maxDate && g.config.maxDate < g.now ? g.config.maxDate : g.now);try {
        void 0 !== t && (g.currentYear = t.getFullYear(), g.currentMonth = t.getMonth());
      } catch (e) {
        e.message = "Invalid date supplied: " + t, g.config.errorHandler(e);
      }g.redraw();
    }function A(e) {
      ~e.target.className.indexOf("arrow") && Y(e, e.target.classList.contains("arrowUp") ? 1 : -1);
    }function Y(e, t, n) {
      var a = e && e.target,
          i = n || a && a.parentNode && a.parentNode.firstChild,
          o = me("increment");o.delta = t, i && i.dispatchEvent(o);
    }function j(e, t, n, a) {
      var i = Z(t, !0),
          o = d("span", "flatpickr-day " + e, t.getDate().toString());return o.dateObj = t, o.$i = a, o.setAttribute("aria-label", g.formatDate(t, g.config.ariaDateFormat)), -1 === e.indexOf("hidden") && 0 === w(t, g.now) && (g.todayDateElem = o, o.classList.add("today"), o.setAttribute("aria-current", "date")), i ? (o.tabIndex = -1, ge(t) && (o.classList.add("selected"), g.selectedDateElem = o, "range" === g.config.mode && (c(o, "startRange", g.selectedDates[0] && 0 === w(t, g.selectedDates[0], !0)), c(o, "endRange", g.selectedDates[1] && 0 === w(t, g.selectedDates[1], !0)), "nextMonthDay" === e && o.classList.add("inRange")))) : o.classList.add("disabled"), "range" === g.config.mode && function (e) {
        return !("range" !== g.config.mode || g.selectedDates.length < 2) && w(e, g.selectedDates[0]) >= 0 && w(e, g.selectedDates[1]) <= 0;
      }(t) && !ge(t) && o.classList.add("inRange"), g.weekNumbers && 1 === g.config.showMonths && "prevMonthDay" !== e && n % 7 == 1 && g.weekNumbers.insertAdjacentHTML("beforeend", "<span class='flatpickr-day'>" + g.config.getWeek(t) + "</span>"), fe("onDayCreate", o), o;
    }function H(e) {
      e.focus(), "range" === g.config.mode && ee(e);
    }function L(e) {
      for (var t = e > 0 ? 0 : g.config.showMonths - 1, n = e > 0 ? g.config.showMonths : -1, a = t; a != n; a += e) {
        for (var i = g.daysContainer.children[a], o = e > 0 ? 0 : i.children.length - 1, r = e > 0 ? i.children.length : -1, l = o; l != r; l += e) {
          var c = i.children[l];if (-1 === c.className.indexOf("hidden") && Z(c.dateObj)) return c;
        }
      }
    }function W(e, t) {
      var n = Q(document.activeElement || document.body),
          a = void 0 !== e ? e : n ? document.activeElement : void 0 !== g.selectedDateElem && Q(g.selectedDateElem) ? g.selectedDateElem : void 0 !== g.todayDateElem && Q(g.todayDateElem) ? g.todayDateElem : L(t > 0 ? 1 : -1);return void 0 === a ? g._input.focus() : n ? void function (e, t) {
        for (var n = -1 === e.className.indexOf("Month") ? e.dateObj.getMonth() : g.currentMonth, a = t > 0 ? g.config.showMonths : -1, i = t > 0 ? 1 : -1, o = n - g.currentMonth; o != a; o += i) {
          for (var r = g.daysContainer.children[o], l = n - g.currentMonth === o ? e.$i + t : t < 0 ? r.children.length - 1 : 0, c = r.children.length, d = l; d >= 0 && d < c && d != (t > 0 ? c : -1); d += i) {
            var s = r.children[d];if (-1 === s.className.indexOf("hidden") && Z(s.dateObj) && Math.abs(e.$i - d) >= Math.abs(t)) return H(s);
          }
        }g.changeMonth(i), W(L(i), 0);
      }(a, t) : H(a);
    }function R(e, t) {
      for (var n = (new Date(e, t, 1).getDay() - g.l10n.firstDayOfWeek + 7) % 7, a = g.utils.getDaysInMonth((t - 1 + 12) % 12), i = g.utils.getDaysInMonth(t), o = window.document.createDocumentFragment(), r = g.config.showMonths > 1, l = r ? "prevMonthDay hidden" : "prevMonthDay", c = r ? "nextMonthDay hidden" : "nextMonthDay", s = a + 1 - n, u = 0; s <= a; s++, u++) {
        o.appendChild(j(l, new Date(e, t - 1, s), s, u));
      }for (s = 1; s <= i; s++, u++) {
        o.appendChild(j("", new Date(e, t, s), s, u));
      }for (var f = i + 1; f <= 42 - n && (1 === g.config.showMonths || u % 7 != 0); f++, u++) {
        o.appendChild(j(c, new Date(e, t + 1, f % i), f, u));
      }var m = d("div", "dayContainer");return m.appendChild(o), m;
    }function B() {
      if (void 0 !== g.daysContainer) {
        s(g.daysContainer), g.weekNumbers && s(g.weekNumbers);for (var e = document.createDocumentFragment(), t = 0; t < g.config.showMonths; t++) {
          var n = new Date(g.currentYear, g.currentMonth, 1);n.setMonth(g.currentMonth + t), e.appendChild(R(n.getFullYear(), n.getMonth()));
        }g.daysContainer.appendChild(e), g.days = g.daysContainer.firstChild, "range" === g.config.mode && 1 === g.selectedDates.length && ee();
      }
    }function K() {
      var e = d("div", "flatpickr-month"),
          t = window.document.createDocumentFragment(),
          n = d("span", "cur-month"),
          a = u("cur-year", { tabindex: "-1" }),
          i = a.getElementsByTagName("input")[0];i.setAttribute("aria-label", g.l10n.yearAriaLabel), g.config.minDate && i.setAttribute("min", g.config.minDate.getFullYear().toString()), g.config.maxDate && (i.setAttribute("max", g.config.maxDate.getFullYear().toString()), i.disabled = !!g.config.minDate && g.config.minDate.getFullYear() === g.config.maxDate.getFullYear());var o = d("div", "flatpickr-current-month");return o.appendChild(n), o.appendChild(a), t.appendChild(o), e.appendChild(t), { container: e, yearElement: i, monthElement: n };
    }function J() {
      s(g.monthNav), g.monthNav.appendChild(g.prevMonthNav), g.config.showMonths && (g.yearElements = [], g.monthElements = []);for (var e = g.config.showMonths; e--;) {
        var t = K();g.yearElements.push(t.yearElement), g.monthElements.push(t.monthElement), g.monthNav.appendChild(t.container);
      }g.monthNav.appendChild(g.nextMonthNav);
    }function U() {
      g.weekdayContainer ? s(g.weekdayContainer) : g.weekdayContainer = d("div", "flatpickr-weekdays");for (var e = g.config.showMonths; e--;) {
        var t = d("div", "flatpickr-weekdaycontainer");g.weekdayContainer.appendChild(t);
      }return q(), g.weekdayContainer;
    }function q() {
      var e = g.l10n.firstDayOfWeek,
          t = g.l10n.weekdays.shorthand.slice();e > 0 && e < t.length && (t = t.splice(e, t.length).concat(t.splice(0, e)));for (var n = g.config.showMonths; n--;) {
        g.weekdayContainer.children[n].innerHTML = "\n      <span class='flatpickr-weekday'>\n        " + t.join("</span><span class='flatpickr-weekday'>") + "\n      </span>\n      ";
      }
    }function $(e, t) {
      void 0 === t && (t = !0);var n = t ? e : e - g.currentMonth;n < 0 && !0 === g._hidePrevMonthArrow || n > 0 && !0 === g._hideNextMonthArrow || (g.currentMonth += n, (g.currentMonth < 0 || g.currentMonth > 11) && (g.currentYear += g.currentMonth > 11 ? 1 : -1, g.currentMonth = (g.currentMonth + 12) % 12, fe("onYearChange")), B(), fe("onMonthChange"), pe());
    }function z(e) {
      return !(!g.config.appendTo || !g.config.appendTo.contains(e)) || g.calendarContainer.contains(e);
    }function G(e) {
      if (g.isOpen && !g.config.inline) {
        var t = "function" == typeof (r = e).composedPath ? r.composedPath()[0] : r.target,
            n = z(t),
            a = t === g.input || t === g.altInput || g.element.contains(t) || e.path && e.path.indexOf && (~e.path.indexOf(g.input) || ~e.path.indexOf(g.altInput)),
            i = "blur" === e.type ? a && e.relatedTarget && !z(e.relatedTarget) : !a && !n && !z(e.relatedTarget),
            o = !g.config.ignoredFocusElements.some(function (e) {
          return e.contains(t);
        });i && o && (g.close(), "range" === g.config.mode && 1 === g.selectedDates.length && (g.clear(!1), g.redraw()));
      }var r;
    }function V(e) {
      if (!(!e || g.config.minDate && e < g.config.minDate.getFullYear() || g.config.maxDate && e > g.config.maxDate.getFullYear())) {
        var t = e,
            n = g.currentYear !== t;g.currentYear = t || g.currentYear, g.config.maxDate && g.currentYear === g.config.maxDate.getFullYear() ? g.currentMonth = Math.min(g.config.maxDate.getMonth(), g.currentMonth) : g.config.minDate && g.currentYear === g.config.minDate.getFullYear() && (g.currentMonth = Math.max(g.config.minDate.getMonth(), g.currentMonth)), n && (g.redraw(), fe("onYearChange"));
      }
    }function Z(e, t) {
      void 0 === t && (t = !0);var n = g.parseDate(e, void 0, t);if (g.config.minDate && n && w(n, g.config.minDate, void 0 !== t ? t : !g.minDateHasTime) < 0 || g.config.maxDate && n && w(n, g.config.maxDate, void 0 !== t ? t : !g.maxDateHasTime) > 0) return !1;if (0 === g.config.enable.length && 0 === g.config.disable.length) return !0;if (void 0 === n) return !1;for (var a = g.config.enable.length > 0, i = a ? g.config.enable : g.config.disable, o = 0, r = void 0; o < i.length; o++) {
        if ("function" == typeof (r = i[o]) && r(n)) return a;if (r instanceof Date && void 0 !== n && r.getTime() === n.getTime()) return a;if ("string" == typeof r && void 0 !== n) {
          var l = g.parseDate(r, void 0, !0);return l && l.getTime() === n.getTime() ? a : !a;
        }if ("object" == (typeof r === "undefined" ? "undefined" : _typeof(r)) && void 0 !== n && r.from && r.to && n.getTime() >= r.from.getTime() && n.getTime() <= r.to.getTime()) return a;
      }return !a;
    }function Q(e) {
      return void 0 !== g.daysContainer && -1 === e.className.indexOf("hidden") && g.daysContainer.contains(e);
    }function X(e) {
      var t = e.target === g._input,
          n = g.config.allowInput,
          a = g.isOpen && (!n || !t),
          i = g.config.inline && t && !n;if (13 === e.keyCode && t) {
        if (n) return g.setDate(g._input.value, !0, e.target === g.altInput ? g.config.altFormat : g.config.dateFormat), e.target.blur();g.open();
      } else if (z(e.target) || a || i) {
        var o = !!g.timeContainer && g.timeContainer.contains(e.target);switch (e.keyCode) {case 13:
            o ? (x(), le()) : ce(e);break;case 27:
            e.preventDefault(), le();break;case 8:case 46:
            t && !g.config.allowInput && (e.preventDefault(), g.clear());break;case 37:case 39:
            if (o) g.hourElement && g.hourElement.focus();else if (e.preventDefault(), void 0 !== g.daysContainer && (!1 === n || document.activeElement && Q(document.activeElement))) {
              var r = 39 === e.keyCode ? 1 : -1;e.ctrlKey ? (e.stopPropagation(), $(r), W(L(1), 0)) : W(void 0, r);
            }break;case 38:case 40:
            e.preventDefault();var l = 40 === e.keyCode ? 1 : -1;g.daysContainer && void 0 !== e.target.$i || e.target === g.input ? e.ctrlKey ? (e.stopPropagation(), V(g.currentYear - l), W(L(1), 0)) : o || W(void 0, 7 * l) : g.config.enableTime && (!o && g.hourElement && g.hourElement.focus(), x(e), g._debouncedChange());break;case 9:
            if (o) {
              var c = [g.hourElement, g.minuteElement, g.secondElement, g.amPM].filter(function (e) {
                return e;
              }),
                  d = c.indexOf(e.target);if (-1 !== d) {
                var s = c[d + (e.shiftKey ? -1 : 1)];void 0 !== s ? (e.preventDefault(), s.focus()) : e.shiftKey && (e.preventDefault(), g._input.focus());
              }
            }}
      }if (void 0 !== g.amPM && e.target === g.amPM) switch (e.key) {case g.l10n.amPM[0].charAt(0):case g.l10n.amPM[0].charAt(0).toLowerCase():
          g.amPM.textContent = g.l10n.amPM[0], T(), ve();break;case g.l10n.amPM[1].charAt(0):case g.l10n.amPM[1].charAt(0).toLowerCase():
          g.amPM.textContent = g.l10n.amPM[1], T(), ve();}fe("onKeyDown", e);
    }function ee(e) {
      if (1 === g.selectedDates.length && (!e || e.classList.contains("flatpickr-day") && !e.classList.contains("disabled"))) {
        for (var t = e ? e.dateObj.getTime() : g.days.firstElementChild.dateObj.getTime(), n = g.parseDate(g.selectedDates[0], void 0, !0).getTime(), a = Math.min(t, g.selectedDates[0].getTime()), i = Math.max(t, g.selectedDates[0].getTime()), o = g.daysContainer.lastChild.lastChild.dateObj.getTime(), r = !1, l = 0, c = 0, d = a; d < o; d += y.DAY) {
          Z(new Date(d), !0) || (r = r || d > a && d < i, d < n && (!l || d > l) ? l = d : d > n && (!c || d < c) && (c = d));
        }for (var s = 0; s < g.config.showMonths; s++) {
          for (var u = g.daysContainer.children[s], f = g.daysContainer.children[s - 1], m = function m(a, i) {
            var o = u.children[a],
                d = o.dateObj.getTime(),
                m = l > 0 && d < l || c > 0 && d > c;return m ? (o.classList.add("notAllowed"), ["inRange", "startRange", "endRange"].forEach(function (e) {
              o.classList.remove(e);
            }), "continue") : r && !m ? "continue" : (["startRange", "inRange", "endRange", "notAllowed"].forEach(function (e) {
              o.classList.remove(e);
            }), void (void 0 !== e && (e.classList.add(t < g.selectedDates[0].getTime() ? "startRange" : "endRange"), !u.contains(e) && s > 0 && f && f.lastChild.dateObj.getTime() >= d || (n < t && d === n ? o.classList.add("startRange") : n > t && d === n && o.classList.add("endRange"), d >= l && (0 === c || d <= c) && b(d, n, t) && o.classList.add("inRange")))));
          }, p = 0, h = u.children.length; p < h; p++) {
            m(p);
          }
        }
      }
    }function te() {
      !g.isOpen || g.config.static || g.config.inline || oe();
    }function ne() {
      g.setDate(void 0 !== g.config.minDate ? new Date(g.config.minDate.getTime()) : new Date(), !1), I(), ve();
    }function ae(e) {
      return function (t) {
        var n = g.config["_" + e + "Date"] = g.parseDate(t, g.config.dateFormat),
            a = g.config["_" + ("min" === e ? "max" : "min") + "Date"];void 0 !== n && (g["min" === e ? "minDateHasTime" : "maxDateHasTime"] = n.getHours() > 0 || n.getMinutes() > 0 || n.getSeconds() > 0), g.selectedDates && (g.selectedDates = g.selectedDates.filter(function (e) {
          return Z(e);
        }), g.selectedDates.length || "min" !== e || k(n), ve()), g.daysContainer && (re(), void 0 !== n ? g.currentYearElement[e] = n.getFullYear().toString() : g.currentYearElement.removeAttribute(e), g.currentYearElement.disabled = !!a && void 0 !== n && a.getFullYear() === n.getFullYear());
      };
    }function ie() {
      "object" != _typeof(g.config.locale) && void 0 === E.l10ns[g.config.locale] && g.config.errorHandler(new Error("flatpickr: invalid locale " + g.config.locale)), g.l10n = _e({}, E.l10ns.default, "object" == _typeof(g.config.locale) ? g.config.locale : "default" !== g.config.locale ? E.l10ns[g.config.locale] : void 0), p.K = "(" + g.l10n.amPM[0] + "|" + g.l10n.amPM[1] + "|" + g.l10n.amPM[0].toLowerCase() + "|" + g.l10n.amPM[1].toLowerCase() + ")", g.formatDate = v(g), g.parseDate = D({ config: g.config, l10n: g.l10n });
    }function oe(e) {
      if (void 0 !== g.calendarContainer) {
        fe("onPreCalendarPosition");var t = e || g._positionElement,
            n = Array.prototype.reduce.call(g.calendarContainer.children, function (e, t) {
          return e + t.offsetHeight;
        }, 0),
            a = g.calendarContainer.offsetWidth,
            i = g.config.position.split(" "),
            o = i[0],
            r = i.length > 1 ? i[1] : null,
            l = t.getBoundingClientRect(),
            d = window.innerHeight - l.bottom,
            s = "above" === o || "below" !== o && d < n && l.top > n,
            u = window.pageYOffset + l.top + (s ? -n - 2 : t.offsetHeight + 2);if (c(g.calendarContainer, "arrowTop", !s), c(g.calendarContainer, "arrowBottom", s), !g.config.inline) {
          var f = window.pageXOffset + l.left - (null != r && "center" === r ? (a - l.width) / 2 : 0),
              m = window.document.body.offsetWidth - l.right,
              p = f + a > window.document.body.offsetWidth,
              h = m + a > window.document.body.offsetWidth;if (c(g.calendarContainer, "rightMost", p), !g.config.static) if (g.calendarContainer.style.top = u + "px", p) {
            if (h) {
              var v = document.styleSheets[0];if (void 0 === v) return;var D = window.document.body.offsetWidth,
                  w = Math.max(0, D / 2 - a / 2),
                  b = v.cssRules.length,
                  y = "{left:" + l.left + "px;right:auto;}";c(g.calendarContainer, "rightMost", !1), c(g.calendarContainer, "centerMost", !0), v.insertRule(".flatpickr-calendar.centerMost:before,.flatpickr-calendar.centerMost:after" + y, b), g.calendarContainer.style.left = w + "px", g.calendarContainer.style.right = "auto";
            } else g.calendarContainer.style.left = "auto", g.calendarContainer.style.right = m + "px";
          } else g.calendarContainer.style.left = f + "px", g.calendarContainer.style.right = "auto";
        }
      }
    }function re() {
      g.config.noCalendar || g.isMobile || (pe(), B());
    }function le() {
      g._input.focus(), -1 !== window.navigator.userAgent.indexOf("MSIE") || void 0 !== navigator.msMaxTouchPoints ? setTimeout(g.close, 0) : g.close();
    }function ce(e) {
      e.preventDefault(), e.stopPropagation();var t = function e(t, n) {
        return n(t) ? t : t.parentNode ? e(t.parentNode, n) : void 0;
      }(e.target, function (e) {
        return e.classList && e.classList.contains("flatpickr-day") && !e.classList.contains("disabled") && !e.classList.contains("notAllowed");
      });if (void 0 !== t) {
        var n = t,
            a = g.latestSelectedDateObj = new Date(n.dateObj.getTime()),
            i = (a.getMonth() < g.currentMonth || a.getMonth() > g.currentMonth + g.config.showMonths - 1) && "range" !== g.config.mode;if (g.selectedDateElem = n, "single" === g.config.mode) g.selectedDates = [a];else if ("multiple" === g.config.mode) {
          var o = ge(a);o ? g.selectedDates.splice(parseInt(o), 1) : g.selectedDates.push(a);
        } else "range" === g.config.mode && (2 === g.selectedDates.length && g.clear(!1, !1), g.latestSelectedDateObj = a, g.selectedDates.push(a), 0 !== w(a, g.selectedDates[0], !0) && g.selectedDates.sort(function (e, t) {
          return e.getTime() - t.getTime();
        }));if (T(), i) {
          var r = g.currentYear !== a.getFullYear();g.currentYear = a.getFullYear(), g.currentMonth = a.getMonth(), r && fe("onYearChange"), fe("onMonthChange");
        }if (pe(), B(), ve(), g.config.enableTime && setTimeout(function () {
          return g.showTimeInput = !0;
        }, 50), i || "range" === g.config.mode || 1 !== g.config.showMonths ? void 0 !== g.selectedDateElem && void 0 === g.hourElement && g.selectedDateElem && g.selectedDateElem.focus() : H(n), void 0 !== g.hourElement && void 0 !== g.hourElement && g.hourElement.focus(), g.config.closeOnSelect) {
          var l = "single" === g.config.mode && !g.config.enableTime,
              c = "range" === g.config.mode && 2 === g.selectedDates.length && !g.config.enableTime;(l || c) && le();
        }F();
      }
    }g.parseDate = D({ config: g.config, l10n: g.l10n }), g._handlers = [], g._bind = _, g._setHoursFromDate = k, g._positionCalendar = oe, g.changeMonth = $, g.changeYear = V, g.clear = function (e, t) {
      void 0 === e && (e = !0);void 0 === t && (t = !0);g.input.value = "", void 0 !== g.altInput && (g.altInput.value = "");void 0 !== g.mobileInput && (g.mobileInput.value = "");g.selectedDates = [], g.latestSelectedDateObj = void 0, !0 === t && (g.currentYear = g._initialDate.getFullYear(), g.currentMonth = g._initialDate.getMonth());g.showTimeInput = !1, !0 === g.config.enableTime && I();g.redraw(), e && fe("onChange");
    }, g.close = function () {
      g.isOpen = !1, g.isMobile || (void 0 !== g.calendarContainer && g.calendarContainer.classList.remove("open"), void 0 !== g._input && g._input.classList.remove("active"));fe("onClose");
    }, g._createElement = d, g.destroy = function () {
      void 0 !== g.config && fe("onDestroy");for (var e = g._handlers.length; e--;) {
        var t = g._handlers[e];t.element.removeEventListener(t.event, t.handler, t.options);
      }if (g._handlers = [], g.mobileInput) g.mobileInput.parentNode && g.mobileInput.parentNode.removeChild(g.mobileInput), g.mobileInput = void 0;else if (g.calendarContainer && g.calendarContainer.parentNode) if (g.config.static && g.calendarContainer.parentNode) {
        var n = g.calendarContainer.parentNode;if (n.lastChild && n.removeChild(n.lastChild), n.parentNode) {
          for (; n.firstChild;) {
            n.parentNode.insertBefore(n.firstChild, n);
          }n.parentNode.removeChild(n);
        }
      } else g.calendarContainer.parentNode.removeChild(g.calendarContainer);g.altInput && (g.input.type = "text", g.altInput.parentNode && g.altInput.parentNode.removeChild(g.altInput), delete g.altInput);g.input && (g.input.type = g.input._type, g.input.classList.remove("flatpickr-input"), g.input.removeAttribute("readonly"), g.input.value = "");["_showTimeInput", "latestSelectedDateObj", "_hideNextMonthArrow", "_hidePrevMonthArrow", "__hideNextMonthArrow", "__hidePrevMonthArrow", "isMobile", "isOpen", "selectedDateElem", "minDateHasTime", "maxDateHasTime", "days", "daysContainer", "_input", "_positionElement", "innerContainer", "rContainer", "monthNav", "todayDateElem", "calendarContainer", "weekdayContainer", "prevMonthNav", "nextMonthNav", "currentMonthElement", "currentYearElement", "navigationCurrentMonth", "selectedDateElem", "config"].forEach(function (e) {
        try {
          delete g[e];
        } catch (e) {}
      });
    }, g.isEnabled = Z, g.jumpToDate = P, g.open = function (e, t) {
      void 0 === t && (t = g._positionElement);if (!0 === g.isMobile) return e && (e.preventDefault(), e.target && e.target.blur()), void 0 !== g.mobileInput && (g.mobileInput.focus(), g.mobileInput.click()), void fe("onOpen");if (g._input.disabled || g.config.inline) return;var n = g.isOpen;g.isOpen = !0, n || (g.calendarContainer.classList.add("open"), g._input.classList.add("active"), fe("onOpen"), oe(t));!0 === g.config.enableTime && !0 === g.config.noCalendar && (0 === g.selectedDates.length && ne(), !1 !== g.config.allowInput || void 0 !== e && g.timeContainer.contains(e.relatedTarget) || setTimeout(function () {
        return g.hourElement.select();
      }, 50));
    }, g.redraw = re, g.set = function (e, n) {
      null !== e && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) ? Object.assign(g.config, e) : (g.config[e] = n, void 0 !== de[e] ? de[e].forEach(function (e) {
        return e();
      }) : t.indexOf(e) > -1 && (g.config[e] = l(n)));g.redraw(), ve(!1);
    }, g.setDate = function (e, t, n) {
      void 0 === t && (t = !1);void 0 === n && (n = g.config.dateFormat);if (0 !== e && !e || e instanceof Array && 0 === e.length) return g.clear(t);se(e, n), g.showTimeInput = g.selectedDates.length > 0, g.latestSelectedDateObj = g.selectedDates[0], g.redraw(), P(), k(), ve(t), t && fe("onChange");
    }, g.toggle = function (e) {
      if (!0 === g.isOpen) return g.close();g.open(e);
    };var de = { locale: [ie, q], showMonths: [J, M, U] };function se(e, t) {
      var n = [];if (e instanceof Array) n = e.map(function (e) {
        return g.parseDate(e, t);
      });else if (e instanceof Date || "number" == typeof e) n = [g.parseDate(e, t)];else if ("string" == typeof e) switch (g.config.mode) {case "single":case "time":
          n = [g.parseDate(e, t)];break;case "multiple":
          n = e.split(g.config.conjunction).map(function (e) {
            return g.parseDate(e, t);
          });break;case "range":
          n = e.split(g.l10n.rangeSeparator).map(function (e) {
            return g.parseDate(e, t);
          });} else g.config.errorHandler(new Error("Invalid date supplied: " + JSON.stringify(e)));g.selectedDates = n.filter(function (e) {
        return e instanceof Date && Z(e, !1);
      }), "range" === g.config.mode && g.selectedDates.sort(function (e, t) {
        return e.getTime() - t.getTime();
      });
    }function ue(e) {
      return e.slice().map(function (e) {
        return "string" == typeof e || "number" == typeof e || e instanceof Date ? g.parseDate(e, void 0, !0) : e && "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && e.from && e.to ? { from: g.parseDate(e.from, void 0), to: g.parseDate(e.to, void 0) } : e;
      }).filter(function (e) {
        return e;
      });
    }function fe(e, t) {
      if (void 0 !== g.config) {
        var n = g.config[e];if (void 0 !== n && n.length > 0) for (var a = 0; n[a] && a < n.length; a++) {
          n[a](g.selectedDates, g.input.value, g, t);
        }"onChange" === e && (g.input.dispatchEvent(me("change")), g.input.dispatchEvent(me("input")));
      }
    }function me(e) {
      var t = document.createEvent("Event");return t.initEvent(e, !0, !0), t;
    }function ge(e) {
      for (var t = 0; t < g.selectedDates.length; t++) {
        if (0 === w(g.selectedDates[t], e)) return "" + t;
      }return !1;
    }function pe() {
      g.config.noCalendar || g.isMobile || !g.monthNav || (g.yearElements.forEach(function (e, t) {
        var n = new Date(g.currentYear, g.currentMonth, 1);n.setMonth(g.currentMonth + t), g.monthElements[t].textContent = m(n.getMonth(), g.config.shorthandCurrentMonth, g.l10n) + " ", e.value = n.getFullYear().toString();
      }), g._hidePrevMonthArrow = void 0 !== g.config.minDate && (g.currentYear === g.config.minDate.getFullYear() ? g.currentMonth <= g.config.minDate.getMonth() : g.currentYear < g.config.minDate.getFullYear()), g._hideNextMonthArrow = void 0 !== g.config.maxDate && (g.currentYear === g.config.maxDate.getFullYear() ? g.currentMonth + 1 > g.config.maxDate.getMonth() : g.currentYear > g.config.maxDate.getFullYear()));
    }function he(e) {
      return g.selectedDates.map(function (t) {
        return g.formatDate(t, e);
      }).filter(function (e, t, n) {
        return "range" !== g.config.mode || g.config.enableTime || n.indexOf(e) === t;
      }).join("range" !== g.config.mode ? g.config.conjunction : g.l10n.rangeSeparator);
    }function ve(e) {
      if (void 0 === e && (e = !0), 0 === g.selectedDates.length) return g.clear(e);void 0 !== g.mobileInput && g.mobileFormatStr && (g.mobileInput.value = void 0 !== g.latestSelectedDateObj ? g.formatDate(g.latestSelectedDateObj, g.mobileFormatStr) : ""), g.input.value = he(g.config.dateFormat), void 0 !== g.altInput && (g.altInput.value = he(g.config.altFormat)), !1 !== e && fe("onValueUpdate");
    }function De(e) {
      e.preventDefault();var t = g.prevMonthNav.contains(e.target),
          n = g.nextMonthNav.contains(e.target);t || n ? $(t ? -1 : 1) : g.yearElements.indexOf(e.target) >= 0 ? e.target.select() : e.target.classList.contains("arrowUp") ? g.changeYear(g.currentYear + 1) : e.target.classList.contains("arrowDown") && g.changeYear(g.currentYear - 1);
    }return function () {
      g.element = g.input = n, g.isOpen = !1, function () {
        var a = ["wrap", "weekNumbers", "allowInput", "clickOpens", "time_24hr", "enableTime", "noCalendar", "altInput", "shorthandCurrentMonth", "inline", "static", "enableSeconds", "disableMobile"],
            i = _e({}, f, JSON.parse(JSON.stringify(n.dataset || {}))),
            o = {};g.config.parseDate = i.parseDate, g.config.formatDate = i.formatDate, Object.defineProperty(g.config, "enable", { get: function get() {
            return g.config._enable;
          }, set: function set(e) {
            g.config._enable = ue(e);
          } }), Object.defineProperty(g.config, "disable", { get: function get() {
            return g.config._disable;
          }, set: function set(e) {
            g.config._disable = ue(e);
          } });var r = "time" === i.mode;i.dateFormat || !i.enableTime && !r || (o.dateFormat = i.noCalendar || r ? "H:i" + (i.enableSeconds ? ":S" : "") : E.defaultConfig.dateFormat + " H:i" + (i.enableSeconds ? ":S" : "")), i.altInput && (i.enableTime || r) && !i.altFormat && (o.altFormat = i.noCalendar || r ? "h:i" + (i.enableSeconds ? ":S K" : " K") : E.defaultConfig.altFormat + " h:i" + (i.enableSeconds ? ":S" : "") + " K"), Object.defineProperty(g.config, "minDate", { get: function get() {
            return g.config._minDate;
          }, set: ae("min") }), Object.defineProperty(g.config, "maxDate", { get: function get() {
            return g.config._maxDate;
          }, set: ae("max") });var c = function c(e) {
          return function (t) {
            g.config["min" === e ? "_minTime" : "_maxTime"] = g.parseDate(t, "H:i");
          };
        };Object.defineProperty(g.config, "minTime", { get: function get() {
            return g.config._minTime;
          }, set: c("min") }), Object.defineProperty(g.config, "maxTime", { get: function get() {
            return g.config._maxTime;
          }, set: c("max") }), "time" === i.mode && (g.config.noCalendar = !0, g.config.enableTime = !0), Object.assign(g.config, o, i);for (var d = 0; d < a.length; d++) {
          g.config[a[d]] = !0 === g.config[a[d]] || "true" === g.config[a[d]];
        }t.filter(function (e) {
          return void 0 !== g.config[e];
        }).forEach(function (e) {
          g.config[e] = l(g.config[e] || []).map(h);
        }), g.isMobile = !g.config.disableMobile && !g.config.inline && "single" === g.config.mode && !g.config.disable.length && !g.config.enable.length && !g.config.weekNumbers && /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);for (var d = 0; d < g.config.plugins.length; d++) {
          var s = g.config.plugins[d](g) || {};for (var u in s) {
            t.indexOf(u) > -1 ? g.config[u] = l(s[u]).map(h).concat(g.config[u]) : void 0 === i[u] && (g.config[u] = s[u]);
          }
        }fe("onParseConfig");
      }(), ie(), g.input = g.config.wrap ? n.querySelector("[data-input]") : n, g.input ? (g.input._type = g.input.type, g.input.type = "text", g.input.classList.add("flatpickr-input"), g._input = g.input, g.config.altInput && (g.altInput = d(g.input.nodeName, g.input.className + " " + g.config.altInputClass), g._input = g.altInput, g.altInput.placeholder = g.input.placeholder, g.altInput.disabled = g.input.disabled, g.altInput.required = g.input.required, g.altInput.tabIndex = g.input.tabIndex, g.altInput.type = "text", g.input.setAttribute("type", "hidden"), !g.config.static && g.input.parentNode && g.input.parentNode.insertBefore(g.altInput, g.input.nextSibling)), g.config.allowInput || g._input.setAttribute("readonly", "readonly"), g._positionElement = g.config.positionElement || g._input) : g.config.errorHandler(new Error("Invalid input element specified")), function () {
        g.selectedDates = [], g.now = g.parseDate(g.config.now) || new Date();var e = g.config.defaultDate || ("INPUT" !== g.input.nodeName && "TEXTAREA" !== g.input.nodeName || !g.input.placeholder || g.input.value !== g.input.placeholder ? g.input.value : null);e && se(e, g.config.dateFormat), g._initialDate = g.selectedDates.length > 0 ? g.selectedDates[0] : g.config.minDate && g.config.minDate.getTime() > g.now.getTime() ? g.config.minDate : g.config.maxDate && g.config.maxDate.getTime() < g.now.getTime() ? g.config.maxDate : g.now, g.currentYear = g._initialDate.getFullYear(), g.currentMonth = g._initialDate.getMonth(), g.selectedDates.length > 0 && (g.latestSelectedDateObj = g.selectedDates[0]), void 0 !== g.config.minTime && (g.config.minTime = g.parseDate(g.config.minTime, "H:i")), void 0 !== g.config.maxTime && (g.config.maxTime = g.parseDate(g.config.maxTime, "H:i")), g.minDateHasTime = !!g.config.minDate && (g.config.minDate.getHours() > 0 || g.config.minDate.getMinutes() > 0 || g.config.minDate.getSeconds() > 0), g.maxDateHasTime = !!g.config.maxDate && (g.config.maxDate.getHours() > 0 || g.config.maxDate.getMinutes() > 0 || g.config.maxDate.getSeconds() > 0), Object.defineProperty(g, "showTimeInput", { get: function get() {
            return g._showTimeInput;
          }, set: function set(e) {
            g._showTimeInput = e, g.calendarContainer && c(g.calendarContainer, "showTimeInput", e), g.isOpen && oe();
          } });
      }(), g.utils = { getDaysInMonth: function getDaysInMonth(e, t) {
          return void 0 === e && (e = g.currentMonth), void 0 === t && (t = g.currentYear), 1 === e && (t % 4 == 0 && t % 100 != 0 || t % 400 == 0) ? 29 : g.l10n.daysInMonth[e];
        } }, g.isMobile || function () {
        var e = window.document.createDocumentFragment();if (g.calendarContainer = d("div", "flatpickr-calendar"), g.calendarContainer.tabIndex = -1, !g.config.noCalendar) {
          if (e.appendChild((g.monthNav = d("div", "flatpickr-months"), g.yearElements = [], g.monthElements = [], g.prevMonthNav = d("span", "flatpickr-prev-month"), g.prevMonthNav.innerHTML = g.config.prevArrow, g.nextMonthNav = d("span", "flatpickr-next-month"), g.nextMonthNav.innerHTML = g.config.nextArrow, J(), Object.defineProperty(g, "_hidePrevMonthArrow", { get: function get() {
              return g.__hidePrevMonthArrow;
            }, set: function set(e) {
              g.__hidePrevMonthArrow !== e && (c(g.prevMonthNav, "disabled", e), g.__hidePrevMonthArrow = e);
            } }), Object.defineProperty(g, "_hideNextMonthArrow", { get: function get() {
              return g.__hideNextMonthArrow;
            }, set: function set(e) {
              g.__hideNextMonthArrow !== e && (c(g.nextMonthNav, "disabled", e), g.__hideNextMonthArrow = e);
            } }), g.currentYearElement = g.yearElements[0], pe(), g.monthNav)), g.innerContainer = d("div", "flatpickr-innerContainer"), g.config.weekNumbers) {
            var t = function () {
              g.calendarContainer.classList.add("hasWeeks");var e = d("div", "flatpickr-weekwrapper");e.appendChild(d("span", "flatpickr-weekday", g.l10n.weekAbbreviation));var t = d("div", "flatpickr-weeks");return e.appendChild(t), { weekWrapper: e, weekNumbers: t };
            }(),
                n = t.weekWrapper,
                a = t.weekNumbers;g.innerContainer.appendChild(n), g.weekNumbers = a, g.weekWrapper = n;
          }g.rContainer = d("div", "flatpickr-rContainer"), g.rContainer.appendChild(U()), g.daysContainer || (g.daysContainer = d("div", "flatpickr-days"), g.daysContainer.tabIndex = -1), B(), g.rContainer.appendChild(g.daysContainer), g.innerContainer.appendChild(g.rContainer), e.appendChild(g.innerContainer);
        }g.config.enableTime && e.appendChild(function () {
          g.calendarContainer.classList.add("hasTime"), g.config.noCalendar && g.calendarContainer.classList.add("noCalendar"), g.timeContainer = d("div", "flatpickr-time"), g.timeContainer.tabIndex = -1;var e = d("span", "flatpickr-time-separator", ":"),
              t = u("flatpickr-hour");g.hourElement = t.getElementsByTagName("input")[0];var n = u("flatpickr-minute");if (g.minuteElement = n.getElementsByTagName("input")[0], g.hourElement.tabIndex = g.minuteElement.tabIndex = -1, g.hourElement.value = _i(g.latestSelectedDateObj ? g.latestSelectedDateObj.getHours() : g.config.time_24hr ? g.config.defaultHour : function (e) {
            switch (e % 24) {case 0:case 12:
                return 12;default:
                return e % 12;}
          }(g.config.defaultHour)), g.minuteElement.value = _i(g.latestSelectedDateObj ? g.latestSelectedDateObj.getMinutes() : g.config.defaultMinute), g.hourElement.setAttribute("step", g.config.hourIncrement.toString()), g.minuteElement.setAttribute("step", g.config.minuteIncrement.toString()), g.hourElement.setAttribute("min", g.config.time_24hr ? "0" : "1"), g.hourElement.setAttribute("max", g.config.time_24hr ? "23" : "12"), g.minuteElement.setAttribute("min", "0"), g.minuteElement.setAttribute("max", "59"), g.timeContainer.appendChild(t), g.timeContainer.appendChild(e), g.timeContainer.appendChild(n), g.config.time_24hr && g.timeContainer.classList.add("time24hr"), g.config.enableSeconds) {
            g.timeContainer.classList.add("hasSeconds");var a = u("flatpickr-second");g.secondElement = a.getElementsByTagName("input")[0], g.secondElement.value = _i(g.latestSelectedDateObj ? g.latestSelectedDateObj.getSeconds() : g.config.defaultSeconds), g.secondElement.setAttribute("step", g.minuteElement.getAttribute("step")), g.secondElement.setAttribute("min", "0"), g.secondElement.setAttribute("max", "59"), g.timeContainer.appendChild(d("span", "flatpickr-time-separator", ":")), g.timeContainer.appendChild(a);
          }return g.config.time_24hr || (g.amPM = d("span", "flatpickr-am-pm", g.l10n.amPM[o((g.latestSelectedDateObj ? g.hourElement.value : g.config.defaultHour) > 11)]), g.amPM.title = g.l10n.toggleTitle, g.amPM.tabIndex = -1, g.timeContainer.appendChild(g.amPM)), g.timeContainer;
        }()), c(g.calendarContainer, "rangeMode", "range" === g.config.mode), c(g.calendarContainer, "animate", !0 === g.config.animate), c(g.calendarContainer, "multiMonth", g.config.showMonths > 1), g.calendarContainer.appendChild(e);var r = void 0 !== g.config.appendTo && void 0 !== g.config.appendTo.nodeType;if ((g.config.inline || g.config.static) && (g.calendarContainer.classList.add(g.config.inline ? "inline" : "static"), g.config.inline && (!r && g.element.parentNode ? g.element.parentNode.insertBefore(g.calendarContainer, g._input.nextSibling) : void 0 !== g.config.appendTo && g.config.appendTo.appendChild(g.calendarContainer)), g.config.static)) {
          var l = d("div", "flatpickr-wrapper");g.element.parentNode && g.element.parentNode.insertBefore(l, g.element), l.appendChild(g.element), g.altInput && l.appendChild(g.altInput), l.appendChild(g.calendarContainer);
        }g.config.static || g.config.inline || (void 0 !== g.config.appendTo ? g.config.appendTo : window.document.body).appendChild(g.calendarContainer);
      }(), function () {
        if (g.config.wrap && ["open", "close", "toggle", "clear"].forEach(function (e) {
          Array.prototype.forEach.call(g.element.querySelectorAll("[data-" + e + "]"), function (t) {
            return _(t, "click", g[e]);
          });
        }), g.isMobile) !function () {
          var e = g.config.enableTime ? g.config.noCalendar ? "time" : "datetime-local" : "date";g.mobileInput = d("input", g.input.className + " flatpickr-mobile"), g.mobileInput.step = g.input.getAttribute("step") || "any", g.mobileInput.tabIndex = 1, g.mobileInput.type = e, g.mobileInput.disabled = g.input.disabled, g.mobileInput.required = g.input.required, g.mobileInput.placeholder = g.input.placeholder, g.mobileFormatStr = "datetime-local" === e ? "Y-m-d\\TH:i:S" : "date" === e ? "Y-m-d" : "H:i:S", g.selectedDates.length > 0 && (g.mobileInput.defaultValue = g.mobileInput.value = g.formatDate(g.selectedDates[0], g.mobileFormatStr)), g.config.minDate && (g.mobileInput.min = g.formatDate(g.config.minDate, "Y-m-d")), g.config.maxDate && (g.mobileInput.max = g.formatDate(g.config.maxDate, "Y-m-d")), g.input.type = "hidden", void 0 !== g.altInput && (g.altInput.type = "hidden");try {
            g.input.parentNode && g.input.parentNode.insertBefore(g.mobileInput, g.input.nextSibling);
          } catch (e) {}_(g.mobileInput, "change", function (e) {
            g.setDate(e.target.value, !1, g.mobileFormatStr), fe("onChange"), fe("onClose");
          });
        }();else {
          var e = r(te, 50);g._debouncedChange = r(F, C), g.daysContainer && !/iPhone|iPad|iPod/i.test(navigator.userAgent) && _(g.daysContainer, "mouseover", function (e) {
            "range" === g.config.mode && ee(e.target);
          }), _(window.document.body, "keydown", X), g.config.static || _(g._input, "keydown", X), g.config.inline || g.config.static || _(window, "resize", e), void 0 !== window.ontouchstart ? _(window.document, "click", G) : _(window.document, "mousedown", N(G)), _(window.document, "focus", G, { capture: !0 }), !0 === g.config.clickOpens && (_(g._input, "focus", g.open), _(g._input, "mousedown", N(g.open))), void 0 !== g.daysContainer && (_(g.monthNav, "mousedown", N(De)), _(g.monthNav, ["keyup", "increment"], S), _(g.daysContainer, "mousedown", N(ce))), void 0 !== g.timeContainer && void 0 !== g.minuteElement && void 0 !== g.hourElement && (_(g.timeContainer, ["increment"], x), _(g.timeContainer, "blur", x, { capture: !0 }), _(g.timeContainer, "mousedown", N(A)), _([g.hourElement, g.minuteElement], ["focus", "click"], function (e) {
            return e.target.select();
          }), void 0 !== g.secondElement && _(g.secondElement, "focus", function () {
            return g.secondElement && g.secondElement.select();
          }), void 0 !== g.amPM && _(g.amPM, "mousedown", N(function (e) {
            x(e), F();
          })));
        }
      }(), (g.selectedDates.length || g.config.noCalendar) && (g.config.enableTime && k(g.config.noCalendar ? g.latestSelectedDateObj || g.config.minDate : void 0), ve(!1)), M(), g.showTimeInput = g.selectedDates.length > 0 || g.config.noCalendar;var a = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);!g.isMobile && a && oe(), fe("onReady");
    }(), g;
  }function x(e, t) {
    for (var n = Array.prototype.slice.call(e).filter(function (e) {
      return e instanceof HTMLElement;
    }), a = [], i = 0; i < n.length; i++) {
      var o = n[i];try {
        if (null !== o.getAttribute("data-fp-omit")) continue;void 0 !== o._flatpickr && (o._flatpickr.destroy(), o._flatpickr = void 0), o._flatpickr = M(o, t || {}), a.push(o._flatpickr);
      } catch (e) {
        console.error(e);
      }
    }return 1 === a.length ? a[0] : a;
  }"undefined" != typeof HTMLElement && (HTMLCollection.prototype.flatpickr = NodeList.prototype.flatpickr = function (e) {
    return x(this, e);
  }, HTMLElement.prototype.flatpickr = function (e) {
    return x([this], e);
  });var E = function E(e, t) {
    return "string" == typeof e ? x(window.document.querySelectorAll(e), t) : e instanceof Node ? x([e], t) : x(e, t);
  };return E.defaultConfig = n, E.l10ns = { en: _e({}, a), default: _e({}, a) }, E.localize = function (t) {
    E.l10ns.default = _e({}, E.l10ns.default, t);
  }, E.setDefaults = function (t) {
    E.defaultConfig = _e({}, E.defaultConfig, t);
  }, E.parseDate = D({}), E.formatDate = v({}), E.compareDates = w, "undefined" != typeof jQuery && (jQuery.fn.flatpickr = function (e) {
    return x(this, e);
  }), Date.prototype.fp_incr = function (e) {
    return new Date(this.getFullYear(), this.getMonth(), this.getDate() + ("string" == typeof e ? parseInt(e, 10) : e));
  }, "undefined" != typeof window && (window.flatpickr = E), E;
});

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (action, payload, callback, security, ajax_args) {

	var ajax_data = {
		'action': action,
		'security': security,
		'omh_ajax': _.defaultTo(payload, {})
	};

	if (omh.debug) {
		console.log('AJAX Data', ajax_data);
	}

	var ajaxStart = Date.now();

	ajax_args = _.assignIn({
		url: ajax_object.ajax_url,
		type: "POST",
		data: ajax_data,
		cache: false,
		dataType: "json",
		complete: function complete(response, status) {

			if (omh.debug) {
				console.log('AJAX Request Duration', Date.now() - ajaxStart);
				console.log('Raw AJAX Response', response);
			}

			if ('success' == _.get(response, 'responseJSON.status')) {
				jQuery(document).trigger(action, response.responseJSON, response);
			}

			// Handle possible redirect from response
			if (_.get(response, 'responseJSON.redirect')) {
				window.location = _.get(response, 'responseJSON.redirect');
				// Do not continue processing response if redirecting
				return true;
			}

			// Handle possible table refresh from response //dev:generalize
			if (_.get(response, 'responseJSON.refresh_tables')) {
				omh.Tables.refreshTables();
			}

			if (_.isArray(_.get(response, 'responseJSON.messages'))) {
				omh.Notices.processNotices(response.responseJSON);
			}

			if (_.isFunction(callback)) {
				callback(response.responseJSON);
			}
		}
	}, _.defaultTo(ajax_args, {}));

	return jQuery.ajax(ajax_args);
};

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function ($) {

	// Initialize Drawers event handlers
	$(document).on('click', '.onyx-menu-item--parent > a', function (e) {

		$target = $(e.target);

		if ($target.closest('.onyx-drawer').length) {
			e.preventDefault();
			e.stopPropagation();
		}

		$target.closest('.onyx-menu-item--parent').toggleClass('submenu-expanded');
	}).on('click', '.onyx-page-overlay', function (e) {
		$('.onyx-drawer').removeClass('onyx-drawer--open');
		$('.onyx-drawer__toggle').removeClass('is-active');
		$('body,html').removeClass('onyx-body--drawer-open');
	}).on('click', '.onyx-drawer__toggle', function (e) {
		e.preventDefault();
		var $target = $(e.target);

		var $drawer = $target.attr('data-drawer-toggle') ? $('.onyx-drawer--' + $target.data('drawer-toggle')) : $target.closest('.onyx-drawer');

		// Close other drawers
		$('.onyx-drawer:not(.onyx-drawer--no-close)').not($drawer).removeClass('onyx-drawer--open');
		$('body').removeClass('blur-content');
		$drawer.toggleClass('onyx-drawer--open');

		if ($drawer.hasClass('blur-content')) {
			$('body').toggleClass('blur-content', $drawer.hasClass('onyx-drawer--open'));
		}

		// Maybe Auto-focus search input
		if ($('.onyx-drawer--search.onyx-drawer--open').length) {
			$('.onyx-drawer--search.onyx-drawer--open').find('.search__input').focus();
		} else {
			$('.onyx-drawer--search .search__input').blur();
		}

		// Maybe toggle hamburger state
		if ($target.hasClass('hamburger') && !$target.hasClass('no-js-toggle')) {
			$target.toggleClass('is-active', $drawer.hasClass('onyx-drawer--open'));
		}

		// Add body class if drawer is open
		$('body,html').toggleClass('onyx-body--drawer-open', $('.onyx-drawer--open').length);
	});
}(jQuery);

/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

var _formFieldCheckbox = __webpack_require__(6);

var _formFieldCheckbox2 = _interopRequireDefault(_formFieldCheckbox);

var _formFieldFile = __webpack_require__(22);

var _formFieldFile2 = _interopRequireDefault(_formFieldFile);

var _formFieldAjaxSearch = __webpack_require__(23);

var _formFieldAjaxSearch2 = _interopRequireDefault(_formFieldAjaxSearch);

var _formFieldAjaxFile = __webpack_require__(24);

var _formFieldAjaxFile2 = _interopRequireDefault(_formFieldAjaxFile);

var _formFieldSelect = __webpack_require__(25);

var _formFieldSelect2 = _interopRequireDefault(_formFieldSelect);

var _formFieldSelect3 = __webpack_require__(26);

var _formFieldSelect4 = _interopRequireDefault(_formFieldSelect3);

var _formFieldDatepicker = __webpack_require__(27);

var _formFieldDatepicker2 = _interopRequireDefault(_formFieldDatepicker);

var _formFieldAdvanced = __webpack_require__(1);

var _formFieldAdvanced2 = _interopRequireDefault(_formFieldAdvanced);

var _formFieldProductPriceTiers = __webpack_require__(28);

var _formFieldProductPriceTiers2 = _interopRequireDefault(_formFieldProductPriceTiers);

var _formFieldProductPriceTier = __webpack_require__(7);

var _formFieldProductPriceTier2 = _interopRequireDefault(_formFieldProductPriceTier);

var _formFieldGarmentPriceTiers = __webpack_require__(29);

var _formFieldGarmentPriceTiers2 = _interopRequireDefault(_formFieldGarmentPriceTiers);

var _formFieldGarmentPriceTier = __webpack_require__(8);

var _formFieldGarmentPriceTier2 = _interopRequireDefault(_formFieldGarmentPriceTier);

var _formFieldGarment = __webpack_require__(9);

var _formFieldGarment2 = _interopRequireDefault(_formFieldGarment);

var _formFieldGarmentsTable = __webpack_require__(30);

var _formFieldGarmentsTable2 = _interopRequireDefault(_formFieldGarmentsTable);

var _formFieldGarmentSizes = __webpack_require__(31);

var _formFieldGarmentSizes2 = _interopRequireDefault(_formFieldGarmentSizes);

var _formFieldInksoftProductResults = __webpack_require__(32);

var _formFieldInksoftProductResults2 = _interopRequireDefault(_formFieldInksoftProductResults);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import OMHFormFieldInksoftProduct			from './form-fields/form-field-inksoft-product.js';
// import OMHFormFieldInksoftProductStyle		from './form-fields/form-field-inksoft-product-style.js';


// dev:improve - maybe these should be moved so they are only loaded on templates where they are needed?

module.exports = function ($) {

	var fieldTypes = {
		base: _formFieldBase2.default,
		checkbox: _formFieldCheckbox2.default,
		file: _formFieldFile2.default,
		ajaxFile: _formFieldAjaxFile2.default,
		ajaxSearch: _formFieldAjaxSearch2.default,
		select: _formFieldSelect2.default,
		select2: _formFieldSelect4.default,
		datePicker: _formFieldDatepicker2.default,
		advanced: _formFieldAdvanced2.default,
		productPriceTiers: _formFieldProductPriceTiers2.default,
		productPriceTier: _formFieldProductPriceTier2.default,
		garmentPriceTiers: _formFieldGarmentPriceTiers2.default,
		garmentPriceTier: _formFieldGarmentPriceTier2.default,
		garment: _formFieldGarment2.default,
		garmentsTable: _formFieldGarmentsTable2.default,
		garmentSizes: _formFieldGarmentSizes2.default,

		inksoftProductResults: _formFieldInksoftProductResults2.default
		// inksoftProduct 			: OMHFormFieldInksoftProduct,
		// inksoftProductStyle 	: OMHFormFieldInksoftProductStyle,
	};

	function addFieldType(fieldType, fieldTypeConstructor, extendField) {

		// Validate params
		if (_.isString(fieldType) && _.isFunction(fieldTypeConstructor)) {

			// Maybe extend the prototype
			if (_.has(fieldTypes, _.defaultTo(extendField, false))) {

				fieldTypeConstructor.prototype = $.extend({}, extendField.prototype, fieldTypeConstructor.prototype);
			}

			fieldTypes[fieldType] = fieldTypeConstructor;
		}
	};

	$.fn.OMHFormField = function (parent, options) {

		this.each(function () {

			// Determine field constructor; default to base
			var FieldConstructor = _.get(fieldTypes, $(this).data('omh-field-type'), fieldTypes.base);

			new FieldConstructor(this, parent, options);
		});
	};

	return {
		addFieldType: addFieldType
	};
}(jQuery);

/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldFile(input, parent, options) {
	this.initialized = false;

	this.initialized = _formFieldBase2.default.call(this, input, parent, options);

	// Initialize img thumb preview
	this.$fieldImgThumb = $('[data-omh-field-img-thumb="' + this.getName() + '"]');

	this.$input
	// .on('focus', this.fileInputFocus.bind(this))
	.on('change', this.fileInputChange.bind(this)).on('reset', this.fileInputReset.bind(this));

	return OMHFormFieldFile.prototype.completeInit.call(this);
}

OMHFormFieldFile.prototype = $.extend({}, _formFieldBase2.default.prototype, OMHFormFieldFile.prototype, {
	focus: function focus() {
		this.$input.focus().click();
	},
	fileInputFocus: function fileInputFocus(event) {
		this.$input.click();
	},
	getValue: function getValue() {
		return this.$input[0].files[0];
	},
	getFileName: function getFileName() {
		return _.get(this.getValue(), 'name', null);
	},
	setValue: function setValue(value) {
		this.$input.val(value);
		this.setFileLabel();
	},
	setFileLabel: function setFileLabel(label) {
		label = _.defaultTo(label, _.defaultTo(this.getFileName(), 'Choose New File'));
		this.$input.next('label').text(label);
		// Set wrap class to track if new file is set
		this.$fieldStateElems.toggleClass('valid-new-file', _.defaultTo(this.getFileName(), false));
	},
	fileInputChange: function fileInputChange() {
		if (this.getValue()) {
			$('.btn.input-reset-btn[for="' + this.getName() + '"]').show();
		}
		this.setFileLabel();
	},
	fileInputReset: function fileInputReset() {
		// Hide reset buttons (only used for custom file inputs
		this.$fieldResetBtns.hide();
		this.$wrap.removeClass('valid-new-file').find('input.form-file-img-remove').prop('checked', false).trigger('change'); //dev:improve
	},
	processResponseValue: function processResponseValue(response) {
		if ('success' == response.status) {
			this.setValue(this.origValue);
			var newImageSrc = _.get(response, ['data', this.getName(), 'value']);

			if (newImageSrc) {
				this.$fieldStateElems.removeClass('file-no-image').addClass('file-has-image');
				this.$fieldImgThumb.html('<img class="mh-file-input-thumb mb-1" src="' + newImageSrc + '" />');
			} else if (false === newImageSrc) {
				this.$fieldStateElems.removeClass('file-has-image').addClass('file-no-image');
				this.$fieldImgThumb.html('<div class="no-image"></div>');
			}
		}
	}
});

module.exports = OMHFormFieldFile;

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldAjaxSearch(input, parent, options) {

	this.initialized = false;

	var settingsDefaults = {
		ajaxSearchMinLength: 2,
		ajaxSearchDebounce: 1000
	};

	options = $.extend({}, settingsDefaults, _.defaultTo(options, {}));

	this.initialized = _formFieldBase2.default.call(this, input, parent, options);

	var $searchResults = this.$searchResults = this.$wrap.find('.ajax-search-results'),
	    ajaxSearchModel = this.ajaxSearchModel = this.$wrap.data('omh-ajax-search-model'),
	    inputSearchTimeout = this.inputSearchTimeout,
	    inputSearchNonce = this.inputSearchNonce = $('#omh_ajax_search_nonce').val();

	// Initialize event handler for search results items
	this.$wrap.on('click', '.ajax-search-result', this.selectResult.bind(this)).on('clearValue', this.clearSearchInput.bind(this));

	this.$searchInput.on('input', this.ajaxSearchDebounce.bind(this)).on('focus', this.focusSearchInput.bind(this));

	return OMHFormFieldAjaxSearch.prototype.completeInit.call(this);
}

OMHFormFieldAjaxSearch.prototype = $.extend({}, _formFieldBase2.default.prototype, OMHFormFieldAjaxSearch.prototype, {
	varsInit: function varsInit() {

		this.$searchInput = this.$wrap.find('input.ajax-input-label');

		this.$fieldStateElems = this.$fieldStateElems.add(this.$searchInput);
	},
	focusSearchInput: function focusSearchInput(event) {
		this.$searchInput.select();
	},
	clearSearchInput: function clearSearchInput(event) {
		this.$searchInput.val(null);
	},
	ajaxSearchDebounce: function ajaxSearchDebounce(event) {
		clearTimeout(this.inputSearchTimeout);

		this.$searchResults.empty();

		this.setValue(null);

		var searchTerm = event.target.value;

		if (searchTerm.length <= this.settings.ajaxSearchMinLength) {
			return;
		}

		this.inputSearchTimeout = setTimeout(this.getAjaxSearchResults.bind(this), this.settings.ajaxSearchDebounce);
	},
	getAjaxSearchResults: function getAjaxSearchResults() {
		// this.$searchResults.empty();

		this.$wrap.addClass('ajax-loading');

		omh.ajax('omh_ajax_search', {
			omh_model: this.ajaxSearchModel,
			search_term: this.$searchInput.val()
		}, this.ajaxSearchCallback.bind(this), this.inputSearchNonce);
	},
	ajaxSearchCallback: function ajaxSearchCallback(responseData) {

		this.$searchResults.html(responseData.data.html_results);

		this.$wrap.removeClass('ajax-loading');
	},
	selectResult: function selectResult(event) {

		clearTimeout(this.inputSearchTimeout);

		this.$searchResults.empty();

		var $target = $(event.target);

		this.setValue($target.data('result-value'));

		this.$searchInput.val($target.data('result-label'));

		this.$input.trigger('change');
	},
	processResponseValue: function processResponseValue(response) {

		this.setValue(_.get(response, ['data', this.getName(), 'value']));

		this.origValue = this.getValue();

		// this.setValue( _.get( response, ['data', this.getName(), 'value'] ));
	}
});

module.exports = OMHFormFieldAjaxSearch;

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldAjaxFile(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {});

	this.initialized = _formFieldBase2.default.call(this, input, parent, options);

	var self = this;
	this.$wrap.fileupload({
		filesContainer: this.$wrap.find('.mh-file-upload-box__file'),
		dropZone: this.$wrap.find('.mh-file-upload-box__file'),
		url: ajax_object.ajax_url,
		dataType: 'json',
		add: function add(e, data) {
			e.preventDefault();
			self.uploadFile(data.files);
		}
	});

	return OMHFormFieldAjaxFile.prototype.completeInit.call(this);
}

OMHFormFieldAjaxFile.prototype = _.create(_formFieldBase2.default.prototype, {
	varsInit: function varsInit() {
		this.$uploading = this.$wrap.find('.mh-file-upload-box__uploading');
		this.$success = this.$wrap.find('.mh-file-upload-box__success');
		this.$error = this.$wrap.find('.mh-file-upload-box__error');
	},
	getValue: function getValue() {
		return this.currValue;
	},
	isOrigValue: function isOrigValue() {
		return _.isEqual(this.origValue, this.currValue);
	},
	setValue: function setValue(value, setOrigValue) {

		setOrigValue = _.defaultTo(setOrigValue, false);

		// this.$input.val(value);
		this.currValue = value;

		if (_.get(value, 'thumbnail_src')) {
			this.$success.html('<img src="' + value.thumbnail_src + '">').show();
		} else {
			this.$success.html('').hide();
		}

		if (setOrigValue) {
			this.origValue = value;
		}
		return value;
	},
	uploadFile: function uploadFile(files) {
		var formData = new FormData();

		_.forEach(files, function (file, name) {
			formData.append('temporaryFile' + name, file);
		});

		formData.set('action', 'omh_temporary_file_upload');
		formData.set('security', '');

		this.$uploading.show();

		omh.ajax('omh_temporary_file_upload', {}, this.processTempUploadResponse.bind(this), '', {
			data: formData,
			processData: false,
			contentType: false
			// progress : function(param) {
			// 	console.log( 'PROGRESS', param);
			// },
			// beforeSend 	: function(xhr) {
			// 	console.log(x)
			// 	xhr.progress((evt) => {
			// 		var percentComplete = Math.round((evt.loaded * 100) / evt.total);
			// 		console.log('UPLOADING: ' + percentComplete);
			// 	});

			// }
		});
	},
	processTempUploadResponse: function processTempUploadResponse(response) {
		var self = this,
		    $uploadingBox = this.$uploading,
		    $successBox = this.$success,
		    $errorBox = this.$error,
		    uploadedFile = response.data.temporaryFile0;

		$uploadingBox.hide();
		$errorBox.hide();
		$successBox.hide();

		uploadedFile.id = uploadedFile.id ? _.toInteger(uploadedFile.id) : uploadedFile.id;

		if ('success' === response.status) {

			// $successBox.show();

			// $successBox.html('<img src="' + uploadedFile.thumbnail_src + '">');
			this.setValue(uploadedFile);

			this.$wrap.trigger('input').trigger({ type: 'input', namespace: this.getParentEventNamespace() }).trigger('change').trigger({ type: 'change', namespace: this.getParentEventNamespace() });
		} else {

			$errorBox.show();
		}
	}
});

module.exports = OMHFormFieldAjaxFile;

/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldSelect(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		select2: {
			width: '100%'
		}
	});

	this.initialized = _formFieldBase2.default.call(this, input, parent, options);

	this.isMultiple = _.get(this.settings.select2, 'multiple', false); // dev:improve

	this.$input.on('select2:select', this._onSelect2Select.bind(this)).on('change.select2', this._onChange.bind(this)).on('select2:open', this._onFocus.bind(this));

	this.$input.select2(this.settings.select2);
	// Maybe preset value
	if (_.get(this.settings, 'presetValue', null)) {
		this.setValue(this.settings.presetValue, true);
	}

	return OMHFormFieldSelect.prototype.completeInit.call(this);
}

OMHFormFieldSelect.prototype = $.extend({}, _formFieldBase2.default.prototype, OMHFormFieldSelect.prototype, {
	_onSelect2Select: function _onSelect2Select(event) {
		if (_.get(this.settings.select2, 'multiple', false)) {

			var currValue = _.toArray(this.getValue());

			currValue.push(event.params.data.id);

			this.setValue(currValue);
		} else {

			this.setValue(event.params.data.id);
		}

		this.$input.trigger('change');
	},
	processResponseValue: function processResponseValue(response) {

		var value = _.get(response, ['data', this.getName(), 'value']),
		    options = _.get(response, ['data', this.getName(), 'options'], false);

		if (options !== false) {
			this.setOptions(options);
		}

		this.setValue(value, true);

		this.origValue = this.getValue();

		this.resetState();
	},
	isOrigValue: function isOrigValue() {
		if (!this.isMultiple) {
			return _.isEqual(this.getValue(), this.origValue);
		} else {
			return _.isEqual(_.sortBy(this.getValue()), _.sortBy(this.origValue));
		}
	},
	setOptions: function setOptions() {
		var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

		console.log('setting options', options);
	}
});

module.exports = OMHFormFieldSelect;

/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldSelect2(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		select2: {
			ajax: {
				delay: 250,
				transport: this.select2Transport.bind(this),
				processResults: this.select2ProcessResults
			},
			width: '100%',
			minimumInputLength: 3
		}
	});

	this.initialized = _formFieldBase2.default.call(this, input, parent, options);

	var ajaxSearchModel = this.ajaxSearchModel = this.$wrap.data('omh-ajax-search-model'),
	    inputSearchNonce = this.inputSearchNonce = $('#omh_ajax_search_nonce').val();

	this.isMultiple = _.get(this.settings.select2, 'multiple', false); // dev:improve

	this.$input.on('select2:select', this._onSelect2Select.bind(this)).on('change.select2', this._onChange.bind(this)).on('select2:open', this._onFocus.bind(this));

	this.$input.select2(this.settings.select2);
	// Maybe preset value
	if (_.get(this.settings, 'presetValue', null)) {
		this.setValue(this.settings.presetValue, true);
	}

	return OMHFormFieldSelect2.prototype.completeInit.call(this);
}

OMHFormFieldSelect2.prototype = $.extend({}, _formFieldBase2.default.prototype, OMHFormFieldSelect2.prototype, {
	_onSelect2Select: function _onSelect2Select(event) {
		if (_.get(this.settings.select2, 'multiple', false)) {

			var currValue = _.toArray(this.getValue());

			currValue.push(event.params.data.id);

			this.setValue(currValue);
		} else {

			this.setValue(event.params.data.id);
		}

		this.$input.trigger('change');
	},
	// setValue: function(value, setOrigValue) {
	// 	setOrigValue = _.defaultTo( setOrigValue, false);

	// 	// Add <option> if not exists
	// 	if( $input.find("option[value='" + value + "']").length ) {
	// 	    $input.val(value);
	// 	} else { 
	// 	    // Create a DOM Option and pre-select by default
	// 	    var newOption = new Option(data.text, data.id, true, true);
	// 	    // Append it to the select
	// 	    $('#mySelect2').append(newOption).trigger('change');
	// 	} 
	// 	this.$input.val(value);
	// 	this.currValue = value;
	// 	if( setOrigValue ) {
	// 		this.origValue = value;
	// 	}
	// 	return value;
	// },
	clearValue: function clearValue(clearOrig, reset) {
		clearOrig = _.defaultTo(Boolean(clearOrig), true);
		reset = _.defaultTo(Boolean(reset), true);

		this.$input.empty();
		_formFieldBase2.default.prototype.clearValue.call(this, clearOrig, reset);
		this.$input.select2(this.settings.select2);
	},
	processResponseValue: function processResponseValue(response) {

		// Check if multiple and do other stuff here? This is an issue with categories mainly

		var value = _.get(response, ['data', this.getName(), 'value'], false);

		if (!value) {
			value = $('select.omh-field-input[name="product_categories"]').find('option:contains(Uncategorized)').val();
		}

		// Add value if not exists
		if (value && !this.$input.children("option[value='" + _.get(value, 'id', value) + "']").length) {

			// var valueLabel = _.get( response, ['data', this.getName(), 'value_label'], value );
			var newOption = new Option(_.get(value, 'text', value), _.get(value, 'id', value), true, true);

			this.$input.append(newOption).trigger('change');
		}

		this.setValue(_.get(value, 'id', value), true);

		this.origValue = this.getValue();

		this.resetState();
	},
	select2ProcessResults: function select2ProcessResults(results) {
		return {
			results: results.data.results
		};
	},
	isOrigValue: function isOrigValue() {
		if (!this.isMultiple) {
			return _.isEqual(this.getValue(), this.origValue);
		} else {
			return _.isEqual(_.sortBy(this.getValue()), _.sortBy(this.origValue));
		}
	},
	select2Transport: function select2Transport(params, success, failure) {

		return omh.ajax('omh_ajax_search', {
			omh_model: this.ajaxSearchModel,
			search_term: params.data.term
		}, success, this.inputSearchNonce);
	}
});

module.exports = OMHFormFieldSelect2;

/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldBase = __webpack_require__(0);

var _formFieldBase2 = _interopRequireDefault(_formFieldBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldDatePicker(input, parent, options) {
	this.initialized = false;

	options = _.defaultsDeep(options, {
		presetValue: false,
		flatpickr: {
			enableTime: true,
			minDate: $(input).data('allowPast') === true ? false : new Date(),
			dateFormat: 'Y-m-d H:i:s',
			altInput: true,
			altFormat: 'F d, Y h:i K'
		}
	});

	return _formFieldBase2.default.call(this, input, parent, options);
}

OMHFormFieldDatePicker.prototype = $.extend({}, _formFieldBase2.default.prototype, OMHFormFieldDatePicker.prototype, {
	varsInit: function varsInit() {
		// Check input elem
		if (this.$input.attr('data-flatpickr-options')) {
			var flatpickrOverrides = JSON.parse(atob(this.$input.attr('data-flatpickr-options')));

			_.merge(this.settings.flatpickr, flatpickrOverrides);
		}

		this.datePicker = this.$input.flatpickr(this.settings.flatpickr);
	}
});

module.exports = OMHFormFieldDatePicker;

/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldAdvanced = __webpack_require__(1);

var _formFieldAdvanced2 = _interopRequireDefault(_formFieldAdvanced);

var _formFieldProductPriceTier = __webpack_require__(7);

var _formFieldProductPriceTier2 = _interopRequireDefault(_formFieldProductPriceTier);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldProductPriceTiers(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		subfieldsIsArray: true,
		tierIncrement: 1,
		presetValue: _.get(window, 'omhProductData.priceTiers', []),
		tiersType: 'product'
	});

	this.initialized = _formFieldAdvanced2.default.call(this, input, parent, options);

	this.$addPriceTierBtn = this.$wrap.find('.price-tier__add-btn').eq(0);

	this.$addPriceTierBtn.click(this.addNewPriceTier.bind(this));

	return OMHFormFieldProductPriceTiers.prototype.completeInit.call(this);
}

OMHFormFieldProductPriceTiers.prototype = _.create(_formFieldAdvanced2.default.prototype, {
	varsInit: function varsInit() {
		this.$headerRow = this.$wrap.find('.price-tiers__header-row');
		this.$valuesRow = this.$wrap.find('.price-tiers__values-row');

		this.templates = {
			tierHeader: _.template(['<th class="text-center price-tier__number" data-price-tier="<%= tierIndex %>">', '<%= tierIndex %>', '<span class="price-tier__remove">&#215;</span>', '</th>'].join("\n")),
			tierValue: _.template(['<td class="text-center cell--input omh-field-wrap" >', ['<input', 'class="form-control omh-table-input price-tier__quantity omh-field-input"', 'data-omh-field-type="priceTier"', 'data-omh-subfield-of="<%= parentField.namespace %>"', 'data-price-tier="<%= tierIndex %>"', 'name="price_tiers[<%= tierIndex %>]"', 'type="number"', 'min="0"', 'step="<%= tierIncrement %>"', 'value="<%= tierValue %>"', '/>'].join(' '), '</td>'].join("\n"), { imports: { parentField: this, tierIncrement: this.settings.tierIncrement } })
		};
	},
	initChildFields: function initChildFields() {

		var self = this;
		_.map(this.settings.presetValue, function (tierValue, indexZero) {

			var priceTierOptions = {
				tierIndex: indexZero + 1,
				tierValue: tierValue,
				tiersType: self.settings.tiersType
			};
			new _formFieldProductPriceTier2.default('', self, priceTierOptions);
		});

		this.recalcTiers();
	},
	recalcTiers: function recalcTiers() {

		var fields = this.fields,
		    tierIncrement = this.settings.tierIncrement,
		    tierIndex = 0,
		    tierValueLast = 0,
		    nextPriceTier = null;

		_.forEach(fields, function (priceTier, indexZero) {

			nextPriceTier = _.get(fields, indexZero + tierIncrement, null);

			var tierValueMax = nextPriceTier ? nextPriceTier.getValue() - tierIncrement : null;
			var tierValueMin = tierValueLast + tierIncrement;

			// Double check to force min & max
			if (priceTier.getValue() < tierValueMin) {
				priceTier.setValue(tierValueMin);
			}

			if (tierValueMax && priceTier.getValue() > tierValueMax) {
				priceTier.setValue(tierValueMax);
			}

			priceTier.$input.attr({
				min: tierValueMin,
				max: tierValueMax
			});

			tierValueLast = priceTier.getValue();
		});
	},
	addNewPriceTier: function addNewPriceTier(event, tierValue, quietly) {
		quietly = _.defaultTo(quietly, false);

		tierValue = _.defaultTo(tierValue, _.last(this.fields).getValue() + 1);

		var priceTierOptions = {
			tierIndex: this.fields.length + 1,
			tierValue: tierValue,
			tiersType: this.settings.tiersType
		};

		new _formFieldProductPriceTier2.default('', this, priceTierOptions);

		if (!quietly) {
			this.recalcTiers();
			this.$input.trigger({ type: 'change', namespace: this.getParentEventNamespace() });
		}
	},
	deletePriceTier: function deletePriceTier(priceTier, quietly) {
		quietly = _.defaultTo(quietly, false);

		priceTier.$wrap.remove();
		priceTier.$tierHeader.remove();

		$(document).trigger('productPriceTierDeleted', [priceTier]);

		_.remove(this.fields, priceTier);

		if (!quietly) {
			this.recalcTiers();
			this.$input.trigger({ type: 'change', namespace: this.getParentEventNamespace() });
		}
	},
	deleteTierByIndex: function deleteTierByIndex(tierIndex, quietly) {
		this.deletePriceTier(this.fields[tierIndex - 1], quietly);
	},
	reset: function reset(quietly) {

		if (this.initialized && this.origValue.length != this.fields.length) {

			this.initialized = false;

			// Clear the Price Tiers from the DOM
			this.$headerRow.children('[data-price-tier]').add(this.$valuesRow.children('.omh-field-wrap')).remove();

			// Clear all Garments' Price Tiers
			this.parent.fields.product_garments.$wrap.find('.garment__price-tiers').find('th.price-tier__number, td.omh-field-wrap').remove();

			_.forEach(this.parent.fields.product_garments.fields, function (productGarment) {
				productGarment.fields.pricing.fields = [];
			});

			this.fields = [];

			// this.setValue(this.origValue);

			this.initChildFields();

			this.initialized = true;

			// this.setValue(this.origValue, true);
		}

		_formFieldAdvanced2.default.prototype.reset.call(this);

		// quietly = _.defaultTo( quietly, false );


		// if( this.initialized ) {
		// 	// Reset the input's value
		// 	this.setValue(this.origValue);

		// 	// Add or remove priceTiers if the current count of them does not 
		// 	// match the original count
		// 	while( this.origValue.length != this.fields.length ) {

		// 		if( this.origValue.length < this.fields.length ) {

		// 			this.addNewPriceTier( this.origValue[ this.fields.length ], this.origValue[ this.fields.length ], true );
		// 		}
		// 		else {

		// 			this.deletePriceTier( this.fields[this.fields.length - 1], true );
		// 		}
		// 	}	
		// }


		// OMHFormFieldAdvanced.prototype.reset.call( this, true );

	}
});

module.exports = OMHFormFieldProductPriceTiers;

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldAdvanced = __webpack_require__(1);

var _formFieldAdvanced2 = _interopRequireDefault(_formFieldAdvanced);

var _formFieldGarmentPriceTier = __webpack_require__(8);

var _formFieldGarmentPriceTier2 = _interopRequireDefault(_formFieldGarmentPriceTier);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

// import OMHFormFieldProductPriceTiers 	from './form-field-product-price-tiers.js';


// import OMHFormField 			from './form-field-base.js';

function OMHFormFieldGarmentPriceTiers(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		productPriceTiers: parent.settings.productPriceTiers, // Reference to productPriceTiers field
		validationClasses: false,
		subfieldsIsArray: true,
		tierValueIncrement: 0,
		tierIncrement: 0.01,
		namespace: 'pricing[' + parent.settings.garmentStyleId + ']',
		tiersType: 'garment',
		autoInitFields: false // Price Tiers are initted by the productPriceTiers advanced field
	});

	this.initialized = _formFieldAdvanced2.default.call(this, input, parent, options);

	$(document).on('productPriceTierAdded', this.addGarmentPriceTier.bind(this)).on('productPriceTierUpdated', this.updateGarmentPriceTier.bind(this)).on('productPriceTierDeleted', this.deleteGarmentPriceTier.bind(this));

	return _formFieldAdvanced2.default.prototype.completeInit.call(this);
}

OMHFormFieldGarmentPriceTiers.prototype = _.create(_formFieldAdvanced2.default.prototype, {
	varsInit: function varsInit() {
		this.$headerRow = this.$wrap.find('.price-tiers__header-row');
		this.$valuesRow = this.$wrap.find('.price-tiers__values-row');

		this.templates = {
			tierHeader: _.template(['<th class="text-center price-tier__number" data-price-tier="<%= tierIndex %>">', '<%= tierIndex %>', '<span class="product-price-tier-qty"><%= productPriceTierQty %></span>', '</th>'].join("\n")),
			tierValue: _.template(['<td class="text-center cell--input omh-field-wrap" >', ['<input', 'class="form-control omh-table-input price-tier__quantity omh-field-input"', 'data-omh-field-type="priceTier"', 'data-omh-subfield-of="<%= parentField.namespace %>"', 'data-price-tier="<%= tierIndex %>"', 'name="price_tiers[<%= tierIndex %>]"', 'type="number"', 'min="0"', 'step="<%= tierIncrement %>"', 'value="<%= tierValue %>"', '/>'].join(' '), '</td>'].join("\n"), { imports: { parentField: this, tierIncrement: this.settings.tierIncrement } })
		};

		// OMHFormFieldProductPriceTiers.prototype.varsInit.call( this );

		// this.templates.tierHeader = _.template([
		// 			'<th class="text-center price-tier__number" data-price-tier="<%= tierIndex %>">',
		// 				'<%= tierIndex %>',
		// 				'<span class="product-price-tier-qty"><%= productPriceTierQty %></span>',
		// 			'</th>'
		// 		].join("\n")
		// 	);

	},
	getProductPriceTiers: function getProductPriceTiers() {
		return this.parent.getProductPriceTiers();
	},
	addGarmentPriceTier: function addGarmentPriceTier(event, priceTier, tierSettings) {

		var productPriceTiersInitialized = _.get(this.getProductPriceTiers(), 'initialized', false);
		var tierIndexZero = tierSettings.tierIndex - 1;

		// If the price tiers have not been intialized, then the new tier value
		// should be the highest tier value, otherwise it should be the tier's
		// preset value because this means we are still initializing the screen
		var garmentTierValue = productPriceTiersInitialized ? _.last(this.getValue()) : _.defaultTo(this.settings.presetValue[tierIndexZero], 20);

		var garmentPriceTierOptions = {
			productPriceTierQty: priceTier.getValue(),
			tierIndex: tierSettings.tierIndex,
			tierValue: garmentTierValue,
			tiersType: 'garment'
		};

		new _formFieldGarmentPriceTier2.default('', this, garmentPriceTierOptions);
	},
	updateGarmentPriceTier: function updateGarmentPriceTier(event, priceTier) {
		var tierIndexZero = priceTier.settings.tierIndex - 1;

		this.fields[tierIndexZero].$tierMinQty.html(priceTier.getValue());
	},
	deleteGarmentPriceTier: function deleteGarmentPriceTier(event, productPriceTier) {

		var garmentPriceTier = this.fields[productPriceTier.settings.tierIndex - 1];

		garmentPriceTier.$wrap.remove();
		garmentPriceTier.$tierHeader.remove();

		_.remove(this.fields, garmentPriceTier);

		// this.$input.trigger({type:'change', namespace: this.getParentEventNamespace()});
	},
	initChildFields: function initChildFields() {

		var productPriceTiers = this.getProductPriceTiers();

		if (productPriceTiers) {

			var self = this;
			_.forEach(productPriceTiers.fields, function (priceTier, indexZero) {
				self.addGarmentPriceTier({}, priceTier, priceTier.settings);
			});
		}
		// var productPriceTiersInitialized = _.get( this.getProductPriceTiers(), 'initialized', false);

		// console.log( 'GarmentPriceTiers.initChildFields', this.settings );
		// var self = this;
		// _.map( this.settings.presetValue, function(tierValue, indexZero) {

		// 	var tierQuantity = _.get( self.settings.productPriceTiers, 'fields[indexZero].getValue()', null);

		// 	var priceTierOptions = {
		// 		productPriceTierQty: tierQuantity,
		// 		tierIndex: indexZero + 1,
		// 		tierValue: tierValue,
		// 		tiersType: self.settings.tiersType
		// 	};
		// 	new OMHFormFieldGarmentPriceTier( '', self, priceTierOptions );
		// });

		// this.recalcTiers();
	},
	deletePriceTier: function deletePriceTier(priceTier, quietly) {
		quietly = _.defaultTo(quietly, false);

		priceTier.$wrap.remove();
		priceTier.$tierHeader.remove();

		_.remove(this.fields, priceTier);

		if (!quietly) {
			this.recalcTiers();
			this.$input.trigger({ type: 'change', namespace: this.getParentEventNamespace() });
		}
	},
	// Override the productPriceTiers function so it does nothing
	recalcTiers: function recalcTiers() {},
	reset: function reset() {}
	// reset: function( quietly ) {

	// 	if( this.initialized && this.origValue.length != this.fields.length ) {
	// 		// Clear the Price Tiers from the DOM
	// 		this.$headerRow.children('[data-price-tier]')
	// 			.add( this.$valuesRow.children('.omh-field-input') )
	// 			.remove();

	// 		this.fields = [];

	// 		this.setValue(this.origValue);

	// 		this.initChildFields();
	// 	}

	// 	OMHFormFieldAdvanced.prototype.reset.call( this );

	// 	// quietly = _.defaultTo( quietly, false );


	// 	// if( this.initialized ) {
	// 	// 	// Reset the input's value
	// 	// 	this.setValue(this.origValue);

	// 	// 	// Add or remove priceTiers if the current count of them does not 
	// 	// 	// match the original count
	// 	// 	while( this.origValue.length != this.fields.length ) {

	// 	// 		if( this.origValue.length < this.fields.length ) {

	// 	// 			this.addNewPriceTier( this.origValue[ this.fields.length ], this.origValue[ this.fields.length ], true );
	// 	// 		}
	// 	// 		else {

	// 	// 			this.deletePriceTier( this.fields[this.fields.length - 1], true );
	// 	// 		}
	// 	// 	}	
	// 	// }


	// 	// OMHFormFieldAdvanced.prototype.reset.call( this, true );


	// }
});

module.exports = OMHFormFieldGarmentPriceTiers;

/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldAdvanced = __webpack_require__(1);

var _formFieldAdvanced2 = _interopRequireDefault(_formFieldAdvanced);

var _formFieldGarment = __webpack_require__(9);

var _formFieldGarment2 = _interopRequireDefault(_formFieldGarment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldGarmentsTable(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		rawGarmentsData: _.get(window, 'omhProductData.garmentData', {}),
		presetValue: _.get(window, 'omhProductData.garmentData', {}),
		pruneFieldsOnSetValue: true
	});

	this.initialized = _formFieldAdvanced2.default.call(this, input, parent, options);

	this.productPriceTiers = _.get(this.parent, 'fields.product_price_tiers', null);

	this.origValue = this.settings.presetValue;

	// this.$addPriceTierBtn = this.$wrap.find( '.price-tier__add-btn' ).eq(0);

	// this.$addPriceTierBtn.click( this.addNewPriceTier.bind(this) );

	return OMHFormFieldGarmentsTable.prototype.completeInit.call(this);
	// _.invoke( this, 'completeInit' );
}

OMHFormFieldGarmentsTable.prototype = _.create(_formFieldAdvanced2.default.prototype, {
	getProductPriceTiers: function getProductPriceTiers() {
		return _.get(this.parent, 'fields.product_price_tiers', null);
	},
	varsInit: function varsInit() {
		this.$tableBody = this.$wrap.find('.omh-table-section-results');
		this.$garmentTemplate = this.$tableBody.children('.garment__template-row');

		// Prepare presetValue
		var self = this;
		_.forEach(this.settings.presetValue, function (presetGarment, presetGarmentId) {
			_.unset(self.settings.presetValue[presetGarmentId], 'garment');
			self.settings.presetValue[presetGarmentId].featured = 'false' == presetGarment.featured ? false : Boolean(presetGarment.featured);
			_.map(self.settings.presetValue[presetGarmentId].pricing, parseFloat);
		});

		this.templates = {
			garmentRow: _.template(this.$garmentTemplate.prop('outerHTML'), { imports: { parentField: this } })
		};
	},
	getValue: function getValue() {
		var garmentMeta = {};

		_.forEach(this.fields, function (garment, garmentStyleId) {
			if (!garment.shouldDelete()) {
				garmentMeta[garmentStyleId] = garment.getValue();
			}
		});

		return garmentMeta;
	},
	isOrigValue: function isOrigValue() {
		// Because of the added garment__removed field, 
		// just check if each of the garment rows is OrigValue
		return _.every(_.invokeMap(this.fields, 'isOrigValue'));
	},
	initChildFields: function initChildFields() {

		var self = this;
		_.forEach(this.settings.rawGarmentsData, function (garmentData, garmentStyleId) {

			// garmentData.create = false;
			// garmentData.delete = false;
			// garmentData.featured = 'false' == garmentData.featured ? false : Boolean( garmentData.featured );

			var garmentOptions = {
				garmentStyleId: garmentData.garment_style_id,
				productPriceTiers: self.productPriceTiers,
				presetValue: self.settings.presetValue[garmentStyleId],
				garmentStyle: garmentData.garment
			};

			// Remove garment style global data from presetValue
			_.unset(garmentOptions.presetValue, 'garment');

			new _formFieldGarment2.default('', self, garmentOptions);
		});

		// this.recalcTiers();
	},
	addNewGarment: function addNewGarment(garmentStyle) {

		// Check that this garment is not already on the product
		if (_.has(this.getValue(), garmentStyle.id)) {

			omh.Notices.setNotice({
				type: 'warning',
				content: 'This product already has Garment ID ' + garmentStyle.id,
				key: 'duplicate_product_garment_style'
			});
		} else {
			var array = [];
			if (this.getProductPriceTiers()) {
				array = Array(this.getProductPriceTiers().fields.length);
			}

			var garmentOptions = {
				garmentStyleId: garmentStyle.id,
				garmentStyle: garmentStyle.garment,
				productPriceTiers: this.productPriceTiers,
				presetValue: {
					garmentStyleId: garmentStyle.id,
					image_front: null,
					image_back: null,
					image_proof: null,
					featured: false,
					pricing: _.fill(array, 20.00),
					base_price: _.get(garmentStyle, 'style_base_cost', 10),
					retail_price: _.get(garmentStyle, 'style_base_cost', 10),
					sizes: {}
				}
			};

			garmentOptions.garmentStyle.id = garmentStyle.id;

			// Initialize sizes
			garmentOptions.garmentStyle.sizes = garmentOptions.garmentStyle.sizes.split(',');

			garmentOptions.presetValue.sizes = _.zipObject(garmentOptions.garmentStyle.sizes, false);

			var newGarment = new _formFieldGarment2.default('', this, garmentOptions);

			if (newGarment.fields.pricing) {
				newGarment.fields.pricing.initChildFields();
			}

			this.$input.trigger({ type: 'change', namespace: this.getParentEventNamespace() });
		}
	}

	// setValue: function(value, setOrigValue) {
	// 	setOrigValue = _.defaultTo( setOrigValue, false);

	// 	// var self = this;
	// 	// _.forEach( this.currValue, function(garment, garmentId) {
	// 	// 	if( _.get( garment.fields, 'delete' ) && !_.has( value, garmentId ) ) {
	// 	// 		garment.$wrap.remove();
	// 	// 		_.unset( self.currValue, garmentId );
	// 	// 	}
	// 	// });

	// 	OMHFormFieldAdvanced.prototype.setValue.call( this, value, setOrigValue );


	// },
	// addProductPriceTier: function(priceTier, quietly) {


	// }
	// completeInit: function() {
	// 	this.initialized = true;


	// 	return false;
	// }
});

module.exports = OMHFormFieldGarmentsTable;

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldAdvanced = __webpack_require__(1);

var _formFieldAdvanced2 = _interopRequireDefault(_formFieldAdvanced);

var _formFieldCheckbox = __webpack_require__(6);

var _formFieldCheckbox2 = _interopRequireDefault(_formFieldCheckbox);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

function OMHFormFieldGarmentSizes(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		garmentStyle: null,
		// validationClasses: true,
		subfieldsIsArray: false,
		presetValue: _.get(parent.settings, 'presetValue.sizes', {}),
		namespace: 'sizes[' + parent.settings.garmentStyleId + ']'
	});

	this.initialized = _formFieldAdvanced2.default.call(this, input, parent, options);

	// this.initGarmentSizes();

	return OMHFormFieldGarmentSizes.prototype.completeInit.call(this);
}

OMHFormFieldGarmentSizes.prototype = _.create(_formFieldAdvanced2.default.prototype, {
	varsInit: function varsInit() {

		this.templates = {
			garmentSize: _.template(['<div class="form-check omh-field-wrap">', '<label for="<%= inputId %>" class="form-check-label"><%= sizeLabel %>', '<input type="checkbox" data-omh-field-type="checkbox" class="form-check-input field--template sizes-garment__sizes <%= disabled %>" id="<%= inputId %>" name="<%= sizeLabel %>" <%= disabled %> data-omh-subfield-of="<%= parentNamespace %>">', '</label>', '</div>'].join("\n"))
		};

		this.$invalidFeedback = _.defaultTo(this.$invalidFeedback, this.parent.$wrap.find('.invalid-feedback').eq(0));

		this.initGarmentSizes();
	},
	initGarmentSizes: function initGarmentSizes() {

		var self = this,
		    wrapDisabled = this.$wrap.hasClass('disabled');

		_.forEach(this.settings.presetValue, function (sizeEnabled, sizeLabel) {

			var garmentSizeOptions = {
				sizeLabel: sizeLabel,
				inputId: 'garment-' + self.parent.settings.garmentStyleId + '-' + sizeLabel,
				// checked: sizeEnabled ? 'checked' : '',
				presetValue: sizeEnabled,
				disabled: wrapDisabled ? 'disabled' : '',
				parentNamespace: self.settings.namespace
			};

			// Only show enabled sizes if the wrap is disabled
			if (sizeEnabled || !wrapDisabled) {
				self.$wrap.append(_.template(self.templates.garmentSize(garmentSizeOptions)));
			}
		});

		// this.recalcTiers();
	},
	isValid: function isValid() {

		return !_.isEmpty(_.filter(this.getValue())) || !_.get(this.parent, 'fields.garment__removed.currValue');
	},
	validate: function validate(addClass) {
		if (_.defaultTo(addClass, true)) {
			this.$fieldStateElems.addClass('was-validated');
		}

		var isValid = this.isValid();

		this.setFeedbackMsg('You must enable at least one size.', 'invalid', 'default', !isValid);

		// if( !this.isValid() ) {
		// 	this.setFeed
		// }
		// var validationMessage = this.inputElem.validationMessage;

		// var isValid = this.isValid();

		// // Check for custom validation messages //dev:improve
		// if (!isValid && this.$input.attr('pattern') && this.$input.data('invalid-msg-pattern')) {
		// 	validationMessage = this.$input.data('invalid-msg-pattern');
		// }

		// // Maybe toggle state classes
		// if (this.settings.validationClasses) {
		// 	this.$fieldStateElems.toggleClass('is-valid', isValid)
		// 		.toggleClass('is-invalid', !isValid);
		// }


		// // Set (enable/disable) default feedback message //dev:improve
		// this.setFeedbackMsg(validationMessage, 'invalid', 'default', !isValid);
	}
});

module.exports = OMHFormFieldGarmentSizes;

/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _formFieldAdvanced = __webpack_require__(1);

var _formFieldAdvanced2 = _interopRequireDefault(_formFieldAdvanced);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var $ = jQuery;

// import OMHFormFieldInksoftProduct 		from './form-field-inksoft-product.js';
// import OMHFormFieldInksoftProductStyle 	from './form-field-inksoft-product-style.js';

function OMHFormFieldInksoftProductResults(input, parent, options) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		// validationClasses		: false,
		inksoftProduct: null
	});

	this.initialized = _formFieldAdvanced2.default.call(this, input, parent, options);

	this.origValue = this.settings.presetValue;

	return OMHFormFieldInksoftProductResults.prototype.completeInit.call(this);
}

OMHFormFieldInksoftProductResults.prototype = _.create(_formFieldAdvanced2.default.prototype, {
	domInit: function domInit() {

		var self = this,
		    $tableHeader = this.parent.$container.find('.omh-table-section-headers'),
		    $tableBody = this.parent.$container.find('.omh-table-section-results'),
		    productTemplate = _.template(['<tr class="inksoft-product-table__template" name="<%= inksoftProduct.ID %>">', '<th scope="col" data-col="add">Add?</th>', '<th scope="col" data-col="garment">', '<div class="omh-add-global-garment-name"><%= inksoftProduct.Name %></div>', '<div class="omh-add-global-garment-information">', '<%= inksoftProduct.Manufacturer %> - <%= inksoftProduct.Sku %>', '</div>', '</th>', '</tr>'].join("\n")),
		    productStyleTemplate = _.template(['<tr class="inksoft-product-style-table__template">', '<td class="add-garment-style">', '<% if( inksoftProductStyle.Added ) { %>', '<small class="text-muted">Added</small>', '<% } else { %>', '<div class="form-check omh-field-wrap">', '<input type="checkbox" class="omh-field-input" id="<%= inksoftProductStyle.ID %>" name="<%= inksoftProductStyle.ID %>" data-omh-field-type="checkbox" data-omh-subfield-of="inksoft_style_additions" />', '<% } %>', '</td>', '<td>', '<img class="omh-add-global-garment-thumbnail" src="//stores.inksoft.com/<%= inksoftProductStyle.Sides[0][\'ImageFilePath\'] %>" />', '<div class="omh-add-global-garment-name"><%= inksoftProductStyle.Name %></div>', '</td>', '</tr>'].join("\n"));

		$(productTemplate(this.settings)).insertBefore($tableHeader.children().last());

		_.forEach(this.settings.inksoftProduct.Styles, function (inksoftProductStyle) {
			$(productStyleTemplate({ inksoftProductStyle: inksoftProductStyle })).insertBefore($tableBody.children().last());
		});
	},
	isValid: function isValid() {

		if (_.isEmpty(this.getValue())) {
			return false;
		}

		return _formFieldAdvanced2.default.prototype.isValid.call(this);
	}
	// getValue: function() {
	// 	var inksoftProductStyles = [];

	// 	_.forEach(this.fields, function(productStyle, productStyleId) {
	// 		inksoftProductStyles[ productStyleId ] = productStyle.getValue();
	// 	});

	// 	return inksoftProductStyles;
	// },
	// addInksoftProduct: function(inksoftProduct) {

	// 	var self = this,
	// 		inksoftProductOptions = {
	// 			inksoftProductId 	: inksoftProduct.ID,
	// 			inksoftProduct 		: inksoftProduct,
	// 			presetValue: {
	// 				inksoftProductId 	: inksoftProduct.ID,
	// 				styles 				: _.get( inksoftProduct, 'Styles', [] )
	// 			}
	// 		};

	// 	// Initialize styles

	// 	var newInksoftProduct = new OMHFormFieldInksoftProduct( '', this, inksoftProductOptions );

	// 	_.forEach( inksoftProduct.Styles, function(inksoftProductStyle) {

	// 		var inksoftProductStyleOptions = {
	// 			inksoftProductStyleId 	: inksoftProductStyle.ID,
	// 			inksoftProductStyle 	: inksoftProductStyle,
	// 			presetValue 			: {
	// 				inksoftProductStyle 	: inksoftProductStyle,
	// 				inksoftProductStyleId 	: inksoftProductStyle.ID,
	// 			}
	// 		};

	// 		var newInksoftProductStyle = new OMHFormFieldInksoftProductStyle( '', self, inksoftProductStyleOptions );
	// 	});

	// 	this.$input.trigger({type: 'change',namespace: this.getParentEventNamespace()});
	// }
});

module.exports = OMHFormFieldInksoftProductResults;

/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function ($) {

	var instances = [];

	// Freeze form fields before initialization
	function freezeUninitializedFields(event) {
		event.preventDefault();
		event.stopPropagation();
		$(this).blur();
	}
	$(document)
	// Freeze fields before initialization (removed after init)
	.on('input click focus keydown keyup', 'form.mh-form input, form.mh-form textarea, form.mh-form select', freezeUninitializedFields)
	// Virtual disable fields (disable them but still allow contraint validation)
	.on('input click focus keydown keyup', '[data-virtual-disabled]', function (event) {
		event.preventDefault();
	});

	function reset() {
		_.forEach(instances, function (form) {
			form.reset();
		});
	}

	function submit() {
		_.forEach(instances, function (form) {
			form.submit();
		});
	}

	// Declare Form object (class)
	function OMHForm(container, options) {

		var self = this,
		    $container = this.$container = $(container);

		// Default options and overrides
		var defaults = {
			ajax_action: 'omh_' + $container.data('ajax-action'),
			allowSaveUnchanged: $container.hasClass('allow-save-unchanged'),
			defaultInvalidMsg: 'Please correct the errors below.',
			customInvalidMsg: null
		};

		var settings = this.settings = $.extend(true, {}, defaults, options);

		// Form attributes
		var form_id = $container.attr('id'),
		    form_nonce = this.form_nonce = $container.data('form-nonce'),
		    form_model = this.form_model = $container.data('omh-form-model');

		var namespace = this.namespace = form_id;

		// jQuery elements
		var $fields = this.$fields = $('.omh-field-input[data-omh-form="' + form_id + '"]').add('.omh-field-input:not([data-omh-subfield-of])', $container),
		    $submitBtns = this.$submitBtns = $('.btn[form="' + form_id + '"][value="submit"]'),
		    $resetBtns = this.$resetBtns = $('.btn[data-reset-form="' + form_id + '"]'),
		    $formModal = this.$formModal = $container.closest('.modal'),
		    $saveBar = this.$saveBar = $('#mh-dashboard-save-bar');

		var fields = this.fields = {};

		var submitting = false;

		// function addField( field ) {
		// 	fields[ field.getName() ] = field;

		// 	if( !self.$fields.filter( field.$input ).length ) {

		// 		self.$fields = self.$fields.add(field.$input);
		// 	}
		// }

		function getField(name) {
			return _.get(fields, name);
		}

		function setFieldValue(name, value) {
			if (field = getField(name)) {
				field.setValue(value);
			}
		}

		function setFieldValues(fieldValues) {
			_.forEach(fieldValues, function () {
				setFieldValue(value, name);
			});
		}

		function valuesHaveChanged() {
			return !_.every(_.invokeMap(self.fields, 'isOrigValue'));
			// return Boolean( self.$fields.filter('.changed-value').length );
		}

		function valuesAreValid() {
			return _.every(_.invokeMap(self.fields, 'isValid'));
			// return ( !Boolean( self.$fields.filter('.is-invalid').length ) );
		}

		function validateFields() {
			_.invokeMap(fields, 'validate');
		}

		function canSubmit() {
			return valuesAreValid() && (valuesHaveChanged() || settings.allowSaveUnchanged);
		}

		function updateUI() {

			var formInvalidMsg = '';
			if (!valuesAreValid()) {

				formInvalidMsg = settings.defaultInvalidMsg;

				var customInvalidMsgField = self.$fields.filter('.is-invalid[data-form-invalid-msg]'); //dev:omhff

				if (customInvalidMsgField.length) {

					formInvalidMsg = customInvalidMsgField.data('form-invalid-msg');
				}
			}

			$('.mh-screen-form-msgs').html(formInvalidMsg);

			// Open the savebar drawer if values have changed and this is the screen form
			if ('mh-screen-form' == form_id) {

				$saveBar.toggleClass('onyx-drawer--open', valuesHaveChanged()).toggleClass('onyx-drawer--warning', !valuesAreValid());
			}

			// Enable reset buttons if values have changed
			$resetBtns.prop('disabled', !valuesHaveChanged());

			// Enable / Disable submit buttons
			$submitBtns.prop('disabled', !canSubmit()).filter('.submit-allow-valid').prop('disabled', !valuesAreValid());
		}

		function reset() {
			_.invokeMap(fields, 'reset');

			if (!$formModal.length) {
				//dev:generalize //dev:config
				omh.Notices.removeNotice(settings.ajax_action);
			}

			updateUI();
			$container.trigger('omh_form_reset', this);
		}

		function clear() {
			_.invokeMap(fields, 'clearValue');

			updateUI();
			$container.trigger('omh_form_clear', this);
		}

		function submit(submitAction, submitAllowValid, eventHooks) {

			// submitAllowValid flag means the form can be submitted without any values having been changed
			submitAction = _.defaultTo(submitAction, defaults.ajax_action);
			submitAllowValid = _.defaultTo(submitAllowValid, false);
			eventHooks = _.defaultTo(eventHooks, true);

			if (!submitting) {

				validateFields();

				if (canSubmit() || submitAllowValid && valuesAreValid()) {

					// Check if we should confirm the submit action
					var confirmSubmit = false;
					if ($container.data('confirm-msg') || $container.data('confirm-if')) {

						confirmSubmit = true;

						// If data-confirm-if is present, check
						if ($container.data('confirm-if')) {
							// confirm-if is in very simpley [field name]=[field value] format
							var confirmIf = $container.data('confirm-if').split('=');

							if (!_.has(fields, confirmIf[0]) || confirmIf[1] != fields[confirmIf[0]].getValue()) {
								confirmSubmit = false;
							}
						}

						if (confirmSubmit) {

							// Determine confirmation message
							var confirmMsg = _.defaultTo($container.data('confirm-msg'), 'Click OK to confirm form submission');

							if (!confirm(confirmMsg)) {
								return false;
							}
						}
					}

					// Event hook before submit
					if (eventHooks) {

						var beforeSubmit = {
							allowSubmit: true,
							submitAction: submitAction,
							submitAllowValid: submitAllowValid,
							eventHooks: eventHooks
						};

						$container.trigger('omh_form_before_submit', [self, beforeSubmit]);

						// Allow event handlers to modify beforeSubmit (and cancel the submit)
						if (!beforeSubmit.allowSubmit) {
							return false;
						}
					}

					// Commence form submit
					submitting = true;

					// Disable form buttons
					$resetBtns.prop('disabled', true);
					$submitBtns.prop('disabled', true);

					var $customActionSubmitBtns = $submitBtns.filter('[data-ajax-action="' + submitAction + '"]');

					if ($customActionSubmitBtns.length) {
						$customActionSubmitBtns.addClass('cta-loading');
					} else {
						$submitBtns.filter(':not([data-ajax-action])').addClass('cta-loading');
					}

					var ajax_data = {},
					    ajax_args = {};

					if ($container.data('submit-type') == 'form') {

						var formData = new FormData(container[0]);

						_.forEach(fields, function (field, name) {

							if (field.getValue()) {
								if ('file' == field.getType()) {
									formData.set(name, field.getValue(), field.getFileName());
								} else {
									formData.set(name, field.getValue());
								}
							}
						});

						formData.set('action', _.defaultTo(submitAction, settings.ajax_action));
						formData.set('security', form_nonce);
						formData.set('obj_id', $container.data('objectId'));

						ajax_args = {
							data: formData,
							processData: false,
							contentType: false
						};
					} else {

						ajax_data = {
							obj_id: $container.data('objectId'),
							form_data: parseFields()
						};
					}

					omh.ajax(_.defaultTo(submitAction, settings.ajax_action), ajax_data, processRequest, form_nonce, ajax_args);
				}
			}
		}

		function parseFields() {

			var field_data = {};

			_.forEach(fields, function (field, name) {

				// var namePath = name.split('[')
				// Parse input arrays
				_.set(field_data, _.join(_.map(name.split('['), function (part) {

					var trimmedPart = _.trim(part, ']');
					return isNaN(trimmedPart) ? trimmedPart : "'" + trimmedPart + "'";
				}), '.'), field.getValue());

				// field_data[name] = field.getValue();
			});

			return field_data;
		}

		function loadData(options) {

			var loadData_nonce = $('#omh_load_data_nonce').val();

			if (loadData_nonce) {

				var loadData_model = _.get(options, 'model', form_model);

				omh.ajax('omh_load_data', {
					model: loadData_model,
					id: options.id
				}, processRequest, loadData_nonce);
			} else {
				omh.Notices.setNotice({
					key: 'omh_load_data_error',
					type: 'danger',
					content: 'Unable to load form data.'
				});
			}
		}

		function processRequest(response) {

			var responseData = response.data;

			form_nonce = _.get(responseData, 'form_nonce', form_nonce);

			// Emit event omh_process_form_request
			if ('omh_load_data' != response.request.action) {
				$container.trigger('omh_form_after_submit', [self, response]);
			}

			// Update field values and add any error messages
			_.forEach(responseData, function (fieldResponse, fieldName) {

				if (_.has(fields, fieldName)) {
					fields[fieldName].handleResponse(response);
				}
			});

			// Hide modal form on success //dev:improve
			if ($formModal.length > 0 && response.request.action !== 'omh_load_data' && $formModal.data('preserve-on-form-submit') !== true) {
				$formModal.modal('hide');
			}

			submitting = false;

			$submitBtns.removeClass('cta-loading');

			updateUI();
		}

		// INITIALIZE

		// Initialize form field objects
		this.$fields.filter('.omh-field-input--load-first').OMHFormField(this);
		this.$fields.filter(':not(.omh-field-input--load-first)').OMHFormField(this);

		// Form level event handling //dev:improve //dev:generalize //dev:object structure
		var namespacedEvents = ['input.' + this.namespace, 'change.' + this.namespace, 'reset.' + this.namespace];

		$(document).on(namespacedEvents.join(' '), updateUI);

		$container.on('submit', function (event) {
			event.preventDefault();
		});

		// Maybe set custom submit text
		if ($container.data('custom-submit-label')) {
			$submitBtns.not('.mh-forms-submit--custom').text($container.data('custom-submit-label'));
		}

		// Initialize UI
		updateUI();

		// Submit button event handling
		$submitBtns.click(function (event) {
			event.preventDefault();
			if (!($(this).prop('disabled') || $(this).hasClass('disabled'))) {
				submit($(this).data('ajax-action'), $(this).hasClass('submit-allow-valid'));
			}
		});

		// Attach this object to the DOM
		$container[0].omhForm = this; // pretty sure this doesn't do anything...
		$container.get(0).omhForm = this;

		// function initForm() {

		// 	// this.$fields = $('.omh-form-field[data-omh-form="'+form_id+'"').add('.omh-form-field', $container);

		// 	// this.$fields.OMHFormField( this );

		// 	$container
		// 		// Initialize fields
		// 		// .find('input, select, textarea').filter('[name]').each(function() {

		// 		// 	fields[ $(this).attr('name') ] = new FormField( $(this), self );
		// 		// })
		// 		// Setup form-level event handlers
		// 		.on('input change reset', updateUI)
		// 		.on('submit', function(event) {
		// 			event.preventDefault();
		// 		});

		// 	// Modal Forms event handling //dev:improve
		// 	$formModal.on('hide.bs.modal',reset);

		// 	if( $container.data('custom-submit-label') ) {

		// 		$submitBtns.not('.mh-forms-submit--custom').text( $container.data('custom-submit-label') );
		// 	}

		// 	updateUI();

		// 	$submitBtns.click(function(event) {
		// 		event.preventDefault();

		// 		submit( $(this).data('ajax-action'), $(this).hasClass('submit-allow-valid') );
		// 	});
		// }
		// initForm();

		this.access = {
			fields: fields,
			// addField: addField,
			$container: $container,
			getField: getField,
			setFieldValue: setFieldValue,
			setFieldValues: setFieldValues,
			valuesAreValid: valuesAreValid,
			valuesHaveChanged: valuesHaveChanged,
			loadData: loadData,
			parseFields: parseFields,
			reset: reset,
			clear: clear,
			submit: submit
		};

		return this.access;
	}

	OMHForm.prototype = $.extend({}, OMHForm.prototype, {
		addField: function addField(field) {
			this.fields[field.getName()] = field;

			// if( !this.$fields.filter( field.$input ).length ) {

			// 	this.$fields = this.$fields.add(field.$input);
			// }
		},
		getField: function getField(name) {
			return _.get(this.fields, name);
		},
		setFieldValue: function setFieldValue(name, value) {
			var field = this.getField(name);
			if (field) {
				field.setValue(value);
			}
		},
		setFieldValues: function setFieldValues(fieldValues) {
			_.forEach(fieldValues, function () {
				this.setFieldValue(value, name);
			});
		}
	});

	function init() {

		$('form.mh-form').each(function () {

			instances.push(new OMHForm($(this)));
		});

		// Unfreeze form fields
		$(document).off('input click focus keydown keyup', 'form.mh-form input, form.mh-form textarea, form.mh-form select', freezeUninitializedFields);

		$(document).trigger('omh_forms_initialized');

		// $('.btn.input-reset-btn').click(function() {})

		// //dev:improve
		// $('.btn.remove-file').click(function() {$(this).children('input').click();});
	}

	$(document).ready(init);

	return {
		instances: instances,
		reset: reset,
		submit: submit
	};
}(jQuery);

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function ($) {

	var instances = [];

	function Table(target) {

		var $wrap = this.$wrap = $(target);
		var table_type = this.table_type = $wrap.data('table-type');
		var query = this.query = _.defaultTo($wrap.data('query'), {});
		var pagination = this.pagination = _.defaultTo($wrap.data('pagination'), {});
		var scopes = this.scopes = _.toArray(_.toString($wrap.attr('data-force-scope')).split(','));
		var filters = this.filters = _.defaultTo($wrap.data('default-filters'), '{}');

		var tooltipDefaults = {
			container: 'body',
			placement: 'bottom',
			trigger: 'hover',
			boundary: 'viewport',
			delay: {
				show: 400,
				hide: 100
			}
		};

		/**
   * Process AJAX request responses
   *
   * @param      JSON  response  The AJAX response data
   */
		function processRequest(response) {

			var responseData = response.data;

			// Update local query object
			query = responseData.query || query;
			scopes = responseData.scopes || scopes;
			pagination = _.get(responseData, 'pagination', {});

			// Add/Replace html content sections
			if (_.has(responseData, 'sections')) {

				// Mark all .omh-table-section elements with a class that will be removed if their section was in the AJAX results
				$('.omh-table-section', $wrap).addClass('section-to-hide');

				_.forEach(responseData.sections, function (value, section) {

					$wrap.find('.omh-table-section-' + section).removeClass('section-to-hide').html(value).show();
				});

				// Empty and hide all sections still marked as not found in the results
				$('.omh-table-section.section-to-hide', $wrap).empty().hide().removeClass('section-to-hide');

				// Initialize tooltips and popovers
				$wrap.find('[data-toggle="tooltip"]:not([data-original-title])').tooltip(tooltipDefaults);
				$wrap.find('[data-toggle="popover"]:not([data-original-title])').popover(tooltipDefaults);
			}

			// Set loaded status classes
			$wrap.removeClass('not-loaded loading').addClass('loaded');

			$wrap.trigger('omh_table_request', { table: this, response: response });

			if (omh.debug) {
				console.log('processRequest', response);
			}
		}

		/**
   * Gets the table data.
   */
		function getData() {

			if (!$wrap.hasClass('table--static')) {

				var ajax_data = {
					type: table_type,
					query: query,
					pagination: pagination,
					scopes: scopes,
					filters: filters
				};

				$wrap.addClass('loading');

				omh.ajax('omh_table_request', ajax_data, processRequest, window.table_security);
			}
		}

		/**
   * Sets the table scopes.
   *
   * @param      mixed [string,array]  scopes       The scope(s) to add
   * @param      bool  clearScopes  Whether to clear existing scopes or not
   */
		function setScopes(addScopes, clearScopes) {

			var forceScope = $wrap.data('force-scope');

			// Reset sorting
			_.unset(query, 'order');
			_.unset(query, 'orderby');

			if (forceScope) {
				scopes = [forceScope];
			} else {

				clearScopes = _.defaultTo(clearScopes, false);

				var newScopes = _.isArray(addScopes) ? addScopes : [addScopes];

				scopes = clearScopes ? newScopes : _.union(scopes, newScopes);
			}

			_.set(pagination, 'paged', 1);

			getData();
		}

		// Set Event Handlers
		$wrap.on('click', '[data-set-scope]', function (e) {
			e.preventDefault();
			setScopes($(this).data('set-scope'), $(this).data('clear-scopes'));
		}).on('click', '.btn-thead', function () {
			var $th = $(this).closest('th');

			if ($th.hasClass('sortable')) {

				if ($th.hasClass('sorted-desc')) {
					// If sorted DESC, sort ASC
					query.orderby = $th.data('query-col');
					query.order = 'ASC';
				} else if ($th.hasClass('sorted-asc')) {
					// If sorted ASC, reset/remove sorting
					_.unset(query, 'order');
					_.unset(query, 'orderby');
				} else {
					// If not sorted, sort DESC
					query.orderby = $th.data('query-col');
					query.order = 'DESC';
				}

				getData();
			}
		}).on('click', '.page-link', function (e) {
			e.preventDefault();
			if ($(this).data('go-to-page')) {
				pagination.paged = $(this).data('go-to-page');
				getData();
			}

			// setPagination( $(this).data('go-to-page') );
		}).on('click', '.omh-table-search-btn', function (e) {

			query.search = $wrap.find('.omh-table-search-input').val();
			getData();
		}).on('keypress', '.omh-table-search-input', function (e) {
			if (e.which === 13) {
				query.search = $wrap.find('.omh-table-search-input').val();
				getData();
			}
		}).on('click', 'tr[data-default-row-action]', function (e) {

			var $target = $(e.target);

			if (!$target.is('a') && !$target.hasClass('row-action')) {

				var $row = $(this).closest('tr');
				var defaultRowAction = $row.data('default-row-action');
				var $rowAction = $row.find('.row-action[data-action="' + defaultRowAction + '"]');

				if ($rowAction.length) {
					$rowAction[0].click();
				}
			}
		}).on('omh_table_loaded', function (e) {

			// Initialize tooltips and popovers
			$wrap.find('[data-toggle="tooltip"]:not([data-original-title])').tooltip(tooltipDefaults);
			$wrap.find('[data-toggle="popover"]:not([data-original-title])').popover(tooltipDefaults);
		});

		// Inital data load
		if (!$wrap.hasClass('static-table') && $wrap.hasClass('not-loaded')) {
			getData();
		} else {
			$wrap.trigger('omh_table_loaded');
		}

		return {
			setScopes: setScopes,
			getData: getData,
			self: this
		};
	}

	function refreshTables() {

		_.forEach(instances, function (instance) {
			instance.getData();
		});
	}

	function initTable(target) {

		instances.push(new Table(target));
	}

	function init() {

		$('.omh-table-wrap').each(function () {

			initTable($(this));
		});
	}

	$(document).ready(init);

	return {
		initTable: initTable,
		refreshTables: refreshTables,
		instances: instances
	};
}(jQuery);

/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function ($) {

	var iconMap = {
		primary: 'check',
		secondary: 'question-circle',
		success: 'check',
		danger: 'exclamation-triangle',
		warning: 'exclamation-triangle',
		info: 'info',
		light: 'question-circle',
		dark: 'question-circle'
	};

	function getNoticeLevel(notice) {

		return _.get(notice, 'type', 'info');
	}

	function getIconClass(notice) {

		return 'fa-' + _.get(iconMap, getNoticeLevel(notice), 'info');
	}

	function getNoticePosition(notice) {

		return 'fixed' == _.get(notice, 'position') ? 'position-fixed' : '';
	}

	function setNotice(notice) {

		// Build notice
		var $notice = $('<div>', {
			role: 'alert',
			html: _.get(notice, 'content', ''),
			class: ['alert', 'alert-' + getNoticeLevel(notice), getNoticePosition(notice)].join(' ')
		}).prepend('<i class="fa ' + getIconClass(notice) + '"></i>').append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');

		var $keyedNotice = false;

		if (_.get(notice, 'key')) {

			$notice.attr('data-notice-key', notice.key);
			$keyedNotice = $('.alert[data-notice-key="' + notice.key + '"]');
		}

		if ($keyedNotice && $keyedNotice.length) {

			$keyedNotice.replaceWith($notice);
		} else {

			var noticeTarget = _.get(notice, 'target', 'mh-dashboard-notices');
			$('#' + noticeTarget).append($notice);
		}
	}

	function processNotices(response) {

		if (omh.debug) {
			console.log('processNotices', response);
		}

		_.forEach(_.get(response, 'messages', {}), setNotice);
	}

	function removeNotice(key) {

		$('.alert[data-notice-key="' + key + '"]').remove();
	}

	return {
		processNotices: processNotices,
		removeNotice: removeNotice,
		setNotice: setNotice
	};
}(jQuery);

/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function ($) {

	var instances = {};

	function getValue($target) {

		var value = null;

		if ($target.is('input')) {
			value = parseFloat($target.val());
		} else {
			value = parseFloat($target.text());
		}

		return value;
	}

	function setValue(value, $target) {

		var currentValue = getValue($target);

		if ($target.is('input')) {
			$target.val(value);
		} else {
			$target.text(value);
		}

		// Maybe trigger change event on $field
		if (currentValue != value) {
			$target.trigger('change');
		}
	}

	function CalcField(field) {

		var $field = $(field),
		    $children,
		    childSelector,
		    calcOperation;

		// function setValue(value, $target) {

		// 	var currentValue = getValue($target);

		// 	if( $target.is('input') ) {
		// 		$target.val(value);
		// 	}
		// 	else {
		// 		$target.text(value);
		// 	}

		// 	// Maybe trigger change event on $field
		// 	if( currentValue != value ) {
		// 		$target.trigger('change');
		// 	}
		// }

		// function getValue($target) {

		// 	var value = null;

		// 	if( $target.is('input') ) {
		// 		value = parseFloat( $target.val() );
		// 	}
		// 	else {
		// 		value = parseFloat( $target.text() );
		// 	}

		// 	if( $target.hasClass( 'price-val') ) {
		// 		value = parseFloat( value ).toFixed( 2 );
		// 	}

		// 	return value;
		// }

		function calcValue() {

			var newValue = 0;

			if ('product' == calcOperation) {

				newValue = 1;
			}

			$children.each(function () {

				var childValue = getValue($(this));

				if (_.isFinite(childValue)) {

					if ('sum' == calcOperation) {

						newValue += 'true' === $(this).attr('data-calc-negate') ? -childValue : childValue;
					} else if ('product' == calcOperation) {

						newValue *= childValue;
					}
				}
			});

			if ($field.hasClass('price-val')) {
				newValue = parseFloat(newValue).toFixed(2);
			}

			setValue(newValue, $field);
		}

		function init() {

			// Determine operation type
			if (childSelector = $field.attr('data-calc-sum')) {

				calcOperation = 'sum';
				$children = $('[data-calc-sum-for="' + childSelector + '"]');
			} else if (childSelector = $field.attr('data-calc-product')) {

				calcOperation = 'product';
				$children = $('[data-calc-product-for="' + childSelector + '"]');
			}

			if ($children.length) {

				calcValue();
				$children.on('input change reset calcFields', calcValue);
			}
		}
		init();

		$field.data('calcField', this);

		return {
			field: $field,
			calcValue: calcValue,
			children: $children,
			childSelector: childSelector
		};
	}

	function initCalcFields() {

		$('[data-calc-sum],[data-calc-product]').each(function () {

			if (!$(this).data('calcField')) {
				var calcAttr = $(this).attr('data-calc-sum') ? $(this).attr('data-calc-sum') : $(this).attr('data-calc-product');

				instances[calcAttr] = new CalcField($(this));
			}
		});
	}
	$(document).ready(initCalcFields);

	return {
		instances: instances,
		initCalcFields: initCalcFields,
		getValue: getValue,
		setValue: setValue
	};
}(jQuery);

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function ($) {

	var instances = [];

	function Modal(container) {

		var $modal = this.$modal = $(container);
		var omhForm = this.omhForm = _.get($modal.find('.mh-form')[0], 'omhForm');

		// Set event handlers
		$modal.on('show.bs.modal shown.bs.modal', function () {
			$('.modal-backdrop').appendTo($modal.parent());
		});

		$modal[0].omhModal = this;

		return {
			$modal: $modal,
			omhForm: omhForm
		};
	}

	function initModals() {

		$('.modal.mh-modal').each(function () {
			instances.push(new Modal($(this)));
		}).on('hide.bs.modal', function (event) {

			var $modal = $(event.target);

			if ($modal.length && $modal[0].omhModal.omhForm) {
				$modal[0].omhModal.omhForm.access.reset();
			}
		});

		// Modal Forms event handling //dev:improve
		// $formModal.on('hide.bs.modal',reset);

		// Fix modal backdrop overlay placement
		// $(document).on('show.bs.modal shown.bs.modal', function() {
		// 	$( '.modal-backdrop' ).appendTo( '#mh-dashboard-content' );
		// });
	}
	$(document).ready(initModals);

	$(document).ready(function () {

		// Set generalized modal event handlers
		$(document).on('click', '[data-omh-toggle-modal]', function (event) {

			var $target = $(event.target);
			var $modal = $('#' + $target.data('omh-toggle-modal'));

			if ($modal.length) {

				// Maybe preload modal form
				var preloadID = $target.data('omh-modal-load-id');

				if (preloadID && $modal[0].omhModal) {

					$modal[0].omhModal.omhForm.access.loadData({ id: preloadID });
				}

				$modal.modal();
			}
		});
	});

	return {
		instances: instances
	};
}(jQuery);

/***/ }),
/* 38 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function ($) {

	var instances = [];

	function PriceWidget(container) {

		// Vars
		// -=-=-=-=-

		var self = this;
		var $container = this.$container = $(container);

		var widgetAnimationLength = 750,
		    widgetElems = {
			priceInput: $('input#price'),
			basePriceInput: $('input#base_price'),
			earningsInput: $('input#earnings_per_unit'),
			basePriceWrap: $('.mhpw__base-price-wrap'),
			basePriceLabels: $('.mhpw__base-price-labels'),
			basePrice: $('.mhpw__base-price'),
			yourPrice: $('.mhpw__your-price'),
			profit: $('.mhpw__profit'),
			profitPercent: $('.mhpw__profit-percent')
		};

		// Helper Functions / Event Handlers
		// -=-=-=-=-

		function initDynamicNumber(elem, upperBound, format) {
			elem.dynamicNumber({
				from: 0,
				to: upperBound,
				duration: widgetAnimationLength,
				format: format,
				percentage: {
					decimals: 1
				},
				currency: {
					indicator: '$',
					decimals: '2',
					separator: ',',
					decimalsPoint: '.'
				}
			});
		}

		function validatePriceInput(event) {

			if (event.key === '.' && widgetElems.priceInput.val().indexOf('.') > -1) {
				// Dont allow duplicate Periods
				event.preventDefault();
			} else if (event.key.length === 1 && /\D/.test(event.key) && event.key !== '.') {
				// Dont allow non digits/periods 
				event.preventDefault();
			}
		}

		function updateEarningsInput() {

			var yourPrice = widgetElems.priceInput.val(),
			    basePrice = widgetElems.basePriceInput.val(),
			    profit = parseFloat(yourPrice - basePrice).toFixed(2);

			widgetElems.earningsInput.val(profit);
			widgetElems.earningsInput.trigger('change');
		}

		function updatePriceWidget() {
			var yourPrice = widgetElems.priceInput.val(),
			    basePrice = widgetElems.basePriceInput.val(),
			    profit = widgetElems.earningsInput.val(),
			    profitPercent = Math.round(profit / yourPrice * 100),
			    newWidth = 100 - profitPercent < 100 ? 100 - profitPercent : 100;

			if (yourPrice.length > 0) {

				// Update widget numbers
				widgetElems.yourPrice.dynamicNumber('go', yourPrice);
				widgetElems.basePrice.dynamicNumber('go', basePrice);
				widgetElems.profit.dynamicNumber('go', profit);
				widgetElems.profitPercent.dynamicNumber('go', profitPercent + '%');

				// Animate the widget 
				widgetElems.basePriceWrap.animate({ width: newWidth + '%' }, widgetAnimationLength);
				if (newWidth >= 89) {
					widgetElems.basePriceLabels.animate({ right: 100 - 89 + '%' });
				} else {
					widgetElems.basePriceLabels.animate({ right: 0 });
				}
			}
		}

		// Init Price Widget
		// -=-=-=-=-

		function initPriceWidget() {

			updatePriceWidget(); // run this once on page load

			// Init Dynamic Numbers
			// -=-=-=-=-

			var mhInitials = {
				price: parseFloat(widgetElems.priceInput.val()),
				base: parseFloat(widgetElems.basePriceInput.val()) // Add the rest after object initialized
			};mhInitials.profit = mhInitials.price - mhInitials.base;
			mhInitials.percent = mhInitials.profit / mhInitials.price * 100;

			initDynamicNumber(widgetElems.yourPrice, mhInitials.price, 'currency');
			initDynamicNumber(widgetElems.basePrice, mhInitials.base, 'currency');
			initDynamicNumber(widgetElems.profit, mhInitials.profit, 'currency');
			initDynamicNumber(widgetElems.profitPercent, mhInitials.percent, 'percentage');

			$('.mhpw__base-price, .mhpw__your-price, .mhpw__profit, .mhpw__profit-percent').dynamicNumber('start');
		}
		initPriceWidget();

		// Attach Event Handlers
		// -=-=-=-=-

		widgetElems.priceInput.keyup(updateEarningsInput);
		widgetElems.priceInput.keydown(function (event) {
			validatePriceInput.call(this, event);
		});
		widgetElems.earningsInput.change(updatePriceWidget);
	}

	function initPriceWidgets() {

		$('.mhpw').each(function () {

			instances.push(new PriceWidget($(this)));
		});
	}

	$(document).ready(initPriceWidgets);

	return {
		instances: instances
	};
}(jQuery);

/***/ }),
/* 39 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function ($) {

	if ($('body.single-product').length) {

		// Flickity.defaults.cellSelector = '.enabled';

		$(document).ready(function ($) {
			// Single Product page size attr
			// ==========


			var $sizeSelect = $('select#pa_size'),
			    $sizeOptionBlocks = $('.size-blocks-wrap > .pdp-option-block'),
			    $garmentSelect = $('select#pa_garment'),
			    $garmentOptionBlocks = $('.garment-blocks-wrap > .pdp-option-block'),
			    $form = $('form.variations_form'),
			    $sizeOptionBlocks = $('.size-blocks-wrap .pdp-option-block'),
			    $garmentDetails = $('.garment-product-details'),
			    $bulkPricingTable = $('#BulkPricingTablePDP'),
			    $qtyInput = $('.pdp__meta-item--qty input[name="quantity"]'),
			    $qtyPreviewCells = $('#BulkPricingTablePDP .price-tiers__preview-row .qty-preview'),
			    $mainProdSlider = $('.product div.images .flickity.product-slider .slider'),
			    $thumbProdSlider = $('.product div .flickity.product-thumbs .slider');

			var garmentData = $form.data('product_garments'),
			    variationsData = $form.data('product_variations'),
			    priceTiers = $form.data('price_tiers'),
			    origCartQty = parseInt($bulkPricingTable.data('orig-qty')),

			// flickityMain 			= $mainProdSlider.data('flickity'),
			// flickityThumbs 			= $thumbProdSlider.data('flickity'),
			// garmentCountAvailable 	= parseInt( $('.pdp-option--pa_garment').data('options-count') ) || 0,
			selectedGarment = null,
			    galleryGarment = null;

			// var garmentCountTotal 		= Object.keys( garmentData ).length;

			// if( garmentCountTotal > 1 ) {
			// 	$('body').addClass('product-multiple-garments');
			// }


			window.$mainProdSlider = $mainProdSlider;
			window.$thumbProdSlider = $thumbProdSlider;

			if (omh.debug) {
				console.log('omh.pdp INIT');
				console.log('garmentData', garmentData);
				console.log('variationsData', variationsData);
				console.log('priceTiers', priceTiers);
			}

			// Init size blocks
			if ($sizeSelect.length) {
				var $selectWrap = $sizeSelect.closest('td.value');
				$selectWrap.append('<div class="pdp-option-blocks size-blocks-wrap"></div>');
				var $blocksWrap = $selectWrap.find('.size-blocks-wrap');

				$sizeSelect.children('option').each(function () {
					var value = $(this).attr('value');
					var label = $(this).html();

					if (label != 'Choose an option') {
						$blocksWrap.append('<div class="pdp-option-block size-select-block" data-value="' + value + '" data-label="' + label + '">' + label + '</div>');
					}
				});

				$sizeOptionBlocks = $('.size-blocks-wrap .pdp-option-block');
			}

			// Helper to get product garment object from product garment attribute slug
			function getProductGarmentBySlug(slug) {

				var foundGarment = false;

				$.each(garmentData, function (index, garment) {

					if (slug == garment.garment.style_slug) {
						foundGarment = garment;
						return false;
					}
				});

				return foundGarment;
			}

			function getGarmentFirstAvailableSize(garment) {

				var firstAvailableSize = null;

				$.each(garment.sizes, function (size, enabled) {
					if (enabled) {
						firstAvailableSize = size;
						return false;
					}
				});

				return firstAvailableSize;
			}

			function garmentHasSizeAvailable(garment, checkSize) {

				checkSize = getSizeLabel(checkSize);
				var hasSizeAvailable = false;

				$.each(garment.sizes, function (size, enabled) {
					if (size == checkSize && enabled) {
						hasSizeAvailable = true;
						return false;
					}
				});

				return hasSizeAvailable;
			}

			function getSizeSlug(size) {

				return 'size-' + size.replace('size-', '').toLowerCase();
			}

			function getSizeLabel(size) {
				return size.replace('size-', '').toUpperCase();
			}

			// Sync bulk pricing Qty Preview
			// function updateCartQtyPreview() {
			// 	// $qtyPreviewAdd.text( $qtyInput.val() ).trigger('calcFields');


			// }
			// updateCartQtyPreview();
			// 
			// 
			// function getPreviewPrice() {
			// 	return parseFloat( $bulkPricingTable.find('.price-tiers__garment-row.selected-garment td.preview-tier').text() );
			// }

			// function updateVariationPrice() {
			// 	var $variationPrice = $('.woocommerce-variation-price .woocommerce-Price-amount');
			// 	var childrenHtml = $variationPrice.children().wrap('<div>').parent().html();
			// 	// $variationPrice.html( childrenHtml + getPreviewPrice() );

			// 	// Get preview variation price

			// }

			// Helper to get price tier based on a qty
			function getPriceTierAtQty(qty) {
				// qty = ( typeof qty === 'undefined' ) ?   
				var tierIndex = 1;

				$.each(priceTiers, function (index, minQty) {
					if (qty >= minQty) {
						tierIndex = index + 1;
					}
				});

				return tierIndex;
			}

			function initializeQtyPreview() {

				var origTier = getPriceTierAtQty(origCartQty);

				$qtyPreviewCells.filter('[data-tier="' + origTier + '"]').children('.current-qty').text(origCartQty);

				$bulkPricingTable.find('[data-tier="' + origTier + '"]').addClass('orig-tier');
			}

			function updateQtyPreview() {

				var previewQtyTotal = parseInt(origCartQty) + parseInt($qtyInput.val());

				var previewTier = getPriceTierAtQty(previewQtyTotal);

				$qtyPreviewCells.filter('[data-tier="' + previewTier + '"]').addClass('preview-tier').children('.preview-qty').text(previewQtyTotal);
				$bulkPricingTable.find('[data-tier]').removeClass('preview-tier').filter('[data-tier="' + previewTier + '"]').addClass('preview-tier');
				$qtyPreviewCells.not('.preview-tier').children('.preview-qty').empty();

				// updateVariationPrice();
			}
			// initializeQtyPreview();
			// updateQtyPreview();
			// $qtyInput.change(updateQtyPreview);


			// $form.on( 'found_variation', function(event, variation) {


			function updateGalleryGarment() {

				// Make sure to initialize the featured thumnbs				
				if ($thumbProdSlider.find('.thumb:not([data-attachment-id])').length) {

					$thumbProdSlider.find('.thumb:not([data-attachment-id])').attr('data-attachment-id', $form.data('product_featured_img_id'));
				}

				// Maybe update Gallery images
				if (!galleryGarment || selectedGarment.garment_style_id != galleryGarment.garment_style_id) {

					// Pre-disable all gallery slides
					$mainProdSlider.find('.slide').removeClass('enabled');
					$thumbProdSlider.find('.thumb').removeClass('enabled');

					// var imgKeys = ['image_front', 'image_back', 'image_proof'];

					// for( var i = 0; i < imgKeys.length; i++) {

					// 	var imgKey = imgKeys[i];
					// 	if ( selectedGarment.hasOwnProperty[ imgKeys[i] ] 
					// 		&& selectedGarment[ imgKeys[i] ].hasOwnProperty('id')	
					// 	) {

					// 		var $imgThumb = $thumbProdSlider.find('.thumb[data-attachment-id="' + selectedGarment[ imgKeys[i] ].id + '"]').first();
					// 		$imgThumb.addClass('enabled');

					// 		$mainProdSlider.find('.slide').eq($imgThumb.index()).addClass('enabled');
					// 	}
					// }

					// Don't worry about garment.image_front 
					// because WC/Salient already change that one 
					// (hence the :not(:first-child) above)

					// Find the thumbs to enable, and then enable main slider
					// slides based on thumb indeces

					var selectedSlideIndex = false;

					if (selectedGarment.image_front.hasOwnProperty('id')) {

						var $imgFrontThumb = $thumbProdSlider.find('.thumb[data-attachment-id="' + selectedGarment.image_front.id + '"]').first();
						$imgFrontThumb.addClass('enabled');

						selectedSlideIndex = selectedSlideIndex === false ? $imgFrontThumb.index() : selectedSlideIndex;

						$mainProdSlider.find('.slide').eq($imgFrontThumb.index()).addClass('enabled');
					}

					if (selectedGarment.image_back.hasOwnProperty('id')) {

						var $imgBackThumb = $thumbProdSlider.find('.thumb[data-attachment-id="' + selectedGarment.image_back.id + '"]').first();
						$imgBackThumb.addClass('enabled');

						selectedSlideIndex = selectedSlideIndex === false ? $imgBackThumb.index() : selectedSlideIndex;

						$mainProdSlider.find('.slide').eq($imgBackThumb.index()).addClass('enabled');
					}

					if (selectedGarment.image_proof.hasOwnProperty('id')) {

						var $imgProofThumb = $thumbProdSlider.find('.thumb[data-attachment-id="' + selectedGarment.image_proof.id + '"]').first();
						$imgProofThumb.addClass('enabled');

						selectedSlideIndex = selectedSlideIndex === false ? $imgProofThumb.index() : selectedSlideIndex;

						$mainProdSlider.find('.slide').eq($imgProofThumb.index()).addClass('enabled');
					}

					$thumbProdSlider.flickity('reloadCells');
					$mainProdSlider.flickity('reloadCells');

					$mainProdSlider.flickity('select', selectedSlideIndex, false, true);

					galleryGarment = selectedGarment;
				}
			}
			// });

			$form.on('show_variation', function (event, variation, purchasable) {
				// updateVariationPrice();


				updateGalleryGarment();
			});

			// Sync PDP Options Blocks to select changes
			$('table.variations select').change(function () {

				// Set corresponding block
				$(this).closest('td.value').children('.pdp-option-blocks').children().removeClass('selected').filter('[data-value="' + $(this).val() + '"]').addClass('selected');
			}).each(function () {
				$(this).trigger('change');
			});

			// Handle UI changes based on garment selection
			$form.on('update_variation_values', function (event) {

				var garment = getProductGarmentBySlug($garmentSelect.val());

				// Hide unavailable sizes for this garment
				$sizeOptionBlocks.each(function () {

					var sizeEnabled = false,
					    sizeLabel = $(this).data('label');

					if (sizeLabel && garment.sizes.hasOwnProperty(sizeLabel) && garment.sizes[sizeLabel]) {
						sizeEnabled = true;
					}

					$(this).toggle(sizeEnabled);
				});

				// Highlight selected garment in bulk pricing table
				$bulkPricingTable.find('tr.price-tiers__garment-row').removeClass('selected-garment').filter('[data-garment-style-id="' + garment.garment_style_id + '"]').addClass('selected-garment');

				selectedGarment = garment;
				// updateVariationPrice();
				updateGalleryGarment();
			});

			$form.on('check_variations', function (event) {

				var garmentSlug = $garmentSelect.val();

				var garment = getProductGarmentBySlug(garmentSlug);

				$garmentDetails.hide().filter('[data-garment-product-id="' + garment.garment.garment_product_id + '"]').show();
			});

			$garmentOptionBlocks.click(function () {
				// Determine first available size for this garment
				var garmentSlug = $(this).attr('data-value');

				var garment = getProductGarmentBySlug(garmentSlug);

				// Select first available size if current size doesn't match
				if (!garmentHasSizeAvailable(garment, $sizeSelect.val())) {

					var firstAvailableSize = getGarmentFirstAvailableSize(garment);

					$sizeSelect.val(getSizeSlug(firstAvailableSize)).trigger('change');
				}

				$garmentSelect.val(garmentSlug).trigger('change');

				selectedGarment = garment;

				// $(this).closest('td.value').find('select').val( $(this).attr('data-value') ).trigger('change');
			});

			$sizeOptionBlocks.click(function () {

				$sizeSelect.val($(this).attr('data-value')).trigger('change');
				// $(this).closest('td.value').find('select').val( $(this).attr('data-value') ).trigger('change');
			});

			$('a.bulk-pricing-cta').click(function (event) {
				event.preventDefault();
				event.stopPropagation();
				$('#tab-title-bulk_pricing a').click();
			});

			// Make sure a variation is selected
			if (!$garmentSelect.val()) {
				$garmentOptionBlocks.first().click();
			}

			// $sizeSelect.change(function() {
			// 	$blocksWrap
			// 		.children().removeClass('selected')
			// 		.filter('[data-value="'+$sizeSelect.val()+'"]').addClass('selected');
			// });

			// $blocksWrap.children().click(function() {
			// 	$sizeSelect.val( $(this).attr('data-value') ).trigger('change');
			// });

			// $sizeSelect.trigger('change');

		});
	}
}(jQuery);

/***/ })
/******/ ]);
//# sourceMappingURL=omh.js.map
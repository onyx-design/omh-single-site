import select2 from './vendor/select2.full.min';

jQuery(document).ready(function() {
    jQuery('.select2-admin').select2(
    	{
    		width: "100%",
    		containerCssClass : "omh-select2"
    	}
    );
});

module.exports = (function($) {

	// Initialize Drawers event handlers
	$(document).on('click', '.onyx-menu-item--parent > a', function(e) {

		$target = $(e.target);

		if( $target.closest('.onyx-drawer').length ) {
		  e.preventDefault();
		  e.stopPropagation();  
		}
		
		$target.closest('.onyx-menu-item--parent').toggleClass('submenu-expanded');
	})
	.on('click', '.onyx-page-overlay', function(e) {
		$('.onyx-drawer').removeClass('onyx-drawer--open');
		$('.onyx-drawer__toggle').removeClass('is-active');
		$('body,html').removeClass('onyx-body--drawer-open');
	})
	.on('click', '.onyx-drawer__toggle', function(e) {
		e.preventDefault();
		var $target = $(e.target);

		var $drawer = 
		    $target.attr('data-drawer-toggle') ?
		    $('.onyx-drawer--' + $target.data('drawer-toggle') ) :
		    $target.closest('.onyx-drawer');

		// Close other drawers
		$('.onyx-drawer:not(.onyx-drawer--no-close)').not( $drawer ).removeClass('onyx-drawer--open');
		$('body').removeClass('blur-content');
		$drawer.toggleClass('onyx-drawer--open');

		if( $drawer.hasClass('blur-content') ) {
			$('body').toggleClass('blur-content', $drawer.hasClass('onyx-drawer--open'));
		}

		// Maybe Auto-focus search input
		if( $('.onyx-drawer--search.onyx-drawer--open').length ) {
			$('.onyx-drawer--search.onyx-drawer--open').find('.search__input').focus();
		}
		else {
			$('.onyx-drawer--search .search__input').blur();
		}

		// Maybe toggle hamburger state
		if( $target.hasClass('hamburger') && !$target.hasClass('no-js-toggle') ) {
			$target.toggleClass('is-active', $drawer.hasClass('onyx-drawer--open'));
		}     

		// Add body class if drawer is open
		$('body,html').toggleClass('onyx-body--drawer-open', $('.onyx-drawer--open').length );
	});

})(jQuery);

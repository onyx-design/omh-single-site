module.exports = (function($) {

	if( $('body.single-product').length ) {

		// Flickity.defaults.cellSelector = '.enabled';

		$(document).ready( function($) {
			// Single Product page size attr
			// ==========
			

			var $sizeSelect 			= $('select#pa_size'),
				$sizeOptionBlocks 		= $('.size-blocks-wrap > .pdp-option-block'),
				$garmentSelect 			= $('select#pa_garment'),
				$garmentOptionBlocks 	= $('.garment-blocks-wrap > .pdp-option-block'),
				$form					= $('form.variations_form'),
				$sizeOptionBlocks 		= $('.size-blocks-wrap .pdp-option-block'),
				$garmentDetails 		= $('.garment-product-details'),
				$bulkPricingTable 		= $('#BulkPricingTablePDP'),
				$qtyInput 				= $('.pdp__meta-item--qty input[name="quantity"]'),
				$qtyPreviewCells 		= $('#BulkPricingTablePDP .price-tiers__preview-row .qty-preview'),
				$mainProdSlider 		= $('.product div.images .flickity.product-slider .slider'),
				$thumbProdSlider 		= $('.product div .flickity.product-thumbs .slider');

			var garmentData  			= $form.data('product_garments'),
				variationsData 			= $form.data('product_variations'),
				priceTiers 				= $form.data('price_tiers'),
				origCartQty 			= parseInt( $bulkPricingTable.data('orig-qty') ),
				// flickityMain 			= $mainProdSlider.data('flickity'),
				// flickityThumbs 			= $thumbProdSlider.data('flickity'),
				// garmentCountAvailable 	= parseInt( $('.pdp-option--pa_garment').data('options-count') ) || 0,
				selectedGarment 		= null,
				galleryGarment			= null;

			// var garmentCountTotal 		= Object.keys( garmentData ).length;

			// if( garmentCountTotal > 1 ) {
			// 	$('body').addClass('product-multiple-garments');
			// }


			window.$mainProdSlider = $mainProdSlider;
			window.$thumbProdSlider = $thumbProdSlider;
			

			if( omh.debug ) {
				console.log( 'omh.pdp INIT' );
				console.log( 'garmentData', garmentData );
				console.log( 'variationsData', variationsData );
				console.log( 'priceTiers', priceTiers );
			}

			// Init size blocks
			if( $sizeSelect.length ) {
				var $selectWrap = $sizeSelect.closest('td.value');
				$selectWrap.append('<div class="pdp-option-blocks size-blocks-wrap"></div>');
				var $blocksWrap = $selectWrap.find('.size-blocks-wrap');

				$sizeSelect.children('option').each(function() {
					var value = $(this).attr('value');
					var label = $(this).html();
					
					if( label != 'Choose an option') {
						$blocksWrap.append('<div class="pdp-option-block size-select-block" data-value="'+value+'" data-label="'+label+'">'+label+'</div>');	
					}
				});

				$sizeOptionBlocks 		= $('.size-blocks-wrap .pdp-option-block');
			}



			

			// Helper to get product garment object from product garment attribute slug
			function getProductGarmentBySlug( slug ) {

				var foundGarment = false;

				$.each( garmentData, function( index, garment ) {

					if( slug == garment.garment.style_slug ) {
						foundGarment = garment;
						return false;
					}
				});

				return foundGarment;
			}

			function getGarmentFirstAvailableSize( garment ) {



				var firstAvailableSize = null;

				$.each( garment.sizes, function( size, enabled ) {
					if( enabled ) {
						firstAvailableSize = size;
						return false;
					}
				});

				return firstAvailableSize;
			}

			function garmentHasSizeAvailable( garment, checkSize ) {

				checkSize = getSizeLabel( checkSize );
				var hasSizeAvailable = false;

				$.each( garment.sizes, function( size, enabled ) {
					if( size == checkSize && enabled ) {
						hasSizeAvailable = true;
						return false;
					}
				});

				return hasSizeAvailable;
			}

			function getSizeSlug( size ) {

				return 'size-' + size.replace( 'size-', '' ).toLowerCase();
			}

			function getSizeLabel( size ) {
				return size.replace( 'size-', '' ).toUpperCase();
			}

			// Sync bulk pricing Qty Preview
			// function updateCartQtyPreview() {
			// 	// $qtyPreviewAdd.text( $qtyInput.val() ).trigger('calcFields');


			// }
			// updateCartQtyPreview();
			// 
			// 
			// function getPreviewPrice() {
			// 	return parseFloat( $bulkPricingTable.find('.price-tiers__garment-row.selected-garment td.preview-tier').text() );
			// }

			// function updateVariationPrice() {
			// 	var $variationPrice = $('.woocommerce-variation-price .woocommerce-Price-amount');
			// 	var childrenHtml = $variationPrice.children().wrap('<div>').parent().html();
			// 	// $variationPrice.html( childrenHtml + getPreviewPrice() );
				
			// 	// Get preview variation price
				
			// }
			
			// Helper to get price tier based on a qty
			function getPriceTierAtQty( qty ) {
				// qty = ( typeof qty === 'undefined' ) ?   
				var tierIndex = 1;

				$.each( priceTiers, function(index, minQty) {
					if( qty >= minQty ) {
						tierIndex = index+1;
					}
				});

				return tierIndex;
			}

			function initializeQtyPreview() {

				var origTier = getPriceTierAtQty( origCartQty );

				$qtyPreviewCells.filter('[data-tier="'+origTier+'"]').children('.current-qty').text( origCartQty );

				$bulkPricingTable.find( '[data-tier="'+origTier+'"]' ).addClass( 'orig-tier' );
			}

			function updateQtyPreview() {

				var previewQtyTotal = parseInt( origCartQty ) + parseInt( $qtyInput.val() );

				var previewTier = getPriceTierAtQty( previewQtyTotal );

				
				$qtyPreviewCells.filter('[data-tier="'+previewTier+'"]').addClass('preview-tier').children('.preview-qty').text( previewQtyTotal );
				$bulkPricingTable.find('[data-tier]').removeClass('preview-tier').filter( '[data-tier="'+previewTier+'"]' ).addClass( 'preview-tier' );
				$qtyPreviewCells.not('.preview-tier').children('.preview-qty').empty();

				// updateVariationPrice();

			}
			// initializeQtyPreview();
			// updateQtyPreview();
			// $qtyInput.change(updateQtyPreview);


			// $form.on( 'found_variation', function(event, variation) {
				
				
			function updateGalleryGarment() {

				// Make sure to initialize the featured thumnbs				
				if(  $thumbProdSlider.find('.thumb:not([data-attachment-id])' ).length ) {

					$thumbProdSlider.find('.thumb:not([data-attachment-id])' ).attr('data-attachment-id', $form.data( 'product_featured_img_id' ) );
					
				}


				// Maybe update Gallery images
				if( !galleryGarment || selectedGarment.garment_style_id != galleryGarment.garment_style_id ) {

					// Pre-disable all gallery slides
					$mainProdSlider.find('.slide').removeClass('enabled');
					$thumbProdSlider.find('.thumb').removeClass('enabled');

					// var imgKeys = ['image_front', 'image_back', 'image_proof'];

					// for( var i = 0; i < imgKeys.length; i++) {

					// 	var imgKey = imgKeys[i];
					// 	if ( selectedGarment.hasOwnProperty[ imgKeys[i] ] 
					// 		&& selectedGarment[ imgKeys[i] ].hasOwnProperty('id')	
					// 	) {

					// 		var $imgThumb = $thumbProdSlider.find('.thumb[data-attachment-id="' + selectedGarment[ imgKeys[i] ].id + '"]').first();
					// 		$imgThumb.addClass('enabled');

					// 		$mainProdSlider.find('.slide').eq($imgThumb.index()).addClass('enabled');
					// 	}
					// }

					// Don't worry about garment.image_front 
					// because WC/Salient already change that one 
					// (hence the :not(:first-child) above)
					
					// Find the thumbs to enable, and then enable main slider
					// slides based on thumb indeces

					var selectedSlideIndex = false;

					if( selectedGarment.image_front.hasOwnProperty('id') ) {

						var $imgFrontThumb = $thumbProdSlider.find('.thumb[data-attachment-id="'+selectedGarment.image_front.id+'"]').first();
						$imgFrontThumb.addClass('enabled');

						selectedSlideIndex = selectedSlideIndex === false ? $imgFrontThumb.index() : selectedSlideIndex;
						
						$mainProdSlider.find('.slide').eq( $imgFrontThumb.index() ).addClass('enabled');
					}
					
					if( selectedGarment.image_back.hasOwnProperty('id') ) {

						var $imgBackThumb = $thumbProdSlider.find('.thumb[data-attachment-id="'+selectedGarment.image_back.id+'"]').first();
						$imgBackThumb.addClass('enabled');

						selectedSlideIndex = selectedSlideIndex === false ? $imgBackThumb.index() : selectedSlideIndex;
						
						$mainProdSlider.find('.slide').eq( $imgBackThumb.index() ).addClass('enabled');
					}

					

					if( selectedGarment.image_proof.hasOwnProperty('id') ) {

						var $imgProofThumb = $thumbProdSlider.find('.thumb[data-attachment-id="'+selectedGarment.image_proof.id+'"]').first();
						$imgProofThumb.addClass('enabled');

						selectedSlideIndex = selectedSlideIndex === false ? $imgProofThumb.index() : selectedSlideIndex;
						
						$mainProdSlider.find('.slide').eq( $imgProofThumb.index() ).addClass('enabled');
					}

					$thumbProdSlider.flickity('reloadCells');
					$mainProdSlider.flickity('reloadCells');
					
					$mainProdSlider.flickity('select', selectedSlideIndex, false, true );

					galleryGarment = selectedGarment;
				}
			}
			// });

			$form.on( 'show_variation', function( event, variation, purchasable ) {
				// updateVariationPrice();

				
				updateGalleryGarment();
				

				
			});

			// Sync PDP Options Blocks to select changes
			$('table.variations select').change(function() {
						

				// Set corresponding block
				$(this).closest('td.value').children('.pdp-option-blocks')
					.children().removeClass('selected')
					.filter('[data-value="'+$(this).val()+'"]').addClass('selected');

				
			}).each(function() {$(this).trigger('change')});

			
			// Handle UI changes based on garment selection
			$form.on('update_variation_values', function(event) {



				var garment = getProductGarmentBySlug( $garmentSelect.val() );


				// Hide unavailable sizes for this garment
				$sizeOptionBlocks.each(function() {

					var sizeEnabled = false,
						sizeLabel 	= $(this).data('label');

					if( sizeLabel && garment.sizes.hasOwnProperty( sizeLabel ) && garment.sizes[ sizeLabel ] ) {
						sizeEnabled = true;
					}
					
					$(this).toggle( sizeEnabled );
				});


				// Highlight selected garment in bulk pricing table
				$bulkPricingTable.find('tr.price-tiers__garment-row')
					.removeClass('selected-garment')
					.filter('[data-garment-style-id="'+garment.garment_style_id+'"]').addClass('selected-garment');

				selectedGarment = garment;
				// updateVariationPrice();
				updateGalleryGarment();
			});

			

			$form.on('check_variations', function(event) {
				
				var garmentSlug = $garmentSelect.val();

				var garment = getProductGarmentBySlug( garmentSlug );

				$garmentDetails.hide().filter('[data-garment-product-id="'+garment.garment.garment_product_id +'"]').show();

			});

			

			$garmentOptionBlocks.click(function() {
				// Determine first available size for this garment
				var garmentSlug = $(this).attr('data-value');

				var garment = getProductGarmentBySlug( garmentSlug );

				// Select first available size if current size doesn't match
				if( !garmentHasSizeAvailable( garment, $sizeSelect.val() ) ) {

					var firstAvailableSize = getGarmentFirstAvailableSize( garment );	

					$sizeSelect.val( getSizeSlug( firstAvailableSize ) ).trigger('change');
				}
				
				$garmentSelect.val( garmentSlug ).trigger('change');

				selectedGarment = garment;

				

				// $(this).closest('td.value').find('select').val( $(this).attr('data-value') ).trigger('change');
			});

			$sizeOptionBlocks.click(function() {
				
				$sizeSelect.val( $(this).attr('data-value') ).trigger('change');
				// $(this).closest('td.value').find('select').val( $(this).attr('data-value') ).trigger('change');
			});

			$('a.bulk-pricing-cta').click(function(event) {
				event.preventDefault();
				event.stopPropagation();
				$('#tab-title-bulk_pricing a').click();
			});



			// Make sure a variation is selected
			if( !$garmentSelect.val() ) {
				$garmentOptionBlocks.first().click();
			}
			
			

			// $sizeSelect.change(function() {
			// 	$blocksWrap
			// 		.children().removeClass('selected')
			// 		.filter('[data-value="'+$sizeSelect.val()+'"]').addClass('selected');
			// });

			// $blocksWrap.children().click(function() {
			// 	$sizeSelect.val( $(this).attr('data-value') ).trigger('change');
			// });

			// $sizeSelect.trigger('change');

		
		});
	}
})(jQuery);
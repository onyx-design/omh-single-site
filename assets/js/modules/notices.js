module.exports = (function($) {

	var iconMap = {
		primary		: 'check',
		secondary	: 'question-circle',
		success		: 'check',
		danger		: 'exclamation-triangle',
		warning		: 'exclamation-triangle',
		info		: 'info',
		light		: 'question-circle',
		dark		: 'question-circle'
	};

	function getNoticeLevel( notice ) {

		return _.get( notice, 'type', 'info' );
	}

	function getIconClass( notice ) {

		return ( 'fa-' + _.get( iconMap, getNoticeLevel( notice ), 'info' ) );
	}

	function getNoticePosition( notice ) {

		return ('fixed' == _.get(notice, 'position' ) ) ? 'position-fixed' : '';
	}

	function setNotice( notice ) {

		// Build notice
		var $notice = $('<div>', 
			{
				role: 'alert',
				html: _.get( notice, 'content', '' ),
				class: [
					'alert',
					'alert-' + getNoticeLevel(notice),
					getNoticePosition(notice)
				].join(' ')
			}
		)
		.prepend('<i class="fa ' + getIconClass(notice) + '"></i>')
		.append('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');

		var $keyedNotice = false;

		if( _.get( notice, 'key' ) ) {

			$notice.attr('data-notice-key', notice.key );
			$keyedNotice = $('.alert[data-notice-key="' + notice.key + '"]');
		}

		if( $keyedNotice && $keyedNotice.length ) {

			$keyedNotice.replaceWith( $notice );
		} 
		else {

			var noticeTarget = _.get(notice, 'target', 'mh-dashboard-notices');
			$('#'+noticeTarget).append($notice);
		}
	}

	function processNotices( response ) {

		if( omh.debug ) {
			console.log('processNotices', response);	
		}

		_.forEach( _.get(response, 'messages', {}), setNotice );
	}

	function removeNotice( key ) {
		
		$('.alert[data-notice-key="' + key + '"]').remove();
	}


	return {
		processNotices: processNotices,
		removeNotice: removeNotice,
		setNotice: setNotice
	};

})(jQuery);
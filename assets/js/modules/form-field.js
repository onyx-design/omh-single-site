
import OMHFormField 						from './form-fields/form-field-base.js';
import OMHFormFieldCheckbox 				from './form-fields/form-field-checkbox.js';
import OMHFormFieldFile 					from './form-fields/form-field-file.js';
import OMHFormFieldAjaxSearch 				from './form-fields/form-field-ajax-search.js';
import OMHFormFieldAjaxFile 				from './form-fields/form-field-ajax-file.js';
import OMHFormFieldSelect 					from './form-fields/form-field-select.js';
import OMHFormFieldSelect2 					from './form-fields/form-field-select2.js';
import OMHFormFieldDatePicker 				from './form-fields/form-field-datepicker.js';
import OMHFormFieldAdvanced 				from './form-fields/form-field-advanced.js';

// dev:improve - maybe these should be moved so they are only loaded on templates where they are needed?

import OMHFormFieldProductPriceTiers 		from './form-fields/form-field-product-price-tiers.js';
import OMHFormFieldProductPriceTier 		from './form-fields/form-field-product-price-tier.js';
import OMHFormFieldGarmentPriceTiers 		from './form-fields/form-field-garment-price-tiers.js';
import OMHFormFieldGarmentPriceTier 		from './form-fields/form-field-garment-price-tier.js';
import OMHFormFieldGarment 					from './form-fields/form-field-garment.js';
import OMHFormFieldGarmentsTable 			from './form-fields/form-field-garments-table.js';
import OMHFormFieldGarmentSizes 			from './form-fields/form-field-garment-sizes.js';

import OMHFormFieldInksoftProductResults 	from './form-fields/form-field-inksoft-product-results.js';
// import OMHFormFieldInksoftProduct			from './form-fields/form-field-inksoft-product.js';
// import OMHFormFieldInksoftProductStyle		from './form-fields/form-field-inksoft-product-style.js';


module.exports = (function($) {

	var fieldTypes = {
		base 					: OMHFormField,
		checkbox 				: OMHFormFieldCheckbox,
		file 					: OMHFormFieldFile,
		ajaxFile 				: OMHFormFieldAjaxFile,
		ajaxSearch 				: OMHFormFieldAjaxSearch,
		select 	 				: OMHFormFieldSelect,
		select2 				: OMHFormFieldSelect2,
		datePicker 				: OMHFormFieldDatePicker,
		advanced 				: OMHFormFieldAdvanced,
		productPriceTiers 		: OMHFormFieldProductPriceTiers,
		productPriceTier 		: OMHFormFieldProductPriceTier,
		garmentPriceTiers 		: OMHFormFieldGarmentPriceTiers,
		garmentPriceTier 		: OMHFormFieldGarmentPriceTier,
		garment 				: OMHFormFieldGarment,
		garmentsTable 			: OMHFormFieldGarmentsTable,
		garmentSizes 			: OMHFormFieldGarmentSizes,

		inksoftProductResults 	: OMHFormFieldInksoftProductResults,
		// inksoftProduct 			: OMHFormFieldInksoftProduct,
		// inksoftProductStyle 	: OMHFormFieldInksoftProductStyle,
	};

	function addFieldType( fieldType, fieldTypeConstructor, extendField ) {

		// Validate params
		if( _.isString( fieldType) && _.isFunction( fieldTypeConstructor ) ) {

			// Maybe extend the prototype
			if( _.has( fieldTypes, _.defaultTo( extendField, false ) ) ) {

				fieldTypeConstructor.prototype = $.extend({}, extendField.prototype, fieldTypeConstructor.prototype );
			}

			fieldTypes[ fieldType ] = fieldTypeConstructor;			
		}
	};

	$.fn.OMHFormField = function( parent, options ) {
		
		this.each(function() {

			// Determine field constructor; default to base
			var FieldConstructor = _.get( fieldTypes, $(this).data('omh-field-type'), fieldTypes.base );

			new FieldConstructor( this, parent, options );
		});
	};

	return {
		addFieldType: addFieldType
	};

})(jQuery);
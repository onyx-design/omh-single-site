module.exports = (function($) {

	var instances = [];

	function Modal(container) {

		var $modal = (this.$modal = $(container));
		var omhForm = (this.omhForm = _.get( $modal.find('.mh-form')[0], 'omhForm' ));

		// Set event handlers
		$modal.on( 'show.bs.modal shown.bs.modal', function () {
			$( '.modal-backdrop' ).appendTo( $modal.parent() );
		} );

		$modal[0].omhModal = this;

		return {
			$modal: $modal,
			omhForm: omhForm
		};
	}

	function initModals() {

		$('.modal.mh-modal').each(function() {
			instances.push( new Modal( $(this) ) );
		})
		.on('hide.bs.modal',function(event) {

			var $modal = $(event.target);

			if( $modal.length && $modal[0].omhModal.omhForm ) {
				$modal[0].omhModal.omhForm.access.reset();
			}
		});

		// Modal Forms event handling //dev:improve
		// $formModal.on('hide.bs.modal',reset);

		// Fix modal backdrop overlay placement
		// $(document).on('show.bs.modal shown.bs.modal', function() {
		// 	$( '.modal-backdrop' ).appendTo( '#mh-dashboard-content' );
		// });
	}
	$(document).ready(initModals);

	$(document).ready(function() {

		// Set generalized modal event handlers
		$(document).on('click', '[data-omh-toggle-modal]', function(event) {

			var $target = $(event.target);
			var $modal = $('#'+$target.data('omh-toggle-modal'));

			if( $modal.length ) {

				// Maybe preload modal form
				var preloadID = $target.data('omh-modal-load-id');

				if( preloadID && $modal[0].omhModal ) {

					$modal[0].omhModal.omhForm.access.loadData({id: preloadID});
				}

				$modal.modal();
			}
		});
	});

	return {
		instances: 	instances
	};
	
})(jQuery);
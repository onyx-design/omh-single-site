module.exports = (function($) {

	var instances = {};

	function getValue($target) {

		var value = null;

		if( $target.is('input') ) {
			value = parseFloat( $target.val() );
		}
		else {
			value = parseFloat( $target.text() );
		}

		return value;
	}

	function setValue(value, $target) {

		var currentValue = getValue($target);

		if( $target.is('input') ) {
			$target.val(value);
		}
		else {
			$target.text(value);
		}

		// Maybe trigger change event on $field
		if( currentValue != value ) {
			$target.trigger('change');
		}
	}

	function CalcField(field) {

		var $field = $(field),
			$children,
			childSelector,
			calcOperation;


		// function setValue(value, $target) {

		// 	var currentValue = getValue($target);

		// 	if( $target.is('input') ) {
		// 		$target.val(value);
		// 	}
		// 	else {
		// 		$target.text(value);
		// 	}

		// 	// Maybe trigger change event on $field
		// 	if( currentValue != value ) {
		// 		$target.trigger('change');
		// 	}
		// }

		// function getValue($target) {

		// 	var value = null;

		// 	if( $target.is('input') ) {
		// 		value = parseFloat( $target.val() );
		// 	}
		// 	else {
		// 		value = parseFloat( $target.text() );
		// 	}

		// 	if( $target.hasClass( 'price-val') ) {
		// 		value = parseFloat( value ).toFixed( 2 );
		// 	}

		// 	return value;
		// }

		function calcValue() {
			
			var newValue = 0;

			if( 'product' == calcOperation ) {

				newValue = 1;
			}
			
			$children.each(function() {

				var childValue = getValue($(this));

				if( _.isFinite( childValue ) ) {

					if( 'sum' == calcOperation ) {

						newValue += ('true' === $(this).attr('data-calc-negate') ? -childValue : childValue );
					}
					else if( 'product' == calcOperation ) {

						newValue *= childValue;
					}
				}
			});

			if( $field.hasClass('price-val') ) {
				newValue = parseFloat( newValue ).toFixed( 2 );
			}

			setValue(newValue, $field);
		}

		function init() {

			// Determine operation type
			if( childSelector = $field.attr('data-calc-sum') ) {

				calcOperation = 'sum';
				$children = $('[data-calc-sum-for="'+childSelector+'"]');
			}
			else if( childSelector = $field.attr('data-calc-product') ) {

				calcOperation = 'product';
				$children = $('[data-calc-product-for="'+childSelector+'"]');
			}

			if( $children.length ) {

				calcValue();
				$children.on('input change reset calcFields',calcValue);
			}

			
		}
		init();

		$field.data('calcField', this);

		return {
			field: $field,
			calcValue: calcValue,
			children: $children,
			childSelector: childSelector
		};
	}

	function initCalcFields() {

		$('[data-calc-sum],[data-calc-product]').each(function() {

			if( !$(this).data('calcField') ) {
				var calcAttr =
					$(this).attr('data-calc-sum') ?
					$(this).attr('data-calc-sum') :
					$(this).attr('data-calc-product');

				instances[calcAttr] = new CalcField($(this));
			}
		});
	}
	$(document).ready(initCalcFields);

	return {
		instances: instances,
		initCalcFields: initCalcFields,
		getValue: getValue,
		setValue: setValue
	};
})(jQuery);
import _ from '../vendor/lodash.min';
import '../vendor/jquery.ui.widget';
import '../vendor/jquery.fileupload';

module.exports = ( function($) {

	var instances = [],
		// Make sure we can do drag and drop with FormData
		canDragAndDrop = function() {
			var div = document.createElement('div');
			return (
					( 'draggable' in div ) 
					|| ( 'ondragstart' in div && 'ondrop' in div )
				) 
				&& 'FormData' in window && 'FileReader' in window;
		}(),
		selectors = {
			uploadingBox 	: '.mh-file-upload-box__uploading',
			successBox 		: '.mh-file-upload-box__success',
			errorBox 		: '.mh-file-upload-box__error'
		};

	function FileUpload( container ) {

		var self = this,
			$container = ( this.$container = $( container ) );

		var form_id = $container.attr( 'id' ),
			form_nonce = ( this.form_nonce = $container.data( 'form-nonce' ) );

		var submitting = false,
			droppedFiles = false;

		function uploadFiles( files ) {

			var formData = new FormData();

			_.forEach( files, function( file,name ) {
				formData.append( 'temporaryFile' + name, file );
			});

			formData.set( 'action', 'omh_temporary_file_upload' );
			formData.set( 'security', '' );

			$container.find( selectors.uploadingBox ).show();

			omh.ajax(
				'omh_temporary_file_upload',
				{},
				handleFileUpload,
				'',
				{
					data 		: formData,
					processData : false,
					contentType : false
				}
			);
		}

		function handleFileUpload( response ) {

			var $errorBox = $container.find( selectors.errorBox ),
				$successBox = $container.find( selectors.successBox ),
				$uploadingBox = $container.find( selectors.uploadingBox );

			$uploadingBox.hide();
			$errorBox.hide();
			$successBox.hide();

			if( 'success' === response.status ) {

				$successBox.show();

				$.each( response.data, function( index,value ) {
					$successBox.html( '<img src="' + value.thumbnail_src + '">' );
				});
			} else {

				$errorBox.show();
			}
		}

		function initFileUpload() {

			if( canDragAndDrop ) {

				$container.fileupload({
					url: ajax_object.ajax_url,
					dataType: 'json',
					add: function ( e, data ) {
						// data.context = $( '<p/>' ).text( 'Uploading...' ).appendTo( document.body );
						uploadFiles( data.files );
					},
					done: function( e, data ) {
						console.log( 'data',data );
					}
				});

				$container.addClass( 'has-advanced-upload' );

				$container.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
					e.preventDefault();
					e.stopPropagation();
				})
				.on('dragover dragenter', function() {
					$container.addClass('is-dragover');
				})
				.on('dragleave dragend drop', function() {
					$container.removeClass('is-dragover');
				})
				.on('drop', function(e) {

					uploadFiles( e.originalEvent.dataTransfer.files );
				});
			}
		}
		initFileUpload();

		return {
			droppedFiles: droppedFiles
		}
	}

	/**
	 * Attach event handlers
	 */
	function initTempUploadScreen() {

		$( '.mh-file-upload-box' ).each(function() {

			// $(this).fileupload();
			instances.push( new FileUpload( $( this ) ) );
		})
	}
	$( document ).ready( initTempUploadScreen );

	return {
		instances: 	instances,
	};
})(jQuery);
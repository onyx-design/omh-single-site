
module.exports = function( action, payload, callback, security, ajax_args ) {

	var ajax_data = {
		'action' 	: action,
		'security'	: security,
		'omh_ajax'	: _.defaultTo( payload, {} )
	};

	if( omh.debug ) {
		console.log('AJAX Data', ajax_data);
	}

	var ajaxStart = Date.now();

	ajax_args = _.assignIn({
		url			: ajax_object.ajax_url,
		type		: "POST",
		data 		: ajax_data,
		cache		: false,
		dataType	: "json",
		complete	: function( response, status ) {
			
			if( omh.debug ) {
				console.log('AJAX Request Duration', ( Date.now() - ajaxStart ) );
				console.log('Raw AJAX Response', response);
			}

			if( 'success' == _.get( response, 'responseJSON.status' ) ) {
				jQuery(document).trigger( action, response.responseJSON, response );
			}

			// Handle possible redirect from response
			if( _.get( response, 'responseJSON.redirect' ) ) {
				window.location = _.get( response, 'responseJSON.redirect' );
				// Do not continue processing response if redirecting
				return true;
			}

			// Handle possible table refresh from response //dev:generalize
			if( _.get( response, 'responseJSON.refresh_tables' ) ) {
				omh.Tables.refreshTables();
			}

			if( _.isArray( _.get( response, 'responseJSON.messages' ) ) ) {
				omh.Notices.processNotices( response.responseJSON );
			}

			if( _.isFunction( callback ) ) {
				callback( response.responseJSON );
			}
		}
	}, 
	_.defaultTo(ajax_args, {}));

	return jQuery.ajax( ajax_args );
};
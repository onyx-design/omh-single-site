module.exports = (function($) {

	var instances = [];

	function PriceWidget( container ) {

		// Vars
		// -=-=-=-=-

		var self = this;
		var $container = (this.$container = $(container));

		var widgetAnimationLength = 750,
			widgetElems = {
				priceInput: $('input#price'),
				basePriceInput: $('input#base_price'),
				earningsInput: $('input#earnings_per_unit'),
				basePriceWrap: $('.mhpw__base-price-wrap'),
				basePriceLabels: $('.mhpw__base-price-labels'),
				basePrice: $('.mhpw__base-price'),
				yourPrice: $('.mhpw__your-price'),
				profit: $('.mhpw__profit'),
				profitPercent: $('.mhpw__profit-percent')
			};
		

		// Helper Functions / Event Handlers
		// -=-=-=-=-

		function initDynamicNumber(elem, upperBound, format) {
			elem.dynamicNumber({
				from: 0,
				to: upperBound,
				duration: widgetAnimationLength,
				format: format,
				percentage: {
					decimals: 1
				},
				currency: {
					indicator: '$',
					decimals: '2',
					separator: ',',
					decimalsPoint: '.'
				}
			});
		}

		function validatePriceInput(event) {

			if ( event.key === '.' && widgetElems.priceInput.val().indexOf('.') > -1 ) { // Dont allow duplicate Periods
				event.preventDefault();
			} 
			else if ( event.key.length === 1 && /\D/.test(event.key) && event.key !== '.' ) { // Dont allow non digits/periods 
				event.preventDefault();
			}
		}

		function updateEarningsInput() {

			var yourPrice = widgetElems.priceInput.val(),
				basePrice = widgetElems.basePriceInput.val(),
				profit = parseFloat(yourPrice - basePrice).toFixed(2);

			widgetElems.earningsInput.val(profit);
			widgetElems.earningsInput.trigger('change');
		}

		function updatePriceWidget() {
			var yourPrice = widgetElems.priceInput.val(),
				basePrice = widgetElems.basePriceInput.val(),
				profit = widgetElems.earningsInput.val(),
				profitPercent = Math.round(profit / yourPrice * 100),
				newWidth = 100 - profitPercent < 100 ? (100 - profitPercent) : 100;

			if ( yourPrice.length > 0 ) {

				// Update widget numbers
				widgetElems.yourPrice.dynamicNumber('go', yourPrice);
				widgetElems.basePrice.dynamicNumber('go', basePrice);
				widgetElems.profit.dynamicNumber('go', profit);
				widgetElems.profitPercent.dynamicNumber('go', profitPercent + '%');

				// Animate the widget 
				widgetElems.basePriceWrap.animate({width: newWidth + '%'}, widgetAnimationLength);
				if (newWidth >= 89 ) {
					widgetElems.basePriceLabels.animate({right: (100 - 89) + '%'});
				} else {
					widgetElems.basePriceLabels.animate({right: 0});
				}

			}
		}

		// Init Price Widget
		// -=-=-=-=-

		function initPriceWidget() {
		
			updatePriceWidget(); // run this once on page load

			// Init Dynamic Numbers
			// -=-=-=-=-

			var mhInitials = {
				price: parseFloat( widgetElems.priceInput.val() ),
				base: parseFloat( widgetElems.basePriceInput.val() ),
			} // Add the rest after object initialized
			mhInitials.profit = mhInitials.price - mhInitials.base;
			mhInitials.percent = (mhInitials.profit / mhInitials.price) * 100;

			initDynamicNumber(widgetElems.yourPrice, mhInitials.price, 'currency');
			initDynamicNumber(widgetElems.basePrice, mhInitials.base, 'currency');
			initDynamicNumber(widgetElems.profit, mhInitials.profit, 'currency');
			initDynamicNumber(widgetElems.profitPercent, mhInitials.percent, 'percentage');

			$('.mhpw__base-price, .mhpw__your-price, .mhpw__profit, .mhpw__profit-percent').dynamicNumber('start');
		}
		initPriceWidget();



		// Attach Event Handlers
		// -=-=-=-=-

		widgetElems.priceInput.keyup(updateEarningsInput);
		widgetElems.priceInput.keydown(function(event) { validatePriceInput.call(this, event); });
		widgetElems.earningsInput.change(updatePriceWidget);		
	}

	function initPriceWidgets() {

		$('.mhpw').each(function() {

			instances.push( new PriceWidget( $(this) ) );
		});
	}

	$(document).ready(initPriceWidgets);

	return {
		instances: instances
	};
})(jQuery);
import _ from '../vendor/lodash.min';

module.exports = (function($) {

	var productOrderType 	= '',
		savingDesign 		= false,
		flashvars 			= {
			DesignerLocation: "https://images.inksoft.com/designer/html5",
			EnforceBoundaries: "1",
			Background: "",
			VectorOnly: false,
			DigitalPrint: true,
			ScreenPrint: true,
			Embroidery: false,
			MaxScreenPrintColors: "8",
			RoundPrices: false,
			StoreID: "143867", // "84643"
			PublisherID: "8526",
			SessionID: "",
			SessionToken: "",
			CartRetailItemID: "",
			UserID: "1034779",
			UserName: "Admin Merch House",
			UserEmail: "admin@merch.house",
			DesignID: "",
			DefaultProductID: "1000098", // "1000042"
			DefaultProductStyleID: "1001563", // "1001034"
			ProductID: "1000098", // "1000098"
			ProductStyleID: "1001563", // "1001561"
			ProductCategoryID: "1000034", // "1000003"
			ClipArtGalleryID: "",
			DisableAddToCart: true, //false
			DisableUploadImage: false,
			DisableClipArt: false,
			DisableUserArt: false,
			DisableProducts: false,
			DisableDesigns: true, //false
			DisableDistress: true, //false
			DisableResolutionMeter: false,
			DisableUploadVectorArt: false,
			DisableUploadRasterArt: false,
			StartPage: "",
			StartPageCategoryID: "",
			StartPageHTML: "",
			StartBanner: "",
			OrderID: "",
			CartID: "",
			ArtID: "",
			FontID: "",
			Domain: "stores.inksoft.com",
			SSLEnabled: true,
			SSLDomain: "stores.inksoft.com",
			StoreURI: "mhnational", // "merchhouse2"
			Admin: "",
			NextURL: "",
			CartURL: false, // "https://stores.inksoft.com/merchhouse2/Cart",
			OrderSummary: false, //true,
			VideoLink: false, // "http://www.youtube.com/watch?v=EfXICdRwt4E",
			Phone: "661-373-3501",
			WelcomeScreen: "",
			ContactUsLink: "https://merch.house/contact-us", //"/merchhouse2/Stores/Contact",
			WelcomeVideo: "",
			GreetBoxSetting: "",
			HelpVideoOverview: "",
			AutoZoom: true,
			EnableNameNumbers: false, //true,
			AddThisPublisherId: "xa-4fccb0966fef0ba7",
			EnableCartPricing: false,
			EnableCartCheckout: false,
			EnableCartBilling: false,
			EnableCartShipping: false, //true,
			PaymentDisabled: true, //false,
			PaymentRequired: false,
			BillingAddressRequired: false, //true,
			PasswordLength: "4",
			DefaultCountryCode: "US",
			CurrencyCode: "USD",
			CurrencySymbol: "$",
			HideProductPricing: true,
			PB: true,
			HideClipArtNames: true,
			HideDesignNames: true,
			ThemeName: "flat",
			FullScreen: false,
			Version: "3.37.0.0",
			BackgroundColor: "",
			StoreLogo: "//stores.inksoft.com/images/publishers/8526/stores/mhnational/img/logo.png",
			StoreName: "Merch House",
			StoreEmail: "admin@merch.house",
			EnableEZD: false,
			EmbedType: "div"
		};

	function designEmpty() {
		return state.selectedDesign.isEmpty();
	}

	function saveDesign(event, form, beforeSubmit) {

		savingDesign = true;

		$('.mh-create-product-form-btn').addClass('omh-saving-design').html('Saving Design...').prop('disabled',true);

		// If the design is empty, we don't have to submit anything to InkSoft
		if( designEmpty() ) {

			updateHiddenFormFields();
			omh.Forms.submit();
		} else {

			// Taken from ui.controllers.SaveDesignController
			var t = {
				designId: 		state.selectedDesign.design_id,
				productId: 		state.selectedProductID,
				productStyleId: state.selectedProductStyle.product_style_id,
				sessionId: 		state.currentSessionID,
				userId: 		state.activeUserID,
				name: 			$('.mh-create-product-wrapper .mh-form #product_name').val(),
				notes: 			$('.mh-create-product-wrapper .mh-form #comments').val(),
				guest_email: 	"",
				success: 		function(t, r) {
					
					state.selectedDesignID = state.selectedDesign.design_id = r.designID, 
					ajaxCallEnded("SaveDesignTemplate"), 
					state.designSaveResult = r, 
					//e.savingDesign = !1, 
					r.sessionID && (state.currentSessionID = parseInt(r.sessionID.trim(), 10)), 
					r.userID && (state.activeUserID = parseInt(r.userID, 10)), 
					r.sessionToken && (state.currentSessionToken = r.sessionToken.trim()), 
					
					updateHiddenFormFields();
					
					omh.Forms.submit();
					window.onbeforeunload = null
				},
				error: function(t) {
					window.onbeforeunload = null,
					ajaxCallEnded("SaveDesignTemplate"), alert("There was an unexpected error saving this design.  The error returned was: " + t)
				}
			};

			state.designer.saveDesign(t);
		}
	}

	function updateHiddenFormFields() {

		var image_url = typeof( state.selectedProductStyle.selectedRegion && state.selectedProductStyle.selectedRegion.imgurl ) !== "undefined"
			? "https:" + state.selectedProductStyle.selectedRegion.imgurl 
			: "https://stores.inksoft.com" + state.selectedProductStyle.image_file_path_front + "/500.png";

		// Remove unneeded data
		state.selectedProduct.product_styles = ['removed to save space'];
		state.selectedProduct.defaultStyle = ['removed to save space'];

		state.selectedProductStyle.product_regions = ['removed to save space'];

		// Sometimes the name is wrapped inside a function that must be called
		if( typeof state.selectedProduct.name.$$unwrapTrustedValue === "function" ) {
			state.selectedProduct.name = state.selectedProduct.name.$$unwrapTrustedValue();
		}

		$('.mh-create-product-wrapper .mh-form #product_id').val(state.selectedProduct.product_id);
		$('.mh-create-product-wrapper .mh-form #manufacturer').val(state.selectedProduct.manufacturer);
		$('.mh-create-product-wrapper .mh-form #manufacturer_sku').val(state.selectedProduct.manufacturer_sku);
		$('.mh-create-product-wrapper .mh-form #sku').val(state.selectedProduct.sku);
		$('.mh-create-product-wrapper .mh-form #name').val(state.selectedProduct.name);
		$('.mh-create-product-wrapper .mh-form #long_description').val(state.selectedProduct.long_description);
		$('.mh-create-product-wrapper .mh-form #product_style_id').val(state.selectedProductStyle.product_style_id);
		$('.mh-create-product-wrapper .mh-form #color').val(state.selectedProductStyle.color);
		$('.mh-create-product-wrapper .mh-form #sizes').val(state.selectedProductStyle.sizes);
		$('.mh-create-product-wrapper .mh-form #base_cost').val(state.selectedProductStyle.unit_price);
		$('.mh-create-product-wrapper .mh-form #image_url').val(image_url);
		$('.mh-create-product-wrapper .mh-form #selected_product').val(JSON.stringify(state.selectedProduct));
		$('.mh-create-product-wrapper .mh-form #selected_product_style').val(JSON.stringify(state.selectedProductStyle));

		$('.mh-create-product-wrapper .mh-form #design_id').val(state.selectedDesignID);
	}

	function initCreateProductScreen() {

		window.scrollTo(0,0);

		

		$('.mh-create-product-wrapper .mh-form').on('submit',function(e){

			e.preventDefault();
			e.stopPropagation();
		});

		$('.mh-create-product-wrapper .mh-create-product-form-btn').click(function(e){

			e.preventDefault();
			e.stopPropagation();

			if( !$('.mh-create-product-wrapper .mh-form #product_name').val() ) {

				$('.mh-create-product-wrapper .mh-form #product_name').focus();
			} else {

				if( !savingDesign ) {

					saveDesign();
				}
			}
		});

		if( $('.mh-create-product-wrapper').length ) {

			// Alias our Lodash since it will be replaced with the Design Studio's Lodash
			var lodash = window._,
			// Set interval to check for version and set to newest version if it's not the newest
				lodashVersionInterval = setInterval( function() {
					if( window._.VERSION.substring(0,1) != '4' ) {
						window._ = lodash;
						clearInterval(lodashVersionInterval);
					}
				}, 200);
			
			// code to trigger on AJAX form render
	 		launchDesigner('HTML5DS', flashvars, document.getElementById("embeddedDesigner") );

			// $('.mh-create-product-wrapper .mh-form #product_name').focus();
		}
	}

	$(document).ready(initCreateProductScreen);

	return {
		designEmpty : designEmpty,
		saveDesign 	: saveDesign,
	}
})(jQuery) ;

module.exports = (function($) {

	var instances = [];

	function Table( target ) {

		var $wrap = (this.$wrap = $(target));
		var table_type = (this.table_type = $wrap.data('table-type'));
		var query = (this.query = _.defaultTo($wrap.data('query'),{}));
		var pagination = (this.pagination = _.defaultTo($wrap.data('pagination'),{}));
		var scopes = (this.scopes = _.toArray( _.toString( $wrap.attr('data-force-scope') ).split(',') ));
		var filters = (this.filters = _.defaultTo($wrap.data('default-filters'),'{}'));

		var tooltipDefaults = {
			container: 'body',
			placement: 'bottom',
			trigger: 'hover',
			boundary: 'viewport',
			delay: {
				show: 400,
				hide: 100
			}
		};

		/**
		 * Process AJAX request responses
		 *
		 * @param      JSON  response  The AJAX response data
		 */
		function processRequest( response ) {

			var responseData = response.data;

			// Update local query object
			query = responseData.query || query;
			scopes = responseData.scopes || scopes;
			pagination = _.get( responseData, 'pagination', {});

			// Add/Replace html content sections
			if( _.has( responseData, 'sections' ) ) {

				// Mark all .omh-table-section elements with a class that will be removed if their section was in the AJAX results
				$('.omh-table-section', $wrap).addClass('section-to-hide');
		
				_.forEach( responseData.sections, function( value, section ) {

					$wrap.find('.omh-table-section-' + section).removeClass('section-to-hide').html(value).show();
				});

				// Empty and hide all sections still marked as not found in the results
				$('.omh-table-section.section-to-hide', $wrap).empty().hide().removeClass('section-to-hide');

				// Initialize tooltips and popovers
				$wrap.find('[data-toggle="tooltip"]:not([data-original-title])').tooltip(tooltipDefaults);
				$wrap.find('[data-toggle="popover"]:not([data-original-title])').popover(tooltipDefaults);
			}
			
			// Set loaded status classes
			$wrap.removeClass('not-loaded loading').addClass('loaded');

			$wrap.trigger( 'omh_table_request', {table: this, response: response});

			if( omh.debug ) {
				console.log( 'processRequest', response );	
			}
		}

		/**
		 * Gets the table data.
		 */
		function getData() {

			if( !$wrap.hasClass('table--static') ) {

				var ajax_data = {
					type		: table_type,
					query 		: query,
					pagination 	: pagination,
					scopes		: scopes,
					filters 	: filters
				};

				$wrap.addClass('loading');

				omh.ajax( 'omh_table_request', ajax_data, processRequest, window.table_security );	
			}
		}

		/**
		 * Sets the table scopes.
		 *
		 * @param      mixed [string,array]  scopes       The scope(s) to add
		 * @param      bool  clearScopes  Whether to clear existing scopes or not
		 */
		function setScopes( addScopes, clearScopes ) {

			var forceScope = $wrap.data('force-scope');

			// Reset sorting
			_.unset( query, 'order' );
			_.unset( query, 'orderby' );

			if( forceScope ) {
				scopes = [ forceScope ];
			}
			else {
	
				clearScopes = _.defaultTo( clearScopes, false );
	
				var newScopes = _.isArray( addScopes ) ? addScopes : [ addScopes ];
	
				scopes = 
					clearScopes ? 
					newScopes :
					_.union( scopes, newScopes );
			}

			_.set( pagination, 'paged', 1 );

			getData();
		}

		// Set Event Handlers
		$wrap
			.on('click','[data-set-scope]',function(e) {
				e.preventDefault();
				setScopes( 
					$(this).data('set-scope'), 
					$(this).data('clear-scopes') 
				);
			})
			.on('click', '.btn-thead', function() {
				var $th = $(this).closest('th');

				if( $th.hasClass( 'sortable' ) ) {

					if( $th.hasClass( 'sorted-desc' ) ) {
						// If sorted DESC, sort ASC
						query.orderby = $th.data('query-col');	
						query.order = 'ASC';
					}
					else if( $th.hasClass( 'sorted-asc' ) ) {
						// If sorted ASC, reset/remove sorting
						_.unset( query, 'order' );
						_.unset( query, 'orderby' );
					}
					else {
						// If not sorted, sort DESC
						query.orderby = $th.data('query-col');	
						query.order = 'DESC';
					}

					getData();
				}
			})
			.on('click', '.page-link', function(e) {
				e.preventDefault();
				if( $(this).data('go-to-page') ) {
					pagination.paged = $(this).data('go-to-page');
					getData();	
				}
				
				// setPagination( $(this).data('go-to-page') );
			})
			.on('click', '.omh-table-search-btn', function(e) {

				query.search = $wrap.find('.omh-table-search-input').val();
				getData();
			})
			.on('keypress', '.omh-table-search-input', function(e) {
				if( e.which === 13 ) {
					query.search = $wrap.find('.omh-table-search-input').val();
					getData();
				}
			})
			.on('click', 'tr[data-default-row-action]', function(e) {

				var $target = $(e.target);

				if( !$target.is('a') && !$target.hasClass('row-action') ) {
					
					var $row = $(this).closest('tr');					
					var defaultRowAction = $row.data('default-row-action');
					var $rowAction = $row.find('.row-action[data-action="' + defaultRowAction + '"]');

					if( $rowAction.length ) {
						$rowAction[0].click();
					}
				}
			})
			.on('omh_table_loaded', function(e) {
				
				// Initialize tooltips and popovers
				$wrap.find('[data-toggle="tooltip"]:not([data-original-title])').tooltip(tooltipDefaults);
				$wrap.find('[data-toggle="popover"]:not([data-original-title])').popover(tooltipDefaults);
			});

		// Inital data load
		if( !$wrap.hasClass('static-table') && $wrap.hasClass('not-loaded') ) {
			getData();	
		}
		else {
			$wrap.trigger('omh_table_loaded');
		}

		return {
			setScopes: setScopes,
			getData: getData,
			self: this
		};

	}

	function refreshTables() {

		_.forEach( instances, function(instance) {
			instance.getData();
		});
	}

	function initTable( target ) {

		instances.push( new Table( target ) );
	}

	function init() {

		$('.omh-table-wrap').each(function() {

			initTable( $(this) );
		});
	}

	$(document).ready(init);

	return {
		initTable: initTable,
		refreshTables: refreshTables,
		instances: instances
	};
	
})(jQuery);


module.exports = (function($) {

	var instances = [];

	// Freeze form fields before initialization
	function freezeUninitializedFields(event) {
		event.preventDefault();
		event.stopPropagation();
		$(this).blur();
	}
	$(document)
		// Freeze fields before initialization (removed after init)
		.on(
			'input click focus keydown keyup',
			'form.mh-form input, form.mh-form textarea, form.mh-form select', 
			freezeUninitializedFields
		)
		// Virtual disable fields (disable them but still allow contraint validation)
		.on(
			'input click focus keydown keyup',
			'[data-virtual-disabled]', 
			function(event) {event.preventDefault();}
		);

	function reset() {
		_.forEach(instances,function(form) {form.reset();});
	}

	function submit() {
		_.forEach(instances,function(form) {form.submit();});
	}	

	// Declare Form object (class)
	function OMHForm( container, options ) {

		var self = this,
			$container = (this.$container = $(container));

		// Default options and overrides
		var defaults = {
			ajax_action: 	 	'omh_' + $container.data('ajax-action'),
			allowSaveUnchanged: $container.hasClass('allow-save-unchanged'),
			defaultInvalidMsg: 'Please correct the errors below.',
			customInvalidMsg: null
		};

		var settings = this.settings = $.extend( true, {}, defaults, options);

		// Form attributes
		var form_id = $container.attr('id'),
			form_nonce = (this.form_nonce = $container.data('form-nonce')),
			form_model = (this.form_model = $container.data('omh-form-model'));

		var namespace = (this.namespace = form_id);
		
		// jQuery elements
		var $fields = (this.$fields = $('.omh-field-input[data-omh-form="'+form_id+'"]').add('.omh-field-input:not([data-omh-subfield-of])', $container)),
			$submitBtns = (this.$submitBtns = $('.btn[form="'+form_id+'"][value="submit"]')),
			$resetBtns = (this.$resetBtns = $('.btn[data-reset-form="'+form_id+'"]') ),
			$formModal = (this.$formModal = $container.closest('.modal')),
			$saveBar = (this.$saveBar = $('#mh-dashboard-save-bar'));

		var fields = (this.fields = {});

		var submitting = false;

		// function addField( field ) {
		// 	fields[ field.getName() ] = field;

		// 	if( !self.$fields.filter( field.$input ).length ) {

		// 		self.$fields = self.$fields.add(field.$input);
		// 	}
		// }

		function getField( name ) {
			return _.get( fields, name );
		}

		function setFieldValue( name, value ) {
			if( field = getField( name ) ) {
				field.setValue( value );
			}
		}

		function setFieldValues( fieldValues ) {
			_.forEach( fieldValues, function() {setFieldValue( value, name );});
		}

		function valuesHaveChanged() {
			return !_.every( _.invokeMap( self.fields, 'isOrigValue' ) )
			// return Boolean( self.$fields.filter('.changed-value').length );
		}

		function valuesAreValid() {
			return _.every( _.invokeMap( self.fields, 'isValid' ) );
			// return ( !Boolean( self.$fields.filter('.is-invalid').length ) );
		} 

		function validateFields() {
			_.invokeMap(fields,'validate');
		}

		function canSubmit() {
			return ( valuesAreValid() && ( valuesHaveChanged() || settings.allowSaveUnchanged ) );
		}

		function updateUI() {

			var formInvalidMsg = '';
			if( !valuesAreValid() ) {
				
				formInvalidMsg = settings.defaultInvalidMsg;

				var customInvalidMsgField = self.$fields.filter('.is-invalid[data-form-invalid-msg]'); //dev:omhff

				if( customInvalidMsgField.length ) {

					formInvalidMsg = customInvalidMsgField.data('form-invalid-msg');
				}
			}

			$('.mh-screen-form-msgs').html( formInvalidMsg );

			// Open the savebar drawer if values have changed and this is the screen form
			if( 'mh-screen-form' == form_id ) {
				
				$saveBar
					.toggleClass('onyx-drawer--open', valuesHaveChanged())
					.toggleClass('onyx-drawer--warning', !valuesAreValid());
			}

			// Enable reset buttons if values have changed
			$resetBtns.prop('disabled', !valuesHaveChanged());

			// Enable / Disable submit buttons
			$submitBtns.prop('disabled', !canSubmit())
				.filter('.submit-allow-valid').prop('disabled', !valuesAreValid());
		}

		function reset() {
			_.invokeMap(fields, 'reset');

			if( !$formModal.length ) { //dev:generalize //dev:config
				omh.Notices.removeNotice(settings.ajax_action);
			}
			
			updateUI();
			$container.trigger('omh_form_reset',this);
		}

		function clear() {
			_.invokeMap(fields,'clearValue');
			
			updateUI();
			$container.trigger('omh_form_clear',this);
		}

		function submit(submitAction, submitAllowValid, eventHooks) {

			// submitAllowValid flag means the form can be submitted without any values having been changed
			submitAction 		= _.defaultTo(submitAction, defaults.ajax_action);
			submitAllowValid 	= _.defaultTo(submitAllowValid, false);
			eventHooks 			= _.defaultTo(eventHooks, true);

			if( !submitting ) {

				validateFields();

				if( canSubmit() || (submitAllowValid && valuesAreValid()) ) {

					// Check if we should confirm the submit action
					var confirmSubmit = false;
					if( $container.data('confirm-msg') || $container.data('confirm-if') ) {
						
						confirmSubmit = true;

						// If data-confirm-if is present, check
						if( $container.data('confirm-if') ) {
							// confirm-if is in very simpley [field name]=[field value] format
							var confirmIf = $container.data('confirm-if').split('=');

							if( !_.has( fields, confirmIf[0] ) 
								|| confirmIf[1] != fields[ confirmIf[0] ].getValue()
							) {
								confirmSubmit = false;
							}
						}

						if( confirmSubmit ) {

							// Determine confirmation message
							var confirmMsg = _.defaultTo( $container.data('confirm-msg'), 'Click OK to confirm form submission' );

							if( !confirm( confirmMsg ) ) {
								return false;
							}	
						}
					}

					// Event hook before submit
					if( eventHooks ) {

						var beforeSubmit = {
							allowSubmit: true,
							submitAction: submitAction,
							submitAllowValid: submitAllowValid,
							eventHooks: eventHooks
						};

						$container.trigger('omh_form_before_submit', [self, beforeSubmit]);	

						// Allow event handlers to modify beforeSubmit (and cancel the submit)
						if( !beforeSubmit.allowSubmit ) {
							return false;
						}
					}

					// Commence form submit
					submitting = true;

					// Disable form buttons
					$resetBtns.prop('disabled', true);
					$submitBtns.prop('disabled', true);

					var $customActionSubmitBtns = $submitBtns.filter('[data-ajax-action="'+submitAction+'"]');

					if( $customActionSubmitBtns.length ) {
						$customActionSubmitBtns.addClass('cta-loading');
					}
					else {
						$submitBtns.filter(':not([data-ajax-action])').addClass('cta-loading');
					}

					var ajax_data = {},
						ajax_args = {};

					if( $container.data('submit-type') == 'form' ){

						var formData = new FormData(container[0]);

						_.forEach( fields, function(field, name) {

              				if( field.getValue() ) {
								if( 'file' == field.getType() ) {
									formData.set( name, field.getValue(), field.getFileName() );
								} else {
									formData.set( name, field.getValue() );
								}
							}
						});

						formData.set( 'action', _.defaultTo(submitAction,settings.ajax_action) );
						formData.set( 'security', form_nonce );
						formData.set( 'obj_id', $container.data( 'objectId' ) );

						ajax_args = {
							data 		: formData,
							processData	: false,
							contentType : false,
						};
					} else {

						ajax_data = {
							obj_id 		: $container.data( 'objectId' ),
							form_data 	: parseFields()
						};
					}	

					omh.ajax(
						_.defaultTo(submitAction,settings.ajax_action),
						ajax_data,
						processRequest,
						form_nonce,
						ajax_args
					);
				}
			}	
		}

		function parseFields() {

			var field_data = {};

			_.forEach( fields, function(field, name) {

				// var namePath = name.split('[')
				// Parse input arrays
				_.set( field_data, 
					_.join( 
						_.map( 
							name.split('['), 
							function(part) {

								var trimmedPart = _.trim(part, ']');
								return isNaN(trimmedPart) ? trimmedPart : "'" + trimmedPart + "'";
							} 
						), 
						'.'
					),
					field.getValue()
				);

				// field_data[name] = field.getValue();
			});


			return field_data;
		}

		function loadData(options) {

			var loadData_nonce = $('#omh_load_data_nonce').val();

			if( loadData_nonce ) {

				var loadData_model = _.get( options, 'model', form_model );

				omh.ajax(
					'omh_load_data',
					{
						model: loadData_model,
						id: options.id
					},
					processRequest,
					loadData_nonce
				);
			}
			else {
				omh.Notices.setNotice({
					key: 'omh_load_data_error',
					type: 'danger',
					content: 'Unable to load form data.'
				});
			}
		}

		function processRequest(response) {

			var responseData = response.data;

			form_nonce = _.get( responseData, 'form_nonce', form_nonce );

			// Emit event omh_process_form_request
			if( 'omh_load_data' != response.request.action ) {
				$container.trigger('omh_form_after_submit', [self, response]);	
			}
			

			// Update field values and add any error messages
			_.forEach( responseData, function( fieldResponse, fieldName ) {

				if( _.has( fields, fieldName ) ) {
					fields[ fieldName ].handleResponse( response );
				}
			});

			// Hide modal form on success //dev:improve
			if( $formModal.length > 0 && response.request.action !== 'omh_load_data' && $formModal.data('preserve-on-form-submit') !== true ) {
				$formModal.modal('hide');
			}

			submitting = false;

			$submitBtns.removeClass('cta-loading');

			updateUI();
		}

		// INITIALIZE

		// Initialize form field objects
		this.$fields.filter('.omh-field-input--load-first').OMHFormField( this );
		this.$fields.filter(':not(.omh-field-input--load-first)').OMHFormField( this );

		// Form level event handling //dev:improve //dev:generalize //dev:object structure
		var namespacedEvents = [
			'input.' + this.namespace,
			'change.' + this.namespace,
			'reset.' + this.namespace
		];

		$(document).on( namespacedEvents.join(' '), updateUI);

		$container
			.on('submit', function(event) {
				event.preventDefault();
			});

		// Maybe set custom submit text
		if( $container.data('custom-submit-label') ) {
			$submitBtns.not('.mh-forms-submit--custom').text( $container.data('custom-submit-label') );
		}

		// Initialize UI
		updateUI();

		// Submit button event handling
		$submitBtns.click(function(event) {
			event.preventDefault();
			if( !($(this).prop('disabled') || $(this).hasClass('disabled')) ) {
				submit( $(this).data('ajax-action'), $(this).hasClass('submit-allow-valid') );
			}
		});


		// Attach this object to the DOM
		$container[0].omhForm = this; // pretty sure this doesn't do anything...
		$container.get(0).omhForm = this;

		// function initForm() {

		// 	// this.$fields = $('.omh-form-field[data-omh-form="'+form_id+'"').add('.omh-form-field', $container);

		// 	// this.$fields.OMHFormField( this );

		// 	$container
		// 		// Initialize fields
		// 		// .find('input, select, textarea').filter('[name]').each(function() {
								
		// 		// 	fields[ $(this).attr('name') ] = new FormField( $(this), self );
		// 		// })
		// 		// Setup form-level event handlers
		// 		.on('input change reset', updateUI)
		// 		.on('submit', function(event) {
		// 			event.preventDefault();
		// 		});

		// 	// Modal Forms event handling //dev:improve
		// 	$formModal.on('hide.bs.modal',reset);

		// 	if( $container.data('custom-submit-label') ) {

		// 		$submitBtns.not('.mh-forms-submit--custom').text( $container.data('custom-submit-label') );
		// 	}

		// 	updateUI();

		// 	$submitBtns.click(function(event) {
		// 		event.preventDefault();

		// 		submit( $(this).data('ajax-action'), $(this).hasClass('submit-allow-valid') );
		// 	});
		// }
		// initForm();

		this.access = {
			fields: fields,
			// addField: addField,
			$container: $container,
			getField: getField,
			setFieldValue: setFieldValue,
			setFieldValues: setFieldValues,
			valuesAreValid: valuesAreValid,
			valuesHaveChanged: valuesHaveChanged,
			loadData: loadData,
			parseFields: parseFields,
			reset: 	reset,
			clear: clear,
			submit: submit
		};

		return this.access;
	}

	OMHForm.prototype = $.extend( {}, OMHForm.prototype, {
		addField: function( field ) {
			this.fields[ field.getName() ] = field;

			// if( !this.$fields.filter( field.$input ).length ) {

			// 	this.$fields = this.$fields.add(field.$input);
			// }
		},
		getField: function( name ) {
			return _.get( this.fields, name );
		},
		setFieldValue: function( name, value ) {
			var field = this.getField(name);
			if( field ) {
				field.setValue( value );
			}
		},
		setFieldValues: function( fieldValues ) {
			_.forEach( fieldValues, function() {this.setFieldValue( value, name );});
		}
	});

	function init() {

		$('form.mh-form').each(function() {

			instances.push( new OMHForm( $(this) ) );
		});

		// Unfreeze form fields
		$(document).off(
			'input click focus keydown keyup',
			'form.mh-form input, form.mh-form textarea, form.mh-form select', 
			freezeUninitializedFields
		);

		$(document).trigger('omh_forms_initialized');

		// $('.btn.input-reset-btn').click(function() {})

		// //dev:improve
		// $('.btn.remove-file').click(function() {$(this).children('input').click();});
	}

	$(document).ready(init);

	return {
		instances: 	instances,
		reset: 		reset,
		submit: 	submit
	};
	
})(jQuery);
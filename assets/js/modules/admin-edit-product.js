// import _ from '../vendor/lodash.min';

import OMHFormFieldPriceTiers from './form-fields/form-field-price-tiers.js';

module.exports = (function($) {


	var selectors = {
			container: '.screen--edit-product',
			addTier: '.price-tier__add-wrap',
			removeTier: '.price-tier__number:last-of-type .price-tier__remove',
			price: '.price-tier__price',
			quantity: '.price-tier__quantity',
			sizes: '.sizes-garment__sizes',
			highestTier: '.price-tier__number:last-of-type',
			quantityRow: '.price-tier__quantity-row',
			garmentSearch: '#add_garment_id',
			garmentAdd: '.garment__add-confirm',
			garmentSizeOptions: '.garment__size-option',
			newestVariation: '.garment-row:last-of-type'
		},
		// Hold the results of the current garment search
		garmentSearch;

	/**
	 * Attach event handlers
	 */
	function initAdminEditProductScreen() {
		var $container = $( selectors.container );

		// Price Tier events
		$container.on('click', selectors.addTier, addTier);
		$container.on('click', selectors.removeTier, removeTier);
		$container.on('change', selectors.quantity, handleTierQuantityChange);

		// Garment Events
		$container.on('input', selectors.garmentSizeOptions, handleGarmentSelect);
		$container.on('click', selectors.garmentAdd, handleGarmentAdd);

		$container.on('change', selectors.garmentSearch, handleGarmentSelect);
	}

	/**
	 * Set the min and max attributes on adjacent tiers based on the supplied tier's value
	 * 
	 * @param {int} tier - the tier whose neighbors we want to validate
	 */
	function setTierLimits(tier) {
		var tierValue = +$('input[name="price_tier_qty_' + tier + '"]').val();

		var $prevTier = $('input[name="price_tier_qty_' + (tier - 1) + '"]');
		var $nextTier = $('input[name="price_tier_qty_' + (tier + 1) + '"]');

		if ($prevTier.length) {
			$prevTier.attr('max', tierValue - 1);
		}
		if ($nextTier.length) {
			$nextTier.attr('min', tierValue + 1);
		}
	}

	/**
	 * 	When a tier qty changes, update the min and max attributes of adjacent tiers
	 */
	function handleTierQuantityChange(event) {
		var tier = $(event.target).parent().data('price-tier');
		setTierLimits(tier);
	}

	/**
	 * Remove the highest tier from the table
	 */
	function removeTier() {

		// Tiers Table
		// -=-=-=-=-=-=-=-=-=-=-=-=-=-

		// Remove header
		$(selectors.highestTier).remove();
		// Rmeove qty input
		$(selectors.quantityRow + ' td:last-child').remove();
		// Remove max on next highest tier
		$(selectors.quantityRow + ' td:last-child ').find(selectors.quantity).removeAttr('max');

		// Variations Teirs Tables
		// -=-=-=-=-=-=-=-=-=-=-=-=-=-

		$('.table--garment-base-cost .omh-table-row > *:last-child').remove();
	}

	/**
	 * 	Build out a new column for the table with a new highest tier
	 */
	function addTier() {
		var currentHighest = $(selectors.highestTier).data('price-tier'),
			newTier = currentHighest + 1,
			newTierQty = +$(selectors.quantity + '[name="price_tiers[' + currentHighest + ']"]').val() + 1;

		// Tiers Table
		// -=-=-=-=-=-=-=-=-=-=-=-=-=-
		
		// Build new column
		var $tierHeader = $(
				'<th class="text-center price-tier__number" data-price-tier="' + newTier + '">' + 
					newTier + 
					'<span class="price-tier__remove">&#215;</span>' +
				'</th>'
			),
			$tierQuantity = $(
				'<td class="text-center cell--input" data-price-tier="' + newTier + '">' +
					'<input class="form-control omh-table-input price-tier__new price-tier__quantity initialized was-validated is-valid changed-value" type="number" step="1" min="' + newTierQty + '" value="' + newTierQty + '" name="price_tiers[' + newTier + ']"></input>' +
				'</td>'
			);

		// Append new column
		$(selectors.highestTier).after($tierHeader);
		$(selectors.quantityRow).append($tierQuantity);

		// Validate Neighbor tiers
		setTierLimits(newTier);

		// Scroll the table over if necessary
		$('.table--price-quantity-tiers').parents('.omh-table-wrap-inner').scrollLeft( 10000 );

		// Variations Teirs Tables
		// -=-=-=-=-=-=-=-=-=-=-=-=-=-

		// dev:todo Update the keys for this name
		var $tierPriceCell = '<td class="text-center cell--input">' +
				'<input class="form-control omh-table-input omh-field-input price-tier__price never-input never-changed orig-value initialized was-validated is-valid" name="product_garments[-1][pricing][' + newTier + ']" type="number" min="0" step="0.01" value="10">'
			'</td>';

		$('.table--garment-base-cost tr:first-child').append( $('<th class="text-center">' + newTier + '</th>') );
		$('.table--garment-base-cost tr:last-child').append($tierPriceCell);

		initNewFormFields();
	}

	/**
	 * Check for an active garment selection to activate Add Garment button
	 */
	function handleGarmentSelect() {

		if( $(selectors.garmentSearch).val() ) {
			garmentSearch = $(selectors.garmentSearch).val();
			$(selectors.garmentAdd).attr('disabled', false);
		} else {
			garmentSearch = 0;
			$(selectors.garmentAdd).attr('disabled', true);
		}
	}

	/**
	 * 	Use the selected garment to build out a new garment row // TODO
	 */
	function handleGarmentAdd() {
		
		$(selectors.garmentAdd).attr('disabled', true);

		omh.ajax(
			'omh_get_product_garment',
			{ 
				garment_id: garmentSearch 
			},
			function( response ) {

				// Build the garment row
				buildGarmentRow(response.data);
				// Close the Modal
				$('#add_garment-modal').modal('hide');
				// Clear the Form
				$(selectors.garmentSearch)[0].omhFormField.reset();
				// $(selectors.garmentSearch)[0].omhFormField.clearSearchInput();
				$(selectors.garmentSearch).val('');
			},
			$('#omh_ajax_product_garment').val()
		);
	}

	/**
	 * Build a new garment row and append it to the table
	 * 
	 * @param {obj} garment - meta necessary to build out the row
	 */
	function buildGarmentRow(garment) {

		var baseCost = garment.base_cost / 100.00;		

		/**
		 * Build out the mini size input table
		 * 
		 * @param {obj} garment - 
		 */
		function buildSizes(garment) {
			var sizes = garment.sizes.split(','),
				sizeHtml = '';

			$(sizes).each(function(index, size) {
				var sizeInputSlug = garment.slug + '_size_' + size;

				var sizeInput = '<div class="form-check">' +
									'<label for="' + sizeInputSlug + '" class="form-check-label">' +
										size +
										'<input type="checkbox" class="form-check-input field--template sizes-garment__sizes" id="' + sizeInputSlug + '" name="product_garments[' + garment.ID + '][sizes][' + size + ']">' +
									'</label>' +
								'</div>';

				sizeHtml += sizeInput;
			});

			return sizeHtml;
		}

		/**
		 * Built out the price tiers table
		 * 
		 * @param {obj} garment
		 */
		function buildPriceTiers(garment) {
			var priceTiers = $('.price-tier__number'),
				tierHeaders = '',
				tierInputs = '';

			priceTiers.each(function(index, tier) {

				var tierNumber = $(tier).data('priceTier');

				tierHeaders += '<th class="text-center">' + tierNumber + '</th>';

				tierInputs += '<td class="text-center cell--input">' +
					'<input class="form-control omh-table-input field--template price-tier__price never-focused never-input never-changed orig-value initialized" name="product_garments[' + garment.ID + '][pricing][' + tierNumber + ']" type="number" min="0" step="0.01" value="' + baseCost + '">' +
				'</td>';
			});

			var tierHtml = '<div class="omh-table-wrap">' +
				'<table class="table omh-table table-equal-width table--garment-base-cost pricing-tiers pricing-tiers--price">' +
					'<tbody>' +
						'<tr class="omh-table-row">' +
							'<th>Tier</th>' +
							tierHeaders +
						'</tr>' +
						'<tr class="omh-table-row">' +
							'<th>Price</th>' +
							tierInputs +
						'</tr>' +
					'</tbody>' +
				'</table>' +
			'</div>';

			return tierHtml;
		}

		/**
		 * Clone the Garment Template Row
		 */
		var rowHtml = $( '.product-garments-table-wrap .omh-table-section-results > .garment__template-row' ).clone().removeClass( 'garment__template-row' ).addClass( 'garment-row' );

		rowHtml.find('[name=garment__removed]').attr('name','product_garments[' + garment.ID + '][delete]');
		rowHtml.find('[name=garment__new]').attr('name','product_garments[' + garment.ID + '][create]');

		// Add the built components to the cloned template
		rowHtml.find('.garment__price-tiers').append( buildPriceTiers(garment) );
		rowHtml.find('.garment__sizes').append( buildSizes(garment) );

		// Append the new row
		rowHtml.prependTo( '.product-garments-table-wrap .omh-table-section-results' );
		// $('.product-garments-table-wrap .omh-table-section-results > tr:last-child').before( $(rowHtml) );

		initNewFormFields();
	}

	function initNewFormFields() {

		var $container = $( selectors.container ),
			$omhForm = _.get( $container.find('.mh-form')[0], 'omhForm' );

		$('.product-garments-table-wrap .omh-table-section-results .omh-table-row:not(.garment__template-row) .field--template').removeClass('field--template').addClass('omh-field-input').OMHFormField( $omhForm );

		$('.price-tier__new').removeClass('price-tier__new').addClass('omh-field-input').OMHFormField( $omhForm );

		// $(selectors.quantity).OMHFormField( $omhForm );
		// $(selectors.price).OMHFormField( $omhForm );
		// $(selectors.sizes).OMHFormField( $omhForm );
	}

	// $(document).ready(initAdminEditProductScreen);

	return {
		setTierLimits: setTierLimits,
		handleTierQuantityChange: handleTierQuantityChange,
		removeTier: removeTier,
		addTier: addTier,
		buildGarmentRow: buildGarmentRow
	};
})(jQuery);

var $ = jQuery;

// import OMHFormFieldProductPriceTiers 	from './form-field-product-price-tiers.js';
import OMHFormFieldAdvanced 			from './form-field-advanced.js';
import OMHFormFieldGarmentPriceTier 	from './form-field-garment-price-tier.js';

// import OMHFormField 			from './form-field-base.js';

function OMHFormFieldGarmentPriceTiers( input, parent, options ) {

	this.initialized 		= false;

	options = _.defaultsDeep(options, {
		productPriceTiers: parent.settings.productPriceTiers, // Reference to productPriceTiers field
		validationClasses: false,
		subfieldsIsArray: true,
		tierValueIncrement: 0,
		tierIncrement: 0.01,
		namespace: 'pricing[' + parent.settings.garmentStyleId + ']',
		tiersType : 'garment',
		autoInitFields: false // Price Tiers are initted by the productPriceTiers advanced field
	});

	this.initialized = OMHFormFieldAdvanced.call( this, input, parent, options );

	$(document)
		.on('productPriceTierAdded', this.addGarmentPriceTier.bind(this))
		.on('productPriceTierUpdated', this.updateGarmentPriceTier.bind(this))
		.on('productPriceTierDeleted', this.deleteGarmentPriceTier.bind(this));


	return OMHFormFieldAdvanced.prototype.completeInit.call( this );
}

OMHFormFieldGarmentPriceTiers.prototype = _.create(OMHFormFieldAdvanced.prototype, {
	varsInit: function() {
		this.$headerRow = this.$wrap.find('.price-tiers__header-row');
		this.$valuesRow = this.$wrap.find('.price-tiers__values-row');

		this.templates = {
			tierHeader : _.template([
					'<th class="text-center price-tier__number" data-price-tier="<%= tierIndex %>">',
						'<%= tierIndex %>',
						'<span class="product-price-tier-qty"><%= productPriceTierQty %></span>',
					'</th>'
				].join("\n")
			),
			tierValue : _.template([
					'<td class="text-center cell--input omh-field-wrap" >',
							['<input',
								'class="form-control omh-table-input price-tier__quantity omh-field-input"',
								'data-omh-field-type="priceTier"',
								'data-omh-subfield-of="<%= parentField.namespace %>"',
								'data-price-tier="<%= tierIndex %>"',
								'name="price_tiers[<%= tierIndex %>]"',
								'type="number"',
								'min="0"',
								'step="<%= tierIncrement %>"',
								'value="<%= tierValue %>"',
								'/>'
							].join(' '),
					'</td>'
				].join("\n"),
				{imports: {parentField: this, tierIncrement: this.settings.tierIncrement}}
			)
		};

		// OMHFormFieldProductPriceTiers.prototype.varsInit.call( this );

		// this.templates.tierHeader = _.template([
		// 			'<th class="text-center price-tier__number" data-price-tier="<%= tierIndex %>">',
		// 				'<%= tierIndex %>',
		// 				'<span class="product-price-tier-qty"><%= productPriceTierQty %></span>',
		// 			'</th>'
		// 		].join("\n")
		// 	);
		

	},
	getProductPriceTiers: function() {
		return this.parent.getProductPriceTiers();
	},
	addGarmentPriceTier: function(event, priceTier, tierSettings) {

		var productPriceTiersInitialized = _.get( this.getProductPriceTiers(), 'initialized', false);
		var tierIndexZero = tierSettings.tierIndex - 1;

		// If the price tiers have not been intialized, then the new tier value
		// should be the highest tier value, otherwise it should be the tier's
		// preset value because this means we are still initializing the screen
		var garmentTierValue = productPriceTiersInitialized 
			? _.last( this.getValue() )
			: _.defaultTo( this.settings.presetValue[ tierIndexZero ], 20 );

		var garmentPriceTierOptions = {
			productPriceTierQty: priceTier.getValue(),
			tierIndex: tierSettings.tierIndex,
			tierValue: garmentTierValue,
			tiersType: 'garment'
		};

		new OMHFormFieldGarmentPriceTier( '', this, garmentPriceTierOptions );
	},
	updateGarmentPriceTier: function(event, priceTier) {
		var tierIndexZero = priceTier.settings.tierIndex - 1;

		this.fields[ tierIndexZero ].$tierMinQty.html( priceTier.getValue() );
	},
	deleteGarmentPriceTier: function(event, productPriceTier) {

		var garmentPriceTier = this.fields[ productPriceTier.settings.tierIndex - 1];

		garmentPriceTier.$wrap.remove();
		garmentPriceTier.$tierHeader.remove();


		_.remove( this.fields, garmentPriceTier );

		// this.$input.trigger({type:'change', namespace: this.getParentEventNamespace()});
	},
	initChildFields: function() {

		var productPriceTiers = this.getProductPriceTiers();

		if( productPriceTiers ) {

			var self = this;
			_.forEach( productPriceTiers.fields, function( priceTier, indexZero ) {
				self.addGarmentPriceTier( {}, priceTier, priceTier.settings );
			});
		}
		// var productPriceTiersInitialized = _.get( this.getProductPriceTiers(), 'initialized', false);

		// console.log( 'GarmentPriceTiers.initChildFields', this.settings );
		// var self = this;
		// _.map( this.settings.presetValue, function(tierValue, indexZero) {

		// 	var tierQuantity = _.get( self.settings.productPriceTiers, 'fields[indexZero].getValue()', null);

		// 	var priceTierOptions = {
		// 		productPriceTierQty: tierQuantity,
		// 		tierIndex: indexZero + 1,
		// 		tierValue: tierValue,
		// 		tiersType: self.settings.tiersType
		// 	};
		// 	new OMHFormFieldGarmentPriceTier( '', self, priceTierOptions );
		// });

		// this.recalcTiers();
	},
	deletePriceTier: function(priceTier, quietly) {
		quietly = _.defaultTo( quietly, false );

		priceTier.$wrap.remove();
		priceTier.$tierHeader.remove();


		_.remove( this.fields, priceTier );

		

		if( !quietly ) {
			this.recalcTiers();
			this.$input.trigger({type:'change', namespace: this.getParentEventNamespace()});	
		}
		
	},
	// Override the productPriceTiers function so it does nothing
	recalcTiers: function() {

	},
	reset: function() {

	}
	// reset: function( quietly ) {

	// 	if( this.initialized && this.origValue.length != this.fields.length ) {
	// 		// Clear the Price Tiers from the DOM
	// 		this.$headerRow.children('[data-price-tier]')
	// 			.add( this.$valuesRow.children('.omh-field-input') )
	// 			.remove();

	// 		this.fields = [];

	// 		this.setValue(this.origValue);

	// 		this.initChildFields();
	// 	}

	// 	OMHFormFieldAdvanced.prototype.reset.call( this );
		
	// 	// quietly = _.defaultTo( quietly, false );
		


	// 	// if( this.initialized ) {
	// 	// 	// Reset the input's value
	// 	// 	this.setValue(this.origValue);

	// 	// 	// Add or remove priceTiers if the current count of them does not 
	// 	// 	// match the original count
	// 	// 	while( this.origValue.length != this.fields.length ) {

	// 	// 		if( this.origValue.length < this.fields.length ) {

	// 	// 			this.addNewPriceTier( this.origValue[ this.fields.length ], this.origValue[ this.fields.length ], true );
	// 	// 		}
	// 	// 		else {

	// 	// 			this.deletePriceTier( this.fields[this.fields.length - 1], true );
	// 	// 		}
	// 	// 	}	
	// 	// }
		


	// 	// OMHFormFieldAdvanced.prototype.reset.call( this, true );

		



	// }
});

module.exports = OMHFormFieldGarmentPriceTiers;

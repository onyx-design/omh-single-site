
var $ = jQuery;

import OMHFormField from './form-field-base.js';

function OMHFormFieldAjaxSearch( input, parent, options ) {

	this.initialized = false;
	
	var settingsDefaults = {
		ajaxSearchMinLength: 2,
		ajaxSearchDebounce: 1000
	};

	options = $.extend({}, settingsDefaults, _.defaultTo(options, {}));

	this.initialized = OMHFormField.call( this, input, parent, options );

	var $searchResults 		= (this.$searchResults = this.$wrap.find('.ajax-search-results')),
		ajaxSearchModel 	= (this.ajaxSearchModel = this.$wrap.data('omh-ajax-search-model')),
		inputSearchTimeout 	= (this.inputSearchTimeout),
		inputSearchNonce 	= (this.inputSearchNonce = $('#omh_ajax_search_nonce').val());

	// Initialize event handler for search results items
	this.$wrap
		.on('click', '.ajax-search-result', this.selectResult.bind(this) )
		.on('clearValue', this.clearSearchInput.bind(this));

	this.$searchInput
		.on('input', this.ajaxSearchDebounce.bind(this))
		.on('focus', this.focusSearchInput.bind(this));

	return OMHFormFieldAjaxSearch.prototype.completeInit.call( this );
}

OMHFormFieldAjaxSearch.prototype = $.extend({}, OMHFormField.prototype, OMHFormFieldAjaxSearch.prototype, {
	varsInit: function() {

		this.$searchInput = this.$wrap.find('input.ajax-input-label');
		
		this.$fieldStateElems = this.$fieldStateElems.add(this.$searchInput);
	},
	focusSearchInput: function(event) {
		this.$searchInput.select();
	},
	clearSearchInput: function(event) {
		this.$searchInput.val(null);
	},
	ajaxSearchDebounce: function(event) {
		clearTimeout( this.inputSearchTimeout );

		this.$searchResults.empty();

		this.setValue(null);

		var searchTerm = event.target.value;

		if( searchTerm.length <= this.settings.ajaxSearchMinLength ) {
			return;
		}

		this.inputSearchTimeout = setTimeout(this.getAjaxSearchResults.bind(this), this.settings.ajaxSearchDebounce);
	},
	getAjaxSearchResults: function() {
		// this.$searchResults.empty();

		this.$wrap.addClass('ajax-loading');

		omh.ajax(
			'omh_ajax_search',
			{
				omh_model: this.ajaxSearchModel,
				search_term: this.$searchInput.val()
			},
			this.ajaxSearchCallback.bind(this),
			this.inputSearchNonce
		);
	},
	ajaxSearchCallback: function(responseData) {

		this.$searchResults.html(responseData.data.html_results);

		this.$wrap.removeClass('ajax-loading');
	},
	selectResult: function(event) {

		clearTimeout( this.inputSearchTimeout );

		this.$searchResults.empty();

		var $target = $(event.target);

		this.setValue($target.data('result-value'));

		this.$searchInput.val($target.data('result-label'));

		this.$input.trigger('change');
	},
	processResponseValue: function(response) {

		this.setValue( _.get( response, ['data', this.getName(), 'value'] ));

		this.origValue = this.getValue();

		// this.setValue( _.get( response, ['data', this.getName(), 'value'] ));
	}
});

module.exports = OMHFormFieldAjaxSearch;
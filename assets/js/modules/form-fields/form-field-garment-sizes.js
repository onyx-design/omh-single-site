
var $ = jQuery;

import OMHFormFieldAdvanced 	from './form-field-advanced.js';
import OMHFormFieldCheckbox 	from './form-field-checkbox.js';


function OMHFormFieldGarmentSizes( input, parent, options ) {

	this.initialized 		= false;

	options = _.defaultsDeep(options, {
		garmentStyle: null,
		// validationClasses: true,
		subfieldsIsArray: false,
		presetValue : _.get( parent.settings, 'presetValue.sizes', {}),
		namespace: 'sizes[' + parent.settings.garmentStyleId + ']'
	});

	this.initialized = OMHFormFieldAdvanced.call( this, input, parent, options );

	// this.initGarmentSizes();

	return OMHFormFieldGarmentSizes.prototype.completeInit.call(this);
}

OMHFormFieldGarmentSizes.prototype = _.create(OMHFormFieldAdvanced.prototype, {
	varsInit: function() {

		this.templates = {
			garmentSize : _.template([
				'<div class="form-check omh-field-wrap">',
					'<label for="<%= inputId %>" class="form-check-label"><%= sizeLabel %>',
						'<input type="checkbox" data-omh-field-type="checkbox" class="form-check-input field--template sizes-garment__sizes <%= disabled %>" id="<%= inputId %>" name="<%= sizeLabel %>" <%= disabled %> data-omh-subfield-of="<%= parentNamespace %>">',
					'</label>',
				'</div>'
			].join("\n"))
		};

		this.$invalidFeedback = _.defaultTo(this.$invalidFeedback, this.parent.$wrap.find('.invalid-feedback').eq(0));
		
		this.initGarmentSizes();
	},
	initGarmentSizes: function() {

		var self = this,
			wrapDisabled = this.$wrap.hasClass('disabled');

		_.forEach( this.settings.presetValue, function(sizeEnabled, sizeLabel) {

			var garmentSizeOptions = {
				sizeLabel: sizeLabel,
				inputId: 'garment-' + self.parent.settings.garmentStyleId + '-' + sizeLabel,
				// checked: sizeEnabled ? 'checked' : '',
				presetValue: sizeEnabled,
				disabled: wrapDisabled ? 'disabled' : '',
				parentNamespace: self.settings.namespace
			};

			// Only show enabled sizes if the wrap is disabled
			if( sizeEnabled || !wrapDisabled ) {
				self.$wrap.append(_.template(self.templates.garmentSize(garmentSizeOptions)));
			}
		});

		// this.recalcTiers();
	},
	isValid: function() {

		return ( !_.isEmpty( _.filter( this.getValue() ) ) || !_.get( this.parent, 'fields.garment__removed.currValue' ) );
	},
	validate: function(addClass) {
		if (_.defaultTo(addClass, true)) {
			this.$fieldStateElems.addClass('was-validated');
		}

		var isValid = this.isValid();


		this.setFeedbackMsg('You must enable at least one size.', 'invalid', 'default', !isValid);


		// if( !this.isValid() ) {
		// 	this.setFeed
		// }
		// var validationMessage = this.inputElem.validationMessage;

		// var isValid = this.isValid();

		// // Check for custom validation messages //dev:improve
		// if (!isValid && this.$input.attr('pattern') && this.$input.data('invalid-msg-pattern')) {
		// 	validationMessage = this.$input.data('invalid-msg-pattern');
		// }

		// // Maybe toggle state classes
		// if (this.settings.validationClasses) {
		// 	this.$fieldStateElems.toggleClass('is-valid', isValid)
		// 		.toggleClass('is-invalid', !isValid);
		// }


		// // Set (enable/disable) default feedback message //dev:improve
		// this.setFeedbackMsg(validationMessage, 'invalid', 'default', !isValid);
	}
});

module.exports = OMHFormFieldGarmentSizes;

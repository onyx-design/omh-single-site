
var $ = jQuery;

import OMHFormField from './form-field-base.js';

function OMHFormFieldSelect( input, parent, options ) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		select2: {
			width:'100%',
		}
	});

	this.initialized = OMHFormField.call( this, input, parent, options );

	this.isMultiple 		= _.get( this.settings.select2, 'multiple', false ); // dev:improve

	this.$input
		.on( 'select2:select', this._onSelect2Select.bind(this) )
		.on( 'change.select2', this._onChange.bind(this) )
		.on( 'select2:open', this._onFocus.bind(this) );

	this.$input.select2( this.settings.select2 );
	// Maybe preset value
	if( _.get( this.settings, 'presetValue', null ) ) {
		this.setValue(this.settings.presetValue, true);
	}

	return OMHFormFieldSelect.prototype.completeInit.call(this);
}

OMHFormFieldSelect.prototype = $.extend({}, OMHFormField.prototype, OMHFormFieldSelect.prototype, {
	_onSelect2Select: function(event) {
		if( _.get( this.settings.select2, 'multiple', false ) ) {

			var currValue = _.toArray( this.getValue() );

			currValue.push( event.params.data.id );

			this.setValue( currValue );
		}
		else {

			this.setValue(event.params.data.id);	
		}
		
		this.$input.trigger('change');
	},
	processResponseValue: function(response) {

		var value = _.get( response, ['data', this.getName(), 'value'] ),
			options = _.get( response, ['data', this.getName(), 'options'], false );

		if( options !== false ) {
			this.setOptions(options);
		}

		this.setValue( value, true);

		this.origValue = this.getValue();

		this.resetState();
	},
	isOrigValue: function() {
		if( !this.isMultiple ) {
			return _.isEqual( this.getValue(), this.origValue );
		}
		else {
			return _.isEqual( _.sortBy( this.getValue() ), _.sortBy( this.origValue ) );
		}
	},
	setOptions: function(options = {}) {
		console.log('setting options', options);
	}
});

module.exports = OMHFormFieldSelect;
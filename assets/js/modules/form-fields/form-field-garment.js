
var $ = jQuery;

import OMHFormFieldAdvanced 	from './form-field-advanced.js';
// import OMHFormFieldPriceTiers 	from './form-field-price-tiers.js';
// import OMHFormField 			from './form-field-base.js';

function OMHFormFieldGarment( input, parent, options ) {

	this.initialized 		= false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		productPriceTiers: null,
		createDom: true,
		tiersType: 'garment',
		garmentStyleId: null,
		garmentStyle: null,
		presetValue: null
	});

	options.garmentFullName = _.get( options.garmentStyle, 'style_full_name', '');

	

	this.initialized = OMHFormFieldAdvanced.call( this, input, parent, options );


	return OMHFormFieldGarment.prototype.completeInit.call(this);
}

OMHFormFieldGarment.prototype = _.create(OMHFormFieldAdvanced.prototype, {
	getProductPriceTiers: function() {
		return this.parent.getProductPriceTiers();
	},
	domInit: function() {
		this.settings.garmentNamespace = 'product_garments['+this.settings.garmentStyleId+']';
		// this.settings.garmentFullName = _.get( this.settings.garmentStyle, 'style_full_name' );
		// this.settings.garmentImgSrc = '//stores.inksoft.com/' + this.settings.garmentStyle.inksoft_image_url;

		// Optionally create the Price Tier's Dom nodes (th & td)
		if( this.settings.createDom ) {

			var templateClone = this.parent.templates.garmentRow(this.settings);

			$(templateClone).insertBefore(this.parent.$tableBody.children().last() );

			this.$input = this.parent.$tableBody.children('[name="'+this.settings.garmentStyleId+'"]');

			// Refresh / add Calc fields
			omh.CalcFields.initCalcFields();
		}


	},
	varsInit: function() {
		this.$removeGarment = this.$wrap.find('input.remove-garment').eq(0);
	},
	shouldDelete: function() {
		return Boolean( this.$removeGarment.prop('checked') );
	},
	completeInit: function() {

		this.$wrap.find('.omh-garment-name').html(this.settings.garmentFullName);
		// this.$wrap.find('.garment-blank-image').attr('src',this.settings.garmentImgSrc);
		this.$wrap.removeClass('row--template');

		return OMHFormFieldAdvanced.prototype.completeInit( this );
	}
});

module.exports = OMHFormFieldGarment;


var $ = jQuery;

import OMHFormField from './form-field-base.js';

function OMHFormFieldAjaxFile( input, parent, options ) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		
	});

	this.initialized = OMHFormField.call( this, input, parent, options );

	var self = this;
	this.$wrap.fileupload({
		filesContainer: this.$wrap.find('.mh-file-upload-box__file'),
		dropZone: this.$wrap.find('.mh-file-upload-box__file'),
		url: ajax_object.ajax_url,
		dataType: 'json',
		add: function ( e, data ) {
			e.preventDefault();
			self.uploadFile( data.files );
		}
	});

	return OMHFormFieldAjaxFile.prototype.completeInit.call(this);
}

OMHFormFieldAjaxFile.prototype = _.create(OMHFormField.prototype, {
	varsInit: function() {
		this.$uploading = this.$wrap.find('.mh-file-upload-box__uploading');
		this.$success = this.$wrap.find('.mh-file-upload-box__success');
		this.$error = this.$wrap.find('.mh-file-upload-box__error');

	},
	getValue: function() {
		return this.currValue;
	},
	isOrigValue: function() {
		return _.isEqual( this.origValue, this.currValue );
	},
	setValue: function(value, setOrigValue) {

		setOrigValue = _.defaultTo( setOrigValue, false);

		
		// this.$input.val(value);
		this.currValue = value;

		if( _.get( value, 'thumbnail_src') ) {
			this.$success.html('<img src="' + value.thumbnail_src + '">').show();	
		}
		else {
			this.$success.html('').hide();
		}
		
		if( setOrigValue ) {
			this.origValue = value;
		}
		return value;
	},
	uploadFile: function( files ) {
		var formData = new FormData();

		_.forEach( files, function( file,name ) {
			formData.append( 'temporaryFile' + name, file );
		});

		formData.set( 'action', 'omh_temporary_file_upload' );
		formData.set( 'security', '' );

		this.$uploading.show();

		omh.ajax(
			'omh_temporary_file_upload',
			{},
			this.processTempUploadResponse.bind(this),
			'',
			{
				data 		: formData,
				processData : false,
				contentType : false,
				// progress : function(param) {
				// 	console.log( 'PROGRESS', param);
				// },
				// beforeSend 	: function(xhr) {
				// 	console.log(x)
				// 	xhr.progress((evt) => {
				// 		var percentComplete = Math.round((evt.loaded * 100) / evt.total);
				// 		console.log('UPLOADING: ' + percentComplete);
				// 	});
					
				// }
			}
		);
	},
	processTempUploadResponse: function( response ) {
		var self = this,
			$uploadingBox = this.$uploading,
			$successBox = this.$success,
			$errorBox = this.$error,
			uploadedFile = response.data.temporaryFile0;

		$uploadingBox.hide();
		$errorBox.hide();
		$successBox.hide();

		uploadedFile.id = uploadedFile.id ? _.toInteger( uploadedFile.id ) : uploadedFile.id;

		if( 'success' === response.status ) {

			// $successBox.show();

			// $successBox.html('<img src="' + uploadedFile.thumbnail_src + '">');
			this.setValue(uploadedFile);

			this.$wrap
				.trigger('input')
				.trigger({type:'input', namespace: this.getParentEventNamespace()})
				.trigger('change')
				.trigger({type:'change', namespace: this.getParentEventNamespace()});
		} else {

			$errorBox.show();
		}

		
	}
});

module.exports = OMHFormFieldAjaxFile;

var $ = jQuery;

import OMHFormField from './form-field-base.js';

function OMHFormFieldProductPriceTier( input, parent, options ) {
	
	this.initialized = false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		createDom: true,
		tierIndex: 1,
		tierValue: 1,
		tiersType: 'product'
	});

	this.initialized = OMHFormField.call( this, input, parent, options );

	this.$deleteBtn = this.$tierHeader.find('.price-tier__remove');

	this.$deleteBtn.click(this.deletePriceTier.bind(this));

	this.$input.trigger( 'productPriceTierAdded', [this, this.settings] );

	return OMHFormFieldProductPriceTier.prototype.completeInit.call(this);
}


OMHFormFieldProductPriceTier.prototype = _.create(OMHFormField.prototype, {

	domInit: function() {

		// Optionally create the Price Tier's Dom nodes (th & td)
		if( this.settings.createDom ) {

			this.parent.$headerRow.append( 
				this.parent.templates.tierHeader(this.settings)
				
			);

			this.parent.$valuesRow.append(
				this.parent.templates.tierValue(this.settings)
			);
		}
	},
	varsInit: function() {
		this.$input = this.parent.$valuesRow.find('[data-price-tier="'+this.settings.tierIndex+'"]');
		this.$wrap = this.$input.parent();
		this.$tierHeader = this.parent.$headerRow.find('[data-price-tier="'+this.settings.tierIndex+'"]');
	},
	getValue: function() {
		return parseInt( this.$input.val() );
	},
	_onChange: function(event) {

		OMHFormField.prototype._onChange.call( this, event );

		this.$input.trigger( 'productPriceTierUpdated', [this]);

		this.parent.recalcTiers();
	},
	isHighestTier: function() {
		return ( this.settings.tierIndex == this.parent.fields.length );
	},
	deletePriceTier: function() {
		if( this.isHighestTier() ) {
			this.parent.deletePriceTier(this);
		}
	}

});

module.exports = OMHFormFieldProductPriceTier;

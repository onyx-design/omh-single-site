
var $ = jQuery;

import OMHFormField from './form-field-base.js';

function OMHFormFieldCheckbox( input, parent, options ) {
	this.initialized = false;

	options = _.defaultsDeep(options, {
		presetValue: false
	});

	return OMHFormField.call( this, input, parent, options );
}

OMHFormFieldCheckbox.prototype = $.extend({}, OMHFormField.prototype, OMHFormFieldCheckbox.prototype, {
	getValue: function() {
		return this.$input.prop('checked');
	},
	setValue: function(value, setOrigValue) {
		setOrigValue = _.defaultTo( setOrigValue, false);
		this.$input.prop('checked', Boolean(value));
		this.currValue = value;
		if( setOrigValue ) {
			this.origValue = value;
		}

		return value;
	}
});

module.exports = OMHFormFieldCheckbox;

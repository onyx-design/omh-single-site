
var $ = jQuery;

import OMHFormFieldAdvanced 	from './form-field-advanced.js';
import OMHFormFieldGarment 		from './form-field-garment.js';


function OMHFormFieldGarmentsTable( input, parent, options ) {

	this.initialized 		= false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		rawGarmentsData: _.get( window, 'omhProductData.garmentData', {} ),
		presetValue : _.get( window, 'omhProductData.garmentData', {} ),
		pruneFieldsOnSetValue: true
	});

	this.initialized = OMHFormFieldAdvanced.call( this, input, parent, options );

	this.productPriceTiers = _.get( this.parent, 'fields.product_price_tiers', null);

	this.origValue = this.settings.presetValue;

	// this.$addPriceTierBtn = this.$wrap.find( '.price-tier__add-btn' ).eq(0);

	// this.$addPriceTierBtn.click( this.addNewPriceTier.bind(this) );

	return OMHFormFieldGarmentsTable.prototype.completeInit.call(this);
	// _.invoke( this, 'completeInit' );
}

OMHFormFieldGarmentsTable.prototype = _.create(OMHFormFieldAdvanced.prototype, {
	getProductPriceTiers: function() {
		return _.get( this.parent, 'fields.product_price_tiers', null);
	},
	varsInit: function() {
		this.$tableBody 		= this.$wrap.find('.omh-table-section-results');
		this.$garmentTemplate 	= this.$tableBody.children('.garment__template-row');

		// Prepare presetValue
		var self = this;
		_.forEach( this.settings.presetValue, function(presetGarment, presetGarmentId) {
			_.unset( self.settings.presetValue[ presetGarmentId ], 'garment' );
			self.settings.presetValue[ presetGarmentId ].featured = 'false' == presetGarment.featured ? false : Boolean( presetGarment.featured );
			_.map( self.settings.presetValue[ presetGarmentId ].pricing, parseFloat);
			
		});

		this.templates = {
			garmentRow : _.template(
				this.$garmentTemplate.prop('outerHTML'),
				{imports: {parentField: this }}
			)
		};

	},
	getValue: function() {
		var garmentMeta = {};

		_.forEach(this.fields, function(garment, garmentStyleId) {
			if( !garment.shouldDelete() ) {
				garmentMeta[ garmentStyleId ] = garment.getValue();
			}
		});

		return garmentMeta;
	},
	isOrigValue: function() {
		// Because of the added garment__removed field, 
		// just check if each of the garment rows is OrigValue
		return _.every( _.invokeMap( this.fields, 'isOrigValue' ) );
	},
	initChildFields: function() {

		var self = this;
		_.forEach( this.settings.rawGarmentsData, function(garmentData, garmentStyleId) {

			// garmentData.create = false;
			// garmentData.delete = false;
			// garmentData.featured = 'false' == garmentData.featured ? false : Boolean( garmentData.featured );

			var garmentOptions = {
				garmentStyleId: garmentData.garment_style_id,
				productPriceTiers: self.productPriceTiers,
				presetValue: self.settings.presetValue[ garmentStyleId ],
				garmentStyle: garmentData.garment
			};

			// Remove garment style global data from presetValue
			_.unset( garmentOptions.presetValue, 'garment' );

			new OMHFormFieldGarment( '', self, garmentOptions );
		});

		// this.recalcTiers();
	},
	addNewGarment: function(garmentStyle) {

		// Check that this garment is not already on the product
		if( _.has( this.getValue(), garmentStyle.id ) ) {

			omh.Notices.setNotice({
				type: 'warning',
				content: 'This product already has Garment ID ' + garmentStyle.id,
				key: 'duplicate_product_garment_style'
			});
		}
		else {
			let array = []
			if (this.getProductPriceTiers()) {
				array = Array(this.getProductPriceTiers().fields.length)
			}

			var garmentOptions = {
				garmentStyleId : garmentStyle.id,
				garmentStyle: garmentStyle.garment,
				productPriceTiers: this.productPriceTiers,
				presetValue: {
					garmentStyleId:garmentStyle.id,
					image_front: null,
					image_back: null,
					image_proof: null,
					featured: false,
					pricing: _.fill( array, 20.00 ),
					base_price: _.get( garmentStyle, 'style_base_cost', 10 ),
					retail_price: _.get( garmentStyle, 'style_base_cost', 10 ),
					sizes: {}
				}
			};

			garmentOptions.garmentStyle.id = garmentStyle.id;

			// Initialize sizes
			garmentOptions.garmentStyle.sizes = garmentOptions.garmentStyle.sizes.split(',');

			garmentOptions.presetValue.sizes = _.zipObject( garmentOptions.garmentStyle.sizes, false);

			var newGarment = new OMHFormFieldGarment( '', this, garmentOptions );
			
			if (newGarment.fields.pricing) {
				newGarment.fields.pricing.initChildFields();
			}
			
			this.$input.trigger({type:'change', namespace: this.getParentEventNamespace()});

		}

		
	},

	// setValue: function(value, setOrigValue) {
	// 	setOrigValue = _.defaultTo( setOrigValue, false);

	// 	// var self = this;
	// 	// _.forEach( this.currValue, function(garment, garmentId) {
	// 	// 	if( _.get( garment.fields, 'delete' ) && !_.has( value, garmentId ) ) {
	// 	// 		garment.$wrap.remove();
	// 	// 		_.unset( self.currValue, garmentId );
	// 	// 	}
	// 	// });

	// 	OMHFormFieldAdvanced.prototype.setValue.call( this, value, setOrigValue );


	// },
	// addProductPriceTier: function(priceTier, quietly) {


	// }
	// completeInit: function() {
	// 	this.initialized = true;


	// 	return false;
	// }
});


module.exports = OMHFormFieldGarmentsTable;

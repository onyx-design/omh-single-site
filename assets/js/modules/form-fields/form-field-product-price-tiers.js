
var $ = jQuery;

import OMHFormFieldAdvanced 			from './form-field-advanced.js';
import OMHFormFieldProductPriceTier 	from './form-field-product-price-tier.js';


function OMHFormFieldProductPriceTiers( input, parent, options ) {

	this.initialized 		= false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		subfieldsIsArray: true,
		tierIncrement: 1,
		presetValue : _.get( window, 'omhProductData.priceTiers', [] ),
		tiersType : 'product'
	});


	this.initialized = OMHFormFieldAdvanced.call( this, input, parent, options );

	this.$addPriceTierBtn = this.$wrap.find( '.price-tier__add-btn' ).eq(0);

	this.$addPriceTierBtn.click( this.addNewPriceTier.bind(this) );

	return OMHFormFieldProductPriceTiers.prototype.completeInit.call(this);
}

OMHFormFieldProductPriceTiers.prototype = _.create(OMHFormFieldAdvanced.prototype, {
	varsInit: function() {
		this.$headerRow = this.$wrap.find('.price-tiers__header-row');
		this.$valuesRow = this.$wrap.find('.price-tiers__values-row');

		this.templates = {
			tierHeader : _.template([
					'<th class="text-center price-tier__number" data-price-tier="<%= tierIndex %>">',
						'<%= tierIndex %>',
						'<span class="price-tier__remove">&#215;</span>',
					'</th>'
				].join("\n")
			),
			tierValue : _.template([
					'<td class="text-center cell--input omh-field-wrap" >',
							['<input',
								'class="form-control omh-table-input price-tier__quantity omh-field-input"',
								'data-omh-field-type="priceTier"',
								'data-omh-subfield-of="<%= parentField.namespace %>"',
								'data-price-tier="<%= tierIndex %>"',
								'name="price_tiers[<%= tierIndex %>]"',
								'type="number"',
								'min="0"',
								'step="<%= tierIncrement %>"',
								'value="<%= tierValue %>"',
								'/>'
							].join(' '),
					'</td>'
				].join("\n"),
				{imports: {parentField: this, tierIncrement: this.settings.tierIncrement}}
			)
		};

	},
	initChildFields: function() {

		var self = this;
		_.map( this.settings.presetValue, function(tierValue, indexZero) {

			var priceTierOptions = {
				tierIndex: indexZero + 1,
				tierValue: tierValue,
				tiersType: self.settings.tiersType
			};
			new OMHFormFieldProductPriceTier( '', self, priceTierOptions );
		});

		this.recalcTiers();
	},
	recalcTiers: function() {

		var fields 			= this.fields,
			tierIncrement 	= this.settings.tierIncrement,
			tierIndex 		= 0,
			tierValueLast 	= 0,
			nextPriceTier 	= null;

		_.forEach( fields, function( priceTier, indexZero ) {


			nextPriceTier = _.get( fields, (indexZero + tierIncrement), null)
			
			var tierValueMax = nextPriceTier ? ( nextPriceTier.getValue() - tierIncrement ) : null;
			var tierValueMin = tierValueLast + tierIncrement;
			
			// Double check to force min & max
			if( priceTier.getValue() < tierValueMin ) {
				priceTier.setValue(tierValueMin);	
			}

			if( tierValueMax && priceTier.getValue() > tierValueMax ) {
				priceTier.setValue(tierValueMax);	
			}

			priceTier.$input.attr({
				min: tierValueMin,
				max: tierValueMax
			});

			tierValueLast = priceTier.getValue();

		});
	},
	addNewPriceTier: function( event, tierValue, quietly ) {
		quietly = _.defaultTo( quietly, false );

		tierValue = _.defaultTo( tierValue, (_.last(this.fields).getValue() + 1) );

		var priceTierOptions = {
			tierIndex: ( this.fields.length + 1 ),
			tierValue: tierValue,
			tiersType: this.settings.tiersType
		};
		
		new OMHFormFieldProductPriceTier( '', this, priceTierOptions );

		if( !quietly ) {
			this.recalcTiers();
			this.$input.trigger({type:'change', namespace: this.getParentEventNamespace()});	
		}
	},
	deletePriceTier: function(priceTier, quietly) {
		quietly = _.defaultTo( quietly, false );

		priceTier.$wrap.remove();
		priceTier.$tierHeader.remove();

		$(document).trigger('productPriceTierDeleted', [priceTier]);

		_.remove( this.fields, priceTier );
		

		if( !quietly ) {
			this.recalcTiers();
			this.$input.trigger({type:'change', namespace: this.getParentEventNamespace()});	
		}
		
	},
	deleteTierByIndex: function(tierIndex, quietly) {
		this.deletePriceTier( this.fields[ tierIndex - 1], quietly );
	},
	reset: function( quietly ) {

		if( this.initialized && this.origValue.length != this.fields.length ) {

			this.initialized = false;

			// Clear the Price Tiers from the DOM
			this.$headerRow.children('[data-price-tier]')
				.add( this.$valuesRow.children('.omh-field-wrap') )
				.remove();

			// Clear all Garments' Price Tiers
			this.parent.fields.product_garments.$wrap.find('.garment__price-tiers').find( 'th.price-tier__number, td.omh-field-wrap').remove();

			_.forEach( this.parent.fields.product_garments.fields, function(productGarment) {
				productGarment.fields.pricing.fields = [];
			});



			this.fields = [];

			// this.setValue(this.origValue);

			this.initChildFields();

			this.initialized = true;

			// this.setValue(this.origValue, true);
		}

		OMHFormFieldAdvanced.prototype.reset.call( this );
		
		// quietly = _.defaultTo( quietly, false );
		


		// if( this.initialized ) {
		// 	// Reset the input's value
		// 	this.setValue(this.origValue);

		// 	// Add or remove priceTiers if the current count of them does not 
		// 	// match the original count
		// 	while( this.origValue.length != this.fields.length ) {

		// 		if( this.origValue.length < this.fields.length ) {

		// 			this.addNewPriceTier( this.origValue[ this.fields.length ], this.origValue[ this.fields.length ], true );
		// 		}
		// 		else {

		// 			this.deletePriceTier( this.fields[this.fields.length - 1], true );
		// 		}
		// 	}	
		// }
		


		// OMHFormFieldAdvanced.prototype.reset.call( this, true );

		



	}
});

module.exports = OMHFormFieldProductPriceTiers;


var $ = jQuery;

import OMHFormFieldAdvanced 			from './form-field-advanced.js';

function OMHFormFieldInksoftProduct( input, parent, options ) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		createDom: true,
		inksoftProductId: null,
		inksoftProduct: null,
		presetValue: null
	});

	this.initialized = OMHFormFieldAdvanced.call( this, input, parent, options );

	this.productStyles = _.get( this.parent, 'fields.product_styles', null );

	return OMHFormFieldInksoftProduct.prototype.completeInit.call( this );
}

OMHFormFieldInksoftProduct.prototype = _.create( OMHFormFieldAdvanced.prototype, {
	getProductStyles: function() {
		return _.get( this.parent, 'fields.product_styles', null );
	},
	domInit: function() {

		if( this.settings.createDom ) {

			var productTemplate = _.template([
					'<tr class="inksoft-product-table__template" name="<%= inksoftProduct.ID %>">',
						'<th scope="col" data-col="add">Add?</th>',
						'<th scope="col" data-col="garment">',
							'<div class="omh-add-global-garment-name"><%= inksoftProduct.Name %></div>',
							'<div class="omh-add-global-garment-information">',
								'<%= inksoftProduct.Manufacturer %> - <%= inksoftProduct.Sku %>',
							'</div>',
						'</th>',
					'</tr>',
				].join("\n")
			);

			$(productTemplate(this.settings)).insertBefore(this.parent.$tableHeader.children().last());
		}
	},
	completeInit: function() {
		return OMHFormFieldAdvanced.prototype.completeInit(this);
	}
} );

module.exports = OMHFormFieldInksoftProduct;
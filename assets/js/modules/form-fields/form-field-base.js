
var $ = jQuery;


function OMHFormField( input, parent, options ) {
	
	this.initialized 		= false;

	this.settings 			= _.defaultsDeep(options, {
		origValue 	: null,
		presetValue	: null,
		validationClasses: true
	});

	// parent is either the OMHForm object or extended OMHFormFieldAdvanced field
	this.parent 			= _.defaultTo(parent, null);

	// Optional Hook for DOM preparation
	_.invoke( this, 'domInit' );

	this.$input 			= _.defaultTo(this.$input, $(input));
	this.inputElem 			= _.defaultTo(this.inputElem, _.nth( this.$input, 0 ));
	_.set( this, 'inputElem.omhFormField', this ); // Make this object accessible via the DOM

	// Determine omh-field-wrap element (if any)
	this.$wrap 				= _.defaultTo(this.$wrap, this.$input.closest('.omh-field-wrap') );
	this.wrapElem 			= _.defaultTo(this.wrapElem, _.nth( this.$wrap, 0 ));
	_.set( this, 'wrapElem.omhFormField', this ); // Make this object accessible via the DOM
	
	// Set up custom jQuery set of items that will receive field state classes
	this.$fieldStateElems 	= _.defaultTo(this.$fieldStateElems, $([]));
	this.$fieldStateElems 	= this.$fieldStateElems.add(this.$wrap).add(this.$input).add('[data-omh-field-state="'+this.getName()+'"]');

	this.$fieldLabel 		= _.defaultTo(this.$fieldLabel, this.$wrap.children('label').first());
	this.$invalidFeedback 	= _.defaultTo(this.$invalidFeedback, this.$wrap.find('.invalid-feedback').eq(0));

	this.$fieldResetBtns 		= _.defaultTo(this.$fieldResetBtns, $('.btn.input-reset-btn[for="'+this.getName()+'"]'));
	this.$fieldFocusBtns 		= _.defaultTo(this.$fieldFocusBtns, $('.btn.input-focus-btn[for="'+this.getName()+'"]'));

	// Hook for custom code to modify options //dev:improve
	this.$input.trigger( 'omhFieldBeforeInit.'+this.getName(), [this, this.settings] );

	// Optional Method Hook for additional var initialization 
	_.invoke( this, 'varsInit' );

	// Maybe preset value
	if( _.get( this.settings, 'presetValue', null ) ) {
		this.setValue(this.settings.presetValue, true );
	}

	// Initialize values for state tracking
	this.initValue 	= this.getValue();
	this.origValue 	= this.getValue();
	this.currValue 	= this.getValue();

	this.reset(true);

	// Init event handlers
	this.$input
		.on('focus', this._onFocus.bind(this) )
		.on('blur', this._onBlur.bind(this) )
		.on('input', this._onInput.bind(this) )
		.on('change', this._onChange.bind(this) )
		.on('click', this._onClick.bind(this) );

	// Possible custom button event handlers
	this.$fieldResetBtns.on('click', this.reset.bind(this));
	this.$fieldFocusBtns.on('click', this.focus.bind(this));

	this.$fieldStateElems.addClass('initialized');

	// Add this field to its Form
	_.invoke( this, 'parent.addField', this );

	return OMHFormField.prototype.completeInit.call( this );
}

OMHFormField.prototype = $.extend({}, OMHFormField.prototype, {
	completeInit: function() {
		// Set this.initialized to true
		this.initialized = true;

		// But return false to revert this.initialized if this constructor
		// was run from a child class
		return false;
	},
	focus: function() {
		this.$input.focus();
	},
	getType: function() {
		return this.$input.attr('type');
	},
	getName: function() {
		return this.$input.data('omh-field-name') || this.$input.attr('name');
	},
	getNiceName: function() {
		return this.$fieldLabel.length ? this.$fieldLabel.text() : this.getName();
	},
	// Value getters and setters
	getValue: function() {
		var value = this.$input.val();

		switch( this.$input.data( 'omh-var-type' ) ) {
			case 'int':
				return parseInt( value );
				break;
			case 'price':
				return parseFloat( value ).toFixed(2);
				break;
			default:
				return value;
				break;
		}
		// return this.$input.val();
	},
	setValue: function(value, setOrigValue) {
		setOrigValue = _.defaultTo( setOrigValue, false);
		this.$input.val(value);
		this.currValue = value;
		if( setOrigValue ) {
			this.origValue = value;
			this.$input.trigger('change'); // For select2
		}

		return value;
	},
	clearValue: function(clearOrig,reset) {
		
		this.setValue(null);
		
		if( _.defaultTo( Boolean(clearOrig), true)) {
			this.origValue = null;
		}

		if( _.defaultTo( Boolean(reset), true) ) {
			this.reset();
		}

		this.$input.trigger('clearValue');
	},
	// State getters and setters
	isOrigValue: function() {
		return _.isEqual( this.getValue(), this.origValue );
	},
	checkOrig: function() {
		this.$fieldStateElems
			.toggleClass('orig-value', this.isOrigValue())
			.toggleClass('changed-value', !this.isOrigValue());
	},
	getParentEventNamespace: function() {
		return _.get( this.parent, 'namespace', '' );
	},
	resetState: function() {
		// Reset state classes
		this.$fieldStateElems.addClass('never-focused never-input never-changed orig-value')
			.removeClass('changed-value was-validated is-valid is-invalid');

		// Remove all feedback messages //dev:improve
		this.$wrap.children('.invalid-feedback, .valid-feedback').remove();
	},
	reset: function( quietly ) {
		quietly = _.defaultTo( quietly, false );

		// Reset the input's value
		this.setValue(this.origValue);

		this.resetState();

		// Maybe pre-validate
		if( this.$input.hasClass( 'pre-validate') ) {
			this.validate();
		}

		// Event hook for calc-fields.js
		if( !quietly ) {

			this.$input
				.trigger('reset')
				.trigger({type:'reset', namespace: this.getParentEventNamespace()})
				.trigger({type:'change', isReset:true})
				.trigger({type:'change', isReset:true, namespace: this.getParentEventNamespace()})
				.trigger('calcFields');	
		}
		
	},
	deleteSelf: function( quietly ) {
		quietly = _.defaultTo( quietly, false );

		_.invoke( this.parent, 'deleteField', [this, quietly]);
	},
	handleResponse: function(response) {

		this.processResponseValue(response);

		if( 'success' == response.status ) {
			this.reset(true);
		}
		else {
			this.checkOrig();
			this.validate(false);	
		}

		this.$fieldStateElems.addClass('never-changed-after-response');
	},
	processResponseValue: function(response) {

		this.setValue( _.get( response, ['data', this.getName(), 'value'] ), true);

		this.origValue = this.getValue();
	},
	
	// Event Handlers
	_onFocus: function() {
		this.$fieldStateElems.removeClass('never-focused');
	},
	_onBlur: function() {
		this.validate();				
	},
	_onInput: function() {
		this.$fieldStateElems.removeClass('never-input');
		this.checkOrig();
		if( this.$input.hasClass('was-validated') ) {
			this.validate();
		}
		this.$input.trigger({type:'input', namespace: this.getParentEventNamespace()});
	},
	_onChange: function(event) {
		
		// Don't count the change if this is a reset
		if( !_.get( event, 'isReset') ) {
			this.$fieldStateElems.removeClass('never-changed never-input');	
		}
		
		this.checkOrig();
		this.validate();
	},
	_onClick: function() {
		this.checkOrig();
	},
	isValid: function() {

		var isValid = _.defaultTo( _.invoke( this.inputElem, 'checkValidity' ), true );

		// Validate match-field (fields that must have matching values)
		if( this.$input.data( 'match-field' ) ) {
			isValid = (this.validateMatchField() && isValid);
		}

		return isValid;
	},
	// Validation
	validate: function(addClass) {

		if( _.defaultTo( addClass, true ) ) {
			this.$fieldStateElems.addClass('was-validated');	
		}

		var validationMessage = this.inputElem.validationMessage;

		var isValid = this.isValid();

		// Check for custom validation messages //dev:improve
		if( !isValid && this.$input.attr('pattern') && this.$input.data('invalid-msg-pattern') ) {
			validationMessage = this.$input.data('invalid-msg-pattern');
		}

		// Maybe toggle state classes
		if( this.settings.validationClasses ) {
			this.$fieldStateElems.toggleClass('is-valid', isValid )
				.toggleClass('is-invalid', !isValid );	
		}
		

		// Set (enable/disable) default feedback message //dev:improve
		this.setFeedbackMsg(validationMessage, 'invalid', 'default', !isValid);
	},
	validatePattern: function() {

	},
	validateMatchField: function() {
		var matchField = this.parent.getField( this.$input.data( 'match-field' ) );
		var matchValid = true;

		if( matchField && !( matchField.$input.hasClass('never-changed') || matchField.$input.hasClass('never-changed-after-response') ) ) {
			matchValid = ( matchField.getValue() == this.getValue() );

			// Validate the matchField too. Check whether the match-invalid class has been set to prevent infinite loop
			if( matchValid == this.$input.hasClass( 'match-invalid' ) ) {
				this.$input.toggleClass( 'match-invalid', !matchValid );
				matchField.validate();
			}

			this.$input.toggleClass( 'match-invalid', !matchValid );
			
			var invalidMatchMsg = 'Value must match ' + matchField.getNiceName() + ' field.';

			this.setFeedbackMsg( invalidMatchMsg, 'invalid', 'invalid-match', !matchValid );
		}

		return matchValid;
	},
	setFeedbackMsg: function( msg, type, feedbackId, enabled) {
		msg 		= _.defaultTo(msg, '');
		type 		= _.defaultTo(type, 'invalid');
		enabled		= _.defaultTo( Boolean(enabled), true);

		// See if there is an existing feedback message with this ID
		var $existingMsg = null;
		if( _.defaultTo(feedbackId, null) ) {
			$existingMsg = this.$wrap.children('[data-feedback-id="'+feedbackId+'"]').first();
		}

		if( $existingMsg.length ) {
			$existingMsg.html(msg).toggle(enabled);
		}
		else if(enabled) {
			var msgClass = type + '-feedback';
			$('<div>', {class: msgClass}).attr('data-feedback-id', feedbackId).html(msg).toggle(enabled).appendTo( this.$wrap );
		}
	}
});

module.exports = OMHFormField;

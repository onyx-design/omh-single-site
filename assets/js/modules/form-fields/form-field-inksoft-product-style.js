
var $ = jQuery;

import OMHFormFieldAdvanced 			from './form-field-advanced.js';

function OMHFormFieldInksoftProductStyle( input, parent, options ) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		validationClasses: false,
		createDom: true,
		inksoftProductStyle: null,
		inksoftProductStyleId: null,
		presetValue: null
	});

	this.initialized = OMHFormFieldAdvanced.call( this, input, parent, options );

	this.productStyles = _.get( this.parent, 'fields.product_styles', null );

	return OMHFormFieldInksoftProductStyle.prototype.completeInit.call( this );
}

OMHFormFieldInksoftProductStyle.prototype = _.create( OMHFormFieldAdvanced.prototype, {
	domInit: function() {

		if( this.settings.createDom ) {

			var productStyleTemplate = _.template([
					'<tr class="inksoft-product-style-table__template">',
						'<td class="add-garment-style">',
							'<% if( inksoftProductStyle.Added ) { %>',
								'<small class="text-muted">Added</small>',
							'<% } else { %>',
								'<div class="form-check omh-field-wrap">',
									'<input type="checkbox" class="omh-field-input" id="<%= inksoftProductStyle.ID %>" name="<%= inksoftProductStyle.ID %>" data-omh-field-type="checkbox" data-omh-namespace="inksoft_product" data-omh-subfield-of="inksoft_product" />',
							'<% } %>',
						'</td>',
						'<td>',
							'<img class="omh-add-global-garment-thumbnail" src="//stores.inksoft.com/<%= inksoftProductStyle.Sides[0][\'ImageFilePath\'] %>" />',
							'<div class="omh-add-global-garment-name"><%= inksoftProductStyle.Name %></div>',
						'</td>',
					'</tr>'
				].join("\n")
			);

			$(productStyleTemplate(this.settings)).insertBefore(this.parent.$tableBody.children().last());

			if( !this.settings.inksoftProductStyle.Added ) {
				this.$input = this.parent.$tableBody.children('[name="' + this.settings.inksoftProductStyleId + '"]');
			}
		}
	},
	shouldAdd: function() {

	},
} );

module.exports = OMHFormFieldInksoftProductStyle;
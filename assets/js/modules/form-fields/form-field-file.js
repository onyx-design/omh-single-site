
var $ = jQuery;

import OMHFormField from './form-field-base.js';

function OMHFormFieldFile( input, parent, options ) {
	this.initialized = false;

	this.initialized = OMHFormField.call( this, input, parent, options );

	// Initialize img thumb preview
	this.$fieldImgThumb 	= $('[data-omh-field-img-thumb="'+this.getName()+'"]');

	this.$input
		// .on('focus', this.fileInputFocus.bind(this))
		.on('change', this.fileInputChange.bind(this))
		.on('reset', this.fileInputReset.bind(this));

	return OMHFormFieldFile.prototype.completeInit.call( this );
}

OMHFormFieldFile.prototype = $.extend({}, OMHFormField.prototype, OMHFormFieldFile.prototype, {
	focus: function() {
		this.$input.focus().click();
	},
	fileInputFocus: function(event) {
		this.$input.click();
	},
	getValue: function() {
		return this.$input[0].files[0];
	},
	getFileName: function() {
		return _.get( this.getValue(), 'name', null );
	},
	setValue: function(value) {
		this.$input.val(value);
		this.setFileLabel();
	},
	setFileLabel: function(label) {
		label = _.defaultTo(label, _.defaultTo( this.getFileName(), 'Choose New File' ));
		this.$input.next('label').text(label);
		// Set wrap class to track if new file is set
		this.$fieldStateElems.toggleClass('valid-new-file',_.defaultTo(this.getFileName(),false));
	},
	fileInputChange: function() {
		if( this.getValue() ) {
			$('.btn.input-reset-btn[for="'+this.getName()+'"]').show();
		}
		this.setFileLabel();
	},
	fileInputReset: function() {
		// Hide reset buttons (only used for custom file inputs
		this.$fieldResetBtns.hide();
		this.$wrap.removeClass('valid-new-file').find('input.form-file-img-remove').prop('checked',false).trigger('change'); //dev:improve
	},
	processResponseValue: function(response) {
		if( 'success' == response.status ) {
			this.setValue(this.origValue);
			var newImageSrc = _.get( response, ['data', this.getName(), 'value'] );

			if( newImageSrc ) {
				this.$fieldStateElems.removeClass('file-no-image').addClass('file-has-image');
				this.$fieldImgThumb.html('<img class="mh-file-input-thumb mb-1" src="' + newImageSrc +'" />');
			}	
			else if ( false === newImageSrc ){
				this.$fieldStateElems.removeClass('file-has-image').addClass('file-no-image');
				this.$fieldImgThumb.html('<div class="no-image"></div>');
			}
		}
	}
});

module.exports = OMHFormFieldFile;
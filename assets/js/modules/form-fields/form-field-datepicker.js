
var $ = jQuery;

import OMHFormField from './form-field-base.js';

function OMHFormFieldDatePicker( input, parent, options ) {
	this.initialized = false;

	options = _.defaultsDeep(options, {
		presetValue	: false,
		flatpickr: {
			enableTime: true,
			minDate: (($(input).data('allowPast') === true) ? false : new Date()),
			dateFormat: 'Y-m-d H:i:s',
			altInput: true,
			altFormat: 'F d, Y h:i K',
		}
	});

	return OMHFormField.call( this, input, parent, options );
}

OMHFormFieldDatePicker.prototype = $.extend({}, OMHFormField.prototype, OMHFormFieldDatePicker.prototype, {
	varsInit: function() {
		// Check input elem
		if( this.$input.attr( 'data-flatpickr-options' ) ) {
			var flatpickrOverrides = JSON.parse(atob(this.$input.attr('data-flatpickr-options')));

			_.merge(this.settings.flatpickr, flatpickrOverrides);
		}

		this.datePicker = this.$input.flatpickr(this.settings.flatpickr);
	}
});

module.exports = OMHFormFieldDatePicker;
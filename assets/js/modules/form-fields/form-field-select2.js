
var $ = jQuery;

import OMHFormField from './form-field-base.js';

function OMHFormFieldSelect2( input, parent, options ) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		select2: {
			ajax: {
				delay: 250,
				transport: this.select2Transport.bind(this),
				processResults: this.select2ProcessResults
			},
			width:'100%',
			minimumInputLength: 3
		}
	});

	this.initialized = OMHFormField.call( this, input, parent, options );

	var	ajaxSearchModel 	= (this.ajaxSearchModel = this.$wrap.data('omh-ajax-search-model')),
		inputSearchNonce 	= (this.inputSearchNonce = $('#omh_ajax_search_nonce').val());

	this.isMultiple 		= _.get( this.settings.select2, 'multiple', false ); // dev:improve

	this.$input
		.on('select2:select', this._onSelect2Select.bind(this) )
		.on('change.select2', this._onChange.bind(this))
		.on('select2:open', this._onFocus.bind(this));

	this.$input.select2( this.settings.select2);
	// Maybe preset value
	if( _.get( this.settings, 'presetValue', null ) ) {
		this.setValue(this.settings.presetValue, true);
	}

	return OMHFormFieldSelect2.prototype.completeInit.call(this);
}

OMHFormFieldSelect2.prototype = $.extend({}, OMHFormField.prototype, OMHFormFieldSelect2.prototype, {
	_onSelect2Select: function(event) {
		if( _.get( this.settings.select2, 'multiple', false ) ) {

			var currValue = _.toArray( this.getValue() );

			currValue.push( event.params.data.id );

			this.setValue( currValue );
		}
		else {

			this.setValue(event.params.data.id);	
		}
		
		
		this.$input.trigger('change');
	},
	// setValue: function(value, setOrigValue) {
	// 	setOrigValue = _.defaultTo( setOrigValue, false);

	// 	// Add <option> if not exists
	// 	if( $input.find("option[value='" + value + "']").length ) {
	// 	    $input.val(value);
	// 	} else { 
	// 	    // Create a DOM Option and pre-select by default
	// 	    var newOption = new Option(data.text, data.id, true, true);
	// 	    // Append it to the select
	// 	    $('#mySelect2').append(newOption).trigger('change');
	// 	} 
	// 	this.$input.val(value);
	// 	this.currValue = value;
	// 	if( setOrigValue ) {
	// 		this.origValue = value;
	// 	}
	// 	return value;
	// },
	clearValue: function(clearOrig, reset) {
		clearOrig 	= _.defaultTo( Boolean(clearOrig), true);
		reset 		= _.defaultTo( Boolean(reset), true);

		this.$input.empty();
		OMHFormField.prototype.clearValue.call( this, clearOrig, reset );
		this.$input.select2(this.settings.select2);
	},
	processResponseValue: function(response) {

		// Check if multiple and do other stuff here? This is an issue with categories mainly
		
		var value = _.get( response, ['data', this.getName(), 'value'], false );

		if( !value ) {
			value = $('select.omh-field-input[name="product_categories"]').find('option:contains(Uncategorized)').val();
		}

		// Add value if not exists
		if( value && !this.$input.children("option[value='" + _.get( value, 'id', value ) + "']").length ) {

			// var valueLabel = _.get( response, ['data', this.getName(), 'value_label'], value );
			var newOption = new Option( _.get( value, 'text', value ), _.get( value, 'id', value ), true, true );

			this.$input.append(newOption).trigger('change');
		}

		this.setValue( _.get( value, 'id', value ), true);

		this.origValue = this.getValue();

		this.resetState();
	},
	select2ProcessResults: function( results ) {
		return {
			results: results.data.results
		};
	},
	isOrigValue: function() {
		if( !this.isMultiple ) {
			return _.isEqual( this.getValue(), this.origValue );
		}
		else {
			return _.isEqual( _.sortBy( this.getValue() ), _.sortBy( this.origValue ) );
		}
	},
	select2Transport: function(params, success, failure) {
		
		return omh.ajax(
			'omh_ajax_search',
			{
				omh_model: this.ajaxSearchModel,
				search_term: params.data.term
			},
			success,
			this.inputSearchNonce
		);
	}
});

module.exports = OMHFormFieldSelect2;
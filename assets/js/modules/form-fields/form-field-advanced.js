
var $ = jQuery;

import OMHFormField from './form-field-base.js';

function OMHFormFieldAdvanced( input, parent, options ) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		autoInitFields: true,
		subfieldsIsArray: false,
		pruneFieldsOnSetValue: false
	});

	// Child field instances
	this.fields = options.subfieldsIsArray ? [] : {};

	this.initialized = OMHFormField.call( this, input, parent, options );



	this.settings.namespace = this.namespace = _.get( this.settings, 'namespace', _.defaultTo( this.$wrap.data('omh-namespace'), this.getName() ) );

	this.settings.childFieldSelector = '[data-omh-subfield-of="'+this.namespace+'"]';

	

	if( this.settings.autoInitFields ) {
		this.initChildFields();
	}

	// Initialize values for state tracking //dev:improve
	this.initValue 	= this.getValue();
	this.origValue 	= this.getValue();
	this.currValue 	= this.getValue();

	var namespacedEvents = [
		'input.' + this.namespace,
		'change.' + this.namespace,
		'reset.' + this.namespace
	];

	$(document).on( namespacedEvents.join(' '), this._onInput.bind(this) );

	return OMHFormFieldAdvanced.prototype.completeInit.call( this );
}

OMHFormFieldAdvanced.prototype = $.extend({}, OMHFormField.prototype, OMHFormFieldAdvanced.prototype, {
	// getName: function() {
	// 	return this.$wrap.attr('name');
	// },
	initChildFields: function() {

		var self 		= this,
			$childFields = $( this.settings.childFieldSelector );

		$childFields.each(function(index, childField) {

			var options = {};

			// Check for preset value of child
			if( _.has( self.settings.presetValue, $(childField).attr('name') ) ) {

				options.presetValue = self.settings.presetValue[ $(childField).attr('name') ];
			}

			$(childField).OMHFormField( self, options );
		});

		
		// $childField.OMHFormField( this );
	},
	// Called from child fields via OMHFormFieldBase constructor
	addField: function(field) { //dev:improve
		if( this.settings.subfieldsIsArray ) {
			this.fields.push(field);
		}
		else {
			this.fields[ field.getName() ] = field;	
		}
	},
	// Will often be overwritten for advanced customization
	getValue: function() {
		return this.settings.subfieldsIsArray 
			? _.toArray( _.invokeMap( this.fields, 'getValue' ) )
			: _.zipObject( _.keys(this.fields), _.invokeMap(this.fields, 'getValue') );
	},
	setValue: function(value, setOrigValue) {
		setOrigValue = _.defaultTo( setOrigValue, false);

		this.currValue = value;

		// Field Pruning: Remove fields not found in new setValue
		// Maybe prepare list of previous field keys to know which ones to prune
		var pruneKeys = [];
		if( this.settings.pruneFieldsOnSetValue ) {
			pruneKeys = _.keys( this.fields );
		}

		var self = this;
		_.forEach( value, function( fieldValue, fieldIndex ) {

			var childField = _.defaultTo( self.fields[ fieldIndex ] );

			if( childField ) {

				childField.setValue( fieldValue, setOrigValue );

				// Maybe update field pruning key tracking
				if( self.settings.pruneFieldsOnSetValue ) {
					_.pull( pruneKeys, fieldIndex );	
				}
				
			}
			// self.fields[ fieldIndex ].setValue( fieldValue );
		});

		_.map( pruneKeys, function(deleteFieldKey) {
			self.deleteField( self.fields[ deleteFieldKey ] );
		});

		if( setOrigValue ) {
			this.origValue = this.getValue();
		}

		// if( this.settings.subfieldsIsArray ) {
		// 	var self = this;
		// 	_.forEach( this.fields, function( field, index) {
		// 		field.setValue( value[ index ] );
		// 	});
		// }
	},
	
	isValid: function() {
		return _.every( _.invokeMap( this.fields, 'isValid' ) );
	},
	reset: function( quietly ) {
		quietly = _.defaultTo( quietly, false );

		OMHFormField.prototype.reset.call( this, quietly );

		_.invokeMap( this.fields, 'reset', quietly );
	},
	deleteField: function(field, quietly) {
		quietly = _.defaultTo( quietly, false );

		field.$wrap.remove();
		// priceTier.$tierHeader.remove();

		$(document).trigger('fieldDeleted', [field]);

		if( this.settings.subfieldsIsArray ) {
			_.remove( this.fields, field );
		}
		else {
			_.unset( this.fields, field.getName() );
		}
		
		

		if( !quietly ) {
			this.$input.trigger({type:'change', namespace: this.getParentEventNamespace()});	
		}
		
	},	
});

module.exports = OMHFormFieldAdvanced;


var $ = jQuery;

import OMHFormFieldAdvanced 			from './form-field-advanced.js';
// import OMHFormFieldInksoftProduct 		from './form-field-inksoft-product.js';
// import OMHFormFieldInksoftProductStyle 	from './form-field-inksoft-product-style.js';

function OMHFormFieldInksoftProductResults( input, parent, options ) {

	this.initialized = false;

	options = _.defaultsDeep(options, {
		// validationClasses		: false,
		inksoftProduct 			: null,
	});

	this.initialized = OMHFormFieldAdvanced.call( this, input, parent, options );

	this.origValue = this.settings.presetValue;

	return OMHFormFieldInksoftProductResults.prototype.completeInit.call(this);
}

OMHFormFieldInksoftProductResults.prototype = _.create(OMHFormFieldAdvanced.prototype, {
	domInit: function() {

		var self = this,
			$tableHeader = this.parent.$container.find('.omh-table-section-headers'),
			$tableBody = this.parent.$container.find('.omh-table-section-results'),
			productTemplate = _.template([
					'<tr class="inksoft-product-table__template" name="<%= inksoftProduct.ID %>">',
						'<th scope="col" data-col="add">Add?</th>',
						'<th scope="col" data-col="garment">',
							'<div class="omh-add-global-garment-name"><%= inksoftProduct.Name %></div>',
							'<div class="omh-add-global-garment-information">',
								'<%= inksoftProduct.Manufacturer %> - <%= inksoftProduct.Sku %>',
							'</div>',
						'</th>',
					'</tr>',
				].join("\n")
			),
			productStyleTemplate = _.template([
					'<tr class="inksoft-product-style-table__template">',
						'<td class="add-garment-style">',
							'<% if( inksoftProductStyle.Added ) { %>',
								'<small class="text-muted">Added</small>',
							'<% } else { %>',
								'<div class="form-check omh-field-wrap">',
									'<input type="checkbox" class="omh-field-input" id="<%= inksoftProductStyle.ID %>" name="<%= inksoftProductStyle.ID %>" data-omh-field-type="checkbox" data-omh-subfield-of="inksoft_style_additions" />',
							'<% } %>',
						'</td>',
						'<td>',
							'<img class="omh-add-global-garment-thumbnail" src="//stores.inksoft.com/<%= inksoftProductStyle.Sides[0][\'ImageFilePath\'] %>" />',
							'<div class="omh-add-global-garment-name"><%= inksoftProductStyle.Name %></div>',
						'</td>',
					'</tr>'
				].join("\n")
			);

		$(productTemplate(this.settings)).insertBefore($tableHeader.children().last());

		_.forEach( this.settings.inksoftProduct.Styles, function(inksoftProductStyle) {
			$(productStyleTemplate({inksoftProductStyle})).insertBefore($tableBody.children().last());
		} );
	},
	isValid: function() {
		
		if( _.isEmpty( this.getValue() ) ) {
			return false;
		}

		return OMHFormFieldAdvanced.prototype.isValid.call(this);
	},
	// getValue: function() {
	// 	var inksoftProductStyles = [];

	// 	_.forEach(this.fields, function(productStyle, productStyleId) {
	// 		inksoftProductStyles[ productStyleId ] = productStyle.getValue();
	// 	});

	// 	return inksoftProductStyles;
	// },
	// addInksoftProduct: function(inksoftProduct) {

	// 	var self = this,
	// 		inksoftProductOptions = {
	// 			inksoftProductId 	: inksoftProduct.ID,
	// 			inksoftProduct 		: inksoftProduct,
	// 			presetValue: {
	// 				inksoftProductId 	: inksoftProduct.ID,
	// 				styles 				: _.get( inksoftProduct, 'Styles', [] )
	// 			}
	// 		};

	// 	// Initialize styles

	// 	var newInksoftProduct = new OMHFormFieldInksoftProduct( '', this, inksoftProductOptions );

	// 	_.forEach( inksoftProduct.Styles, function(inksoftProductStyle) {

	// 		var inksoftProductStyleOptions = {
	// 			inksoftProductStyleId 	: inksoftProductStyle.ID,
	// 			inksoftProductStyle 	: inksoftProductStyle,
	// 			presetValue 			: {
	// 				inksoftProductStyle 	: inksoftProductStyle,
	// 				inksoftProductStyleId 	: inksoftProductStyle.ID,
	// 			}
	// 		};

	// 		var newInksoftProductStyle = new OMHFormFieldInksoftProductStyle( '', self, inksoftProductStyleOptions );
	// 	});

	// 	this.$input.trigger({type: 'change',namespace: this.getParentEventNamespace()});
	// }
});

module.exports = OMHFormFieldInksoftProductResults;
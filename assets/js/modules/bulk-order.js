import _ from '../vendor/lodash.min';

module.exports = (function($) {

	var $orderQtyTotal = $('input[name="order_qty_total"]'),
		orderQtyTotal = 0,
		qtyTiers = [],
		currentTier = null,
		$priceTierCols = $('[data-tier-min]'),
		animatingTiers = false,
		garments = [];

	function getOrderQty() {
		return omh.CalcFields.getValue( $orderQtyTotal );
	}

	function getCurrentTier() {

		var orderQty = getOrderQty();
		var tier = null;

		_.forEachRight( qtyTiers, function(tierMin) {

			if( orderQty >= tierMin ) {
				tier = tierMin;
				return false;
			}
		});

		return tier;
	}

	function setTierUI() {

		var oldOrderQtyTotal = orderQtyTotal;

		orderQtyTotal = getOrderQty();

		//var $priceTierCols_directional = false;
		var directionForward = null;

		// console.log( 'oldOrderQtyTotal', oldOrderQtyTotal);
		// console.log('orderQtyTotal', orderQtyTotal);
		
		// Set active tier
		$priceTierCols.removeClass('active-tier')
			.filter('[data-tier-min="' + currentTier + '"]').addClass('active-tier');
		

		// if( !$priceTierCols.is(':animated') ) {



		// 	_.forEach(qtyTiers, function(index, qtyTier) {

		// 		var $priceTierCol = $('th[data-tier-min="'+qtyTier+'"]');
		// 		var animateCallback = null,
		// 			thisTier = {
		// 				min : qtyTier,
		// 				$progress : $priceTierCol.find('.progress'),
		// 				nextTier : _.defaultTo( qtyTiers[index +1], (qtyTier * 2)),
						
		// 				orderTotal : getValue( $orderQtyTotal ),
		// 			};

		// 			thisTier.interval = thisTier.nextTier - currentTier;
		// 			thisTier.progressOverall = Math.round( ( thisTier.orderTotal / thisTier.interval ) * 100 );
		// 			thisTier.progressPercent = Math.round( ( ( thisTier.orderTotal - thisTier.min ) / thisTier.interval ) * 100 );

					
					
		// 			console.log( thisTier );
		// 			if( thisTier.progressOverall < 0 ) {


		// 			}
		// 			else if ( thisTier.progressOverall > 100 ) {


		// 			}
		// 			else {

		// 				animateCallback = setTierUI;
		// 			}

		// 			if( thisTier.$progress.data('target-progress') != thisTier.progressPercent ) {

		// 				if( thisTier.min != currentTier ) {
		// 					animateCallback = setTierUI;
		// 				}

		// 				animatingTiers = true;

		// 				thisTier.$progress.data('target-progress', thisTier.progressPercent); 

		// 				console.log( thisTier.$progress );

		// 				thisTier.$progress.animate(
		// 					{width: thisTier.progressPercent + '%'},
		// 					500,
		// 					'swing',
		// 					animateCallback
		// 				);
		// 			}
		// 	});
		// }
		



		// Determine direction to animate
		// if( orderQtyTotal < oldOrderQtyTotal ) {

		// 	console.log('here');

		// 	directionForward = true;
		// 	$priceTierCols_directional = $($priceTierCols.filter('th').get().reverse());

		// 	var tierIndex = 0;

		// 	$priceTierCols_directional.each(function( index ) {

				
		// 		var $priceTierCol = $(this),
		// 			thisTier = {
		// 				min : $priceTierCol.data('tier-min'),
		// 				$progress : $priceTierCol.find('.progress'),
		// 				nextTier : _.defaultTo( qtyTiers[index +1], ($priceTierCol.data('tier-min') * 2)),
						
		// 				orderTotal : getValue( $orderQtyTotal ),
		// 			};

		// 			thisTier.interval = thisTier.nextTier - currentTier;
		// 			thisTier.progressShowing = thisTier.$progress.css('width');
		// 			thisTier.progressPercent = ( ( thisTier.orderTotal - thisTier.min ) / thisTier.interval ) * 100;

		// 			console.log( thisTier );
		// 			// thisTier = $priceTierCol.data('tier-min'),
		// 			// $tierProgress = $priceTierCol.find('.progress'),
		// 			// tierProgressShowing = $tierProgress.css('width'),
		// 			// nextTier = _.defaultTo( qtyTiers[index +1], false),
		// 			// tierInterval = nextTier - currentTier,
		// 			// tierProgress = getValue( $orderQtyTotal ),
		// 			// tierPercent = ( tierProgress / tierInterval ) * 100;

		// 		if( currentTier < thisTier.min && '0%' != thisTier.progressShowing ) {

		// 			//$priceTierCol.removeClass('active-tier')
		// 			thisTier.$progress.animate(
		// 				{width: thisTier.progressPercent + '%'},
		// 				500,
		// 				'swing',
		// 				setTierUI
		// 			);

		// 			return false;
		// 		}

		// 	});
		// }
		// else if( orderQtyTotal > oldOrderQtyTotal ) {

		// 	directionForward = false;
		// 	$priceTierCols_directional = $priceTierCols.filter('th');
		// }

		// if( $priceTierCols_directional ) {

		// 	var tierIndex = 0;

		// 	$priceTierCols_directional.each(function( index, $priceTierCol ) ) {

		// 		if( )

		// 	}
		// }

		// $priceTierCols.filter('th').each(function(index, $priceTierCol) {

		// 	thisTier = $priceTierCol.data('tier-min');

		// 	if( currentTier <= thisTier ) {
				
		// 		var nextTier = _defaultTo( qtyTiers[index +1], false),
		// 			tierInterval = nextTier - currentTier,
		// 			tierProgress = getValue( $orderQtyTotal ),
		// 			tierPercent = ( tierProgress / tierInterval ) * 100;

		// 		$priceTierCol.find('.progress').css('width',tierPercent + '%');

		// 	}
		// });

		// _.forEach( $priceTierCols.filter('th'), function($priceTierCol) {

		// 	thisTier = $priceTierCol.data('tier-min');

		// 	if( currentTier == thisTier ) {
		// 		var nextTier = 
		// 	}
		// });

		// Set active tier
		$priceTierCols.removeClass('active-tier')
			.filter('[data-tier-min="' + currentTier + '"]').addClass('active-tier');


	}


	function updateUI() {

		var lastTier = currentTier;

		currentTier = getCurrentTier();

		if( lastTier != currentTier ) {

			// update Garment Prices
			_.forEach(garments, function(garment) {
				garment.updatePrices();
			});
			
			
		}

		setTierUI();

	}

	function Garment(container) {

		var $container = $(container),
			garmentName = $container.data('garment'),
			$tiersTable = $container.find('.table--garment-price-tiers'),
			$variationsTable = $container.find('.table--garment-variations'),
			priceTiers = [],
			variations = [];

		function Variation(variation_id) {

		}

		function updatePrices() {

			var tierBasePrice = 0;

			_.forEach(priceTiers, function(priceTier) {

				if( priceTier.min == currentTier ) {
					tierBasePrice = parseFloat( priceTier.price );
					return false;
				}
			});

			_.forEach(variations, function(variation) {
				var variationTierPrice = _.defaultTo( variation.markup, 0 ) + tierBasePrice;

				omh.CalcFields.setValue( parseFloat( variationTierPrice ).toFixed( 2 ), $variationsTable.find('[data-variation-field="line_item_price"][data-variation-id="'+variation.variation_id+'"]'));
			});

		}

		function initGarment() {
			// Garment name

			// Initialize price tiers
			$tiersTable.find('.garment-tier').each(function() {
				priceTiers.push({
					min:$(this).data('tier-min'),
					price:$(this).data('tier-price')
				});
			});

			// Initialize variations
			$variationsTable.find('th div[data-variation-id]').each(function() {
				variations.push({
					variation_id: $(this).data('variation-id'),
					markup: omh.CalcFields.getValue( $(this) )
				});
			});
		}
		initGarment();

		return {
			garmentName: garmentName,
			updatePrices: updatePrices,
			priceTiers: priceTiers,
			variations: variations
		};
	}

	

	function initBulkOrderScreen() {

		// Init quantity tier breaks
		$('.table--order-price-tiers').eq(0).find('.garment-tier').each(function() {
			qtyTiers.push($(this).data('tier-min'));
		});

		// Ini ti
		$('.garment-wrap').each(function() {
			garments.push( new Garment( $(this) ) );
		});

		orderQtyTotal = omh.CalcFields.getValue( $orderQtyTotal );

		$priceTierCols = $('[data-tier-min]');

		$orderQtyTotal = $('input[name="order_qty_total"]');

		updateUI();
		$orderQtyTotal.on('change', updateUI);
	}
	$(document).ready(initBulkOrderScreen);

	return {
		getOrderQty: getOrderQty,
		$priceTierCols: $priceTierCols,
		qtyTiers: qtyTiers,
		garments: garments
	};

})(jQuery);
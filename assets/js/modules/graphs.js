require('../vendor/Chart.min');

module.exports = (function($) {

	var instances = [];

	function Graph(container) {

		// var $container = (this.$container = $(container));

		var graph = new Chart(container, graphData[container.id]);
	}

	function initGraphs() {

		$('.mh-graph').each(function() {
			instances.push( new Graph(this) );
		});
	}

	$(document).ready(initGraphs);

	return {
		instances: instances
	};

})(jQuery);
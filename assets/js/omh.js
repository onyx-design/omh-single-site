import _ from './vendor/lodash.min';
import ClipboardJS from './vendor/clipboard.min';
import './vendor/jquery.ui.widget';
import './vendor/jquery.fileupload';
require('./vendor/bootstrap.bundle.min');
require('./vendor/flatpickr.min');

// Moved debug flag out of omh assignment so it can be used immediately in omh componennts
global.omhDebug = window.omhDebug = true;

global.omh = window.omh = {
	debug: 				omhDebug,
	ajax: 				require('./modules/ajax'),
	Drawers: 			require('./modules/drawers'),
	FormFields: 		require('./modules/form-field'),
	Forms: 				require('./modules/forms'),
	Tables: 			require('./modules/tables'),
	// Graphs: 			require('./modules/graphs'),
	Notices: 			require('./modules/notices'),
	CalcFields: 		require('./modules/calc-fields'),
	// BulkOrder:  		require('./modules/bulk-order'),
	Modals: 			require('./modules/modals'),
	PriceWidget:		require('./modules/price-widget'),
	// CreateProduct: 		require('./modules/create-product'),
	// AdminEditProduct: 	require('./modules/admin-edit-product'),
	// TempUpload: 		require('./modules/temp-upload'),
	pdp: 				require('./modules/frontend/pdp.js')
};

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-109289476-1', 'auto');
ga('send', 'pageview');


jQuery(document).ready(function($) {
	new ClipboardJS('.copy-to-clipboard');

	$('.form-control')
	.click(function(e) {
		if($(this).prop('readonly') ) { // Auto-select contents of disabled cells
			$(this).select();
		}	
	} );

	jQuery('[data-toggle="popover"]').popover();	

	// disable mousewheel on a input number field when in focus
	// (to prevent Cromium browsers changing the value when scrolling)
	$(document).on('focus', 'input[type=number]', function (e) {
	  $(this).on('mousewheel.disableScroll', function (e) {
	    e.preventDefault();
	  });
	});
	$(document).on('blur', 'input[type=number]', function (e) {
	  $(this).off('mousewheel.disableScroll');
	})
	.on('omh_save_user_account_details', function(event,responseData) {

		var userInitials 	= _.get( responseData, 'data.user_initials.value' ),
			userFullName	= _.get( responseData, 'data.full_name.value' );

		$( '.dashboard-user-initials' ).text( userInitials );
		$( '.dashboard-user-fullname' ).text( userFullName );
	});

	// Home button animation
	if( $('#mh-home-hero .nectar-button').length ) {

		var $homeCta 	= $('#mh-home-hero .nectar-button span').eq(0),
			ctaPrefix 	= 'Search ',
			ctaSuffixes = ['Products', 'Fraternities', 'Sororities'],
			typingSpeed	= 100,
			typingPause = 600,
			suffixIndex = 0,
			letterIndex = 1,
			typeForward = 1;

		var currentWord 	= ctaSuffixes[ suffixIndex ],
			currentPart 	= '';

			function ctaType() {
				// If currentPart is not blank and does not equal the current word
				// we are mid-typing
				if( ( typeForward == -1 && currentPart ) || ( typeForward == 1 && currentPart != currentWord ) ) {
					letterIndex += typeForward; // Advance (or reduce) the slice length
					currentPart = currentWord.slice(0, letterIndex);
					$homeCta.html( ctaPrefix + currentPart );
					setTimeout( ctaType, typingSpeed );
				}
				else {

					// Maybe advance word
					if( typeForward == -1 ) {
						suffixIndex++;
						suffixIndex = ( suffixIndex >= ctaSuffixes.length ? 0 : suffixIndex );
						currentWord = ctaSuffixes[ suffixIndex ];
					} 

					// Always switch direction
					typeForward = typeForward * -1;
					
					setTimeout( ctaType, typingPause );
				}
				
			}

			setTimeout( ctaType, 1500);
	}

	// Login Page
	$('#login-register-links #customer-register').on( 'click', function(e) {
		$('.woocommerce-account .main-content .nectar-form-controls .control:last-child').click();
	})

	// Add Signup modal attributes
	$('.omh-trigger-signup-modal').attr('data-toggle', 'modal');
	$('.omh-trigger-signup-modal').attr('data-target', '#signup-modal');

	if (window.location.hash === "#signup") {
		$("#signup-modal").modal();
	}
	// Add Start design modal attributes
	$('.omh-trigger-start-design-modal').attr('data-toggle', 'modal');
	$('.omh-trigger-start-design-modal').attr('data-target', '#start-design-modal');

	// $(document).on('show.bs.modal', '.modal', function() {
	// 	$(".woocommerce-store-notice").css("display", "none")
	// })

	// $(document).on('hide.bs.modal', '.modal', function() {
	// 	$(".woocommerce-store-notice").css("display", "block")
	// })

	// Fire custom load event to be able to run inline JS after OMH and theme code has ran
	$(document).trigger('omhloaded');
});
<div class="mh-design-gallery-item">
    <input type="radio" name="designGalleryOption" id="designGalleryOption2" value="RETRO" style="display:none;">
    <label class="mh-design-gallery-item__label" for="designGalleryOption2">
        <img src="https://merchstaging.wpengine.com/wp-content/uploads/2020/06/Retro.jpg">
    </label>
</div>



<div class="form-check form-check-inline">
<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
<label class="form-check-label" for="inlineRadio2">
<img src="https://merchstaging.wpengine.com/wp-content/uploads/2020/06/Retro.jpg">
</label>
</div>	

<div class="mh-design-gallery-selected-design">
    <div class="mh-design-gallery-selected-design__img"></div>
    <h3 class="mh-design-gallery-selected-design__title"></h3>
    <p class="mh-design-gallery-selected-design__descrip">Please select a design from the options above, or choose Create Your Own for a custom design.</p>
</div>